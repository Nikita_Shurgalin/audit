
#Область ПрограммныйИнтерфейс

// Заполняет реквизиты формы обработки и свойства реквизита "Объект" формы обработки по данным параметров.
//
// Параметры:
//  Параметры - ДанныеФормыСтруктура - Параметры формы обработки.
//  Форма - ФормаКлиентскогоПриложения - Форма обработки.
//
Процедура ЗаполнитьПараметрыОбработки(Параметры, Форма) Экспорт
	
	ЗаполнитьЗначенияСвойств(Форма, Параметры, ИменаЗаполняемыхРеквизитовФормы(Параметры, Форма));
	ЗаполнитьЗначенияСвойств(Форма.Объект, Параметры, ИменаЗаполняемыхРеквизитовОбъектаФормы(Параметры, Форма));
	
КонецПроцедуры

// Возвращает структуру параметров для открытия формы обработки
//
// Параметры:
//  ФормаВладелец - ФормаКлиентскогоПриложения - форма из которой происходит открытие обработки.
//  ИмяОбработки - Строка - имя открываемой обработки.
//  ДополнительныеКлючи - Строка - ключи структуры параметров.
//  Значения - Произвольный - значения параметров по умолчанию.
//
// Возвращаемое значение:
//   Структура - параметры обработки
//
Функция СтруктураПараметровОбработки(ФормаВладелец, ИмяОбработки, ДополнительныеКлючи = "", Значения = Неопределено) Экспорт
	
	ПараметрыОбработки = Обработки[ИмяОбработки].МассивИспользуемыхПараметров();
	
	СтруктураПараметров = Новый Структура(КлючиПараметровОбработки(ПараметрыОбработки, ДополнительныеКлючи));
	
	ИменаРеквизитов = СтрСоединить(ПараметрыОбработки, ",");
	
	Для Каждого Элемент Из глРеквизиты(ФормаВладелец.КнопкаУчетнойТочки, ИменаРеквизитов) Цикл
		
		Если ТипЗнч(Элемент.Значение) = Тип("РезультатЗапроса") Тогда 
			
			СписокЗначений = Новый СписокЗначений;
			СписокЗначений.ЗагрузитьЗначения(Элемент.Значение.Выгрузить().ВыгрузитьКолонку("Значение"));
			
			СтруктураПараметров[Элемент.Ключ] = СписокЗначений;
			
		Иначе 
			
			СтруктураПараметров[Элемент.Ключ] = Элемент.Значение;
			
		КонецЕсли; 
		
	КонецЦикла;
	
	ЗаполнитьЗначенияСвойств(СтруктураПараметров, ФормаВладелец);
	ЗаполнитьЗначенияСвойств(СтруктураПараметров, ФормаВладелец.Объект);
	
	Если Значения <> Неопределено Тогда
		ЗаполнитьЗначенияСвойств(СтруктураПараметров, Значения);
	КонецЕсли;
	
	Возврат СтруктураПараметров;
	
КонецФункции


// Добавляет поле ввода на форму
//
// Параметры:
//  Форма  - ФормаКлиентскогоПриложения - изменяемая форма
//  ПутьКДанным  - Строка - путь к данным элемента
//  Имя  - Строка - имя элемента
//  Родитель  - ГруппаФормы, ТаблицаФормы, ПолеФормы, КнопкаФормы - имя элемента
//  ВставитьПередЭлементом  - ГруппаФормы, ДекорацияФормы, КнопкаФормы, ТаблицаФормы, ПолеФормы - элемент,
//              перед которым нужно разместить перемещаемый элемент.
//
// Возвращаемое значение:
//  ПолеФормы   - добавленный элемент.
//
Функция ПолеВводаДобавить_КУТ(Форма, ПутьКДанным, Имя = "", Родитель = Неопределено, ВставитьПередЭлементом = Неопределено) Экспорт
	
	Поле = к2ИзменениеФорм.ПолеВвода(Форма, ПутьКДанным, Имя, Родитель, ВставитьПередЭлементом);
	Поле.УстановитьДействие("ПриИзменении", "Подключаемый_РеквизитПриИзменении");
	
	Возврат Поле;
	
КонецФункции

// Добавляет поле флажка на форму
//
// Параметры:
//  Форма  - ФормаКлиентскогоПриложения - изменяемая форма
//  ПутьКДанным  - Строка - путь к данным элемента
//  Имя  - Строка - имя элемента
//  Родитель  - ГруппаФормы, ТаблицаФормы, ПолеФормы, КнопкаФормы - имя элемента
//  ВставитьПередЭлементом  - ГруппаФормы, ДекорацияФормы, КнопкаФормы, ТаблицаФормы, ПолеФормы - элемент,
//              перед которым нужно разместить перемещаемый элемент.
//
// Возвращаемое значение:
//  ПолеФормы   - добавленный элемент.
//
Функция ПолеФлажкаДобавить_КУТ(Форма, ПутьКДанным, Имя = "", Родитель = Неопределено, ВставитьПередЭлементом = Неопределено) Экспорт
	
	Поле = к2ИзменениеФорм.ПолеФлажка(Форма, ПутьКДанным, Имя, Родитель, ВставитьПередЭлементом);
	Поле.УстановитьДействие("ПриИзменении", "Подключаемый_РеквизитПриИзменении");
	
	Возврат Поле;
	
КонецФункции

// Добавляет таблицу списком значений
//
// Параметры:
//  Форма  - ФормаКлиентскогоПриложения - изменяемая форма
//  ПутьКДанным  - Строка - путь к данным таблицы
//  Родитель  - ГруппаФормы, ТаблицаФормы, ПолеФормы, КнопкаФормы - имя элемента
//  ВставитьПередЭлементом  - ГруппаФормы, ДекорацияФормы, КнопкаФормы, ТаблицаФормы, ПолеФормы - элемент,
//              перед которым нужно разместить перемещаемый элемент.
//
// Возвращаемое значение:
//  ТаблицаФормы   - добавленный элемент.
//
Функция СписокТаблицыДобавить_КУТ(Форма, ПутьКДанным, Родитель = Неопределено, ВставитьПередЭлементом = Неопределено) Экспорт
	
	Имя = к2ИзменениеФорм.ИмяПоПутиКДанным(ПутьКДанным);
	
	РеквизитСписка = Новый РеквизитФормы(Имя, Новый ОписаниеТипов("СписокЗначений"), , , Истина);
	
	Массив = Новый Массив;
	Массив.Добавить(РеквизитСписка);
	
	Форма.ИзменитьРеквизиты(Массив);
	
	Форма[Имя].ТипЗначения = Метаданные.Справочники.к2КнопкиУчетныхТочек.ТабличныеЧасти[Имя].Реквизиты.Значение.Тип;
	
	Поле = к2ИзменениеФорм.ПолеВвода(Форма, Имя, Имя, Родитель, ВставитьПередЭлементом);
	Поле.УстановитьДействие("ПриИзменении", "Подключаемый_СписокТабличнойЧастиПриИзменении");
	
	Возврат Поле;
	
КонецФункции

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// См. Справочник.к2КнопкиУчетныхТочек.Формы.ФормаЭлемента.ПриСозданииНаСервере()
//
Процедура ПриСозданииНаСервере_КУТ(Форма, Отказ, СтандартнаяОбработка, ДополнительныеПараметры = Неопределено) Экспорт
	
	Если к2ИзменениеФорм.ЕстьОбъектСсылкаНаФорме(Форма) И Не ЗначениеЗаполнено(Форма.Объект.Ссылка) Тогда 
		ПриЧтенииСоздании_КУТ(Форма);
	КонецЕсли;
	
КонецПроцедуры

// См. Справочник.к2КнопкиУчетныхТочек.Формы.ФормаЭлемента.ПриЧтенииНаСервере()
//
Процедура ПриЧтенииНаСервере_КУТ(Форма, ТекущийОбъект) Экспорт
	
	ПриЧтенииСоздании_КУТ(Форма);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ИменаЗаполняемыхРеквизитовФормы(Параметры, Форма)
	
	Реквизиты = Форма.ПолучитьРеквизиты();
	
	Возврат ЗаполняемыеСвойства(Параметры, Реквизиты);
	
КонецФункции

Функция ИменаЗаполняемыхРеквизитовОбъектаФормы(Параметры, Форма)
	
	Реквизиты = Форма.РеквизитФормыВЗначение("Объект").Метаданные().Реквизиты;
	
	Возврат ЗаполняемыеСвойства(Параметры, Реквизиты);
	
КонецФункции

Функция ЗаполняемыеСвойства(Параметры, Реквизиты)
	
	ИменСвойств = Новый Массив;
	
	Для Каждого Реквизит Из Реквизиты Цикл
		
		Если Параметры.Свойство(Реквизит.Имя) Тогда
			ИменСвойств.Добавить(Реквизит.Имя);
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат СтрСоединить(ИменСвойств, ",");
	
КонецФункции


Функция КлючиПараметровОбработки(ПараметрыОбработки, ДополнительныеКлючи)
	
	Ключи = "ДатаСмены, Смена, УчетнаяТочка, КнопкаУчетнойТочки, Ответственный";
	
	Если Не ПараметрыОбработки.Количество() = 0 Тогда 
		Ключи = Ключи + ", " + СтрСоединить(ПараметрыОбработки, ", ");
	КонецЕсли;
	
	Если Не ПустаяСтрока(ДополнительныеКлючи) Тогда 
		Ключи = Ключи + ", " + ДополнительныеКлючи;
	КонецЕсли;
	
	Возврат Ключи;
	
КонецФункции

Процедура ПриЧтенииСоздании_КУТ(Форма)
	
	Если к2ИзменениеФорм.ИзмененаПрограммно(Форма) Тогда 
		Возврат;
	КонецЕсли;
	
	к2УчетныеТочкиПереопределяемый.ПриЧтенииСоздании_КУТ(Форма);
	
КонецПроцедуры

#КонецОбласти