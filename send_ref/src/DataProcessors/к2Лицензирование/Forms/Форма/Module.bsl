
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновитьТекущееСостояниеЛицензирования();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ФайлЛицензииНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	Диалог                    = Новый ДиалогВыбораФайла( РежимДиалогаВыбораФайла.Открытие );
	Диалог.Заголовок          = НСтр( "ru = 'Укажите файл лицензирования'" );
	Диалог.Фильтр             = "Файлы настроек k2fresh (*.xml)|*.xml|XML файлы (*.xml)|*.xml";
	Диалог.Расширение         = "xml";
	Диалог.ПолноеИмяФайла     = ФайлЛицензии;
	Диалог.МножественныйВыбор = Ложь;
	
	оповещение = Новый ОписаниеОповещения( "ФайлЛицензииНачалоВыбораЗавершение", ЭтотОбъект );
	
	ФайловаяСистемаКлиент.ПоказатьДиалогВыбора( оповещение, Диалог);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПрочитатьФайлЛицензии( Команда )
	
	оповещение = Новый ОписаниеОповещения( "ДиалогВыбораФайлаЗавершение", ЭтотОбъект );
	
	ПараметрыЗагрузки = ФайловаяСистемаКлиент.ПараметрыЗагрузкиФайла();
	
	ПараметрыЗагрузки.Диалог.Заголовок   = НСтр( "ru = 'Укажите файл лицензирования'" );
	ПараметрыЗагрузки.Диалог.Фильтр      = НСтр( "ru='Файлы настроек k2fresh (*.xml)|*.xml|XML файлы (*.xml)|*.xml'" );
	ПараметрыЗагрузки.Диалог.Расширение  = "xml";
	ПараметрыЗагрузки.Интерактивно       = Не ЗначениеЗаполнено( ФайлЛицензии );
	ПараметрыЗагрузки.ИдентификаторФормы = УникальныйИдентификатор;
	
	ФайловаяСистемаКлиент.ЗагрузитьФайл( оповещение, ПараметрыЗагрузки, ФайлЛицензии );
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеРеквизитовФормы

&НаСервере
Процедура ОбновитьРеквизитыЛицензирования()
	
	СтруктураРеквизитов = к2ЛицензированиеВызовСервераПолныеПрава.ПустаяСтруктураНастроекЛицензирования();
	к2ЛицензированиеВызовСервераПолныеПрава.УдалитьСлужебныеРеквизитыИзСтруктурыНастроек(СтруктураРеквизитов);
	
	РеквизитыВерхнегоУровня = ЭтотОбъект.ПолучитьРеквизиты();
	
	ДобавляемыеРеквизиты = Новый Массив();
	
	Для Каждого ЭлементСтруктуры Из СтруктураРеквизитов Цикл 
		
		ИмяДобавляемогоРеквизита = ЭлементСтруктуры.Ключ;
		
		РеквизитСуществует = Ложь;
		
		Для Каждого Реквизит Из РеквизитыВерхнегоУровня Цикл
			Если Реквизит.Имя = ИмяДобавляемогоРеквизита Тогда
				РеквизитСуществует = Истина;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		
		Если НЕ РеквизитСуществует Тогда
			
			Реквизит = Новый РеквизитФормы(ИмяДобавляемогоРеквизита, Новый ОписаниеТипов("Булево"));
			ДобавляемыеРеквизиты.Добавить(Реквизит);
			
		КонецЕсли;
	
	КонецЦикла;
	
	Если ДобавляемыеРеквизиты.Количество() > 0 Тогда
		ЭтотОбъект.ИзменитьРеквизиты(ДобавляемыеРеквизиты);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьЭлементыЛицензирования()
	
	УдаляемыеЭлементы = Новый Массив();
	
	Для Каждого ТекЭлемент Из Элементы.ГруппаТекущееСостояние.ПодчиненныеЭлементы Цикл
		
		УдаляемыеЭлементы.Добавить( ТекЭлемент );
		
	КонецЦикла;
	
	Для Каждого УдаляемыйЭлемент Из УдаляемыеЭлементы Цикл
		
		ЭтотОбъект.Элементы.Удалить( УдаляемыйЭлемент );
		
	КонецЦикла;
	
	соответствиеЗаголовков = к2ЛицензированиеВызовСервераПолныеПрава.СоответствиеЗаголовковКлючамПустойСтруктурыНастроек();
	
	Для Каждого цЭлемент Из к2ЛицензированиеВызовСервераПолныеПрава.ПорядокЭлементов() Цикл
		
		ЗаголовокРеквизита = соответствиеЗаголовков.Получить( цЭлемент );
		
		Если ЗаголовокРеквизита = Неопределено Тогда
			
			Продолжить;
			
		КонецЕсли;
		
		НовоеПоле = к2ИзменениеФорм.ПолеФлажка(ЭтотОбъект, цЭлемент, цЭлемент, элементы.ГруппаТекущееСостояние);
		НовоеПоле.Заголовок = ЗаголовокРеквизита;
		НовоеПоле.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Право;
		НовоеПоле.ТолькоПросмотр     = Истина;
		НовоеПоле.ШрифтЗаголовка     = Новый Шрифт( НовоеПоле.ШрифтЗаголовка, , , ЭтотОбъект[цЭлемент] );
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиенте
Процедура ФайлЛицензииНачалоВыбораЗавершение( ВыбранныеФайлы, ДополнительныеПараметры ) Экспорт
	
	Если Не ВыбранныеФайлы = Неопределено Тогда
		
		ФайлЛицензии = ВыбранныеФайлы[0];
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДиалогВыбораФайлаЗавершение( Результат, Контекст ) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЗагрузитьФайлЛицензииНаСервере(Результат.Хранение);
	
	ОбщегоНазначенияКлиент.ОбновитьИнтерфейсПрограммы();
	ОбновитьПовторноИспользуемыеЗначения();
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьФайлЛицензииНаСервере( Знач пАдресФайла )
	
	к2ЛицензированиеВызовСервераПолныеПрава.ЗагрузитьФайлЛицензирования( пАдресФайла );
	ОбновитьПовторноИспользуемыеЗначения();
	ОбновитьТекущееСостояниеЛицензирования();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьТекущееСостояниеЛицензирования()
	
	ОбновитьРеквизитыЛицензирования();
	
	ПрочитатьТекущиеНастройки();
	
	ОбновитьЭлементыЛицензирования();
	
КонецПроцедуры

&НаСервере
Процедура ПрочитатьТекущиеНастройки()
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, РегистрыСведений.к2НастройкиЛицензирования.СтруктураТекущихНастроек());
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти


