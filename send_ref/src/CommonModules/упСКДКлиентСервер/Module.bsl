
Процедура ИнициализироватьКомпоновщикНастроек(КомпоновщикНастроек, АдресСхемыКомпоновкиДанных) Экспорт
	
	СхемаКомпоновкиДанных = ПолучитьИзВременногоХранилища(АдресСхемыКомпоновкиДанных);
	КомпоновщикНастроек.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(АдресСхемыКомпоновкиДанных));
	
	КомпоновщикНастроек.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	КомпоновщикНастроек.Восстановить();
	
КонецПроцедуры


Процедура ПоместитьНастройкиВСхемуКомпоновкиДанных(КомпоновщикНастроек, АдресСхемыКомпоновкиДанных) Экспорт

	КомпоновщикНастроек.Восстановить();
	
	СхемаКомпоновкиДанных = ПолучитьИзВременногоХранилища(АдресСхемыКомпоновкиДанных);

	ОчиститьНастройкиКомпоновкиДанных(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	СкопироватьНастройкиКомпоновкиДанных(СхемаКомпоновкиДанных.НастройкиПоУмолчанию, КомпоновщикНастроек.ПолучитьНастройки());

	ПоместитьВоВременноеХранилище(СхемаКомпоновкиДанных, АдресСхемыКомпоновкиДанных);

КонецПроцедуры

 // Процедура удаляет все элементы настройки компоновки данных из объекта
//
Процедура ОчиститьНастройкиКомпоновкиДанных(Настройки) Экспорт
	
	Если Настройки = Неопределено ИЛИ ТипЗнч(Настройки) <> Тип("НастройкиКомпоновкиДанных") Тогда
		Возврат;
	КонецЕсли;
	
	Для каждого Параметр Из Настройки.ПараметрыДанных.Элементы Цикл
		Параметр.Значение = Неопределено;
		Параметр.Использование = ложь;
	КонецЦикла;
	
	Для каждого Параметр Из Настройки.ПараметрыВывода.Элементы Цикл
		Параметр.Использование = ложь;
	КонецЦикла;
	
	Настройки.ПользовательскиеПоля.Элементы.Очистить();
	Настройки.Отбор.Элементы.Очистить();
	Настройки.Порядок.Элементы.Очистить();
	Настройки.Выбор.Элементы.Очистить();
	Настройки.Структура.Очистить();
	
КонецПроцедуры

// Копирует настройки компоновки данных
//
Процедура СкопироватьНастройкиКомпоновкиДанных(НастройкиПриемник, НастройкиИсточник) Экспорт
	
	Если НастройкиИсточник = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(НастройкиПриемник) = Тип("НастройкиКомпоновкиДанных") Тогда
		Для каждого Параметр Из НастройкиИсточник.ПараметрыДанных.Элементы Цикл
			ЗначениеПараметра = НастройкиПриемник.ПараметрыДанных.НайтиЗначениеПараметра(Параметр.Параметр);
			Если ЗначениеПараметра <> Неопределено Тогда
				ЗаполнитьЗначенияСвойств(ЗначениеПараметра, Параметр);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Если ТипЗнч(НастройкиИсточник) = Тип("НастройкиВложенногоОбъектаКомпоновкиДанных") Тогда
		ЗаполнитьЗначенияСвойств(НастройкиПриемник, НастройкиИсточник);
		СкопироватьНастройкиКомпоновкиДанных(НастройкиПриемник.Настройки, НастройкиИсточник.Настройки);
		Возврат;
	КонецЕсли;
	
	// Копирование настроек
	Если ТипЗнч(НастройкиИсточник) = Тип("НастройкиКомпоновкиДанных") Тогда
		
		ЗаполнитьЭлементы(НастройкиПриемник.ПараметрыДанных, НастройкиИсточник.ПараметрыДанных);
		СкопироватьЭлементы(НастройкиПриемник.ПользовательскиеПоля, НастройкиИсточник.ПользовательскиеПоля);
		СкопироватьЭлементы(НастройкиПриемник.Отбор,         НастройкиИсточник.Отбор);
		СкопироватьЭлементы(НастройкиПриемник.Порядок,       НастройкиИсточник.Порядок);
		
	КонецЕсли;
	
	Если ТипЗнч(НастройкиИсточник) = Тип("ГруппировкаКомпоновкиДанных")
	 ИЛИ ТипЗнч(НастройкиИсточник) = Тип("ГруппировкаТаблицыКомпоновкиДанных")
	 ИЛИ ТипЗнч(НастройкиИсточник) = Тип("ГруппировкаДиаграммыКомпоновкиДанных") Тогда
		
		СкопироватьЭлементы(НастройкиПриемник.ПоляГруппировки, НастройкиИсточник.ПоляГруппировки);
		СкопироватьЭлементы(НастройкиПриемник.Отбор,           НастройкиИсточник.Отбор);
		СкопироватьЭлементы(НастройкиПриемник.Порядок,         НастройкиИсточник.Порядок);
		ЗаполнитьЗначенияСвойств(НастройкиПриемник, НастройкиИсточник);
		
	КонецЕсли;
	
	СкопироватьЭлементы(НастройкиПриемник.Выбор,              НастройкиИсточник.Выбор);
	СкопироватьЭлементы(НастройкиПриемник.УсловноеОформление, НастройкиИсточник.УсловноеОформление);
	ЗаполнитьЭлементы(НастройкиПриемник.ПараметрыВывода,      НастройкиИсточник.ПараметрыВывода);
	
	// Копирование структуры
	Если ТипЗнч(НастройкиИсточник) = Тип("НастройкиКомпоновкиДанных")
	 ИЛИ ТипЗнч(НастройкиИсточник) = Тип("ГруппировкаКомпоновкиДанных") Тогда
		
		Для каждого ЭлементСтруктурыИсточник Из НастройкиИсточник.Структура Цикл
			ЭлементСтруктурыПриемник = НастройкиПриемник.Структура.Добавить(ТипЗнч(ЭлементСтруктурыИсточник));
			СкопироватьНастройкиКомпоновкиДанных(ЭлементСтруктурыПриемник, ЭлементСтруктурыИсточник);
		КонецЦикла;
		
	КонецЕсли;
	
	Если ТипЗнч(НастройкиИсточник) = Тип("ГруппировкаТаблицыКомпоновкиДанных")
	 ИЛИ ТипЗнч(НастройкиИсточник) = Тип("ГруппировкаДиаграммыКомпоновкиДанных") Тогда
		
		Для каждого ЭлементСтруктурыИсточник Из НастройкиИсточник.Структура Цикл
			ЭлементСтруктурыПриемник = НастройкиПриемник.Структура.Добавить();
			СкопироватьНастройкиКомпоновкиДанных(ЭлементСтруктурыПриемник, ЭлементСтруктурыИсточник);
		КонецЦикла;
		
	КонецЕсли;
	
	Если ТипЗнч(НастройкиИсточник) = Тип("ТаблицаКомпоновкиДанных") Тогда
		
		Для каждого ЭлементСтруктурыИсточник Из НастройкиИсточник.Строки Цикл
			ЭлементСтруктурыПриемник = НастройкиПриемник.Строки.Добавить();
			СкопироватьНастройкиКомпоновкиДанных(ЭлементСтруктурыПриемник, ЭлементСтруктурыИсточник);
		КонецЦикла;
		
		Для каждого ЭлементСтруктурыИсточник Из НастройкиИсточник.Колонки Цикл
			ЭлементСтруктурыПриемник = НастройкиПриемник.Колонки.Добавить();
			СкопироватьНастройкиКомпоновкиДанных(ЭлементСтруктурыПриемник, ЭлементСтруктурыИсточник);
		КонецЦикла;
		
	КонецЕсли;
	
	Если ТипЗнч(НастройкиИсточник) = Тип("ДиаграммаКомпоновкиДанных") Тогда
		
		Для каждого ЭлементСтруктурыИсточник Из НастройкиИсточник.Серии Цикл
			ЭлементСтруктурыПриемник = НастройкиПриемник.Серии.Добавить();
			СкопироватьНастройкиКомпоновкиДанных(ЭлементСтруктурыПриемник, ЭлементСтруктурыИсточник);
		КонецЦикла;
		
		Для каждого ЭлементСтруктурыИсточник Из НастройкиИсточник.Точки Цикл
			ЭлементСтруктурыПриемник = НастройкиПриемник.Точки.Добавить();
			СкопироватьНастройкиКомпоновкиДанных(ЭлементСтруктурыПриемник, ЭлементСтруктурыИсточник);
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры


// Копирует элементы из одной коллекции в другую
Процедура СкопироватьЭлементы(ПриемникЗначения, ИсточникЗначения, ПроверятьДоступность = Ложь, ОчищатьПриемник = Истина) Экспорт
	
	Если ТипЗнч(ИсточникЗначения) = Тип("УсловноеОформлениеКомпоновкиДанных")
		ИЛИ ТипЗнч(ИсточникЗначения) = Тип("ВариантыПользовательскогоПоляВыборКомпоновкиДанных")
		ИЛИ ТипЗнч(ИсточникЗначения) = Тип("ОформляемыеПоляКомпоновкиДанных")
		ИЛИ ТипЗнч(ИсточникЗначения) = Тип("ЗначенияПараметровДанныхКомпоновкиДанных") Тогда
		СоздаватьПоТипу = Ложь;
	Иначе
		СоздаватьПоТипу = Истина;
	КонецЕсли;
	ПриемникЭлементов = ПриемникЗначения.Элементы;
	ИсточникЭлементов = ИсточникЗначения.Элементы;
	Если ОчищатьПриемник Тогда
		ПриемникЭлементов.Очистить();
	КонецЕсли;
	
	Для каждого ЭлементИсточник Из ИсточникЭлементов Цикл
		
		Если ТипЗнч(ЭлементИсточник) = Тип("ЭлементПорядкаКомпоновкиДанных") Тогда
			// Элементы порядка добавляем в начало
			Индекс = ИсточникЭлементов.Индекс(ЭлементИсточник);
			ЭлементПриемник = ПриемникЭлементов.Вставить(Индекс, ТипЗнч(ЭлементИсточник));
		Иначе
			Если СоздаватьПоТипу Тогда
				ЭлементПриемник = ПриемникЭлементов.Добавить(ТипЗнч(ЭлементИсточник));
			Иначе
				ЭлементПриемник = ПриемникЭлементов.Добавить();
			КонецЕсли;
		КонецЕсли;
		
		ЗаполнитьЗначенияСвойств(ЭлементПриемник, ЭлементИсточник);
		// В некоторых коллекциях необходимо заполнить другие коллекции
		Если ТипЗнч(ИсточникЭлементов) = Тип("КоллекцияЭлементовУсловногоОформленияКомпоновкиДанных") Тогда
			СкопироватьЭлементы(ЭлементПриемник.Поля, ЭлементИсточник.Поля);
			СкопироватьЭлементы(ЭлементПриемник.Отбор, ЭлементИсточник.Отбор);
			ЗаполнитьЭлементы(ЭлементПриемник.Оформление, ЭлементИсточник.Оформление); 
		ИначеЕсли ТипЗнч(ИсточникЭлементов)	= Тип("КоллекцияВариантовПользовательскогоПоляВыборКомпоновкиДанных") Тогда
			СкопироватьЭлементы(ЭлементПриемник.Отбор, ЭлементИсточник.Отбор);
		КонецЕсли;
		
		// В некоторых элементах коллекции необходимо заполнить другие коллекции
		Если ТипЗнч(ЭлементИсточник) = Тип("ГруппаЭлементовОтбораКомпоновкиДанных") Тогда
			СкопироватьЭлементы(ЭлементПриемник, ЭлементИсточник);
		ИначеЕсли ТипЗнч(ЭлементИсточник) = Тип("ГруппаВыбранныхПолейКомпоновкиДанных") Тогда
			СкопироватьЭлементы(ЭлементПриемник, ЭлементИсточник);
		ИначеЕсли ТипЗнч(ЭлементИсточник) = Тип("ПользовательскоеПолеВыборКомпоновкиДанных") Тогда
			СкопироватьЭлементы(ЭлементПриемник.Варианты, ЭлементИсточник.Варианты);
		ИначеЕсли ТипЗнч(ЭлементИсточник) = Тип("ПользовательскоеПолеВыражениеКомпоновкиДанных") Тогда
			ЭлементПриемник.УстановитьВыражениеДетальныхЗаписей (ЭлементИсточник.ПолучитьВыражениеДетальныхЗаписей());
			ЭлементПриемник.УстановитьВыражениеИтоговыхЗаписей(ЭлементИсточник.ПолучитьВыражениеИтоговыхЗаписей());
			ЭлементПриемник.УстановитьПредставлениеВыраженияДетальныхЗаписей(ЭлементИсточник.ПолучитьПредставлениеВыраженияДетальныхЗаписей ());
			ЭлементПриемник.УстановитьПредставлениеВыраженияИтоговыхЗаписей(ЭлементИсточник.ПолучитьПредставлениеВыраженияИтоговыхЗаписей ());
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьЭлементы(ПриемникЗначения, ИсточникЗначения, ПервыйУровень = Неопределено) Экспорт
	
	Если ТипЗнч(ПриемникЗначения) = Тип("КоллекцияЗначенийПараметровКомпоновкиДанных") Тогда
		КоллекцияЗначений = ИсточникЗначения;
	Иначе
		КоллекцияЗначений = ИсточникЗначения.Элементы;
	КонецЕсли;
	
	Для каждого ЭлементИсточник Из КоллекцияЗначений Цикл
		Если ПервыйУровень = Неопределено Тогда
			ЭлементПриемник = ПриемникЗначения.НайтиЗначениеПараметра(ЭлементИсточник.Параметр);
		Иначе
			ЭлементПриемник = ПервыйУровень.НайтиЗначениеПараметра(ЭлементИсточник.Параметр);
		КонецЕсли;
		Если ЭлементПриемник = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		ЗаполнитьЗначенияСвойств(ЭлементПриемник, ЭлементИсточник);
		Если ТипЗнч(ЭлементИсточник) = Тип("ЗначениеПараметраКомпоновкиДанных") Тогда
			Если ЭлементИсточник.ЗначенияВложенныхПараметров.Количество() <> 0 Тогда
				ЗаполнитьЭлементы(ЭлементПриемник.ЗначенияВложенныхПараметров, ЭлементИсточник.ЗначенияВложенныхПараметров, ПриемникЗначения);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры


Функция УстановитьПараметр(Настройки, Параметр, Значение, Использование = истина) Экспорт
	
	ЗначениеПараметра = Неопределено;
	ПолеПараметр = ?(ТипЗнч(Параметр) = Тип("Строка"), Новый ПараметрКомпоновкиДанных(Параметр), Параметр);
	
	Если ТипЗнч(Настройки) = Тип("НастройкиКомпоновкиДанных") Тогда
		ЗначениеПараметра = Настройки.ПараметрыДанных.НайтиЗначениеПараметра(ПолеПараметр);
	ИначеЕсли ТипЗнч(Настройки) = Тип("ПользовательскиеНастройкиКомпоновкиДанных") Тогда
		Для каждого ЭлементНастройки Из Настройки.Элементы Цикл
			Если ТипЗнч(ЭлементНастройки) = Тип("ЗначениеПараметраНастроекКомпоновкиДанных") И ЭлементНастройки.Параметр = ПолеПараметр Тогда
				ЗначениеПараметра = ЭлементНастройки;
				Прервать;
			КонецЕсли;
		КонецЦикла;
	ИначеЕсли ТипЗнч(Настройки) = Тип("КомпоновщикНастроекКомпоновкиДанных") Тогда
		Для каждого ЭлементНастройки Из Настройки.ПользовательскиеНастройки.Элементы Цикл
			Если ТипЗнч(ЭлементНастройки) = Тип("ЗначениеПараметраНастроекКомпоновкиДанных") И ЭлементНастройки.Параметр = ПолеПараметр Тогда
				ЗначениеПараметра = ЭлементНастройки;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		Если ЗначениеПараметра = Неопределено Тогда
			ЗначениеПараметра = Настройки.Настройки.ПараметрыДанных.НайтиЗначениеПараметра(ПолеПараметр);
		КонецЕсли;
	КонецЕсли;
	
	ЗначениеПараметра.Значение = Значение;
	ЗначениеПараметра.Использование = Использование;
	
	Возврат ЗначениеПараметра;
КонецФункции

Функция СоздатьСКДПоОбъекту( пОбъект ) Экспорт
	
	СтруктураРеквизитов = упСтруктурыПроцессов.СоздатьСтруктуруРеквизитовПоОбъекту( пОбъект );
	
	Возврат СоздатьСКДПоСтруктуреРеквизитов( СтруктураРеквизитов );
	
КонецФункции	//СоздатьСКДПоОбъекту


Функция СоздатьСКДПоВерсии( пВерсияБП ) Экспорт
	
	Возврат СоздатьСКДПоОбъекту( пВерсияБП );
	
КонецФункции	//СоздатьСКДПоВерсии


Функция СоздатьСКДПоСтруктуреРеквизитов( пСтруктураРеквизитов )
	
	СКД = Новый СхемаКомпоновкиДанных;
	ИД = ДобавитьИсточникДанных( СКД );
	НаборДанных = ДобавитьНаборДанных( СКД , ИД.Имя );
	
	ДобавитьВНаборСпецПолеДляУсловия( НаборДанных );
	
	Если пСтруктураРеквизитов.Свойство( упИК.РеквизитЗадача() )
		И пСтруктураРеквизитов.Количество() = 2 Тогда
		
		// Если кроме реквизита "Задача" (и индекса картинки) больше ничего не передается, то это условие по неизвестному
		// бизнес-процессу. Добавим для задачи характеристики-реквизиты.
		
		НаборДанныхДляЗадачи = ДобавитьНаборДанныхДляЗадачи( СКД , ИД.Имя );
		
		новСвязьНаборовДанных = СКД.СвязиНаборовДанных.Добавить();
		
		новСвязьНаборовДанных.НаборДанныхИсточник = НаборДанных.Имя;
		новСвязьНаборовДанных.НаборДанныхПриемник = НаборДанныхДляЗадачи.Имя;
		
		новСвязьНаборовДанных.ВыражениеИсточник = упИК.РеквизитЗадача();
		новСвязьНаборовДанных.ВыражениеПриемник = упИК.РеквизитЗадача();
		
		новСвязьНаборовДанных.Обязательная = Истина;
		
	КонецЕсли;
	
	ЗаполнитьНаборДанныхПоСтруктуреРеквизитов( НаборДанных , пСтруктураРеквизитов );
	
	ЗаполнитьНастройкиДляУсловия( СКД );
	
	Возврат СКД;
	
КонецФункции	//СоздатьСКДПоСтруктуреРеквизитов



Функция ДобавитьИсточникДанных( пСКД , пИмя = "ИсточникДанных" , пТипИсточникаДанных = "Local" )
	
	ИД = пСКД.ИсточникиДанных.Добавить();
	ИД.Имя = пИмя;
	ИД.ТипИсточникаДанных = пТипИсточникаДанных;
	
	Возврат ИД;
	
КонецФункции	//ДобавитьИсточникДанных

Функция ДобавитьНаборДанных( пСКД , пИмяИсточникаДанных )
	
	НаборДанных = пСКД.НаборыДанных.Добавить(Тип("НаборДанныхОбъектСхемыКомпоновкиДанных"));
	НаборДанных.Имя = упИК.ИмяВнешнегоИсточникаДанных_ТаблицаРеквизитов();
	НаборДанных.ИмяОбъекта = упИК.ИмяВнешнегоИсточникаДанных_ТаблицаРеквизитов();
	НаборДанных.ИсточникДанных = пИмяИсточникаДанных;
	
	Возврат НаборДанных;
	
КонецФункции	//ДобавитьНаборДанных


Функция ДобавитьНаборДанныхДляЗадачи( пСКД , пИмяИсточникаДанных )
	
	текстЗапроса = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	упЗадача.Ссылка КАК СинонимЗадачи
	|ИЗ
	|	Задача.упЗадача КАК упЗадача
	|{ХАРАКТЕРИСТИКИ
	|	ТИП(Задача.упЗадача)
	|	ВИДЫХАРАКТЕРИСТИК (ВЫБРАТЬ
	|			упРеквизиты.Ссылка КАК Ключ,
	|			""["" + упРеквизиты.ИмяДляСписка + ""]"" КАК ИмяДляСписка,
	|			упРеквизиты.ТипРеквизита.ТипЗначения КАК ТипЗначения
	|		ИЗ
	|			Справочник.упРеквизиты КАК упРеквизиты
	|		ГДЕ
	|			упРеквизиты.ПометкаУдаления = ЛОЖЬ
	|			И упРеквизиты.ЭтоГруппа = ЛОЖЬ
	|			И упРеквизиты.Предопределенный = ЛОЖЬ
	|			И упРеквизиты.ЭтоТЧ = ЛОЖЬ
	|			И НЕ упРеквизиты.ИмяДляСписка = """")
	|	ПОЛЕКЛЮЧА Ключ
	|	ПОЛЕИМЕНИ ИмяДляСписка
	|	ПОЛЕТИПАЗНАЧЕНИЯ ТипЗначения
	|	ЗНАЧЕНИЯХАРАКТЕРИСТИК (ВЫБРАТЬ
	|			упЗадачаРеквизитыСессии.Ссылка КАК Задача,
	|			упЗадачаРеквизитыСессии.СсылкаНаРеквизит,
	|			упЗадачаРеквизитыСессии.Значение
	|		ИЗ
	|			Задача.упЗадача.РеквизитыСессии КАК упЗадачаРеквизитыСессии)
	|	ПОЛЕОБЪЕКТА Задача
	|	ПОЛЕВИДА СсылкаНаРеквизит
	|	ПОЛЕЗНАЧЕНИЯ Значение }";
	
	текстЗапроса = СтрЗаменить( текстЗапроса, "СинонимЗадачи", упИК.РеквизитЗадача() );
	
	НаборДанных = пСКД.НаборыДанных.Добавить(Тип("НаборДанныхЗапросСхемыКомпоновкиДанных"));
	НаборДанных.Имя            = упИК.ИмяНабораДанных_Задача();
	НаборДанных.Запрос         = текстЗапроса;
	НаборДанных.ИсточникДанных = пИмяИсточникаДанных;
	
	Возврат НаборДанных;
	
КонецФункции


Функция ДобавитьВНаборСпецПолеДляУсловия( пНаборДанных )
	
	ПолеНабораДанных = пНаборДанных.Поля.Добавить( Тип( "ПолеНабораДанныхСхемыКомпоновкиДанных" ) );
	
	ПолеНабораДанных.Поле = упИК.ПредопределенныйРеквизитДляУсловия();
	ПолеНабораДанных.ПутьКДанным = упИК.ПредопределенныйРеквизитДляУсловия();
	ПолеНабораДанных.ТипЗначения = Новый ОписаниеТипов("Булево");
	
	ПолеНабораДанных.ОграничениеИспользования.Условие = Истина;
	
	Возврат ПолеНабораДанных;
	
КонецФункции	//ДобавитьВНаборСпецПолеДляУсловия


Процедура ЗаполнитьНаборДанныхПоСтруктуреРеквизитов( пНаборДанных , пСтруктураРеквизитов )
	
	Если пСтруктураРеквизитов.Свойство( упИК.РеквизитЗадача() ) Тогда
		
		ДобавитьПолеНабораПоРеквизитуБП( пНаборДанных , Справочники.упРеквизиты.Задача , упИК.РеквизитЗадача() , Новый ОписаниеТипов("ЗадачаСсылка.упЗадача") );
		
	КонецЕсли;
	
	Для Каждого цЭлемент Из упСтруктурыПроцессов.СтруктураРеквизитовБП( пСтруктураРеквизитов ) Цикл
		
		Если Справочники.упРеквизиты.ЭтоТЧ( цЭлемент.Значение[упИК.СсылкаНаРеквизит()] ) Тогда
			Продолжить;
		КонецЕсли;
		
		ДобавитьПолеНабораПоРеквизитуБП( пНаборДанных , цЭлемент.Значение[упИК.СсылкаНаРеквизит()] , цЭлемент.Ключ );
		
	КонецЦикла;
	
КонецПроцедуры	//ЗаполнитьНаборДанныхПоСтруктуреРеквизитов

Функция ДобавитьПолеНабораПоРеквизитуБП( пНаборДанных , пРеквизит , пИмя = "" , пТипЗначения = Неопределено )
	
	Если пИмя = "" Тогда
		
		пИмя = глРеквизит_Кэш( пРеквизит , "Имя" );
		
	КонецЕсли;
	
	Если пТипЗначения = Неопределено Тогда
		
		пТипЗначения = Справочники.упРеквизиты.ТипЗначения( пРеквизит );
		
	КонецЕсли;
	
	
	ПолеНабораДанных = пНаборДанных.Поля.Добавить( Тип( "ПолеНабораДанныхСхемыКомпоновкиДанных" ) );
	ПолеНабораДанных.Поле = пИмя;
	ПолеНабораДанных.ПутьКДанным = пИмя;
	ПолеНабораДанных.Заголовок = глРеквизит_Кэш( пРеквизит , "Наименование" );
	ПолеНабораДанных.ТипЗначения = пТипЗначения;
	
	ПолеНабораДанных.ОграничениеИспользованияРеквизитов.Поле = Ложь;
	
	ПолеНабораДанных.ПараметрыРедактирования.УстановитьЗначениеПараметра( "Маска" , глРеквизит_Кэш( пРеквизит , "Маска" ) );
	
	Если ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ЭтоДополнительныеЗначения( пТипЗначения ) Тогда
		
		ПараметрыВыбораКД = ПолеНабораДанных.ПараметрыРедактирования.НайтиЗначениеПараметра( Новый ПараметрКомпоновкиДанных ("ПараметрыВыбора") );
		
		Если Не ПараметрыВыбораКД = Неопределено Тогда
			
			новПараметрВыбора = ПараметрыВыбораКД.Значение.Добавить();
			
			новПараметрВыбора.Имя = "Отбор.Владелец";
			новПараметрВыбора.Значение = глРеквизит_Кэш( пРеквизит , "ТипРеквизита" );
			
			ПараметрыВыбораКД.Использование = Истина;
			
		КонецЕсли;
		
	КонецЕсли;
	
	ПолеНабораДанных.Оформление.УстановитьЗначениеПараметра( "Формат" , глРеквизит_Кэш( пРеквизит , "Формат" ) );
	
КонецФункции	//ДобавитьПолеНабораПоРеквизитуБП


Процедура ЗаполнитьНастройкиДляУсловия( пСКД )
	
	Настройки = пСКД.НастройкиПоУмолчанию;
	
	ГруппировкаДетальныеЗаписи = Настройки.Структура.Добавить(Тип("ГруппировкаКомпоновкиДанных"));
	ГруппировкаДетальныеЗаписи.Использование = Истина;
	
	ВыбранныеПоляДляНоменклатуры = ГруппировкаДетальныеЗаписи.Выбор.Элементы.Добавить(Тип("АвтоВыбранноеПолеКомпоновкиДанных"));
	ВыбранныеПоляДляНоменклатуры.Использование = Истина;
	
	ПредопредПолеВыбора = Настройки.Выбор.Элементы.Добавить(Тип("ВыбранноеПолеКомпоновкиДанных"));
	ПредопредПолеВыбора.Использование = Истина;
	ПредопредПолеВыбора.Поле = Новый ПолеКомпоновкиДанных( упИК.ПредопределенныйРеквизитДляУсловия() );
	
КонецПроцедуры	//ЗаполнитьНастройкиДляУсловия



Процедура БезопасноСкопироватьНастройки( пСКД, пНастройкиИсточник, пНастройкиПриемник ) Экспорт
	
	КомпоновщикНастроек = Новый КомпоновщикНастроекКомпоновкиДанных;
	
	КомпоновщикНастроек.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных( пСКД ));
	КомпоновщикНастроек.ЗагрузитьНастройки( пНастройкиИсточник );
	
	КомпоновщикНастроек.Восстановить();
	
	ОчиститьНастройкиКомпоновкиДанных( пНастройкиПриемник );
	СкопироватьНастройкиКомпоновкиДанных( пНастройкиПриемник, КомпоновщикНастроек.ПолучитьНастройки() );
	
КонецПроцедуры



// Формирует и возвращает таблицу значений по СКД
// Параметры:
// СКД - схема компоновки данных,
// ВнешниеНаборыДанных - Тип: Структура. Ключ структуры соответствует имени внешнего набора данных. Значение структуры -
//                       внешнему набору данных.
//
Функция ТаблицаСКД( СКД , ВнешниеНаборыДанных = Неопределено, пНастройки = Неопределено ) Экспорт
	
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	
	Если пНастройки = Неопределено Тогда
		
		МакетКомпоновки = КомпоновщикМакета.Выполнить( СКД , СКД.НастройкиПоУмолчанию ,,, Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
		
	Иначе
		
		МакетКомпоновки = КомпоновщикМакета.Выполнить( СКД , пНастройки ,,, Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
		
	КонецЕсли;
	
	ПроцессорКомпоновкиДанных = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновкиДанных.Инициализировать( МакетКомпоновки , ВнешниеНаборыДанных );
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
	ТаблицаЗначений = Новый ТаблицаЗначений;
	ПроцессорВывода.УстановитьОбъект(ТаблицаЗначений);
	ПроцессорВывода.Вывести(ПроцессорКомпоновкиДанных);
	
	Возврат ТаблицаЗначений;
	
КонецФункции



Функция ПредставлениеОтбора( СКД ) Экспорт
	
	Возврат Строка( СКД.НастройкиПоУмолчанию.Отбор );
	
КонецФункции
