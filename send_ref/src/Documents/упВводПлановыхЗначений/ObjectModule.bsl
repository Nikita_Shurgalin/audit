#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ОбработкаПроведения(Отказ, Режим)
	
	Движения.упПлан.Записывать = Истина;
	Движения.упПлан.Загрузить(План.Выгрузить());
	
КонецПроцедуры

#КонецЕсли