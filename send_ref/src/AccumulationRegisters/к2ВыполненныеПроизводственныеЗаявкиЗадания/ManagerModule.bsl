#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.Таблица_к2ВыполненныеПроизводственныеЗаявкиЗадания;
	
	Если Отказ Или Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.к2ВыполненныеПроизводственныеЗаявкиЗадания.Записывать = Истина;
	Движения.к2ВыполненныеПроизводственныеЗаявкиЗадания.Загрузить(Таблица);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
