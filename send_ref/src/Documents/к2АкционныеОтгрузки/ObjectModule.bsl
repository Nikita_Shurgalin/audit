#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения( ДанныеЗаполнения, СтандартнаяОбработка )
	
	Если ТипЗнч( ДанныеЗаполнения ) = Тип( "ДокументСсылка.к2МаркетинговоеМероприятие" ) Тогда
		
		ЗаполнитьПоМаркетинговомуМероприятию( ДанныеЗаполнения );
		
	ИначеЕсли ТипЗнч( ДанныеЗаполнения ) = Тип( "ДокументСсылка.к2Маркетинг_КоммерческаяАктивность" ) Тогда
		
		ЗаполнитьПоКоммерческойАктивности( ДанныеЗаполнения );
		
	КонецЕсли;
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияХарактеристик();
	ПараметрыПроверки.Вставить("ИмяТЧ", "СводныеПоказатели");
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ, ПараметрыПроверки);
	
	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияХарактеристик();
	ПараметрыПроверки.Вставить("ИмяТЧ", "Показатели");
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ, ПараметрыПроверки);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	СпособСоздания = Перечисления.к2СпособыСозданияДокументов.СозданВручную;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	НоменклатураСервер.ЗаполнитьАналитикиУчетаПоНоменклатуре( Показатели );
	
	ЗаполнитьСредниеОтгрузкиПоДням();
	
	к2ПроведениеСервер.ПередЗаписью( ЭтотОбъект, РежимЗаписи, РежимПроведения );
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	к2ПроведениеСервер.ОбработкаПроведения( ЭтотОбъект, Отказ, РежимПроведения );
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	к2ПроведениеСервер.ОбработкаУдаленияПроведения( ЭтотОбъект, Отказ );
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ИнициализацияИЗаполнение

Процедура ЗаполнитьРеквизитыПоУмолчанию()
	
	глПрисвоитьЕслиНеЗаполнено( Ответственный, глТекущийПользователь() );
	глПрисвоитьЕслиНеЗаполнено( Дата, ТекущаяДатаСеанса() );
	глПрисвоитьЕслиНеЗаполнено( СтратегияВлиянияНаОтгрузки, Перечисления.к2СтратегииВлиянияМаркетинговыхМероприятийНаОтгрузки.СПриростами );
	глПрисвоитьЕслиНеЗаполнено( СпособСоздания, Перечисления.к2СпособыСозданияДокументов.СозданВручную );
	
КонецПроцедуры

Процедура ЗаполнитьПоМаркетинговомуМероприятию( МаркетинговоеМероприятие )
	
	Основание = МаркетинговоеМероприятие;
	
	реквизитыОснования = глРеквизиты( Основание, "СтратегияВлиянияНаОтгрузки,ФакторПродаж" );
	
	СтратегияВлиянияНаОтгрузки = реквизитыОснования.СтратегияВлиянияНаОтгрузки;
	ФакторПродаж               = реквизитыОснования.ФакторПродаж;
	
	Показатели.Очистить();
	СводныеПоказатели.Очистить();
	
	СтатусМероприятия = РегистрыСведений.к2СтатусыМаркетинговыхМероприятий.СтатусМаркетинговогоМероприятия( МаркетинговоеМероприятие );
	
	Если СтатусМероприятия = Перечисления.к2СтатусыМаркетинговыхМероприятий.Утверждена Тогда
		
		ЗаполнитьПоУтвержденномуМаркетинговомуМероприятию( МаркетинговоеМероприятие );
		
	ИначеЕсли СтатусМероприятия = Перечисления.к2СтатусыМаркетинговыхМероприятий.Завершена Тогда
		
		ЗаполнитьПоЗавершенномуМаркетинговомуМероприятию( МаркетинговоеМероприятие );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьПоУтвержденномуМаркетинговомуМероприятию(МаркетинговоеМероприятие)
		
	СтруктураДат = глРеквизиты(МаркетинговоеМероприятие, "ПлановаяДатаНачала КАК ДатаНачала, ПлановаяДатаОкончания КАК ДатаОкончания");
	
	Для Каждого Строка Из ТаблицаСводныхПоказателейПоМаркетинговомуМероприятию(МаркетинговоеМероприятие) Цикл
		
		НоваяСтрока = СводныеПоказатели.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
		НоваяСтрока.ДатаНачала = СтруктураДат.ДатаНачала;
		НоваяСтрока.ДатаОкончания = СтруктураДат.ДатаОкончания;
	
	КонецЦикла;
		
	МассивДнейПериода = к2ДатыКлиентСервер.МассивДнейПериода(СтруктураДат.ДатаНачала, СтруктураДат.ДатаОкончания);
		
	ТаблицаКоэффициентовДняПрофиля = ТаблицаКоэффициентовДняПрофиля(МаркетинговоеМероприятие.ПрофильАкции, МассивДнейПериода.Количество());
	
	Для Каждого Строка Из СводныеПоказатели Цикл
		
		СуммарныйВес = СуммарныйВесПериода(СтруктураДат.ДатаНачала, СтруктураДат.ДатаОкончания, Строка.Номенклатура, Строка.Контрагент);
		
		ТаблицаОбщихКоэффициентов = ПустаяТаблицаКоэффициентовДня();

		НомерДня = 1;
		СуммарныйОбъемОбщихКоэффициентов = 0;
		
		Для Каждого День Из МассивДнейПериода Цикл
			
			НайденныеСтроки = ТаблицаКоэффициентовДняПрофиля.НайтиСтроки(Новый Структура("НомерДня", НомерДня));
			
			НоваяСтрокаКоэффициентов = ТаблицаОбщихКоэффициентов.Добавить();
			НоваяСтрокаКоэффициентов.НомерДня = НомерДня;
			Если НайденныеСтроки.Количество() > 0 Тогда
				НоваяСтрокаКоэффициентов.КоэффициентОбъема = к2КоллекцииКлиентСервер.Разделить(ВесДня(День, Строка.Номенклатура, Строка.Контрагент), СуммарныйВес) * НайденныеСтроки[0].КоэффициентОбъема / 100;						
			Иначе
			    НоваяСтрокаКоэффициентов.КоэффициентОбъема = к2КоллекцииКлиентСервер.Разделить(ВесДня(День, Строка.Номенклатура, Строка.Контрагент), СуммарныйВес);
			КонецЕсли;
			СуммарныйОбъемОбщихКоэффициентов = СуммарныйОбъемОбщихКоэффициентов + НоваяСтрокаКоэффициентов.КоэффициентОбъема;
			
			НомерДня = НомерДня + 1;
				
		КонецЦикла;
		
		Для Каждого СтрокаКоэффициента Из ТаблицаОбщихКоэффициентов Цикл
			
			СтрокаКоэффициента.КоэффициентОбъема = к2КоллекцииКлиентСервер.Разделить(СтрокаКоэффициента.КоэффициентОбъема, СуммарныйОбъемОбщихКоэффициентов);
			
		КонецЦикла;
		
		МассивКоэффициентов = ТаблицаОбщихКоэффициентов.ВыгрузитьКолонку("КоэффициентОбъема");

		МассивПриростов = к2КоллекцииКлиентСервер.РаспределитьСуммуПропорциональноКоэффициентам(Строка.КоличествоПрирост, МассивКоэффициентов);
		
		НомерДня = 0;
		Для Каждого День Из МассивДнейПериода Цикл
			
			НоваяСтрока = Показатели.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка,, "СредниеПродажи");
			НоваяСтрока.Дата = День;
			НоваяСтрока.КоличествоПрирост = МассивПриростов[НомерДня];
			
			НомерДня = НомерДня + 1;
			
		КонецЦикла;
	
	КонецЦикла;
	
КонецПроцедуры

// Отражаем только те акции, которые имели прирост более среднего, рассчитанного в маркетинговом мероприятии.
Процедура ЗаполнитьПоЗавершенномуМаркетинговомуМероприятию(МаркетинговоеМероприятие)
	
	СтруктураММ = глРеквизиты(МаркетинговоеМероприятие, "ПлановаяДатаНачала КАК ДатаНачала, ПлановаяДатаОкончания КАК ДатаОкончания, Показатели");

	ТаблицаПродаж = ТаблицаФактическихПродажПоМаркетинговомуМероприятию(МаркетинговоеМероприятие, СтруктураММ.ДатаНачала, СтруктураММ.ДатаОкончания);
	
	Для Каждого СтрокаОбхода Из СтруктураММ.Показатели.Выгрузить() Цикл
		
		СтруктураПоиска = Новый Структура("Номенклатура,Характеристика,Контрагент");
		ЗаполнитьЗначенияСвойств(СтруктураПоиска, СтрокаОбхода);
		
		СуммаФактическихПродаж = к2Коллекции.СуммаТаблицыФормыПоОтбору(ТаблицаПродаж, СтруктураПоиска, "КоличествоОборот");
		Если СуммаФактическихПродаж <= СтрокаОбхода.ОбъемПродажБезАкции Тогда
			Продолжить;
		КонецЕсли;
		
		СтрокаСводныхПоказателей = СводныеПоказатели.Добавить();
		ЗаполнитьЗначенияСвойств(СтрокаСводныхПоказателей, СтрокаОбхода, "Номенклатура,Характеристика,Контрагент");
		СтрокаСводныхПоказателей.ДатаНачала = СтруктураММ.ДатаНачала;
		СтрокаСводныхПоказателей.ДатаОкончания = СтруктураММ.ДатаОкончания;
		СтрокаСводныхПоказателей.СредниеПродажи = СтрокаОбхода.ОбъемПродажБезАкции;
		СтрокаСводныхПоказателей.КоличествоПрирост = СуммаФактическихПродаж - СтрокаСводныхПоказателей.СредниеПродажи;
		
		Для Каждого Строка Из ТаблицаПродаж.НайтиСтроки(СтруктураПоиска) Цикл
			
			СтрокаДетальныхПоказателей = Показатели.Добавить();
			ЗаполнитьЗначенияСвойств(СтрокаДетальныхПоказателей, СтрокаОбхода, "Номенклатура,Характеристика,Контрагент");
			СтрокаДетальныхПоказателей.Дата = Строка.Период;
			СтрокаДетальныхПоказателей.КоличествоПрирост = Строка.КоличествоОборот;
			СтрокаДетальныхПоказателей.СредниеПродажи = СтрокаОбхода.ОбъемПродажБезАкции * к2КоллекцииКлиентСервер.Разделить(Строка.КоличествоОборот, СуммаФактическихПродаж);
			СтрокаДетальныхПоказателей.КоличествоПрирост = СтрокаДетальныхПоказателей.КоличествоПрирост - СтрокаДетальныхПоказателей.СредниеПродажи;
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

Функция ТаблицаФактическихПродажПоМаркетинговомуМероприятию(МаркетинговоеМероприятие, ДатаНачала, ДатаОкончания)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	к2МаркетинговоеМероприятиеПоказатели.Номенклатура КАК Номенклатура,
		|	к2МаркетинговоеМероприятиеПоказатели.Контрагент КАК Контрагент,
		|	к2МаркетинговоеМероприятиеПоказатели.Характеристика КАК Характеристика,
		|	к2МаркетинговоеМероприятиеКонтрагенты.УточнятьДоТочекДоставки КАК УточнятьДоТочекДоставки,
		|	к2МаркетинговоеМероприятиеТочкиДоставки.ТочкаДоставки КАК ТочкаДоставки
		|ПОМЕСТИТЬ ВТРазрезов
		|ИЗ
		|	Документ.к2МаркетинговоеМероприятие.Показатели КАК к2МаркетинговоеМероприятиеПоказатели
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.к2МаркетинговоеМероприятие.Контрагенты КАК к2МаркетинговоеМероприятиеКонтрагенты
		|		ПО к2МаркетинговоеМероприятиеПоказатели.Контрагент = к2МаркетинговоеМероприятиеКонтрагенты.Контрагент
		|			И к2МаркетинговоеМероприятиеПоказатели.Ссылка = к2МаркетинговоеМероприятиеКонтрагенты.Ссылка
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.к2МаркетинговоеМероприятие.ТочкиДоставки КАК к2МаркетинговоеМероприятиеТочкиДоставки
		|		ПО к2МаркетинговоеМероприятиеПоказатели.Контрагент = к2МаркетинговоеМероприятиеТочкиДоставки.Контрагент
		|			И к2МаркетинговоеМероприятиеПоказатели.Ссылка = к2МаркетинговоеМероприятиеТочкиДоставки.Ссылка
		|ГДЕ
		|	к2МаркетинговоеМероприятиеПоказатели.Ссылка = &Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТРазрезов.Номенклатура КАК Номенклатура,
		|	ВТРазрезов.Контрагент КАК Контрагент,
		|	ВТРазрезов.Характеристика КАК Характеристика,
		|	ЕСТЬNULL(к2ОтгрузкиТоваровОбороты.КоличествоОборот, 0) КАК КоличествоОборот,
		|	ЕСТЬNULL(к2ОтгрузкиТоваровОбороты.Период, &ДатаНачала) КАК Период
		|ИЗ
		|	ВТРазрезов КАК ВТРазрезов
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.к2ОтгрузкиТоваров.Обороты(&ДатаНачала, &ДатаОкончания, День, ) КАК к2ОтгрузкиТоваровОбороты
		|		ПО ВТРазрезов.Номенклатура = к2ОтгрузкиТоваровОбороты.АналитикаУчетаНоменклатуры.Номенклатура
		|			И ВТРазрезов.Характеристика = к2ОтгрузкиТоваровОбороты.АналитикаУчетаНоменклатуры.Характеристика
		|			И ВТРазрезов.Контрагент = к2ОтгрузкиТоваровОбороты.АналитикаУчетаПоПартнерам.Контрагент
		|			И (ВЫБОР
		|				КОГДА НЕ ВТРазрезов.УточнятьДоТочекДоставки
		|					ТОГДА ИСТИНА
		|				ИНАЧЕ ВТРазрезов.ТочкаДоставки = к2ОтгрузкиТоваровОбороты.АналитикаУчетаПоПартнерам.ТочкаДоставки
		|			КОНЕЦ)";
	
	Запрос.УстановитьПараметр("Ссылка", МаркетинговоеМероприятие);
	Запрос.УстановитьПараметр("ДатаНачала", ДатаНачала);
	Запрос.УстановитьПараметр("ДатаОкончания", ДатаОкончания);
	
	Возврат Запрос.Выполнить().Выгрузить();
					
КонецФункции

Функция ТаблицаСводныхПоказателейПоМаркетинговомуМероприятию(МаркетинговоеМероприятие)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.Номенклатура КАК Номенклатура,
	|	к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.Характеристика КАК Характеристика,
	|	к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.Контрагент КАК Контрагент,
	|	СУММА(к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.ОбъемПродажБезАкцииОборот) КАК СредниеПродажи,
	|	СУММА(ВЫБОР
	|			КОГДА &АкцияЗавершена
	|				ТОГДА ВЫБОР
	|						КОГДА к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.ПродажиФактОборот - к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.ОбъемПродажБезАкцииОборот < 0
	|							ТОГДА 0
	|						ИНАЧЕ к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.ПродажиФактОборот - к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.ОбъемПродажБезАкцииОборот
	|					КОНЕЦ
	|			ИНАЧЕ к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.ПриростОбъемовОборот
	|		КОНЕЦ) КАК КоличествоПрирост
	|ИЗ
	|	РегистрНакопления.к2ОбъемныеПоказателиМаркетинговыхМероприятий.Обороты(, , , МаркетинговоеМероприятие = &МаркетинговоеМероприятие) КАК к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты
	|
	|СГРУППИРОВАТЬ ПО
	|	к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.Номенклатура,
	|	к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.Характеристика,
	|	к2ОбъемныеПоказателиМаркетинговыхМероприятийОбороты.Контрагент";
	
	Запрос.УстановитьПараметр("МаркетинговоеМероприятие", МаркетинговоеМероприятие);
	Запрос.УстановитьПараметр("АкцияЗавершена", РегистрыСведений.к2СтатусыМаркетинговыхМероприятий.СтатусМаркетинговогоМероприятия(МаркетинговоеМероприятие) = Перечисления.к2СтатусыМаркетинговыхМероприятий.Завершена);
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

Функция СуммарныйВесПериода(ДатаНачала, ДатаОкончания, Номенклатура, Контрагент)
	
	СуммарныйВес = 0;
	
	Для Каждого День Из к2ДатыКлиентСервер.МассивДнейПериода(ДатаНачала, ДатаОкончания) Цикл
		
		СуммарныйВес = СуммарныйВес + ВесДня(День, Номенклатура, Контрагент);
		
	КонецЦикла;
	
	Возврат СуммарныйВес;
	
КонецФункции

Функция ВесДня( Знач День, Знач Номенклатура, Знач Контрагент )
	
	лДеньНедели = ДеньНедели( День );
	лТоварнаяКатегория = глРеквизит_Кэш( Номенклатура, "ТоварнаяКатегория" );
	лГруппаПланирования = глРеквизит_Кэш( Контрагент, "ГруппаПланирования" );
	
	Возврат к2КэшНаСеанс.ВесаДнейБалансировки_Вес( лДеньНедели, лТоварнаяКатегория, лГруппаПланирования );
	
КонецФункции

Функция ТаблицаКоэффициентовДняПрофиля(ПрофильАкции, КоличествоДней)
	
	ТаблицаКоэффициентов = ПустаяТаблицаКоэффициентовДня();
	
	Если КоличествоДней = 0 ИЛИ Не ЗначениеЗаполнено(ПрофильАкции) Тогда
		Возврат ТаблицаКоэффициентов;
	КонецЕсли;
	
	ДеньПериода = 1;
	
	Пока ДеньПериода <= КоличествоДней Цикл
		
		СтрокаКоэффициента = ТаблицаКоэффициентов.Добавить();
		СтрокаКоэффициента.НомерДня = ДеньПериода;
				
		ПроцентПрошлогоПериода = к2КоллекцииКлиентСервер.Разделить((ДеньПериода - 1) * 100, КоличествоДней);
		ПроцентТекущегоПериода = к2КоллекцииКлиентСервер.Разделить(ДеньПериода * 100, КоличествоДней);
		
		Для Каждого СтрокаПоказателя Из ПрофильАкции.ПоказателиПрофиля Цикл
			
			Если СтрокаПоказателя.КонечныйПроцент < ПроцентПрошлогоПериода
				ИЛИ СтрокаПоказателя.НачальныйПроцент > ПроцентТекущегоПериода Тогда
				Продолжить;
			КонецЕсли;
			
			Если СтрокаПоказателя.НачальныйПроцент <= ПроцентПрошлогоПериода
				И СтрокаПоказателя.КонечныйПроцент < ПроцентТекущегоПериода Тогда
				
				ПроцентПериода = СтрокаПоказателя.КонечныйПроцент - СтрокаПоказателя.НачальныйПроцент;
				
				СтрокаКоэффициента.КоэффициентОбъема = СтрокаКоэффициента.КоэффициентОбъема + к2КоллекцииКлиентСервер.Разделить(СтрокаПоказателя.ПроцентОбъема * (СтрокаПоказателя.КонечныйПроцент - ПроцентПрошлогоПериода), ПроцентПериода);
				
			ИначеЕсли СтрокаПоказателя.НачальныйПроцент < ПроцентПрошлогоПериода
				И СтрокаПоказателя.КонечныйПроцент >= ПроцентТекущегоПериода Тогда
				
				ПроцентПериода = СтрокаПоказателя.КонечныйПроцент - СтрокаПоказателя.НачальныйПроцент;
				
				СтрокаКоэффициента.КоэффициентОбъема = СтрокаКоэффициента.КоэффициентОбъема + к2КоллекцииКлиентСервер.Разделить(СтрокаПоказателя.ПроцентОбъема * (ПроцентТекущегоПериода - ПроцентПрошлогоПериода), ПроцентПериода);
				
			ИначеЕсли СтрокаПоказателя.НачальныйПроцент > ПроцентПрошлогоПериода
				И СтрокаПоказателя.КонечныйПроцент < ПроцентТекущегоПериода Тогда
				
				СтрокаКоэффициента.КоэффициентОбъема = СтрокаКоэффициента.КоэффициентОбъема + СтрокаПоказателя.ПроцентОбъема;
				
			ИначеЕсли СтрокаПоказателя.НачальныйПроцент >= ПроцентПрошлогоПериода
				И СтрокаПоказателя.КонечныйПроцент >= ПроцентТекущегоПериода Тогда
				
				ПроцентПериода = СтрокаПоказателя.КонечныйПроцент - СтрокаПоказателя.НачальныйПроцент;
				
				СтрокаКоэффициента.КоэффициентОбъема = СтрокаКоэффициента.КоэффициентОбъема + к2КоллекцииКлиентСервер.Разделить(СтрокаПоказателя.ПроцентОбъема * (ПроцентТекущегоПериода - СтрокаПоказателя.НачальныйПроцент), ПроцентПериода);
				
			КонецЕсли;
			
		КонецЦикла;
		
		ДеньПериода = ДеньПериода + 1;
		
	КонецЦикла;
	
	СуммаКоэффициентовОбъема = ТаблицаКоэффициентов.Итог("КоэффициентОбъема");
	ТаблицаКоэффициентов.Сортировать("КоэффициентОбъема Убыв");
	Если Не СуммаКоэффициентовОбъема = 100 Тогда
		ТаблицаКоэффициентов[0].КоэффициентОбъема = ТаблицаКоэффициентов[0].КоэффициентОбъема - (СуммаКоэффициентовОбъема - 100);
	КонецЕсли;
	
	Возврат ТаблицаКоэффициентов;
	
КонецФункции

Функция ПустаяТаблицаКоэффициентовДня()
	
	Таблица = Новый ТаблицаЗначений;
	Таблица.Колонки.Добавить("НомерДня", Новый ОписаниеТипов("Число"));
	Таблица.Колонки.Добавить("КоэффициентОбъема", Новый ОписаниеТипов("Число"));
	
	Возврат Таблица;
	
КонецФункции

Процедура ЗаполнитьПоКоммерческойАктивности( Знач пКоммерческаяАктивность )
	
	Основание = пКоммерческаяАктивность;
	
	СтратегияВлиянияНаОтгрузки = Перечисления.к2СтратегииВлиянияМаркетинговыхМероприятийНаОтгрузки.Полнообъемная;
	ФакторПродаж               = Основание.ФакторПродаж;
	
	Показатели.Очистить();
	СводныеПоказатели.Очистить();
	
	Для каждого цСтрока Из Основание.Показатели Цикл
		
		новСтрока = СводныеПоказатели.Добавить();
		ЗаполнитьЗначенияСвойств( новСтрока, цСтрока );
		
		новСтрока.ДатаНачала = Основание.ПлановаяДатаНачала;
		новСтрока.ДатаОкончания = Основание.ПлановаяДатаОкончания;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Процедура СформироватьСписокРегистровДляКонтроля() Экспорт
	
	Массив = Новый Массив;
	
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);
	
КонецПроцедуры

Процедура ЗаполнитьСредниеОтгрузкиПоДням()
	
	Для Каждого Строка Из к2Коллекции.ТаблицаОбхода(Показатели.Выгрузить(), "Контрагент, Номенклатура") Цикл
		
		ТаблицаОтбора = Показатели.НайтиСтроки(ОбщегоНазначения.СтрокаТаблицыЗначенийВСтруктуру(Строка));
		массивкоэффициентов = Новый Массив;
		Для Каждого СтрокаПоОтбору Из ТаблицаОтбора Цикл
			массивкоэффициентов.Добавить(СтрокаПоОтбору.КоличествоПрирост);
		КонецЦикла;
		
		СуммаКРаспределению = 0;
		СтрокиСводныхПоказателей = СводныеПоказатели.НайтиСтроки(ОбщегоНазначения.СтрокаТаблицыЗначенийВСтруктуру(Строка));
		
		Если СтрокиСводныхПоказателей.Количество() > 0 Тогда
			СуммаКРаспределению = СтрокиСводныхПоказателей[0].СредниеПродажи;
		КонецЕсли;
		
		МассивРаспределения = ОбщегоНазначения.РаспределитьСуммуПропорциональноКоэффициентам(СуммаКРаспределению, массивкоэффициентов);
		
		Если НЕ МассивРаспределения = Неопределено Тогда
		
			Для Индекс = 0 По МассивРаспределения.Количество()-1 Цикл
				ТаблицаОтбора[Индекс].СредниеПродажи = МассивРаспределения[Индекс];
			КонецЦикла;
		
		КонецЕсли;
		
	КонецЦикла;

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
