// см. ЭлектронноеАктированиеЕИС.ОбменПоОрганизацииВключен
Функция ОбменПоОрганизацииВключен(Организация) Экспорт
	
	Возврат ЭлектронноеАктированиеЕИС.ОбменПоОрганизацииВключен(Организация);
	
КонецФункции

Функция МестаПоставкиДокумента(Документ) Экспорт
	
	МестаПоставки = Новый Массив;
	ЭлектронноеАктированиеЕИСПереопределяемый.
		ЗаполнитьМестаПоставкиДокумента(Документ, МестаПоставки);
		
	Возврат МестаПоставки;
	
КонецФункции

Функция ЗначениеРеквизитаОбъекта(Объект, ИмяРеквизита) Экспорт

	Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект, ИмяРеквизита);
	
КонецФункции

Функция ПредставлениеКонтрактаПоДоговору(Договор) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	ПРЕДСТАВЛЕНИЕ(ГосударственныеКонтрактыЕИС.Ссылка) КАК Представление,
	|	ГосударственныеКонтрактыЕИС.Номер,
	|	ГосударственныеКонтрактыЕИС.ДатаЗаключенияКонтракта
	|ИЗ
	|	Справочник.ГосударственныеКонтрактыЕИС КАК ГосударственныеКонтрактыЕИС
	|ГДЕ
	|	ГосударственныеКонтрактыЕИС.ВладелецКонтракта = &ВладелецКонтракта";
	
	Запрос.УстановитьПараметр("ВладелецКонтракта", Договор);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Шаблон = НСтр("ru = 'Данные для электронного актирования по контракту №%1 от %2'");
		Представление = СтрШаблон(Шаблон, Выборка.Номер,
			Формат(Выборка.ДатаЗаключенияКонтракта, "ДФ=dd.MM.yyyy;"));
		Возврат Представление;
	Иначе
		Возврат "<нет данных о контракте>";
	КонецЕсли;
	
КонецФункции

Функция ИспользуетсяНесколькоОрганизаций() Экспорт
	
	Возврат ОбщегоНазначенияБЭД.ИспользуетсяНесколькоОрганизаций();
	
КонецФункции

Процедура СкрытьЭлементыФормыПриИспользованииОднойОрганизации(Форма, ИмяЭлементаИлиМассив) Экспорт
		
	Если НЕ ИспользуетсяНесколькоОрганизаций() Тогда
		
		Если ТипЗнч(ИмяЭлементаИлиМассив) = Тип("Массив") Тогда
			
			Для Каждого ИмяОдногоЭлемента Из ИмяЭлементаИлиМассив Цикл
				
				СкрываемыйЭлемент = Форма.Элементы.Найти(ИмяОдногоЭлемента);
				
				Если СкрываемыйЭлемент <> Неопределено Тогда
					СкрываемыйЭлемент.Видимость = Ложь;
				КонецЕсли;
				
			КонецЦикла; 
			
		ИначеЕсли ТипЗнч(ИмяЭлементаИлиМассив) = Тип("Строка") Тогда
			
			СкрываемыйЭлемент = Форма.Элементы.Найти(ИмяЭлементаИлиМассив);
			
			Если СкрываемыйЭлемент <> Неопределено Тогда
				
				СкрываемыйЭлемент.Видимость = Ложь;
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Функция ДанныеГосударственногоКонтракта(СсылкаНаКонтракт, ВВидеСтроки=Ложь) Экспорт
	
	Возврат ЭлектронноеАктированиеЕИС.ДанныеГосударственногоКонтракта(СсылкаНаКонтракт, ВВидеСтроки);
	
КонецФункции

Функция ДанныеСтрокКонтрактаПоНоменклатуре(ГосударственныйКонтрактЕИС, Номенклатура) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки.Идентификатор КАК Идентификатор,
	               |	ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки.Номенклатура КАК Номенклатура,
	               |	ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки.Количество КАК Количество,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.ЕдиницаИзмерения КАК ЕдиницаИзмерения,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.Количество КАК Количество1,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.Цена КАК Цена,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.Сумма КАК Сумма,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.КодТовараДляЕИС КАК КодТовараДляЕИС,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.СтавкаНДС КАК СтавкаНДС,
	               |	ГосударственныеКонтрактыЕИСОбъектыЗакупки.Тип КАК Тип
	               |ИЗ
	               |	Справочник.ГосударственныеКонтрактыЕИС.НоменклатураОбъектовЗакупки КАК ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ГосударственныеКонтрактыЕИС.ОбъектыЗакупки КАК ГосударственныеКонтрактыЕИСОбъектыЗакупки
	               |		ПО ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки.Идентификатор = ГосударственныеКонтрактыЕИСОбъектыЗакупки.Идентификатор
	               |ГДЕ
	               |	ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки.Ссылка = &ГосКонтрактСсылка
	               |	И ГосударственныеКонтрактыЕИСНоменклатураОбъектовЗакупки.Номенклатура = &Номенклатура";
	
	Запрос.УстановитьПараметр("ГосКонтрактСсылка", ГосударственныйКонтрактЕИС);
	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
	
	ИдентификаторыСтрокКонтракта = Запрос.Выполнить().Выгрузить();
	Возврат ОбщегоНазначения.ТаблицаЗначенийВМассив(ИдентификаторыСтрокКонтракта);
	
КонецФункции

Процедура ОбработкаВыбораГосударственногоКонтракта(Владелец,
		ПредыдущееЗначение,
		НовоеЗначение) Экспорт
	
	Если ПредыдущееЗначение = НовоеЗначение Тогда
		// Не изменилось значение.
		Возврат;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ПредыдущееЗначение) И НЕ ПредыдущееЗначение.Пустая() Тогда
		// Очищаем владельца предыдущего контракта.
		ОбъектКонтракта = ПредыдущееЗначение.ПолучитьОбъект();
		ОбъектКонтракта.ВладелецКонтракта = Неопределено;
		ОбъектКонтракта.Записать();
	КонецЕсли;
	
	Если ЗначениеЗаполнено(НовоеЗначение) И НЕ НовоеЗначение.Пустая() Тогда
		ОбъектКонтракта = НовоеЗначение.ПолучитьОбъект();
		ОбъектКонтракта.ВладелецКонтракта = Владелец.Ссылка;
		ОбъектКонтракта.Записать();
	КонецЕсли;
	
КонецПроцедуры

Функция НайтиГосударственныйКонтрактПоВладельцу(ВладелецКонтракта) Экспорт
	
	Возврат ЭлектронноеАктированиеЕИС.НайтиГосударственныйКонтрактПоВладельцу(ВладелецКонтракта);
	
КонецФункции

Функция ПодписантыЕИС(Объект) Экспорт
	
	Возврат ЭлектронноеАктированиеЕИС.ПодписантыЕИС(Объект);
	
КонецФункции

Функция ПриложенныеДокументыДляЕИС(Объект) Экспорт
	
	ТаблицаФайлов = ЭлектронноеАктированиеЕИС.ДанныеФайловПрисоединенныхКДокументу(Объект);
	
	Возврат ОбщегоНазначения.ТаблицаЗначенийВМассив(ТаблицаФайлов);
	
КонецФункции

Функция МестаПоставкиДляЕИС(Объект) Экспорт
	
	Возврат ЭлектронноеАктированиеЕИС.МестаПоставкиДляЕИС(Объект);
	
КонецФункции