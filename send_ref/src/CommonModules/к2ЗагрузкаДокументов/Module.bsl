#Область СлужебныйПрограммныйИнтерфейс

Функция СоздатьЗаказыКлиента(Знач пТаблицаЗаказов) Экспорт
	
	реквизитыШапки = "ДатаЗаказа,Комментарий,Ответственный,
	|Организация,Подразделение,Контрагент,Соглашение,ТочкаДоставки,
	|ДатаОтгрузки,ДатаДоставки,
	|НомерПоДаннымКлиента,ДатаПоДаннымКлиента";
	
	созданныеДокументы = Новый Массив;
	
	тзОбхода = к2Коллекции.СгруппированнаяТаблица(пТаблицаЗаказов, реквизитыШапки);
	
	Для каждого цСтрокаОбхода Из тзОбхода Цикл
		
		обЗаказ = Документы.ЗаказКлиента.СоздатьДокумент();
		
		обЗаказ.УстановитьОтветственного(цСтрокаОбхода.Ответственный);
		обЗаказ.УстановитьОрганизацию(цСтрокаОбхода.Организация);
		обЗаказ.УстановитьПодразделение(цСтрокаОбхода.Подразделение);
		обЗаказ.УстановитьКонтрагента(цСтрокаОбхода.Контрагент);
		обЗаказ.УстановитьСоглашение(цСтрокаОбхода.Соглашение);
		обЗаказ.УстановитьТочкуДоставки(цСтрокаОбхода.ТочкаДоставки);
		обЗаказ.УстановитьДатыОтгрузки(цСтрокаОбхода.ДатаОтгрузки, цСтрокаОбхода.ДатаДоставки);
		обЗаказ.УстановитьДанныеКлиента(цСтрокаОбхода.НомерПоДаннымКлиента, цСтрокаОбхода.ДатаПоДаннымКлиента);
		
		обЗаказ.Инициализировать(цСтрокаОбхода.ДатаЗаказа, цСтрокаОбхода.Комментарий);
		
		обЗаказ.УстановитьТовары(цСтрокаОбхода.Таблица);
		
		обЗаказ.СпособСоздания = Перечисления.к2СпособыСозданияДокументов.ЗагрузкаИзФайла;
		
		оперативно = НачалоДня(обЗаказ.Дата) = НачалоДня(ТекущаяДатаСеанса());
		
		успешно = к2ПроведениеСервер.ПровестиИлиЗаписатьДокумент(обЗаказ, оперативно,, Ложь);
		
		Если успешно Тогда
			
			созданныеДокументы.Добавить(обЗаказ.Ссылка);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат созданныеДокументы;
	
КонецФункции

Функция СоздатьЗаказыНаПеремещение(Знач пТаблицаЗаказов) Экспорт
	
	реквизитыШапки = "ДатаЗаказа,Комментарий,Ответственный,
	|Организация,Подразделение,СкладОтправитель,СкладПолучатель,
	|ДатаОтгрузки,ДатаДоставки";
	
	созданныеДокументы = Новый Массив;
	
	тзОбхода = к2Коллекции.СгруппированнаяТаблица(пТаблицаЗаказов, реквизитыШапки);
	
	Для каждого цСтрокаОбхода Из тзОбхода Цикл
		
		обЗаказ = Документы.к2ЗаказНаПеремещение.СоздатьДокумент();
		
		обЗаказ.УстановитьОтветственного(цСтрокаОбхода.Ответственный);
		обЗаказ.УстановитьОрганизацию(цСтрокаОбхода.Организация);
		обЗаказ.УстановитьПодразделение(цСтрокаОбхода.Подразделение);
		обЗаказ.УстановитьСклады(цСтрокаОбхода.СкладОтправитель, цСтрокаОбхода.СкладПолучатель);
		обЗаказ.УстановитьДатыОтгрузки(цСтрокаОбхода.ДатаОтгрузки, цСтрокаОбхода.ДатаДоставки);
		
		обЗаказ.Инициализировать(цСтрокаОбхода.ДатаЗаказа, цСтрокаОбхода.Комментарий);
		
		обЗаказ.УстановитьТовары(цСтрокаОбхода.Таблица);
		
		обЗаказ.СпособСоздания = Перечисления.к2СпособыСозданияДокументов.ЗагрузкаИзФайла;
		
		оперативно = НачалоДня(обЗаказ.Дата) = НачалоДня(ТекущаяДатаСеанса());
		
		успешно = к2ПроведениеСервер.ПровестиИлиЗаписатьДокумент(обЗаказ, оперативно, , Ложь);
		
		Если успешно Тогда
			
			созданныеДокументы.Добавить(обЗаказ.Ссылка);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат созданныеДокументы;
	
КонецФункции

#КонецОбласти