
#Область ПрограммныйИнтерфейс

#Область УправлениеДоступом

// Описание поставляемого профиля "Планирование разруба"
//
// Параметры:
//  ОписанияПрофилей - Массив из структуры см. УправлениеДоступом.НовоеОписаниеПрофиляГруппДоступа - описания профилей групп доступа.
//
Процедура ДобавитьОписаниеПрофиля_ПланированиеРазруба(ОписанияПрофилей) Экспорт
	
	ОписаниеПрофиля = УправлениеДоступом.НовоеОписаниеПрофиляГруппДоступа();
	ОписаниеПрофиля.Имя           = "ПланированиеРазруба";
	ОписаниеПрофиля.Идентификатор = "3111d297-3877-4bf6-94dd-80c91a1da3f5";
	
	ОписаниеПрофиля.Наименование = НСтр("ru = 'Планирование разруба'", ОбщегоНазначения.КодОсновногоЯзыка());
	
	ОписаниеПрофиля.Описание = НСтр("ru = 'Предоставляет права на планирование разруба.'");
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.Подсистемак2ПланированиеОпераций.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Разруб_ИспользованиеОбработкиПланирование.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Разруб_СозданиеИзменениеДокумента_План.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Разруб_ЧтениеСправочник_Варианты.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникВолны.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Разруб_ЧтениеСправочникСценарии.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ЧтениеСправочникСценарии.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникНастройкиФормированияЗаявкиНаПроизводство.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникВариантыНачалаПериода.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникРесурсныеСпецификации.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникЭтапыПроизводства.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникНоменклатура.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникХарактеристикиНоменклатуры.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникРабочиеЦентры.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникВидыРабочихЦентров.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникПодразделения.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникТехнологическиеОперации.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникОрганизации.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ЧтениеРегистрСведенийПараметры.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ЧтениеРегистрСведенийПриоритеты.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеДокументЗаявкаНаПроизводство.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеДокументПодтверждениеЗаявкиНаПроизводство.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеДокументПланУбоя.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ПросмотрОтчетЗаявкиНаПроизводство.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ИспользованиеОбработкиПанельПользователя.Имя);
	
	ОписанияПрофилей.Добавить(ОписаниеПрофиля);
	
КонецПроцедуры

// Описание поставляемого профиля "Изменение НСИ разруба"
//
// Параметры:
//  ОписанияПрофилей - Массив из структуры см. УправлениеДоступом.НовоеОписаниеПрофиляГруппДоступа - описания профилей групп доступа.
//
Процедура ДобавитьОписаниеПрофиля_ИзменениеНСИРазруба(ОписанияПрофилей) Экспорт
	
	ОписаниеПрофиля = УправлениеДоступом.НовоеОписаниеПрофиляГруппДоступа();
	ОписаниеПрофиля.Имя           = "ИзменениеНСИРазруба";
	ОписаниеПрофиля.Идентификатор = "6e3a9048-6fac-4aa1-b1f4-3b02b3a1ff22";
	
	ОписаниеПрофиля.Наименование = НСтр("ru = 'Изменение НСИ разруба'", ОбщегоНазначения.КодОсновногоЯзыка());
	
	ОписаниеПрофиля.Описание = НСтр("ru = 'Предоставляет права на НСИ разруба.'");
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.Подсистемак2НормативноСправочнаяИнформация.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.Подсистемак2ПланированиеОпераций.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникТехнологическиеОперации.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ЧтениеСправочникНастройкиФормированияЗаявкиНаПроизводство.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникТоварныеКатегории.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникПроизводители.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникВидыНоменклатуры.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникНоменклатура.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникХарактеристикиНоменклатуры.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникСклады.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникПодразделения.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникВидыРабочихЦентров.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникРабочиеЦентры.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникРесурсныеСпецификации.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникЭтапыПроизводства.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Разруб_ДобавлениеИзменениеСправочник_Варианты.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникВолны.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Разруб_ДобавлениеИзменениеСправочникСценарии.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ДобавлениеИзменениеСправочникСценарии.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникВариантыНачалаПериода.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2ДобавлениеИзменениеСправочникНастройкиФормированияЗаявкиНаПроизводство.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ИспользованиеОбработки_Настройки.Имя);
	
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ДобавлениеИзменениеРегистрСведенийПараметры.Имя);
	ОписаниеПрофиля.Роли.Добавить(Метаданные.Роли.к2Планирование_ДобавлениеИзменениеРегистрСведенийПриоритеты.Имя);
	
	ОписанияПрофилей.Добавить(ОписаниеПрофиля);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Функция ШтрафЗаОграничениеВыхода(Знач пВес, Знач пОграничение) Экспорт

	Если пОграничение = Неопределено Тогда
		
		Возврат 0;
		
	КонецЕсли;
	
	Если пВес = 0 Тогда
		
		Возврат 0;
	
	КонецЕсли;
	
	СТОИМОСТЬ_НАРУШЕНИЯ         = 10000000;
	ПРОЦЕНТ_БЕЗ_УЧЕТА_НАРУШЕНИЯ = 0.05;
	
	Если пОграничение = 0 Тогда
		
		Возврат СТОИМОСТЬ_НАРУШЕНИЯ + СТОИМОСТЬ_НАРУШЕНИЯ*пВес*пВес;
	
	КонецЕсли;
	
	отклонение = (пОграничение - пВес)/пОграничение;
	
	Если отклонение = 0 Тогда
		
		Возврат СТОИМОСТЬ_НАРУШЕНИЯ;
		
	ИначеЕсли отклонение < 0 Тогда
		
		Возврат СТОИМОСТЬ_НАРУШЕНИЯ + СТОИМОСТЬ_НАРУШЕНИЯ*отклонение*отклонение;
		
	ИначеЕсли отклонение > ПРОЦЕНТ_БЕЗ_УЧЕТА_НАРУШЕНИЯ Тогда
		
		Возврат 0;
		
	Иначе
		
		Возврат 1/(отклонение*отклонение);
		
	КонецЕсли;

КонецФункции

#КонецОбласти
