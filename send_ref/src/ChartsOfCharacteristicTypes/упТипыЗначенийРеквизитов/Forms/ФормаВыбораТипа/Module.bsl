
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СоставнойТипДанных = Параметры.НачальноеЗначение.Типы().Количество() > 1;
	ТолькоВыбранные = СоставнойТипДанных;
	
	ЗаполнитьИсходноеДерево();
	
	ЗаполнитьДеревоНаФормеПоФильтру();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	колвоВыбранныхСтрок = ВыбранныеСтроки.Количество();
	
	Если колвоВыбранныхСтрок > 0 Тогда
		
		Элементы.ДеревоОбъектов.ТекущаяСтрока = ВыбранныеСтроки[колвоВыбранныхСтрок-1].Значение;
		
	КонецЕсли;
	
КонецПроцедуры


&НаКлиенте
Процедура Отмена(Команда)
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура Ок(Команда)
	
	выбранныеТипы = Новый Массив;
	
	КвалификаторыЧисла  = Неопределено;
	КвалификаторыСтроки = Неопределено;
	КвалификаторыДаты   = Неопределено;
	
	Для Каждого цСтрока Из ВыбранныеСтроки Цикл
		
		типДанных = исхДеревоОбъектов.НайтиПоИдентификатору( цСтрока.Значение ).ТипДанных;
		
		Если типДанных.СодержитТип( Тип("Число") ) Тогда
			
			КвалификаторыЧисла = типДанных.КвалификаторыЧисла;
			
		ИначеЕсли типДанных.СодержитТип( Тип("Дата") ) Тогда
			
			КвалификаторыДаты = типДанных.КвалификаторыДаты;
			
		ИначеЕсли типДанных.СодержитТип( Тип("Строка") ) Тогда
			
			КвалификаторыСтроки = типДанных.КвалификаторыСтроки;
			
		КонецЕсли;
		
		массивТипов = типДанных.Типы();
		
		Для Каждого цТип Из массивТипов Цикл
			
			выбранныеТипы.Добавить( цТип );
			
		КонецЦикла;
		
	КонецЦикла;
	
	выбранныеТип = Новый ОписаниеТипов( выбранныеТипы, КвалификаторыЧисла, КвалификаторыСтроки, КвалификаторыДаты);
	
	ОповеститьОВыборе( выбранныеТип );
	
КонецПроцедуры




Процедура ЗаполнитьИсходноеДерево()
	
	Дерево = РеквизитФормыВЗначение("исхДеревоОбъектов");
	
	типыПВХ = Метаданные.ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Тип.Типы();
	
	ДобавитьСтроку( Дерево.Строки, Тип("Число") , БиблиотекаКартинок.упТипЧисло  );
	ДобавитьСтроку( Дерево.Строки, Тип("Строка"), БиблиотекаКартинок.упТипСтрока );
	ДобавитьСтроку( Дерево.Строки, Тип("Дата")  , БиблиотекаКартинок.упТипДата   );
	ДобавитьСтроку( Дерево.Строки, Тип("Булево"), БиблиотекаКартинок.упТипБулево );
	
	
	ДобавитьГруппу( Дерево, "Справочники"                    , Справочники.ТипВсеСсылки().Типы()                                , типыПВХ, БиблиотекаКартинок.Справочник);
	ДобавитьГруппу( Дерево, "Документы"                      , Документы.ТипВсеСсылки().Типы()                                  , типыПВХ, БиблиотекаКартинок.Документ);
	ДобавитьГруппу( Дерево, "Перечисления"                   , Перечисления.ТипВсеСсылки().Типы()                               , типыПВХ, БиблиотекаКартинок.Перечисление);
	ДобавитьГруппу( Дерево, "Планы видов характеристик"      , ПланыВидовХарактеристик.ТипВсеСсылки().Типы()                    , типыПВХ, БиблиотекаКартинок.ПланВидовХарактеристик);
	ДобавитьГруппу( Дерево, "Планы счетов"                   , ПланыСчетов.ТипВсеСсылки().Типы()                                , типыПВХ, БиблиотекаКартинок.ПланСчетов);
	ДобавитьГруппу( Дерево, "Планы видов расчета"            , ПланыВидовРасчета.ТипВсеСсылки().Типы()                          , типыПВХ, БиблиотекаКартинок.ПланВидовРасчета);
	ДобавитьГруппу( Дерево, "Бизнес-процессы"                , БизнесПроцессы.ТипВсеСсылки().Типы()                             , типыПВХ, БиблиотекаКартинок.БизнесПроцесс);
	ДобавитьГруппу( Дерево, "Точки маршрута бизнес-процесса" , БизнесПроцессы.ТипВсеСсылкиТочекМаршрутаБизнесПроцессов().Типы() , типыПВХ, БиблиотекаКартинок.БизнесПроцесс);
	ДобавитьГруппу( Дерево, "Задачи"                         , Задачи.ТипВсеСсылки().Типы()                                     , типыПВХ, БиблиотекаКартинок.Задача);
	ДобавитьГруппу( Дерево, "Планы обмена"                   , ПланыОбмена.ТипВсеСсылки().Типы()                                , типыПВХ, БиблиотекаКартинок.ПланОбмена);
	
	ЗначениеВРеквизитФормы(Дерево, "исхДеревоОбъектов");
	
КонецПроцедуры

Процедура ЗаполнитьДеревоНаФормеПоФильтру( ИДТекСтроки = Неопределено )
	
	текДанные = Элементы.ДеревоОбъектов.ТекущаяСтрока;
	
	Если текДанные = Неопределено Тогда
		
		текТип = Неопределено;
		
	Иначе
		
		текТип = ДеревоОбъектов.НайтиПоИдентификатору( текДанные ).ТипДанных;
		
	КонецЕсли;
	
	ИДТекСтроки = Неопределено;
	
	строкиДерева = ДеревоОбъектов.ПолучитьЭлементы();
	строкиДерева.Очистить();
	
	ВыбранныеСтроки.Очистить();
	ОтображаемыеВыбранныеСтроки.Очистить();
	
	ДобавитьСтрокиВФильтрДерево( строкиДерева, исхДеревоОбъектов, текТип, ИДТекСтроки );
	
КонецПроцедуры

Процедура ДобавитьСтрокиВФильтрДерево( пСтроки, пИсхСтрока, пТекТип, пИДТекСтроки )
	
	Для Каждого цИсхСтрока Из пИсхСтрока.ПолучитьЭлементы() Цикл
		
		Если цИсхСтрока.Выбрано Тогда
			ВыбранныеСтроки.Добавить( цИсхСтрока.ПолучитьИдентификатор() );
		КонецЕсли;
		
		Если Не ДобавлятьСтроку( цИсхСтрока ) Тогда
			Продолжить;
		КонецЕсли;
		
		новСтрока = пСтроки.Добавить();
		
		ЗаполнитьЗначенияСвойств( новСтрока, цИсхСтрока );
		новСтрока.ИДИсходнойСтроки = цИсхСтрока.ПолучитьИдентификатор();  //установка связи с исходным деревом
		
		// текущую строку установим по возвращению с клиента
		Если новСтрока.ТипДанных = пТекТип Тогда
			пИДТекСтроки = новСтрока.ПолучитьИдентификатор();
		КонецЕсли;
		
		// сохраним выбранные в сз для более быстрого поиска
		Если новСтрока.Выбрано Тогда
			ОтображаемыеВыбранныеСтроки.Добавить( новСтрока );
		КонецЕсли;
		
		ДобавитьСтрокиВФильтрДерево( новСтрока.ПолучитьЭлементы() , цИсхСтрока, пТекТип, пИДТекСтроки );
		
		Если новСтрока.ЭтоГруппа
			И новСтрока.ПолучитьЭлементы().Количество() = 0 Тогда
			
			пСтроки.Удалить( новСтрока );
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры


Функция ДобавлятьСтроку( пСтрока )
	
	Если пСтрока.ЭтоГруппа Тогда
		// добавляем строки - группы
		Возврат Истина;
	КонецЕсли;
	
	фильтрПоПоиску = Не ЗначениеЗаполнено( ПоискТипа ) ИЛИ СтрНайти( пСтрока.Представление, ПоискТипа ) > 0;
	фильтрПоВыбранному = Не ТолькоВыбранные ИЛИ пСтрока.Выбрано;
	
	Возврат фильтрПоПоиску И фильтрПоВыбранному;
	
КонецФункции

&НаКлиенте
Процедура РазвернутьУровниПриНеобходимости()
	
	Если ЗначениеЗаполнено( ПоискТипа )
		ИЛИ ТолькоВыбранные Тогда
		
		Для Каждого цСтрока Из ДеревоОбъектов.ПолучитьЭлементы() Цикл
			
			Элементы.ДеревоОбъектов.Развернуть( цСтрока.ПолучитьИдентификатор() );
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры


&НаСервере
Процедура ДобавитьГруппу(Дерево, ИмяГруппы, ТипыГруппы, ТипыПВХ, пКартинка )
	
	добавляемыеТипы = упКоллекцииКлиентСервер.ПересечьМассивы( ТипыГруппы, ТипыПВХ );
	
	Если добавляемыеТипы.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	СтрГруппы = Дерево.Строки.Добавить();
	СтрГруппы.ТипДанных     = Новый ОписаниеТипов( добавляемыеТипы );
	СтрГруппы.Представление = ИмяГруппы;
	СтрГруппы.Картинка      = пКартинка;
	СтрГруппы.ЭтоГруппа    = Истина;
	
	Для Каждого цТип Из добавляемыеТипы Цикл
		
		ДобавитьСтроку( СтрГруппы.Строки, цТип, пКартинка );
		
	КонецЦикла;
	
	СтрГруппы.Строки.Сортировать( "Представление" );
	
КонецПроцедуры

Процедура ДобавитьСтроку( пСтроки, пТип, пКартинка )
	
	текСтрока = пСтроки.Добавить();
	текСтрока.ТипДанных     = Новый ОписаниеТипов( глСоздатьМассив( пТип ), Параметры.НачальноеЗначение.КвалификаторыЧисла, Параметры.НачальноеЗначение.КвалификаторыСтроки, Параметры.НачальноеЗначение.КвалификаторыДаты );
	текСтрока.Представление = Строка( текСтрока.ТипДанных );
	текСтрока.Картинка      = пКартинка;
	текСтрока.Выбрано       = Параметры.НачальноеЗначение.СодержитТип( пТип );
	текСтрока.ЭтоГруппа     = Ложь;
	
КонецПроцедуры




&НаКлиенте
Процедура УстановитьКвалификаторы()
	
	Элементы.КвалификаторыДаты.Видимость   = Ложь;
	Элементы.КвалификаторыСтроки.Видимость = Ложь;
	Элементы.КвалификаторыЧисла.Видимость  = Ложь;
	
	текДанные = Элементы.ДеревоОбъектов.ТекущиеДанные;
	
	Если текДанные = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	текТип = текДанные.ТипДанных;
	
	// описание типа атомарно и не может состоять из нескольких типов
	
	// это число
	Если текТип.СодержитТип( Тип("Число") ) Тогда
		
		Элементы.КвалификаторыЧисла.Видимость   = Истина;
		Элементы.КвалификаторыЧисла.Доступность = текДанные.Выбрано;
		
		квалификатор = текТип.КвалификаторыЧисла;
		
		ДлинаЧисла      = квалификатор.Разрядность;
		ТочностьЧисла   = квалификатор.РазрядностьДробнойЧасти;
		Неотрицательное = (квалификатор.ДопустимыйЗнак = ДопустимыйЗнак.Неотрицательный);
		
		// это дата
	ИначеЕсли текТип.СодержитТип( Тип("Дата") ) Тогда
		
		Элементы.КвалификаторыДаты.Видимость   = Истина;
		Элементы.КвалификаторыДаты.Доступность = текДанные.Выбрано;
		
		квалификатор = текТип.КвалификаторыДаты;
		
		СоставДаты = Строка( квалификатор.ЧастиДаты );
		
		// это строка
	ИначеЕсли текТип.СодержитТип( Тип("Строка") ) Тогда
		
		Элементы.КвалификаторыСтроки.Видимость   = Истина;
		Элементы.КвалификаторыСтроки.Доступность = текДанные.Выбрано;
		
		квалификатор = текТип.КвалификаторыСтроки;
		
		ДлинаСтроки           = квалификатор.Длина;
		ДопустимаяДлинаСтроки = Строка( текТип.КвалификаторыСтроки.ДопустимаяДлина );
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоОбъектовПриАктивизацииСтроки(Элемент)
	
	ПодключитьОбработчикОжидания( "УстановитьКвалификаторы", 0.1, Истина );
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоОбъектовВыбраноПриИзменении(Элемент)
	
	УстановитьКвалификаторы();
	
	ОтработатьИзменениеФлажка();
	
	Если ОбновитьДерево Тогда
		
		ОбновитьДерево();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтработатьИзменениеФлажка()
	
	текДанные = Элементы.ДеревоОбъектов.ТекущиеДанные;
	
	Если текДанные.Выбрано Тогда
		
		ДобавитьВыбраннуюСтроку( текДанные );
		
	Иначе
		
		УдалитьВыбраннуюСтроку( текДанные );
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьВыбраннуюСтроку( пСтрокаФормы )
	
	ДобавитьИсхСтрокуВВыбранныеСтроки( пСтрокаФормы.ИДИсходнойСтроки );
	ОтображаемыеВыбранныеСтроки.Добавить( пСтрокаФормы.ПолучитьИдентификатор() );
	
	Если Не СоставнойТипДанных
		И ВыбранныеСтроки.Количество() > 1 Тогда
		
		СнятьФлажкиУОстальныхСтрок( пСтрокаФормы.ИДИсходнойСтроки );
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьИсхСтрокуВВыбранныеСтроки( ИДИсходнойСтроки )
	
	Если ИДИсходнойСтроки = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ВыбранныеСтроки.НайтиПоЗначению( ИДИсходнойСтроки ) = Неопределено Тогда
		
		ВыбранныеСтроки.Добавить( ИДИсходнойСтроки );
		
	КонецЕсли;
	
	УстановитьЗначениеВИсхСтроку( ИДИсходнойСтроки, Истина );
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьВыбраннуюСтроку( пСтрокаФормы )
	
	УдалитьИсхСтрокуИзВыбранныхСтрок( пСтрокаФормы.ИДИсходнойСтроки );
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьИсхСтрокуИзВыбранныхСтрок( ИДИсходнойСтроки )
	
	найденноеЗначение = ВыбранныеСтроки.НайтиПоЗначению( ИДИсходнойСтроки );
	
	Если Не найденноеЗначение = Неопределено Тогда
		
		ВыбранныеСтроки.Удалить( найденноеЗначение );
		
	КонецЕсли;
	
	УстановитьЗначениеВИсхСтроку( ИДИсходнойСтроки, Ложь );
	
	Если ТолькоВыбранные Тогда
		
		ОбновитьДерево = Истина;
		
	КонецЕсли;
	
КонецПроцедуры



&НаКлиенте
Процедура УстановитьЗначениеВИсхСтроку( пИД, пЗначение )
	
	строкаИсхДерева = исхДеревоОбъектов.НайтиПоИдентификатору( пИД );
	
	Если Не строкаИсхДерева = Неопределено Тогда
		
		строкаИсхДерева.Выбрано = пЗначение;
		
	КонецЕсли;
	
КонецПроцедуры




&НаКлиенте
Процедура СнятьФлажкиУОстальныхСтрок( пИсхИДСтроки )
	
	Для Каждого цИД Из ВыбранныеСтроки.ВыгрузитьЗначения() Цикл
		
		Если цИД = пИсхИДСтроки Тогда
			Продолжить;
		КонецЕсли;
		
		УдалитьИсхСтрокуИзВыбранныхСтрок( цИД );
		
	КонецЦикла;
	
	ДобавитьИсхСтрокуВВыбранныеСтроки( пИсхИДСтроки );
	
	Если ТолькоВыбранные Тогда
		
		ОтображаемыеВыбранныеСтроки.Очистить();
		ОбновитьДерево = Истина;
		
	Иначе
		
		сохрИД = Неопределено;
		
		// чтоб не перечитывать все дерево, меняем тока выбранные строки
		Для каждого цЭлемент Из ОтображаемыеВыбранныеСтроки Цикл
			
			цСтрока = ДеревоОбъектов.НайтиПоИдентификатору( цЭлемент.Значение );
			
			Если цСтрока = Неопределено Тогда
				Продолжить;
			ИначеЕсли цСтрока.ИДИсходнойСтроки = пИсхИДСтроки Тогда
				сохрИД = цЭлемент.Значение;
				Продолжить;
			КонецЕсли;
			
			цСтрока.Выбрано = Ложь;
			
		КонецЦикла;
		
		ОтображаемыеВыбранныеСтроки.Очистить();
		Если Не сохрИД = Неопределено Тогда
			
			ОтображаемыеВыбранныеСтроки.Добавить( сохрИД );
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры





&НаКлиенте
Процедура КвалификаторПриИзменении(Элемент)
	
	ЗаписатьКвалификаторы();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьКвалификаторы()
	
	текДанные = Элементы.ДеревоОбъектов.ТекущиеДанные;
	
	Если текДанные = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	текТип = текДанные.ТипДанных;
	
	// это число
	Если текТип.СодержитТип( Тип("Число") ) Тогда
		
		Если Неотрицательное Тогда
			
			допустЗнак = ДопустимыйЗнак.Неотрицательный;
			
		Иначе
			
			допустЗнак = ДопустимыйЗнак.Любой;
			
		КонецЕсли;
		
		текДанные.ТипДанных = Новый ОписаниеТипов( "Число" , Новый КвалификаторыЧисла(ДлинаЧисла, ТочностьЧисла, допустЗнак) );
		
		// это дата
	ИначеЕсли текТип.СодержитТип( Тип("Дата") ) Тогда
		
		Если СоставДаты = Строка( ЧастиДаты.Дата ) Тогда
			
			лСоставДаты = ЧастиДаты.Дата;
			
		ИначеЕсли СоставДаты = Строка( ЧастиДаты.Время ) Тогда
			
			лСоставДаты = ЧастиДаты.Время;
			
		ИначеЕсли СоставДаты = Строка( ЧастиДаты.ДатаВремя ) Тогда
			
			лСоставДаты = ЧастиДаты.ДатаВремя;
			
		КонецЕсли;
		
		текДанные.ТипДанных = Новый ОписаниеТипов("Дата", , , Новый КвалификаторыДаты( лСоставДаты ));
		
		// это строка
	ИначеЕсли текТип.СодержитТип( Тип("Строка") ) Тогда
		
		Если ДопустимаяДлинаСтроки = Строка( ДопустимаяДлина.Переменная ) Тогда
			
			лДопустимаяДлина = ДопустимаяДлина.Переменная;
			
		ИначеЕсли ДопустимаяДлинаСтроки = Строка( ДопустимаяДлина.Фиксированная ) Тогда
			
			лДопустимаяДлина = ДопустимаяДлина.Фиксированная;
			
		КонецЕсли;
		
		текДанные.ТипДанных = Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(ДлинаСтроки, лДопустимаяДлина));
		
	КонецЕсли;
	
	ЗаписатьСтрокуВИсхДерево( текДанные.ПолучитьИдентификатор() );
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьСтрокуВИсхДерево( пИДТекСтроки )
	
	записываемыеДанные = ДеревоОбъектов.НайтиПоИдентификатору( пИДТекСтроки );
	
	Если Не записываемыеДанные = Неопределено Тогда
		
		строкаПриемник = исхДеревоОбъектов.НайтиПоИдентификатору( записываемыеДанные.ИДИсходнойСтроки );
		
		ЗаполнитьЗначенияСвойств( строкаПриемник, записываемыеДанные );
		
	КонецЕсли;
	
	ОбновитьДерево();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоискТипаПриИзменении(Элемент)
	
	ОбновитьДерево();
	
КонецПроцедуры

&НаКлиенте
Процедура ТолькоВыбранныеПриИзменении(Элемент)
	
	ОбновитьДерево();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьДерево()
	
	ИДТекСтроки = Неопределено;
	
	ЗаполнитьДеревоНаФормеПоФильтру( ИДТекСтроки );
	
	// восстановление текущей строки
	
	Если ЗначениеЗаполнено( ИДТекСтроки ) Тогда
		
		Элементы.ДеревоОбъектов.ТекущаяСтрока = ИДТекСтроки;
		
	КонецЕсли;
	
	РазвернутьУровниПриНеобходимости();
	
	ОбновитьДерево = Ложь;
	
КонецПроцедуры // ОбновитьДерево()

&НаКлиенте
Процедура СоставнойТипДанныхПриИзменении(Элемент)
	
	колвоВыбранныхСтрок = ВыбранныеСтроки.Количество();
	
	Если колвоВыбранныхСтрок > 1 Тогда
		
		ИДИсхСтроки = ВыбранныеСтроки[колвоВыбранныхСтрок-1].Значение;
		
		СнятьФлажкиУОстальныхСтрок( ИДИсхСтроки );
		
	КонецЕсли;
	
	Если ОбновитьДерево Тогда
		ОбновитьДерево();
	КонецЕсли;
	
КонецПроцедуры








