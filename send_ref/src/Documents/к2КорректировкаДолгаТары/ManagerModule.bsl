#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область СлужебныеПроцедурыИФункции

#Область Проведение

Функция ДополнительныеИсточникиДанныхДляДвижений(ИмяРегистра) Экспорт

	ИсточникиДанных = Новый Соответствие;

	Возврат ИсточникиДанных; 

КонецФункции

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	////////////////////////////////////////////////////////////////////////////
	// Создадим запрос инициализации движений
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	////////////////////////////////////////////////////////////////////////////
	// Сформируем текст запроса
	
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблица_к2МногооборотнаяТараКлиента(Запрос, ТекстыЗапроса, Регистры);
	
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	ИменаРеквизитов = "Дата,Ссылка,Контрагент,Организация,ТочкаДоставки,Подразделение,ТипКорректировки";
	
	РеквизитыДокумента = глРеквизиты(ДокументСсылка, ИменаРеквизитов);
	
	Для Каждого Реквизит Из РеквизитыДокумента Цикл
		
		Запрос.УстановитьПараметр(Реквизит.Ключ, Реквизит.Значение);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2МногооборотнаяТараКлиента(Запрос, ТекстыЗапроса, Регистры) 
	
	ИмяРегистра = "к2МногооборотнаяТараКлиента";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	#Область ТекстЗапроса
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	ВЫБОР
	|		КОГДА &ТипКорректировки = ЗНАЧЕНИЕ(Перечисление.к2ТипыКорректировкиДолгаТары.УвеличениеДолга)
	|			ТОГДА ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|		ИНАЧЕ ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	|	КОНЕЦ КАК ВидДвижения,
	|	к2КорректировкаДолгаТарыТовары.Номенклатура КАК Номенклатура,
	|	к2КорректировкаДолгаТарыТовары.Количество КАК Количество,
	|	&Контрагент КАК Контрагент,
	|	&ТочкаДоставки КАК ТочкаДоставки
	|ИЗ
	|	Документ.к2КорректировкаДолгаТары.Товары КАК к2КорректировкаДолгаТарыТовары
	|ГДЕ
	|	к2КорректировкаДолгаТарыТовары.Ссылка = &Ссылка
	|	И к2КорректировкаДолгаТарыТовары.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.к2ТипыНоменклатуры.МногооборотнаяТара)";
	#КонецОбласти
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли