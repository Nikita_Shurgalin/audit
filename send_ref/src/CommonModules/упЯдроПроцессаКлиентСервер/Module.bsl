
Функция ПолучитьБизнесПроцесс( пПараметр ) Экспорт
	
	Если пПараметр = Неопределено Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	XMLТипЗнч = XMLТипЗнч(пПараметр);
	
	Если XMLТипЗнч = Неопределено Тогда
		
		Возврат Неопределено;
		
	Иначе
		
		ИмяТипа = XMLТипЗнч.ИмяТипа;
		ЭтоОбъект = СтрНайти(ИмяТипа, "Object.") > 0;
		
	КонецЕсли;
	
	Если СтрНайти(ИмяТипа, ".упБизнесПроцессы") > 0 Тогда
		
		Если ЭтоОбъект Тогда
			
			Возврат пПараметр.Ссылка;
			
		Иначе
			
			Возврат пПараметр;
			
		КонецЕсли;
		
	ИначеЕсли СтрНайти(ИмяТипа, ".упВерсии") > 0 Тогда
		
		Возврат глРеквизит_Кэш( пПараметр , "Владелец" );
		
	ИначеЕсли СтрНайти(ИмяТипа, ".упСессии") > 0
		ИЛИ СтрНайти(ИмяТипа, ".упСостояния") > 0
		ИЛИ СтрНайти(ИмяТипа, ".упПереходы") > 0 Тогда
		
		владелец = глРеквизит_Кэш( пПараметр , "Владелец" );
		
		Возврат ПолучитьБизнесПроцесс( владелец );
		
	ИначеЕсли СтрНайти(ИмяТипа, ".упУсловия") > 0
		ИЛИ СтрНайти(ИмяТипа, ".упАлгоритмы") > 0 Тогда
		
		версияБП = глРеквизит_Кэш( пПараметр , "ВерсияБП" );
		
		Возврат ПолучитьБизнесПроцесс( версияБП );
		
	ИначеЕсли СтрНайти(ИмяТипа, ".упПотоки") > 0 Тогда
		
		сессия = глРеквизит_Кэш( пПараметр , "Сессия" );
		
		Возврат ПолучитьБизнесПроцесс( сессия );
		
	ИначеЕсли СтрНайти(ИмяТипа, ".упЗадача") > 0 Тогда
		
		версияБП = глРеквизит_Кэш( пПараметр , "ВерсияБизнесПроцесса" );
		
		Возврат ПолучитьБизнесПроцесс( версияБП );
		
	Иначе
		
		Возврат Неопределено;
		
	КонецЕсли;
	
КонецФункции


Функция РазрешеноЗапускатьБизнесПроцесс( пБП ) Экспорт
	
	ТекстОшибки = "";
	
	Если Не упСистемаБезопасностиВызовСервера.ПользователюРазрешеноЗапускатьБП( упЯдроПроцессаКлиентСервер.ПолучитьБизнесПроцесс( пБП ) , ТекстОшибки ) Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюВОкно( ТекстОшибки );
		
	КонецЕсли;
	
	Возврат ПустаяСтрока( ТекстОшибки );
	
КонецФункции	//РазрешеноЗапускатьБизнесПроцесс

Функция РазрешеноЗапускатьСессию( пСессия ) Экспорт
	
	ТекстОшибки = "";
	
	Если Не ПроверитьСессиюДляЗапуска( пСессия , ТекстОшибки ) Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюВОкно( ТекстОшибки );
		
	КонецЕсли;
	
	Возврат ПустаяСтрока( ТекстОшибки );
	
КонецФункции	//РазрешеноЗапускатьБизнесПроцесс


//ЗАПУСК БП

Функция ПроверитьСессиюДляЗапуска( пСессия , пТекстОшибки = "" ) Экспорт
	
	пТекстОшибки = "";
	
	Попытка
		
		ПроверитьЗаполнение(пСессия);
		ПроверитьСессияНовая(пСессия);
		
	Исключение
		
		информацияОбОшибке = ИнформацияОбОшибке();
		
		представлениеОшибки = КраткоеПредставлениеОшибки( информацияОбОшибке );
		
		шаблонТекста = упИК.т__ЗапускБП_ОтказВЗапускеСессии();
		пТекстОшибки = стрЗаполнить( шаблонТекста, пСессия , представлениеОшибки );
		
		упЯдроПроцессаВызовСервера.Ошибка( пТекстОшибки , пСессия ,, информацияОбОшибке );
		
	КонецПопытки;
	
	Возврат ПустаяСтрока(пТекстОшибки);
	
КонецФункции	//ПроверитьСессиюДляЗапуска

Процедура ПроверитьЗаполнение( пСессия )
	
	Если Не ЗначениеЗаполнено( пСессия ) Тогда
		
		ВызватьИсключение упИК.т__Сессия_Пустая();
		
	КонецЕсли;
	
КонецПроцедуры	//ПроверитьЗаполнение

Процедура ПроверитьСессияНовая( пСессия )
	
	Если упКэшНаВызов.МассивЗадачПоСессии( пСессия ).Количество() > 0 Тогда
		
		ВызватьИсключение упИК.т__Сессия_УжеСуществуютЗадачи();
		
	КонецЕсли;
	
	Если Не Справочники.упСессии.СессияНовая( пСессия ) Тогда
		
		ВызватьИсключение упИК.т__Сессия_СтатусДолженБытьНовый();
		
	КонецЕсли;
	
КонецПроцедуры	//ПроверитьЗаполнение






//////////////////////////////////////////////////////////////////////////////////////
//ЗАПУСК БП

Функция ПолучитьНаименованиеСессииПоУмолчанию( пСессия ) Экспорт
	
	Возврат упПереопределяемыйКлиентСервер.ПолучитьНаименованиеСессииПоУмолчанию( пСессия );
	
КонецФункции

Функция ПолучитьМассивНаименованийСессииПоУмолчанию( пСессия ) Экспорт
	
	массивНаименований = Новый Массив;
	
	массивНаименований.Добавить( ПолучитьНаименованиеСессииПоУмолчанию( пСессия ) );    //
	массивНаименований.Добавить( Строка( пСессия.Владелец ) );
	массивНаименований.Добавить( Строка( глРеквизит( пСессия.Владелец , "Владелец" ) ) );
	массивНаименований.Добавить( Строка( пСессия.Источник ) );
	
	Возврат массивНаименований;
	
КонецФункции




Процедура упУстановитьКодИзБППриУстановкеНовогоКода(Источник, СтандартнаяОбработка, Префикс) Экспорт
	
	БП = упЯдроПроцессаКлиентСервер.ПолучитьБизнесПроцесс( Источник );
	
	Префикс = упКэш.ПолучитьПрефиксКодаБП( БП );
	
КонецПроцедуры
