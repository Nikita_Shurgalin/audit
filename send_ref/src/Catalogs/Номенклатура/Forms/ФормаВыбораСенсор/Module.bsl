
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Если Параметры.Свойство("ВидНоменклатуры") Тогда
		
		Элементы.ВидНоменклатуры.Видимость = Ложь;
		
		Параметры.Свойство("ВидНоменклатуры", ВидНоменклатуры);
		
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Список, 
			"ВидНоменклатуры",
			ВидНоменклатуры,
			,
			,
			ЗначениеЗаполнено(ВидНоменклатуры));
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить();
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ВидНоменклатурыПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список,
		"ВидНоменклатуры",
		ВидНоменклатуры,
		ВидСравненияКомпоновкиДанных.Равно,
		,
		ЗначениеЗаполнено(ВидНоменклатуры));
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

#КонецОбласти
