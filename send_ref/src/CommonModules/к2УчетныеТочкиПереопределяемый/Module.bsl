
#Область ПрограммныйИнтерфейс

// Процедура, вызываемая из обработчиков событий формы ПриСозданииНаСервер и ПриЧтенииНаСервере.
// См. к2УчетныеТочки.ПриЧтенииСоздании_КУТ()
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчиков событий которой происходит вызов процедуры.
//
Процедура ПриЧтенииСоздании_КУТ(Форма) Экспорт
	
	Возврат;
	
КонецПроцедуры


// Определить список используемых обработок в качестве кнопок учетных точек.
// См. Справочник.к2КнопкиУчетныхТочек.Формы.ФормаВыбораОбработок.ОпределитьСписокОбработокДляКнопокУчетныхТочек()
//
// Параметры:
//   ТаблицаОбработок - ТаблицаЗначений - список используемых обработок. Поля таблицы:
//    * ИмяОбработки           - Строка - имя обработки;
//    * ПредставлениеОбработки - Строка - синоним обработки.
//
Процедура ОпределитьСписокОбработокДляКнопокУчетныхТочек(ТаблицаОбработок) Экспорт
	
	Возврат;
	
КонецПроцедуры

// Процедура переопределяет настройки формы элемента справочника к2КнопкиУчетныхТочек.
// См. Справочники.к2КнопкиУчетныхТочек.НастроитьФорму()
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура НастроитьФорму(Форма) Экспорт
	
	Возврат;
	
КонецПроцедуры

// Процедура переопределяет связи реквизитов настроек справочника к2КнопкиУчетныхТочек.
// См. Справочники.к2КнопкиУчетныхТочек.СвязиНастроек()
//
// Параметры:
//  СтруктураСвязей - Структура - см. Справочники.к2КнопкиУчетныхТочек.СвязиНастроек()
//  НастройкиОбработки - Массив из Строка - см. Справочники.к2КнопкиУчетныхТочек.НастройкиОбработки()
//  Реквизиты - КоллекцияОбъектовМетаданных - коллекция реквизитов справочника
//  ТабличныеЧасти - КоллекцияОбъектовМетаданных - коллекция табличных частей справочника
//
Процедура СвязиНастроек(СтруктураСвязей, НастройкиОбработки, Реквизиты, ТабличныеЧасти) Экспорт
	
	Возврат;
	
КонецПроцедуры

// Процедура переопределяет параметры используемые обработкой. См. Справочники.к2КнопкиУчетныхТочек
//
// Параметры:
//  ИмяОбработки - Строка - Имя обработки с параметрами
//  МассивПараметров - Массив из Строка - массив имен параметров КУТ
//
Процедура МассивИспользуемыхПараметров(ИмяОбработки, МассивПараметров) Экспорт
	
	Возврат;
	
КонецПроцедуры

#КонецОбласти