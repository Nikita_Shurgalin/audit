#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает ссылку по структуре описания
//
// Параметры:
//  пНаименование - Строка - особые отметки
//  пСоздаватьЭлемент - Булево - создавать ли элемент в случае отсутствия
//
// Возвращаемое значение:
//  СправочникСсылка.к2ОсобыеОтметки_Меркурий - Пустая ссылка, если передана пустая строка.
//
Функция ПолучитьПоОписанию(Знач пНаименование, Знач пСоздаватьЭлемент = Истина) Экспорт

	Если Не ЗначениеЗаполнено(пНаименование) Тогда

		Возврат Справочники.к2ОсобыеОтметки_Меркурий.ПустаяСсылка();

	КонецЕсли;

	хеш = к2ОбщегоНазначения_Меркурий.КонтрольнаяСуммаСтроки(СокрЛП(НРег(пНаименование)));

	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
				   |	к2ОсобыеОтметки_Меркурий.Ссылка КАК Ссылка
				   |ИЗ
				   |	Справочник.к2ОсобыеОтметки_Меркурий КАК к2ОсобыеОтметки_Меркурий
				   |ГДЕ
				   |	к2ОсобыеОтметки_Меркурий.хеш = &хеш
				   |	И НЕ к2ОсобыеОтметки_Меркурий.ПометкаУдаления";
	Запрос.УстановитьПараметр("хеш", хеш);

	выборка = Запрос.Выполнить().Выбрать();

	Если выборка.Следующий() Тогда

		Возврат выборка.Ссылка;

	ИначеЕсли Не пСоздаватьЭлемент Тогда

		Возврат Справочники.к2ОсобыеОтметки_Меркурий.ПустаяСсылка();

	Иначе

		об = Справочники.к2ОсобыеОтметки_Меркурий.СоздатьЭлемент();
		об.Наименование = пНаименование;
		об.НаименованиеПолное = пНаименование;
		об.Записать();

		Возврат об.Ссылка;

	КонецЕсли;

КонецФункции

// Заполняет описания вложенных объектов
//
// Параметры:
//  пОписание - Структура - текущее описание
//  пПараметрыОбновления - Структура - Структура сопроводительных данных
Процедура ЗаполнитьОписанияВложенныхОбъектов(Знач пОписание, пПараметрыОбновления) Экспорт

	Возврат;

КонецПроцедуры

#КонецОбласти

#КонецЕсли