#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	Если Не Параметры.Свойство("ОсновнойМатериал") Тогда 
		Возврат;
	КонецЕсли;
	
	СтандартнаяОбработка = Ложь;
	
	МассивСпискаВыбора = Новый Массив;
	МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.НеРаспределять);
	МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоНормеСерииПоСреднему);
	МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоНормеСерииПоФЕФО);
	МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоНормеСерииПоФИФО);
	
	Если Параметры.ОсновнойМатериал Тогда
		
		МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоНормеСерииВПорядкеВыбора);
		
		Если Параметры.Свойство("ИмяОбработкиКнопкиУчетныхТочек") Тогда
			
			Если Параметры.ИмяОбработкиКнопкиУчетныхТочек = Метаданные.Обработки.к2ОперативныйУчетНаРабочемЦентре.Имя Тогда
				
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоВыпуску);
				
			Иначе
				
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоНормеСерииПоВыпуску);
				
			КонецЕсли;
			
			Если Параметры.ИмяОбработкиКнопкиУчетныхТочек = Метаданные.Обработки.к2РаботаСЗаданиями.Имя Тогда
				
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоОстатку);
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоВыпускуСерииПоФЕФО);
				
			КонецЕсли;
			
		Иначе
			
			МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоВыпуску);
			МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериалов.КоличествоПоНормеСерииПоВыпуску);
			
		КонецЕсли;
		
	КонецЕсли;
	
	ДанныеВыбора = Новый СписокЗначений;
	ДанныеВыбора.ЗагрузитьЗначения(МассивСпискаВыбора);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли