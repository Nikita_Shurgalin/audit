#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Движения.к2ЗарплатаПоОперациям.Отразить(ДополнительныеСвойства.ТаблицыДляДвижений.Таблица_к2ЗарплатаПоОперациям, Отказ);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
