#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Функция СтруктураДанныхПоУмолчанию() Экспорт
	
	Структура = Новый Структура;
	Структура.Вставить("Период", к2ОбменДаннымиИБКлиентСервер.ПустаяДата());
	Структура.Вставить("ПакетДанныхУИД", "");
	Структура.Вставить("Статус", Перечисления.к2СтатусыВходящихПакетовОбменДаннымиИБ.ПустаяСсылка());
	Структура.Вставить("ТекстСообщения", "");
	Структура.Вставить("ХешОбъектаИБ", "");
	Структура.Вставить("ОбъектИБ", Неопределено);
	Структура.Вставить("Правило", Справочники.к2ПравилаВыгрузкиОбменДаннымиИБ.ПустаяСсылка());
	Структура.Вставить("ВремяОбработки", 0);
	Структура.Вставить("ПодробноеПредставлениеОшибки", "");
	
	Возврат Структура;
	
КонецФункции

Процедура ДобавитьЗапись(СтруктураДанных) Экспорт
	
	МенеджерЗаписи = СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Период = СтруктураДанных.Период;
	МенеджерЗаписи.ПакетДанныхУИД = СтруктураДанных.ПакетДанныхУИД;
	МенеджерЗаписи.Статус = СтруктураДанных.Статус;
	МенеджерЗаписи.ТекстСообщения = СтруктураДанных.ТекстСообщения;
	МенеджерЗаписи.ХешОбъектаИБ = СтруктураДанных.ХешОбъектаИБ;
	МенеджерЗаписи.ОбъектИБ = СтруктураДанных.ОбъектИБ;
	МенеджерЗаписи.Правило = СтруктураДанных.Правило;
	МенеджерЗаписи.ВремяОбработки = СтруктураДанных.ВремяОбработки;
	МенеджерЗаписи.ПодробноеПредставлениеОшибки = СтруктураДанных.ПодробноеПредставлениеОшибки;
	
	МенеджерЗаписи.Записать(Истина);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
