#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	ПлановаяДатаПеремещения = ТекущаяДатаСеанса();
	
	УстановитьВидимостьДоступность();
	
	УстановитьЗаголовок();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если Не глРеквизит(СкладПолучатель, "ИспользоватьАдресноеХранение") Тогда 
		МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаПолучатель");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СкладПолучательПриИзменении(Элемент)
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаОК(Команда)
	
	Если Не ПроверитьЗаполнение() Тогда 
		Возврат;
	КонецЕсли;
	
	Структура = Новый Структура;
	Структура.Вставить("ПлановаяДатаПеремещения", ПлановаяДатаПеремещения);
	Структура.Вставить("СкладПолучатель", СкладПолучатель);
	Структура.Вставить("ЯчейкаПолучатель", ЯчейкаПолучатель);
	
	Закрыть(Структура);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.ЯчейкаПолучатель.Видимость = глРеквизит(СкладПолучатель, "ИспользоватьАдресноеХранение");
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗаголовок()
	
	Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Переместить: %1'"),
		стрСобрать(Параметры.МассивТар));
	
КонецПроцедуры

#КонецОбласти