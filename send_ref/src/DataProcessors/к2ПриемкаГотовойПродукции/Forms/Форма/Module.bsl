
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();

	УстановитьУсловноеОформление();
	УстановитьВидимостьДоступность();
	НастроитьДинамическиеСписки();
	
	Ответственный = глТекущийПользователь();
	Организация = к2Документы.ОрганизацияПоУмолчанию();
	РабочееМесто = ПараметрыСеанса.РабочееМестоКлиента;
	РабочийЦентр = глРеквизит(РабочееМесто, "к2РабочийЦентр");
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	УстановитьВидимостьДоступность();
	НастроитьДинамическиеСписки();
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если Не глРеквизит(Склад, "ИспользоватьАдресноеХранение") Тогда
		МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаПриемки");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(, ЭтотОбъект, "СканерШтрихкода");

	ИнициализироватьДокумент();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" Тогда
			
			ШтрихКод = Параметр[0];
			
			ОбработатьШтрихкод(ШтрихКод);
			
		КонецЕсли;
	КонецЕсли;
			
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(, ЭтотОбъект);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПериодПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(СписокДокументов, "НачалоПериода", Период.ДатаНачала);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(СписокДокументов, "КонецПериода", Период.ДатаОкончания);
	
КонецПроцедуры

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	УстановитьВидимостьДоступность();
	ПриемкаОбъект.Склад = Склад;
	ЯчейкаПриемки = глРеквизит(Склад, "ЯчейкаПриемки");
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокДокументов, "Склад", Склад, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Склад));

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыУпаковочныйЛистТовары

&НаКлиенте
Процедура УпаковочныйЛистТоварыПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Отказ = Истина;
	
	Если ПроверитьЗаполнение() Тогда
		
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("РабочийЦентр", РабочийЦентр);
		ПараметрыФормы.Вставить("ВыбиратьШаблонЭтикетки", Ложь);
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ВыборНоменклатурыЗавершение", ЭтотОбъект);
		
		ОткрытьФорму("Справочник.к2НастройкаКнопокНоменклатуры.Форма.ФормаВыбораНоменклатуры",
			ПараметрыФормы,
			ЭтотОбъект,
			,
			,
			,
			ОписаниеОповещения,
			РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПодтвердитьПриемку(Команда)
	
	Если Не ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(УпаковочныйЛист) Тогда
		
		ПоказатьВопрос(
			Новый ОписаниеОповещения("ВопросПодтверждениеБезУпаковочногоЛистаЗавершение", ЭтотОбъект),
			НСтр("ru = 'Принять на склад без упаковочного листа?'"),
			РежимДиалогаВопрос.ДаНетОтмена);
			
	Иначе
		
		Приемка = ПодтвердитьПриемкуСервер();
		
		Если ЗначениеЗаполнено(Приемка) Тогда
			ИнициализироватьДокумент();
		КонецЕсли;

	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ПечатьПаспортПаллеты(Команда)
	
	Если Не ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	Приемка = ПодтвердитьПриемкуСервер();
	
	Если ЗначениеЗаполнено(Приемка) И Не ЗначениеЗаполнено(УпаковочныйЛист) Тогда
		УпаковочныйЛист = СформироватьУпаковочныйЛистСервер();
		Для Каждого Строка Из ПриемкаОбъект.Товары Цикл
			Строка.УпаковочныйЛист = УпаковочныйЛист;
		КонецЦикла;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УпаковочныйЛист) Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Документ.к2УпаковочныйЛист",
			"ПаспортПаллеты",
			УпаковочныйЛист,
			ЭтотОбъект);
		
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеРеквизитовФормы

&НаСервере
Процедура УстановитьУсловноеОформление()

КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.ЯчейкаПриемки.Видимость = глРеквизит(Склад, "ИспользоватьАдресноеХранение");
	
КонецПроцедуры

&НаСервере
Процедура НастроитьДинамическиеСписки()
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(СписокДокументов, "НачалоПериода", Период.ДатаНачала);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(СписокДокументов, "КонецПериода", Период.ДатаОкончания);
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(СписокДокументов, "Склад", Склад, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Склад));
	
КонецПроцедуры

&НаКлиенте
Процедура ВыборНоменклатурыЗавершение(Результат, ДополнительныеПараметры) Экспорт

	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
		
	НоваяСтрока = ПриемкаОбъект.Товары.Добавить();
	НоваяСтрока.Ячейка = ЯчейкаПриемки;
	
	ЗаполнитьЗначенияСвойств(НоваяСтрока, Результат);
	
	ПараметрыКалькулятора = к2ПроизводствоКлиентСервер.СтруктураПараметровКалькуляторСенсор();
	
	ПараметрыКалькулятора.Тип = ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Число");
	ПараметрыКалькулятора.ДлинаЧисла = 15;
	ПараметрыКалькулятора.ТочностьЧисла = 3;
	ПараметрыКалькулятора.Заголовок = НСтр("ru = 'Введите количество'");
	
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ТекущаяСтрока", НоваяСтрока);
	
	ОткрытьФорму("ОбщаяФорма.к2КалькуляторСенсор",
		ПараметрыКалькулятора,
		ЭтотОбъект,
		,
		,
		,
		Новый ОписаниеОповещения("ВводКоличестваЗавершение", ЭтотОбъект, ДополнительныеПараметры),
		РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		
КонецПроцедуры
	
&НаКлиенте
Процедура ВводКоличестваЗавершение(Результат, ДополнительныеПараметры) Экспорт

	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ДополнительныеПараметры.ТекущаяСтрока.КоличествоУпаковок = Результат.Результат;

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ДополнительныеПараметры.ТекущаяСтрока, СтруктураДействий, Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура ВопросПодтверждениеБезУпаковочногоЛистаЗавершение(Результат, ДополнительныеПараметры) Экспорт

	Если Результат <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	Приемка = ПодтвердитьПриемкуСервер();
	
	Если ЗначениеЗаполнено(Приемка) Тогда
		ИнициализироватьДокумент();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработкаШтрихКода

&НаКлиенте
Процедура ОбработатьШтрихкод(Штрихкод)
	
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(Штрихкод);
	
	Если ПараметрыШтрихкода.Результат = Неопределено Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(ПараметрыШтрихкода.ТекстОшибки);
		Возврат;
	КонецЕсли;
	
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(Штрихкод);

	ОбработатьПараметрыШтрихкодаСервер(ПараметрыШтрихкода);
		
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Функция ПодтвердитьПриемкуСервер()

	ДокументОбъект = РеквизитФормыВЗначение("ПриемкаОбъект");
		
	к2ПроведениеСервер.ПровестиИлиЗаписатьДокумент(ДокументОбъект);
	
	ЗначениеВРеквизитФормы(ДокументОбъект, "ПриемкаОбъект");
	
	Возврат ДокументОбъект.Ссылка;
	
КонецФункции

&НаСервере
Функция СформироватьУпаковочныйЛистСервер()

	ДокументОбъект = Документы.к2УпаковочныйЛист.СоздатьДокумент();
	
	ДокументОбъект.Заполнить(ПриемкаОбъект.Ссылка);
	ДокументОбъект.Организация = Организация;
	
	к2ПроведениеСервер.ПровестиИлиЗаписатьДокумент(ДокументОбъект);
	
	Возврат ДокументОбъект.Ссылка;
	
КонецФункции

&НаСервере
Процедура ИнициализироватьДокумент()
	
	ДокументОбъект = Документы.к2РаспоряжениеНаПриемку.СоздатьДокумент();
	
	ДокументОбъект.Заполнить(Неопределено);
	
	ДокументОбъект.Склад = Склад;
	ДокументОбъект.СпособСоздания = ПредопределенноеЗначение("Перечисление.к2СпособыСозданияДокументов.ПриемкаГотовойПродукции");
	ДокументОбъект.Дата = ТекущаяДатаСеанса();
	ДокументОбъект.Ответственный = Ответственный;
	
	ЗначениеВРеквизитФормы(ДокументОбъект, "ПриемкаОбъект");
	
	УпаковочныйЛист = Документы.к2УпаковочныйЛист.ПустаяСсылка();
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьПараметрыШтрихкодаСервер(ПараметрыШтрихкода)
	
	СтруктураДанныхШтрихкода = к2ШтрихкодыУпаковок.ДанныеПоПараметрамШтрихкода(ПараметрыШтрихкода);
	
	Если ЗначениеЗаполнено(СтруктураДанныхШтрихкода.ТекстОшибки) Тогда 
		ОбщегоНазначения.СообщитьПользователю(СтруктураДанныхШтрихкода.ТекстОшибки);
		Возврат;
	КонецЕсли;
	
	ТекущийУпаковочныйЛист = Документы.к2УпаковочныйЛист.ПустаяСсылка();
	
	Для Каждого Строка Из СтруктураДанныхШтрихкода.ТаблицаНоменклатуры Цикл
		
		НоваяСтрока = ПриемкаОбъект.Товары.Добавить();
		НоваяСтрока.Ячейка = ЯчейкаПриемки;
		
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
		
		ТекущийУпаковочныйЛист = Строка.УпаковочныйЛист;
		
	КонецЦикла;
	
	Если ЗначениеЗаполнено(УпаковочныйЛист) Тогда
		УпаковочныйЛист = Документы.к2УпаковочныйЛист.ПустаяСсылка();
	ИначеЕсли ПриемкаОбъект.Товары.Количество() = СтруктураДанныхШтрихкода.ТаблицаНоменклатуры.Количество() Тогда
		УпаковочныйЛист = ТекущийУпаковочныйЛист;
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#КонецОбласти
