
#Область ПрограммныйИнтерфейс

#Область РаботаСДеревьями

Процедура ПересчитатьПоказателиВКроссДеревеПоИерархии(пФорма, пДерево, Знач пМассивПрефиксов) Экспорт
	
	массивПрефиксов = к2КоллекцииКлиентСервер.ОбернутьВМассивПриНеобходимости(пМассивПрефиксов);
	
	массивКолонок = Новый Массив;
	
	Для Каждого ИмяКолонки Из массивПрефиксов Цикл
		
		Для Каждого СтрокаПериода Из пФорма.Периоды.НайтиСтроки(Новый Структура("Активная", Истина)) Цикл
			
			массивКолонок.Добавить(ИмяКолонки + "_" + СтрокаПериода.ИмяКолонки);
			
		КонецЦикла;
		
	КонецЦикла;
	
	Для Каждого цСтрокаДерева Из пДерево.ПолучитьЭлементы() Цикл
		
		ПересчитатьИтогиВнизОтСтрокиКроссДереваПоИерархии(цСтрокаДерева, массивКолонок);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ПересчитатьИтогиВнизОтСтрокиКроссДереваПоИерархии(пСтрокаДерева, Знач пМассивКолонок) Экспорт
	
	строкиДерева = пСтрокаДерева.ПолучитьЭлементы();
	
	Если строкиДерева.Количество() = 0 Тогда
		
		Возврат;
		
	КонецЕсли;
	
	пМассивКолонок = к2КоллекцииКлиентСервер.ОбернутьВМассивПриНеобходимости(пМассивКолонок);
	
	суммы = Новый Соответствие;
	
	Для каждого цСтрокаДерева Из строкиДерева Цикл
		
		ПересчитатьИтогиВнизОтСтрокиКроссДереваПоИерархии(цСтрокаДерева, пМассивКолонок);
		
		Для каждого цКолонка Из пМассивКолонок Цикл
			
			к2КоллекцииКлиентСервер.ДобавитьВСчетчик(суммы, цКолонка, цСтрокаДерева[цКолонка]);
			
		КонецЦикла;
		
	КонецЦикла;
	
	Для каждого цКолонка Из пМассивКолонок Цикл
		
		пСтрокаДерева[цКолонка] = суммы[цКолонка];
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти