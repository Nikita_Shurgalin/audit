
Процедура ОткрытьФормуПоНастройке( пНастройка ) Экспорт
	
	Если Не ЗначениеЗаполнено( пНастройка ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	структНастроек = глСтруктураРеквизитов( пНастройка , "КлючОбъекта,КлючНастроек" );
	
	КлючНазначенияИспользования = структНастроек.КлючНастроек;
	ИмяФормы = структНастроек.КлючОбъекта;
	
	ПараметрыФормы = Новый Структура( "КлючНазначенияИспользования" , КлючНазначенияИспользования );
	ОткрытьФорму( ИмяФормы , ПараметрыФормы );
	
КонецПроцедуры	//ОткрытьФормуПоНастройке

Процедура ОткрытьФормуПоЭлементуСтатистики( пЭлементСтатистики , пОкно = Неопределено ) Экспорт
	
	Если Не ЗначениеЗаполнено( пЭлементСтатистики ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура( "ЭлементСтатистики" , пЭлементСтатистики );
	
	Если глРеквизит_Кэш(пЭлементСтатистики, "ИмяТаблицыМетаданных") = упИК.ИмяТаблицыМетаданныхСтатистикиЗадачи() Тогда
		
		ОткрытьФорму("Задача.упЗадача.ФормаСписка" , ПараметрыФормы , , , пОкно );
		
	ИначеЕсли глРеквизит_Кэш(пЭлементСтатистики, "ИмяТаблицыМетаданных") = упИК.ИмяТаблицыМетаданныхСтатистикиСессии() Тогда	
		
		ОткрытьФорму("Справочник.упСессии.ФормаСписка" , ПараметрыФормы , , , пОкно );
		
	Иначе
				
	КонецЕсли;
		
КонецПроцедуры	//ОткрытьФормуПоЭлементуСтатистики



Процедура УстановитьСтандартнуюНастройку( пФорма, пДинамическиеСписки = "Список" ) Экспорт
	
	ОчиститьУсловноеОформлениеФорме( пФорма, пДинамическиеСписки );
	
	Если пФорма.Параметры.Свойство( "ОтключитьАвтосохранение" ) Тогда
		пФорма.Параметры.ОтключитьАвтосохранение = Истина;
	КонецЕсли;
	
	Если пФорма.Параметры.Свойство( "ЗагрузитьНастройкиПоУмолчанию" ) Тогда
		пФорма.Параметры.ЗагрузитьНастройкиПоУмолчанию = Истина;
	КонецЕсли;
	
	Если пФорма.Параметры.Свойство( "ЭлементСтатистики" ) Тогда
		пФорма.Параметры.ЭлементСтатистики = Неопределено;
	КонецЕсли;
	
	упНастройкиВызовСервера.УдалитьСистемныеНастройкиФормы( пФорма.ИмяФормы, глТекущийПользователь() );
	
	пФорма.ЗагрузитьНастройкиПоУмолчанию();
	
	Если Не пФорма.Открыта() Тогда
		
		#Если Не ТолстыйКлиентОбычноеПриложение Тогда
			ПерейтиПоНавигационнойСсылке( упПереопределяемыйКлиентСервер.НавСсылка__ПодсистемаКСУП() );
		#КонецЕсли
		
	КонецЕсли;
	
	пФорма.ОбновитьОтображениеДанных();
	
КонецПроцедуры

Процедура ОчиститьУсловноеОформлениеФорме( пФорма, пДинамическиеСписки )
	
	Для Каждого цИмяРеквизита Из стрРазбить( пДинамическиеСписки ) Цикл
		
		пФорма[цИмяРеквизита].УсловноеОформление.Элементы.Очистить();
		
	КонецЦикла;
	
КонецПроцедуры



