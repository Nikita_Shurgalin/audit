
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения; // используется механизмом обработки изменения реквизитов ТЧ

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьУсловноеОформление();
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	ВыполняемаяОперация = "";
	
	Если ВыбранноеЗначение.Свойство("ВыполняемаяОперация", ВыполняемаяОперация) И ВыполняемаяОперация = "ВыборСтрокиВыпуска" Тогда
		
		ТекущиеДанные = ТекущийЭлемент.ТекущиеДанные;
		
		Если ТекущиеДанные = Неопределено Тогда
			Возврат;
		КонецЕсли;
		
		Модифицированность = Истина;
		
		ЗаполнитьЗначенияСвойств(ТекущиеДанные, ВыбранноеЗначение.ЗначениеВыбора);
		
		ТекущийЭлемент.ЗакончитьРедактированиеСтроки(Ложь);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если УказыватьСкладВТаблицеВыпуск
		Или Объект.Статус = Перечисления.к2СтатусыПереработки.Начат
		Или Объект.Статус = Перечисления.к2СтатусыПереработки.ВПроцессе Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("СкладВыпуск");
		МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаВыпуск");
		
	Иначе
		
		Если Не СкладВыпуск.ИспользоватьАдресноеХранение Тогда
			МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаВыпуск");
		КонецЕсли;
				
	КонецЕсли;
	
	Если УказыватьСкладВТаблицеМатериалы Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("СкладМатериалы");
		МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаМатериалы");
		
	Иначе
		
		Если Объект.РаспределениеМатериалов.Количество() = 0 Тогда
			
			МассивНепроверяемыхРеквизитов.Добавить("СкладМатериалы");
			МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаМатериалы");
			
		КонецЕсли;
		
		Если Не СкладМатериалы.ИспользоватьАдресноеХранение Тогда
			МассивНепроверяемыхРеквизитов.Добавить("ЯчейкаМатериалы");
		КонецЕсли;
		
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
	ЗаполнитьСлужебныеРеквизиты();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура РабочийЦентрПриИзменении(Элемент)
	
	РабочийЦентрПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СкладМатериалыПриИзменении(Элемент)
	
	СкладМатериалыПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЯчейкаМатериалыПриИзменении(Элемент)
	
	ЯчейкаМатериалыПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СкладВыпускПриИзменении(Элемент)
	
	СкладВыпускПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЯчейкаВыпускПриИзменении(Элемент)
	
	ЯчейкаВыпускПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий");
	
КонецПроцедуры

&НаКлиенте
Процедура ВидОперацииПриИзменении(Элемент)
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура УказыватьСкладВТаблицеМатериалыПриИзменении(Элемент)
	
	УказыватьСкладМатериалыВТаблицеПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура УказыватьСкладВТаблицеВыпускПриИзменении(Элемент)
	
	УказыватьСкладВыпускВТаблицеПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыВыпуск

&НаКлиенте
Процедура ВыпускПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Выпуск.ТекущиеДанные;
	
	Если Не ТекущиеДанные = Неопределено Тогда
		
		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ЗаполнитьАналитикуУчетаНоменклатуры");
		
		к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
		
	КонецЕсли;
	
	ВыпускПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыпускНоменклатураПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Выпуск.ТекущиеДанные;
	
	СтруктураХарактеристики = к2ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияХарактеристики(Объект.РабочийЦентр);
	СтруктураСпецификации = к2ОбработкаТабличнойЧастиКлиентСервер.СтруктураЗаполненияСпецификации(Объект.РабочийЦентр, Объект.Дата);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьХарактеристикуПоРабочемуЦентруЭтапаСпецификации", СтруктураХарактеристики);
	СтруктураДействий.Вставить("ПроверитьСериюПоВидуНоменклатуры", ТекущиеДанные.Серия);
	СтруктураДействий.Вставить("ЗаполнитьВариантИспользованияДополнительныхЕдиницИзмерения", ТекущиеДанные.Номенклатура);
	СтруктураДействий.Вставить("ЗаполнитьСпецификациюДляСтроки", СтруктураСпецификации);
	СтруктураДействий.Вставить("ПроверитьЭтапПоВладельцуВыходномуИзделию", ТекущиеДанные.Этап);
	СтруктураДействий.Вставить("ЗаполнитьКоличествоВыходногоИзделия", ТекущиеДанные.Количество);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыпускХарактеристикаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Выпуск.ТекущиеДанные;
	
	СтруктураСпецификации = к2ОбработкаТабличнойЧастиКлиентСервер.СтруктураЗаполненияСпецификации(Объект.РабочийЦентр, Объект.Дата);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьСпецификациюДляСтроки", СтруктураСпецификации);
	СтруктураДействий.Вставить("ПроверитьЭтапПоВладельцуВыходномуИзделию", ТекущиеДанные.Этап);
	СтруктураДействий.Вставить("ЗаполнитьКоличествоВыходногоИзделия", ТекущиеДанные.Количество);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыпускСпецификацияПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Выпуск.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьЭтапПоВладельцуВыходномуИзделию", ТекущиеДанные.Этап);
	СтруктураДействий.Вставить("ЗаполнитьКоличествоВыходногоИзделия", ТекущиеДанные.Количество);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыпускСкладПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Выпуск.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьИспользоватьАдресноеХранениеЗаполнитьЯчейку", ТекущиеДанные.Склад);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыРаспределениеМатериалов

&НаКлиенте
Процедура РаспределениеМатериаловПриИзменении(Элемент)
	
	РаспределениеМатериаловПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловПередУдалением(Элемент, Отказ)
	
	ТекущиеДанные = Элементы.РаспределениеМатериалов.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Структура = Новый Структура;
	Структура.Вставить("КлючСвязиМатериалов", ТекущиеДанные.КлючСвязи);
	
	МассивУдаляемыхСтрок = Объект.МатериалыНаТарахВыпуска.НайтиСтроки(Структура);
	
	Для Каждого Строка Из МассивУдаляемыхСтрок Цикл
		Объект.МатериалыНаТарахВыпуска.Удалить(Строка);
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловНоменклатураПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.РаспределениеМатериалов.ТекущиеДанные;

	СтруктураХарактеристики = к2ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияХарактеристики(Объект.РабочийЦентр, Истина);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьХарактеристикуПоРабочемуЦентруЭтапаСпецификации", СтруктураХарактеристики);
	СтруктураДействий.Вставить("ПроверитьСериюПоВидуНоменклатуры", ТекущиеДанные.Серия);
	СтруктураДействий.Вставить("ЗаполнитьВариантИспользованияДополнительныхЕдиницИзмерения", ТекущиеДанные.Номенклатура);
	СтруктураДействий.Вставить("ПроверитьКорректностьЗаполнитьСтатьюЗатратПоНоменклатуре");
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловНоменклатураОригиналПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.РаспределениеМатериалов.ТекущиеДанные;
	
	СтруктураХарактеристики = Новый Структура("Характеристика, Суффикс", ТекущиеДанные.ХарактеристикаОригинал, "Оригинал");
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьХарактеристикуПоВладельцуСуффикс", СтруктураХарактеристики);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
	Если Не ТекущиеДанные = Неопределено И Не ЗначениеЗаполнено(ТекущиеДанные.НоменклатураОригинал) Тогда
		ТекущиеДанные.КоличествоОригинал = Неопределено;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловСтрокаВыпускаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОткрытьФормаВыбораСтрокиВыпуска(Элемент, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловСтрокаВыпускаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.РаспределениеМатериалов.ТекущаяСтрока;
	
	Если ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	РаспределениеМатериаловСтрокаВыпускаЗаполнить(ТекущаяСтрока);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	Если Не Объект.Выпуск.Количество() = 1 Тогда
		Возврат;
	КонецЕсли;
	
	ЗаполнитьКлючСвязиПоЕдинственнойСтрокеВыпуска(Элементы.РаспределениеМатериалов.ТекущаяСтрока);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловСкладПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.РаспределениеМатериалов.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьИспользоватьАдресноеХранениеЗаполнитьЯчейку", ТекущиеДанные.Склад);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаЗаполнитьЗначениямиДляМатериалов(Команда)
	
	СкладВыпуск = СкладМатериалы;
	ЯчейкаВыпуск = ЯчейкаМатериалы;
	
	СкладВыпускПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЗаполнить(Команда)
	
	ОткрытьФормуНастройкиЗаполненияМатериалов();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область СтандартныеПодсистемы

#Область ПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Объект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
	
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Объект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#КонецОбласти

#Область УсловноеОформление

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	к2УО.Характеристика(ЭтотОбъект
		, "РаспределениеМатериаловХарактеристика", "Объект.РаспределениеМатериалов.ХарактеристикиИспользуются");
		
	к2УО.Характеристика(ЭтотОбъект
		, "РаспределениеМатериаловХарактеристикаОригинал", "Объект.РаспределениеМатериалов.ХарактеристикиИспользуютсяОригинал");
		
	к2УО.Серия(ЭтотОбъект
		, "РаспределениеМатериаловСерия", "Объект.РаспределениеМатериалов.СерииИспользуются");
	
	к2УО.Характеристика(ЭтотОбъект, "ВыпускХарактеристика", "Объект.Выпуск.ХарактеристикиИспользуются");
	к2УО.Серия(ЭтотОбъект, "ВыпускСерия", "Объект.Выпуск.СерииИспользуются");
	
	к2УО.Характеристика(ЭтотОбъект, "МатериалыНаТарахВыпускаХарактеристика", "Объект.МатериалыНаТарахВыпуска.ХарактеристикиИспользуются");
	к2УО.Серия(ЭтотОбъект, "МатериалыНаТарахВыпускаСерия", "Объект.МатериалыНаТарахВыпуска.СерииИспользуются");
	
	НоменклатураСервер.УстановитьУсловноеОформлениеПолейКоличеств(ЭтотОбъект, СтруктураДанныхДляОформленияКоличеств());
	СкладыСервер.УстановитьУсловноеОформлениеПоляСклада(ЭтотОбъект, СтруктураДанныхДляОформленияСклада());
	
	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.РаспределениеМатериаловСтрокаВыпуска.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.РаспределениеМатериалов.ОшибкаСоответствияСтрокеВыпуска");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.к2ПросроченныйДокумент);
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<не найдена, выберите строку выпуска>'"));
	
КонецПроцедуры

&НаСервере
Функция СтруктураДанныхДляОформленияКоличеств()
	
	ОформляемыеПоля = Новый Массив;
	ОформляемыеПоля.Добавить("РаспределениеМатериаловКоличество1");
	ОформляемыеПоля.Добавить("РаспределениеМатериаловКоличество2");
	ОформляемыеПоля.Добавить("РаспределениеМатериаловНоменклатураЕдиницаИзмерения1");
	ОформляемыеПоля.Добавить("РаспределениеМатериаловНоменклатураЕдиницаИзмерения2");
	ОформляемыеПоля.Добавить("ВыпускКоличество1");
	ОформляемыеПоля.Добавить("ВыпускКоличество2");
	ОформляемыеПоля.Добавить("ВыпускНоменклатураЕдиницаИзмерения1");
	ОформляемыеПоля.Добавить("ВыпускНоменклатураЕдиницаИзмерения2");
	ОформляемыеПоля.Добавить("МатериалыНаТарахВыпускаКоличество1");
	ОформляемыеПоля.Добавить("МатериалыНаТарахВыпускаКоличество2");
	ОформляемыеПоля.Добавить("МатериалыНаТарахВыпускаНоменклатураЕдиницаИзмерения1");
	ОформляемыеПоля.Добавить("МатериалыНаТарахВыпускаНоменклатураЕдиницаИзмерения2");
		
	СоответствиеПутейКДаннымПустогоЗначения = Новый Соответствие;
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("РаспределениеМатериаловКоличество1", "Объект.РаспределениеМатериалов.Количество1");
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("РаспределениеМатериаловКоличество2", "Объект.РаспределениеМатериалов.Количество2");
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("ВыпускКоличество1", "Объект.Выпуск.Количество1");
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("ВыпускКоличество2", "Объект.Выпуск.Количество2");
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("МатериалыНаТарахВыпускаКоличество1", "Объект.МатериалыНаТарахВыпуска.Количество1");
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("МатериалыНаТарахВыпускаКоличество2", "Объект.МатериалыНаТарахВыпуска.Количество2");
	
	СоответствиеПредставленийПустогоЗначения = Новый Соответствие;
	СоответствиеПредставленийПустогоЗначения.Вставить("РаспределениеМатериаловКоличество1", НСтр("ru = '<Количество 1>'"));
	СоответствиеПредставленийПустогоЗначения.Вставить("РаспределениеМатериаловКоличество2", НСтр("ru = '<Количество 2>'"));
	СоответствиеПредставленийПустогоЗначения.Вставить("ВыпускКоличество1", НСтр("ru = '<Количество 1>'"));
	СоответствиеПредставленийПустогоЗначения.Вставить("ВыпускКоличество2", НСтр("ru = '<Количество 2>'"));
	СоответствиеПредставленийПустогоЗначения.Вставить("МатериалыНаТарахВыпускаКоличество1", НСтр("ru = '<Количество 1>'"));
	СоответствиеПредставленийПустогоЗначения.Вставить("МатериалыНаТарахВыпускаКоличество2", НСтр("ru = '<Количество 2>'"));
	
	СоответствиеПутейКДанным = Новый Соответствие;
	СоответствиеПутейКДанным.Вставить("РаспределениеМатериаловКоличество1"
		, "Объект.РаспределениеМатериалов.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("РаспределениеМатериаловКоличество2"
		, "Объект.РаспределениеМатериалов.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("РаспределениеМатериаловНоменклатураЕдиницаИзмерения1"
		, "Объект.РаспределениеМатериалов.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("РаспределениеМатериаловНоменклатураЕдиницаИзмерения2"
		, "Объект.РаспределениеМатериалов.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("ВыпускКоличество1"
		, "Объект.Выпуск.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("ВыпускКоличество2"
		, "Объект.Выпуск.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("ВыпускНоменклатураЕдиницаИзмерения1"
		, "Объект.Выпуск.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("ВыпускНоменклатураЕдиницаИзмерения2"
		, "Объект.Выпуск.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("МатериалыНаТарахВыпускаКоличество1"
		, "Объект.МатериалыНаТарахВыпуска.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("МатериалыНаТарахВыпускаКоличество2"
		, "Объект.МатериалыНаТарахВыпуска.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("МатериалыНаТарахВыпускаНоменклатураЕдиницаИзмерения1"
		, "Объект.МатериалыНаТарахВыпуска.ИспользованиеДопЕдиницИзмерения");
	СоответствиеПутейКДанным.Вставить("МатериалыНаТарахВыпускаНоменклатураЕдиницаИзмерения2"
		, "Объект.МатериалыНаТарахВыпуска.ИспользованиеДопЕдиницИзмерения");
	
	Структура = НоменклатураСервер.СтруктураДанныхДляОформленияКоличеств();
	Структура.ОформляемыеПоля = ОформляемыеПоля;
	Структура.СоответствиеПутейКДаннымПустогоЗначения = СоответствиеПутейКДаннымПустогоЗначения;
	Структура.СоответствиеПредставленийПустогоЗначения = СоответствиеПредставленийПустогоЗначения;
	Структура.СоответствиеПутейКДаннымВариантИспользованияДополнительныхЕдиницИзмерения = СоответствиеПутейКДанным;
	
	Возврат Структура;
	
КонецФункции

&НаСервере
Функция СтруктураДанныхДляОформленияСклада()
	
	ОформляемыеПоля = Новый Массив;
	ОформляемыеПоля.Добавить("ВыпускЯчейка");
	ОформляемыеПоля.Добавить("РаспределениеМатериаловЯчейка");
	
	СоответствиеПутейКДаннымПустогоЗначения = Новый Соответствие;
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("ВыпускЯчейка", "Объект.Выпуск.Ячейка");
	СоответствиеПутейКДаннымПустогоЗначения.Вставить("РаспределениеМатериаловЯчейка", "Объект.РаспределениеМатериалов.Ячейка");
	
	СоответствиеПредставленийПустогоЗначения = Новый Соответствие;
	СоответствиеПредставленийПустогоЗначения.Вставить("ВыпускЯчейка", НСтр("ru = '<Ячейка>'"));
	СоответствиеПредставленийПустогоЗначения.Вставить("РаспределениеМатериаловЯчейка", НСтр("ru = '<Ячейка>'"));
	
	СоответствиеПутейКДаннымВариантИспользованияЯчеек = Новый Соответствие;
	СоответствиеПутейКДаннымВариантИспользованияЯчеек.Вставить("ВыпускЯчейка"
		, "Объект.Выпуск.ИспользоватьАдресноеХранение");
	СоответствиеПутейКДаннымВариантИспользованияЯчеек.Вставить("РаспределениеМатериаловЯчейка"
		, "Объект.РаспределениеМатериалов.ИспользоватьАдресноеХранение");
	
	Структура = СкладыСервер.СтруктураДанныхДляОформленияСклада();
	Структура.ОформляемыеПоля = ОформляемыеПоля;
	Структура.СоответствиеПутейКДаннымПустогоЗначения = СоответствиеПутейКДаннымПустогоЗначения;
	Структура.СоответствиеПредставленийПустогоЗначения = СоответствиеПредставленийПустогоЗначения;
	Структура.СоответствиеПутейКДаннымВариантИспользованияЯчеек = СоответствиеПутейКДаннымВариантИспользованияЯчеек;
	
	Возврат Структура;
	
КонецФункции

#КонецОбласти

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.ВыпускПричинаБрака.Видимость = Объект.ВидОперации = ПредопределенноеЗначение("Перечисление.к2ВидыОперацийПереработки.Брак");
	
	Элементы.ВыпускГруппаСклад.Видимость = УказыватьСкладВТаблицеВыпуск;
	Элементы.ГруппаСкладЯчейкаВыпуск.Видимость = Не Элементы.ВыпускГруппаСклад.Видимость;
	Элементы.ЯчейкаВыпуск.Видимость = СкладВыпуск.ИспользоватьАдресноеХранение;
	
	Элементы.РаспределениеМатериаловГруппаСклад.Видимость = УказыватьСкладВТаблицеМатериалы;
	Элементы.ГруппаСкладЯчейкаМатериалы.Видимость = Не Элементы.РаспределениеМатериаловГруппаСклад.Видимость;
	Элементы.ЯчейкаМатериалы.Видимость = СкладМатериалы.ИспользоватьАдресноеХранение;
	Элементы.ГруппаМатериалыНаТарахВыпуска.Видимость = Не Объект.МатериалыНаТарахВыпуска.Количество() = 0;
	
КонецПроцедуры

#КонецОбласти

#Область Заполнить

&НаКлиенте
Процедура ОткрытьФормуНастройкиЗаполненияМатериалов()
	
	Оповещение = Новый ОписаниеОповещения("ОткрытьФормуНастройкиЗаполненияМатериаловЗавершение", ЭтотОбъект);
	
	ОткрытьФорму("Документ.к2Переработка.Форма.ФормаНастройкаЗаполненияМатериалов", , ЭтотОбъект, , , , Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуНастройкиЗаполненияМатериаловЗавершение(РезультатЗакрытия, ДополнительныеПараметры) Экспорт
	
	Если РезультатЗакрытия = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЗаполнитьРаспределениеМатериаловНаСервере(РезультатЗакрытия);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРаспределениеМатериаловНаСервере(СтруктураЗаполнения)
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	
	ДокументОбъект.РаспределениеМатериаловЗаполнитьПоСтруктуреНастроек(СтруктураЗаполнения);
	
	ЗначениеВРеквизитФормы(ДокументОбъект, "Объект");
	
	ЗаполнитьСлужебныеРеквизиты();
	
КонецПроцедуры

#КонецОбласти

#Область РаботаСоСтрокойВыпускаРаспределенияМатериалов

&НаКлиенте
Процедура ОткрытьФормаВыбораСтрокиВыпуска(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ТекущиеДанные = Элементы.РаспределениеМатериалов.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Или Объект.Выпуск.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура("АдресТаблицыВыпуска", ВыпускПоместитьВоВременноеХранилище());
	
	ОткрытьФорму("Документ.к2Переработка.Форма.ФормаВыбораСтрокиВыпуска", ПараметрыФормы, ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Функция ВыпускПоместитьВоВременноеХранилище()
	
	ТаблицаВыпуска = Объект.Выпуск.Выгрузить();
	КолонкиГруппировки = "Номенклатура, Характеристика, Серия, АналитикаУчетаНоменклатуры, Спецификация, Этап";
	КолонкиСуммирования = "Количество, Количество1, Количество2";
	
	ТаблицаВыпуска.Свернуть(КолонкиГруппировки, КолонкиСуммирования);
	
	Возврат ПоместитьВоВременноеХранилище(ТаблицаВыпуска, УникальныйИдентификатор);
	
КонецФункции

&НаСервере
Процедура РаспределениеМатериаловСтрокиВыпускаЗаполнить()
	
	Для Каждого СтрокаРаспределениеМатериала Из Объект.РаспределениеМатериалов Цикл
		РаспределениеМатериаловСтрокаВыпускаЗаполнить(СтрокаРаспределениеМатериала.ПолучитьИдентификатор());
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура РаспределениеМатериаловСтрокаВыпускаЗаполнить(ТекущаяСтрока)
	
	ТекущиеДанные = Объект.РаспределениеМатериалов.НайтиПоИдентификатору(ТекущаяСтрока);
	
	СтруктураОтбора = Новый Структура;
	
	Если ЗначениеЗаполнено(ТекущиеДанные.АналитикаУчетаВыпуска) Тогда
		АналитикаУчетаВыпуска = ТекущиеДанные.АналитикаУчетаВыпуска;
	Иначе
		АналитикаУчетаВыпуска = Неопределено;
	КонецЕсли;
	
	СтруктураОтбора.Вставить("АналитикаУчетаНоменклатуры", АналитикаУчетаВыпуска);
	
	НайденныеСтроки = Объект.Выпуск.НайтиСтроки(СтруктураОтбора);
	
	Если НайденныеСтроки.Количество() = 0 Тогда
		
		ТекущиеДанные.ОшибкаСоответствияСтрокеВыпуска = Истина;
		
	Иначе
		
		СтрокаВыпуска = НайденныеСтроки[0];
		ТекущиеДанные.СтрокаВыпуска = Документы.к2Переработка.ПредставлениеСтрокиВыпуска(СтрокаВыпуска);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьИсходныеАналитикиУчетаНоменклатуры()
	
	Для Каждого Строка Из Объект.Выпуск Цикл
		
		Строка.ИсходнаяАналитикаУчетаНоменклатуры = Строка.АналитикаУчетаНоменклатуры;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ЗаполнениеСкладовЯчеек

#Область СкладМатериалы

&НаСервере
Процедура СкладЯчейкаМатериалыЗаполнитьПоТаблице()
	
	Таблица = Объект.РаспределениеМатериалов.Выгрузить(, "Склад, Ячейка");
	
	к2Коллекции.СгруппироватьТаблицу(Таблица, "");
	
	УказыватьСкладВТаблицеМатериалы = Таблица.Количество() > 1;
	
	Если УказыватьСкладВТаблицеМатериалы Тогда
		
		СкладМатериалы = Неопределено;
		ЯчейкаМатериалы = Неопределено;
		
	Иначе
		
		Если Таблица.Количество() = 1 Тогда
			
			СкладМатериалы = Таблица[0].Склад;
			ЯчейкаМатериалы = Таблица[0].Ячейка;
			
		КонецЕсли;
		
		СкладМатериалыПриИзмененииНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УказыватьСкладМатериалыВТаблицеПриИзмененииНаСервере()
	
	СкладМатериалы = Неопределено;
	ЯчейкаМатериалы = Неопределено;
	
	МатериалыСкладЯчейкаЗаполнить();
	
КонецПроцедуры

&НаСервере
Процедура СкладМатериалыПриИзмененииНаСервере()
	
	МатериалыСкладЯчейкаЗаполнить();
	
КонецПроцедуры

&НаСервере
Процедура ЯчейкаМатериалыПриИзмененииНаСервере()
	
	Если Не ЗначениеЗаполнено(ЯчейкаВыпуск) И СкладМатериалы = СкладВыпуск Тогда
		
		ЯчейкаВыпуск = ЯчейкаМатериалы;
		ЯчейкаВыпускПриИзмененииНаСервере();
		
	КонецЕсли;
	
	МатериалыСкладЯчейкаЗаполнить();
	
КонецПроцедуры

&НаСервере
Процедура МатериалыСкладЯчейкаЗаполнить()
	
	Для Каждого Строка Из Объект.РаспределениеМатериалов Цикл
		
		Строка.Склад = СкладМатериалы;
		Строка.Ячейка = ЯчейкаМатериалы;
		
		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ПроверитьИспользоватьАдресноеХранениеЗаполнитьЯчейку", Строка.Склад);
		
		к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(Строка, СтруктураДействий, Неопределено);
		
	КонецЦикла;
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

#КонецОбласти

#Область СкладВыпуск

&НаСервере
Процедура СкладЯчейкаВыпускЗаполнитьПоТаблице()
	
	Таблица = Объект.Выпуск.Выгрузить(, "Склад, Ячейка");
	
	к2Коллекции.СгруппироватьТаблицу(Таблица, "");
	
	УказыватьСкладВТаблицеВыпуск = Таблица.Количество() > 1;
	
	Если УказыватьСкладВТаблицеВыпуск Тогда
		
		СкладВыпуск = Неопределено;
		ЯчейкаВыпуск = Неопределено;
		
	Иначе
		
		Если Таблица.Количество() = 1 Тогда
			
			СкладВыпуск = Таблица[0].Склад;
			ЯчейкаВыпуск = Таблица[0].Ячейка;
			
		КонецЕсли;
		
		СкладВыпускПриИзмененииНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УказыватьСкладВыпускВТаблицеПриИзмененииНаСервере()
	
	СкладВыпуск = Неопределено;
	ЯчейкаВыпуск = Неопределено;
	
	ВыпускСкладЯчейкаЗаполнить();
	
КонецПроцедуры

&НаСервере
Процедура СкладВыпускПриИзмененииНаСервере()
	
	ВыпускСкладЯчейкаЗаполнить();
	
КонецПроцедуры

&НаСервере
Процедура ЯчейкаВыпускПриИзмененииНаСервере()
	
	ВыпускСкладЯчейкаЗаполнить();
	
КонецПроцедуры

&НаСервере
Процедура ВыпускСкладЯчейкаЗаполнить()
	
	Для Каждого Строка Из Объект.Выпуск Цикл
		
		Строка.Склад = СкладВыпуск;
		Строка.Ячейка = ЯчейкаВыпуск;
		
		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ПроверитьИспользоватьАдресноеХранениеЗаполнитьЯчейку", Строка.Склад);
		
		к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(Строка, СтруктураДействий, Неопределено);
		
	КонецЦикла;
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ЗаполнитьСлужебныеРеквизиты();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизиты()
	
	СтруктураХарактеристикиИспользуются = Новый Структура;
	СтруктураХарактеристикиИспользуются.Вставить("Номенклатура", "ХарактеристикиИспользуются");
	СтруктураХарактеристикиИспользуются.Вставить("НоменклатураОригинал", "ХарактеристикиИспользуютсяОригинал");
	
	СтруктураСерииИспользуются = Новый Структура;
	СтруктураСерииИспользуются.Вставить("Номенклатура", "СерииИспользуются");
	
	СтруктураИспользованиеДопЕдиницИзмерения = Новый Структура;
	СтруктураИспользованиеДопЕдиницИзмерения.Вставить("Номенклатура", "ИспользованиеДопЕдиницИзмерения");
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются", СтруктураХарактеристикиИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьПризнакСерииИспользуются", СтруктураСерииИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьВариантИспользованияДополнительныхЕдиницИзмерения", СтруктураИспользованиеДопЕдиницИзмерения);
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.РаспределениеМатериалов, СтруктураДействий);
	
	СтруктураХарактеристикиИспользуются = Новый Структура;
	СтруктураХарактеристикиИспользуются.Вставить("Номенклатура", "ХарактеристикиИспользуются");
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются", СтруктураХарактеристикиИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьПризнакСерииИспользуются", СтруктураСерииИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьВариантИспользованияДополнительныхЕдиницИзмерения", СтруктураИспользованиеДопЕдиницИзмерения);
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.Выпуск, СтруктураДействий);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются", СтруктураХарактеристикиИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьПризнакСерииИспользуются", СтруктураСерииИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьВариантИспользованияДополнительныхЕдиницИзмерения", СтруктураИспользованиеДопЕдиницИзмерения);
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.МатериалыНаТарахВыпуска, СтруктураДействий);
	
	СтруктураИспользоватьАдресноеХранение = Новый Структура;
	СтруктураИспользоватьАдресноеХранение.Вставить("Склад", "ИспользоватьАдресноеХранение");
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакИспользоватьАдресноеХранение", СтруктураИспользоватьАдресноеХранение);
	СкладыСервер.ЗаполнитьСлужебныеРеквизитыПоСкладуВКоллекции(Объект.Выпуск, СтруктураДействий);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакИспользоватьАдресноеХранение", СтруктураИспользоватьАдресноеХранение);
	СкладыСервер.ЗаполнитьСлужебныеРеквизитыПоСкладуВКоллекции(Объект.РаспределениеМатериалов, СтруктураДействий);
	
	РаспределениеМатериаловСтрокиВыпускаЗаполнить();
	
	ЗаполнитьИсходныеАналитикиУчетаНоменклатуры();
	
	СкладЯчейкаВыпускЗаполнитьПоТаблице();
	СкладЯчейкаМатериалыЗаполнитьПоТаблице();
	
КонецПроцедуры

&НаСервере
Процедура РабочийЦентрПриИзмененииНаСервере()
	
	УказыватьСкладВТаблицеМатериалы = Ложь;
	
	СкладыРабочегоЦентра = Справочники.к2РабочиеЦентры.СкладыРабочихЦентров(Объект.РабочийЦентр);
	
	Если СкладыРабочегоЦентра.Количество() = 1 Тогда
		СкладМатериалы = СкладыРабочегоЦентра[0];
	Иначе
		СкладМатериалы = Неопределено;
	КонецЕсли;
	
	ЯчейкаМатериалы = Неопределено;
	
	СкладМатериалыПриИзмененииНаСервере();
	
	Если Не ЗначениеЗаполнено(СкладВыпуск) Тогда
		
		УказыватьСкладВТаблицеВыпуск = Ложь;
		
		СкладВыпуск = СкладМатериалы;
		ЯчейкаВыпуск = Неопределено;
		
		СкладВыпускПриИзмененииНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ВыпускПриИзмененииНаСервере()
	
	РаспределениеМатериаловСтрокиВыпускаЗаполнить();
	
	Если Не УказыватьСкладВТаблицеВыпуск Тогда
		ВыпускСкладЯчейкаЗаполнить();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура РаспределениеМатериаловПриИзмененииНаСервере()
	
	Если Не УказыватьСкладВТаблицеМатериалы Тогда
		МатериалыСкладЯчейкаЗаполнить();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьКлючСвязиПоЕдинственнойСтрокеВыпуска(ИдентификаторСтрокиРаспределения)
	
	Если Не Объект.Выпуск.Количество() = 1
		Или ИдентификаторСтрокиРаспределения = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	ТекущаяСтрокаРаспределения = Объект.РаспределениеМатериалов.НайтиПоИдентификатору(ИдентификаторСтрокиРаспределения);
	ТекущаяСтрокаВыпуска = Объект.Выпуск.Получить(0);
	
	Если ТекущаяСтрокаРаспределения <> Неопределено И ТекущаяСтрокаВыпуска <> Неопределено Тогда
		
		ТекущаяСтрокаРаспределения.АналитикаУчетаВыпуска = ТекущаяСтрокаВыпуска.АналитикаУчетаНоменклатуры;
		ТекущаяСтрокаРаспределения.СтрокаВыпуска = Документы.к2Переработка.ПредставлениеСтрокиВыпуска(ТекущаяСтрокаВыпуска);
		ТекущаяСтрокаРаспределения.ОшибкаСоответствияСтрокеВыпуска = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти