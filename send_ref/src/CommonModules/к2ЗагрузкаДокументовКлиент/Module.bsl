
#Область СлужебныйПрограммныйИнтерфейс

Процедура ЗагрузитьЗаказыКлиентаИзФайла() Экспорт
	
	ПараметрыЗагрузки = ЗагрузкаДанныхИзФайлаКлиент.ПараметрыЗагрузкиДанных();
	ПараметрыЗагрузки.ПолноеИмяТабличнойЧасти = "Обработка.к2ЗагрузкаДокументовИзФайла.ТабличнаяЧасть.ЗаказыКлиента";
	ПараметрыЗагрузки.Заголовок               = НСтр("ru = 'Загрузка заказов клиента из файла'");
	ПараметрыЗагрузки.ИмяМакетаСШаблоном      = "ЗаказыКлиентов";
	
	Оповещение = Новый ОписаниеОповещения("ЗагрузитьЗаказыИзФайлаЗавершение", ЭтотОбъект);
	ЗагрузкаДанныхИзФайлаКлиент.ПоказатьФормуЗагрузки(ПараметрыЗагрузки, Оповещение);
	
КонецПроцедуры

Процедура ЗагрузитьЗаказыНаПеремещениеИзФайла() Экспорт
	
	ПараметрыЗагрузки = ЗагрузкаДанныхИзФайлаКлиент.ПараметрыЗагрузкиДанных();
	ПараметрыЗагрузки.ПолноеИмяТабличнойЧасти = "Обработка.к2ЗагрузкаДокументовИзФайла.ТабличнаяЧасть.ЗаказыНаПеремещение";
	ПараметрыЗагрузки.Заголовок               = НСтр("ru = 'Загрузка заказов на перемещение из файла'");
	ПараметрыЗагрузки.ИмяМакетаСШаблоном      = "ЗаказыНаПеремещение";
	
	Оповещение = Новый ОписаниеОповещения("ЗагрузитьЗаказыНаПеремещениеИзФайлаЗавершение", ЭтотОбъект);
	ЗагрузкаДанныхИзФайлаКлиент.ПоказатьФормуЗагрузки(ПараметрыЗагрузки, Оповещение);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ЗагрузитьЗаказыИзФайлаЗавершение(АдресЗагруженныхДанных, ДополнительныеПараметры) Экспорт
	
	Если АдресЗагруженныхДанных = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	созданныеЗаказы = к2ЗагрузкаДокументовВызовСервера.ЗагрузитьЗаказыПоТаблицеВоВременномХранилище(АдресЗагруженныхДанных);
	
	ОповеститьОСозданныхЗаказах(созданныеЗаказы);
	
КонецПроцедуры

Процедура ЗагрузитьЗаказыНаПеремещениеИзФайлаЗавершение(АдресЗагруженныхДанных, ДополнительныеПараметры) Экспорт
	
	Если АдресЗагруженныхДанных = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	созданныеЗаказы = к2ЗагрузкаДокументовВызовСервера.ЗагрузитьЗаказыНаПеремещениеПоТаблицеВоВременномХранилище(АдресЗагруженныхДанных);
	
	ОповеститьОСозданныхЗаказахНаПеремещение(созданныеЗаказы);
	
КонецПроцедуры

Процедура ОповеститьОСозданныхЗаказах(Знач пМассивЗаказов)
	
	Если пМассивЗаказов.Количество() = 0 Тогда
		
		действиеПриНажатии = "";
		картинка           = БиблиотекаКартинок.Ошибка32;
		текст              = НСтр("ru = 'Не удалось загрузить заказы'");
		
	Иначе
		
		ОповеститьОбИзменении(Тип("ДокументСсылка.ЗаказКлиента"));
		
		Оповестить("ЗаписаныЗаказы", пМассивЗаказов);
		
		параметрыНажатия = Новый Структура;
		параметрыНажатия.Вставить("Ссылки", пМассивЗаказов);
		параметрыНажатия.Вставить("ИмяФормы", "Документ.ЗаказКлиента.ФормаСписка");
		
		действиеПриНажатии = Новый ОписаниеОповещения("ДействиеПриНажатии", ЭтотОбъект, параметрыНажатия);
		
		картинка = БиблиотекаКартинок.Успешно32;
		текст    = НСтр("ru = 'Создано заказов: %1'");
		текст    = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(текст, пМассивЗаказов.Количество());
		
	КонецЕсли;
	
	ПоказатьОповещениеПользователя(
		НСтр("ru = 'Загрузка заказов клиента'"),
		действиеПриНажатии,
		текст,
		картинка,
		СтатусОповещенияПользователя.Информация);
	
КонецПроцедуры

Процедура ОповеститьОСозданныхЗаказахНаПеремещение(Знач пМассивЗаказов)
	
	Если пМассивЗаказов.Количество() = 0 Тогда
		
		действиеПриНажатии = "";
		картинка           = БиблиотекаКартинок.Ошибка32;
		текст              = НСтр("ru = 'Не удалось загрузить заказы'");
		
	Иначе
		
		ОповеститьОбИзменении(Тип("ДокументСсылка.к2ЗаказНаПеремещение"));
		
		Оповестить("ЗаписаныЗаказыНаПеремещение", пМассивЗаказов);
		
		параметрыНажатия = Новый Структура;
		параметрыНажатия.Вставить("Ссылки", пМассивЗаказов);
		параметрыНажатия.Вставить("ИмяФормы", "Документ.к2ЗаказНаПеремещение.ФормаСписка");
		
		действиеПриНажатии = Новый ОписаниеОповещения("ДействиеПриНажатии", ЭтотОбъект, параметрыНажатия);
		
		картинка = БиблиотекаКартинок.Успешно32;
		текст    = НСтр("ru = 'Создано заказов: %1'");
		текст    = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(текст, пМассивЗаказов.Количество());
		
	КонецЕсли;
	
	ПоказатьОповещениеПользователя(
		НСтр("ru = 'Загрузка заказов на перемещение'"),
		действиеПриНажатии,
		текст,
		картинка,
		СтатусОповещенияПользователя.Информация);
	
КонецПроцедуры

Процедура ДействиеПриНажатии(ДополнительныеПараметры) Экспорт
	
	параметрыОткрытия = Новый Структура();
	параметрыОткрытия.Вставить("Отбор", Новый Структура("Ссылка", ДополнительныеПараметры.Ссылки));
	
	ОткрытьФорму(ДополнительныеПараметры.ИмяФормы, параметрыОткрытия, , Новый УникальныйИдентификатор);
	
КонецПроцедуры

#КонецОбласти