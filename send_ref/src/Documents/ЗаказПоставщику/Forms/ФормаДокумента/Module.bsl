
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения; // используется механизмом обработки изменения реквизитов ТЧ

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	к2КопированиеСтрокСервер.ПриСозданииНаСевере( Элементы, "Товары" );
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства
	
	// СтандартныеПодсистемы.РаботаСФайлами
	ПараметрыГиперссылки = РаботаСФайлами.ГиперссылкаФайлов();
	ПараметрыГиперссылки.Размещение = "КоманднаяПанель";
	РаботаСФайлами.ПриСозданииНаСервере(ЭтотОбъект, ПараметрыГиперссылки);
	// Конец СтандартныеПодсистемы.РаботаСФайлами
	
	УстановитьУсловноеОформление();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	// СтандартныеПодсистемы.РаботаСФайлами
	РаботаСФайламиКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	// Конец СтандартныеПодсистемы.РаботаСФайлами
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ПодборНоменклатурыПроизведен"
		И ЗначениеЗаполнено( Параметр )
		И Источник = УникальныйИдентификатор Тогда
		
		ЗагрузитьНоменклатуруИзХранилища( Параметр );
		
	КонецЕсли;
	
	Этапы_ОбработкаОповещения(ИмяСобытия);
	
	к2КопированиеСтрокКлиент.ОбработкаОповещения( ИмяСобытия, Параметр, Элементы );
	
	// СтандартныеПодсистемы.Свойства 
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства
	
	// СтандартныеПодсистемы.РаботаСФайлами
	РаботаСФайламиКлиент.ОбработкаОповещения(ЭтотОбъект, ИмяСобытия);
	// Конец СтандартныеПодсистемы.РаботаСФайлами
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства

КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	к2СобытияФорм.ОбработкаПроверкиЗаполненияНаСервере( ЭтотОбъект, Отказ, ПроверяемыеРеквизиты );
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	к2ЭтапыДокументов.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПриЗаписиНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	РаботаСФайлами.ПриЗаписиНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи, Параметры);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи( ПараметрыЗаписи )
	
	Оповестить( "Запись_ЗаказПоставщику", Объект.Ссылка, ЭтотОбъект );
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ДатаПриИзменении(Элемент)
	
	ДатаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаДоставкиПриИзменении(Элемент)
	
	ДатаДоставкиПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОрганизацияПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	КонтрагентПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашениеПриИзменении(Элемент)
	
	ПриИзмененииСоглашенияСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура НалогообложениеПриИзменении(Элемент)
	
	ЗаполнитьУсловияПродажПоСоглашению();
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария( Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий" );
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТовары

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)
	
	к2ПокупкиКлиент.НоменклатураПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыХарактеристикаПриИзменении(Элемент)
	
	к2ПродажиКлиент.ХарактеристикаПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	
	к2ПродажиКлиент.КоличествоПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоУпаковокПриИзменении(Элемент)
	
	к2ПродажиКлиент.КоличествоУпаковокПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
	к2ЭтапыДокументовКлиент.КоличествоПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыУпаковкаПриИзменении(Элемент)
	
	к2ПродажиКлиент.УпаковкаПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
	к2ЭтапыДокументовКлиент.УпаковкаПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)
	
	к2ПродажиКлиент.ЦенаПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСтавкаНДСПриИзменении(Элемент)
	
	к2ПродажиКлиент.СтавкаНДСПриИзменении( ЭтотОбъект, КэшированныеЗначения );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	ТекущиеДанныеТоваров = Элементы.Товары.ТекущиеДанные;
	
	Если НоваяСтрока Тогда
		ТекущиеДанныеТоваров.КодСтроки = 0;
		ТекущиеДанныеТоваров.КлючСвязи = Новый УникальныйИдентификатор;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	к2ДокументыКлиентСервер.РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПослеУдаления(Элемент)
	
	к2ДокументыКлиентСервер.РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПодобратьТовары(Команда)

	СтруктураПодбораНоменклатуры = СтруктураПодбораНоменклатурыНаСервере();

	ДополнительныеПараметрыПодбора = НоменклатураКлиент.ДополнительныеПараметрыПодбора();
	ДополнительныеПараметрыПодбора.Контрагент = Объект.Поставщик;
	ДополнительныеПараметрыПодбора.ОткрыватьФормуПодбораСКоличеством = СтруктураПодбораНоменклатуры.ОткрыватьФормуПодбораСКоличеством;

	НоменклатураКлиент.ПодобратьНоменклатуруСПараметрами(ЭтотОбъект,
														   СтруктураПодбораНоменклатуры.АдресПодобраннойНоменклатуры,
														   ,
														   ДополнительныеПараметрыПодбора);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыКопироватьСтроки(Команда)
	
	количествоСкопировано = СкопироватьСтрокиТоваровНаСервере();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОКопированииСтрок( количествоСкопировано, "Товары" );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыВставитьСтроки(Команда)
	
	количествоВставлено = ПолучитьСтрокиТоваровИзБуфераОбмена();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОВставкеСтрок( количествоВставлено );
	
КонецПроцедуры

&НаКлиенте
Процедура РазбитьСтроку(Команда)
	
	ТаблицаФормы  = Элементы.Товары;
	ДанныеТаблицы = Объект.Товары;
	ОповещениеПослеРазбиения = Новый ОписаниеОповещения("РазбитьСтрокуЗавершение", ЭтотОбъект);
	
	к2ОбщегоНазначенияКлиент.РазбитьСтрокуТЧ(ДанныеТаблицы, ТаблицаФормы, ОповещениеПослеРазбиения, Неопределено);

КонецПроцедуры

&НаКлиенте
Процедура УстановитьСледующийЭтап(Команда)
	
	УстановитьСледующийЭтапНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область СтандартныеПодсистемы

// СтандартныеПодсистемы.РаботаСФайлами

&НаКлиенте
Процедура Подключаемый_КомандаПанелиПрисоединенныхФайлов(Команда)
	
	РаботаСФайламиКлиент.КомандаУправленияПрисоединеннымиФайлами(ЭтотОбъект, Команда);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПолеПредпросмотраНажатие(Элемент, СтандартнаяОбработка)
	
	РаботаСФайламиКлиент.ПолеПредпросмотраНажатие( ЭтотОбъект, Элемент, СтандартнаяОбработка );
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПолеПредпросмотраПеретаскивание(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка)
	
	РаботаСФайламиКлиент.ПолеПредпросмотраПеретаскивание( ЭтотОбъект, Элемент,
														  ПараметрыПеретаскивания, СтандартнаяОбработка );
														  
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПолеПредпросмотраПроверкаПеретаскивания(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка)
	
	РаботаСФайламиКлиент.ПолеПредпросмотраПроверкаПеретаскивания( ЭтотОбъект, Элемент,
																  ПараметрыПеретаскивания, СтандартнаяОбработка );
																  
КонецПроцедуры

// Конец СтандартныеПодсистемы.РаботаСФайлами

#Область ПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Объект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

// СтандартныеПодсистемы.Свойства

&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда,
												 НавигационнаяСсылка  = Неопределено,
												 СтандартнаяОбработка = Неопределено)
                                                 
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьДополнительныйРеквизит(Команда)
	
	к2ОбщегоНазначенияКлиент.ДобавитьДополнительныйРеквизит(ТекущийНаборСвойство());
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.Свойства

#КонецОбласти

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	к2УО.Товары(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()

	НастроитьЭлементы();

	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();
	
	Этапы_ПриЧтенииСозданииНаСервере();

КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементы()
	
	Если к2ИзменениеФорм.ИзмененаПрограммно(ЭтотОбъект) Тогда
		
		Возврат;
	
	КонецЕсли;
	
	добавленныеЭлементы = к2Документы.ДобавитьИспользованияДополнительныхЕдиницИзмерения(ЭтотОбъект);

	// Перемещение существующей группы в созданную
	Элементы.Переместить(Элементы.ТоварыГруппа_ВЕдиницахХранения,
						 добавленныеЭлементы.ГруппаКоличество,
						 добавленныеЭлементы.ГруппаВЕдиницахХранения1);
						 
	// Перемещение новой группы с количествами перед ценой
	Элементы.Переместить(добавленныеЭлементы.ГруппаКоличество,
						 добавленныеЭлементы.ГруппаКоличество.Родитель,
						 Элементы.ТоварыЦена);
	
	к2Документы.НастроитьЭлементыТабличнойЧасти(ЭтотОбъект);
	к2ИзменениеФорм.ЗаполнитьЕдиницыИзмеренияВЗаголовкахДокумента(ЭтотОбъект, Объект.Валюта);
	
КонецПроцедуры

&НаСервере
Процедура ДатаПриИзмененииНаСервере()
	
	ЗаполнитьСоглашение();
	
КонецПроцедуры

&НаСервере
Процедура ДатаДоставкиПриИзмененииНаСервере()
	
	Если Объект.МоментОпределенияЦены = Перечисления.к2МоментОпределенияЦены.НаДатуОтгрузки Тогда
		
		ЗаполнитьУсловияПродажПоСоглашению();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	
	ЗаполнитьСоглашение();
	
КонецПроцедуры

&НаСервере
Процедура КонтрагентПриИзмененииНаСервере()
	
	ЗаполнитьСоглашение();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСоглашение()
	
	к2Покупки.ЗаполнитьСоглашениеВДокументеПокупки(Объект);
	
	ПриИзмененииСоглашенияСервер();
	
КонецПроцедуры

&НаСервере
Процедура ПриИзмененииСоглашенияСервер()
	
	к2Покупки.ЗаполнитьПоСоглашениюВДокументеПокупки( ЭтотОбъект );
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьУсловияПродажПоСоглашению()

	к2Ценообразование.ЗаполнитьУсловияПродажПоСоглашению(Объект);

	к2Документы.ЗаполнитьСлужебныеРеквизиты(ЭтотОбъект);

КонецПроцедуры

#Область Этапы

&НаСервере
Процедура Этапы_ПриЧтенииСозданииНаСервере()
	
	параметрыДокумента = Документы.ЗаказПоставщику.Этапы_Параметры();
	
	параметрыЭтапов = к2ЭтапыДокументов.Параметры(параметрыДокумента);
	
	параметрыЭтапов.ИмяКнопки_УстановитьСледующийЭтап = "УстановитьСледующийЭтап";
	параметрыЭтапов.ИмяГруппыВШапке = Элементы.Группа_Этап.Имя;
	параметрыЭтапов.ИмяГруппыВТаблице = Элементы.Группа_КоличествоПоЭтапам.Имя;
	
	к2ЭтапыДокументов.ПриЧтенииСоздании(ЭтотОбъект, параметрыЭтапов);

КонецПроцедуры

&НаКлиенте
Процедура Этапы_ОбработкаОповещения(Знач ИмяСобытия)
	
	Если ИмяСобытия = "Запись_к2ЭтапыДокументов" Тогда
		
		Этапы_НастроитьЭлементы();
		
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ЭтапПриИзменении(Элемент)
	
	ЭтапПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_КоличествоПоЭтапуПриИзменении(Элемент)

	к2ЭтапыДокументовКлиент.КоличествоПоЭтапуПриИзменении(ЭтотОбъект);

	к2ПродажиКлиент.КоличествоУпаковокПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	
КонецПроцедуры

&НаСервере
Процедура ЭтапПриИзмененииНаСервере()

	к2ЭтапыДокументов.ПриИзменении(ЭтотОбъект);

КонецПроцедуры

&НаСервере
Процедура УстановитьСледующийЭтапНаСервере()
	
	к2ЭтапыДокументов.УстановитьСледующий(ЭтотОбъект);

КонецПроцедуры

&НаСервере
Процедура Этапы_НастроитьЭлементы()

	к2ЭтапыДокументов.НастроитьЭлементы(ЭтотОбъект);

КонецПроцедуры

#КонецОбласти

#Область Подборы

&НаСервере
Функция СтруктураПодбораНоменклатурыНаСервере()

	СтруктураВозврата = Новый Структура;
	СтруктураВозврата.Вставить("АдресПодобраннойНоменклатуры", АдресПодобраннойНоменклатуры());
	СтруктураВозврата.Вставить("ОткрыватьФормуПодбораСКоличеством",
							   к2ПраваПользователяПовтИсп.ОткрыватьФормуПодбораНоменклатурыСКоличеством());

	Возврат СтруктураВозврата;

КонецФункции

&НаСервере
Функция АдресПодобраннойНоменклатуры()
	
	Возврат ПоместитьВоВременноеХранилище( Объект.Товары.Выгрузить() );
	
КонецФункции

&НаСервере
Процедура ЗагрузитьНоменклатуруИзХранилища(Адрес)
	
	Если Не ЗначениеЗаполнено(Адрес) Тогда
		
		Возврат;
	
	КонецЕсли;
	
	тзНоменклатура = ПолучитьИзВременногоХранилища(Адрес);
	тзНоменклатура = к2Коллекции.ПолучитьСвернутуюТаблицуПоВсемКолонкам(тзНоменклатура);
	
	к2Покупки.ЗагрузитьТоварыИзТаблицы(Объект, тзНоменклатура);
	
	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();
	к2ДокументыКлиентСервер.РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область КопированиеВставка

&НаСервере
Функция СкопироватьСтрокиТоваровНаСервере()
	
	Возврат к2КопированиеСтрокСервер.Копировать( Объект.Товары, Элементы.Товары.ВыделенныеСтроки, "Товары" );
	
КонецФункции

&НаСервере
Функция ПолучитьСтрокиТоваровИзБуфераОбмена()
	
	добавленоСтрок = к2КопированиеСтрокСервер.Вставить(
		Объект.Товары,
		Элементы.Товары,
		"Товары",
		"Номенклатура,Характеристика,Упаковка",
		"КодСтроки");
	
	КэшированныеЗначения = Неопределено;
	
	Для каждого цСтрока Из Элементы.Товары.ВыделенныеСтроки Цикл
		
		текСтрока = Объект.Товары.НайтиПоИдентификатору( цСтрока );
		к2Покупки.СтрокаТоваровПриИзменении(Объект, текСтрока, КэшированныеЗначения);
		
	КонецЦикла;
	
	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();
	к2ДокументыКлиентСервер.РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
	Возврат добавленоСтрок;
	
КонецФункции

#КонецОбласти

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизитыПоНоменклатуре()
	
	структураДействий = к2Покупки.ДействияЗаполненияСлужебныхРеквизитов();
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.Товары, структураДействий);

	к2ДокументыКлиентСервер.РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура РазбитьСтрокуЗавершение(НоваяСтрока, ДополнительныеПараметры) Экспорт
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	Если НоваяСтрока <> Неопределено Тогда
		
		НоваяСтрока.КодСтроки = 0;
		
		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
		к2ДокументыКлиентСервер.ДобавитьВСтруктуруДействийПересчетСуммы( СтруктураДействий, Объект, ТекущаяСтрока );
		
		к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
		
		СтруктураДействий.Вставить("ПересчитатьВес", к2ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаВеса());
		
		к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, КэшированныеЗначения);
		
	КонецЕсли;
	
КонецПроцедуры

#Область Свойства

// СтандартныеПодсистемы.Свойства
&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов( ЭтотОбъект );
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов( ЭтотОбъект );
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.Свойства

&НаСервере
Функция ТекущийНаборСвойство()
	
	Возврат УправлениеСвойствами.НаборСвойствПоИмени( "Документ_ЗаказПоставщику" );
	
КонецФункции

#КонецОбласти

#КонецОбласти