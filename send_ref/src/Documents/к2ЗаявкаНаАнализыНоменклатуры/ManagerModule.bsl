#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	////////////////////////////////////////////////////////////////////////////
	// Создадим запрос инициализации движений
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	////////////////////////////////////////////////////////////////////////////
	// Сформируем текст запроса
	
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапроса_к2ЗаявкиНаАнализыНоменклатуры(Запрос, ТекстыЗапроса, Регистры);
	
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

#Область СозданиеНаОсновании

// Определяет список команд создания на основании.
//
// Параметры:
//   КомандыСозданияНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры к2СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры к2СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСоздатьНаОсновании, Параметры) Экспорт

	Документы.к2АнализыНоменклатуры.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	СтруктураИменПараметров = Новый Структура;
	СтруктураИменПараметров.Вставить("Ссылка");
	СтруктураИменПараметров.Вставить("Дата");
	СтруктураИменПараметров.Вставить("Номенклатура");
	СтруктураИменПараметров.Вставить("Характеристика");
	СтруктураИменПараметров.Вставить("Серия");
	СтруктураИменПараметров.Вставить("ВидАнализа");
	СтруктураИменПараметров.Вставить("Основание");
	
	Параметры = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДокументСсылка, СтруктураИменПараметров);
	
	Для Каждого Параметр Из Параметры Цикл
		Запрос.УстановитьПараметр(Параметр.Ключ, Параметр.Значение);
	КонецЦикла;
	
КонецПроцедуры

Функция ТекстЗапроса_к2ЗаявкиНаАнализыНоменклатуры(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ЗаявкиНаАнализыНоменклатуры";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	&Номенклатура КАК Номенклатура,
	|	&Характеристика КАК Характеристика,
	|	&Серия КАК Серия,
	|	&ВидАнализа КАК ВидАнализа,
	|	&Ссылка КАК ЗаявкаНаАнализы,
	|	ЗНАЧЕНИЕ(Перечисление.к2СостоянияАнализовНоменклатуры.Заявка) КАК Состояние";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли