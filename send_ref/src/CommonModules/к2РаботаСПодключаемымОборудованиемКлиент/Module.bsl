
#Область ПрограммныйИнтерфейс

#Область ОбработчикиСобытий

// Процедура, вызываемая из одноименного обработчика события формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма с клиентской переменной модуля формы "ПодключаемоеОборудование".
//  Отказ - Булево - признак отказа открытия формы.
//  ПодключаемоеОборудование - Структура, Неопределено - параметры подключение к оборудованию.
//                 ПриОткрытии ПодключаемоеОборудование = Неопределено. См. к2РаботаСПодключаемымОборудованием.СтруктураОборудования()
//
Процедура ПриОткрытии(Форма, Отказ, ПодключаемоеОборудование) Экспорт
	
	ПодключитьОборудование(Форма, ПодключаемоеОборудование, Отказ);
	
КонецПроцедуры

// Процедура, вызываемая из одноименного обработчика события формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма с клиентской переменной модуля формы "ПодключаемоеОборудование".
//  ЗавершениеРаботы - Булево - признак завершения работы.
//  ПодключаемоеОборудование - Структура - параметры подключение к оборудованию см. к2РаботаСПодключаемымОборудованием.СтруктураОборудования
//
Процедура ПриЗакрытии(Форма, ЗавершениеРаботы, ПодключаемоеОборудование) Экспорт
	
	ОтключитьОборудование(Форма, ПодключаемоеОборудование);
	
КонецПроцедуры

#КонецОбласти

#Область ЭлектронныеВесы

// Подключить электронные весы.
// 
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма
//  Весы - Структура - Весы
//  Отказ - Булево - Отказ
Процедура Подключить_ЭлектронныеВесы(Форма, Весы, Отказ = Ложь) Экспорт
	
	Весы.Подключенные = Неопределено;
	Весы.Менеджер = Неопределено;
	
	СтруктураПодключения = Весы.СоответствиеПодключений.Получить(Весы.Выбранные);
	
	Если Не СтруктураПодключения = Неопределено Тогда
		
		Драйвер = СтруктураПодключения.Драйвер;
		
		Попытка
			
			Подключить_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы);
			
		Исключение
			
			Ошибка = ИнформацияОбОшибке();
			ШаблонОшибки = НСтр("ru='Ошибка подключения. Драйвер ""%1"". Весы ""%2"": %3.'");
			ПоказатьПредупреждениеПользователю(Драйвер, Ошибка, ШаблонОшибки, Весы);
			
			Весы.Подключенные = Неопределено;
			Весы.Менеджер = Неопределено;
			
		КонецПопытки;
		
	КонецЕсли;
	
	Если Весы.Менеджер = Неопределено Тогда
		Отказ = Истина;
	Иначе
		ЭлементыФормыНастроить_ЭлектронныеВесы(Форма, Весы);
	КонецЕсли;
	
КонецПроцедуры

// Отключить электронные весы.
// 
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма
//  Весы - Структура - Весы
Процедура Отключить_ЭлектронныеВесы(Форма, Весы) Экспорт
	
	Если Весы.Менеджер = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураПодключения = Весы.СоответствиеПодключений.Получить(Весы.Подключенные);
	
	Если Не СтруктураПодключения = Неопределено Тогда
		
		Драйвер = СтруктураПодключения.Драйвер;
		
		Попытка
			
			Отключить_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы);
			
		Исключение
			
			Ошибка = ИнформацияОбОшибке();
			ШаблонОшибки = НСтр("ru='Ошибка отключения. Драйвер ""%1"". Весы ""%2"": %3.'");
			ПоказатьПредупреждениеПользователю(Драйвер, Ошибка, ШаблонОшибки, Весы);
			
		КонецПопытки;
		
	КонецЕсли;
	
	Весы.Подключенные = Неопределено;
	Весы.Менеджер = Неопределено;
	
КонецПроцедуры

// Осуществляет получение веса с весов.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма с клиентской переменной модуля формы "ПодключаемоеОборудование".
//  Весы - Структура - параметры подключение к весам. См. к2РаботаСПодключаемымОборудованием.Структура_ЭлектронныеВесы().
//
// Возвращаемое значение:
//  Число - Вес с весов.
//
Функция ПолучитьВес_ЭлектронныеВесы(Форма, Весы) Экспорт
	
	Вес = 0;
	
	Весы.ВесСтабилен = Ложь;
	Весы.ТекущееЗначениеВеса = 0;
	
	СтруктураПодключения = ПодключитьПриНеобходимости_ЭлектронныеВесы(Форма, Весы);
	
	Если Не СтруктураПодключения = Неопределено Тогда
		
		Драйвер = СтруктураПодключения.Драйвер;
		
		Попытка
			
			Вес = ПрочитатьВес_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы);
			
		Исключение
			
			Ошибка = ИнформацияОбОшибке();
			ШаблонОшибки = НСтр("ru='Ошибка чтения веса. Драйвер ""%1"". Весы ""%2"". %3'");
			ПоказатьПредупреждениеПользователю(Драйвер, Ошибка, ШаблонОшибки, Весы);
			
			Весы.АвтоВзвешивание = Ложь;
			Вес = 0;
			
		КонецПопытки;
		
	КонецЕсли;
	
	Если Весы.АвтоВзвешивание Тогда
		ОбработатьАвтоВзвешивание_ЭлектронныеВесы(Весы, Вес);
	Иначе
		Весы.ВесЗафиксирован = Ложь;
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

// Осуществляет получение веса тары с весов.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма с клиентской переменной модуля формы "ПодключаемоеОборудование".
//  Весы - Структура - параметры подключение к весам. См. к2РаботаСПодключаемымОборудованием.Структура_ЭлектронныеВесы().
//
// Возвращаемое значение:
//  Число - Вес тары с весов.
//
Функция ПолучитьВесТары_ЭлектронныеВесы(Форма, Весы) Экспорт
	
	ВесТары = 0;
	
	СтруктураПодключения = ПодключитьПриНеобходимости_ЭлектронныеВесы(Форма, Весы);
	
	Если Не СтруктураПодключения = Неопределено Тогда
		
		Драйвер = СтруктураПодключения.Драйвер;
		
		Попытка
			
			ВесТары = ПрочитатьВесТары_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы);
			
		Исключение
			
			Ошибка = ИнформацияОбОшибке();
			
			ШаблонОшибки = НСтр("ru='Ошибка чтения веса тары. Драйвер ""%1"". Весы ""%2"". %3'");
			ПоказатьПредупреждениеПользователю(Драйвер, Ошибка, ШаблонОшибки, Весы);
			
		КонецПопытки;
		
	КонецЕсли;
	
	Возврат ВесТары;
	
КонецФункции

// Осуществляет установку веса тары на весах.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма с клиентской переменной модуля формы "ПодключаемоеОборудование".
//  Весы - Структура - параметры подключение к весам. См. к2РаботаСПодключаемымОборудованием.Структура_ЭлектронныеВесы().
//  ВесТары - Число - устанавливаемый вес тары.
//
Процедура УстановитьВесТары_ЭлектронныеВесы(Форма, Весы, ВесТары = 0) Экспорт
	
	СтруктураПодключения = ПодключитьПриНеобходимости_ЭлектронныеВесы(Форма, Весы);
	
	Если Не СтруктураПодключения = Неопределено Тогда
		
		Драйвер = СтруктураПодключения.Драйвер;
		
		Попытка
			
			УстановитьВесТары_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы, ВесТары);
			
		Исключение
			
			Ошибка = ИнформацияОбОшибке();
			
			ШаблонОшибки = НСтр("ru='Ошибка установки веса тары. Драйвер ""%1"". Весы ""%2"". %3'");
			ПоказатьПредупреждениеПользователю(Драйвер, Ошибка, ШаблонОшибки, Весы);
			
		КонецПопытки;
		
	КонецЕсли;
	
КонецПроцедуры

// Осуществляет установку текущих весов для получения данных.
// См. к2РаботаСПодключаемымОборудованием.ДобавитьРеквизитВыбораВесов()
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма с клиентской переменной модуля формы "ПодключаемоеОборудование".
//  Весы - Структура - параметры подключение к весам. См. к2РаботаСПодключаемымОборудованием.Структура_ЭлектронныеВесы().
//
Процедура ПриИзменении_ЭлектронныеВесы(Форма, Весы) Экспорт
	
	ИмяИдентификатор = к2РаботаСПодключаемымОборудованиемКлиентСервер.ИмяРеквизита_ИдентификаторЭлектронныеВесы();
	ИмяСоответствие = к2РаботаСПодключаемымОборудованиемКлиентСервер.ИмяРеквизита_СоответствиеЭлектронныеВесы();
	
	Соответствие = Форма[ИмяСоответствие];
	
	Весы.Выбранные = Соответствие.Получить(Форма[ИмяИдентификатор]);
	
КонецПроцедуры

// Возвращает признак фиксации нового взвешивания
//
// Параметры:
//  Весы - Структура - параметры подключение к весам. См. к2РаботаСПодключаемымОборудованием.Структура_ЭлектронныеВесы
//  Вес - Число -
//
// Возвращаемое значение:
//  Булево -
//
Функция ФиксироватьВес(Весы, Вес) Экспорт
	
	Фиксировать = Ложь;
	
	Если Весы.АвтоВзвешивание Тогда
		
		Если ЗначениеЗаполнено(Вес) И Не Весы.ВесЗафиксирован Тогда
			
			Весы.ВесЗафиксирован = Истина;
			Фиксировать = Истина;
			
		КонецЕсли;
		
	Иначе
		
		Фиксировать = Истина;
		
	КонецЕсли;
	
	Возврат Фиксировать;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Процедура ПроверитьДоступность_ЭлектронныеВесы(Форма, Весы, ОтображатьСообщения = Ложь, Отказ = Ложь) Экспорт
	
	Форма.ИспользоватьПодключаемоеОборудование = Истина;
	
	Подключить_ЭлектронныеВесы(Форма, Весы, Отказ);
	
	Если Не Весы.Менеджер = Неопределено Тогда
		
		Вес = ПолучитьВес_ЭлектронныеВесы(Форма, Весы);
		
		Отключить_ЭлектронныеВесы(Форма, Весы);
		
		Если Не Отказ И ОтображатьСообщения Тогда
			
			ШаблонСообщения = НСтр("ru='Подключение установлено. Значение: %1'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, Вес);
			ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения);
			
		КонецЕсли;
		
	КонецЕсли;
	
	Форма.ИспользоватьПодключаемоеОборудование = Ложь;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область СлужебныеПроцедурыИФункции_ОбработчикиСобытий

Процедура ПодключитьОборудование(Форма, ПодключаемоеОборудование, Отказ = Ложь)
	
	ПодключаемоеОборудование = к2РаботаСПодключаемымОборудованиемВызовСервера.СтруктураОборудования(Форма.КнопкаУчетнойТочки);
	
	Если ПроверитьПодключение_ЭлектронныеВесы(Форма, ПодключаемоеОборудование.Весы, Отказ) Тогда
		Подключить_ЭлектронныеВесы(Форма, ПодключаемоеОборудование.Весы, Отказ);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОтключитьОборудование(Форма, ПодключаемоеОборудование)
	
	Отключить_ЭлектронныеВесы(Форма, ПодключаемоеОборудование.Весы);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции_ЭлектронныеВесы

Функция ПроверитьПодключение_ЭлектронныеВесы(Форма, Весы, Отказ)
	
	Если Не Весы.Использовать Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если Весы.СоответствиеПодключений.Количество() = 0
		Или Весы.СоответствиеПодключений.Получить(Весы.Выбранные) = Неопределено Тогда
		
		ТекстСообщения = НСтр("ru='Некорректная структура настроек весов.'");
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстСообщения, Отказ);
		
	КонецЕсли;
	
	Если Не Отказ Тогда
		
		ИспользоватьПодключаемоеОборудование = Форма.ИспользоватьПодключаемоеОборудование;
		
		ВыбранныеВесы = Весы.Выбранные;
		
		Для Каждого Элемент Из Весы.СоответствиеПодключений Цикл
			
			Весы.Выбранные = Элемент.Ключ;
			
			ПроверитьДоступность_ЭлектронныеВесы(Форма, Весы, Ложь, Отказ);
			
		КонецЦикла;
		
		Весы.Выбранные = ВыбранныеВесы;
		
		Форма.ИспользоватьПодключаемоеОборудование = ИспользоватьПодключаемоеОборудование;
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

Функция ПодключитьПриНеобходимости_ЭлектронныеВесы(Форма, Весы)
	
	Если Не Весы.Выбранные = Весы.Подключенные Тогда
		
		Отключить_ЭлектронныеВесы(Форма, Весы);
		Подключить_ЭлектронныеВесы(Форма, Весы);
		
	КонецЕсли;
	
	СтруктураПодключения = Весы.СоответствиеПодключений.Получить(Весы.Выбранные);
	
	Возврат СтруктураПодключения;
	
КонецФункции

Процедура Подключить_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы)
	
	Если Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.ScalesMassaK") Тогда
		
		Подключить_ЭлектронныеВесы_ScalesMassaK(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.БПО") Тогда
		
		Подключить_ЭлектронныеВесы_БПО(Форма, Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.Tenso_m") Тогда
		
		Подключить_ЭлектронныеВесы_Tenso(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.МассаКДрайверР") Тогда
		
		Подключить_ЭлектронныеВесы_МассаКДрайверР(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.CAS") Тогда
		
		Подключить_ЭлектронныеВесы_CAS(Весы);
		
	Иначе
		
		к2РаботаСПодключаемымОборудованиемКлиентПереопределяемый.Подключить_ЭлектронныеВесы_ПоДрайверу(Драйвер, Весы);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы)
	
	Если Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.ScalesMassaK") Тогда
		
		Отключить_ЭлектронныеВесы_ScalesMassaK(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.БПО") Тогда
		
		Отключить_ЭлектронныеВесы_БПО(Форма, Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.Tenso_m") Тогда
		
		Отключить_ЭлектронныеВесы_Tenso(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.МассаКДрайверР") Тогда
		
		Отключить_ЭлектронныеВесы_МассаКДрайверР(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.CAS") Тогда
		
		Отключить_ЭлектронныеВесы_CAS(Весы);
		
	Иначе
		
		к2РаботаСПодключаемымОборудованиемКлиентПереопределяемый.Отключить_ЭлектронныеВесы_ПоДрайверу(Драйвер, Весы);
		
	КонецЕсли;
	
КонецПроцедуры

Функция ПрочитатьВес_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы)
	
	Вес = 0;
	
	Если Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.ScalesMassaK") Тогда
		
		Вес = ПрочитатьВес_ЭлектронныеВесы_ScalesMassaK(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.БПО") Тогда
		
		Вес = ПрочитатьВес_ЭлектронныеВесы_БПО(Форма, Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.Tenso_m") Тогда
		
		Вес = ПрочитатьВес_ЭлектронныеВесы_Tenso(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.МассаКДрайверР") Тогда
		
		Вес = ПрочитатьВес_ЭлектронныеВесы_МассаКДрайверР(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.CAS") Тогда
		
		Вес = ПрочитатьВес_ЭлектронныеВесы_CAS(Весы);
		
	Иначе
		
		Вес = к2РаботаСПодключаемымОборудованиемКлиентПереопределяемый.ПрочитатьВес_ЭлектронныеВесы_ПоДрайверу(Драйвер, Весы);
		
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

Функция ПрочитатьВесТары_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы)
	
	ВесТары = 0;
	
	Если Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.ScalesMassaK") Тогда
		
		ВесТары = ПрочитатьВесТары_ЭлектронныеВесы_ScalesMassaK(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.МассаКДрайверР") Тогда
		
		ВесТары = ПрочитатьВесТары_ЭлектронныеВесы_МассаКДрайверР(Весы);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.CAS") Тогда
		
		ВесТары = ПрочитатьВесТары_ЭлектронныеВесы_CAS(Весы);
		
	Иначе
		
		ВесТары = к2РаботаСПодключаемымОборудованиемКлиентПереопределяемый.ПрочитатьВесТары_ЭлектронныеВесы_ПоДрайверу(Драйвер, Весы);
		
	КонецЕсли;
	
	Возврат ВесТары;
	
КонецФункции

Процедура УстановитьВесТары_ЭлектронныеВесы_ПоДрайверу(Форма, Драйвер, Весы, ВесТары)
	
	Если Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.ScalesMassaK") Тогда
		
		УстановитьВесТары_ЭлектронныеВесы_ScalesMassaK(Весы, ВесТары);
		
	ИначеЕсли Драйвер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.МассаКДрайверР") Тогда
		
		УстановитьВесТары_ЭлектронныеВесы_МассаКДрайверР(Весы, ВесТары);
		
	Иначе
		
		к2РаботаСПодключаемымОборудованиемКлиентПереопределяемый.УстановитьВесТары_ЭлектронныеВесы_ПоДрайверу(Драйвер, Весы, ВесТары);
		
	КонецЕсли;
	
КонецПроцедуры

#Область Драйвер_ScalesMassaK

Процедура Подключить_ЭлектронныеВесы_ScalesMassaK(Весы)
	
	Весы.Подключенные = Весы.Выбранные;
	
	СтруктураПараметров = Весы.СоответствиеПодключений.Получить(Весы.Подключенные);
	
	Если СтруктураПараметров.ТипСвязи = ПредопределенноеЗначение("Перечисление.к2ТипыСвязиЭлектронныеВесы.RS232") Тогда
		СтрокаПодключения = "COM" + Формат(СтруктураПараметров.НомерПорта, "ЧГ=");
	Иначе
		СтрокаПодключения = СтруктураПараметров.Адрес;
	КонецЕсли;
	
	Весы.Менеджер = СоздатьМенеджер_ЭлектронныеВесы("ScalesMassaK.Scale");
	Весы.Менеджер.Connection = СтрокаПодключения;
	
	РезультатОткрытияСоединения = Весы.Менеджер.OpenConnection();
	
	Если Не РезультатОткрытияСоединения = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_ScalesMassaK(РезультатОткрытияСоединения);
	КонецЕсли;
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_ScalesMassaK(Весы)
	
	РезультатЗакрытияСоединения = Весы.Менеджер.CloseConnection();
	
	Если Не РезультатЗакрытияСоединения = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_ScalesMassaK(РезультатЗакрытияСоединения);
	КонецЕсли;
	
КонецПроцедуры

Функция ПрочитатьВес_ЭлектронныеВесы_ScalesMassaK(Весы)
	
	Вес = 0;
	
	РезультатЧтенияВеса = Весы.Менеджер.ReadWeight(); // Модифицирует Weight (Вес), Division (Размерность), Stable (Вес стабилен)
	
	Если РезультатЧтенияВеса = 0 Тогда
		
		Весы.ВесСтабилен = Весы.Менеджер.Stable = 1;
		Весы.ТекущееЗначениеВеса = КоэффициентПересчета_ЭлектронныеВесы(Весы.Менеджер.Division) * Весы.Менеджер.Weight;
		
		Если Весы.ВесСтабилен Тогда
			Вес = Весы.ТекущееЗначениеВеса;
		КонецЕсли;
		
	Иначе
		
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_ScalesMassaK(РезультатЧтенияВеса);
		
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

Функция ПрочитатьВесТары_ЭлектронныеВесы_ScalesMassaK(Весы)

	ВесТары = 0;
	
	РезультатЧтенияВесаТары = Весы.Менеджер.ReadTare(); // Модифицирует Tare (Вес тары), Division (Размерность)
	
	Если РезультатЧтенияВесаТары = 0 Тогда
		ВесТары = КоэффициентПересчета_ЭлектронныеВесы(Весы.Менеджер.Division) * Весы.Менеджер.Tare;
	Иначе
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_ScalesMassaK(РезультатЧтенияВесаТары);
	КонецЕсли;
	
	Возврат ВесТары;
	
КонецФункции

Процедура УстановитьВесТары_ЭлектронныеВесы_ScalesMassaK(Весы, ВесТары)
	
	ПрочитатьВесТары_ЭлектронныеВесы_ScalesMassaK(Весы);
	
	Весы.Менеджер.Tare = ВесТары / КоэффициентПересчета_ЭлектронныеВесы(Весы.Менеджер.Division);
	
	РезультатУстановкиВеса = Весы.Менеджер.SetTare();
	
	Если Не РезультатУстановкиВеса = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_ScalesMassaK(РезультатУстановкиВеса);
	КонецЕсли;
	
КонецПроцедуры

Функция КоэффициентПересчета_ЭлектронныеВесы(Размерность)
	
	Если Размерность = 0 Тогда
		КоэффициентПересчета = 0.000001;
	ИначеЕсли Размерность = 1 Тогда
		КоэффициентПересчета = 0.001;
	Иначе
		КоэффициентПересчета = 1;
	КонецЕсли;
	
	Возврат КоэффициентПересчета;
	
КонецФункции

Процедура ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_ScalesMassaK(КодОшибки)
	
	РасшифровкаКодаОшибки = РасшифровкаКодаОшибки_ЭлектронныеВесы_ScalesMassaK();
	ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы(КодОшибки, РасшифровкаКодаОшибки);
	
КонецПроцедуры

Функция РасшифровкаКодаОшибки_ЭлектронныеВесы_ScalesMassaK()
	
	// Драйвер не поддерживается
	Соответствие = Новый Соответствие;
	Соответствие.Вставить(1, НСтр("ru='Связь с весами не установлена'"));
	Соответствие.Вставить(2, НСтр("ru='Ошибка обмена с весами'"));
	Соответствие.Вставить(3, НСтр("ru='Весы не готовы к передаче данных'"));
	Соответствие.Вставить(4, НСтр("ru='Параметр не поддерживается весами'"));
	Соответствие.Вставить(5, НСтр("ru='Установка параметра невозможна'"));
	
	Возврат Соответствие;
	
КонецФункции

#КонецОбласти

#Область Драйвер_БПО

Процедура Подключить_ЭлектронныеВесы_БПО(Форма, Весы)
	
	Если Форма.ИспользоватьПодключаемоеОборудование Тогда
		
		ОповещениеПриПодключении = Новый ОписаниеОповещения("Подключить_ЭлектронныеВесы_БПО_Завершение", ЭтотОбъект, Весы);
		МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(ОповещениеПриПодключении, Форма, "ЭлектронныеВесы");
		
	Иначе
		
		Структура = Новый Структура;
		Структура.Вставить("Результат", Ложь);
		Структура.Вставить("ОписаниеОшибки", НСтр("ru='Настройте подключение к весам через БПО.'"));
		
		Подключить_ЭлектронныеВесы_БПО_Завершение(Структура, Весы);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура Подключить_ЭлектронныеВесы_БПО_Завершение(РезультатВыполнения, Весы) Экспорт
	
	Если РезультатВыполнения.Результат Тогда
		
		Весы.Подключенные = Весы.Выбранные;
		Весы.Менеджер = ПредопределенноеЗначение("Перечисление.к2ТипыДрайвераЭлектронныеВесы.БПО");
		
	Иначе
		
		Весы.Подключенные = Неопределено;
		Весы.Менеджер = Неопределено;
		
		ШаблонОшибки = НСтр("ru='При подключении оборудования произошла ошибка:""%1"".'");
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонОшибки, РезультатВыполнения.ОписаниеОшибки);
		ВызватьИсключение ТекстОшибки;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_БПО(Форма, Весы)
	
	ОповещениеПриОтключении = Новый ОписаниеОповещения("Отключить_ЭлектронныеВесы_БПО_Завершение", ЭтотОбъект, Весы);
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПоТипу(ОповещениеПриОтключении
		, Форма.УникальныйИдентификатор
		, СтрРазделить("ЭлектронныеВесы", ","));
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_БПО_Завершение(РезультатВыполнения, Весы) Экспорт
	
	Если Не РезультатВыполнения.Результат Тогда
		
		ШаблонОшибки = НСтр("ru='При отключении оборудования произошла ошибка: ""%1"".'");
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонОшибки, РезультатВыполнения.ОписаниеОшибки);
		ВызватьИсключение ТекстОшибки;
		
	КонецЕсли;
	
КонецПроцедуры

Функция ПрочитатьВес_ЭлектронныеВесы_БПО(Форма, Весы)
	
	Вес = 0;
	
	Структура = Новый Структура;
	Структура.Вставить("Вес", 0);
	
	ОписаниеОповещениеПриЗавершении = Новый ОписаниеОповещения("ПрочитатьВес_ЭлектронныеВесы_БПО_Завершение", ЭтотОбъект, Структура);
	МенеджерОборудованияКлиент.НачатьПолученияВесаСЭлектронныхВесов(ОписаниеОповещениеПриЗавершении
		, Форма.УникальныйИдентификатор
		, Весы.Подключенные);
	
	Весы.ВесСтабилен = Истина;
	Весы.ТекущееЗначениеВеса = Структура.Вес;
	Вес = Весы.ТекущееЗначениеВеса;
	
	Возврат Вес;
	
КонецФункции

Процедура ПрочитатьВес_ЭлектронныеВесы_БПО_Завершение(РезультатВыполнения, Структура) Экспорт
	
	Если РезультатВыполнения.Результат Тогда
		Структура.Вес = РезультатВыполнения.Вес;
	Иначе
		Структура.Вес = 0;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Драйвер_Tenso

Процедура Подключить_ЭлектронныеВесы_Tenso(Весы)
	
	Весы.Подключенные = Весы.Выбранные;
	Весы.Менеджер = СоздатьМенеджер_ЭлектронныеВесы("Controller.ScAuto");
	
	ПрочитатьВес_ЭлектронныеВесы_Tenso(Весы);
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_Tenso(Весы)
	
	Весы.Подключенные = Неопределено;
	Весы.Менеджер = Неопределено;
	
КонецПроцедуры

Функция ПрочитатьВес_ЭлектронныеВесы_Tenso(Весы)
	
	Вес = 0;
	
	СтруктураПараметров = Весы.СоответствиеПодключений.Получить(Весы.Подключенные);
	
	Весы.ВесСтабилен = ВесСтабилен_ЭлектронныеВесы_Tenso(Весы, СтруктураПараметров);
	Весы.ТекущееЗначениеВеса = ТекущееЗначениеВеса_ЭлектронныеВесы_Tenso(Весы, СтруктураПараметров);
	
	Если Весы.ВесСтабилен Тогда
		Вес = Весы.ТекущееЗначениеВеса;
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

Функция ВесСтабилен_ЭлектронныеВесы_Tenso(Весы, СтруктураПараметров)
	
	ВесСтабилен = Ложь;
	
	Если СтруктураПараметров.ТипСвязи = ПредопределенноеЗначение("Перечисление.к2ТипыСвязиЭлектронныеВесы.RS232") Тогда
		Результат = Весы.Менеджер.GetStatus(СтруктураПараметров.НомерПорта, СтруктураПараметров.НомерКанала);
	Иначе 
		Результат = Весы.Менеджер.GetStatusNet(СтруктураПараметров.Адрес);
	КонецЕсли;
	
	РезультатСтрокой = ЗначениеСтрокой_ЭлектронныеВесы_Tenso(Результат);
	
	Если РезультатСтрокой = "-5002" Тогда
		ВесСтабилен = Истина;
	ИначеЕсли РезультатСтрокой = "-5003" Или РезультатСтрокой = "0" Тогда
		ВесСтабилен = Ложь;
	Иначе
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_Tenso(РезультатСтрокой);
	КонецЕсли;
	
	Возврат ВесСтабилен;
	
КонецФункции

Функция ТекущееЗначениеВеса_ЭлектронныеВесы_Tenso(Весы, СтруктураПараметров)
	
	Вес = 0;
	
	Если СтруктураПараметров.ТипСвязи = ПредопределенноеЗначение("Перечисление.к2ТипыСвязиЭлектронныеВесы.RS232") Тогда
		Результат = Весы.Менеджер.GetWeight(СтруктураПараметров.НомерПорта, СтруктураПараметров.НомерКанала);
	Иначе
		Результат = Весы.Менеджер.GetWeightNet(СтруктураПараметров.Адрес);
	КонецЕсли;
	
	РезультатСтрокой = ЗначениеСтрокой_ЭлектронныеВесы_Tenso(Результат);
	РезультатЧислом = СтроковыеФункцииКлиентСервер.СтрокаВЧисло(РезультатСтрокой);
	
	Если РезультатЧислом >= 0 Тогда
		Вес = РезультатЧислом;
	Иначе
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_Tenso(РезультатСтрокой);
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

Функция ЗначениеСтрокой_ЭлектронныеВесы_Tenso(Значение)
	
	// По документации статус/ошибка - тип строка
	// По факту некоторые весы возвращают число
	Если ТипЗнч(Значение) = Тип("Строка") Тогда
		ЗначениеСтрокой = Значение;
	Иначе
		ЗначениеСтрокой = Формат(Значение, "ЧН=0; ЧГ=; ЧФ=Ч;");
	КонецЕсли;
	
	Возврат ЗначениеСтрокой;
	
КонецФункции

Процедура ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_Tenso(КодОшибки)
	
	РасшифровкаКодаОшибки = РасшифровкаКодаОшибки_ЭлектронныеВесы_Tenso();
	ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы(КодОшибки, РасшифровкаКодаОшибки);
	
КонецПроцедуры

Функция РасшифровкаКодаОшибки_ЭлектронныеВесы_Tenso()
	
	Соответствие = Новый Соответствие;
	Соответствие.Вставить("-5000", НСтр("ru='терминал не обнаружен'"));
	Соответствие.Вставить("-5001", НСтр("ru='с терминалом нет связи'"));
	Соответствие.Вставить("-5002", НСтр("ru='вес стабилен для  метода GetStatus'"));
	Соответствие.Вставить("-5003", НСтр("ru='вес не стабилен для метода GetStatus'"));
	Соответствие.Вставить("-5006", НСтр("ru='не удалось открыть Com-порт'"));
	Соответствие.Вставить("-5007", НСтр("ru='ошибка ключа защиты на сервере'"));
	Соответствие.Вставить("-5008", НСтр("ru='сервер не отвечает'"));
	
	Возврат Соответствие;
	
КонецФункции

#КонецОбласти

#Область Драйвер_МассаКДрайверР

Процедура Подключить_ЭлектронныеВесы_МассаКДрайверР(Весы)
	
	Весы.Подключенные = Весы.Выбранные;
	
	СтруктураПараметров = Весы.СоответствиеПодключений.Получить(Весы.Подключенные);
	
	Если СтруктураПараметров.ТипСвязи = ПредопределенноеЗначение("Перечисление.к2ТипыСвязиЭлектронныеВесы.RS232") Тогда
		СтрокаПодключения = Формат(СтруктураПараметров.НомерПорта, "ЧГ=");
	Иначе
		СтрокаПодключения = СтруктураПараметров.Адрес + ":" + Формат(СтруктураПараметров.НомерПорта, "ЧГ=");
	КонецЕсли;
	
	Весы.Менеджер = СоздатьМенеджер_ЭлектронныеВесы("TerminalMassaK.Device");
	Весы.Менеджер.Connection = СтрокаПодключения;
	Весы.Менеджер.Add();
	
	РезультатОткрытияСоединения = Весы.Менеджер.OpenConnection();
	
	Если Не РезультатОткрытияСоединения = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_МассаКДрайверР(РезультатОткрытияСоединения);
	КонецЕсли;
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_МассаКДрайверР(Весы)
	
	РезультатЗакрытияСоединения = Весы.Менеджер.CloseConnection();
	
	Если Не РезультатЗакрытияСоединения = 0 Тогда 
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_МассаКДрайверР(РезультатЗакрытияСоединения);
	КонецЕсли;
	
КонецПроцедуры

Функция ПрочитатьВес_ЭлектронныеВесы_МассаКДрайверР(Весы)
	
	Вес = 0;
	
	РезультатЧтенияВеса = Весы.Менеджер.GetWeight(); // Модифицирует Weight (Вес), Stable (Вес стабилен).
	
	Если РезультатЧтенияВеса = 0 Тогда
		
		Весы.ВесСтабилен = Весы.Менеджер.Stable = 1;
		Весы.ТекущееЗначениеВеса = Весы.Менеджер.Weight / ГраммВКилограмме(); // Вес в граммах. Пересчет в кг.
		
		Если Весы.ВесСтабилен Тогда
			Вес = Весы.ТекущееЗначениеВеса;
		КонецЕсли;
		
	Иначе
		
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_МассаКДрайверР(РезультатЧтенияВеса);
		
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

Функция ПрочитатьВесТары_ЭлектронныеВесы_МассаКДрайверР(Весы)

	ВесТары = 0;
	
	РезультатЧтенияВесаТары = Весы.Менеджер.GetTare(); // Модифицирует TareWeight (Вес тары)
	
	Если РезультатЧтенияВесаТары = 0 Тогда
		
		ВесТары = Весы.Менеджер.TareWeight / ГраммВКилограмме();
		
	Иначе
		
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_МассаКДрайверР(РезультатЧтенияВесаТары);
		
	КонецЕсли;
	
	Возврат ВесТары;
	
КонецФункции

Процедура УстановитьВесТары_ЭлектронныеВесы_МассаКДрайверР(Весы, ВесТары)
	
	ПрочитатьВесТары_ЭлектронныеВесы_МассаКДрайверР(Весы);
	
	Весы.Менеджер.TareWeight = ВесТары * ГраммВКилограмме();
	
	РезультатУстановкиВеса = Весы.Менеджер.SetTare();
	
	Если Не РезультатУстановкиВеса = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_МассаКДрайверР(РезультатУстановкиВеса);
	КонецЕсли;
	
КонецПроцедуры

Функция ГраммВКилограмме()
	
	Возврат 1000;
	
КонецФункции

Процедура ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_МассаКДрайверР(КодОшибки)
	
	РасшифровкаКодаОшибки = РасшифровкаКодаОшибки_ЭлектронныеВесы_МассаКДрайверР();
	ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы(КодОшибки, РасшифровкаКодаОшибки);
	
КонецПроцедуры

Функция РасшифровкаКодаОшибки_ЭлектронныеВесы_МассаКДрайверР()
	
	// "МАССА-К: Драйвер R". V3.2 - Редакция 12 (2020)
	Соответствие = Новый Соответствие;
	Соответствие.Вставить(-1, НСтр("ru='Драйвер занят'"));
	Соответствие.Вставить(-2, НСтр("ru='Файл не найден'"));
	Соответствие.Вставить(-3, НСтр("ru='Терминал не найден'"));
	Соответствие.Вставить(-4, НСтр("ru='Ошибка подключения к терминалу'"));
	Соответствие.Вставить(-5, НСтр("ru='Ошибка передачи данных в терминал'"));
	Соответствие.Вставить(-6, НСтр("ru='Ошибка приема данных из терминала'"));
	Соответствие.Вставить(-7, НСтр("ru='Ошибка обработки данных регистрации'"));
	Соответствие.Вставить(-8, НСтр("ru='Ошибка установки режима работы драйвера'"));
	Соответствие.Вставить(-9, НСтр("ru='-9 данный код ошибки не используется в текущей версии драйвера'"));
	Соответствие.Вставить(-10, НСтр("ru='Нет места для записи данных в буфер драйвера'"));
	Соответствие.Вставить(-11, НСтр("ru='Не хватает памяти в терминале для загрузки такого объема данных'"));
	Соответствие.Вставить(-12, НСтр("ru='В справочнике больше нет записей'"));
	Соответствие.Вставить(-13, НСтр("ru='На USB-flash больше нет файлов регистраций'"));
	Соответствие.Вставить(-14, НСтр("ru='Идет сжатие данных, пожалуйста, подождите'"));
	Соответствие.Вставить(-15, НСтр("ru='Сжатие данных завершено'"));
	Соответствие.Вставить(-1000, НСтр("ru='Неверное значение свойства'"));
	Соответствие.Вставить(-1001, НСтр("ru='-1001 данный код ошибки не используется в текущей версии драйвера'"));
	
	Возврат Соответствие;
	
КонецФункции

#КонецОбласти

#Область Драйвер_CAS

Процедура Подключить_ЭлектронныеВесы_CAS(Весы)
	
	Весы.Подключенные = Весы.Выбранные;
	
	СтруктураПараметров = Весы.СоответствиеПодключений.Получить(Весы.Подключенные);
	
	Весы.Менеджер = СоздатьМенеджер_ЭлектронныеВесы("CAScentre_DLL_printScale.Scale");
	Весы.Менеджер.Type = СтруктураПараметров.ТипВесов;
	
	Если СтруктураПараметров.ТипСвязи = ПредопределенноеЗначение("Перечисление.к2ТипыСвязиЭлектронныеВесы.RS232") Тогда
		
		Весы.Менеджер.IP = "COM" + Формат(СтруктураПараметров.НомерПорта, "ЧГ=");
		Весы.Менеджер.Port = СтруктураПараметров.СкоростьОбмена;
		
	Иначе
		
		Весы.Менеджер.IP = СтруктураПараметров.Адрес;
		Весы.Менеджер.Port = СтруктураПараметров.НомерПорта;
		
	КонецЕсли;
	
	Весы.Менеджер.Open();
	РезультатОткрытияСоединения = Весы.Менеджер.ResultCode;
	
	Если Не РезультатОткрытияСоединения = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_CAS(РезультатОткрытияСоединения);
	КонецЕсли;
	
КонецПроцедуры

Процедура Отключить_ЭлектронныеВесы_CAS(Весы)
	
	Весы.Менеджер.Close();
	РезультатЗакрытияСоединения = Весы.Менеджер.ResultCode;
	
	Если Не РезультатЗакрытияСоединения = 0 Тогда
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_CAS(РезультатЗакрытияСоединения);
	КонецЕсли;
	
КонецПроцедуры

Функция ПрочитатьВес_ЭлектронныеВесы_CAS(Весы)
	
	Вес = 0;
	
	// Модифицирует:
	// statusWeight - Строка - вес в килограммах с разделителем целой и дробной части. Текущий вес на весах
	// statusPrice - Строка - цена в рублях с разделителем целой и дробной части. Текущая цена на весах
	// statusWeightTare - Строка -  вес в килограммах с разделителем целой и дробной части. Текущий вес тары на весах
	// statusStable - Булево - стабильность веса
	// statusDept - Число - номер выбранного отдела
	// statusPlu - Число - номер выбранного товара на весах
	Весы.Менеджер.ReadCurrentStatus();
	РезультатЧтенияВеса = Весы.Менеджер.ResultCode;
	
	Если РезультатЧтенияВеса = 0 Тогда
		
		СтрокаОтрицательногоЗначенияСВесов = "4294967,244";
		
		ТекущееЗначениеВесаСтрокой = Весы.Менеджер.statusWeight;
		
		Если ТекущееЗначениеВесаСтрокой = СтрокаОтрицательногоЗначенияСВесов Тогда
			ТекущееЗначениеВесаСтрокой = "";
		КонецЕсли;
		
		Весы.ВесСтабилен = Весы.Менеджер.statusStable;
		Весы.ТекущееЗначениеВеса = СтроковыеФункцииКлиентСервер.СтрокаВЧисло(ТекущееЗначениеВесаСтрокой);
		
		Если Весы.ВесСтабилен Тогда
			Вес = Весы.ТекущееЗначениеВеса;
		КонецЕсли;
		
	Иначе
		
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_CAS(РезультатЧтенияВеса);
		
	КонецЕсли;
	
	Возврат Вес;
	
КонецФункции

Функция ПрочитатьВесТары_ЭлектронныеВесы_CAS(Весы)

	ВесТары = 0;
	
	// Модифицирует:
	// statusWeight - Строка - вес в килограммах с разделителем целой и дробной части. Текущий вес на весах
	// statusPrice - Строка - цена в рублях с разделителем целой и дробной части. Текущая цена на весах
	// statusWeightTare - Строка -  вес в килограммах с разделителем целой и дробной части. Текущий вес тары на весах
	// statusStable - Булево - стабильность веса
	// statusDept - Число - номер выбранного отдела
	// statusPlu - Число - номер выбранного товара на весах
	Весы.Менеджер.ReadCurrentStatus();
	РезультатЧтенияВесаТары = Весы.Менеджер.ResultCode;
	
	Если РезультатЧтенияВесаТары = 0 Тогда
		
		ВесТарыСтрокой = Весы.Менеджер.statusWeightTare;
		ВесТары = СтроковыеФункцииКлиентСервер.СтрокаВЧисло(ВесТарыСтрокой);
		
	Иначе
		
		ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_CAS(РезультатЧтенияВесаТары);
		
	КонецЕсли;
	
	Возврат ВесТары;
	
КонецФункции

Процедура ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы_CAS(КодОшибки)
	
	РасшифровкаКодаОшибки = РасшифровкаКодаОшибки_ЭлектронныеВесы_CAS();
	ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы(КодОшибки, РасшифровкаКодаОшибки);
	
КонецПроцедуры

Функция РасшифровкаКодаОшибки_ЭлектронныеВесы_CAS()
	
	// Руководство программиста ООО «КАСцентр», 2010-12-23
	Соответствие = Новый Соответствие;
	Соответствие.Вставить(0, НСтр("ru='Все прошло успешно.'"));
	Соответствие.Вставить(-1, НСтр("ru='Не удалось инициировать библиотеку.'"));
	Соответствие.Вставить(-2, НСтр("ru='Не удалось соединиться с весами.'"));
	Соответствие.Вставить(-3, НСтр("ru='Не удалось отправить товар на весы.'"));
	Соответствие.Вставить(-4, НСтр("ru='Не удалось отправить текст ингредиента на весы.'"));
	Соответствие.Вставить(-5, НСтр("ru='Не удалось удалить все товары из весов.'"));
	Соответствие.Вставить(-6, НСтр("ru='Не удалось отправить название магазина.'"));
	Соответствие.Вставить(-7, НСтр("ru='Не удалось отправить текущее время и дату.'"));
	Соответствие.Вставить(-8, НСтр("ru='Не удалось отправить информация о быстрых клавишах.'"));
	Соответствие.Вставить(-9, НСтр("ru='Не удалось отправить параметры печати этикетки.'"));
	Соответствие.Вставить(-10, НСтр("ru='Не удалось отправить или получить спец. настройки на весы CL5000(J).'"));
	Соответствие.Вставить(-11, НСтр("ru='Не удалось прочитать состояние весов.'"));
	Соответствие.Вставить(-12, НСтр("ru='Не удалось задать настройки весам LP-1.6 или LP-II'"));
	Соответствие.Вставить(-13, НСтр("ru='Не удалось отправить штрих-код в весы'"));
	Соответствие.Вставить(-14, НСтр("ru='Не удалось отправить рекламные сообщения'"));
	Соответствие.Вставить(-15, НСтр("ru='Не удалось отправить бегущие сообщения'"));
	Соответствие.Вставить(-16, НСтр("ru='Не удалось отправить Страну-производителя'"));
	Соответствие.Вставить(-17, НСтр("ru='Не удалось отправить единицу измерения'"));
	Соответствие.Вставить(-18, НСтр("ru='Нельзя отправить товар, т.к. в весах нет больше памяти'"));
	Соответствие.Вставить(-19, НСтр("ru='Не удалось отправить этикетку в весы'"));
	Соответствие.Вставить(-20, НСтр("ru='Не удалось прочитать файл с этикеткой'"));
	Соответствие.Вставить(-21, НСтр("ru='Не удалось удалить товар'"));
	Соответствие.Вставить(-999, НСтр("ru='Неизвестная ошибка'"));
	
	Возврат Соответствие;
	
КонецФункции

#КонецОбласти

#Область Прочее_ЭлектронныеВесы

Процедура ОбработатьАвтоВзвешивание_ЭлектронныеВесы(Весы, Вес)
	
	СтруктураПараметров = Весы.СоответствиеПодключений.Получить(Весы.Подключенные);
	
	Если Весы.ТекущееЗначениеВеса <= СтруктураПараметров.ГраницаНовогоВзвешивания Тогда
		
		Весы.ВесЗафиксирован = Ложь;
		Вес = 0;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЭлементыФормыНастроить_ЭлектронныеВесы(Форма, Весы)
	
	ИмяИдентификатор = к2РаботаСПодключаемымОборудованиемКлиентСервер.ИмяРеквизита_ИдентификаторЭлектронныеВесы();
	ИмяСоответствие = к2РаботаСПодключаемымОборудованиемКлиентСервер.ИмяРеквизита_СоответствиеЭлектронныеВесы();
	
	ПодключаемыеВесы = Форма.Элементы.Найти(ИмяИдентификатор);
	
	Если ПодключаемыеВесы = Неопределено Тогда 
		Возврат;
	КонецЕсли;
	
	ПодключаемыеВесы.СписокВыбора.Очистить();
	ПодключаемыеВесы.Видимость = Не Весы.СоответствиеПодключений.Количество() = 0;
	
	Соответствие = Новый Соответствие;
	
	Для Каждого ЭлементСоответствия Из Весы.СоответствиеПодключений Цикл 
		
		Идентификатор = Строка(ЭлементСоответствия.Ключ.УникальныйИдентификатор());
		
		Соответствие.Вставить(Идентификатор, ЭлементСоответствия.Ключ);
		ПодключаемыеВесы.СписокВыбора.Добавить(Идентификатор, ЭлементСоответствия.Значение.Представление);
		
	КонецЦикла;
	
	Форма[ИмяСоответствие] = Новый ФиксированноеСоответствие(Соответствие);
	Форма[ИмяИдентификатор] = Строка(Весы.Выбранные.УникальныйИдентификатор());
	
КонецПроцедуры

Функция СоздатьМенеджер_ЭлектронныеВесы(Имя)
	
	Менеджер = Неопределено;
	
	СистемнаяИнформация = Новый СистемнаяИнформация;
	
	Если СистемнаяИнформация.ТипПлатформы = ТипПлатформы.Windows_x86
		Или СистемнаяИнформация.ТипПлатформы = ТипПлатформы.Windows_x86_64 Тогда
		
		#Если Не МобильныйКлиент Тогда
		Менеджер = Новый COMОбъект(Имя);
		#Иначе
		ВызватьИсключение НСтр("ru='Работа с драйвером весов не доступна на мобильном клиенте.'");
		#КонецЕсли
		
	Иначе
		
		ВызватьИсключение НСтр("ru='Работа с драйвером весов доступна только в Windows.'");
		
	КонецЕсли;
	
	Возврат Менеджер;
	
КонецФункции

Процедура ВызватьИсключениеПоКодуОшибки_ЭлектронныеВесы(КодОшибки, РасшифровкаКодаОшибки)
	
	ТекстОшибки = РасшифровкаКодаОшибки.Получить(КодОшибки);
	
	Если ТекстОшибки = Неопределено Тогда
		
		ШаблонОшибки = НСтр("ru = 'Отсутствует расшифровка кода ошибки ""%1"".'");
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонОшибки, КодОшибки);
		
	КонецЕсли;
	
	ВызватьИсключение ТекстОшибки;
	
КонецПроцедуры

Процедура ПоказатьПредупреждениеПользователю(Драйвер, Ошибка, ШаблонСообщения, Весы)
	
	КраткоеПредставление = КраткоеПредставлениеОшибки(Ошибка);
	ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения
		, Драйвер
		, Весы.Выбранные
		, КраткоеПредставление);
	
	к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстСообщения);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецОбласти
