#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Функция МассивИспользуемыхПараметров() Экспорт
	
	МассивПараметров = Новый Массив;
	
	Обработки.к2МенюУчетныхТочек.ДополнитьМассивОбщимиПараметрами(МассивПараметров);
	
	к2УчетныеТочкиПереопределяемый.МассивИспользуемыхПараметров(Метаданные.Обработки.к2УчетПоказателейСкладов.Имя, МассивПараметров);
	
	Возврат МассивПараметров;
	
КонецФункции

#КонецОбласти

#КонецЕсли