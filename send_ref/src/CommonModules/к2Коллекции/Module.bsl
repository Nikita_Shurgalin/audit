#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область РаботаСДеревомЗначений

Процедура ДобавитьЗаполнитьКолонкуГруппировки(пДерево, пОписаниеТиповГруппировки, пСоответствиеУровнюГруппировке, УдалятьСтрокиСПустойГруппировкой = Истина) Экспорт
	
	пДерево.Колонки.Добавить("Группировка", пОписаниеТиповГруппировки);
	
	ЗаполнитьКолонкуГруппировки(пДерево.Строки, пСоответствиеУровнюГруппировке);
	
	Если УдалятьСтрокиСПустойГруппировкой Тогда 
		УдалитьСтрокиСПустойГруппировкой(пДерево.Строки);
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьКолонкуГруппировки(пСтроки, пСоответствиеУровнюГруппировке, Знач пУровень = 1) Экспорт 
	
	Для Каждого цСтрока Из пСтроки Цикл
		
		имяПоляГруппировки = пСоответствиеУровнюГруппировке.Получить(пУровень);
		цСтрока.Группировка = цСтрока[имяПоляГруппировки];
		
		ЗаполнитьКолонкуГруппировки(цСтрока.Строки, пСоответствиеУровнюГруппировке, пУровень + 1);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура УдалитьСтрокиСПустойГруппировкой(пСтроки) Экспорт
	
	строкиКУдалению = Новый Массив;
	
	Для Каждого цСтрока Из пСтроки Цикл
		
		Если НЕ ЗначениеЗаполнено(цСтрока.Группировка) Тогда
			строкиКУдалению.Добавить(цСтрока);
		КонецЕсли;
		
		УдалитьСтрокиСПустойГруппировкой(цСтрока.Строки);
		
	КонецЦикла;
	
	Для Каждого цСтрока Из строкиКУдалению Цикл
		пСтроки.Удалить(цСтрока);
	КонецЦикла;
	
КонецПроцедуры

Процедура ДобавитьЗаполнитьКолонкуУровень(пДерево) Экспорт
	
	пДерево.Колонки.Добавить("Уровень", Новый ОписаниеТипов("Число"));
	
	ЗаполнитьКолонкуУровень(пДерево.Строки);
	
КонецПроцедуры

Процедура СкопироватьСтрокиДерева(Знач СтрокаПолучатель, Знач СтрокаИсточник) Экспорт
	
	Для каждого стр Из СтрокаИсточник.Строки Цикл
		НовСтр = СтрокаПолучатель.Строки.Добавить();
		ЗаполнитьЗначенияСвойств(НовСтр, стр);
		СкопироватьСтрокиДерева(НовСтр, стр);
	КонецЦикла;
	
КонецПроцедуры

Процедура СкопироватьСтрокиДереваСУстановкойДетальныхЗаписей(Знач СтрокаПолучатель, Знач СтрокаИсточник) Экспорт
	
	Для каждого стр Из СтрокаИсточник.Строки Цикл
		НовСтр = СтрокаПолучатель.Строки.Добавить();
		ЗаполнитьЗначенияСвойств(НовСтр, стр);
		Если стр.Строки.Количество() = 0 Тогда 
			НовСтр.ЭтоСтрокаДетальныхЗаписей = Истина;
		КонецЕсли;
		СкопироватьСтрокиДереваСУстановкойДетальныхЗаписей(НовСтр, стр);
	КонецЦикла;
	
КонецПроцедуры

// Возвращает таблицу, состоящую из строк-листьев дерева.
//
// Параметры:
//  Дерево - ДеревоЗначений - Исходное дерево.
// 
// Возвращаемое значение:
//  ТаблицаЗначений.
//
Функция ДеревоВТаблицу(Дерево) Экспорт
	
	Таблица = Новый ТаблицаЗначений;
	
	Для Каждого Колонка Из Дерево.Колонки Цикл
		Таблица.Колонки.Добавить(Колонка.Имя, Колонка.ТипЗначения);
	КонецЦикла;
	
	Для Каждого Лист Из ЛистьяДерева(Дерево) Цикл
		ЗаполнитьЗначенияСвойств(Таблица.Добавить(), Лист);
	КонецЦикла;
	
	Возврат Таблица;
	
КонецФункции

// Возвращает массив строк-листьев дерева.
//
// Параметры:
//  Узел - ДеревоЗначений, СтрокаДереваЗначений - Владелец листьев.
// 
// Возвращаемое значение:
//  Массив из СтрокаДереваЗначений.
//
Функция ЛистьяДерева(Узел) Экспорт
	
	Листья = Новый Массив;
	
	Если Узел.Строки.Количество() = 0 И ТипЗнч(Узел) = Тип("СтрокаДереваЗначений") Тогда
		
		Листья.Добавить(Узел);
		
	Иначе
		
		Для Каждого ПодчиненныйУзел Из Узел.Строки Цикл
			
			Для Каждого Лист Из ЛистьяДерева(ПодчиненныйУзел) Цикл
				Листья.Добавить(Лист);
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат Листья;
	
КонецФункции

// Возвращает массив всех подчиненных строк дерева.
//
// Параметры:
//  пСтрокаДерева - ДеревоЗначений, СтрокаДереваЗначений - строка или дерево, подчиненные строки которых нужно получить.
//
// Возвращаемое значение:
//  Массив из СтрокаДереваЗначений - подчиненные строки.
//
Функция ВсеПодчиненныеСтроки(Знач пСтрокаДерева, пМассивПодчиненныхСтрок = Неопределено) Экспорт

	Если пМассивПодчиненныхСтрок = Неопределено Тогда
		
		пМассивПодчиненныхСтрок = Новый Массив;
		
	КонецЕсли;
	
	Для каждого цПодчиненнаяСтрока Из пСтрокаДерева.Строки Цикл
		
		пМассивПодчиненныхСтрок.Добавить(цПодчиненнаяСтрока);
		
		ВсеПодчиненныеСтроки(цПодчиненнаяСтрока, пМассивПодчиненныхСтрок);
		
	КонецЦикла;
	
	Возврат пМассивПодчиненныхСтрок;

КонецФункции

#Область ЗаполнениеПолейВДереве

// Заполняет поля в дереве для каждого уровня. Каждый уровень рассчитывается на основе предыдущего - от листьев к корню.
// 
// Параметры:
// 	пДерево - ДеревоЗначений - Дерево
// 	пПоля - Строка, Массив Из Строка - поля, по которым выполняется заполнение
// 	пАгрегатнаяФункция - Строка - Используемая агрегатная функция. Возможные значения: "Сумма", "Мин", "Макс" и "Среднее"
Процедура ЗаполнитьПоляСтрокамДереваСнизуВверх(пДерево, Знач пПоля, Знач пАгрегатнаяФункция = "Сумма") Экспорт
	
	нРегАгрегат = НРег(пАгрегатнаяФункция);

	агрегатКорректен = нРегАгрегат = "сумма" Или нРегАгрегат = "мин" Или нРегАгрегат = "макс" Или нРегАгрегат = "среднее";

	ОбщегоНазначенияКлиентСервер.Проверить(агрегатКорректен,
										   "Передана некорректная агрегатная функция",
										   "к2Коллекции.ЗаполнитьПоляСтрокамДереваСнизуВверх");

	Если ТипЗнч(пПоля) = Тип("Строка") Тогда

		массивПолей = СтрРазделить(пПоля, ",", Ложь);

	Иначе

		массивПолей = пПоля;

	КонецЕсли;
	
	Для каждого цСтрокаДерева Из пДерево.Строки Цикл
		
		ЗаполнитьПоляПодчиненнымСтрокамДереваВверх(цСтрокаДерева, массивПолей, пАгрегатнаяФункция);
		
	КонецЦикла;
	
КонецПроцедуры

// Определяет сумму значений в подчиненных строках дерева значений по указанной колонке.
// 
// Параметры:
// 	пСтрокаДерева - СтрокаДереваЗначений - Строка дерева 
// 	пИмяКолонки - Строка - Имя колонки
// Возвращаемое значение:
// 	Число - сумма
Функция СуммаПодчиненныхСтрокДЗ(Знач пСтрокаДерева, Знач пИмяКолонки) Экспорт
	
	Сумма = 0;
	
	Для Каждого ПодчиненнаяСтрока Из пСтрокаДерева.Строки Цикл 
		
		Сумма = Сумма + ПодчиненнаяСтрока[пИмяКолонки];
		
	КонецЦикла;
	
	Возврат Сумма;
	
КонецФункции

// Определяет среднее значение в подчиненных строках дерева значений по указанной колонке.
// 
// Параметры:
// 	пСтрокаДерева - СтрокаДереваЗначений - Строка дерева 
// 	пИмяКолонки - Строка - Имя колонки
// 	пЗначениеПоУмолчанию - Число - Значение, которое будет возвращено, если подчиненных строк нет
// Возвращаемое значение:
// 	Число - минимальное значение
Функция СреднееПодчиненныхСтрокДЗ(Знач пСтрокаДерева, Знач пИмяКолонки, Знач пЗначениеПоУмолчанию = 0) Экспорт
	
	количествоСтрок = пСтрокаДерева.Строки.Количество();
	
	Если количествоСтрок = 0 Тогда
		
		Возврат пЗначениеПоУмолчанию;
		
	КонецЕсли;
	
	Возврат СуммаПодчиненныхСтрокДЗ(пСтрокаДерева, пИмяКолонки) / количествоСтрок;
	
КонецФункции

// Определяет минимальное значение в подчиненных строках дерева значений по указанной колонке.
// 
// Параметры:
// 	пСтрокаДерева - СтрокаДереваЗначений - Строка дерева 
// 	пИмяКолонки - Строка - Имя колонки
// 	пЗначениеПоУмолчанию - Число, Дата, Булево, Строка - Значение, которое будет возвращено, если подчиненных строк нет
// Возвращаемое значение:
// 	Число, Дата, Булево, Строка - минимальное значение
Функция МинимумПодчиненныхСтрокДЗ(Знач пСтрокаДерева, Знач пИмяКолонки, Знач пЗначениеПоУмолчанию = Неопределено) Экспорт
	
	строкиДерева = пСтрокаДерева.Строки;
	
	Если строкиДерева.Количество() = 0 Тогда
		
		Возврат пЗначениеПоУмолчанию;
		
	КонецЕсли;
	
	минЗначение = строкиДерева[0][пИмяКолонки];
	
	Для Каждого цСтрокаДерева Из строкиДерева Цикл 
		
		минЗначение = Мин(минЗначение, цСтрокаДерева[пИмяКолонки]);
	
	КонецЦикла;
	
	Возврат минЗначение;
	
КонецФункции

// Определяет максимальное значение в подчиненных строках дерева значений по указанной колонке.
// 
// Параметры:
// 	пСтрокаДерева - СтрокаДереваЗначений - Строка дерева 
// 	пИмяКолонки - Строка - Имя колонки
// 	пЗначениеПоУмолчанию - Число, Дата, Булево, Строка - Значение, которое будет возвращено, если подчиненных строк нет
// Возвращаемое значение:
// 	Число, Дата, Булево, Строка - максимальное значение
Функция МаксимумПодчиненныхСтрокДЗ(Знач пСтрокаДерева, Знач пИмяКолонки, Знач пЗначениеПоУмолчанию = Неопределено) Экспорт
	
	строкиДерева = пСтрокаДерева.Строки;
	
	Если строкиДерева.Количество() = 0 Тогда
		
		Возврат пЗначениеПоУмолчанию;
		
	КонецЕсли;
	
	минЗначение = строкиДерева[0][пИмяКолонки];
	
	Для Каждого цСтрокаДерева Из строкиДерева Цикл 
		
		минЗначение = Макс(минЗначение, цСтрокаДерева[пИмяКолонки]);
	
	КонецЦикла;
	
	Возврат минЗначение;
	
КонецФункции

#КонецОбласти

#Область СозданиеДереваСГруппировками

// Подготавливает данные группировки для создания дерева
// 
// Параметры:
// 	пПоля - Строка - Имена колонок-группировки
// 	пПропускатьПустые - Булево - Когда Истина, то если значение группировки пустое, то она создана не будет
// Возвращаемое значение:
// 	Структура - Описание:
// * Имя - Строка - Имя колонки-группировки
// * ПропускатьПустые - Булево - Когда Истина, то если значение группировки пустое, то она создана не будет
// * ОтносительноеЗаполнение - Структура - для каждого элемента будет выполнено строкаДерева[Элемент.Ключ] = строкаДерева[Элемент.Значение]
// * Заполнение - Структура - для каждого элемента будет выполнено строкаДерева[Элемент.Ключ] = Элемент.Значение
Функция ГруппировкаДерева(Знач пПоля, Знач пПропускатьПустые = Ложь) Экспорт
	
	структ = Новый Структура;
	структ.Вставить("Поля", пПоля);
	структ.Вставить("ПропускатьПустые", пПропускатьПустые);
	структ.Вставить("ОтносительноеЗаполнение", Новый Структура);
	структ.Вставить("Заполнение", Новый Структура);
	
	Возврат структ;
	
КонецФункции

// Находит и возвращает строку сгруппированного дерева или создает ветвь от корня к листу
// 
// Параметры:
// 	пДерево - ДеревоЗначений - Дерево
// 	пОтбор - Структура - значения отбора
// 	пГруппировки - массив из Структура см. к2Коллекции.ГруппировкаДерева - данные группировок
// 	пЗаполнениеДетЗаписи - Структура, Неопределено - значения, которыми нужно заполнить строку детальных записей
// Возвращаемое значение:
// 	СтрокаДереваЗначений - созданная или найденная строка дерева
Функция СоздатьПолучитьВеткуДерева(пДерево, Знач пОтбор, Знач пГруппировки, Знач пЗаполнениеДетЗаписи = Неопределено) Экспорт
	
	строкиДерева = пДерево.Строки.НайтиСтроки(пОтбор, Истина);
	
	Если строкиДерева.Количество() > 0 Тогда
		
		строкаДерева = строкиДерева[0];
		
	Иначе
		
		строкаДерева = СоздатьСтрокуПоСтруктуре(пДерево, пОтбор, пГруппировки, пЗаполнениеДетЗаписи);
		
	КонецЕсли;
	
	Возврат строкаДерева;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область РаботаСТаблицейФормы

Функция МассивУникальныхЗначений(пТаблица, пИмяКолонки) Экспорт
	
	Возврат ТаблицаОбхода(пТаблица, пИмяКолонки).ВыгрузитьКолонку(пИмяКолонки);
	
КонецФункции

Функция ТаблицаОбхода(пТаблица, пКолонки) Экспорт
	
	тз = ТаблицаСКолонками(пТаблица, пКолонки);
	
	тз.Свернуть(пКолонки);
	
	Возврат тз;
	
КонецФункции

Процедура СгруппироватьТаблицу(пТаблица, пКолонкиСуммирования) Экспорт
	
	колонкиГруппировок = КолонкиГруппировок(пТаблица, пКолонкиСуммирования);
	
	пТаблица.Свернуть(колонкиГруппировок, пКолонкиСуммирования);
	
КонецПроцедуры

Функция ПустаяТаблицаПоКолонкам(Колонки) Экспорт
	
	Таблица = Новый ТаблицаЗначений;
	Для Каждого Колонка Из Колонки Цикл
		Таблица.Колонки.Добавить(Колонка.Имя, Колонка.ТипЗначения, Колонка.Заголовок, Колонка.Ширина);
	КонецЦикла;
	
	Возврат Таблица;
	
КонецФункции

// Проверяет существование колонок в таблице. Если колонки не существует - создает
// 
// Параметры:
// 	Таблица - ТаблицаЗначений - Таблица, в которой нужно обеспечить колонки
// 	Колонки - Строка - список колонок через запятую
Процедура ОбеспечитьКолонки(Таблица, Знач Колонки) Экспорт
	
	Если Не ЗначениеЗаполнено(Колонки) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Для каждого цИмяКолонки Из стрРазбить(Колонки) Цикл
		
		имяКолонки = СокрЛП(цИмяКолонки);
		
		Если Таблица.Колонки.Найти(имяКолонки) = Неопределено Тогда
			
			Таблица.Колонки.Добавить(имяКолонки);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Функция СреднееТаблицыФормыПоОтбору(ТаблицаФормы, СтруктураОтбора, ИмяКолонки) Экспорт
	
	Сумма = 0;
	
	НайденныеСтроки = ТаблицаФормы.НайтиСтроки(СтруктураОтбора);
	
	Для Каждого Строка Из НайденныеСтроки Цикл 
		
		Сумма = Сумма + Строка[ИмяКолонки];
		
	КонецЦикла;
	
	КоличествоСтрок = НайденныеСтроки.Количество();
	
	Возврат к2КоллекцииКлиентСервер.Разделить(Сумма, КоличествоСтрок);
	
КонецФункции

Функция СуммаТаблицыФормыПоОтбору(ТаблицаФормы, СтруктураОтбора, ИмяКолонки) Экспорт
	
	Сумма = 0;
	
	НайденныеСтроки = ТаблицаФормы.НайтиСтроки(СтруктураОтбора);
	
	Для Каждого Строка Из НайденныеСтроки Цикл 
		
		Сумма = Сумма + Строка[ИмяКолонки];
		
	КонецЦикла;
	
	
	Возврат Сумма;
	
КонецФункции

// Определяет минимальное значение в колонке таблицы.
// 
// Параметры:
// 	Таблица - ТаблицаЗначений, ДанныеФормыКоллекция, ТабличнаяЧасть - Таблица, в колонке которой нужно определить минимальное значение 
// 	ИмяКолонки - Строка - Имя колонки
// Возвращаемое значение:
// 	Число, Дата, Булево, Строка - минимальное значение
Функция МинимальноеЗначениеКолонкиТаблицы(Знач Таблица, Знач ИмяКолонки) Экспорт

	тз = ТаблицаСКолонками(Таблица, ИмяКолонки);

	значениеПоУмолчанию = тз.Колонки[ИмяКолонки].ТипЗначения.ПривестиЗначение(Неопределено);

	Возврат к2КоллекцииКлиентСервер.Минимум(тз, ИмяКолонки, значениеПоУмолчанию);

КонецФункции

// Определяет максимальное значение в колонке таблицы.
// 
// Параметры:
// 	Таблица - ТаблицаЗначений, ДанныеФормыКоллекция, ТабличнаяЧасть - Таблица, в колонке которой нужно определить максимальное значение 
// 	ИмяКолонки - Строка - Имя колонки
// Возвращаемое значение:
// 	Число, Дата, Булево, Строка - максимальное значение
Функция МаксимальноеЗначениеКолонкиТаблицы(Знач Таблица, Знач ИмяКолонки) Экспорт

	тз = ТаблицаСКолонками(Таблица, ИмяКолонки);

	МаксимальноеЗначение = тз.Колонки[ИмяКолонки].ТипЗначения.ПривестиЗначение(Неопределено);

	Если тз.Количество() > 0 Тогда
		МаксимальноеЗначение = тз[0][ИмяКолонки];
	КонецЕсли;

	Для Каждого цСтрокаТаблицы Из тз Цикл
		МаксимальноеЗначение = Макс(МаксимальноеЗначение, цСтрокаТаблицы[ИмяКолонки]);
	КонецЦикла;

	Возврат МаксимальноеЗначение;

КонецФункции

// Возвращает свернутую таблицу
//
// Параметры:
//  пТаблица			 - ТаблицаЗначений, ТабличнаяЧасть, ДанныеФормыКоллекция - Таблица
//  пКолонкиГруппировок	 - Строка - Имена колонок, разделенные запятыми,
//									по которым необходимо группировать строки табличного поля.
//  пКолонкиСуммирования - Строка - Имена колонок, разделенные запятыми, 
//									по которым необходимо суммировать значения в строках табличного поля
// 
// Возвращаемое значение:
//  ТаблицаЗначений - свернутая таблица
//
Функция ПолучитьСвернутуюТаблицу(Знач пТаблица, Знач пКолонкиГруппировок = "", Знач пКолонкиСуммирования = "") Экспорт
	
	колонки = СоединитьИдентификаторы(пКолонкиГруппировок, пКолонкиСуммирования);
	
	тз = ТаблицаСКолонками(пТаблица, колонки);
	тз.Свернуть(пКолонкиГруппировок, пКолонкиСуммирования);
	
	Возврат тз;
	
КонецФункции

Функция ПолучитьСвернутуюТаблицуПоВсемКолонкам( Знач пТаблица, Знач пКолонкиСуммирования = "" ) Экспорт
	
	массивКолонокСуммирования = стрРазбить(пКолонкиСуммирования);
	
	массивКолонок = Новый Массив;
	
	Для каждого цКолонка Из пТаблица.Колонки Цикл
		
		Если массивКолонокСуммирования.Найти( цКолонка.Имя ) = Неопределено Тогда
			массивКолонок.Добавить( цКолонка.Имя );
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ПолучитьСвернутуюТаблицу( пТаблица, СтрСоединить( массивКолонок, "," ), пКолонкиСуммирования );
	
КонецФункции

// Создает новую таблицу, где колонки группировки и суммирования свернуты, а детальные записи добавлены в новую колонку "Таблица"
//
// Параметры:
//  пИсходнаяТаблица	 - ТаблицаЗначений, ДанныеФормыКоллекция, ТабличнаяЧасть - таблица для группировки
//  пКолонкиГруппировки	 - Строка - Имена колонок, разделенные запятыми,
//									по которым необходимо группировать строки таблицы
//  пКолонкиСуммирования - Строка - Имена колонок, разделенные запятыми,
//									по которым необходимо суммировать значения в строках таблицы
// 
// Возвращаемое значение:
//  ТаблицаЗначений - Свернутая таблица с детальными записями в отдельной подтаблице
//
Функция СгруппированнаяТаблица(Знач пИсходнаяТаблица, Знач пКолонкиГруппировки, Знач пКолонкиСуммирования = "") Экспорт
	
	Если ТипЗнч(пИсходнаяТаблица) = Тип("ТаблицаЗначений") Тогда
		
		исходнаяТаблица = пИсходнаяТаблица;
		
	Иначе
		
		исходнаяТаблица = пИсходнаяТаблица.Выгрузить();
		
	КонецЕсли;
	
	структОтбора = Новый Структура(пКолонкиГруппировки);
	исходнаяТаблица.Индексы.Добавить(пКолонкиГруппировки);
	
	тз = ПолучитьСвернутуюТаблицу(исходнаяТаблица, пКолонкиГруппировки, пКолонкиСуммирования);
	тз.Колонки.Добавить("Таблица");
	
	Для Каждого новСтрока Из тз Цикл
		
		ЗаполнитьЗначенияСвойств(структОтбора, новСтрока);
		
		новСтрока.Таблица = исходнаяТаблица.Скопировать(структОтбора);
		
	КонецЦикла;
	
	Возврат тз;
	
КонецФункции

Функция Последние(Знач пТаблица, Знач пКоличество) Экспорт

	всегоСтрок = пТаблица.Количество();

	Если пКоличество = 0 Или всегоСтрок <= пКоличество Тогда

		Возврат ТаблицаСКолонками(пТаблица);

	КонецЕсли;

	Если ТипЗнч( пТаблица) = Тип( "ТаблицаЗначений") Тогда

		тз = пТаблица.СкопироватьКолонки();

	Иначе

		тз = пТаблица.ВыгрузитьКолонки();

	КонецЕсли;

	Для ц = всегоСтрок - пКоличество По всегоСтрок - 1 Цикл

		ЗаполнитьЗначенияСвойств( тз.Добавить(), пТаблица[ц]);

	КонецЦикла;

	Возврат тз;

КонецФункции

Функция Первые(Знач пТаблица, Знач пКоличество ) Экспорт
	
	Возврат Срез( пТаблица,, пКоличество );
	
КонецФункции

Функция Срез( Знач пТаблица, Знач пОт = 0, Знач пДо = 0 ) Экспорт
	
	всегоСтрок = пТаблица.Количество();
	
	Если ТипЗнч( пТаблица ) = Тип( "ТаблицаЗначений" ) Тогда
		
		тз = пТаблица.СкопироватьКолонки();
		
	Иначе
		
		тз = пТаблица.ВыгрузитьКолонки();
		
	КонецЕсли;
	
	Если всегоСтрок < пОт Тогда
		
		Возврат тз;
		
	КонецЕсли;
	
	Для ц = пОт По Мин( всегоСтрок, пДо ) - 1 Цикл
		
		ЗаполнитьЗначенияСвойств( тз.Добавить(), пТаблица[ц] );
		
	КонецЦикла;
	
	Возврат тз;
	
КонецФункции

Функция ОбъединитьТаблицы( Знач пТаблица1, Знач пТаблица2 ) Экспорт
	
	тзРезультат = пТаблица2.Скопировать();
	
	Для ц = 1 По пТаблица1.Количество() Цикл
		
		тзРезультат.Вставить( 0 );
		
	КонецЦикла;
	
	Для ц = 0 По пТаблица1.Колонки.Количество() -1 Цикл
		
		тзРезультат.ЗагрузитьКолонку( пТаблица1.ВыгрузитьКолонку( ц ), ц );
		
	КонецЦикла;
	
	Возврат тзРезультат;
	
КонецФункции

Процедура ДобавитьКТаблице( Знач пТаблицаПриемник, Знач пТаблица ) Экспорт
	
	Для каждого цСтрока Из пТаблица Цикл
		
		ЗаполнитьЗначенияСвойств( пТаблицаПриемник.Добавить(), цСтрока );
		
	КонецЦикла;
	
КонецПроцедуры

// Устарела. Следует использовать к2КоллекцииКлиентСервер.НайтиПервое
// Находит первую строку в таблице, согласно отбору и возвращает значение.
// Если строки на найдены, то возвращается значение по умолчанию.
// 
// Параметры:
// 	пТаблица - ДанныеФормыКоллекция, ДанныеФормыДерево - таблица или дерево формы в которой нужно осуществлять поиск.
// 	пСтруктураОтбора - Структура - Задает условия поиска: ключ - имя колонки, по которой будет осуществляться поиск,
//										а значение структуры - искомое значение.
// 	пИмяПоля - Строка - имя поля из которого нужно получить значение
// 	пЗначениеПоУмолчанию - Произвольное - Значение, которое будет возвращено, если строки не найдены
// Возвращаемое значение:
// 	Произвольное - значение в найденной строке или значение по умолчанию
Функция НайтиПервое(Знач пТаблица, Знач пСтруктураОтбора, Знач пИмяПоля, Знач пЗначениеПоУмолчанию = Неопределено) Экспорт
	
	Возврат к2КоллекцииКлиентСервер.НайтиПервое(пТаблица, пСтруктураОтбора, пИмяПоля, пЗначениеПоУмолчанию);
	
КонецФункции

// Осуществляет свертку таблицы по указанным колонкам группировки. 
// Строки, у которых совпадают значения в колонках, указанных в первом параметре, сворачиваются в одну строку.
// У значений этих строк, хранящиеся в колонках, указанных во втором параметре, выбирается минимум.
//
// Параметры:
//  пТаблица	 - ТаблицаЗначений, ТаблицаФормы, ТабличнаяЧасть - таблица для свертки
//  пГруппировка - Строка - Имена колонок, разделенные запятыми, по которым необходимо группировать строки таблицы
//  пКолонки	 - Строка - Имена колонок, разделенные запятыми, по которым необходимо получить минимум по строкам таблицы
// 
// Возвращаемое значение:
//  ТаблицаЗначений, ТаблицаФормы, ТабличнаяЧасть - свернутая таблица
//
Функция СвернутьТаблицу_Мин(Знач пТаблица, Знач пГруппировка = "", Знач пКолонки = "") Экспорт
	
	колонки = СоединитьИдентификаторы(пГруппировка, пКолонки);
	
	свернутаяТаблица = ТаблицаСКолонками(пТаблица, колонки);
	
	таблицаОбхода = ТаблицаОбхода(пТаблица, колонки);
	
	свернутаяТаблица.ЗаполнитьЗначения(0, пКолонки);
	свернутаяТаблица.Свернуть(пГруппировка, пКолонки);
	
	отбор = Новый Структура(пГруппировка);
	
	Для каждого цСтрокаТаблицы Из свернутаяТаблица Цикл
		
		ЗаполнитьЗначенияСвойств(отбор, цСтрокаТаблицы);
		
		таблицаСОтбором = таблицаОбхода.Скопировать(отбор);
		
		Для каждого цИмяКолонки Из стрРазбить(пКолонки) Цикл
			
			цСтрокаТаблицы[цИмяКолонки] = МинимальноеЗначениеКолонкиТаблицы(таблицаСОтбором, цИмяКолонки);
			
		КонецЦикла;
		
	КонецЦикла;
	
	Возврат свернутаяТаблица;
	
КонецФункции

// Добавляет колонку в таблицу значений, если с таким именем ее еще не существует
//
// Параметры:
//  пТаблица	 - ТаблицаЗначений - Таблица
//  пИмяКолонки	 - Строка - Имя колонки
//  пТипКолонки	 - ОписаниеТипов - Объект, описывающий допустимые типы значений для колонки.
//
Процедура ОбеспечитьКолонку(Знач пТаблица, Знач пИмяКолонки, Знач пТипКолонки = Неопределено) Экспорт
	
	Если пТаблица.Колонки.Найти(пИмяКолонки) = Неопределено Тогда
		
		пТаблица.Колонки.Добавить(пИмяКолонки, пТипКолонки);
		
	КонецЕсли;
	
КонецПроцедуры

// Возвращает соответствие по строкам коллекции для ПолеКлюч и ПолеЗначение.
//
// Параметры:
//  Коллекция - ТаблицаЗначений, ТаблицаФормы, ТабличнаяЧасть, РезультатЗапроса, ВыборкаИзРезультатаЗапроса - коллекция
//                для получения соответствия.
//  ПолеКлюч - Строка - имя поля для получения ключа соответствия из строки коллекции
//  ПолеЗначение - Строка - имя поля для получения значения соответствия из строки коллекции
//
// Возвращаемое значение:
//  Соответствие - значения ПолеКлюч значению ПолеЗначение
//
Функция КоллекцияВСоответствие(Коллекция, ПолеКлюч = "Ключ", ПолеЗначение = "Значение") Экспорт
	
	Соответствие = Новый Соответствие;
	
	Если ТипЗнч(Коллекция) = Тип("РезультатЗапроса") Тогда 
		КоллекцияДляОбхода = Коллекция.Выбрать();
	Иначе
		КоллекцияДляОбхода = Коллекция;
	КонецЕсли;
	
	Если ТипЗнч(КоллекцияДляОбхода) = Тип("ВыборкаИзРезультатаЗапроса") Тогда 
		
		Пока КоллекцияДляОбхода.Следующий() Цикл 
			
			Соответствие[КоллекцияДляОбхода[ПолеКлюч]] = КоллекцияДляОбхода[ПолеЗначение];
			
		КонецЦикла;
		
	Иначе
		
		Для Каждого Строка Из КоллекцияДляОбхода Цикл 
			
			Соответствие[Строка[ПолеКлюч]] = Строка[ПолеЗначение];
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат Соответствие;
	
КонецФункции

#КонецОбласти

#Область РаботаСМассивами

Функция СложитьМассивы(Массив1, Массив2) Экспорт
	
	Массив = ОбщегоНазначения.СкопироватьРекурсивно(Массив1);
	
	Для Каждого Элемент Из ОбщегоНазначения.СкопироватьРекурсивно(Массив2) Цикл
		Массив.Добавить(Элемент);
	КонецЦикла;
	
	Возврат Массив;
	
КонецФункции

#КонецОбласти

#Область СохранениеВосстановлениеТекущейСтроки

Функция СохранитьДанныеОТекущейСтроке( Форма, Знач пИмяЭлемента, Знач пИмяТаблицы, Знач пПоляУникальности ) Экспорт
	
	текСтрока = Форма.Элементы[пИмяЭлемента].ТекущаяСтрока;
	
	Если текСтрока = Неопределено Тогда
		
		Возврат Неопределено;
		
	КонецЕсли;
	
	структ = Новый Структура;
	структ.Вставить( "ИмяЭлемента", пИмяЭлемента );
	структ.Вставить( "ИмяТаблицы", пИмяТаблицы );
	структ.Вставить( "ПоляУникальности", пПоляУникальности );
	
	текДанные = Форма[пИмяТаблицы].НайтиПоИдентификатору( текСтрока );
	
	Если текДанные = Неопределено Тогда
		
		Возврат Неопределено;
		
	КонецЕсли;
	
	структСтроки = Новый Структура(пПоляУникальности);
	ЗаполнитьЗначенияСвойств( структСтроки, текДанные );
	
	структ.Вставить( "ТекущаяСтрока", структСтроки );
	
	таблицаФормы = Форма[пИмяТаблицы];
	
	Если ТипЗнч( таблицаФормы ) = Тип("ДанныеФормыДерево") Тогда
		таблицаФормы = таблицаФормы.ПолучитьЭлементы();
	КонецЕсли;
	
	текИндекс = таблицаФормы.Индекс( текДанные );
	
	структСтроки = Новый Структура(пПоляУникальности);
	
	Если текИндекс < таблицаФормы.Количество() - 1 Тогда
		
		ЗаполнитьЗначенияСвойств( структСтроки, таблицаФормы[текИндекс + 1] );
		
	ИначеЕсли текИндекс > 0 Тогда
		
		ЗаполнитьЗначенияСвойств( структСтроки, таблицаФормы[текИндекс - 1] );
		
	Иначе
		
		структСтроки = Неопределено;
		
	КонецЕсли;
	
	структ.Вставить( "СоседняяСтрока", структСтроки );
	
	Возврат структ;
	
КонецФункции

Процедура ВосстановитьТекущуюСтроку( Форма, Знач пДанныеОТекСтроке ) Экспорт
	
	Если пДанныеОТекСтроке = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	таблица = Форма[пДанныеОТекСтроке.ИмяТаблицы];
	элемент = Форма.Элементы[пДанныеОТекСтроке.ИмяЭлемента];
	
	структПоиска = Новый Структура(пДанныеОТекСтроке.ПоляУникальности);
	
	ЗаполнитьЗначенияСвойств( структПоиска, пДанныеОТекСтроке.ТекущаяСтрока );
	
	найденныеСтроки = к2КоллекцииКлиентСервер.НайтиСтрокиТаблицыФормы( таблица, структПоиска, Истина );
	
	Если найденныеСтроки.Количество() > 0 Тогда
		
		элемент.ТекущаяСтрока = найденныеСтроки[0].ПолучитьИдентификатор();
		
	ИначеЕсли Не пДанныеОТекСтроке.СоседняяСтрока = Неопределено Тогда
		
		ЗаполнитьЗначенияСвойств( структПоиска, пДанныеОТекСтроке.СоседняяСтрока );
		
		найденныеСтроки = к2КоллекцииКлиентСервер.НайтиСтрокиТаблицыФормы( таблица, структПоиска, Истина );
		
		Если найденныеСтроки.Количество() > 0 Тогда
			
			элемент.ТекущаяСтрока = найденныеСтроки[0].ПолучитьИдентификатор();
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

// Создает таблицу со всеми возможными значениями переданных вариантов
//
// Параметры:
//  пИтераторы	 - Структура - Ключ - Имя колонки, Значение - массив возможных вариантов.
// 
// Возвращаемое значение:
//  ТаблицаЗначений - 
//
Функция СоставнойИтератор( Знач пИтераторы ) Экспорт
	
	пул = Новый Массив;
	
	Для каждого цИтератор Из пИтераторы Цикл
		
		новыйПул = Новый Массив;
		
		Если пул.Количество() = 0 Тогда
			
			Для каждого цЗначенияИтератора Из цИтератор.Значение Цикл
				
				цЭлемент = Новый Структура();
				цЭлемент.Вставить( цИтератор.Ключ, цЗначенияИтератора );
				пул.Добавить( цЭлемент );
				
			КонецЦикла;
			
		Иначе
			
			Для каждого цЗначенияИтератора Из цИтератор.Значение Цикл
				
				Для каждого цЭлемент Из пул Цикл
					
					новЭлемент = Новый Структура;
					
					Для Каждого КлючИЗначение Из цЭлемент Цикл
						
						новЭлемент.Вставить( КлючИЗначение.Ключ, КлючИЗначение.Значение );
						
					КонецЦикла;
					
					новЭлемент.Вставить( цИтератор.Ключ, цЗначенияИтератора );
					новыйПул.Добавить( новЭлемент );
					
				КонецЦикла;
				
			КонецЦикла;
			
			пул = НовыйПул;
			
		КонецЕсли;
		
	КонецЦикла;
	
	тз = Новый ТаблицаЗначений;
	
	Для каждого цЭлемент Из пИтераторы Цикл
		
		тз.Колонки.Добавить( цЭлемент.Ключ );
		
	КонецЦикла;
	
	Для каждого цЭлемент Из пул Цикл
		
		ЗаполнитьЗначенияСвойств( тз.Добавить(), цЭлемент );
		
	КонецЦикла;
	
	Возврат тз;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ТаблицаСКолонками(Знач пТаблица, Знач пКолонки = Неопределено)

	Если ТипЗнч(пТаблица) = Тип("ТаблицаЗначений") Тогда
		Возврат пТаблица.Скопировать( , пКолонки);
	Иначе
		Возврат пТаблица.Выгрузить( , пКолонки);
	КонецЕсли;

КонецФункции

Функция КолонкиГруппировок(пТаблица, пКолонкиСуммирования)
	
	массивИмен = Новый Массив;
	
	массивИменИсключения = стрРазбить(пКолонкиСуммирования);
	
	Если ТипЗнч(пТаблица) = Тип("ТаблицаЗначений") Тогда 
		НаборКолонок = пТаблица.Колонки;
	Иначе 
		НаборКолонок = пТаблица.ВыгрузитьКолонки().Колонки;
		массивИменИсключения.Добавить("НомерСтроки");
	КонецЕсли;
	
	Для Каждого цКолонка Из НаборКолонок Цикл
		
		Если массивИменИсключения.Найти(цКолонка.Имя) = Неопределено Тогда
			массивИмен.Добавить(цКолонка.Имя);
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат СтрСоединить(массивИмен, ",");
	
КонецФункции

Процедура ЗаполнитьКолонкуУровень(пСтроки, Знач пУровень = 1)
	
	Для Каждого цСтрока Из пСтроки Цикл
		
		цСтрока.Уровень = пУровень;
		
		ЗаполнитьКолонкуУровень(цСтрока.Строки, пУровень + 1);
		
	КонецЦикла;
	
КонецПроцедуры

Функция СоединитьИдентификаторы(Знач пСтрокаИдентификаторов1, Знач пСтрокаИдентификаторов2)
	
	массивСтрок = Новый Массив;
	массивСтрок.Добавить(пСтрокаИдентификаторов1);
	массивСтрок.Добавить(пСтрокаИдентификаторов2);
	
	Возврат СтрСоединить(массивСтрок, ",");

КонецФункции

#Область СозданиеДереваЗначенийСГруппировками

// Создает ветку дерева по данным группировок
// 
// Параметры:
// 	Дерево - ДеревоЗначений - Дерево
// 	пОтбор - Структура - Значения группировок
// 	пГруппировки - массив из Структура см. к2Коллекции.ГруппировкаДерева - данные группировок
// 	пЗаполнениеДетЗаписи - Структура, Неопределено - значения, которыми нужно заполнить строку детальных записей
// Возвращаемое значение:
// 	СтрокаДереваЗначений - созданная детальная строка дерева
Функция СоздатьСтрокуПоСтруктуре(Дерево, Знач пОтбор, Знач пГруппировки, Знач пЗаполнениеДетЗаписи = Неопределено)

	строкаДерева = Дерево;
	
	Для Каждого цГруппировка Из пГруппировки Цикл

		новОтбор = Новый Структура(цГруппировка.Поля);
		ЗаполнитьЗначенияСвойств(новОтбор, пОтбор);
	
		поляПустые = Истина;
		
		Для каждого цЭлементОтбора Из новОтбор Цикл
			
			Если ЗначениеЗаполнено(цЭлементОтбора.Значение) Тогда
				
				поляПустые = Ложь;
				
			КонецЕсли;
			
		КонецЦикла;
	
		Если Не (цГруппировка.ПропускатьПустые И поляПустые) Тогда

			строкаДерева = ОбеспечитьСтрокуДерева(строкаДерева, новОтбор, цГруппировка);

		КонецЕсли;

	КонецЦикла;

	Если Не пЗаполнениеДетЗаписи = Неопределено Тогда

		ЗаполнитьЗначенияСвойств(строкаДерева, пЗаполнениеДетЗаписи);

	КонецЕсли;

	Возврат строкаДерева;

КонецФункции

// Находит или создает строку дерева по данным группировки
// 
// Параметры:
// 	пСтрокаРодитель - ДеревоЗначений, СтрокаДереваЗначений - Родитель создаваемой строки
// 	пОтбор - Структура - отбор для поиска строк или для заполнения новой строки, если не найдено
// 	пГруппировка - Структура из см. к2Коллекции.ГруппировкаДерева - Параметры группировки
// Возвращаемое значение:
// 	СтрокаДереваЗначений - созданная детальная строка дерева
Функция ОбеспечитьСтрокуДерева(Знач пСтрокаРодитель, Знач пОтбор, Знач пГруппировка)
	
	найденныеСтроки = пСтрокаРодитель.Строки.НайтиСтроки(пОтбор, Ложь);
	
	Если найденныеСтроки.Количество() > 0 Тогда
		
		строкаДерева = найденныеСтроки[0];
		
	Иначе
		
		строкаДерева = пСтрокаРодитель.Строки.Добавить();
		
		Если ТипЗнч(пСтрокаРодитель) = Тип("СтрокаДереваЗначений") Тогда
			
			ЗаполнитьЗначенияСвойств(строкаДерева, пСтрокаРодитель);
			
		КонецЕсли;
		
		ЗаполнитьЗначенияСвойств(строкаДерева, пОтбор);
		
		Для каждого цЭлемент Из пГруппировка.ОтносительноеЗаполнение Цикл
			
			строкаДерева[цЭлемент.Ключ] = строкаДерева[цЭлемент.Значение];
			
		КонецЦикла;
		
		Для каждого цЭлемент Из пГруппировка.Заполнение Цикл
			
			строкаДерева[цЭлемент.Ключ] = цЭлемент.Значение;
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат строкаДерева;
	
КонецФункции

#КонецОбласти

#Область ЗаполнениеПолейВДереве

Процедура ЗаполнитьПоляПодчиненнымСтрокамДереваВверх(пСтрокаРодитель, Знач пПоля, Знач пАгрегатнаяФункция)
	
	Для каждого цСтрокаДерева Из пСтрокаРодитель.Строки Цикл
		
		ЗаполнитьПоляПодчиненнымСтрокамДереваВверх(цСтрокаДерева, пПоля, пАгрегатнаяФункция);
		
	КонецЦикла;
	
	Для Каждого цПоле Из пПоля Цикл

		Если НРег(пАгрегатнаяФункция) = "мин" Тогда
			
			значениеПодчиненныхСтрок = МинимумПодчиненныхСтрокДЗ(пСтрокаРодитель, цПоле);
			
		ИначеЕсли НРег(пАгрегатнаяФункция) = "макс" Тогда
			
			значениеПодчиненныхСтрок = МаксимумПодчиненныхСтрокДЗ(пСтрокаРодитель, цПоле);
			
		ИначеЕсли НРег(пАгрегатнаяФункция) = "среднее" Тогда
			
			значениеПодчиненныхСтрок = СреднееПодчиненныхСтрокДЗ(пСтрокаРодитель, цПоле);
			
		Иначе
			
			значениеПодчиненныхСтрок = СуммаПодчиненныхСтрокДЗ(пСтрокаРодитель, цПоле);
			
		КонецЕсли;

		пСтрокаРодитель[цПоле] = значениеПодчиненныхСтрок;

	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
