
////////////////////////////////////////////////////////////////////////////////
// ПОЛУЧЕНИЕ РЕКВИЗИТА ЗАПРОСОМ

Функция ЗначениеРеквизита(пСсылкаИЛИОбъект, пИменаРеквизита) Экспорт
	
	Возврат глРеквизит(пСсылкаИЛИОбъект, пИменаРеквизита);
	
КонецФункции

Функция ЗначениеРеквизитов(пСсылкаИЛИОбъект, пИменаРеквизита) Экспорт
	
	Возврат глРеквизиты(пСсылкаИЛИОбъект, пИменаРеквизита);
	
КонецФункции


//////////////////////////////////////////////////////////////////////////////////////
//БИЗНЕС-ПРОЦЕССЫ

Функция ОпубликованнаяВерсия( БП ) Экспорт
	
	Возврат Справочники.упБизнесПроцессы.ОпубликованнаяВерсия( БП );
	
КонецФункции


Функция МассивЗадачПоСессии( пСессия ) Экспорт
	
	Возврат Справочники.упСессии.ПолучитьМассивЗадачСессии( пСессия );
	
КонецФункции


Функция ПолучитьЗначениеРеквизита_Кэш( пСсылка , пИмяРеквизита ) Экспорт
	
	Возврат глРеквизит( пСсылка , пИмяРеквизита );
	
КонецФункции

Функция ПолучитьЗначениеРеквизитов_Кэш( пСсылка , пИмяРеквизита ) Экспорт
	
	Возврат глСтруктураРеквизитов( пСсылка , пИмяРеквизита );
	
КонецФункции


//{РЕКВИЗИТЫ БП

Функция ПолучитьТаблицуРеквизитовЗадачи( пЗадача ) Экспорт
	
	Возврат РегистрыСведений.упЗначенияРеквизитовСессий.ПолучитьРеквизитыЗадачи( пЗадача );
	
КонецФункции

Функция ПолучитьТабличнуюЧастьЗадачи( пЗадача , пРеквизит ) Экспорт
	
	Возврат РегистрыСведений.упЗначенияРеквизитовСессий.ПолучитьТабличнуюЧастьЗадачи( пЗадача , пРеквизит );
	
КонецФункции


Функция ПолучитьТаблицуКолонокРеквизита( пРеквизит ) Экспорт
	
	Возврат Справочники.упРеквизиты.ПолучитьТаблицуКолонокРеквизита( пРеквизит );
	
КонецФункции	//ПолучитьТаблицуКолонокРеквизита



Функция ДанныеОРеквизитах() Экспорт
	
	Возврат Справочники.упРеквизиты.ПустаяТаблицаПараметровРеквизитов();
	
КонецФункции


//}РЕКВИЗИТЫ БП

Функция ЗначениеАвтоПереадресации( пВерсия, пСтарАдресация, пДата ) Экспорт
	
	Возврат РегистрыСведений.упПереадресацияЗадач.НоваяАдресация( упЯдроПроцессаКлиентСервер.ПолучитьБизнесПроцесс( пВерсия ) , пСтарАдресация, пДата );
	
КонецФункции


Функция МассивСвязанныхСУсловиемРеквизитов( пУсловие ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упУсловияСвязанныеРеквизиты.РеквизитБП
	|ИЗ
	|	Справочник.упУсловия.СвязанныеРеквизиты КАК упУсловияСвязанныеРеквизиты
	|ГДЕ
	|	упУсловияСвязанныеРеквизиты.Ссылка = &Условие";
	
	Запрос.УстановитьПараметр( "Условие" , пУсловие );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции


Функция ПолучитьМассивИсходящихПереходов( пСостояние ) Экспорт
	
	Возврат Справочники.упПереходы.ПолучитьМассивИсходящихПереходов( пСостояние );
	
КонецФункции

Функция ПолучитьМассивВходящихПереходов( пСостояние ) Экспорт
	
	Возврат Справочники.упПереходы.ПолучитьМассивВходящихПереходов( пСостояние );
	
КонецФункции



Функция массивСозданныхОбъектовВЭтомВызове() Экспорт
	
	Возврат Новый Массив;
	
КонецФункции	//массивСозданныхОбъектовВЭтомВызове

Функция таблицаДанных( пИмяТаблицы ) Экспорт
	
	тз = Новый ТаблицаЗначений;
	
	тз.Колонки.Добавить( "Параметр" );
	тз.Колонки.Добавить( "ПредставлениеПараметра" );
	тз.Колонки.Добавить( "ПутьКДанным" );
	тз.Колонки.Добавить( "Частота" );
	
	Возврат тз;
	
КонецФункции	//массивСозданныхОбъектовВЭтомВызове




Функция СоздатьСтруктуруРеквизитовВерсии( пВерсия , пДоступенКонтекстЗадачи = Истина , пМеняетДанныеЗадачи = Ложь ) Экспорт
	
	Возврат упСтруктурыПроцессов.СоздатьСтруктуруРеквизитовВерсии( пВерсия , пДоступенКонтекстЗадачи , пМеняетДанныеЗадачи );
	
КонецФункции



Функция ОшибкиТекущегоВызова() Экспорт
	
	Возврат Новый Массив;
	
КонецФункции

Функция ОшибкиОтправкиПочты() Экспорт
	
	Возврат Новый Структура( "БылиОшибки", Ложь );
	
КонецФункции


// {{ПОДЧИНЕНИЕ

Функция ПолучитьМассивПользователейГруппы( пГруппыПользователей, пПолучитьПользователейПодчиненныхГрупп = Ложь ) Экспорт
	
	Возврат упПереопределяемый.ПолучитьМассивПользователей( пГруппыПользователей, пПолучитьПользователейПодчиненныхГрупп );
	
КонецФункции

Функция ПолучитьМассивГруппПользователя( пПользователь, пПолучатьРодителейГрупп = Истина ) Экспорт
	
	Возврат упПользователи.ПолучитьМассивГруппПользователя( пПользователь, пПолучатьРодителейГрупп );
	
КонецФункции

Функция ПолучитьТаблицуПодчиненных( пПользователь, ПоказыватьГруппы, пВидыПодчиненности ) Экспорт
	
	тз = РегистрыСведений.упПодчинение.Таблица( пПользователь , , ПоказыватьГруппы, стрРазбить( пВидыПодчиненности ) );
	
	тз.Колонки.Добавить( "ИсточникПодчинения" );
	тз.ЗаполнитьЗначения( упИК.ИсточникПодчинения_РегистрПодчинения() , "ИсточникПодчинения" );
	
	Если Не ПоказыватьГруппы Тогда
		
		Возврат тз;
		
	КонецЕсли;
	
	ДобавитьПользователейГруппы( пПользователь, тз, "ПодчиненныйПользователь");
	
	УдалитьПользователейИспользуемыхВГруппах( тз, "ПодчиненныйПользователь" );
	
	Возврат тз;
	
КонецФункции

Функция ПолучитьТаблицуРуководителей( пПользователь, ПоказыватьГруппы, пВидыПодчиненности ) Экспорт
	
	тз = РегистрыСведений.упПодчинение.Таблица( , пПользователь , ПоказыватьГруппы, стрРазбить( пВидыПодчиненности ) );
	
	тз.Колонки.Добавить( "ИсточникПодчинения" );
	тз.ЗаполнитьЗначения( упИК.ИсточникПодчинения_РегистрПодчинения() , "ИсточникПодчинения" );
	
	Если Не ПоказыватьГруппы Тогда
		
		Возврат тз;
		
	КонецЕсли;
	
	ДобавитьПользователейГруппы( пПользователь, тз, "Пользователь");
	
	УдалитьПользователейИспользуемыхВГруппах( тз, "Пользователь" );
	
	Возврат тз;
	
КонецФункции

Процедура ДобавитьПользователейГруппы( пТекОбъект, пТЗ, пИмяКолонки )
	
	Если ТипЗнч( пТекОбъект ) = Тип( упПереопределяемыйКлиентСервер.ПолучитьСтрТип_ГруппаПользователей() ) Тогда
		
		// добавляем пользователей группы
		Для Каждого цПользователь Из ПолучитьМассивПользователейГруппы( пТекОбъект, Ложь ) Цикл
			
			Если Не пТЗ.Найти( цПользователь , пИмяКолонки ) = Неопределено Тогда
				// Этот пользователь уже есть в таблице, НЕ дублируем с другим источником подчинения.
				Продолжить;
			КонецЕсли;
			
			новСтрока = пТЗ.Добавить();
			
			новСтрока.Пользователь            = пТекОбъект;
			новСтрока.ПодчиненныйПользователь = пТекОбъект;
			новСтрока.ГруппаРуководитель      = пТекОбъект;
			новСтрока.ГруппаПодчиненный       = пТекОбъект;
			новСтрока.ИсточникПодчинения      = упИК.ИсточникПодчинения_ГруппаПользователей();
			
			новСтрока[пИмяКолонки]            = цПользователь;
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры


Процедура УдалитьПользователейИспользуемыхВГруппах( пТЗ, пИмяКолонки )
	
	// нужно удалить тех пользователей, которые есть в группах
	
	// получить массив групп
	
	пользователиНаУдаление = Новый Соответствие;
	
	Для каждого цСтрока Из пТЗ Цикл
		
		Если Не ТипЗнч( цСтрока[пИмяКолонки] ) = Тип( упПереопределяемыйКлиентСервер.ПолучитьСтрТип_ГруппаПользователей() ) Тогда
			Продолжить;
		КонецЕсли;
		
		Для Каждого цПользователь Из ПолучитьМассивПользователейГруппы( цСтрока[пИмяКолонки], Истина ) Цикл
			
			пользователиНаУдаление.Вставить( цПользователь );
			
		КонецЦикла;
		
	КонецЦикла;
	
	массивСтрокНаУдаление = Новый Массив;
	
	Для Каждого цЭлемент Из пользователиНаУдаление Цикл
		
		Для каждого цСтрокаНаУдаление Из пТЗ.НайтиСтроки( Новый Структура( пИмяКолонки , цЭлемент.Ключ ) ) Цикл
			массивСтрокНаУдаление.Добавить( цСтрокаНаУдаление );
		КонецЦикла;
		
	КонецЦикла;
	
	// удалить строки таблицы по пользователям
	
	Для каждого цСтрока Из массивСтрокНаУдаление Цикл
		
		пТЗ.Удалить( цСтрока );
		
	КонецЦикла;
	
КонецПроцедуры


Функция ПолучитьДоступныеВидыПодчиненности() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	упПодчинение.ВидПодчиненности
	|ИЗ
	|	РегистрСведений.упПодчинение КАК упПодчинение";
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку( 0 );
	
КонецФункции

// }}ПОДЧИНЕНИЕ


Функция ПолучитьЗначениеИКИзАлгоритма( пАлгоритм, пПутьКИК ) Экспорт
	
	Если Не ЗначениеЗаполнено( пАлгоритм ) Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ЛокальныеИменованныеКонстанты.Значение КАК Значение
	               |ИЗ
	               |	Справочник.упАлгоритмы.ЛокальныеИменованныеКонстанты КАК ЛокальныеИменованныеКонстанты
	               |ГДЕ
	               |	ЛокальныеИменованныеКонстанты.Ссылка = &Алгоритм
	               |	И ЛокальныеИменованныеКонстанты.Имя = &ПутьКИК";
	
	Запрос.УстановитьПараметр("Алгоритм", пАлгоритм);
	Запрос.УстановитьПараметр("ПутьКИК", пПутьКИК);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Возврат Выборка.Значение;
		
	Иначе
		
		Возврат Неопределено;
		
	КонецЕсли;
	
КонецФункции
