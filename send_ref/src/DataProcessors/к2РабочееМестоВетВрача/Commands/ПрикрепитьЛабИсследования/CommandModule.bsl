
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	СтруктураКВнесениюЛабИсследований = СоздатьЛабораторныеИсследования(ПараметрКоманды);

	Если Не СтруктураКВнесениюЛабИсследований.МассивДокументов.Количество() = 0 Тогда

		ОткрытьФорму("Обработка.к2РабочееМестоВетВрача.Форма.ФормаГрупповойСинхронизации",
					 СтруктураКВнесениюЛабИсследований,
					 ПараметрыВыполненияКоманды.Источник,
					 ПараметрыВыполненияКоманды.Уникальность,
					 ,
					 ,
					 ,
					 РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);

	КонецЕсли;

КонецПроцедуры
	
#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция СоздатьЛабораторныеИсследования(Знач МассивДокументов)
	
	Возврат Документы.к2ВнесениеСведенийОВетеринарныхМероприятиях_Меркурий.СоздатьПоШаблонам(МассивДокументов);
	
КонецФункции

#КонецОбласти