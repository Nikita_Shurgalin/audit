#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Возвращает дату начала и дату окончания смены
//
// Параметры:
//  ДатаСмены  - Дата - Дата смены в формате дата без времени
//  Смена  - СправочникСсылка.к2Смены - Ссылка на элемент справочника к2Смены
// 
// Возвращаемое значение:
//   Структура  - Структура с полями:
//      *ДатаНачалаСмены - Дата - Дата начала смены в формате дата и время
//      *ДатаОкончанияСмены - Дата - Дата окончания смены в формате дата и время
//
Функция СтруктураПериодаСмены(ДатаСмены, Смена) Экспорт
	
	Структура = Новый Структура("ДатаНачалаСмены, ДатаОкончанияСмены", Дата("00010101"), Дата("00010101"));
	
	Если Не (ЗначениеЗаполнено(ДатаСмены) И ЗначениеЗаполнено(Смена)) Тогда
		Возврат Структура;
	КонецЕсли;
	
	СтруктураРеквизитов = глРеквизиты(Смена, "ВремяРаботы, ВремяНачала, ВремяОкончания");
	
	ВремяНачалаМС = СтруктураРеквизитов.ВремяНачала - Дата("00010101");
	РазницаВремениМС = СтруктураРеквизитов.ВремяОкончания - СтруктураРеквизитов.ВремяНачала;
	
	Если РазницаВремениМС <= 0 Тогда
		
		ДатаНачалаСмены = ДатаСмены + ВремяНачалаМС;
		ДатаОкончанияСмены = ДатаНачалаСмены + к2ИК.СекундВДне() + РазницаВремениМС;
		
	Иначе
		
		ДатаНачалаСмены = ДатаСмены + ВремяНачалаМС;
		ДатаОкончанияСмены = ДатаНачалаСмены + РазницаВремениМС;
		
	КонецЕсли;
	
	Если Формат(ДатаОкончанияСмены, "ДФ=ss") = "00" Тогда
		ДатаОкончанияСмены = ДатаОкончанияСмены - 1;
	КонецЕсли;
	
	Структура.ДатаНачалаСмены = ДатаНачалаСмены;
	Структура.ДатаОкончанияСмены = ДатаОкончанияСмены;
	
	Возврат Структура;
	
КонецФункции

// Проверяет является ли смена текущей или предыдущей по отношению к переданной дате и времени
//
// Параметры:
//  ДатаВремя  - Дата - проверяемая дата и время смены
//  Смена  - СправочникСсылка.к2Смены - проверяемая смена
// 
// Возвращаемое значение:
//   Булево  - признак предыдущей смены
//
Функция ЭтоПредыдущаяСмена(ДатаВремя, Смена) Экспорт
	
	ПроверяемоеВремя = ДатаВремя - НачалоДня(ДатаВремя);
	
	ДатаНачалаСмены = к2ОбщегоНазначенияКлиентСервер.ЗначениеРеквизита(Смена, "ВремяНачала");
	
	ВремяНачалаСмены = ДатаНачалаСмены - НачалоДня(ДатаНачалаСмены);
	
	Возврат ПроверяемоеВремя < ВремяНачалаСмены;
	
КонецФункции

// Возвращает дату время работы в период смены
//
// Параметры:
//  ВремяРаботы  - Дата - Дата в формате дата время
//  ДатаСмены  - Дата - Дата смены в формате дата без времени
//  Смена  - СправочникСсылка.к2Смены - Ссылка на элемент справочника к2Смены
// 
// Возвращаемое значение:
//      Дата - Дата - Дата со временем в рамках выбранной смены
Функция ДатаРаботыПоСмене(ВремяРаботы, ДатаСмены, Смена) Экспорт
	
	ДатаПоВремениСмены = Дата(1, 1, 1, 0, 0, 0);
	
	Если ЗначениеЗаполнено(Смена)
		И ЗначениеЗаполнено(ДатаСмены) Тогда
		
		СтруктураПериода = Справочники.к2Смены.СтруктураПериодаСмены(ДатаСмены, Смена);
		ДатаПоВремениСменыНачало = НачалоДня(СтруктураПериода.ДатаНачалаСмены) 
			+ Час(ВремяРаботы) * к2ИК.СекундВЧасе() 
			+ Минута(ВремяРаботы) * к2ИК.СекундВМинуте() 
			+ Секунда(ВремяРаботы);
		ДатаПоВремениСменыКонец = НачалоДня(СтруктураПериода.ДатаОкончанияСмены) 
			+ Час(ВремяРаботы) * к2ИК.СекундВЧасе() 
			+ Минута(ВремяРаботы) * к2ИК.СекундВМинуте() 
			+ Секунда(ВремяРаботы);
		
		Если ДатаПоВремениСменыКонец >= СтруктураПериода.ДатаНачалаСмены 
			И ДатаПоВремениСменыКонец <= СтруктураПериода.ДатаОкончанияСмены Тогда
			
			ДатаПоВремениСмены = ДатаПоВремениСменыКонец;
			
		ИначеЕсли ДатаПоВремениСменыНачало >= СтруктураПериода.ДатаНачалаСмены 
			И ДатаПоВремениСменыНачало <= СтруктураПериода.ДатаОкончанияСмены Тогда
			
			ДатаПоВремениСмены = ДатаПоВремениСменыНачало;
			
		Иначе
			
			ВремяРаботыСтрокой = Формат(ВремяРаботы, "ДЛФ=T");
			
			ШаблонСообщения = НСтр("ru='Время %1 не входит в границы смены %2 - %3.'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения
				, ВремяРаботыСтрокой
				, СтруктураПериода.ДатаНачалаСмены
				, СтруктураПериода.ДатаОкончанияСмены);
			
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения);
			
		КонецЕсли;
	Иначе
		
		Если Не ЗначениеЗаполнено(ДатаСмены) Тогда
			ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнена дата смены.'"));
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Смена) Тогда
			ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнена смена.'"));
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ДатаПоВремениСмены;
	
КонецФункции

// Возвращает смену по умолчанию
//
// Возвращаемое значение:
//  СправочникСсылка.к2Смены - смена по умолчанию
//
Функция СменаПоУмолчанию() Экспорт
	
	Если ПолучитьФункциональнуюОпцию(Метаданные.ФункциональныеОпции.к2ВестиУчетПоСменам.Имя) Тогда
		Смена = Справочники.к2Смены.ПустаяСсылка();
	Иначе
		Смена = Справочники.к2Смены.Смена24;
	КонецЕсли;
	
	Возврат Смена;
	
КонецФункции

#КонецОбласти

#КонецЕсли