
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем

// Заполняет список команд создания на основании.
// 
// Параметры:
//   КомандыСоздатьНаОсновании - ТаблицаЗначений - состав полей см. в функции ВводНаОсновании.СоздатьКоллекциюКомандСоздатьНаОсновании.
//	 Параметры - Структура - Дополнительные параметры.
//
Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСоздатьНаОсновании, Параметры) Экспорт

	Документы.ПоступлениеТоваровУслуг.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	Документы.к2ПоступлениеДополнительныхЗатрат.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);

КонецПроцедуры

// Определяет список команд создания на основании.
//
// Параметры:
//   КомандыСоздатьНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры к2СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
// Возвращаемое значение:
//  ТаблицаЗначений - Команды создания на основании.
//
Функция ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании) Экспорт
	
	Если ПравоДоступа("Добавление", Метаданные.Документы.ЗаказПоставщику) Тогда
		
		КомандаСоздатьНаОсновании = КомандыСоздатьНаОсновании.Добавить();
		КомандаСоздатьНаОсновании.Менеджер = Метаданные.Документы.ЗаказПоставщику.ПолноеИмя();
		КомандаСоздатьНаОсновании.Представление = ОбщегоНазначения.ПредставлениеОбъекта(Метаданные.Документы.ЗаказПоставщику);
		КомандаСоздатьНаОсновании.РежимЗаписи = "Проводить";
		
		Возврат КомандаСоздатьНаОсновании;
		
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

	Возврат;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти

#КонецОбласти

#КонецЕсли

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)
	
	к2ДокументыКлиентСервер.ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка);
	
КонецПроцедуры

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	
	к2ДокументыКлиентСервер.ОбработкаПолученияПредставления(
		НСтр("ru='Заказ поставщику'"),
		Данные,
		Представление,
		СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Функция Этапы_Параметры() Экспорт
	
	структ = Новый Структура;
	
	структ.Вставить("ИмяТЧ", "Товары");
	структ.Вставить("ИмяКолонкиКоличествоУпаковок", "КоличествоУпаковок");
	структ.Вставить("ИмяКолонкиКоличество", "Количество");
	структ.Вставить("ИмяКолонкиУпаковка", "Упаковка");
	структ.Вставить("ИмяКлючаСвязи", "КлючСвязи");
	структ.Вставить("Документ", Перечисления.к2ДокументСЭтапом.ЗаказПоставщику);
	
	Возврат структ;
	
КонецФункции

#КонецОбласти
	
#Область СлужебныеПроцедурыИФункции

#Область Проведение

Функция ДополнительныеИсточникиДанныхДляДвижений(ИмяРегистра) Экспорт

	ИсточникиДанных = Новый Соответствие;

	Возврат ИсточникиДанных;

КонецФункции

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	// Создание запроса инициализации движений и заполнение его параметров.
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблица_к2ЗаказыПоставщикам(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2КоличестваДокументаПоЭтапам(Запрос, ТекстыЗапроса, Регистры);
	
	// Исполнение запроса и выгрузка полученных таблиц для движений.
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	ИменаРеквизитов = "Ссылка,Дата,Поставщик,Организация,Подразделение,Склад,ДатаДоставки,ТочкаДоставки,Этап";
	
	РеквизитыДокумента = глРеквизиты(ДокументСсылка, ИменаРеквизитов);
	
	Для Каждого Реквизит Из РеквизитыДокумента Цикл
		
		Запрос.УстановитьПараметр(Реквизит.Ключ, Реквизит.Значение);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ЗаказыПоставщикам(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ЗаказыПоставщикам";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&ДатаДоставки КАК Период,
	|	&Ссылка КАК Заказ,
	|	&Поставщик КАК Поставщик,
	|	&Подразделение КАК Подразделение,
	|	&ТочкаДоставки КАК ТочкаДоставки,
	|	ЗаказПоставщикуТовары.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ЗаказПоставщикуТовары.Упаковка КАК Упаковка,
	|	ЗаказПоставщикуТовары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	ЗаказПоставщикуТовары.Количество КАК Количество,
	|	&Организация КАК Организация,
	|	&Склад КАК Склад,
	|	&Ссылка КАК Регистратор,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения
	|ИЗ
	|	Документ.ЗаказПоставщику.Товары КАК ЗаказПоставщикуТовары
	|ГДЕ
	|	ЗаказПоставщикуТовары.Ссылка = &Ссылка
	|	И ЗаказПоставщикуТовары.Количество <> 0";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2КоличестваДокументаПоЭтапам(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2КоличестваДокументаПоЭтапам";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	ТекстЗапроса = "ВЫБРАТЬ
	               |	&Дата КАК Период,
	               |	&Ссылка КАК Документ,
	               |	ЗаказПоставщикуТовары.Номенклатура КАК Номенклатура,
	               |	ЗаказПоставщикуТовары.Характеристика КАК Характеристика,
	               |	СУММА(ЗаказПоставщикуКоличествоПоЭтапам.Количество) КАК Количество,
	               |	СУММА(ЗаказПоставщикуКоличествоПоЭтапам.КоличествоУпаковок) КАК КоличествоУпаковок,
	               |	ЗаказПоставщикуКоличествоПоЭтапам.Упаковка КАК Упаковка,
	               |	ЗаказПоставщикуКоличествоПоЭтапам.Этап КАК Этап,
	               |	ЗаказПоставщикуТовары.КодСтроки КАК КодСтроки
	               |ИЗ
	               |	Документ.ЗаказПоставщику.Товары КАК ЗаказПоставщикуТовары
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ЗаказПоставщику.КоличествоПоЭтапам КАК ЗаказПоставщикуКоличествоПоЭтапам
	               |		ПО ЗаказПоставщикуТовары.Ссылка = ЗаказПоставщикуКоличествоПоЭтапам.Ссылка
	               |			И ЗаказПоставщикуТовары.КлючСвязи = ЗаказПоставщикуКоличествоПоЭтапам.КлючСвязиСТаблицей
	               |ГДЕ
	               |	ЗаказПоставщикуТовары.Ссылка = &Ссылка
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	ЗаказПоставщикуТовары.Номенклатура,
	               |	ЗаказПоставщикуТовары.Характеристика,
	               |	ЗаказПоставщикуКоличествоПоЭтапам.Упаковка,
	               |	ЗаказПоставщикуКоличествоПоЭтапам.Этап,
	               |	ЗаказПоставщикуТовары.КодСтроки";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
