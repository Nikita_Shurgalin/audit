
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения; // используется механизмом обработки изменения реквизитов ТЧ

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2КопированиеСтрокСервер.ПриСозданииНаСевере( Элементы, "Товары" );
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьУсловноеОформление();
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)

	Если ИмяСобытия = "ПодборНоменклатурыПроизведен"
		И ЗначениеЗаполнено(Параметр)
		И Источник = УникальныйИдентификатор Тогда

		ЗагрузитьНоменклатуруИзХранилища(Параметр);

	КонецЕсли;

	к2КопированиеСтрокКлиент.ОбработкаОповещения(ИмяСобытия, Параметр, Элементы);

	РасхожденияАктуальны = Ложь;

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)

	к2СобытияФорм.ОбработкаПроверкиЗаполненияНаСервере(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);

КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)

	к2ЗамерыКлиент.ПередЗаписьюДокумента(ЭтотОбъект, ПараметрыЗаписи);

	ПроверитьРасхождения(Отказ);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	ЗаполнитьСлужебныеРеквизиты();
	
	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)

	ПроверитьРасхождения(Отказ);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОснованиеПриИзменении(Элемент)

	Если ЗначениеЗаполнено(Объект.Основание) Тогда

		ОснованиеПриИзмененииНаСервере();

	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтнестиУслугиВСебестоимостьПриИзменении(Элемент)
	
	НастроитьВидимостьПоОтнесениюУслуг(ЭтотОбъект);
	
	Если Не Объект.ОтнестиУслугиВСебестоимость Тогда
		
		к2КоллекцииКлиентСервер.ЗаполнитьДанныеФормыКоллекцию(Объект.Товары, Неопределено, "СуммаУслуг", 0);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТовары

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)

	к2ПокупкиКлиент.НоменклатураПриИзмененииБезРасчетаИтогов(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыХарактеристикаПриИзменении(Элемент)
	
	к2ДокументыКлиент.ХарактеристикаПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	
	к2ДокументыКлиент.КоличествоПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоУпаковокПриИзменении(Элемент)
	
	к2ДокументыКлиент.КоличествоУпаковокПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыУпаковкаПриИзменении(Элемент)
	
	к2ДокументыКлиент.УпаковкаПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)
	
	к2ДокументыКлиент.ЦенаПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСтавкаНДСПриИзменении(Элемент)
	
	к2ДокументыКлиент.СтавкаНДСПриИзменении(ЭтотОбъект, КэшированныеЗначения);
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	
	Если Копирование Тогда
		ТекущиеДанные.КлючСтроки = "";
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)

	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыПослеУдаления(Элемент)

	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ТоварыПриИзменении(Элемент)
	
	РасхожденияАктуальны = Ложь;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормы_Услуги

&НаКлиенте
Процедура УслугиНоменклатураПриИзменении(Элемент)

	ТекущиеДанные = Элементы.Услуги.ТекущиеДанные;

	СтруктураДействий = Новый Структура;

	к2ДокументыКлиентСервер.ДобавитьВСтруктуруДействийЗаполнениеЦены(СтруктураДействий, Объект);

	СтруктураДействий.Вставить("ЗаполнитьСтавкуНДС", ТекущиеДанные.Номенклатура);

	ПересчитатьСуммыВСтрокеУслуг(СтруктураДействий);

КонецПроцедуры

&НаКлиенте
Процедура УслугиКоличествоПриИзменении(Элемент)

	ПересчитатьСуммыВСтрокеУслуг();

КонецПроцедуры

&НаКлиенте
Процедура УслугиЦенаПриИзменении(Элемент)

	СтруктураЗаполненияЦены = к2ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруЗаполненияЦеныПоСоглашению(Объект);

	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьЦенуСУчетомСкидки", СтруктураЗаполненияЦены);
	СтруктураДействий.Вставить("ЗаполнитьПроцентСкидки");

	к2ДокументыКлиентСервер.ПереопределитьПересчетСкидки(Элементы.Услуги.ТекущиеДанные, СтруктураДействий);

	ПересчитатьСуммыВСтрокеУслуг(СтруктураДействий);

КонецПроцедуры

&НаКлиенте
Процедура УслугиСтавкаНДСПриИзменении(Элемент)

	ПересчитатьСуммыВСтрокеУслуг();

КонецПроцедуры

&НаКлиенте
Процедура УслугиПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)

	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура УслугиПослеУдаления(Элемент)

	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура УслугиПриИзменении(Элемент)

	РасхождениеУслугАктуальны = Ложь;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СчетФактура( Команда )
	
	к2ПокупкиКлиент.СоздатьСчетФактуру( ЭтотОбъект );
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьТовары( Команда )
	
	ДополнительныеПараметрыПодбора = НоменклатураКлиент.ДополнительныеПараметрыПодбора();
	ДополнительныеПараметрыПодбора.Контрагент = Объект.Поставщик;
	
	НоменклатураКлиент.ПодобратьНоменклатуруСПараметрами( ЭтотОбъект, АдресПодобраннойНоменклатуры(), , ДополнительныеПараметрыПодбора );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКопироватьСтроки( Команда )
	
	количествоСкопировано = СкопироватьСтрокиТоваровНаСервере();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОКопированииСтрок( количествоСкопировано, "Товары" );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыВставитьСтроки( Команда )
	
	количествоВставлено = ПолучитьСтрокиТоваровИзБуфераОбмена();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОВставкеСтрок( количествоВставлено );
	
	РасхожденияАктуальны = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПоОснованию( Команда )
	
	Если Не ЗначениеЗаполнено(Объект.Основание) Тогда
		
		ТекстСообщения = НСтр( "ru = 'Основание не заполнено'" );
		ОбщегоНазначенияКлиент.СообщитьПользователю( ТекстСообщения );
		Возврат;
		
	КонецЕсли;
	
	ОповещениеОЗакрытии = Новый ОписаниеОповещения( "ЗаполнитьПоОснованиюЗавершение", ЭтотОбъект );
	ТекстВопроса = НСтр( "ru = 'Табличная часть будет очищена. Продолжить?'" );
	
	ПоказатьВопрос( ОповещениеОЗакрытии, ТекстВопроса, РежимДиалогаВопрос.ДаНет );
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьРасхождения(Команда)

	Если Не ЗначениеЗаполнено(Объект.Основание) Тогда

		ТекстСообщения = НСтр("ru = 'Основание не заполнено'");
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения, "Объект.Основание");
		Возврат;

	КонецЕсли;

	ЗаполнитьРасхожденияНаСервере();

КонецПроцедуры

&НаКлиенте
Процедура РазбитьСтроку( Команда )
	
	ТаблицаФормы  = Элементы.Товары;
	ДанныеТаблицы = Объект.Товары;
	ОповещениеПослеРазбиения = Новый ОписаниеОповещения("РазбитьСтрокуЗавершение", ЭтотОбъект);
	
	к2ОбщегоНазначенияКлиент.РазбитьСтрокуТЧ(ДанныеТаблицы, ТаблицаФормы, ОповещениеПослеРазбиения, Неопределено);

КонецПроцедуры

&НаКлиенте
Процедура РаспределитьПоКоличеству(Команда)

	к2ФинансовыйРезультатКлиент.РаспределитьСуммуУслуг(Объект.Товары, Объект.Услуги.Итог("СуммаСНДС"), "Количество");

КонецПроцедуры

&НаКлиенте
Процедура РаспределитьПоСумме(Команда)

	к2ФинансовыйРезультатКлиент.РаспределитьСуммуУслуг(Объект.Товары, Объект.Услуги.Итог("СуммаСНДС"), "СуммаСНДС");

КонецПроцедуры

&НаКлиенте
Процедура РаспределитьПоВесу(Команда)

	к2ФинансовыйРезультатКлиент.РаспределитьСуммуУслуг(Объект.Товары, Объект.Услуги.Итог("СуммаСНДС"), "Вес");

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Объект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область НастройкаФормы

&НаСервере
Процедура НастроитьФорму()
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформление()

	к2УО.Товары(ЭтотОбъект);
	к2УО.Товары(ЭтотОбъект, "Расхождения");
	к2УО.Товары(ЭтотОбъект, "РасхожденияУслуг");
	к2УО.Товары(ЭтотОбъект, "Услуги");
	
КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементы()
	
	Для Каждого цИмяТЧ Из СтрРазделить("Товары,Расхождения", ",") Цикл

		добавленныеЭлементы = к2Документы.ДобавитьИспользованияДополнительныхЕдиницИзмерения(ЭтотОбъект, цИмяТЧ, цИмяТЧ);

		// Перемещение существующей группы в созданную
		Элементы.Переместить(Элементы.Найти(цИмяТЧ + "Группа_ВЕдиницахХранения"),
							 добавленныеЭлементы.ГруппаКоличество,
							 добавленныеЭлементы.ГруппаВЕдиницахХранения1);
						 
		// Перемещение новой группы с количествами перед ценой
		Элементы.Переместить(добавленныеЭлементы.ГруппаКоличество,
							 добавленныеЭлементы.ГруппаКоличество.Родитель,
							 Элементы.Найти(цИмяТЧ + "Цена"));

		к2Документы.НастроитьЭлементыТабличнойЧасти(ЭтотОбъект, цИмяТЧ);

	КонецЦикла;
	
	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.ТоварыНомерСтроки);
	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.ТоварыНоменклатура);
	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.ТоварыХарактеристика);
	
	ЗаполнитьЕдиницыИзмеренияВНадписях();
	НастроитьГруппу_ЦеныИВалюта();
	ФинансовыйРезультат_НастроитьЭлементы();
	
КонецПроцедуры

&НаСервере
Процедура НастроитьГруппу_ЦеныИВалюта()
	
	к2Покупки.НастроитьГруппу_ЦеныИВалюта(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьЕдиницыИзмеренияВНадписях()
	
	к2ИзменениеФорм.ЗаполнитьЕдиницыИзмеренияВЗаголовкахДокумента(ЭтотОбъект, Объект.Валюта);
	
	Если ЗначениеЗаполнено( Объект.Валюта ) Тогда
		
		постфикс = ", " + Объект.Валюта;
		
	Иначе
		
		постфикс = "";
		
	КонецЕсли;
	
	Элементы.УслугиЦена.Заголовок = НСтр( "ru='Цена'" ) + постфикс;
	Элементы.УслугиСумма.Заголовок = НСтр( "ru='Сумма'" ) + постфикс;
	Элементы.УслугиСуммаСНДС.Заголовок = НСтр( "ru='Всего'" ) + постфикс;
	Элементы.УслугиСуммаНДС.Заголовок = НСтр( "ru='НДС'" ) + постфикс;
	Элементы.ТоварыСуммаУслуг.Заголовок = НСтр( "ru='Услуги'" ) + постфикс;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.Ячейка.Видимость = глРеквизит(Объект.Склад, "ИспользоватьАдресноеХранение");
	
КонецПроцедуры

#КонецОбласти

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()

	НастроитьЭлементы();
	НастроитьФорму();

	ЗаполнитьСлужебныеРеквизиты();

	НастроитьОтображениеЭлементовПоИтогамРасхождений();

	ЗаполнитьСчетФактураНаСервере();

	РасхожденияАктуальны = Истина;
	РасхождениеУслугАктуальны = Истина;

КонецПроцедуры

&НаКлиенте
Процедура ПроверитьРасхождения(Отказ)

	Если РасхожденияАктуальны
		И РасхождениеУслугАктуальны Тогда
		Возврат;
	КонецЕсли;

	СписокКнопок = Новый СписокЗначений;
	СписокКнопок.Добавить(КодВозвратаДиалога.Да, НСтр("ru='Перезаполнить'"));
	СписокКнопок.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru='Отмена'"));

	Отказ = Истина;
	ПоказатьВопрос(Новый ОписаниеОповещения("ПередЗаписьюВопросЗавершение", ЭтотОбъект),
				   НСтр("ru='Табличная часть ""Товары"" была изменена. Перезаполнить расхождения?'"),
				   СписокКнопок,
				   ,
				   КодВозвратаДиалога.Да);

КонецПроцедуры

&НаСервере
Процедура ОснованиеПриИзмененииНаСервере()

	ТипОснования = ТипЗнч(Объект.Основание);

	Если ТипОснования = Тип("ДокументСсылка.ПоступлениеТоваровУслуг") Тогда
		СвойстваИсключения = "Номер,Дата,Ответственный,Товары,Услуги";
		Объект.ДокументПоступление = Объект.Основание;
	Иначе
		СвойстваИсключения = "Номер,Дата,Ответственный,Основание,Товары,Услуги,Расхождения,РасхожденияУслуг";
	КонецЕсли;

	ЗаполнитьЗначенияСвойств(Объект, Объект.Основание, , СвойстваИсключения);

	ЗаполнитьТоварыПоОснованиюНаСервере();
	НастроитьФорму();

КонецПроцедуры

#Область СчетаФактуры

&НаКлиенте
Процедура СчетФактураЗакрытие( РезультатВопроса, ДополнительныеПараметры ) Экспорт
	
	ЗаполнитьСчетФактураНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСчетФактураНаСервере()
	
	к2Покупки.ЗаполнитьСчетФактуру( ЭтотОбъект );
	
КонецПроцедуры

#КонецОбласти

#Область Подборы

&НаСервере
Функция АдресПодобраннойНоменклатуры()
	
	Возврат ПоместитьВоВременноеХранилище( Объект.Товары.Выгрузить() );
	
КонецФункции

&НаСервере
Процедура ЗагрузитьНоменклатуруИзХранилища( Адрес )
	
	Если Не ЗначениеЗаполнено( Адрес ) Тогда
		
		Возврат;
	
	КонецЕсли;
	
	тзНоменклатура = ПолучитьИзВременногоХранилища( Адрес );
	тзНоменклатура.Свернуть( "Номенклатура,Характеристика,Упаковка" );
	
	к2Покупки.ЗагрузитьТоварыИзТаблицы( Объект, тзНоменклатура );
	
	ЗаполнитьСлужебныеРеквизиты();
	
КонецПроцедуры

#КонецОбласти

#Область КопированиеВставка

&НаСервере
Функция СкопироватьСтрокиТоваровНаСервере()
	
	Возврат к2КопированиеСтрокСервер.Копировать( Объект.Товары, Элементы.Товары.ВыделенныеСтроки, "Товары" );
	
КонецФункции

&НаСервере
Функция ПолучитьСтрокиТоваровИзБуфераОбмена()
	
	добавленоСтрок = к2КопированиеСтрокСервер.Вставить( Объект.Товары, Элементы.Товары, "Товары", "Номенклатура,Характеристика,Упаковка" );
	
	КэшированныеЗначения = Неопределено;
	
	Для Каждого цСтрока Из Элементы.Товары.ВыделенныеСтроки Цикл
		
		текСтрока = Объект.Товары.НайтиПоИдентификатору( цСтрока );
		к2Покупки.СтрокаТоваровПриИзменении(Объект, текСтрока, КэшированныеЗначения);
		
	КонецЦикла;
	
	ЗаполнитьСлужебныеРеквизиты();
	
	Возврат добавленоСтрок;
	
КонецФункции

#КонецОбласти

#Область ФинансовыйРезультат

&НаСервере
Процедура ФинансовыйРезультат_НастроитьЭлементы()
	
	включенФинансовыйРезультат = к2ФинансовыйРезультат.Включен();
	
	Элементы.СтраницаУслуги.Видимость = включенФинансовыйРезультат;
	Элементы.РасхождениеУслуг.Видимость = включенФинансовыйРезультат;
	Элементы.ТоварыСуммаУслуг.Видимость = включенФинансовыйРезультат И Объект.ОтнестиУслугиВСебестоимость;
	Элементы.РасхожденияСуммаУслуг.Видимость = включенФинансовыйРезультат И Объект.ОтнестиУслугиВСебестоимость;
	Элементы.ТоварыГруппа_Распределить.Видимость = включенФинансовыйРезультат И Объект.ОтнестиУслугиВСебестоимость;
	
	к2Документы.НастроитьЭлементыТабличнойЧасти(ЭтотОбъект, "Услуги");
	к2Документы.НастроитьЭлементыТабличнойЧасти(ЭтотОбъект, "РасхождениеУслуг");

	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.УслугиНомерСтроки);
	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.УслугиНоменклатура);

	к2ИзменениеФорм.НастроитьКолонку_Ссылка(Элементы.УслугиНаправлениеДеятельности);
	к2ИзменениеФорм.НастроитьКолонку_Ссылка(Элементы.УслугиПодразделение);
	
	к2ИзменениеФорм.НастроитьКолонку_Сумма(Элементы.ТоварыСуммаУслуг);
	к2ИзменениеФорм.НастроитьКолонку_Сумма(Элементы.РасхожденияСуммаУслуг);
	
	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.РасхождениеУслугНомерСтроки);
	к2ИзменениеФорм.ФиксацияВТаблице_Лево(Элементы.РасхождениеУслугНоменклатура);

	к2ИзменениеФорм.НастроитьКолонку_Ссылка(Элементы.РасхождениеУслугНаправлениеДеятельности);
	к2ИзменениеФорм.НастроитьКолонку_Ссылка(Элементы.РасхождениеУслугПодразделение);
	
	НастроитьВидимостьПоОтнесениюУслуг(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура НастроитьВидимостьПоОтнесениюУслуг(Форма)

	Элементы = Форма.Элементы;
	ОтнестиУслугиВСебестоимость = Форма.Объект.ОтнестиУслугиВСебестоимость;
	
	Элементы.УслугиПодразделение.Видимость = Не ОтнестиУслугиВСебестоимость;
	Элементы.УслугиНаправлениеДеятельности.Видимость = Не ОтнестиУслугиВСебестоимость;
	Элементы.УслугиСтатьяЗатрат.Видимость = Не ОтнестиУслугиВСебестоимость;
	Элементы.УслугиАналитикаЗатрат.Видимость = Не ОтнестиУслугиВСебестоимость;
	
	Элементы.РасхождениеУслугПодразделение.Видимость = Не ОтнестиУслугиВСебестоимость;
	Элементы.РасхождениеУслугНаправлениеДеятельности.Видимость = Не ОтнестиУслугиВСебестоимость;
	Элементы.РасхождениеУслугСтатьяЗатрат.Видимость = Не ОтнестиУслугиВСебестоимость;
	Элементы.РасхождениеУслугАналитикаЗатрат.Видимость = Не ОтнестиУслугиВСебестоимость;
	
	Элементы.ТоварыСуммаУслуг.Видимость = ОтнестиУслугиВСебестоимость;
	Элементы.РасхожденияСуммаУслуг.Видимость = ОтнестиУслугиВСебестоимость;
	
	Элементы.ТоварыГруппа_Распределить.Видимость = ОтнестиУслугиВСебестоимость;
	
КонецПроцедуры

&НаКлиенте
Процедура ПересчитатьСуммыВСтрокеУслуг(СтруктураДействий = Неопределено)
	
	ТекущиеДанные = Элементы.Услуги.ТекущиеДанные;
	
	Если СтруктураДействий = Неопределено Тогда
	
		СтруктураДействий = Новый Структура;
		
	КонецЕсли;
	
	к2ДокументыКлиентСервер.ДобавитьВСтруктуруДействийПересчетСуммы(СтруктураДействий, Объект, ТекущиеДанные);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры
	
#КонецОбласти

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьИтоговыеПоказатели(Форма)

	параметрыДокумента = к2ДокументыКлиентСервер.ПараметрыДокумента();
	параметрыДокумента.ИменаТЧ = "Товары, Услуги";
	параметрыДокумента.ВернутьМногооборотнуюТару = Форма.Объект.ВернутьМногооборотнуюТару;

	к2ДокументыКлиентСервер.РассчитатьИтоговыеПоказатели(Форма, параметрыДокумента);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизиты()

	ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВРасхождениях();
	ЗаполнитьСлужебныеРеквизитыПоНоменклатуре();

	к2Документы.ОбновитьПризнакБезВозвратнойТары(ЭтотОбъект);

	РассчитатьИтоговыеПоказатели(ЭтотОбъект);

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВРасхождениях()

	СтруктураДействий = СтруктураДействийЗаполненияНоменклатуры();

	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.Расхождения, СтруктураДействий);

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизитыПоНоменклатуре()

	СтруктураДействий = СтруктураДействийЗаполненияНоменклатуры();
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.Товары, СтруктураДействий);

КонецПроцедуры

&НаСервере
Функция СтруктураДействийЗаполненияНоменклатуры()
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются", Новый Структура("Номенклатура", "ХарактеристикиИспользуются"));
	СтруктураДействий.Вставить("ЗаполнитьПризнакСерииИспользуются", Новый Структура("Номенклатура", "СерииИспользуются"));

	СтруктураДействий.Вставить("ЗаполнитьВариантИспользованияДополнительныхЕдиницИзмерения",
								Новый Структура("Номенклатура", "ИспользованиеДопЕдиницИзмерения"));

	СтруктураДействий.Вставить("ЗаполнитьПризнакТипНоменклатуры", Новый Структура("Номенклатура", "ТипНоменклатуры"));
	
	Возврат СтруктураДействий;
	
КонецФункции

&НаКлиенте
Процедура РазбитьСтрокуЗавершение(НоваяСтрока, ДополнительныеПараметры) Экспорт
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	Если НоваяСтрока <> Неопределено Тогда
		
		НоваяСтрока.КлючСтроки = "";
		
		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ПересчитатьКоличествоЕдиниц");
		СтруктураДействий.Вставить("ЗаполнитьДубликатыЗависимыхРеквизитов", к2ДокументыКлиентСервер.ЗависимыеРеквизитыТары());
		к2ДокументыКлиентСервер.ДобавитьВСтруктуруДействийПересчетСуммы( СтруктураДействий, Объект, ТекущаяСтрока );
		
		к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
		
		СтруктураДействий.Вставить("ПересчитатьВес", к2ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруПересчетаВеса());
		
		к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, КэшированныеЗначения);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПоОснованиюЗавершение(Результат, ДополнительныеПараметры) Экспорт

	Если Не Результат = КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;

	ЗаполнитьТоварыПоОснованиюНаСервере();

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТоварыПоОснованиюНаСервере()
	
	к2КоллекцииКлиентСервер.ЗагрузитьВКоллекцию(Объект.Товары, Объект.Основание.Товары);
	к2КоллекцииКлиентСервер.ЗагрузитьВКоллекцию(Объект.Услуги, Объект.Основание.Услуги);
	
	ЗаполнитьСлужебныеРеквизиты();
	
	РасхожденияАктуальны = Истина;
	РасхождениеУслугАктуальны = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписьюВопросЗавершение( Ответ, ДополнительныеПараметры ) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		
		ЗаполнитьРасхожденияНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРасхожденияНаСервере()

	Документы.КорректировкаПоступления.ЗаполнитьРасхождения(Объект);

	ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВРасхождениях();
	НастроитьОтображениеЭлементовПоИтогамРасхождений();

	РасхожденияАктуальны = Истина;
	РасхождениеУслугАктуальны = Истина;

КонецПроцедуры

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура НастроитьОтображениеЭлементовПоИтогамРасхождений()
	
	ИтогСуммаСНДСУвеличение = 0;
	ИтогСуммаСНДСУменьшение = 0;
	ИтогСуммаНДСУвеличение = 0;
	ИтогСуммаНДСУменьшение = 0;
	
	ВернутьМногооборотнуюТару = Объект.ВернутьМногооборотнуюТару;
	
	Для Каждого СтрокаТЧ Из Объект.Расхождения Цикл

		Если ВернутьМногооборотнуюТару
			И Перечисления.к2ТипыНоменклатуры.ЭтоМногооборотнаяТара(СтрокаТЧ.ТипНоменклатуры) Тогда
			Продолжить;
		КонецЕсли;

		Если СтрокаТЧ.СуммаСНДС > 0 Тогда

			ИтогСуммаСНДСУвеличение = ИтогСуммаСНДСУвеличение + СтрокаТЧ.СуммаСНДС;
			ИтогСуммаНДСУвеличение = ИтогСуммаНДСУвеличение + СтрокаТЧ.СуммаНДС;

		Иначе

			ИтогСуммаСНДСУменьшение = ИтогСуммаСНДСУменьшение - СтрокаТЧ.СуммаСНДС;
			ИтогСуммаНДСУменьшение = ИтогСуммаНДСУменьшение - СтрокаТЧ.СуммаНДС;

		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого цСтрокаУслуг Из Объект.РасхождениеУслуг Цикл

		Если цСтрокаУслуг.СуммаСНДС > 0 Тогда

			ИтогСуммаСНДСУвеличение = ИтогСуммаСНДСУвеличение + цСтрокаУслуг.СуммаСНДС;
			ИтогСуммаНДСУвеличение = ИтогСуммаНДСУвеличение + цСтрокаУслуг.СуммаНДС;

		Иначе

			ИтогСуммаСНДСУменьшение = ИтогСуммаСНДСУменьшение - цСтрокаУслуг.СуммаСНДС;
			ИтогСуммаНДСУменьшение = ИтогСуммаНДСУменьшение - цСтрокаУслуг.СуммаНДС;

		КонецЕсли;
		
	КонецЦикла;
		
КонецПроцедуры

#КонецОбласти

#КонецОбласти
