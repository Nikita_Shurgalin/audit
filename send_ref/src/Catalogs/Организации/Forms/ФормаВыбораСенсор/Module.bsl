
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить();
		
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ОповеститьОВыбореОрганизации();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаВыбор(Команда)
	
	ОповеститьОВыбореОрганизации();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОповеститьОВыбореОрганизации()
	
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		ВыбранноеЗначение = ПредопределенноеЗначение("Справочник.Организации.ПустаяСсылка");
	Иначе
		ВыбранноеЗначение = ТекущиеДанные.Ссылка;
	КонецЕсли;
	
	ОповеститьОВыборе(ВыбранноеЗначение);
	
КонецПроцедуры

#КонецОбласти
