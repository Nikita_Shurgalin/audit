#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ПередУдалением(Отказ)
	
	Отказ = НельзяУдалять;
	глЛог( НСтр( "ru='Это константа используется системой.'" ) );
	
КонецПроцедуры

#КонецЕсли