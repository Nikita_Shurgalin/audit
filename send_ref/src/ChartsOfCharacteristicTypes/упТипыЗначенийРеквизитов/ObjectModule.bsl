#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;
	
	ПредставлениеТипа = Строка(ТипЗначения);
	
	xmlПредставление = упКоллекции.ПолучитьXMLПредставлениеОписанияТипа( ТипЗначения );
	
КонецПроцедуры



Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч( ДанныеЗаполнения ) = Тип( "Структура" )Тогда
		
		ЗаполнитьЗначенияСвойств( ЭтотОбъект , ДанныеЗаполнения );
		
	КонецЕсли;
	
КонецПроцедуры

#КонецЕсли