#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);

КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	МассивНепроверяемыхРеквизитов.Добавить("Характеристика");
	
	Запрос = Новый Запрос;
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ТаблицаТоваров.Номенклатура КАК Номенклатура
	|ПОМЕСТИТЬ СтрокиСОшибками
	|ИЗ
	|	&ТаблицаТоваров КАК ТаблицаТоваров
	|ГДЕ
	|	ТаблицаТоваров.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВЫБОР
	|		КОГДА ВЫРАЗИТЬ(СтрокиСОшибками.Номенклатура КАК Справочник.Номенклатура).ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.к2ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры)
	|				ИЛИ ВЫРАЗИТЬ(СтрокиСОшибками.Номенклатура КАК Справочник.Номенклатура).ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.к2ВариантыИспользованияХарактеристикНоменклатуры.ИндивидуальныеДляНоменклатуры)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК НеЗаполненаХарактеристика
	|ИЗ
	|	СтрокиСОшибками КАК СтрокиСОшибками
	|ГДЕ
	|	ВЫРАЗИТЬ(СтрокиСОшибками.Номенклатура КАК Справочник.Номенклатура).ИспользованиеХарактеристик <> ЗНАЧЕНИЕ(Перечисление.к2ВариантыИспользованияХарактеристикНоменклатуры.НеИспользовать)";

	Запрос.Текст = ТекстЗапроса;
	
	Запрос.УстановитьПараметр("ТаблицаТоваров",  ЭтотОбъект.Выгрузить(,"Номенклатура,Характеристика"));
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		Если Выборка.НеЗаполненаХарактеристика Тогда
			ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Поле ""Характеристика"" не заполнено'"),,"Характеристика","Запись",Отказ);
		КонецЕсли;
		
	КонецЦикла;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)
	
	Если Замещение Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	НаборЗаписей.Номенклатура КАК Номенклатура,
		|	НаборЗаписей.Характеристика КАК Характеристика,
		|	НаборЗаписей.Ячейка КАК Ячейка
		|ПОМЕСТИТЬ НаборЗаписейНовый
		|ИЗ
		|	&НаборЗаписей КАК НаборЗаписей
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	НаборЗаписейНовый.Номенклатура КАК Номенклатура,
		|	НаборЗаписейНовый.Характеристика КАК Характеристика,
		|	НаборЗаписейНовый.Ячейка КАК Ячейка,
		|	ВЫБОР
		|		КОГДА к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию.Ячейка.Владелец ЕСТЬ NULL
		|			ТОГДА ИСТИНА
		|		ИНАЧЕ ВЫРАЗИТЬ(НаборЗаписейНовый.Ячейка КАК Справочник.к2СкладскиеЯчейки).Владелец = к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию.Ячейка.Владелец
		|	КОНЕЦ КАК СкладСовпадает,
		|	к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию.Ячейка.Владелец КАК СкладХранения
		|ПОМЕСТИТЬ ДанныеПроверки
		|ИЗ
		|	НаборЗаписейНовый КАК НаборЗаписейНовый
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию КАК к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию
		|		ПО (к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию.Номенклатура = НаборЗаписейНовый.Номенклатура)
		|			И (к2ЯчейкиХраненияПодпиткиНоменклатурыПоУмолчанию.Характеристика = НаборЗаписейНовый.Характеристика)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДанныеПроверки.Номенклатура КАК Номенклатура,
		|	ДанныеПроверки.Характеристика КАК Характеристика,
		|	ДанныеПроверки.Ячейка КАК Ячейка,
		|	ДанныеПроверки.СкладСовпадает КАК СкладСовпадает,
		|	ДанныеПроверки.СкладХранения КАК СкладХранения
		|ИЗ
		|	ДанныеПроверки КАК ДанныеПроверки
		|ГДЕ
		|	НЕ ДанныеПроверки.СкладСовпадает";
	
	Запрос.УстановитьПараметр("НаборЗаписей", ЭтотОбъект.Выгрузить());
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	ТекстСообщения = НСтр("ru='Склад ячейки %1 не совпадает с текущим складом хранения номенклатуры ""%2"".'");
	
	Пока Выборка.Следующий() Цикл
		
		ОбщегоНазначения.СообщитьПользователю(стрЗаполнить(ТекстСообщения, Выборка.Ячейка, Выборка.СкладХранения), , , ,Отказ);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
