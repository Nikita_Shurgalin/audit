
#Область СлужебныйПрограммныйИнтерфейс

Процедура ПриЧтенииСоздании(Форма) Экспорт
	
	Номенклатура_ФормаГруппы_ПриЧтенииСоздании(Форма, Форма.Элементы);
	Номенклатура_ФормаЭлемента_ПриЧтенииСоздании(Форма, Форма.Элементы);
	Склады_ФормаГруппы_ПриЧтенииСоздании(Форма, Форма.Элементы);
	Склады_ФормаЭлемента_ПриЧтенииСоздании(Форма, Форма.Элементы);
	
	РаспоряжениеНаПеремещение_ФормаДокумента_ПриЧтенииСоздании(Форма, Форма.Элементы);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура Номенклатура_ФормаГруппы_ПриЧтенииСоздании(Форма, Элементы)
	
	Если Не Форма.ИмяФормы = Метаданные.Справочники.Номенклатура.Формы.ФормаГруппы.ПолноеИмя() Тогда
		Возврат;
	КонецЕсли;
	
	к2ЗагрузкаОбменДаннымиИБ.ДобавитьСлужебныеРеквизитыНаФорму(Форма, Элементы);
	
КонецПроцедуры

Процедура Номенклатура_ФормаЭлемента_ПриЧтенииСоздании(Форма, Элементы)
	
	Если Не Форма.ИмяФормы = Метаданные.Справочники.Номенклатура.Формы.ФормаЭлемента.ПолноеИмя() Тогда
		Возврат;
	КонецЕсли;
	
	к2ЗагрузкаОбменДаннымиИБ.ДобавитьСлужебныеРеквизитыНаФорму(Форма, Элементы, Элементы.ОсновноеЛеваяКолонка, Элементы.ГруппаКартинка);
	
КонецПроцедуры

Процедура Склады_ФормаГруппы_ПриЧтенииСоздании(Форма, Элементы)
	
	Если Не Форма.ИмяФормы = Метаданные.Справочники.Склады.Формы.ФормаГруппы.ПолноеИмя() Тогда
		Возврат;
	КонецЕсли;
	
	к2ЗагрузкаОбменДаннымиИБ.ДобавитьСлужебныеРеквизитыНаФорму(Форма, Элементы);
	
КонецПроцедуры

Процедура Склады_ФормаЭлемента_ПриЧтенииСоздании(Форма, Элементы)
	
	Если Не Форма.ИмяФормы = Метаданные.Справочники.Склады.Формы.ФормаЭлемента.ПолноеИмя() Тогда
		Возврат;
	КонецЕсли;
	
	к2ЗагрузкаОбменДаннымиИБ.ДобавитьСлужебныеРеквизитыНаФорму(Форма, Элементы, Элементы.ГруппаСтраницаОсновная);
	
КонецПроцедуры

Процедура РаспоряжениеНаПеремещение_ФормаДокумента_ПриЧтенииСоздании(Форма, Элементы)
	
	Если Не Форма.ИмяФормы = Метаданные.Документы.к2РаспоряжениеНаПеремещение.Формы.ФормаДокумента.ПолноеИмя() Тогда
		Возврат;
	КонецЕсли;
	
	к2ЗагрузкаОбменДаннымиИБ.ДобавитьСлужебныеРеквизитыНаФорму(Форма, Элементы, Элементы.ГруппаШапка);
	
КонецПроцедуры

#КонецОбласти
