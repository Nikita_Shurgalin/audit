

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено( Параметры.СостояниеПриемник ) Тогда
		
		ВерсияБП = глРеквизит_Кэш( Параметры.СостояниеПриемник , "Владелец" );
		
		СостояниеПриемник = Параметры.СостояниеПриемник;
		
		НаборЗаписей.Сортировать( "Приоритет" );
		
	Иначе
		
		Отказ = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	ТекущийОбъект.Отбор.СостояниеПриемник.Установить(СостояниеПриемник);
	
	цПриоритет = 0;
	
	Для каждого цСтрока Из ТекущийОбъект Цикл
		
		цСтрока.СостояниеПриемник = СостояниеПриемник;
		цСтрока.Приоритет = цПриоритет;
		
		цПриоритет = цПриоритет + 1;
		
	КонецЦикла;
	
КонецПроцедуры


&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить( упИК.ОбновленаДинамическаяАдресация() , Параметры.СостояниеПриемник );
	
КонецПроцедуры



