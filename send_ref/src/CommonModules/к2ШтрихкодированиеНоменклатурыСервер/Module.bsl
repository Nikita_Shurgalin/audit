
#Область ПрограммныйИнтерфейс

// Функция - Право регистрации штрихкодов номенклатуры доступно
// 
// Возвращаемое значение:
// Булево - Истина, если доступно.
//
Функция ПравоРегистрацииШтрихкодовНоменклатурыДоступно() Экспорт
	
	Возврат ПравоДоступа("Изменение", Метаданные.РегистрыСведений.к2ШтрихкодыНоменклатуры);
	
КонецФункции

#КонецОбласти

