#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Возвращает номер автомобиля и прицепа
//
// Параметры:
//  Автомобиль - Строка, СправочникСсылка.к2ТранспортныеСредства - автомобиль
// 
// Возвращаемое значение:
//   Структура - данные об автомобиле
//
Функция ДанныеАвтомобиля(Автомобиль) Экспорт
	
	Структура = Новый Структура;
	Структура.Вставить("НомерАвтомобиля", "");
	Структура.Вставить("НомерПрицепа", "");
	
	Если ТипЗнч(Автомобиль) = Тип("СправочникСсылка.к2ТранспортныеСредства") Тогда 
		
		Структура.НомерАвтомобиля = Автомобиль.Код;
		Структура.НомерПрицепа = Автомобиль.ГосНомерПрицепа;
		
	Иначе 
		
		Структура.НомерАвтомобиля = Автомобиль;
		
	КонецЕсли;
	
	Возврат Структура;
	
КонецФункции

#КонецОбласти

#КонецЕсли