
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Не Параметры.Отбор.Свойство("ТорговоеПредложение", ТорговоеПредложение) 
		Или Не ЗначениеЗаполнено(ТорговоеПредложение) Тогда;
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Для Каждого Запись Из ТекущийОбъект Цикл
		Запись.ТорговоеПредложение = ТорговоеПредложение;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти




