
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	
	ИспользоватьОтбор = ЗначениеЗаполнено(Параметры.ТипДокумента)
		И Не Параметры.ТипДокумента = Перечисления.к2ТипыДокументовДляУчетаСдельнойОплаты.Переработка;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список
		, "Предопределенный"
		, Ложь
		,
		,
		, ИспользоватьОтбор);
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список
		, "ВариантВыработки"
		, СписокДополнительныеВариантыВыработкиДляПереработки()
		, ВидСравненияКомпоновкиДанных.НеВСписке
		,
		, ИспользоватьОтбор);
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список
		, "СпособУчета"
		, Перечисления.к2СпособыУчетаОпераций.БригадныйПоСпецификации
		, ВидСравненияКомпоновкиДанных.НеРавно
		,
		, ЗначениеЗаполнено(Параметры.ТипДокумента));
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция СписокДополнительныеВариантыВыработкиДляПереработки()
	
	СписокЗначений = Новый СписокЗначений;
	СписокЗначений.Добавить(Перечисления.к2ВариантыВыработкиНаОбъем.НаЕдиницуМатериала);
	СписокЗначений.Добавить(Перечисления.к2ВариантыВыработкиНаОбъем.НаДополнительнуюЕдиницуМатериала);
	
	Возврат СписокЗначений;
	
КонецФункции

#КонецОбласти