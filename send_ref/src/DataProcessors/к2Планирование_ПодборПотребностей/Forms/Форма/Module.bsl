
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	к2УО.Характеристика(ЭтотОбъект, "ПотребностиХарактеристика", "Объект.Потребности.ХарактеристикиИспользуются");
	
	ПрочитатьВходящиеПараметры();
	
	ВосстановитьПараметрыИзНастроек();
	
	Если ЗначениеЗаполнено(Объект.Сценарий) Тогда
		
		ПрочитатьИзСценария();
		
	Иначе
		
		УстановитьНастройкиПоУмолчанию();
		
		ЗаполнитьРеквизитыПоУмолчанию();
		
		Если ЗначениеЗаполнено(Объект.НачалоПериода)
			И ЗначениеЗаполнено(Объект.КонецПериода) Тогда
			
			ПрочитатьЗаказыНаСервере();
			
		КонецЕсли;
		
	КонецЕсли;
	
	НастроитьЭлементы_ПриСозданииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СценарийСоздание(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	параметрыФормы = Новый Структура;
	параметрыФормы.Вставить("ЗначенияЗаполнения", ПараметрыСозданияСценария());
	
	к2ЗамерыКлиент.НачалоСоздания("Справочник.к2Планирование_СценарииПодбораПотребностей.ФормаОбъекта");
	
	ОткрытьФорму(
		"Справочник.к2Планирование_СценарииПодбораПотребностей.ФормаОбъекта",
		параметрыФормы,
		Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура СценарийПриИзменении(Элемент)
	
	СценарийПриИзмененииНаСервере();
	
	// Почему то заголовок не обновляется на форме и приходится
	
	старЗаголовок = Элементы.НастроитьПериод.Заголовок;
	Элементы.НастроитьПериод.Заголовок = "";
	Элементы.НастроитьПериод.Заголовок = старЗаголовок;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормы_Потребности

&НаКлиенте
Процедура ПотребностиВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле = Элементы.ПотребностиЗаказов Тогда
		
		параметрыОткрытияОтчета = ПараметрыОткрытияОтчетаПоЗаказам();
		
		ОткрытьФорму("Отчет.к2Планирование_СтатистикаОтгрузок.Форма",
					 параметрыОткрытияОтчета,
					 ЭтотОбъект,
					 Новый УникальныйИдентификатор);
		
	Иначе
		
		к2ОбщегоНазначенияКлиент.ОткрытьЭлементПриВыбореВТаблице(Элемент, СтандартнаяОбработка);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НастроитьПериод(Команда)
	
	структПериода = Новый Структура("ДатаНачала, ДатаОкончания", "НачалоПериода", "КонецПериода");
	
	Подключаемый_НастроитьПериодОкончание = Новый ОписаниеОповещения("Подключаемый_НастроитьПериодОкончание", ЭтотОбъект);
	
	к2ОбщегоНазначенияКлиент.РедактироватьПериод(Объект, структПериода, Подключаемый_НастроитьПериодОкончание);
	
КонецПроцедуры

&НаКлиенте
Процедура ПрочитатьЗаказы(Команда)
	
	к2ЗамерыКлиент.НачалоКоманды(ЭтотОбъект, Команда);
	
	ПрочитатьЗаказыНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьРезультатом(Команда)
	
	к2ЗамерыКлиент.НачалоКоманды(ЭтотОбъект, Команда);
	
	Закрыть(РезультатПланирования());
	
КонецПроцедуры

&НаКлиенте
Процедура НастройкиПоУмолчанию(Команда)
	
	УстановитьНастройкиПоУмолчанию();
	
КонецПроцедуры

&НаКлиенте
Процедура СохранитьНастройкиВФайл(Команда)

	к2КомпоновкаДанныхКлиент.СохранитьНастройкиВФайл(АдресНастроек());

КонецПроцедуры

&НаКлиенте
Процедура ПрочитатьНастройкиИзФайла(Команда)

	оповещение = Новый ОписаниеОповещения("ПрочитатьНастройкиИзФайлаЗавершение", ЭтотОбъект);

	к2КомпоновкаДанныхКлиент.ПрочитатьФайлСНастройками(оповещение, УникальныйИдентификатор);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПрочитатьВходящиеПараметры()
	
	Параметры.Свойство("ДатаНачала", Объект.НачалоПериодаВходящее);
	Параметры.Свойство("ДатаОкончания", Объект.КонецПериодаВходящее);
	Параметры.Свойство("Периодичность", Объект.Периодичность);
	Параметры.Свойство("ИспользоватьДатыМаркировки", Объект.ИспользоватьДатыМаркировки);
	Параметры.Свойство("Сценарий", Объект.Сценарий);
	Параметры.Свойство("Дозаполнение", Дозаполнение);

КонецПроцедуры

&НаСервере
Процедура ВосстановитьПараметрыИзНастроек()
	
	постфиксДозаполнение = "";
	
	Если Дозаполнение Тогда
		
		постфиксДозаполнение = "Дозаполнение";
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Сценарий) Тогда
		
		Объект.Сценарий = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(ИмяФормы, "Сценарий" + постфиксДозаполнение);
		
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементы_ПриСозданииНаСервере()
	
	к2ИзменениеФорм.НастроитьКолонку_Номер(Элементы.ПотребностиНомерСтроки);
	к2ИзменениеФорм.НастроитьКолонку_Номенклатура(Элементы.ПотребностиНоменклатура);
	к2ИзменениеФорм.НастроитьКолонку_Характеристика(Элементы.ПотребностиХарактеристика);
	к2ИзменениеФорм.НастроитьКолонку_Склад(Элементы.ПотребностиСкладОтгрузки);
	к2ИзменениеФорм.НастроитьКолонку_Волна(Элементы.ПотребностиВолнаОтгрузки);
	
	к2ИзменениеФорм.НастроитьКолонку_Дата(Элементы.ПотребностиПериод);
	к2ИзменениеФорм.НастроитьКолонку_Дата(Элементы.ПотребностиДатаОтгрузки);
	к2ИзменениеФорм.НастроитьКолонку_Дата(Элементы.ПотребностиДатаМаркировки);
	к2ИзменениеФорм.НастроитьКолонку_Количество(Элементы.ПотребностиПлан);
	к2ИзменениеФорм.НастроитьКолонку_Количество(Элементы.ПотребностиЗаказов);
	
	Элементы.Периодичность.СписокВыбора.ЗагрузитьЗначения(к2Планирование.РазрешенныеПериодичности());
	
	Элементы.ПотребностиДатаМаркировки.Видимость = Объект.ИспользоватьДатыМаркировки;
	
	Если Дозаполнение Тогда
		
		Элементы.ФормаПередатьРезультат.Заголовок = НСтр("ru='Дополнить'");
		Элементы.ФормаПередатьРезультатРасширеннаяПодсказка.Заголовок = НСтр("ru='Добавить строки результата к существующим записям'");
		
	КонецЕсли;
	
	УстановитьЗаголовокПериода(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Функция ПараметрыСозданияСценария()
	
	адресСхемыКомпоновкиДанных = ПоместитьВоВременноеХранилище(Объект.КомпоновщикНастроек_ЗаказыКлиентов.ПолучитьНастройки());
	
	структ = Новый Структура;
	структ.Вставить("Периодичность", Объект.Периодичность);
	структ.Вставить("АдресСхемыКомпоновкиДанных", адресСхемыКомпоновкиДанных);
	
	Возврат структ;
	
КонецФункции

&НаСервере
Процедура СценарийПриИзмененииНаСервере()
	
	постфиксДозаполнение = "";
	
	Если Дозаполнение Тогда
		
		постфиксДозаполнение = "Дозаполнение";
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.Сценарий) Тогда
		
		ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(ИмяФормы, "Сценарий" + постфиксДозаполнение, Объект.Сценарий);
		
	КонецЕсли;
	
	ПрочитатьИзСценария();
	
КонецПроцедуры

&НаСервере
Процедура ПрочитатьИзСценария()
	
	Если ЗначениеЗаполнено(Объект.Сценарий) Тогда
		
		Объект.Периодичность = Объект.Сценарий.Периодичность;
	
	КонецЕсли;
	
	Обработки.к2Планирование_ПодборПотребностей.ЗаполнитьПоСценарию(Объект, УникальныйИдентификатор);
	
	УстановитьЗаголовокПериода(ЭтотОбъект);
	ПрочитатьЗаказыНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРеквизитыПоУмолчанию()
	
	Если Не ЗначениеЗаполнено(Объект.НачалоПериода) Тогда
		
		Объект.НачалоПериода = Объект.НачалоПериодаВходящее;
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.КонецПериода) Тогда
		
		Объект.КонецПериода = Объект.КонецПериодаВходящее;
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Периодичность) Тогда
		
		Объект.Периодичность = Перечисления.к2Периодичность.День;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_НастроитьПериодОкончание(Период, ДополнительныеПараметры) Экспорт
	
	УстановитьЗаголовокПериода(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьЗаголовокПериода(Форма)
	
	Форма.Элементы.НастроитьПериод.Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru='Период: %1'"),
		к2ДатыКлиентСервер.ПредставлениеИнтервала(Форма.Объект.НачалоПериода, Форма.Объект.КонецПериода));
	
КонецПроцедуры

&НаСервере
Процедура ПрочитатьЗаказыНаСервере()
	
	Если Не ЗначениеЗаполнено(Объект.Периодичность) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Обработки.к2Планирование_ПодборПотребностей.ЗаполнитьПотребности(Объект);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются",
							   Новый Структура("Номенклатура", "ХарактеристикиИспользуются"));
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.Потребности, СтруктураДействий);
	
КонецПроцедуры

&НаСервере
Функция РезультатПланирования()
	
	адрес = ПоместитьВоВременноеХранилище(Объект.Потребности.Выгрузить());
	
	Возврат Новый Структура("Адрес", адрес);
	
КонецФункции

&НаКлиенте
Функция ПараметрыОткрытияОтчетаПоЗаказам()
	
	текДанные = Элементы.Потребности.ТекущиеДанные;
	
	отбор = Новый Структура;
	отбор.Вставить("Периодичность", Объект.Периодичность);
	
	Если ЗначениеЗаполнено(текДанные.Период) Тогда
		
		период = Новый СтандартныйПериод(текДанные.Период, к2ДатыКлиентСервер.ОкончаниеИнтервала(текДанные.Период, Объект.Периодичность));
		
		отбор.Вставить("Период", период);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(текДанные.СкладОтгрузки) Тогда
		
		отбор.Вставить("СкладОтгрузки", текДанные.СкладОтгрузки);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(текДанные.Номенклатура) Тогда
		
		отбор.Вставить("Номенклатура", текДанные.Номенклатура);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(текДанные.Характеристика) Тогда
		
		отбор.Вставить("Характеристика", текДанные.Характеристика);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(текДанные.ВолнаОтгрузки) Тогда
		
		отбор.Вставить("ВолнаОтгрузки", текДанные.ВолнаОтгрузки);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(текДанные.ДатаМаркировки) Тогда
		
		отбор.Вставить("ДатаМаркировки", текДанные.ДатаМаркировки);
		
	КонецЕсли;
	
	структ = Новый Структура;
	структ.Вставить("СформироватьПриОткрытии", Истина);
	структ.Вставить("КлючНазначенияИспользования", "к2Планирование_СтатистикаОтгрузок.РасшифровкаПодбораПотребностей");
	структ.Вставить("КлючВарианта", "АнализЗаказов");
	структ.Вставить("Отбор", отбор);
	
	Возврат структ;
	
КонецФункции

&НаСервере
Процедура УстановитьНастройкиПоУмолчанию()

	УстановитьНастройкиКомпоновщика(Неопределено);

КонецПроцедуры

&НаСервере
Процедура УстановитьНастройкиКомпоновщика(пНастройкиКомпоновкиДанных)

	Обработки.к2Планирование_ПодборПотребностей.УстановитьНастройкиКомпоновщика(Объект.КомпоновщикНастроек_ЗаказыКлиентов,
																				пНастройкиКомпоновкиДанных,
																				УникальныйИдентификатор);

КонецПроцедуры

&НаСервере
Функция АдресНастроек()
	
	Возврат к2КомпоновкаДанныхСервер.АдресНастроек(Объект.КомпоновщикНастроек_ЗаказыКлиентов, УникальныйИдентификатор);
	
КонецФункции

&НаКлиенте
Процедура ПрочитатьНастройкиИзФайлаЗавершение(Результат, Контекст) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ЗагрузитьНастройкиИзФайлаНаСервере(Результат.Хранение);
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьНастройкиИзФайлаНаСервере(АдресНастроек)
	
	УстановитьНастройкиКомпоновщика(к2КомпоновкаДанныхСервер.НастройкиПоАдресу(АдресНастроек));
	
КонецПроцедуры

#КонецОбласти