#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.Таблица_к2ПравилаУчетаОпераций;
	
	Если Отказ Или Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.к2ПравилаУчетаОпераций.Записывать = Истина;
	Движения.к2ПравилаУчетаОпераций.Загрузить(Таблица);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

