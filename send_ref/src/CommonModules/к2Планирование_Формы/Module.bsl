#Область ПрограммныйИнтерфейс

#Область ИнтервалыПланированияВАРМах

// Подготавливает панель периодов в форме АРМа планирования
// 
// Параметры:
// 	Форма - ФормаКлиентскогоПриложения - Форма АРМа
Процедура ИнициализироватьПанельПериода(Форма) Экспорт
	
	Объект = Форма.Объект;
	
	Если Не ЗначениеЗаполнено(Объект.ВариантНачала) Тогда
		
		Объект.ВариантНачала = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(Форма.ИмяФормы, "ВариантНачала");
		Объект.НачалоПериода = Справочники.к2ВариантыНачалаПериода.ДатаНачала(Объект.ВариантНачала);
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.ПериодовПланирования) Тогда
		
		ПЕРИОДОВ_ПЛАНИРОВАНИЯ_ПО_УМОЛЧАНИЮ = 7;
		
		Объект.ПериодовПланирования = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(
			Форма.ИмяФормы,
			"ПериодовПланирования",
			ПЕРИОДОВ_ПЛАНИРОВАНИЯ_ПО_УМОЛЧАНИЮ);
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Периодичность) Тогда
		
		Объект.Периодичность = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(
			Форма.ИмяФормы,
			"Периодичность",
			Перечисления.к2Периодичность.День);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.НачалоПериода) Тогда
		
		ЗаполнитьКонецИнтервалаПланирования(Объект);
		
	КонецЕсли;
	
	Форма.Элементы.ПериодичностьПланирования.СписокВыбора.ЗагрузитьЗначения(к2Планирование.РазрешенныеПериодичности());
	
	НастроитьПериод(Форма);
	
КонецПроцедуры

// Заполняет данные интервала в форме АРМа из сценария
//
// Параметры:
//  Форма		 - ФормаКлиентскогоПриложения - Форма АРМа
//  пСценарий	 - СправочникСсылка.к2Планирование_СценарииПланированияЗапуска
//				   СправочникСсылка.к2Планирование_СценарииПланированияПотребностей - сценарий АРМа
//
Процедура ПрочитатьИнтервалПланированияИзСценария(Форма, Знач пСценарий) Экспорт

	Объект = Форма.Объект;
	
	ЗаполнитьЗначенияСвойств(Объект, пСценарий, "ВариантНачала,ПериодовПланирования,Периодичность");
	
	Объект.НачалоПериода = Справочники.к2ВариантыНачалаПериода.ДатаНачала(Объект.ВариантНачала);
	ЗаполнитьКонецИнтервалаПланирования(Объект);
	НастроитьПериод(Форма);

КонецПроцедуры

// Заполняет конец интервала планирования по АРМу планирования
// 
// Параметры:
// 	Объект - ДанныеФормыСтруктура - Объект
Процедура ЗаполнитьКонецИнтервалаПланирования(Объект) Экспорт
	
	Если Не ЗначениеЗаполнено(Объект.НачалоПериода)
		ИЛИ Не ЗначениеЗаполнено(Объект.Периодичность) Тогда
		
		Объект.ОкончаниеПериода = Дата(1, 1, 1);
		
	Иначе
		
		Объект.ОкончаниеПериода = к2Даты.Добавить(
			Объект.НачалоПериода,
			Объект.Периодичность,
			Объект.ПериодовПланирования,
			Ложь) - 1;
		
	КонецЕсли;
		
КонецПроцедуры

// Заполняет оформление периода в АРМах планирования
// 
// Параметры:
// 	Форма - ФормаКлиентскогоПриложения - Форма АРМа
Процедура НастроитьПериод(Форма) Экспорт

	Объект = Форма.Объект;
	Элементы = Форма.Элементы;
	
	представлениеИнтервала = к2ДатыКлиентСервер.ПредставлениеИнтервала(Объект.НачалоПериода, Объект.ОкончаниеПериода);
	
	к2ИзменениеФорм.УстановитьПредставлениеПериодаВЗаголовок(Форма, Форма.СинонимФормы, представлениеИнтервала);
	
	Элементы.Группа_Период.Заголовок = представлениеИнтервала;
	
	Если Не ЗначениеЗаполнено(Объект.НачалоПериода) Или Не ЗначениеЗаполнено(Объект.ОкончаниеПериода) Тогда
		
		Элементы.Группа_Период.ЦветТекстаЗаголовка = ЦветаСтиля.к2ЦветТекста_Красный;
		
	Иначе
		
		Элементы.Группа_Период.ЦветТекстаЗаголовка = ЦветаСтиля.к2ЗеленыйТакси;
		
	КонецЕсли;
	
	ЗаполнитьПериодВКалендаре(Форма);

КонецПроцедуры

// Обновляет панель периодов в форме АРМа планирования при изменении реквизитов
//
// Параметры:
//  Форма	 - ФормаКлиентскогоПриложения - Форма АРМа
//
Процедура ПриИзмененииИнтервалаПланирования(Форма) Экспорт

	Объект = Форма.Объект;
	
	Объект.НачалоПериода = Справочники.к2ВариантыНачалаПериода.ДатаНачала(Объект.ВариантНачала);
	
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(Форма.ИмяФормы, "ВариантНачала", Объект.ВариантНачала);
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(Форма.ИмяФормы, "ПериодовПланирования", Объект.ПериодовПланирования);
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(Форма.ИмяФормы, "Периодичность", Объект.Периодичность);
	
	ЗаполнитьКонецИнтервалаПланирования(Объект);
	
	НастроитьПериод(Форма);

КонецПроцедуры

#КонецОбласти

// Заполняет периоды в АРМах планирования
// 
// Параметры:
// 	Форма - ФормаКлиентскогоПриложения - Форма АРМа
Процедура ЗаполнитьПериоды(Форма) Экспорт

	к2Даты.ЗаполнитьТаблицуПериодов(Форма.Периоды,
									Форма.Объект.Периодичность,
									Форма.Объект.НачалоПериода,
									Форма.Объект.ОкончаниеПериода,
									Ложь,
									Ложь,
									Ложь);

	соотПериодов = Новый Соответствие;

	структОтбора = Новый Структура("Активная", Истина);
	активныеПериоды = Форма.Периоды.НайтиСтроки(структОтбора);

	Для Каждого цСтрокаПериода Из активныеПериоды Цикл

		соотПериодов.Вставить(цСтрокаПериода.ДатаНачала, цСтрокаПериода.ИмяКолонки);

	КонецЦикла;

	Форма.СоответствиеПериодаКолонкам = Новый ФиксированноеСоответствие(соотПериодов);

	Если Форма.Объект.Периодичность = Перечисления.к2Периодичность.День Тогда

		Для Каждого цСтрокаПериода Из активныеПериоды Цикл

			цСтрокаПериода.Заголовок = Формат(цСтрокаПериода.ДатаНачала, "ДФ='dd.MM.yy (ddd)'");

		КонецЦикла;

	ИначеЕсли Форма.Объект.Периодичность = Перечисления.к2Периодичность.Неделя Тогда

		шаблонЗаголовка = "%1 - %2";

		Для Каждого цСтрокаПериода Из активныеПериоды Цикл

			цСтрокаПериода.Заголовок = СтрШаблон(шаблонЗаголовка,
												 Формат(цСтрокаПериода.ДатаНачала, "ДФ='dd.MM'"),
												 Формат(цСтрокаПериода.ДатаОкончания, "ДФ='dd.MM'"));

		КонецЦикла;

	ИначеЕсли Форма.Объект.Периодичность = Перечисления.к2Периодичность.Месяц Тогда

		Для Каждого цСтрокаПериода Из активныеПериоды Цикл

			цСтрокаПериода.Заголовок = Формат(цСтрокаПериода.ДатаНачала, "ДФ='MMMM yy ''г.'''");

		КонецЦикла;

	Иначе

		ВызватьИсключение НСтр("ru='Не поддерживаемая периодичность:'") + " " + Форма.Объект.Периодичность;

	КонецЕсли;

КонецПроцедуры

#Область СозданиеКроссТаблицы

// Создает структуру параметров для создания числового поля кросс-таблицы
// 
// Параметры:
//  пИмяПоля - Строка - Имя поля
//  пЗаголовок - Строка - Заголовок поля
//  пГруппа - ГруппаФормы, Неопределено - группа - владелец
// Возвращаемое значение:
//  Структура - структура параметров числового поля кросс-таблицы:
//   * СтруктураДействий - Структура -
//   * СвойстваЭлемента - Структура -
//   * ШиринаЭлемента - Число -
//   * СоздаватьИтоговыеРеквизиты - Булево -
//   * ТипЭлемента - Строка -
//   * СоздаватьЭлемент - Булево -
//   * УдалятьРеквизитыТаблицы - Булево -
//   * ПрефиксГруппы - Строка -
Функция ЧисловоеПолеКроссТаблицы(Знач пИмяПоля, Знач пЗаголовок, Знач пГруппа = Неопределено) Экспорт
	
	СтруктураПоля = Новый Структура;
	СтруктураПоля.Вставить("ПрефиксРеквизитаКолонки", пИмяПоля + "_");
	
	Если Не пГруппа = Неопределено Тогда
		
		СтруктураПоля.Вставить("ПрефиксГруппы", пГруппа.ПрефиксРеквизитаКолонки);
		
	КонецЕсли;
	
	СтруктураПоля.Вставить("УдалятьРеквизитыТаблицы", Ложь);
	СтруктураПоля.Вставить("СоздаватьЭлемент", Истина);
	СтруктураПоля.Вставить("ТипЭлемента", "Число");
	СтруктураПоля.Вставить("Заголовок", пЗаголовок);
	СтруктураПоля.Вставить("СоздаватьИтоговыеРеквизиты", Истина);
	СтруктураПоля.Вставить("ШиринаЭлемента", ШиринаЧисловогоПоляПоУмолчанию());
	СтруктураПоля.Вставить("СвойстваЭлемента", Новый Структура());
	СтруктураПоля.СвойстваЭлемента.Вставить("ВыделятьОтрицательные", Истина);
	СтруктураПоля.СвойстваЭлемента.Вставить("ТолькоПросмотр", Истина);
	СтруктураПоля.СвойстваЭлемента.Вставить("Формат", "ЧФ=Ч");
	СтруктураПоля.СвойстваЭлемента.Вставить("ФорматРедактирования", "ЧДЦ=2");
	СтруктураПоля.Вставить("СтруктураДействий", Новый Структура);
	
	Возврат СтруктураПоля;
	
КонецФункции

#КонецОбласти

#Область ГруппировкиДерева

// Параметры заполнения механизма группировок
// 
// Возвращаемое значение:
//  Структура - параметры заполнения:
//   * ОбязательныеГруппировки - Массив - группировки используемые всегда
//   * ИсключаемыеГруппировки - Массив - группировки, которые запрещено использовать
//   * ПереключитьОтображение - Массив из Структура - параметры работы с переключением отображения колонок в строке/подчинено:
//    ** Колонка - ПолеФормы - Элемент формы дерева, в котором выводится значение
//    ** Реквизит - Строка - имя реквизита, где хранится значение переключения
//   * КолонкаГруппировка - ПолеФормы - Элемент формы, в заголовок которого нужно установить группировку 
//   * ИмяДерева - Строка - Имя дерева, если на форме их несколько
Функция Группировки_ПараметрыЗаполнения() Экспорт
	
	структ = Новый Структура;
	структ.Вставить("ИмяДерева", "");
	структ.Вставить("КолонкаГруппировка");
	
	структ.Вставить("ПереключитьОтображение", Новый Массив);
	
	ОбязательныеГруппировки = Новый СписокЗначений;
	
	ОбязательныеГруппировки.Добавить("Номенклатура", НСтр("ru='Номенклатура'"));
	ОбязательныеГруппировки.Добавить("Характеристика", НСтр("ru='Характеристика'"));
	
	структ.Вставить("ОбязательныеГруппировки", ОбязательныеГруппировки);
	
	структ.Вставить("ИсключаемыеГруппировки", Новый Массив);
	структ.Вставить("РазрешенныеГруппировки", Новый СписокЗначений);
	
	Возврат структ;
	
КонецФункции

// Инициализирует группировки на форме
//
// Параметры:
// 	Форма - ФормаКлиентскогоПриложения - Форма АРМа
// 	ПараметрыЗаполнения - Структура - см. к2Планирование_Формы.Группировки_ПараметрыЗаполнения.
Процедура Группировки_ПриСозданииНаСервере(Форма, Знач ПараметрыЗаполнения) Экспорт
	
	имяГруппировка1 = ПараметрыЗаполнения.ИмяДерева + "Группировка1";
	имяГруппировка2 = ПараметрыЗаполнения.ИмяДерева + "Группировка2";
	
	Форма[имяГруппировка1] = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(Форма.ИмяФормы,
																			  имяГруппировка1,
																			  "");
	Форма[имяГруппировка2] = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(Форма.ИмяФормы,
																			  имяГруппировка2,
																			  "");
	
	Для каждого цСтруктураПереключения Из ПараметрыЗаполнения.ПереключитьОтображение Цикл
		
		Форма[цСтруктураПереключения.Реквизит] = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(Форма.ИмяФормы,
																								  цСтруктураПереключения.Реквизит,
																								  Ложь);
		
	КонецЦикла;
	
КонецПроцедуры

// Заполняет списки выбора полей группировки разрешенными значениями
//
// Параметры:
//  Поля				 - Массив Из ПолеФормы - поля группировок
//  ПараметрыЗаполнения - Структура - см. к2Планирование_Формы.Группировки_ПараметрыЗаполнения
//
Процедура Группировки_НастроитьПоляВыбора(Знач Поля, Знач ПараметрыЗаполнения) Экспорт
	
	Если ПараметрыЗаполнения.РазрешенныеГруппировки.Количество() = 0 Тогда
	
		разрешенныеГруппировки = РазрешенныеГруппировки(ПараметрыЗаполнения);
		
	Иначе
		
		разрешенныеГруппировки = ПараметрыЗаполнения.РазрешенныеГруппировки;
		
	КонецЕсли;
	
	Для каждого цПоле Из Поля Цикл
		
		к2ИзменениеФорм.РастягиватьПоГоризонтали_Выкл(цПоле);
		к2ИзменениеФорм.ВыборИзСписка(цПоле);
		
		цПоле.СписокВыбора.Очистить();
		
		Для Каждого цЭлемент Из разрешенныеГруппировки Цикл
			
			цПоле.СписокВыбора.Добавить(цЭлемент.Значение, цЭлемент.Представление);
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

// Настраивает форму согласно параметров группировок
// 
// Параметры:
// 	Форма - ФормаКлиентскогоПриложения - Форма АРМа
// 	ПараметрыЗаполнения - Структура - см. к2Планирование_Формы.Группировки_ПараметрыЗаполнения
Процедура Группировки_НастроитьФорму(Форма, Знач ПараметрыЗаполнения) Экспорт

	имяГруппировка1 = ПараметрыЗаполнения.ИмяДерева + "Группировка1";
	имяГруппировка2 = ПараметрыЗаполнения.ИмяДерева + "Группировка2";
	
	поля = Новый Массив;
	поля.Добавить(Форма.Элементы[имяГруппировка1]);
	поля.Добавить(Форма.Элементы[имяГруппировка2]);
	
	Группировки_НастроитьПоляВыбора(поля, ПараметрыЗаполнения);

	группировка1 = Форма[имяГруппировка1];
	группировка2 = Форма[имяГруппировка2];
	
	Для каждого цЗапрещеннаяГруппировка Из ПараметрыЗаполнения.ИсключаемыеГруппировки Цикл
		
		Если цЗапрещеннаяГруппировка = группировка1 Тогда
			
			Форма[имяГруппировка1] = "";
			группировка1 = "";
			
		КонецЕсли;
		
		Если цЗапрещеннаяГруппировка = группировка2 Тогда
			
			Форма[имяГруппировка2] = "";
			группировка2 = "";
			
		КонецЕсли;
		
	КонецЦикла;
	
	сборкаПредставления = Новый Массив;

	Если ЗначениеЗаполнено(группировка1) Тогда

		сборкаПредставления.Добавить(Форма.Элементы[имяГруппировка1].СписокВыбора.НайтиПоЗначению(группировка1).Представление);

	КонецЕсли;

	Если ЗначениеЗаполнено(группировка2) Тогда

		сборкаПредставления.Добавить(Форма.Элементы[имяГруппировка2].СписокВыбора.НайтиПоЗначению(Группировка2).Представление);

	КонецЕсли;

	Для Каждого цГруппировка Из ПараметрыЗаполнения.ОбязательныеГруппировки Цикл

		Если сборкаПредставления.Найти(цГруппировка.Представление) = Неопределено Тогда
		
			сборкаПредставления.Добавить(цГруппировка.Представление);
		
		КонецЕсли;

	КонецЦикла;

	ПараметрыЗаполнения.КолонкаГруппировка.Видимость = сборкаПредставления.Количество() > 0;
	ПараметрыЗаполнения.КолонкаГруппировка.Заголовок = СтрСоединить(сборкаПредставления, " /" + Символы.ПС);

	Для каждого цСтруктураПереключения Из ПараметрыЗаполнения.ПереключитьОтображение Цикл
		
		Если Не цСтруктураПереключения.Колонка = Неопределено Тогда
			
			цСтруктураПереключения.Колонка.Видимость = цСтруктураПереключения.ЗначениеРеквизита;
			
		КонецЕсли;
		
	КонецЦикла;

КонецПроцедуры

// Сохраняет значения реквизитов, чтобы восстановить при следующем открытии формы
// 
// Параметры:
// 	Форма - ФормаКлиентскогоПриложения - Форма АРМа
// 	ПараметрыЗаполнения - Структура - см. к2Планирование_Формы.Группировки_ПараметрыЗаполнения
Процедура Группировки_Сохранить(Форма, Знач ПараметрыЗаполнения) Экспорт
	
	имяГруппировка1 = ПараметрыЗаполнения.ИмяДерева + "Группировка1";
	имяГруппировка2 = ПараметрыЗаполнения.ИмяДерева + "Группировка2";
	
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(Форма.ИмяФормы, имяГруппировка1, Форма[имяГруппировка1]);
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(Форма.ИмяФормы, имяГруппировка2, Форма[имяГруппировка2]);
	
	Для каждого цСтруктураПереключения Из ПараметрыЗаполнения.ПереключитьОтображение Цикл
		
		ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(Форма.ИмяФормы,
														 цСтруктураПереключения.Реквизит,
														 цСтруктураПереключения.ЗначениеРеквизита);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

// Цвет фона редактируемого поля
// 
// Возвращаемое значение:
//  Цвет - цвет фона, который отображает поля, доступные для редактирования
//
Функция ФонРедактируемогоПоля() Экспорт
	
	Возврат ЦветаСтиля.к2ЦветФонаПоляРедактирования;
	
КонецФункции

// Устанавливает полю таблицы свойства редактируемого поля
// 
// Параметры:
// 	Поле - ПолеФормы - поле таблицы
//
Процедура НастроитьПоле_Редактируемое(Поле) Экспорт
	
	к2ИзменениеФорм.ЦветФона(Поле, ФонРедактируемогоПоля());
	к2ИзменениеФорм.УстановитьФормат(Поле, "ЧН=0; ЧФ=Ч");
	Поле.ЦветРамки     = WebЦвета.АкварельноСиний;
	Поле.КартинкаШапки = БиблиотекаКартинок.Изменить;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ЗаполнитьПериодВКалендаре(Форма)

	Элементы = Форма.Элементы;
	Объект = Форма.Объект;
	
	Элементы.Период_Календарь.ВыделенныеДаты.Очистить();
	Форма.Период_Календарь = Объект.НачалоПериода;
	Элементы.Период_Календарь.НачалоПериодаОтображения = НачалоМесяца(Объект.НачалоПериода);	

	Если Не ЗначениеЗаполнено(Объект.ОкончаниеПериода) Тогда

		Элементы.Период_Календарь.ВыделенныеДаты.Добавить(Объект.НачалоПериода);

	Иначе

		текДата = Объект.НачалоПериода;
		Элементы.Период_Календарь.КонецПериодаОтображения = КонецМесяца(Объект.ОкончаниеПериода);

		Пока текДата <= Объект.ОкончаниеПериода Цикл

			Элементы.Период_Календарь.ВыделенныеДаты.Добавить(текДата);

			текДата = к2Даты.СледующийДень(текДата);

		КонецЦикла;

	КонецЕсли;

КонецПроцедуры

Функция ШиринаЧисловогоПоляПоУмолчанию()
	
	Возврат 6;
	
КонецФункции

#Область ГруппировкиДерева

Функция РазрешенныеГруппировки(Знач ПараметрыЗаполнения)

	возможныеГруппировки = Новый СписокЗначений;
	возможныеГруппировки.Добавить("", НСтр("ru='<Нет>'"));
	возможныеГруппировки.Добавить("Склад", НСтр("ru='Склад'"));
	возможныеГруппировки.Добавить("ВолнаОтгрузки", НСтр("ru='Волна'"));
	возможныеГруппировки.Добавить("СезоннаяГруппа", НСтр("ru='Сезонная группа'"));
	возможныеГруппировки.Добавить("БазовыйПродукт", НСтр("ru='Базовый продукт'"));
	возможныеГруппировки.Добавить("ВидНоменклатуры", НСтр("ru='Вид номенклатуры'"));
	возможныеГруппировки.Добавить("Бренд", НСтр("ru='Бренд'"));
	возможныеГруппировки.Добавить("ГруппаНоменклатуры", НСтр("ru='Группа'"));
	возможныеГруппировки.Добавить("ТоварнаяКатегория", НСтр("ru='Товарная категория'"));

	Для каждого цЗапрещеннаяГруппировка Из ПараметрыЗаполнения.ИсключаемыеГруппировки Цикл
		
		найденныйЭлемент = возможныеГруппировки.НайтиПоЗначению(цЗапрещеннаяГруппировка);
		
		Если Не найденныйЭлемент = Неопределено Тогда
			
			возможныеГруппировки.Удалить(найденныйЭлемент);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат возможныеГруппировки;

КонецФункции

#КонецОбласти

#КонецОбласти