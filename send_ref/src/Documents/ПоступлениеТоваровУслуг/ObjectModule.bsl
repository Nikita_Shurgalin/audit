#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Устарела. Следует использовать ЗаполнитьТоварыПоПлану
// Заполняет ТЧ "Товары заявлено" по данным ТЧ "Товары план"
//
Процедура ЗаполнитьПоПлану() Экспорт
	
	ЗаполнитьТоварыПоПлану();
	
КонецПроцедуры

// Заполняет ТЧ "Товары заявлено" по данным ТЧ "ТоварыПлан"
//
Процедура ЗаполнитьТоварыЗаявленоПоПлану() Экспорт
	
	ЗаполнитьТаблицуПоПлану(ТоварыЗаявлено);
	
КонецПроцедуры

// Заполняет ТЧ "Товары" по данным ТЧ "ТоварыПлан"
//
Процедура ЗаполнитьТоварыПоПлану() Экспорт
	
	ЗаполнитьТаблицуПоПлану(Товары);
	
КонецПроцедуры

// Заполняет ТЧ "Товары" по данным ТЧ "ТоварыЗаявлено"
//
Процедура ЗаполнитьТоварыПоЗаявлено() Экспорт
	
	ЗаполнитьТаблицуПоЗаявлено(Товары);
	
КонецПроцедуры

// Заполняет аналитики учета номенклатуры в табличных частях
//
Процедура ЗаполнитьАналитикиУчетаПоНоменклатуре() Экспорт
	
	ЗаполнитьАналитикуУчетаПоНоменклатуреВТаблице(ТоварыПлан);
	ЗаполнитьАналитикуУчетаПоНоменклатуреВТаблице(ТоварыЗаявлено);
	ЗаполнитьАналитикуУчетаПоНоменклатуреВТаблице(Товары);
	
	Структура = Новый Структура("Номенклатура");
	Структура.Вставить("Характеристика", Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка());
	Структура.Вставить("Серия", Справочники.к2СерииНоменклатуры.ПустаяСсылка());
	
	Для Каждого цСтрокаУслуг Из Услуги Цикл
		
		Структура.Номенклатура = цСтрокаУслуг.Номенклатура;
		цСтрокаУслуг.АналитикаУчетаНоменклатуры
			= РегистрыСведений.к2АналитикаУчетаНоменклатуры.ПолучитьКлючАналитикиУчетаПоНоменклатуре(Структура);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ЗаполнитьПоСтруктуре(ДанныеЗаполнения);
	ЗаполнитьПоЗаказуПоставщику(ДанныеЗаполнения);
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)

	к2Документы.ПриКопировании(ЭтотОбъект);
	
	НомерНакладной = "";
	ДатаНакладной = Дата(1, 1, 1);
	ПлановаяДатаПоставки = Дата(1, 1, 1);

	Справочники.к2СоглашенияСКлиентами.ПроверитьДействиеСоглашения(ЭтотОбъект, ТекущаяДатаСеанса());

	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	
	ПроверитьЗаполнениеПоСтатусу(НепроверяемыеРеквизиты);
	
	СкладыСервер.ПроверитьЗаполнениеЯчейкиВШапке(ЭтотОбъект, НепроверяемыеРеквизиты);
	
	ПроверитьЗаполнениеПлановойДатыПоставки(НепроверяемыеРеквизиты);
	ПроверитьЗаполнениеФактическойДатыПоставки(НепроверяемыеРеквизиты);
	
	ПроверитьЗаполнениеХарактеристик("ТоварыПлан", НепроверяемыеРеквизиты, Отказ);
	ПроверитьЗаполнениеХарактеристик("ТоварыЗаявлено", НепроверяемыеРеквизиты, Отказ);
	ПроверитьЗаполнениеХарактеристик("Товары", НепроверяемыеРеквизиты, Отказ);
	
	ПроверитьЗаполнениеСерий("ТоварыЗаявлено", НепроверяемыеРеквизиты, Отказ);
	ПроверитьЗаполнениеСерий("Товары", НепроверяемыеРеквизиты, Отказ);
	
	НоменклатураСервер.ПроверитьЗаполнениеКоличество(ЭтотОбъект, "ТоварыЗаявлено", Отказ);
	НоменклатураСервер.ПроверитьЗаполнениеКоличество(ЭтотОбъект, "Товары", Отказ);
	
	ПроверитьЗаполнениеПричиныБрака(НепроверяемыеРеквизиты, Отказ);
	
	ПроверитьЗаполнениеСтрокФактаВТаблицеТары(НепроверяемыеРеквизиты, Отказ);
	
	// Финансовый результат
	суммаУслуг = Услуги.Итог("СуммаСНДС");
	распределеннаяСумма = Товары.Итог("СуммаУслуг");
	
	Если ОтнестиУслугиВСебестоимость
		И Не распределеннаяСумма = суммаУслуг Тогда
		
		текстСообщения = НСтр("ru = 'Сумма услуг (%1 %3) не равна распределенной сумме услуг по товарам (%2 %3).'");
		
		текстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			текстСообщения,
			Формат(суммаУслуг, "ЧДЦ=2; ЧН=0"),
			Формат(распределеннаяСумма, "ЧДЦ=2; ЧН=0"),
			Валюта);
		
		ОбщегоНазначения.СообщитьПользователю(
			текстСообщения,
			ЭтотОбъект,
			,
			,
			Отказ);
		
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	ДозаполнитьПередЗаписью();
	
	к2ПроведениеСервер.ПередЗаписью(ЭтотОбъект, РежимЗаписи, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	к2ПроведениеСервер.ОбработкаПроведения(ЭтотОбъект, Отказ, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	к2ПроведениеСервер.ОбработкаУдаленияПроведения(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Процедура СформироватьСписокРегистровДляКонтроля() Экспорт
	
	Массив = Новый Массив;
	Массив.Добавить(Движения.к2ТоварыНаСкладах);
	
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПроверкаЗаполнения

Процедура ПроверитьЗаполнениеПоСтатусу(НепроверяемыеРеквизиты)
	
	Если Не Статус = Перечисления.к2СтатусыПриемки.Принято Тогда
		
		НепроверяемыеРеквизиты.Добавить("ДатаНакладной");
		НепроверяемыеРеквизиты.Добавить("НомерНакладной");
		НепроверяемыеРеквизиты.Добавить("Товары");
		НепроверяемыеРеквизиты.Добавить("ТоварыЗаявлено");
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеПлановойДатыПоставки(НепроверяемыеРеквизиты)
	
	Если ТоварыПлан.Количество() = 0 Тогда
		НепроверяемыеРеквизиты.Добавить("ПлановаяДатаПоставки");
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеХарактеристик(ИмяТЧ, НепроверяемыеРеквизиты, Отказ)
	
	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияХарактеристик();
	ПараметрыПроверки.ИмяТЧ = ИмяТЧ;
	
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, НепроверяемыеРеквизиты, Отказ, ПараметрыПроверки);
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеСерий(ИмяТЧ, НепроверяемыеРеквизиты, Отказ)
	
	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияСерий();
	ПараметрыПроверки.ИмяТЧ = ИмяТЧ;
	
	НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект, НепроверяемыеРеквизиты, Отказ, ПараметрыПроверки);
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеФактическойДатыПоставки(НепроверяемыеРеквизиты)
	
	Если Статус = Перечисления.к2СтатусыПриемки.НеПринято Тогда
		НепроверяемыеРеквизиты.Добавить("ФактическаяДатаПоставки");
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеПричиныБрака(НепроверяемыеРеквизиты, Отказ)
	
	НепроверяемыеРеквизиты.Добавить("Товары.ПричинаБрака");
	
	МетаданныеТовары = Метаданные().ТабличныеЧасти.Товары;
	
	ИмяТЧ = МетаданныеТовары.Имя;
	ПредставлениеТЧ = МетаданныеТовары.Синоним;
	ИмяРеквизитаПричинаБрака = МетаданныеТовары.Реквизиты.ПричинаБрака.Имя;
	ПредставлениеРеквизитаПричинаБрака = МетаданныеТовары.Реквизиты.ПричинаБрака.Синоним;
	
	КлючДанных = к2ОбщегоНазначения.КлючДанныхДляСообщенияПользователю(ЭтотОбъект);
	
	СтруктураПоиска = Новый Структура;
	СтруктураПоиска.Вставить("ЭтоБрак", Истина);
	СтруктураПоиска.Вставить("ПричинаБрака", Справочники.к2ПричиныБрака.ПустаяСсылка());
	
	МассивСтрок = Товары.НайтиСтроки(СтруктураПоиска);
	
	ШаблонСообщения
		= НСтр("ru='Не заполнена ""%ПричинаБрака%"" для ""%Номенклатура%"" в строке %НомерСтроки% списка ""%ТаблицаТовары%"".'");
	
	Для Каждого Строка Из МассивСтрок Цикл
		
		ТекстСообщения = СтрЗаменить(ШаблонСообщения, "%ПричинаБрака%", ПредставлениеРеквизитаПричинаБрака);
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ТаблицаТовары%", ПредставлениеТЧ);
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%НомерСтроки%", Строка.НомерСтроки);
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Номенклатура%", Строка.Номенклатура);
		
		Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти(ИмяТЧ, Строка.НомерСтроки, ИмяРеквизитаПричинаБрака);
		
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, КлючДанных, Поле, "Объект", Отказ);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеСтрокФактаВТаблицеТары(НепроверяемыеРеквизиты, Отказ)
	
	НепроверяемыеРеквизиты.Добавить("Тары.КлючСтрокиТовары");
	
	Для Каждого Строка Из Тары Цикл
		
		Если Не ЗначениеЗаполнено(Строка.КлючСтрокиТовары) Тогда
			
			ШаблонСообщения = НСтр("ru = 'Не заполнена колонка ""Строка факта"" в строке ""%1"" списка ""Тары""'");
			
			ТекстСообщения = СтрШаблон(ШаблонСообщения, Строка.НомерСтроки);
			
			КлючДанных = к2ОбщегоНазначения.КлючДанныхДляСообщенияПользователю(ЭтотОбъект);
			
			ПредставлениеТЧ = Метаданные().ТабличныеЧасти.Тары.Имя;
			
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти(ПредставлениеТЧ, Строка.НомерСтроки, "СтрокаФакта");
			
			ПутьКДанным = "Объект";
			
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, КлючДанных, Поле, ПутьКДанным, Отказ);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ИнициализацияИЗаполнение

Процедура ЗаполнитьПоСтруктуре(Знач ДанныеЗаполнения)
	
	Если Не ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		Возврат;
	КонецЕсли;
	
	к2ОбщегоНазначения.ЗаполнитьОбъектПоСтруктуре(ЭтотОбъект, ДанныеЗаполнения);
	
	к2УчетСдельнойОплаты.УстановитьПравило(ЭтотОбъект, ДанныеЗаполнения);
	
КонецПроцедуры

Процедура ЗаполнитьПоЗаказуПоставщику(Знач ДанныеЗаполнения)
	
	Если Не ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ЗаказПоставщику") Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ЗаказПоставщику.Ссылка КАК Заказ,
		|	ЗаказПоставщику.Организация КАК Организация,
		|	ЗаказПоставщику.Поставщик КАК Поставщик,
		|	ЗаказПоставщику.Соглашение КАК Соглашение,
		|	ЗаказПоставщику.Холдинг КАК Холдинг,
		|	ЗаказПоставщику.Валюта КАК Валюта,
		|	ЗаказПоставщику.ВидЦен КАК ВидЦен,
		|	ЗаказПоставщику.Налогообложение КАК Налогообложение,
		|	ЗаказПоставщику.МоментОпределенияЦены КАК МоментОпределенияЦены,
		|	ЗаказПоставщику.ДатаДоставки КАК ПлановаяДатаПоставки,
		|	ЗаказПоставщику.ЦенаВключаетНДС КАК ЦенаВключаетНДС,
		|	ЗаказПоставщику.НомерПоДаннымПоставщика КАК НомерПоДаннымПоставщика,
		|	ЗаказПоставщику.ДатаПоДаннымПоставщика КАК ДатаПоДаннымПоставщика,
		|	ЗаказПоставщику.Подразделение КАК Подразделение,
		|	ЗаказПоставщику.Склад КАК Склад,
		|	ВЫБОР
		|		КОГДА ЗаказПоставщику.Склад.ИспользоватьАдресноеХранение
		|			ТОГДА ЗаказПоставщику.Склад.ЯчейкаПриемки
		|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.к2СкладскиеЯчейки.ПустаяСсылка)
		|	КОНЕЦ КАК Ячейка,
		|	ЗаказПоставщику.Товары.(
		|		Номенклатура,
		|		Характеристика,
		|		Серия,
		|		Упаковка,
		|		КоличествоУпаковок,
		|		Количество,
		|		Количество1,
		|		Количество2,
		|		Цена,
		|		Сумма,
		|		АналитикаУчетаНоменклатуры,
		|		СтавкаНДС,
		|		СуммаНДС,
		|		Вес,
		|		СуммаСНДС,
		|		ПроцентСкидкиНаценки,
		|		СуммаСкидкиНаценки,
		|		ЦенаСУчетомСкидок,
		|		Номенклатура.ТипНоменклатуры КАК ТипНоменклатуры) КАК Товары
		|ИЗ
		|	Документ.ЗаказПоставщику КАК ЗаказПоставщику
		|ГДЕ
		|	ЗаказПоставщику.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", ДанныеЗаполнения);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	выборка = РезультатЗапроса.Выбрать();
	
	Если выборка.Следующий() Тогда
		
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, выборка, , "Товары");
		
		выборкаТовары = выборка.Товары.Выбрать();
		
		Пока выборкаТовары.Следующий() Цикл
			
			Если выборкаТовары.ТипНоменклатуры = Перечисления.к2ТипыНоменклатуры.Услуга Тогда
				
				новСтрока = Услуги.Добавить();
				
			Иначе
				
				новСтрока = Товары.Добавить();
				
			КонецЕсли;
			
			ЗаполнитьЗначенияСвойств(новСтрока, выборкаТовары);
			
		КонецЦикла;
		
	КонецЕсли;
	
	ЗаполнитьТоварыПлан();
	
КонецПроцедуры

Процедура ЗаполнитьРеквизитыПоУмолчанию()

	к2Документы.ЗаполнитьРеквизитыПоУмолчанию(ЭтотОбъект);
	к2Покупки.ЗаполнитьКонтрагентаПоУмолчанию(ЭтотОбъект);

КонецПроцедуры

Процедура ЗаполнитьТаблицуПоПлану(Таблица)
	
	Таблица.Очистить();
	
	Для Каждого Строка Из ТоварыПлан Цикл
		
		НоваяСтрока = Таблица.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
		
		НоваяСтрока.КлючСтроки = Строка(Новый УникальныйИдентификатор);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьТаблицуПоЗаявлено(Таблица)
	
	Таблица.Очистить();
	
	Для Каждого Строка Из ТоварыЗаявлено Цикл
		
		НоваяСтрока = Таблица.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка, , "КлючСтроки");
		
		НоваяСтрока.КлючСтроки = Строка(Новый УникальныйИдентификатор);
		НоваяСтрока.КлючСтрокиТоварыЗаявлено = Строка.КлючСтроки;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьАналитикуУчетаПоНоменклатуреВТаблице(Таблица)
	
	Для Каждого Строка Из Таблица Цикл
		
		Структура = Новый Структура("Номенклатура,Характеристика,Серия");
		ЗаполнитьЗначенияСвойств(Структура, Строка);
		Строка.АналитикаУчетаНоменклатуры = РегистрыСведений.к2АналитикаУчетаНоменклатуры.ПолучитьКлючАналитикиУчетаПоНоменклатуре(Структура);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Процедура ДозаполнитьПередЗаписью()
	
	Если Не (Склад.ИспользоватьАдресноеХранение И Дата >= Склад.ДатаНачалаАдресногоХраненияОстатков) Тогда
		Ячейка = Неопределено;
	КонецЕсли;
	
	ЗаполнитьАналитикиУчетаПоНоменклатуре();
	параметрыДокумента = к2ДокументыКлиентСервер.ПараметрыДокумента();
	параметрыДокумента.ВернутьМногооборотнуюТару = ВернутьМногооборотнуюТару;
	параметрыДокумента.ИменаТЧ = "Товары,Услуги";
	к2Документы.ЗаполнитьИтоги(ЭтотОбъект, параметрыДокумента);
	ЗаполнитьКлючСтрокиТоваров();
	
КонецПроцедуры

Процедура ЗаполнитьТоварыПлан()
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	к2ЗаказыПоставщикамОстатки.АналитикаУчетаНоменклатуры.Номенклатура КАК Номенклатура,
		|	к2ЗаказыПоставщикамОстатки.АналитикаУчетаНоменклатуры.Характеристика КАК Характеристика,
		|	к2ЗаказыПоставщикамОстатки.АналитикаУчетаНоменклатуры.Серия КАК Серия,
		|	к2ЗаказыПоставщикамОстатки.Упаковка КАК Упаковка,
		|	к2ЗаказыПоставщикамОстатки.КоличествоОстаток КАК Количество,
		|	к2ЗаказыПоставщикамОстатки.КоличествоУпаковокОстаток КАК КоличествоУпаковок,
		|	к2ЗаказыПоставщикамОстатки.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры
		|ИЗ
		|	РегистрНакопления.к2ЗаказыПоставщикам.Остатки(&ДатаДоставки, Заказ = &Заказ
		|	И НЕ АналитикаУчетаНоменклатуры.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.к2ТипыНоменклатуры.Услуга)) КАК
		|		к2ЗаказыПоставщикамОстатки";
	
	Запрос.УстановитьПараметр("ДатаДоставки", Новый Граница(ПлановаяДатаПоставки, ВидГраницы.Включая));
	Запрос.УстановитьПараметр("Заказ", Заказ);
	
	ТоварыПлановыеОстатки = Запрос.Выполнить().Выгрузить();
	
	ТоварыПлан.Очистить();
	КэшированныеЗначения = Неопределено;
	
	Для Каждого цСтрока Из ТоварыПлановыеОстатки Цикл
		
		новСтрока = ТоварыПлан.Добавить();
		ЗаполнитьЗначенияСвойств(новСтрока, цСтрока);
		
		к2Покупки.СтрокаТоваровПриИзменении(ЭтотОбъект, новСтрока, КэшированныеЗначения);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьКлючСтрокиТоваров()
	
	Для Каждого Строка Из Товары Цикл
		
		Если ЗначениеЗаполнено(Строка.КлючСтроки) Тогда
			Продолжить;
		КонецЕсли;
		
		Строка.КлючСтроки = Строка(Новый УникальныйИдентификатор);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
