
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	к2СобытияФорм.ФормаСписка_ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);

	Список.Параметры.УстановитьЗначениеПараметра("ОсновноеПодразделение", к2НастройкиПользователей.ОсновноеПодразделение());

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)

	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)

	Если ИмяСобытия = "Запись_Подразделения" Тогда

		Элементы.Список.Обновить();

	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	Если ТипЗнч(Элементы.Список.ТекущаяСтрока) <> Тип("СтрокаГруппировкиДинамическогоСписка")
		И Не Элементы.Список.ТекущиеДанные = Неопределено Тогда

		Элементы.ФормаИспользоватьКакОсновное.Доступность = 
			Не Элементы.Список.ТекущиеДанные.ЭтоОсновноеПодразделение
			И Не Элементы.Список.ТекущиеДанные.ЭтоГруппа;
		
	КонецЕсли;
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ИспользоватьКакОсновное(Команда)
	
	Если ТипЗнч(Элементы.Список.ТекущаяСтрока) = Тип("СтрокаГруппировкиДинамическогоСписка") Тогда
		
		Возврат;
		
	КонецЕсли;
	
	текДанные = Элементы.Список.ТекущиеДанные;
	
	Если текДанные = Неопределено
		ИЛИ текДанные.ЭтоОсновноеПодразделение
		ИЛИ текДанные.ЭтоГруппа Тогда
		
		Возврат;
		
	КонецЕсли;
	
	УстановитьОсновноеПодразделение(текДанные.Ссылка);
	Элементы.ФормаИспользоватьКакОсновное.Доступность = Ложь;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Элементы.Список);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Элементы.Список);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

&НаСервере
Процедура УстановитьОсновноеПодразделение(Знач НовоеОсновноеПодразделение)
	
	к2НастройкиПользователей.УстановитьНовоеПодразделение(НовоеОсновноеПодразделение);
	Список.Параметры.УстановитьЗначениеПараметра("ОсновноеПодразделение", к2НастройкиПользователей.ОсновноеПодразделение());
	
КонецПроцедуры

#КонецОбласти