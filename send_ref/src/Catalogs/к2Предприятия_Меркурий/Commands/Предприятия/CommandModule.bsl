
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	структОтбор = Новый Структура( "ХозяйствующийСубъект", ПараметрКоманды );
	
	ПараметрыФормы = Новый Структура("Отбор", структОтбор);
	ОткрытьФорму("Справочник.к2Предприятия_Меркурий.ФормаСписка",
					ПараметрыФормы,
					ПараметрыВыполненияКоманды.Источник,
					ПараметрыВыполненияКоманды.Уникальность,
					ПараметрыВыполненияКоманды.Окно,
					ПараметрыВыполненияКоманды.НавигационнаяСсылка);
	
КонецПроцедуры

#КонецОбласти
