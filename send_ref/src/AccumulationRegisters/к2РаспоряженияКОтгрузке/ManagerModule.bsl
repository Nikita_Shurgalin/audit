#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.Таблица_к2РаспоряженияКОтгрузке;
	
	Если Отказ Или Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.к2РаспоряженияКОтгрузке.Записывать = Истина;
	Движения.к2РаспоряженияКОтгрузке.Загрузить(Таблица);
	
КонецПроцедуры

#Область КонтрольДвиженийДокументовПоРегистрам

Процедура КонтрольОтрицательныхОстатков(ДанныеТаблиц, МассивКонтролей, ТекстЗапроса) Экспорт
	
	Если к2Продажи.КонтролироватьОстаткиПлановОтгрузки()
		И к2ПроведениеСервер.ЕстьИзмененияВТаблице(ДанныеТаблиц, "к2ДвиженияРаспоряженияКОтгрузкеИзменение") Тогда
		
		МассивКонтролей.Добавить(ВРег("к2РаспоряженияКОтгрузке"));
		
		ТекстЗапроса = ТекстЗапроса
		+
		"
		|"
		+
		"ВЫБРАТЬ
		|	к2РаспоряженияКОтгрузкеОстатки.Склад КАК Склад,
		|	к2РаспоряженияКОтгрузкеОстатки.Распоряжение КАК Распоряжение,
		|	СУММА(к2РаспоряженияКОтгрузкеОстатки.КоличествоВесОстаток) КАК КоличествоВесОстаток,
		|	СУММА(к2РаспоряженияКОтгрузкеОстатки.КоличествоМестОстаток) КАК КоличествоМестОстаток
		|ИЗ
		|	РегистрНакопления.к2РаспоряженияКОтгрузке.Остатки(
		|			,
		|			(Склад, Распоряжение) В
		|				(ВЫБРАТЬ
		|					Таблица.Склад,
		|					Таблица.Распоряжение
		|				ИЗ
		|					к2ДвиженияРаспоряженияКОтгрузкеИзменение КАК Таблица)) КАК к2РаспоряженияКОтгрузкеОстатки
		|
		|СГРУППИРОВАТЬ ПО
		|	к2РаспоряженияКОтгрузкеОстатки.Склад,
		|	к2РаспоряженияКОтгрузкеОстатки.Распоряжение
		|
		|ИМЕЮЩИЕ
		|	(СУММА(к2РаспоряженияКОтгрузкеОстатки.КоличествоВесОстаток) < 0
		|		ИЛИ СУММА(к2РаспоряженияКОтгрузкеОстатки.КоличествоМестОстаток) < 0)"
		+
		";
		|
		|////////////////////////////////////////////////////////////////////////////////
		|";
		
	КонецЕсли;
	
КонецПроцедуры

Процедура СообщитьОбОшибкахПроведенияПоРегистру(Объект, Отказ, РезультатЗапроса) Экспорт
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		СообщитьОбОшибке(Выборка, Объект, Отказ);
	КонецЦикла;
	
КонецПроцедуры

Функция СообщитьОбОшибке(Выборка, Объект = Неопределено, Отказ = Ложь, ВыводитьСообщение = Истина) Экспорт
	
	ТекстСообщения = НСтр("ru = 'Для Плана отгрузки ""%ПланОтгрузки%""
								|Превышен остаток на складе %Склад% на %Количество%.'");
	
	ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ПланОтгрузки%", Выборка.Распоряжение);
	ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Склад%", Выборка.Склад);
	ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Количество%", СформироватьСтрокуКоличество(Выборка));
	
	Если ВыводитьСообщение Тогда
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, Объект, , , Отказ);
	КонецЕсли;
	
	Возврат ТекстСообщения;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция СформироватьСтрокуКоличество(Выборка)
	
	ШаблонСтрокиКоличество = "Вес: %1 %2; Мест: %3 %4";
	
	КоличествоВес = ?(Выборка.КоличествоВесОстаток > 0, Выборка.КоличествоВесОстаток, -Выборка.КоличествоВесОстаток);
	КоличествоМест = ?(Выборка.КоличествоМестОстаток > 0, Выборка.КоличествоМестОстаток, -Выборка.КоличествоМестОстаток);
	
	ЕдиницаИзмеренияВес = НоменклатураСервер.ЕдиницаИзмеренияПоУмолчанию("Вес");
	ЕдиницаИзмеренияОбъем = НоменклатураСервер.ЕдиницаИзмеренияПоУмолчанию("Объем");
	
	СтрокаКоличество = СтрШаблон(ШаблонСтрокиКоличество, КоличествоВес, ЕдиницаИзмеренияВес, КоличествоМест, ЕдиницаИзмеренияОбъем);
	
	Возврат СтрокаКоличество;
	
КонецФункции

#КонецОбласти

#КонецЕсли
