
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере( Отказ, СтандартнаяОбработка )
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	СоздатьПанельОтборов();

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПанельОтборов

&НаСервере
Процедура СоздатьПанельОтборов()
	
	параметрыПанели = к2Отборы.ПараметрыПанелиОтборов();
	параметрыПанели.Кнопка_НастроитьСписок           = Элементы.НастройкаСписка;
	параметрыПанели.Группа_ПользовательскиеНастройки = Элементы.СписокКомпоновщикНастроекПользовательскиеНастройки;
	параметрыПанели.Метаданные = Метаданные.РегистрыСведений.к2НастройкиПроверокДокументов;
	
	ОтборТипДокумента = к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "ТипДокумента");
	
	к2Отборы.СоздатьПанельОтборов(ЭтотОбъект, параметрыПанели);
	
	к2ПроверкиДокументов.УстановитьСписокВыбораДляТиповДокументов(ОтборТипДокумента.Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбработкаПанелиОтборов(Элемент, СтандартнаяОбработка = Истина)
	
	к2ОтборыКлиент.ОбработкаПанелиОтборов(ЭтотОбъект, Элемент, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_УстановитьОтбор(Знач пИмяЭлемента, Знач пСохранять = Истина) Экспорт
		
	к2Отборы.УстановитьОтборСписку(ЭтотОбъект, пИмяЭлемента, пСохранять);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти