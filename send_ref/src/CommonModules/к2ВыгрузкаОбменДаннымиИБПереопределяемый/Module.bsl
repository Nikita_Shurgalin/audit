
#Область СлужебныйПрограммныйИнтерфейс

#Область Справочник_к2ПравилаВыгрузкиОбменДаннымиИБ

#Область МодульОбъекта_Справочник_к2ПравилаВыгрузкиОбменДаннымиИБ

Процедура ОбработкаПроверкиЗаполнения(ПроверяемыйОбъект, Отказ, ПроверяемыеРеквизиты) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.ОбработкаПроверкиЗаполнения(ПроверяемыйОбъект, Отказ, ПроверяемыеРеквизиты);
	
КонецПроцедуры

#КонецОбласти

#Область МодульМенеджера_Справочник_к2ПравилаВыгрузкиОбменДаннымиИБ

// Объект ИБ соответствует условию обмена.
// 
// Параметры:
//  ОбъектИБ - ЛюбаяСсылка - Ссылка на объект данных ИБ
//  Правило - СправочникСсылка.к2ПравилаВыгрузкиОбменДаннымиИБ -
//  РезультатПроверки - см. Справочники.к2ПравилаВыгрузкиОбменДаннымиИБ.СтруктураРезультатаПроверкиПоУмолчанию
Процедура ОбъектСоответствуетУсловиюОбмена(Знач ОбъектИБ, Знач Правило, РезультатПроверки) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.ОбъектСоответствуетУсловиюОбмена(ОбъектИБ, Правило, РезультатПроверки);
	
КонецПроцедуры

#КонецОбласти

#Область ФормаЭлемента_Справочник_к2ПравилаВыгрузкиОбменДаннымиИБ

Процедура ОчиститьФорму(Форма) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.ОчиститьФорму(Форма);
	
КонецПроцедуры

Процедура УстановитьВидимостьДоступность(Форма) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.УстановитьВидимостьДоступность(Форма);
	
КонецПроцедуры

#КонецОбласти

#Область ФормаВыбораТипПакета_Справочник_к2ПравилаВыгрузкиОбменДаннымиИБ

Процедура МассивИменТиповПакетаДополнить(МассивИменТипов) Экспорт
	
	Для Каждого ТипОбъекта Из Метаданные.ОпределяемыеТипы.к2ВыгружаемыеОбъектыОбменДаннымиИБ.Тип.Типы() Цикл
		
		Ссылка = Новый (ТипОбъекта);
		ПолноеИмя = ОбщегоНазначения.ИмяТаблицыПоСсылке(Ссылка);
		
		МассивИменТипов.Добавить(ПолноеИмя);
		
	КонецЦикла;
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.МассивИменТиповПакетаДополнить(МассивИменТипов);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область Обработка_к2ВыгрузкаОбменДаннымиИБ

#Область МодульОбъекта_Обработка_к2ВыгрузкаОбменДаннымиИБ

// Данные запросов выгружаемых объектов.
// 
// Параметры:
//  МассивДанныхЗапросов - Массив из см. Обработки.к2ВыгрузкаОбменДаннымиИБ.ДанныеЗапросаВыгружаемыхОбъектов - данные запросов
Процедура ДанныеЗапросовВыгружаемыхОбъектовДополнить(МассивДанныхЗапросов) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.ДанныеЗапросовВыгружаемыхОбъектовДополнить(МассивДанныхЗапросов);
	
КонецПроцедуры

// Соответствие значений, которые при сериализации подменяются на особые.
// Например, единицы измерения кг, которые не стоит создавать в базе приемника по GUID.
// Подмененные значения уже сериализованы
// 
// Параметры:
//  СоответствиеПодмены - Соответствие из Структура - Соответствие подменяемых ссылок
Процедура СоответствиеПодменяемыхСсылокДополнить(СоответствиеПодмены) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.СоответствиеПодменяемыхСсылокДополнить(СоответствиеПодмены);
	
КонецПроцедуры

// Соответствие дополнительных свойств объекта.
// Например, штрихкоды номенклатуры или вычисляемые реквизиты.
// Дополнительные значения уже сериализованы.
// 
// Параметры:
//  СоответствиеСвойств - Соответствие из КлючИЗначение -
//   *Ключ - ЛюбаяСсылка - Объект ИБ
//   *Значение - Структура - структура дополнительных свойств, удовлетворяет требованиям сериализации JSON
//  МассивОбъектов - Массив из ЛюбаяСсылка -
//  ТипПакета - Строка -
//  СоответствиеПодмены - Соответствие -
Процедура СоответствиеДополнительныхСвойствОбъектаДополнить(СоответствиеСвойств, МассивОбъектов, ТипПакета, СоответствиеПодмены) Экспорт
	
	к2ВыгрузкаЕ4ФОбменДаннымиИБ.СоответствиеДополнительныхСвойствОбъектаДополнить(СоответствиеСвойств
		, МассивОбъектов
		, ТипПакета
		, СоответствиеПодмены);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецОбласти
