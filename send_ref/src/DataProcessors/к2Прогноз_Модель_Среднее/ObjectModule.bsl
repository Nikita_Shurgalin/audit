#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Установка гиперпараметров
//
// Параметры:
//  пКоличествоПоследних - Число - количество последних элементов истории, принимаемых для построения модели.
//
Процедура Инициализировать( Знач пКоличествоПоследних = 0 ) Экспорт
	
	КоличествоПоследних = пКоличествоПоследних;
	
	СреднееЗначение = 0;
	
КонецПроцедуры

// Строит модель по переданой истории
//
// Параметры:
//  пТаблицаИстории	 - ТаблицаЗначений - история для построения модели:
//  * Период - Дата - период истории
//  * Количество - Число - продажи в этот период.
//
Процедура Построить( Знач пТаблицаИстории ) Экспорт
	
	Если пТаблицаИстории.Количество() = 0 Тогда
		
		СреднееЗначение = 0;
		
	ИначеЕсли КоличествоПоследних = 0
		ИЛИ пТаблицаИстории.Количество() <= КоличествоПоследних Тогда
		
		СреднееЗначение = пТаблицаИстории.Итог( "Количество" )/пТаблицаИстории.Количество();
		
	Иначе
		
		тз = к2Коллекции.Последние( пТаблицаИстории, КоличествоПоследних );
		
		СреднееЗначение = тз.Итог( "Количество" )/тз.Количество();
		
	КонецЕсли;
	
КонецПроцедуры

// Подставляет в таблицу прогноза значения прогноза согласно текущей модели.
// Если нужно выполнить прогноз на 3 периода, то в переданной строке должно быть 3 строки.
//
// Параметры:
//  пТаблицаПрогноза	 - ТаблицаЗначений - таблица прогноза:
//  * Период - Дата - период истории
//  * Прогноз - Число - будет заполнен согласно модели.
//
Процедура ВыполнитьПрогноз( пТаблицаПрогноза ) Экспорт
	
	Для каждого цСтрока Из пТаблицаПрогноза Цикл
		
		цСтрока.Прогноз = СреднееЗначение;
		
	КонецЦикла;
	
КонецПроцедуры

// Возвращает параметры модели для сохранения
//
// Возвращаемое значение:
//  Структура - параметры модели
//
Функция ПолучитьПараметры() Экспорт
	
	структ = Новый Структура;
	структ.Вставить( "СреднееЗначение", СреднееЗначение );
	структ.Вставить( "КоличествоПоследних", КоличествоПоследних );
	
	Возврат структ;
	
КонецФункции

// Инициализирует модель по переданным параметрам
//
// Параметры:
//  пПараметрыМодели - Структура - параметры модели, полученные методом ПолучитьПараметры().
//

Процедура УстановитьПараметры( Знач пПараметрыМодели ) Экспорт
	
	СреднееЗначение     = пПараметрыМодели.СреднееЗначение;
	КоличествоПоследних = пПараметрыМодели.КоличествоПоследних;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
