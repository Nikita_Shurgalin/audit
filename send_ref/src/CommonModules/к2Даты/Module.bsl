
#Область ПрограммныйИнтерфейс

// Возвращает дату, смещенную на указанное количество периодов
//
// Параметры:
//	Дата - Дата - исходная дата
//	Периодичность - ПеречислениеСсылка.к2Периодичность, Строка - шаг периода
//	ЧислоПериодов - Число - число периодов, на которые нужно сдвинуть исходную дату
//  НеделяДекадаСПервогоДня - Булево - указывает, будет ли для периодичности "Неделя" и "Декада"
// 		расчетная дата смещаться на начало недели или декады.
//
// Возвращаемое значение:
//	Дата - дата, смещенная на указанное количество периодов.
//
Функция Добавить(Дата, Периодичность, ЧислоПериодов, НеделяДекадаСПервогоДня = Ложь) Экспорт
	
	Возврат к2ДатыПовтИсп.Добавить(Дата, Периодичность, ЧислоПериодов, НеделяДекадаСПервогоДня);
	
КонецФункции

// Получение разности между двумя датами
//
// Параметры:
//  ДатаНачала     - Дата - Дата начала периода.
//  ДатаОкончания  - Дата - Дата окончания периода.
//  Периодичность  - ПеречислениеСсылка.к2Периодичность, Строка - Периодичность, в которой нужно получить разность.
// 
// Возвращаемое значение:
//  Число - разность между двумя датами.
//
Функция Разность(ДатаНачала, ДатаОкончания, Периодичность) Экспорт

	Возврат к2ДатыПовтИсп.Разность(ДатаНачала, ДатаОкончания, Периодичность);

КонецФункции

// Рассчитывает дату начала периода по указанной дате и периодичности
//
// Параметры:
//  Дата - Дата - - дата, к которой будет рассчитана дата начала периода
//  Периодичность - ПеречислениеСсылка.к2Периодичность, Строка - значение перечисления "Периодичность".
//
// Возвращаемое значение:
//  Дата - Дата начала периода
//
Функция НачалоПериода(Знач Дата, Знач Периодичность) Экспорт
	
	Возврат к2ДатыПовтИсп.НачалоПериода(Дата, Периодичность);
	
КонецФункции

// Рассчитывает дату окончания периода по указанной дате и периодичности
//
// Параметры:
//  Дата - Дата - - дата, к которой будет рассчитана дата окончания периода
//  Периодичность - ПеречислениеСсылка.к2Периодичность, Строка - значение перечисления "Периодичность".
//
// Возвращаемое значение:
//  Дата - Дата окончания периода
//
Функция КонецПериода(Знач Дата, Знач Периодичность) Экспорт
	
	Возврат к2ДатыПовтИсп.КонецПериода(Дата, Периодичность);
	
КонецФункции

// Возвращает окончание периода без приведения к границам.
//
// Параметры:
//	НачалоИнтервала - Дата - Дата начала интервала
//	Периодичность - ПеречислениеСсылка.к2Периодичность, Строка - шаг периода
//
// Возвращаемое значение:
//	Дата - Конец интервала. Например, ОкончаниеИнтервала(10.11.2020 (Вторник), Неделя) => 16.11.2020 23:59:59 (Понедельник)
//
Функция ОкончаниеИнтервала(Знач НачалоИнтервала, Знач Периодичность) Экспорт
	
	Возврат к2ДатыПовтИсп.ОкончаниеИнтервала(НачалоИнтервала, Периодичность);
	
КонецФункции

// Входит ли дата в заданный период
// 
// Параметры:
// 	Дата - Дата - Проверяемая дата
// 	Период - СтандартныйПериод - Период, вхождение в который проверяется
// Возвращаемое значение:
// 	Булево - Дата входит в заданный период
Функция ВходитВПериод(Знач Дата, Знач Период) Экспорт
	
	Возврат ВходитВИнтервал(Дата, Период.ДатаНачала, Период.ДатаОкончания);
	
КонецФункции

// Входит ли дата в заданный интервал
// 
// Параметры:
// 	Дата - Дата - Проверяемая дата
// 	НачалоИнтервала - Дата - начало интервала
// 	ОкончаниеИнтервала - Дата - окончание интервала
// Возвращаемое значение:
// 	Булево - Дата входит в заданный период
Функция ВходитВИнтервал(Знач Дата, Знач НачалоИнтервала, Знач ОкончаниеИнтервала) Экспорт
	
	Если Не ЗначениеЗаполнено(ОкончаниеИнтервала) Тогда
		
		ОкончаниеИнтервала = Дата(3999, 12, 31);
		
	КонецЕсли;
	
	Возврат НачалоИнтервала <= Дата И Дата <= ОкончаниеИнтервала;
	
КонецФункции

// Находит период в который попадает дата
//
// Параметры:
//  Дата			 - Дата - Дата для поиска
//  НачалоПериода	 - Дата - начало периода для определения смещения
//  Периодичность 	 - ПеречислениеСсылка.к2Периодичность, Строка - Периодичность
// 
// Возвращаемое значение:
//  Структура - период, в который попадает дата
//    * ДатаНачала - Начало найденного периода
//    * ДатаОкончания - Окончание найденного периода
//
Функция НайтиСмещенныйПериод(Знач Дата, Знач НачалоПериода, Знач Периодичность) Экспорт
	
	Возврат к2ДатыПовтИсп.НайтиСмещенныйПериод(Дата, НачалоПериода, Периодичность);

КонецФункции

// Основной календарь
// 
// Возвращаемое значение:
//  СправочникСсылка.Календари - Календарь из константы
//
Функция ОсновнойКалендарь() Экспорт

	Возврат к2ДатыПовтИсп.ОсновнойКалендарь();

КонецФункции

// Добавить к дате требуемое время согласно графику работы
//
// Параметры:
//  пДата					 - Дата - начальная дата
//  пВремя					 - Число - добавляемое время. Может быть отрицательным для вычитания.
//  пЕдиницаИзмеренияВремени - ПеречислениеСсылка.к2ЕдиницыИзмеренияДлительностиПроизводства - ед.изм. времени.
//  пГрафикРаботы			 - СправочникСсылка.Календари - график работы
// 
// Возвращаемое значение:
//  Дата - результат вычисления даты
//
Функция ДобавитьКДатеПоГрафикуРаботы(Знач пДата, Знач пВремя, Знач пЕдиницаИзмеренияВремени, Знач пГрафикРаботы) Экспорт
	
	Возврат к2ДатыПовтИсп.ДобавитьКДатеПоГрафикуРаботы(пДата, пВремя, пЕдиницаИзмеренияВремени, пГрафикРаботы);
	
КонецФункции

// Формирует представление периода планирование, заданного датами начала и окончания периода.
//
// Параметры:
//  пНачалоИнтервала - Дата - Дата начала интервала планирования
//  пКонецИнтервала	 - Дата - Дата окончания интервала планирования.
//
// Возвращаемое значение:
//  Строка - Представление периода.
//
Функция ПредставлениеИнтервала(Знач пНачалоИнтервала, Знач пКонецИнтервала) Экспорт
	
	Возврат к2ДатыПовтИсп.ПредставлениеИнтервала(пНачалоИнтервала, пКонецИнтервала);
	
КонецФункции

// Устарела. См. к2Даты.ПредставлениеИнтервала
// Формирует представление периода планирование, заданного датами начала и окончания периода.
//
// Параметры:
//  пНачалоИнтервала - Дата - Дата начала интервала планирования
//  пКонецИнтервала	 - Дата - Дата окончания интервала планирования.
//
// Возвращаемое значение:
//  Строка - Представление периода.
//
Функция ПредставлениеИнтервалаПланирования(Знач пНачалоИнтервала, Знач пКонецИнтервала) Экспорт
	
	Возврат ПредставлениеИнтервала(пНачалоИнтервала, пКонецИнтервала);
	
КонецФункции

// Возвращает следующий день
// 
// Параметры:
//  пДата - Дата - дата
// Возвращаемое значение:
//  Дата - начало следующего дня
Функция СледующийДень(Знач пДата) Экспорт
	
	Возврат НачалоДня(пДата + к2ИК.СекундВДне());
	
КонецФункции

// Создает таблицу периодов
//
// Параметры:
//  Периодичность  - ПеречислениеСсылка.к2Периодичность - Периодичность с которой нужно заполнить таблицу
//  ДатаНачала     - Дата - Дата начала периода
//  ДатаОкончания  - Дата - Дата окончания периода
// 
// Возвращаемое значение:
//  ТаблицаЗначений - Таблица с колонками периодов.
//
Функция Периоды(Знач Периодичность, Знач ДатаНачала, Знач ДатаОкончания) Экспорт
	
	тзПериоды = ПустаяТаблицаПериодов();
	
	ЗаполнитьТаблицуПериодов(тзПериоды, Периодичность, ДатаНачала, ДатаОкончания, Ложь, Ложь, Ложь);
	
	Возврат тзПериоды;
	
КонецФункции

// Функция создает таблицу значений для заполнения периодами плана
//
// Возвращаемое значение:
//   ТаблицаЗначений - Таблица с колонками периодов.
//
Функция ПустаяТаблицаПериодов() Экспорт
	
	ОписаниеТиповЧ = к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(10, 0); 
	ОписаниеТиповБулево = Новый ОписаниеТипов("Булево");
	ОписаниеТиповСтрока = Новый ОписаниеТипов("Строка");
	ОписаниеТиповДата = к2ОбщегоНазначения.ПолучитьОписаниеТиповДаты(ЧастиДаты.ДатаВремя);
	
	ТаблицаПериодов = Новый ТаблицаЗначений;
	ТаблицаПериодов.Колонки.Добавить("НомерКолонки",  ОписаниеТиповЧ);
	ТаблицаПериодов.Колонки.Добавить("ИмяКолонки",    ОписаниеТиповСтрока);
	ТаблицаПериодов.Колонки.Добавить("Активная",      ОписаниеТиповБулево);
	ТаблицаПериодов.Колонки.Добавить("ДатаНачала",    ОписаниеТиповДата);
	ТаблицаПериодов.Колонки.Добавить("ДатаОкончания", ОписаниеТиповДата);
	ТаблицаПериодов.Колонки.Добавить("Заголовок",     ОписаниеТиповСтрока);
	
	Возврат ТаблицаПериодов;

КонецФункции

// Процедура заполняет таблицу периодов, последовательно датам с заданной периодичностью, для получения полей и их заголовков.
//
// Параметры:
//  ТаблицаПериоды - ТаблицаЗначений - Таблица, которую заполняем отрезками заданной периодичности
//  Периодичность  - ПеречислениеСсылка.к2Периодичность, Строка - Периодичность с которой нужно заполнить таблицу
//  ДатаНачала     - Дата - Дата начала периода
//  ДатаОкончания  - Дата - Дата окончания периода
//  ОтображатьНомерПериода  - Булево - Флаг отображения заголовка по номеру периода в пределах года.
//  пИмяКолонкиЧерезУИ  - Булево - Старый способ формирования имени колонки для обратной совместимости.
//  пПриводитьКГраницам  - Булево - Приводить ли период к его началу.
//
Процедура ЗаполнитьТаблицуПериодов(ТаблицаПериоды,
							  Знач Периодичность,
							  Знач ДатаНачала,
							  Знач ДатаОкончания,
							  Знач ОтображатьНомерПериода = Ложь,
							  Знач пИмяКолонкиЧерезУИ = Истина,
							  Знач пПриводитьКГраницам = Истина) Экспорт
	
	Для каждого СтрокаПериода Из ТаблицаПериоды Цикл
		
		СтрокаПериода.Активная     = Ложь;
		СтрокаПериода.НомерКолонки = -1;
		
	КонецЦикла;
	
	Если НЕ ЗначениеЗаполнено(Периодичность)
		ИЛИ НЕ ЗначениеЗаполнено(ДатаНачала)
		И НЕ ЗначениеЗаполнено(ДатаОкончания) Тогда
		
		Возврат;
	
	КонецЕсли;
	
	Если пПриводитьКГраницам Тогда
		
		ДобавлениеДатаНачала    = НачалоПериода(ДатаНачала, Периодичность);
		ДобавлениеДатаОкончания = КонецПериода(ДатаНачала, Периодичность);
		
	Иначе
		
		ДобавлениеДатаНачала = ДатаНачала;
		ДобавлениеДатаОкончания = Добавить(ДобавлениеДатаНачала, Периодичность, 1, Ложь) - 1;
		
	КонецЕсли;
	
	Если Периодичность = Перечисления.к2Периодичность.Час Тогда
		
		форматДаты = "ДФ=yyMMdd_HH";
		
	Иначе
		
		форматДаты = "ДФ=yyMMdd";
		
	КонецЕсли;
	
	ТекущийПериод = 1;
	
	Пока ДобавлениеДатаНачала < КонецДня(ДатаОкончания) Цикл
		
		НайденныеСтроки = ТаблицаПериоды.НайтиСтроки(Новый Структура("ДатаНачала, ДатаОкончания",
													 ДобавлениеДатаНачала,
													 ДобавлениеДатаОкончания));
													 
		Если НайденныеСтроки.Количество() = 0 Тогда
			
			НоваяСтрока = ТаблицаПериоды.Добавить();
			
			Если пИмяКолонкиЧерезУИ Тогда
				
				НоваяСтрока.ИмяКолонки = СтрЗаменить(Строка(Новый УникальныйИдентификатор), "-", "_");
				
			Иначе
				
				НоваяСтрока.ИмяКолонки = Формат(ДобавлениеДатаНачала, форматДаты);
				
			КонецЕсли;
		
		Иначе
			
			НоваяСтрока = НайденныеСтроки[0];
			
		КонецЕсли;
		
		НоваяСтрока.НомерКолонки  = ТекущийПериод;
		НоваяСтрока.Активная      = Истина;
		НоваяСтрока.ДатаНачала    = ДобавлениеДатаНачала;
		НоваяСтрока.ДатаОкончания = ДобавлениеДатаОкончания;
		НоваяСтрока.Заголовок     = к2ДатыКлиентСервер.СформироватьЗаголовокПериода(Периодичность,
																					ДобавлениеДатаНачала,
																					ДобавлениеДатаОкончания,
																					ОтображатьНомерПериода);
		
		ТекущийПериод = ТекущийПериод + 1;
		
		Если пПриводитьКГраницам Тогда
		
			ДобавлениеДатаНачала    = НачалоПериода(ДобавлениеДатаОкончания + 1, Периодичность);
			ДобавлениеДатаОкончания = КонецПериода(ДобавлениеДатаНачала, Периодичность);
		
		Иначе
		
			ДобавлениеДатаНачала = ДобавлениеДатаОкончания + 1;
			ДобавлениеДатаОкончания = Добавить(ДобавлениеДатаНачала, Периодичность, 1, Ложь) - 1;
		
		КонецЕсли;
		
	КонецЦикла;
	
	ТаблицаПериоды.Сортировать("НомерКолонки");
	
КонецПроцедуры

#КонецОбласти