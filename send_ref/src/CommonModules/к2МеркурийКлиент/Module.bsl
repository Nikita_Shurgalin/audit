
#Область ПрограммныйИнтерфейс

// Запрашивает результаты запросов и разбирает их, если пришли
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пМассивЗаявок - Массив - массив заявок или партий
//  пИнициатор - СправочникСсылка.к2Пользователи_Меркурий - Пользователь 
//
Процедура ОпроситьМеркурийОЗаявках( Знач пИсточник, Знач пМассивЗаявок, Знач пИнициатор ) Экспорт
	
	Если к2МеркурийКлиентСерверПовтИсп.ЗапретЗапросовСКлиента() Тогда
		
		Возврат;
	
	КонецЕсли;
	
	параметрыПодключения = к2МеркурийКлиентСерверПовтИсп.ПараметрыПодключенияПользователя( пИнициатор );
	
	Для каждого цЗаявка Из пМассивЗаявок Цикл
		
		к2ЗаявкиКлиентСервер_Меркурий.ЗапроситьРезультатВыполненияПоЗаявкам( пИсточник, параметрыПодключения, цЗаявка );
		к2ЗапросыКлиентСервер_Меркурий.ПолучитьРезультатыОбработкиЗаявок( пИсточник, параметрыПодключения, цЗаявка );
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#Область ОбработчикиСобытийФормы

// Обработчик события "ОбработкаОповещения" любой формы.
//
// Параметры:
//  Форма            - УправляемаяФорма - форма, в которой инициировано событие;
//  Объект           - Объект - Объект
//  ИмяСобытия       - Строка - имя события;
//  Параметр         - Произвольный - параметр;
//  Источник         - Произвольный - источник.
//
Процедура ОбработкаОповещения( Форма, Объект, ИмяСобытия, Параметр, Источник ) Экспорт
	
	Если ИмяСобытия = "ОбъектОбновленИзГИСМеркурий"
		И ( Объект.Ссылка = Параметр
		ИЛИ ТипЗнч( Параметр ) = Тип( "СправочникСсылка.к2Заявки_Меркурий" ) ) Тогда
		
		ПеречитатьФорму( Форма );
		
		// АПК:280-выкл
		Попытка
			Форма.УстановитьВидимостьСинхронизации();
		Исключение
			// В некоторых формах может не быть элементов для синхронизации, но их все равно нужно обновлять.
		КонецПопытки;
		// АПК:280-вкл
		
		Если ТипЗнч( Объект.Ссылка ) = Тип( "СправочникСсылка.к2Заявки_Меркурий" ) Тогда
			
			ОповеститьОбИзменении( Тип( "ДокументСсылка.к2ВСД_Меркурий" ) );
			ОповеститьОбИзменении( Тип( "СправочникСсылка.к2ЗаписиЖурналаПродукции_Меркурий" ) );
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

// Выполняет переход по http-ссылке
//
// Параметры:
//  HttpСсылка  - Строка - http-ссылка на сайт.
//
Процедура ВыполнитьПереходПоНавигационнойСсылке( Знач HttpСсылка ) Экспорт
	
	Если Не ЗначениеЗаполнено( HttpСсылка ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	#Если ТолстыйКлиентОбычноеПриложение Тогда
	
	НачатьЗапускПриложения( Новый ОписаниеОповещения( "ВыполнитьПереходПоНавигационнойСсылкеЗавершение",
							к2МеркурийКлиент ),
						HttpСсылка );
	
	#Иначе
	
	ПерейтиПоНавигационнойСсылке( HttpСсылка );
	
	#КонецЕсли
	
КонецПроцедуры

// НачатьЗапускПриложения требует описание оповещения, даже когда обработка не требуется.
//
// Параметры:
//  КодВозврата				 - Число - код возврата
//  ДополнительныеПараметры	 - Структура - дополнительные параметры
//
Процедура ВыполнитьПереходПоНавигационнойСсылкеЗавершение( КодВозврата, ДополнительныеПараметры ) Экспорт
	
	Возврат; // обработка результата запуска не требуется
	
КонецПроцедуры

// Открывает форму аннулирования партий для переданных объектов
//
// Параметры:
//  пМассивВСД - СправочникСсылка, ДокументСсылка, Массив из СправочникСсылка, ДокументСсылка - массив объектов для аннулирования
//
Процедура Аннулировать(Знач пМассивВСД) Экспорт
	
	Если ТипЗнч(пМассивВСД) = Тип("Массив") Тогда
		
		массивДокументов = пМассивВСД;
		
	Иначе
		
		массивДокументов = Новый Массив;
		массивДокументов.Добавить(пМассивВСД);
		
	КонецЕсли;
	
	параметрыОткрытия = Новый Структура("МассивВсдДляАннулирования", массивДокументов);
	
	ОткрытьФорму("Документ.к2АннулированиеВСД_Меркурий.Форма.ПакетноеАннулированиеВСД", параметрыОткрытия);
	
КонецПроцедуры

// Открывает форму пакетного гашения ВСД по переданным данным
//
// Параметры:
//  ДанныеЗаполнения - ДокументСсылка.к2ИсходящаяПартия_Меркурий,
//					   Массив из ДокументСсылка.к2ИсходящаяПартия_Меркурий,
//					   Массив из ДокументСсылка.к2ВСД_Меркурий - массив объектов для гашения
//
Процедура ПакетноеГашение(Знач ДанныеЗаполнения) Экспорт
	
	параметрыОткрытия = Новый Структура("ДанныеЗаполнения", ДанныеЗаполнения);
	
	ОткрытьФорму("Документ.к2ВходящаяПартия_Меркурий.Форма.ПакетноеГашениеВСД", параметрыОткрытия);
	
КонецПроцедуры

Процедура ЭтапСинхронизацииНажатие( Знач ЭтапСинхронизации, СтандартнаяОбработка ) Экспорт
	
	префикс = НСтр( "ru='Заявка отклонена.'" ) + " ";
	
	Если СтрНайти( ЭтапСинхронизации, префикс ) = 1 Тогда
		
		СтандартнаяОбработка = Ложь;
		
		стр = СтрЗаменить( ЭтапСинхронизации, ";", ";" + Символы.ПС );
		
		стр = СтрЗаменить( стр, префикс, "" );
		
		ПоказатьВводСтроки(
			Новый ОписаниеОповещения( "ЭтапСинхронизацииНажатиеЗавершение", ЭтотОбъект ),
			стр,
			префикс,
			,
			Истина );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЭтапСинхронизацииНажатиеЗавершение( Строка, ДополнительныеПараметры ) Экспорт
	Возврат;
КонецПроцедуры

// Получает данные для импорта справочника продукция
//
// Параметры:
//  пТипПродукции			 - Перечисление.к2ТипПродукта_Меркурий - отбор по типу продукции
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы			 - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Продукция( Знач пТипПродукции, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	значениеОтбора = к2МеркурийВызовСервера.ЗначениеПоТипуПродукта( пТипПродукции );
	
	отбор = Новый Структура( "productType", значениеОтбора );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор );
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Аргус();
	данныеЗапроса.Запрос           = "getProductByTypeListRequest";
	данныеЗапроса.Ответ            = "getProductByTypeListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "productList";
	данныеЗапроса.Ответ_ИмяОбъекта = "product";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	соотОтбора = Новый Соответствие;
	соотОтбора.Вставить( пТипПродукции, отбор );
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, данныеЗапроса );
	
	Возврат соотРезультаты[пТипПродукции];
	
КонецФункции

// Получает данные для импорта справочника Наименования продукции
//
// Параметры:
//  пПродукция - СправочникСсылка.к2КлассификаторПродукции_Меркурий - отбор по продукту
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_ВидыПродукции( Знач пПродукция, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	отбор = Новый Структура( "productGuid", к2МеркурийВызовСервера.ГУИДПоСсылке( пПродукция ) );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор );
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Аргус();
	данныеЗапроса.Запрос           = "getSubProductByProductListRequest";
	данныеЗапроса.Ответ            = "getSubProductByProductListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "subProductList";
	данныеЗапроса.Ответ_ИмяОбъекта = "subProduct";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	соотОтбора = Новый Соответствие;
	соотОтбора.Вставить( пПродукция, отбор );
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, данныеЗапроса );
	
	Возврат соотРезультаты[пПродукция];
	
КонецФункции

// Получает данные для импорта справочника Наименования продукции
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пФильтр					 - Структура - отборы запросов
//  пНомерСтраницы			 - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_НаименованияПродукции( Знач пИсточник, Знач пФильтр, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, пФильтр );
	
	данныеЗапроса.ИмяШлюза = к2МеркурийКлиентСервер.ИмяШлюза_Аргус();
	
	данныеЗапроса.Запрос = "getProductItemListRequest";
	данныеЗапроса.Ответ  = "getProductItemListResponse";
	
	данныеЗапроса.Ответ_ИмяСписка  = "productItemList";
	данныеЗапроса.Ответ_ИмяОбъекта = "productItem";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса, пФильтр);
	
КонецФункции

// Получает данные для импорта справочника Наименования продукции
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пФильтр					 - Структура - отборы запросов
//  пНомерСтраницы			 - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_ХозяйствующиеСубъекты( Знач пИсточник, Знач пФильтр, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, пФильтр );
	
	данныеЗапроса.ИмяШлюза = к2МеркурийКлиентСервер.ИмяШлюза_Цербер();
	
	данныеЗапроса.Запрос = "getBusinessEntityListRequest";
	данныеЗапроса.Ответ  = "getBusinessEntityListResponse";
	
	данныеЗапроса.Ответ_ИмяСписка  = "businessEntityList";
	данныеЗапроса.Ответ_ИмяОбъекта = "businessEntity";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса, пФильтр);
	
КонецФункции

// Получает структуру описаний пары "хозяйствующий субъект - площадка" по GLN
//
// Параметры:
//  GLN - Строка - Global Location Number - уникальный номер площадки хозяйствующего субъекта, на которой он
//                 осуществляет деятельность
//  пИсточник - Строка - Источник запроса для отслеживания
// Возвращаемое значение:
//  Структура - Структура описаний.
Функция ХозяйствующийСубъектИПредприятиеПоGLN( Знач GLN, Знач пИсточник ) Экспорт
	
	параметрыПодключения = к2МеркурийКлиентСерверПовтИсп.ПараметрыПодключенияПоУмолчанию();
	
	данныеЗапроса = к2ЗапросыКлиентСервер_Меркурий.ДанныеЗапроса( параметрыПодключения );
	
	данныеЗапроса.ИмяШлюза                = к2МеркурийКлиентСервер.ИмяШлюза_Цербер();
	данныеЗапроса.Запрос                  = "getBusinessMemberByGLNRequest";
	данныеЗапроса.Ответ                   = "getBusinessMemberByGLNResponse";
	данныеЗапроса.Ответ_ИмяОбъекта        = "businessMember";
	данныеЗапроса.Ответ_ТипОбъекта        = "BusinessMember";
	данныеЗапроса.ПространствоИменЗапроса = к2МеркурийКлиентСервер.пи_Определения();
	данныеЗапроса.ПространствоИменОбъекта = к2МеркурийКлиентСервер.пи_Словарь();
	данныеЗапроса.Источник                = пИсточник;
	
	Если параметрыПодключения.ВыполнятьОбменНаСервере Тогда
		
		Результат = к2ЗапросыВызовСервера_Меркурий.ПолучитьХозяйствующийСубъектИПредприятиеПоGLN( GLN,
																								  данныеЗапроса );
		
	Иначе
		
		Результат = к2ЗапросыКлиентСервер_Меркурий.ПолучитьХозяйствующийСубъектИПредприятиеПоGLN( GLN,
																								  данныеЗапроса );
		
	КонецЕсли;
	
	Возврат Результат.Ответ;
	
КонецФункции

// Получает данные предприятий по ХС
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пХС - СправочникСсылка.к2ХозяйствующиеСубъекты_Меркурий - хозяйствующий субъект
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_ПредприятияПоХС(Знач пИсточник, Знач пХС, Знач пНомерСтраницы) Экспорт
	
	структОтбор = Новый Структура;
	структОтбор.Вставить( "businessEntity", пХС );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, структОтбор );
	
	данныеЗапроса.ИмяШлюза = к2МеркурийКлиентСервер.ИмяШлюза_Цербер();
	
	данныеЗапроса.Запрос = "getActivityLocationListRequest";
	данныеЗапроса.Ответ  = "getActivityLocationListResponse";
	
	данныеЗапроса.Ответ_ИмяСписка  = "ActivityLocationList";
	данныеЗапроса.Ответ_ИмяОбъекта = "location";
	данныеЗапроса.Ответ_ТипОбъекта = "BusinessMember";
	
	Возврат РезультатЗапросаСписка(данныеЗапроса, структОтбор);
	
КонецФункции

// Получает данные для импорта справочника Предприятия
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пФильтр					 - Структура - отборы запросов
//  пНомерСтраницы			 - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_РоссийскиеПредприятия( Знач пИсточник, Знач пФильтр, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, пФильтр );
	
	данныеЗапроса.ИмяШлюза = к2МеркурийКлиентСервер.ИмяШлюза_Цербер();
	
	данныеЗапроса.Запрос = "getRussianEnterpriseListRequest";
	данныеЗапроса.Ответ  = "getRussianEnterpriseListResponse";
	
	данныеЗапроса.Ответ_ИмяСписка  = "enterpriseList";
	данныеЗапроса.Ответ_ИмяОбъекта = "enterprise";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса, пФильтр);
	
КонецФункции

// Получает данные для импорта справочника Предприятия
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пФильтр					 - Структура - отборы запросов
//  пНомерСтраницы			 - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_ЗарубежныеПредприятия( Знач пИсточник, Знач пФильтр, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, пФильтр );
	
	данныеЗапроса.ИмяШлюза = к2МеркурийКлиентСервер.ИмяШлюза_Цербер();
	
	данныеЗапроса.Запрос = "getForeignEnterpriseListRequest";
	данныеЗапроса.Ответ  = "getForeignEnterpriseListResponse";
	
	данныеЗапроса.Ответ_ИмяСписка  = "enterpriseList";
	данныеЗапроса.Ответ_ИмяОбъекта = "enterprise";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка( данныеЗапроса, пФильтр );
	
КонецФункции

#Область Адреса

// Получает данные для импорта справочника страны
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы			 - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Страны( Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник );
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Икар();
	данныеЗапроса.Запрос           = "getAllCountryListRequest";
	данныеЗапроса.Ответ            = "getAllCountryListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "countryList";
	данныеЗапроса.Ответ_ИмяОбъекта = "country";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса);
	
КонецФункции

// Получает данные для импорта справочника регионы
//
// Параметры:
//  пСтрана - СправочникСсылка.к2КлассификаторСтран_Меркурий - отбор по стране
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Регионы( Знач пСтрана, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	значениеОтбора = к2МеркурийВызовСервера.ГУИДПоСсылке( пСтрана );
	отбор          = Новый Структура( "countryGuid", значениеОтбора );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор );
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Икар();
	данныеЗапроса.Запрос           = "getRegionListByCountryRequest";
	данныеЗапроса.Ответ            = "getRegionListByCountryResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "regionList";
	данныеЗапроса.Ответ_ИмяОбъекта = "region";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	соотОтбора = Новый Соответствие;
	соотОтбора.Вставить( пСтрана, отбор );
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, данныеЗапроса );
	
	Возврат соотРезультаты[пСтрана];
	
КонецФункции

// Получает данные для импорта справочника регионы
//
// Параметры:
//  пРегион - СправочникСсылка.к2КлассификаторРегионов_Меркурий - отбор по региону
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Районы( Знач пРегион, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	значениеОтбора = к2МеркурийВызовСервера.ГУИДПоСсылке( пРегион );
	отбор = Новый Структура( "regionGuid", значениеОтбора );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Икар();
	данныеЗапроса.Запрос           = "getDistrictListByRegionRequest";
	данныеЗапроса.Ответ            = "getDistrictListByRegionResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "districtList";
	данныеЗапроса.Ответ_ИмяОбъекта = "district";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;

	соотОтбора = Новый Соответствие;
	соотОтбора.Вставить( пРегион, отбор );
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, данныеЗапроса );
	
	Возврат соотРезультаты[пРегион];
	
КонецФункции

// Получает данные для импорта справочника регионы
//
// Параметры:
//  пРегион - СправочникСсылка.к2КлассификаторРегионов_Меркурий, СправочникСсылка.к2КлассификаторРайонов_Меркурий -
//            отбор по региону или району
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_НаселенныеПункты( Знач пВладелец, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	Если ТипЗнч( пВладелец ) = Тип( "СправочникСсылка.к2КлассификаторРайонов_Меркурий" ) Тогда
		
		имяКлючаОтбора = "districtGuid";
		
	Иначе
		
		имяКлючаОтбора = "regionGuid";
		
	КонецЕсли;
	
	значениеОтбора = к2МеркурийВызовСервера.ГУИДПоСсылке( пВладелец );
	отбор = Новый Структура( имяКлючаОтбора, значениеОтбора );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Икар();
	данныеЗапроса.Ответ_ИмяСписка  = "localityList";
	данныеЗапроса.Ответ_ИмяОбъекта = "locality";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Если ТипЗнч( пВладелец ) = Тип( "СправочникСсылка.к2КлассификаторРайонов_Меркурий" ) Тогда
		
		данныеЗапроса.Запрос = "getLocalityListByDistrictRequest";
		данныеЗапроса.Ответ  = "getLocalityListByDistrictResponse";
		
	Иначе
		
		данныеЗапроса.Запрос = "getLocalityListByRegionRequest";
		данныеЗапроса.Ответ  = "getLocalityListByRegionResponse";
		
	КонецЕсли;
	
	соотОтбора = Новый Соответствие;
	соотОтбора.Вставить( пВладелец, отбор );
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, данныеЗапроса );
	
	Возврат соотРезультаты[пВладелец];
	
КонецФункции

// Получает данные для импорта справочника улицы
//
// Параметры:
//  пНаселенныйПункт - СправочникСсылка.к2КлассификаторНаселенныхПунктов_Меркурий - отбор по региону
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Улицы( Знач пНаселенныйПункт, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	значениеОтбора = к2МеркурийВызовСервера.ГУИДПоСсылке( пНаселенныйПункт );
	отбор = Новый Структура( "localityGuid", значениеОтбора );
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Икар();
	данныеЗапроса.Запрос           = "getStreetListByLocalityRequest";
	данныеЗапроса.Ответ            = "getStreetListByLocalityResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "streetList";
	данныеЗапроса.Ответ_ИмяОбъекта = "street";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	соотОтбора = Новый Соответствие;
	соотОтбора.Вставить( пНаселенныйПункт, отбор );
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, данныеЗапроса );
	
	Возврат соотРезультаты[пНаселенныйПункт];
	
КонецФункции

#КонецОбласти

// Получает данные для импорта справочника заболевания
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Заболевания( Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Словарь();
	данныеЗапроса.Запрос           = "getDiseaseListRequest";
	данныеЗапроса.Ответ            = "getDiseaseListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "diseaseList";
	данныеЗапроса.Ответ_ИмяОбъекта = "disease";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса);
	
КонецФункции

// Получает данные для импорта справочника единиц измерений
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_ЕдиницыИзмерения( Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Словарь();
	данныеЗапроса.Запрос           = "getUnitListRequest";
	данныеЗапроса.Ответ            = "getUnitListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "unitList";
	данныеЗапроса.Ответ_ИмяОбъекта = "unit";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса);
	
КонецФункции

// Получает данные для импорта справочника целей
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_Цели( Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Словарь();
	данныеЗапроса.Запрос           = "getPurposeListRequest";
	данныеЗапроса.Ответ            = "getPurposeListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "purposeList";
	данныеЗапроса.Ответ_ИмяОбъекта = "purpose";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса);
	
КонецФункции

// Получает данные для импорта справочника методов исследований
//
// Параметры:
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_МетодыИсследования( Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник );
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Словарь();
	данныеЗапроса.Запрос           = "getResearchMethodListRequest";
	данныеЗапроса.Ответ            = "getResearchMethodListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "researchMethodList";
	данныеЗапроса.Ответ_ИмяОбъекта = "researchMethod";
	данныеЗапроса.Ответ_ТипОбъекта = данныеЗапроса.Ответ_ИмяОбъекта;
	
	Возврат РезультатЗапросаСписка(данныеЗапроса);
	
КонецФункции

// Получает данные для импорта справочника к2КлассификаторУсловийРегионализации_Меркурий
//
// Параметры:
//  пЗаболевание - СправочникСсылка.к2Заболевания_Меркурий - отбор по заболеванию
//  пИсточник - Строка - Источник запроса для отслеживания
//  пНомерСтраницы - Число или Неопределено - номер получаемой страницы, если Неопределено, то будут получены все страницы.
//
// Возвращаемое значение:
//  Структура - результат запроса.
//
Функция РезультатыЗапросов_УсловияРегионализации( Знач пЗаболевание, Знач пИсточник, Знач пНомерСтраницы ) Экспорт
	
	отбор = Новый Структура;
	
	Если ЗначениеЗаполнено( пЗаболевание ) Тогда
		
		отбор.Вставить( "Заболевание", пЗаболевание );
		
	КонецЕсли;
	
	данныеЗапроса = ДанныеЗапросаНаПолучениеСписка( пНомерСтраницы, пИсточник, отбор);
	
	данныеЗапроса.ИмяШлюза         = к2МеркурийКлиентСервер.ИмяШлюза_Регионализация();
	данныеЗапроса.Запрос           = "getR13nConditionListRequest";
	данныеЗапроса.Ответ            = "getR13nConditionListResponse";
	данныеЗапроса.Ответ_ИмяСписка  = "r13nConditionList";
	данныеЗапроса.Ответ_ИмяОбъекта = "condition";
	данныеЗапроса.Ответ_ТипОбъекта = "RegionalizationCondition";
	
	Возврат РезультатЗапросаСписка(данныеЗапроса, отбор);
	
КонецФункции

#Область РаботаЧерезЗаявки

// Читает результаты обработки заявок
//
// Параметры:
//  Форма	 - УправляемаяФорма - форма-владелец
//  пОбъект	 - Ссылка - ссылка фильтр, если указана, то запросы выполняются только по этой заявке.
//
Процедура ОбновитьПрочитатьРезультатыОбработкиЗаявок(Форма, Знач пОбъект) Экспорт

	Если Форма.Объект.ВыполняетсяИмпортДанных
		Или (ТипЗнч(пОбъект) = Тип("СправочникСсылка.к2Заявки_Меркурий")
		И к2МеркурийВызовСервера.ЕстьНеобработанныеДочерниеОбъекты(пОбъект)) Тогда

		Если Не к2МеркурийКлиентСерверПовтИсп.ЗапретЗапросовСКлиента() Тогда

			параметрыПодключения = к2МеркурийКлиентСерверПовтИсп.ПараметрыПодключенияПользователя(Форма.Инициатор);

			к2ЗаявкиКлиентСервер_Меркурий.ЗапроситьРезультатВыполненияПоЗаявкам(Форма.ИмяФормы, параметрыПодключения, пОбъект);
			к2ЗапросыКлиентСервер_Меркурий.ПолучитьРезультатыОбработкиЗаявок(Форма.ИмяФормы, параметрыПодключения, пОбъект);

		КонецЕсли;

	КонецЕсли;

	ПеречитатьФорму(Форма);
	
	Если Форма.Объект.ВыполняетсяИмпортДанных
		Или (ТипЗнч(пОбъект) = Тип("СправочникСсылка.к2Заявки_Меркурий")
		И к2МеркурийВызовСервера.ЕстьНеобработанныеДочерниеОбъекты(пОбъект)) Тогда
		
		к2ФормыКлиент_Меркурий.ПодключитьДинамическийОбработчикОжидания(Форма, "ОбновитьПрочитатьРезультатыОбработкиЗаявок");
		
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ДанныеЗапросаНаПолучениеСписка( Знач пНомерСтраницы, Знач пИсточник, Знач пОтбор = Неопределено )
	
	параметрыПодключения = к2МеркурийКлиентСерверПовтИсп.ПараметрыПодключенияПоУмолчанию();
	
	данныеЗапроса          = к2ЗапросыКлиентСервер_Меркурий.ДанныеЗапроса(параметрыПодключения);
	данныеЗапроса.Страница = пНомерСтраницы;
	данныеЗапроса.Источник = пИсточник;
	данныеЗапроса.Отбор    = пОтбор;
	
	Возврат данныеЗапроса;
	
КонецФункции

Функция РезультатЗапросаСписка( Знач пДанныеЗапроса, Знач пОтбор = Неопределено)

	соотОтбора = Новый Соответствие;
	
	Если пОтбор = Неопределено Тогда
		
		соотОтбора.Вставить( " ", Новый Структура );
		
	Иначе
		
		соотОтбора.Вставить( " ", пОтбор );
		
	КонецЕсли;
	
	соотРезультаты = к2МеркурийКлиентСервер.ПолучитьРезультатыЗапросовСписковКСервису( соотОтбора, пДанныеЗапроса );
	
	Возврат соотРезультаты[" "];

КонецФункции

Процедура ПеречитатьФорму(Форма)
	
	Попытка
		
		Форма.Прочитать();
		
	Исключение
		
		// Если не удалось перечитать форму, то объект удален и форма эта больше не нужна
		
		устаревшийГУИД = ВРег(Строка( Форма.Объект.Ссылка.УникальныйИдентификатор()));
		
		новОбъект = к2МеркурийВызовСервера.НоваяСсылкаПоВременной(устаревшийГУИД);
		
		Если ЗначениеЗаполнено(новОбъект) Тогда
			
			навигационнаяСсылка = ПолучитьНавигационнуюСсылку(новОбъект);
			ПерейтиПоНавигационнойСсылке(навигационнаяСсылка);
			
			Форма.Закрыть();
			
		Иначе
			
			// Если ошибка не в устаревшем объекте, то прокидываем ошибку выше
			
			ВызватьИсключение;
			
		КонецЕсли;
		
	КонецПопытки;
	
КонецПроцедуры

#КонецОбласти
