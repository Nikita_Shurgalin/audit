#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура Добавить( пАдресация , пБП, пПросматривать, пВыполнять, пОтменять ) Экспорт
	
	нз = РегистрыСведений.упРазрешенияНаЗадачи.СоздатьНаборЗаписей();
	
	нз.Отбор.БизнесПроцесс.Установить( пБП );
	нз.Отбор.Адресация.Установить( пАдресация );
	
	новСтрока = нз.Добавить();
	
	новСтрока.Адресация     = пАдресация;
	новСтрока.БизнесПроцесс = пБП;
	новСтрока.Просматривать = пПросматривать;
	новСтрока.Выполнять     = пВыполнять;
	новСтрока.Отменять      = пОтменять;
	
	нз.Записать();
	
КонецПроцедуры	//Добавить

#КонецЕсли