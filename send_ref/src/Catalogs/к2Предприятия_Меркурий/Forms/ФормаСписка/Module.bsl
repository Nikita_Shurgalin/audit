
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	отборПоПринадлежности = глЗначениеСтруктуры(Параметры, "Отбор.ЭтоРоссийскоеПредприятие");
	
	Если отборПоПринадлежности = Ложь Тогда
		
		ЭтоРоссийскоеПредприятие = 0;
		
	Иначе
		
		ЭтоРоссийскоеПредприятие = 1;
		
	КонецЕсли;
	
	НастроитьЭлементы();
	
	Если Не отборПоПринадлежности = Неопределено Тогда
		
		Параметры.Отбор.Удалить("ЭтоРоссийскоеПредприятие");
		
		к2ФормыКлиентСервер_Меркурий.УстановитьЭлементОтбораСписка(Список,
																   "ЭтоРоссийскоеПредприятие",
																   отборПоПринадлежности,
																   Истина,
																   ВидСравненияКомпоновкиДанных.Равно,
																   РежимОтображенияЭлементаНастройкиКомпоновкиДанных.БыстрыйДоступ);
		
	КонецЕсли;
	
	к2Формы_Меркурий.ФормаСписка_ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура Отбор_ЭтоРоссийскоеПредприятиеПриИзменении(Элемент)
	
	НастроитьЭлементы();
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоНаименованиюПриИзменении(Элемент)
	
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоGLNПриИзменении(Элемент)
	
	Элементы.ОтборПоGLN.ОтметкаНезаполненного = ЗначениеЗаполнено(ОтборПоGLN)
		И Не СтрДлина(ОтборПоGLN) = 13;
	
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоСтранеПриИзменении(Элемент)
	
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоРегионуПриИзменении(Элемент)
	
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоХСПриИзменении(Элемент)
	
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоГУИДПриИзменении(Элемент)
	
	к2ФормыКлиентСервер_Меркурий.ОчиститьТаблицуИмпорта(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СоздатьЭлементыПоВыбраннымСтрокам(Команда)
	
	созданныеЭлементы = СоздатьЭлементыПоВыбраннымСтрокамНаСервере();
	
	к2ФормыКлиент_Меркурий.СоздатьЭлементыПоВыбраннымСтрокамТаблицыИмпорта_Завершение(
		ЭтотОбъект,
		созданныеЭлементы);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьЭлементы(Команда)
	
	ЗагрузитьСтраницуЭлементов();
	
КонецПроцедуры

&НаКлиенте
Процедура ПредыдущаяСтраница(Команда)
	
	ТекущаяСтраница = ТекущаяСтраница - 1;
	
	ЗагрузитьСтраницуЭлементов();
	
КонецПроцедуры

&НаКлиенте
Процедура СледующаяСтраница(Команда)
	
	ТекущаяСтраница = ТекущаяСтраница + 1;
	
	ЗагрузитьСтраницуЭлементов();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерваяСтраница(Команда)
	
	ТекущаяСтраница = 1;
	
	ЗагрузитьСтраницуЭлементов();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура НастроитьЭлементы()
	
	Если ЭтоРоссийскоеПредприятие Тогда
		
		ОтборПоСтране = к2МеркурийКлиентСерверПовтИсп.Страна_Россия();
		Элементы.ОтборПоСтране.Доступность = Ложь;
		
	Иначе
		
		Если ОтборПоСтране = к2МеркурийКлиентСерверПовтИсп.Страна_Россия() Тогда
			
			ОтборПоСтране = Неопределено;
			
		КонецЕсли;
		
		Элементы.ОтборПоСтране.Доступность = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьСтраницуЭлементов()
	
	ТекущаяСтраница = Мин(ВсегоСтраниц, ТекущаяСтраница);
	ТекущаяСтраница = Макс(1, ТекущаяСтраница);
	
	Если ЗначениеЗаполнено(ОтборПоГУИД) Тогда
		
		ссылка = ПредопределенноеЗначение("Справочник.к2Предприятия_Меркурий.ПустаяСсылка");
		
		результатЗапроса = к2ЗапросыКлиентПовтИсп_Меркурий.ЭлементыПоГУИДам(ОтборПоГУИД, ссылка, ИмяФормы);
		
	ИначеЕсли ЗначениеЗаполнено(ОтборПоGLN) Тогда
		
		результатЗапроса = к2ЗапросыКлиентПовтИсп_Меркурий.ПредприятиеПоGLN(ОтборПоGLN, ИмяФормы);
		
	ИначеЕсли ЗначениеЗаполнено(ОтборПоХС) Тогда
		
		результатЗапроса = к2ЗапросыКлиентПовтИсп_Меркурий.ПредприятияПоХС(ТекущаяСтраница, ОтборПоХС, ИмяФормы);
		
	ИначеЕсли ЭтоРоссийскоеПредприятие = 1 Тогда
		
		результатЗапроса = к2ЗапросыКлиентПовтИсп_Меркурий.РоссийскиеПредприятия(ТекущаяСтраница,
																				 ОтборПоНаименованию,
																				 ОтборПоНомеруПредприятия,
																				 ОтборПоСтране,
																				 ОтборПоРегиону,
																				 ИмяФормы);
		
	Иначе
		
		результатЗапроса = к2ЗапросыКлиентПовтИсп_Меркурий.ЗарубежныеПредприятия(ТекущаяСтраница,
																				 ОтборПоНаименованию,
																				 ОтборПоНомеруПредприятия,
																				 ОтборПоСтране,
																				 ОтборПоРегиону,
																				 ИмяФормы);
		
	КонецЕсли;
	
	к2ЗапросыКлиент_Меркурий.ДополнитьОписания(результатЗапроса.Описания,
											   "к2Предприятия_Меркурий",
											   ИмяФормы);
	
	ЗаполнитьТаблицуИмпорта(результатЗапроса);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТаблицуИмпорта(Знач пРезультатЗапроса)
	
	к2Формы_Меркурий.ЗаполнитьТаблицуИмпорта(ЭтотОбъект, пРезультатЗапроса);
	
	Для каждого цСтрока Из ТаблицаИмпорта Цикл
		
		цОписание = цСтрока.Описание;
		
		Если цОписание.Свойство("Предприятие") Тогда
			
			цОписание = цОписание.Предприятие;
			
		КонецЕсли;
		
		Если (Не ЗначениеЗаполнено(цСтрока.Страна)) Тогда
			
			цСтрока.Страна = глЗначениеСтруктуры(цОписание, "Страна.Наименование");
			
		КонецЕсли;
		
		Если (Не ЗначениеЗаполнено(цСтрока.Регион)) Тогда
			
			цСтрока.Регион = глЗначениеСтруктуры(цОписание, "Регион.Наименование");
			
		КонецЕсли;
		
		цСтрока.Адрес = глЗначениеСтруктуры(цОписание, "Адрес.Представление");
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция СоздатьЭлементыПоВыбраннымСтрокамНаСервере()
	
	Возврат к2Формы_Меркурий.СоздатьЭлементыПоВыбраннымСтрокам(ЭтотОбъект);
	
КонецФункции

#КонецОбласти