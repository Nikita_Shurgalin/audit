
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда 
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
	Элементы.СтраницаДополнительно.Картинка = ОбщегоНазначенияКлиентСервер.КартинкаКомментария(Объект.Комментарий);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий");
	
КонецПроцедуры

&НаКлиенте
Процедура ВыработкаКоличествоПриИзменении(Элемент)
	
	РассчитатьСуммуУчтенныхОпераций(ПолучитьТаблицуФормы(Элемент).ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыработкаСтоимостьОперацииПриИзменении(Элемент)
	
	РассчитатьСуммуУчтенныхОпераций(ПолучитьТаблицуФормы(Элемент).ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыработкаСуммаУчтенныхОперацийПриИзменении(Элемент)
	
	РассчитатьСтоимостьОперации(ПолучитьТаблицуФормы(Элемент).ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарплатаПремиальныйКоэффициентПриИзменении(Элемент)
	
	РассчитатьСумму(ПолучитьТаблицуФормы(Элемент).ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарплатаСуммаУчтенныхОперацийПриИзменении(Элемент)
	
	РассчитатьСумму(ПолучитьТаблицуФормы(Элемент).ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарплатаСуммаПриИзменении(Элемент)
	
	РассчитатьПремиальныйКоэффициент(ПолучитьТаблицуФормы(Элемент).ТекущиеДанные);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыВыработка

&НаКлиенте
Процедура ВыработкаПриИзменении(Элемент)
	
	ВыработкаПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаЗаполнитьВыработку(Команда)
	
	ЗаполнитьВыработку();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЗаполнитьЗарплату(Команда)
	
	ЗаполнитьЗарплату();
	
КонецПроцедуры

#Область СтандартныеПодсистемыПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	Элементы.СтраницаДополнительно.Картинка = ОбщегоНазначенияКлиентСервер.КартинкаКомментария(Объект.Комментарий);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВыработку()
	
	ТаблицаВыработки = Документы.к2НачислениеЗарплатыПоОперациям.ПолучитьВыработкуПоСотрудникам(
		Объект.РабочийЦентр,
		Объект.ДатаСмены,
		Объект.Смена);
	
	Объект.Выработка.Загрузить(ТаблицаВыработки);
	Объект.Выработка.Сортировать("Сотрудник");
	
	ВыработкаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьЗарплату()
	
	ТаблицаЗарплаты = Объект.Выработка.Выгрузить(, "
	|Сотрудник,
	|СуммаУчтенныхОпераций
	|");
	
	ТаблицаЗарплаты.Свернуть("
	|Сотрудник
	|", "
	|СуммаУчтенныхОпераций
	|");
	
	ТаблицаЗарплаты.Колонки.Добавить("ПремиальныйКоэффициент");
	ТаблицаЗарплаты.Колонки.Добавить("Сумма");
	
	ТаблицаЗарплаты.ЗаполнитьЗначения(1, "ПремиальныйКоэффициент");
	ТаблицаЗарплаты.ЗагрузитьКолонку(ТаблицаЗарплаты.ВыгрузитьКолонку("СуммаУчтенныхОпераций"), "Сумма");
	
	Объект.Зарплата.Загрузить(ТаблицаЗарплаты);
	
	Объект.Зарплата.Сортировать("Сотрудник");
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьСуммуУчтенныхОпераций(СтрокаТабличнойЧастиВыработка)
	
	СтрокаТабличнойЧастиВыработка.СуммаУчтенныхОпераций = (
		СтрокаТабличнойЧастиВыработка.Количество
		* СтрокаТабличнойЧастиВыработка.СтоимостьОперации);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьСтоимостьОперации(СтрокаТабличнойЧастиВыработка)
	
	СтрокаТабличнойЧастиВыработка.СтоимостьОперации = к2КоллекцииКлиентСервер.Разделить(
		СтрокаТабличнойЧастиВыработка.СуммаУчтенныхОпераций,
		СтрокаТабличнойЧастиВыработка.Количество);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьСумму(СтрокаТабличнойЧастиЗарплата)
	
	СтрокаТабличнойЧастиЗарплата.Сумма = (
		СтрокаТабличнойЧастиЗарплата.СуммаУчтенныхОпераций
		* СтрокаТабличнойЧастиЗарплата.ПремиальныйКоэффициент);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьПремиальныйКоэффициент(СтрокаТабличнойЧастиЗарплата)
	
	СтрокаТабличнойЧастиЗарплата.ПремиальныйКоэффициент = к2КоллекцииКлиентСервер.Разделить(
		СтрокаТабличнойЧастиЗарплата.Сумма,
		СтрокаТабличнойЧастиЗарплата.СуммаУчтенныхОпераций);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьТаблицуФормы(Знач ПолеФормы)
	
	Пока Не ТипЗнч(ПолеФормы) = Тип("ТаблицаФормы")
		И Не к2ОбщегоНазначенияКлиентСервер.ЭтоФорма(ПолеФормы) Цикл 
		ПолеФормы = ПолеФормы.Родитель;
	КонецЦикла;
	
	Если ТипЗнч(ПолеФормы) <> Тип("ТаблицаФормы") Тогда 
		ПолеФормы = Неопределено;
	КонецЕсли;
	
	Возврат ПолеФормы;
	
КонецФункции

&НаСервере
Процедура ВыработкаПриИзмененииНаСервере()
	
	Объект.Зарплата.Очистить();
	
КонецПроцедуры

#КонецОбласти
