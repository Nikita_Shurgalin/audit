
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	значенияОтбора = Новый Массив;
	значенияОтбора.Добавить(ПараметрКоманды);
	значенияОтбора.Добавить(ВидПродукции(ПараметрКоманды));
	
	отбор = Новый Структура("НаименованиеПродукции", значенияОтбора);
	
	ПараметрыФормы = Новый Структура("Отбор", отбор);
	ОткрытьФорму(
		"Справочник.к2УпаковкиИМаркировкиПродукции_Меркурий.ФормаСписка",
		ПараметрыФормы,
		ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыВыполненияКоманды.Окно,
		ПараметрыВыполненияКоманды.НавигационнаяСсылка);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция ВидПродукции(Знач НаименованиеПродукции)
	
	Возврат глРеквизит_Кэш(НаименованиеПродукции, "Владелец");
	
КонецФункции

#КонецОбласти

