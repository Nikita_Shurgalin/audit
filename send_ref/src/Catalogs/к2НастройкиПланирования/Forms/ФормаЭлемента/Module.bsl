
#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ОбновлениеОтображения();
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ЗаполнитьОтборы(глРеквизит(ТекущийОбъект, "хзНастройкиОтборов"));
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	СохранитьОтборы(ТекущийОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ОбновлениеОтображения();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОтборТоварныеКатегорииПриИзменении(Элемент)
	
	Модифицированность = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборГруппыПланированияПриИзменении(Элемент)
	
	Модифицированность = Истина;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыОтборовОрганизацийПартнеров

&НаКлиенте
Процедура ОтборОрганизацииКонтрагентыБезОграниченийПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.ОтборОрганизацииКонтрагенты.ТекущиеДанные;
	Если ТекущиеДанные.БезОграничений Тогда 
		ТекущиеДанные.ВидСравнения = Неопределено;
		ТекущиеДанные.Контрагенты = Неопределено;
	КонецЕсли;	
	
	Модифицированность = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборОрганизацииКонтрагентыПриИзменении(Элемент)
	
	Модифицированность = Истина;
	
КонецПроцедуры
	
#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеОтображения

&НаКлиенте
Процедура ОбновлениеОтображения()

	УстановитьВидимостьДоступность();
	
	ПодключитьОбработчикОжидания("Подключаемый_ОбновлениеОтображения", 1, Ложь);	
				
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновлениеОтображения()
	
	Если Модифицированность Тогда
		
		УстановитьВидимостьДоступность();
		ОтключитьОбработчикОжидания("Подключаемый_ОбновлениеОтображения");
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	к2ПроизводствоСервер.УстановитьВидимостьДоступностьКнопокЗакрытьЗаписать(ЭтотОбъект);
			
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ЗаполнитьОтборы(хзНастройки)
	
	Настройки = хзНастройки.Получить();
	Если НЕ ЗначениеЗаполнено(Настройки) Тогда 
		Возврат;
	КонецЕсли;
	
	Если Настройки.Свойство("ОтборОрганизацииКонтрагенты") Тогда 
		ОтборОрганизацииКонтрагенты.Загрузить(Настройки.ОтборОрганизацииКонтрагенты);
	КонецЕсли;
	
	Если Настройки.Свойство("ОтборТоварныеКатегории") Тогда 
		ОтборТоварныеКатегории.ЗагрузитьЗначения(Настройки.ОтборТоварныеКатегории);
	КонецЕсли;
	
	Если Настройки.Свойство("ОтборГруппыПланирования") Тогда 
		ОтборГруппыПланирования.ЗагрузитьЗначения(Настройки.ОтборГруппыПланирования);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура СохранитьОтборы(ТекущийОбъект)
	
	СтруктураНастроекОтборов = Новый Структура;

	СтруктураНастроекОтборов.Вставить("ОтборОрганизацииКонтрагенты", ОтборОрганизацииКонтрагенты.Выгрузить());
	СтруктураНастроекОтборов.Вставить("ОтборТоварныеКатегории", ОтборТоварныеКатегории.ВыгрузитьЗначения());
	СтруктураНастроекОтборов.Вставить("ОтборГруппыПланирования", ОтборГруппыПланирования.ВыгрузитьЗначения());
		
	ТекущийОбъект.хзНастройкиОтборов = Новый ХранилищеЗначения(СтруктураНастроекОтборов);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
