
Функция ВерсияБиблиотеки() Экспорт
	
	Возврат "3.1.2.0";
	
КонецФункции

Функция ИмяБиблиотеки() Экспорт
	
	Возврат "КСУП";
	
КонецФункции

Функция СтатусСборки() Экспорт
	
	Возврат "m";
	
КонецФункции

Функция ВерсияКСУП() Экспорт
	
	cтатус = СтатусСборки();
	
	Если ЗначениеЗаполнено( cтатус ) Тогда
		
		Возврат стрЗаполнить( "%1 [%2]", ВерсияБиблиотеки(), cтатус );
		
	Иначе
		
		Возврат ВерсияБиблиотеки();
		
	КонецЕсли;
	
КонецФункции


Процедура ВыполнитьОбновлениеИнформационнойБазы() Экспорт
	
	// Проверка наличия прав для обновления информационной базы.
	Если НЕ ЕстьПраваНаОбновлениеИнформационнойБазы() Тогда
		Сообщение = НСтр("ru = 'Недостаточно прав для выполнения обновления. Обратитесь к системному администратору.'");
		ЗаписатьОшибку(Сообщение);
		ВызватьИсключение Сообщение;
	КонецЕсли;
	
	СтараяВерсияКСУП = ОбновлениеИнформационнойБазы.ВерсияИБ( ИмяБиблиотеки() );
	
	ОбновлениеИнформационнойБазы.ВыполнитьИтерациюОбновления(ИмяБиблиотеки(),ВерсияБиблиотеки(),ОбработчикиОбновления());
	
КонецПроцедуры

Функция ЕстьПраваНаОбновлениеИнформационнойБазы()
	
	Возврат упСистемаБезопасностиПовтИсп.ЕстьПолныеПрава();
	
КонецФункции

Процедура ЗаписатьОшибку(Знач Текст)
	
	глЛог( Текст, Перечисления.упТипыОшибокДляЛога.Прочее );
	
КонецПроцедуры

Функция ОбработчикиОбновления()
	
	Обработчики = ОбновлениеИнформационнойБазы.НоваяТаблицаОбработчиковОбновления();
	
	//Обработчик = Обработчики.Добавить();
	//Обработчик.Версия       = "ВЕРСИЯ_НАПРИМЕР_3.0.2.32";
	//Обработчик.Процедура    = "ОБРАБОТЧИК_НАПРИМЕР_упОбновлениеИБ.ОбновитьСтатистикиЗадачи";
	
	Возврат Обработчики;
	
КонецФункции


#Область ПервоначальноеЗаполнение

// Процедура вызывается при первом запуске системы для первоначального заполнения информационной базы данными.
//
Процедура ПервоначальноеЗаполнениеИнформационнойБазы() Экспорт
	
	ЗаполнитьПланВидовХарактеристикТипыЗначенийРеквизитов();
	ЗаполнитьПредопределенныеРеквизиты();
	Справочники.упШаблоныМетодов.ЗаполнитьШаблоныПервоначальнымиДанными();
	
	ЗаполнитьПредопределенныеСобытия();
	
КонецПроцедуры

// Процедура заполнения списка типов значений реквизитов БП на основании метаданных конфигурации.
//
Процедура ЗаполнитьПланВидовХарактеристикТипыЗначенийРеквизитов(ВыводитьСообщения = Истина) Экспорт
	
	Перем НовыйТипЗначения;
	
	ВсегоОбъектов = Метаданные.Справочники.Количество() + Метаданные.Документы.Количество() + Метаданные.Перечисления.Количество();
	
	МассивТипов = Новый Массив;
	
	Для Каждого Справочник Из Метаданные.Справочники Цикл
		
		МассивТипов.Очистить();
		
		МассивТипов.Добавить(Тип("СправочникСсылка." + Справочник.Имя));
		
		НовыйТипЗначения = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьЭлементПоТипу( Новый ОписаниеТипов(МассивТипов));
		
		Для Каждого цРеквизит Из Справочник.Реквизиты Цикл
			
			НовыйТипЗначения = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьЭлементПоТипу(цРеквизит.Тип);
			
		КонецЦикла;
		
	КонецЦикла;
	
	Для Каждого Документ Из Метаданные.Документы Цикл
		
		МассивТипов.Очистить();
		МассивТипов.Добавить(Тип("ДокументСсылка." + Документ.Имя));
		
		НовыйТипЗначения = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьЭлементПоТипу(Новый ОписаниеТипов(МассивТипов));
		
		Для Каждого цРеквизит Из Документ.Реквизиты Цикл
			
			НовыйТипЗначения = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьЭлементПоТипу(цРеквизит.Тип);
			
		КонецЦикла;
		
	КонецЦикла;
	
	Для Каждого Перечисление Из Метаданные.Перечисления Цикл
		
		МассивТипов.Очистить();
		МассивТипов.Добавить(Тип("ПеречислениеСсылка." + Перечисление.Имя));
		
		НовыйТипЗначения = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьЭлементПоТипу(Новый ОписаниеТипов(МассивТипов));
		
	КонецЦикла;
		
КонецПроцедуры

Процедура ЗаполнитьПредопределенныеРеквизиты()
	
		// Запонение имен у предопределенных реквизитов
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упРеквизиты.Ссылка
	               |ИЗ
	               |	Справочник.упРеквизиты КАК упРеквизиты
	               |ГДЕ
	               |	упРеквизиты.Предопределенный
	               |	И (НЕ упРеквизиты.ЭтоГруппа)";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		обРеквизит = Выборка.Ссылка.ПолучитьОбъект();
		обРеквизит.Имя = Справочники.упРеквизиты.ПолучитьИмяПредопределенного( Выборка.Ссылка );
		обРеквизит.ТипРеквизита = Справочники.упРеквизиты.ПолучитьТипПредопределенногоЭлемента( обРеквизит.Имя );
		обРеквизит.Записать();
		
	КонецЦикла;
	
КонецПроцедуры


Процедура ЗаполнитьПредопределенныеСобытия()
	
	обСобытие = Справочники.упСобытие.СозданиеЗадачи.ПолучитьОбъект();
	
	обСобытие.Активность = Истина;
	обСобытие.ИсточникСобытия = Перечисления.упВидыИсточниковСобытия.Задача;
	обСобытие.ТипСобытия = Перечисления.упТипСобытияПоЗадаче.СозданаЗадача;
	обСобытие.Комментарий = НСтр( "ru='Системное событие для работы оповещений пользователю'" );
	
	обСобытие.Записать();
	
	обСобытие = Справочники.упСобытие.ИстечениеСрокаВыполненияЗадачи.ПолучитьОбъект();
	
	обСобытие.Активность = Истина;
	обСобытие.ИсточникСобытия = Перечисления.упВидыИсточниковСобытия.Задача;
	обСобытие.ТипСобытия = Перечисления.упТипСобытияПоЗадаче.ПросроченаЗадача;
	обСобытие.Комментарий = НСтр( "ru='Системное событие для работы оповещений пользователю'" );
	
	обСобытие.Записать();
	
	обСобытие = Справочники.упСобытие.ВыполнениеЗадачи.ПолучитьОбъект();
	
	обСобытие.Активность = Истина;
	обСобытие.ИсточникСобытия = Перечисления.упВидыИсточниковСобытия.Задача;
	обСобытие.ТипСобытия = Перечисления.упТипСобытияПоЗадаче.ВыполненаЗадача;
	обСобытие.Комментарий = НСтр( "ru='Системное событие для работы оповещений пользователю'" );
	
	обСобытие.Записать();
	
КонецПроцедуры

#КонецОбласти

////////////////////////////////////////////////////////////////////////////////
// Обновление информационной базы демонстрационной конфигурации (БиблиотекаСтандартныхПодсистемДемо).
//  
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Сведения о библиотеке (или конфигурации).

// См. описание в общем модуле ОбновлениеИнформационнойБазыБСП.
Процедура ПриДобавленииПодсистемы(Описание) Экспорт
	
	Описание.Имя = ИмяБиблиотеки();
	Описание.Версия = ВерсияБиблиотеки();
	
	// Требуется библиотека стандартных подсистем.
	Описание.ТребуемыеПодсистемы.Добавить("КСУП");
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Обработчики обновления информационной базы.

// См. описание в общем модуле ОбновлениеИнформационнойБазыБСП.
Процедура ПриДобавленииОбработчиковОбновления(Обработчики) Экспорт
	
	// Обработчики этого события для подсистем БСП добавляются через подписку на служебное событие:
	// "СтандартныеПодсистемы.ОбновлениеВерсииИБ\ПриДобавленииОбработчиковОбновления".
	//
	// Процедуры обработки этого события всех подсистем БСП имеют то же имя, что и эта процедура,
	// но размещены в своих подсистемах.
	// Чтобы найти процедуры можно выполнить глобальный поиск по имени процедуры.
	// Чтобы найти модули в которых размещены процедуры, можно выполнить поиск по имени события.
	
	
	//ОбработчикиСобытия = ОбщегоНазначения.ОбработчикиСлужебногоСобытия(
	//	"СтандартныеПодсистемы.ОбновлениеВерсииИБ\ПриДобавленииОбработчиковОбновления");
	//
	//Для каждого Обработчик Из ОбработчикиСобытия Цикл
	//	Если Обработчик.Подсистема <> "СтандартныеПодсистемы" Тогда
	//		Продолжить;
	//	КонецЕсли;
	//	Обработчик.Модуль.ПриДобавленииОбработчиковОбновления(Обработчики);
	//КонецЦикла;
	
	Обработчик = Обработчики.Добавить();
	Обработчик.НачальноеЗаполнение = Истина;
	Обработчик.Процедура = "упОбновлениеИБ.ПервоначальноеЗаполнениеИнформационнойБазы";
	
КонецПроцедуры

// См. описание в общем модуле ОбновлениеИнформационнойБазыБСП.
Процедура ПередОбновлениемИнформационнойБазы() Экспорт
	
КонецПроцедуры

// См. описание в общем модуле ОбновлениеИнформационнойБазыБСП.
Процедура ПослеОбновленияИнформационнойБазы(Знач ПредыдущаяВерсия, Знач ТекущаяВерсия,
		Знач ВыполненныеОбработчики, ВыводитьОписаниеОбновлений, МонопольныйРежим) Экспорт
	
КонецПроцедуры

// См. описание в общем модуле ОбновлениеИнформационнойБазыБСП.
Процедура ПриПодготовкеМакетаОписанияОбновлений(Знач Макет) Экспорт
	
КонецПроцедуры

// Добавляет в список процедуры-обработчики перехода с другой программы (с другим именем конфигурации).
// Например, для перехода между разными, но родственными конфигурациями: базовая -> проф -> корп.
// Вызывается перед началом обновления данных ИБ.
//
// Параметры:
//  Обработчики - ТаблицаЗначений - с колонками:
//    * ПредыдущееИмяКонфигурации - Строка - имя конфигурации, с которой выполняется переход;
//    * Процедура                 - Строка - полное имя процедуры-обработчика перехода с программы ПредыдущееИмяКонфигурации. 
//                                  Например, "ОбновлениеИнформационнойБазыУПП.ЗаполнитьУчетнуюПолитику"
//                                  Обязательно должна быть экспортной.
//
// Пример добавления процедуры-обработчика в список:
//  Обработчик = Обработчики.Добавить();
//  Обработчик.ПредыдущееИмяКонфигурации  = "УправлениеТорговлей";
//  Обработчик.Процедура                  = "ОбновлениеИнформационнойБазыУПП.ЗаполнитьУчетнуюПолитику";
//
Процедура ПриДобавленииОбработчиковПереходаСДругойПрограммы(Обработчики) Экспорт
	
	
КонецПроцедуры

// Позволяет переопределить режим обновления данных информационной базы.
// Для использования в редких (нештатных) случаях перехода, не предусмотренных в
// стандартной процедуре определения режима обновления.
//
// Параметры:
//   РежимОбновленияДанных - Строка - в обработчике можно присвоить одно из значений:
//              "НачальноеЗаполнение"     - если это первый запуск пустой базы (области данных);
//              "ОбновлениеВерсии"        - если выполняется первый запуск после обновление конфигурации базы данных;
//              "ПереходСДругойПрограммы" - если выполняется первый запуск после обновление конфигурации базы данных, 
//                                          в которой изменилось имя основной конфигурации.
//
//   СтандартнаяОбработка  - Булево - если присвоить Ложь, то стандартная процедура
//                                    определения режима обновления не выполняется, 
//                                    а используется значение РежимОбновленияДанных.
//
Процедура ПриОпределенииРежимаОбновленияДанных(РежимОбновленияДанных, СтандартнаяОбработка) Экспорт
	
КонецПроцедуры

#КонецОбласти


