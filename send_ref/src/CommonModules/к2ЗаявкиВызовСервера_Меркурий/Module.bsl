#Область ПрограммныйИнтерфейс

// Получает массив заявок, по которым нужно слать подтверждение
//
// Параметры:
//  пПараметрыПодключения - Структура - параметры подключения (см. к2МеркурийВызовСервера.ПараметрыПодключения).
//  пИсточник - Строка - Источник запроса для отслеживания
//  пФильтрПоОбъекту - Ссылка - отбор по конкретному объекту
// 
// Возвращаемое значение:
//  Массив - массив структур с описанием заявки.
//
Функция ЗаявкиДляВыполненияЗапроса(Знач пПараметрыПодключения, Знач пИсточник, Знач пФильтрПоОбъекту = Неопределено) Экспорт

	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 10
				   |	Заявки.ИДЗаявки КАК ИДЗаявки,
				   |	Заявки.БизнесОперация КАК БизнесОперация,
				   |	Заявки.Объект КАК Объект,
				   |	Заявки.Пользователь КАК Пользователь,
				   |	Заявки.Период КАК Период,
				   |	Заявки.ДатаОбновления КАК ДатаОбновления
				   |ИЗ
				   |	РегистрСведений.к2ЗаявкиНаВыполнениеБизнесОперации_Меркурий КАК Заявки
				   |ГДЕ
				   |	Заявки.Обмен = &Обмен
				   |	И Заявки.Статус В(&Статусы)
				   |	И (Заявки.Объект = &Объект ИЛИ Заявки.Объект.ЗаявкаОснование = &Объект)
				   |	И ((ВЫРАЗИТЬ(Заявки.ОписанияОшибок КАК СТРОКА(10))) = """"
				   |			ИЛИ Заявки.ДатаОбновления < &ДатаОбновленияПриОшибке)
				   |
				   |УПОРЯДОЧИТЬ ПО
				   |	ДатаОбновления";
	Запрос.УстановитьПараметр("Обмен", пПараметрыПодключения.Ссылка);
	// Если были проблемы работы с интернетом, то заявка осталась с тем же статусом и уйдет на второй круг.
	// Что приводит к зацикливанию. Отфильтруем такие заявки датой обновления. 
	Запрос.УстановитьПараметр("ДатаОбновленияПриОшибке", ТекущаяДатаСеанса() - 10);

	массивСтатусов = Новый Массив;
	массивСтатусов.Добавить(Перечисления.к2СтатусыОбработкиЗаявки_Меркурий.Принята);
	массивСтатусов.Добавить(Перечисления.к2СтатусыОбработкиЗаявки_Меркурий.Обрабатывается);

	Запрос.УстановитьПараметр("Статусы", массивСтатусов);

	Если пФильтрПоОбъекту = Неопределено Тогда

		Запрос.Текст = СтрЗаменить(Запрос.Текст, "Заявки.Объект = &Объект ИЛИ Заявки.Объект.ЗаявкаОснование = &Объект", "Истина");

	Иначе

		Запрос.УстановитьПараметр("Объект", пФильтрПоОбъекту);

	КонецЕсли;

	выборка = Запрос.Выполнить().Выбрать();

	массивЗаявок = Новый Массив;

	Пока выборка.Следующий() Цикл

		структЗаявка = Новый Структура;

		структЗаявка.Вставить("ИДЗаявки", выборка.ИДЗаявки);
		структЗаявка.Вставить("Период", выборка.Период);
		структЗаявка.Вставить("БизнесОперация", выборка.БизнесОперация);
		структЗаявка.Вставить("Объект", выборка.Объект);
		структЗаявка.Вставить("Пользователь", выборка.Пользователь);
		структЗаявка.Вставить("Результат");
		структЗаявка.Вставить("СтруктураОтвета");

		запрос = к2МеркурийВызовСервера.Запрос_ПодтверждениеЗаявки(выборка.ИДЗаявки, пПараметрыПодключения, пИсточник);

		запрос.Вставить("Объект", структЗаявка.Объект);

		структЗаявка.Вставить("Запрос", запрос);

		массивЗаявок.Добавить(структЗаявка);

	КонецЦикла;

	Возврат массивЗаявок;

КонецФункции

// Фиксирует ответ по заявке в системе
//
// Параметры:
//  пДанныеЗаявки - Структура  - данные заявки
//  пПараметрыПодключения - Структура - параметры подключения (см. к2МеркурийВызовСервера.ПараметрыПодключения)
//
Процедура ЗаписатьОтветПоЗаявке(Знач пДанныеЗаявки, Знач пПараметрыПодключения) Экспорт

	НачатьТранзакцию();

	Попытка

		к2Заявки_Меркурий.ЗаблокироватьЗаявку(пДанныеЗаявки.Объект);

		к2МеркурийВызовСервера.ОбработатьРезультатЗапроса(пДанныеЗаявки.Результат, пДанныеЗаявки.Запрос);

		Если пДанныеЗаявки.Результат.Успех Тогда

			РегистрыСведений.к2ЗаявкиНаВыполнениеБизнесОперации_Меркурий.ИзменитьСтатус(пДанныеЗаявки,
																						Перечисления.к2СтатусыОбработкиЗаявки_Меркурий.ПолученОтвет,
																						пПараметрыПодключения);

		Иначе

			РегистрыСведений.к2ЗаявкиНаВыполнениеБизнесОперации_Меркурий.ИзменитьСтатус(пДанныеЗаявки, Неопределено, пПараметрыПодключения);

		КонецЕсли;

		ЗафиксироватьТранзакцию();

	Исключение

		ОтменитьТранзакцию();

		ЗаписьЖурналаРегистрации(к2МеркурийСервер.СобытиеЖурналаРегистрации(),
								 УровеньЖурналаРегистрации.Ошибка,
								 ,
								 ,
								 ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));

	КонецПопытки;

КонецПроцедуры

// Обрабатывает ответы заявок от шлюза, устанавливая соответствующий статус
//
// Параметры:
//  пПараметрыПодключения - Структура - параметры подключения (см. к2МеркурийВызовСервера.ПараметрыПодключения).
//  пФильтрПоОбъекту - Ссылка - отбор по конкретному объекту
//
// Возвращаемое значение:
//  Массив - массив ссылок обновленных объектов.
//
Функция ОбработатьЗаявки(Знач пПараметрыПодключения, Знач пФильтрПоОбъекту = Неопределено) Экспорт

	Возврат к2Заявки_Меркурий.ОбработатьЗаявкиВ1С(пПараметрыПодключения, пФильтрПоОбъекту);

КонецФункции

#КонецОбласти