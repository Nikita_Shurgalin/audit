
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает ссылку по структуре описания
//
// Параметры:
//  пСтруктура	 - Структура - структура описания
//  пСоздаватьЭлемент - Булево - создавать ли элемент в случае отсутствия
//  пНаселенныйПункт - СправочникСсылка.к2КлассификаторНаселенныхПунктов_Меркурий - владелец.
//
// Возвращаемое значение:
//  СправочникСсылка.к2КлассификаторУлиц_Меркурий - Пустая ссылка, если передана пустая структура.
//
Функция ПолучитьПоОписанию( Знач пСтруктура,
							Знач пСоздаватьЭлемент = Истина,
							Знач пНаселенныйПункт = Неопределено ) Экспорт
	
	Если пСтруктура = Неопределено Тогда
		
		Возврат Справочники.к2КлассификаторУлиц_Меркурий.ПустаяСсылка();
		
	КонецЕсли;
	
	ссылка = глЗначениеСтруктуры( пСтруктура, "Ссылка" );
	
	Если ЗначениеЗаполнено( ссылка ) Тогда
		
		Возврат ссылка;
		
	КонецЕсли;
	
	гуид = глЗначениеСтруктуры( пСтруктура, "ГУИД" );
	
	Если Не ЗначениеЗаполнено( гуид ) Тогда
		
		Возврат Справочники.к2КлассификаторУлиц_Меркурий.ПустаяСсылка();
		
	КонецЕсли;
	
	ссылка = к2МеркурийВызовСервера.ЗначениеПоГуид_Кэш(
		гуид,
		"к2КлассификаторУлиц_Меркурий",
		пСтруктура.УУИД, Не пСоздаватьЭлемент );
	
	Если ЗначениеЗаполнено( ссылка )
		И пСоздаватьЭлемент
		И ЗначениеЗаполнено( пНаселенныйПункт )
		И Не ссылка.Владелец = пНаселенныйПункт Тогда
		
		НачатьТранзакцию();
		Попытка
			Блокировка        = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить( ссылка.Метаданные().ПолноеИмя() );
			ЭлементБлокировки.УстановитьЗначение( "Ссылка", ссылка );
			Блокировка.Заблокировать();
			
			об = ссылка.ПолучитьОбъект();
			об.Заблокировать();
			об.Владелец = пНаселенныйПункт;
			об.ДополнительныеСвойства.Вставить( "ЗагрузкаИзСервиса", Истина );
			об.Записать();
			
			ЗафиксироватьТранзакцию();
		Исключение
			ОтменитьТранзакцию();
			ВызватьИсключение;
		КонецПопытки;
		
	КонецЕсли;
	
	Возврат ссылка;
	
КонецФункции

// Заполняет описания вложенных объектов
//
// Параметры:
//  пОписание - Структура - текущее описание
//  пПараметрыОбновления - Структура - Структура сопроводительных данных.
Процедура ЗаполнитьОписанияВложенныхОбъектов( Знач пОписание, пПараметрыОбновления ) Экспорт
	
	Если пОписание.Свойство( "НаселенныйПункт" )
		И ЗначениеЗаполнено( пОписание.НаселенныйПункт.ГУИД ) Тогда
		
		к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
			Метаданные.Справочники.к2КлассификаторНаселенныхПунктов_Меркурий.Имя,
			пОписание.НаселенныйПункт,
			пПараметрыОбновления );
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
