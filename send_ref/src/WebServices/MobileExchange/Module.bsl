
Функция GetSettings(MobileDeviceID)
	Возврат к2МобильноеПриложение.ВыгрузитьНастройки(MobileDeviceID);	
КонецФункции

Функция StartExchange(MobileDeviceID)
	Возврат к2МобильноеПриложение.НачалоОбмена(MobileDeviceID);
КонецФункции

Функция GetJobTargets(MobileDeviceID)
	Возврат к2МобильноеПриложение.ВыгрузитьЗадания(MobileDeviceID);
КонецФункции

Функция GetCatalogs(MobileDeviceID)
	
	Возврат к2МобильноеПриложение.ВыгрузитьСправочники(MobileDeviceID);
	
КонецФункции

Функция FillJobTargets(MobileDeviceID, MassegeExchange)
	Возврат к2МобильноеПриложение.ЗагрузитьЗадания(MobileDeviceID, MassegeExchange);
КонецФункции

Функция GetCustomers(MobileDeviceID, AllCustomers = Ложь, Customer = "", Address = "")
	
	Возврат к2МобильноеПриложение.ВыгрузитьКлиентов(MobileDeviceID, AllCustomers, Customer, Address);
	
КонецФункции

Функция GetCustomersSales(Customer = "", DateBegin, DateEnd)
	Возврат к2МобильноеПриложение.ВыгрузитьОтчетОтгрузки(Customer, DateBegin, DateEnd);
КонецФункции
