
#Область ОписаниеПеременных

&НаКлиенте
Перем ВыполняетсяЗакрытие;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.УникальныйИдентификатор = Неопределено Тогда
		ВызватьИсключение НСтр("ru='Предусмотрено открытие обработки только из форм объектов.'");
	КонецЕсли;
	
	ОбработатьПереданныеПараметры(Параметры);
	
	ИнициализироватьКомпоновщикСервер(Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если НЕ ВыполняетсяЗакрытие И Не ПеренестиВДокумент И Контрагенты.Количество() > 0 Тогда
		
		ТекстВопроса = НСтр("ru = 'Подобранные контрагенты не перенесены в документ. Перенести?'");
		
		Отказ = Истина;
		
		ПоказатьВопрос(
			Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект), 
			ТекстВопроса, 
			РежимДиалогаВопрос.ДаНетОтмена);
		Возврат;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Нет Тогда
		ВыполняетсяЗакрытие = Истина;
		Закрыть();
	ИначеЕсли РезультатВопроса = КодВозвратаДиалога.Да Тогда
		ВыполняетсяЗакрытие = Истина;
		ПеренестиВДокумент = Истина;
		АдресКонтрагентовВХранилище = ПоместитьВоВременноеХранилищеНаСервере();
		Закрыть(АдресКонтрагентовВХранилище);
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыКонтрагенты

&НаКлиенте
Процедура КонтрагентыКонтрагентПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Контрагенты.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.ГруппаПланирования = глРеквизит(ТекущиеДанные.Контрагент, "ГруппаПланирования");	
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПеренестиВДокумент(Команда)
	
	ПеренестиВДокумент = Истина;
	АдресВХранилище = ПоместитьВоВременноеХранилищеНаСервере();
	Закрыть(АдресВХранилище);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьТаблицуКонтрагентов(Команда)
	
	ТекстВопроса = НСтр("ru = 'При перезаполнении все введенные вручную данные будут потеряны, продолжить?'");
	
	Если Контрагенты.Количество() > 0 Тогда
		ПоказатьВопрос(
			Новый ОписаниеОповещения("ЗаполнитьТаблицуКонтрагентовЗавершение", ЭтотОбъект),
			ТекстВопроса, РежимДиалогаВопрос.ДаНет,,КодВозвратаДиалога.Да);
	Иначе
		ЗаполнитьТаблицуКонтрагентовНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьТаблицуКонтрагентовЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда
		ЗаполнитьТаблицуКонтрагентовНаСервере();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ИнициализироватьКомпоновщикСервер(НастройкаКомпоновки)
	
	СхемаКомпоновки = Обработки.к2ПодборКонтрагентовПоОтбору.ПолучитьМакет("ОтборКонтрагентов");
	АдресСхемы = ПоместитьВоВременноеХранилище(СхемаКомпоновки,УникальныйИдентификатор);
	КомпоновщикНастроек.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(АдресСхемы));
	
	Если НастройкаКомпоновки = Неопределено Тогда
		КомпоновщикНастроек.ЗагрузитьНастройки(СхемаКомпоновки.НастройкиПоУмолчанию);
	Иначе
		КомпоновщикНастроек.ЗагрузитьНастройки(НастройкаКомпоновки);
		КомпоновщикНастроек.Восстановить(СпособВосстановленияНастроекКомпоновкиДанных.ПроверятьДоступность);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТаблицуКонтрагентовНаСервере(ПроверятьЗаполнение = Истина)
	
	СхемаОтбора = Обработки.к2ПодборКонтрагентовПоОтбору.ПолучитьМакет("ОтборКонтрагентов");
	
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных();
	КомпоновщикНастроекРасчета = Новый КомпоновщикНастроекКомпоновкиДанных;
	КомпоновщикНастроекРасчета.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(СхемаОтбора));
	КомпоновщикНастроекРасчета.ЗагрузитьНастройки(КомпоновщикНастроек.Настройки);
	
	МакетКомпоновкиДанных = КомпоновщикМакета.Выполнить(СхемаОтбора, КомпоновщикНастроекРасчета.ПолучитьНастройки(),,,
		Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
	
	// Инициализация процессора компоновки
	ПроцессорКомпоновкиДанных = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновкиДанных.Инициализировать(МакетКомпоновкиДанных);
	
	Контрагенты.Очистить();
	ТаблицаКонтрагентов = РеквизитФормыВЗначение("Контрагенты");
	
	// Получение результата
	ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений =
		Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
	ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений.УстановитьОбъект(ТаблицаКонтрагентов);
	ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений.Вывести(ПроцессорКомпоновкиДанных);
	
	ЗначениеВРеквизитФормы(ТаблицаКонтрагентов, "Контрагенты");
	
	КоличествоПодобранныхКонтрагентов = Контрагенты.Количество();
	
КонецПроцедуры

&НаСервере
Функция ПоместитьВоВременноеХранилищеНаСервере()
	
	Возврат ПоместитьВоВременноеХранилище(Контрагенты.Выгрузить(), ИдентификаторВызывающейФормы);
	
КонецФункции

&НаСервере
Процедура ОбработатьПереданныеПараметры(Параметры)

	ИдентификаторВызывающейФормы = Параметры.УникальныйИдентификатор;
	
	Если ЗначениеЗаполнено(Параметры.Заголовок) Тогда
		Заголовок = Параметры.Заголовок;
	Иначе
		Заголовок = НСтр("ru = 'Подбор контрагентов по отбору'");
	КонецЕсли;
							  
	Если ЗначениеЗаполнено(Параметры.ЗаголовокКнопкиПеренести) Тогда
		Команды["ПеренестиВДокумент"].Заголовок = Параметры.ЗаголовокКнопкиПеренести;
		Команды["ПеренестиВДокумент"].Подсказка = Параметры.ЗаголовокКнопкиПеренести;
	КонецЕсли;

КонецПроцедуры

#КонецОбласти


#Область Инициализация

ВыполняетсяЗакрытие = Ложь;

#КонецОбласти
