#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПриКопировании(ОбъектКопирования)
	
	СпособПолученияЗначения = Перечисления.к2СпособыПолученияЗначенияИнтернетВещей.Последнее;
	ВидПересчета = Перечисления.к2ВидыПересчетовДанныхИнтернетВещей.НеПересчитывать;
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если ТипДанных = Перечисления.к2ТипыДанныхИнтернетВещей.Производство Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("ПоказательПроцесса");
		МассивНепроверяемыхРеквизитов.Добавить("ПараметрСреды");
		МассивНепроверяемыхРеквизитов.Добавить("МестоХранения");
		
	ИначеЕсли ТипДанных = Перечисления.к2ТипыДанныхИнтернетВещей.ПоказателиТехнологическогоПроцесса Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("Маршрут");
		МассивНепроверяемыхРеквизитов.Добавить("Операция");
		МассивНепроверяемыхРеквизитов.Добавить("ПараметрСреды");
		МассивНепроверяемыхРеквизитов.Добавить("МестоХранения");
		
	ИначеЕсли ТипДанных = Перечисления.к2ТипыДанныхИнтернетВещей.ПараметрыПроизводственнойСреды Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("Маршрут");
		МассивНепроверяемыхРеквизитов.Добавить("Операция");
		МассивНепроверяемыхРеквизитов.Добавить("ПоказательПроцесса");
		
	Иначе
		
		к2ОбщегоНазначенияКлиентСервер.ВызватьИсключениеПоСсылке(ТипДанных);
		
	КонецЕсли;
	
	Если Не ВидПересчета = Перечисления.к2ВидыПересчетовДанныхИнтернетВещей.ЛитрыВКилограммы Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("ПоказательПлотности");
		
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
