
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьУсловноеОформление();
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
	Прочитать();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСоответствияДанных

&НаКлиенте
Процедура СоответствияДанныхНоменклатураПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.СоответствияДанных.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьХарактеристикуПоВладельцу", ТекущиеДанные.Характеристика);
	СтруктураДействий.Вставить("ПроверитьСериюПоВидуНоменклатуры", ТекущиеДанные.Серия);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаУстановитьСледующийСтатус(Команда)
	
	УстановитьСледующийСтатусНаСервере();
	
КонецПроцедуры

#Область СтандартныеПодсистемыПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	к2УО.Характеристика(ЭтотОбъект
		, "СоответствияДанныхХарактеристика", "Объект.СоответствияДанных.ХарактеристикиИспользуются");
	
	к2УО.Серия(ЭтотОбъект
		, "СоответствияДанныхСерия", "Объект.СоответствияДанных.СерииИспользуются");
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ЗаполнитьСлужебныеРеквизиты();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизиты()
	
	СтруктураХарактеристикиИспользуются = Новый Структура("Номенклатура", "ХарактеристикиИспользуются");
	СтруктураСерииИспользуются = Новый Структура("Номенклатура", "СерииИспользуются");
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются", СтруктураХарактеристикиИспользуются);
	СтруктураДействий.Вставить("ЗаполнитьПризнакСерииИспользуются", СтруктураСерииИспользуются);
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.СоответствияДанных, СтруктураДействий);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	ТолькоПросмотрДанных = Не Объект.Ссылка.Статус = Перечисления.к2СтатусыПодготовкиГИСМ.ПустаяСсылка();
	
	Элементы.ГруппаШапка.ТолькоПросмотр = ТолькоПросмотрДанных;
	Элементы.СоответствияДанных.ТолькоПросмотр = ТолькоПросмотрДанных;
	Элементы.ГруппаОбъектыГИСМ.ТолькоПросмотр = ТолькоПросмотрДанных;
	
	Элементы.КомандаУстановитьСледующийСтатус.Видимость = Не Объект.Ссылка.Статус = Перечисления.к2СтатусыПодготовкиГИСМ.ЗавершенОбменГИСМ;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСледующийСтатусНаСервере()
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	
	Если Объект.Ссылка.Статус = Перечисления.к2СтатусыПодготовкиГИСМ.ПустаяСсылка() Тогда
		
		Таблица = Документы.к2ПодготовкаИсходящихПартийГИСМ.УчетныеДанныеПоРаспоряжениюНаОтгрузку(ДокументОбъект.РаспоряжениеНаОтгрузку);
		
		ДокументОбъект.ЗаполнитьПоУчету(Таблица);
		
	ИначеЕсли Объект.Ссылка.Статус = Перечисления.к2СтатусыПодготовкиГИСМ.ЗаполненПоУчету Тогда
		
		ДокументОбъект.УстановитьСоответствие();
		
	ИначеЕсли Объект.Ссылка.Статус = Перечисления.к2СтатусыПодготовкиГИСМ.УстановленоСоответствие Тогда
		
		ДокументОбъект.СоздатьОбъектыГИСМ();
		
	ИначеЕсли Объект.Ссылка.Статус = Перечисления.к2СтатусыПодготовкиГИСМ.СозданыОбъектыГИСМ Тогда
		
		ДокументОбъект.ЗавершитьПодготовку();
		
	КонецЕсли;
	
	ЗначениеВРеквизитФормы(ДокументОбъект, "Объект");
	
	Прочитать();
	
КонецПроцедуры

#КонецОбласти