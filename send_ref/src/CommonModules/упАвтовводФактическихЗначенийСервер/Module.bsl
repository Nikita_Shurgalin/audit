#Область ПрограммныйИнтерфейс

// Создает запись в журнале регистрации и сообщениях пользователю, 
//  Поддерживает до 4х параметров в комментарии при помощи функции 
//    СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку
//  Поддерживает передачу информации об обшибке, подробное представление 
//    ошибки добавляется в комментарий записи в журнал регистрации.
// 
// Параметры:
//   ПараметрыЖурнала (Структура) - Параметры записи в журнал регистрации
//       |- Префикс (Строка) - Префикс для имени события журнала регистрации
//       |- Метаданные (ОбъектМетаданных) - Метаданные для записи в журнал регистрации
//       |- Данные (*)       - Данные для записи в журнал регистрации
//   Уровень (Число(0..4))  - Соответствует уровням журнала регистрации
//   Подкласс (Число(0..4)) - Суффикс для имени события журнала регистрации
//   КомментарийСПараметрами (Строка) - Комментарий, возможно с параметрами %1 .. %4
//   ИнформацияОбОшибке (ИнформацияОбОшибке, Строка) - Информация об ошибке, которую так же необходимо
//                                                     задокументировать в комментарии журнала регистрации
//   Параметр1 .. Параметр4 (*) - Параметры для подстановки в комментарий.
//
Процедура ЗаписьЖурнала(ПараметрыЖурнала, УровеньЖурнала = Неопределено, ИмяСобытия = "", 
		Знач КомментарийСПараметрами = "", ИнформацияОбОшибке = Неопределено, 
		Параметр1 = Неопределено, 
		Параметр2 = Неопределено, 
		Параметр3 = Неопределено, 
		Параметр4 = Неопределено
	) Экспорт
	
	// Определение уровня журнала регистрации на основе типа переданного сообщения об ошибке.
	Если ТипЗнч(УровеньЖурнала) <> Тип("УровеньЖурналаРегистрации") Тогда
		Если ТипЗнч(ИнформацияОбОшибке) = Тип("Строка") Тогда
			УровеньЖурнала = УровеньЖурналаРегистрации.Предупреждение;
		ИначеЕсли ТипЗнч(ИнформацияОбОшибке) = Тип("ИнформацияОбОшибке") Тогда
			УровеньЖурнала = УровеньЖурналаРегистрации.Ошибка;
		Иначе
			УровеньЖурнала = УровеньЖурналаРегистрации.Информация;
		КонецЕсли;
	КонецЕсли;
	
	// Комментарий для журнала регистрации и пользователя
	Если Параметр1 <> Неопределено Тогда
		КомментарийСПараметрами = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			КомментарийСПараметрами, Параметр1, Параметр2, Параметр3, Параметр4
		);
	КонецЕсли;
	
	Если ТипЗнч(ИнформацияОбОшибке) = Тип("ИнформацияОбОшибке") Тогда
		Если КомментарийСПараметрами = "" Тогда
			ТестСообщенияПользователю = КраткоеПредставлениеОшибки(ИнформацияОбОшибке);
			КомментарийСПараметрами = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке);
		Иначе
			ТестСообщенияПользователю = КомментарийСПараметрами + Символы.ПС + КраткоеПредставлениеОшибки(ИнформацияОбОшибке);
			КомментарийСПараметрами = КомментарийСПараметрами + Символы.ПС + ПодробноеПредставлениеОшибки(ИнформацияОбОшибке);
		КонецЕсли;
	Иначе
		Если ТипЗнч(ИнформацияОбОшибке) = Тип("Строка") И ИнформацияОбОшибке <> "" Тогда
			КомментарийСПараметрами = КомментарийСПараметрами + Символы.ПС + ИнформацияОбОшибке;
		КонецЕсли;
		ТестСообщенияПользователю = КомментарийСПараметрами;
	КонецЕсли;
	
	// Журнал регистрации 
	УстановитьПривилегированныйРежим(Истина);
	ЗаписьЖурналаРегистрации(
		ПараметрыЖурнала.ГруппаСобытий + ?(ИмяСобытия = "", "", ". "+ ИмяСобытия), 
		УровеньЖурнала, 
		ПараметрыЖурнала.Метаданные, 
		ПараметрыЖурнала.Данные, 
		КомментарийСПараметрами
	);
	УстановитьПривилегированныйРежим(Ложь);
	
	Если УровеньЖурнала = УровеньЖурналаРегистрации.Ошибка ИЛИ УровеньЖурнала = УровеньЖурналаРегистрации.Предупреждение Тогда
		Если УровеньЖурнала = УровеньЖурналаРегистрации.Ошибка Тогда
			ПараметрыЖурнала.Вставить("БылиОшибки", Истина);
		Иначе
			ПараметрыЖурнала.Вставить("БылиПредупреждения", Истина);
		КонецЕсли;
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = СокрЛП(ТестСообщенияПользователю); //  + Символы.ПС + Символы.ПС + НСтр("ru = 'Подробности см. в журнале регистрации.'")
		Сообщение.УстановитьДанные(ПараметрыЖурнала.Данные);
		Сообщение.Сообщить();
	КонецЕсли;
	
КонецПроцедуры

Функция ПолучитьXML(Значение) Экспорт
	
	Запись = Новый ЗаписьXML();
	Запись.УстановитьСтроку();
	СериализаторXDTO.ЗаписатьXML(Запись, Значение);
	Возврат Запись.Закрыть();
	
КонецФункции


////////////////////////////////////////////////////////////////////////////////
// РЕГЛАМЕНТНОЕ ЗАДАНИЕ

Процедура упАвтовводФактическихЗначений(пСправочник) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если Не ЗначениеЗаполнено(пСправочник) Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыЖурнала = Новый Структура("ГруппаСобытий, Метаданные, Данные");
	ПараметрыЖурнала.ГруппаСобытий = НСтр("ru = 'Формирование автоввода фактических значений. Запуск по расписанию'");
	ПараметрыЖурнала.Метаданные = пСправочник.Метаданные();
	ПараметрыЖурнала.Данные = пСправочник;
	
	ЗаписьЖурнала(ПараметрыЖурнала, УровеньЖурналаРегистрации.Примечание, , НСтр("ru='Запуск';en='Start'"));
	
	// Проверки
	Если глРеквизит(пСправочник, "ПометкаУдаления") Тогда
		ЗаписьЖурнала(ПараметрыЖурнала, , ,
			НСтр("ru='Завершение';en='End'"), НСтр("ru = 'Автоввод фактических значений помечен на удаление'"));
		Возврат;
	КонецЕсли;
	
	Попытка
		упАвтовводФактическихЗначенийВызовСервера.Сформировать(пСправочник);
	Исключение
		ЗаписьЖурнала(ПараметрыЖурнала, , ,
			НСтр("ru = 'Ошибка формирования автоввода фактических значений %1'"), ИнформацияОбОшибке(), 
			"'"+ глРеквизит(пСправочник, "Наименование") +"'");
	КонецПопытки;
	
	ЗаписьЖурнала(ПараметрыЖурнала, УровеньЖурналаРегистрации.Примечание, ,НСтр("ru='Завершение';en='End'"));
	
КонецПроцедуры

#КонецОбласти
