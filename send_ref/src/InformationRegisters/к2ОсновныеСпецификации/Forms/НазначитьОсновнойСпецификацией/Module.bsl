
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Параметры.Свойство("Спецификация", Спецификация);
	
	ЗаполнитьНастройкуИзделий();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписокНоменклатуры

&НаКлиенте
Процедура СписокНоменклатурыПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Отказ = Истина;
	ДобавитьХарактеристикиВСписок();
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНоменклатурыПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	УдалитьХарактеристикиИзСписка();
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНоменклатурыОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	СтрокаИзделия = Элементы.СписокНоменклатуры.ТекущиеДанные;
	Если СтрокаИзделия = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если СтрокаИзделия.ТипСтроки = 1 Тогда
		СтрокаИзделия = СтрокаИзделия.ПолучитьРодителя();
	КонецЕсли;
	
	КоллекцияХарактеристик = СтрокаИзделия.ПолучитьЭлементы();
	
	// Проверим наличие выбранной характеристики
	Для каждого ЭлементКоллекции Из КоллекцияХарактеристик Цикл
		Если ЭлементКоллекции.Характеристика = ВыбранноеЗначение Тогда
			Возврат;
		КонецЕсли;
	КонецЦикла;
	
	НоваяХарактеристика = КоллекцияХарактеристик.Добавить();
	НоваяХарактеристика.Характеристика = ВыбранноеЗначение;
	НоваяХарактеристика.ТипСтроки = 1;
	
	Если КоллекцияХарактеристик.Количество() = 0 Тогда
		СтрокаИзделия.ЛюбыеХарактеристики = Истина;
	Иначе
		СтрокаИзделия.ЛюбыеХарактеристики = Ложь;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ДобавитьХарактеристики(Команда)
	
	ДобавитьХарактеристикиВСписок();
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьХарактеристики(Команда)
	
	УдалитьХарактеристикиИзСписка();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)

	Если ЗаписатьНаСервере() Тогда
		
		к2УправлениеДаннымиОбИзделияхКлиент.ОповеститьОЗаписиОсновнойСпецификации();
		
		Закрыть();
				
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.СписокНоменклатурыПометка.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ТипСтроки");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = 1;

	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Истина);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.СписокНоменклатурыНоменклатура.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ТипСтроки");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = 1;

	Элемент.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.СписокНоменклатуры.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ТипСтроки");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = 1;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ПоясняющийТекст);
	Элемент.Оформление.УстановитьЗначениеПараметра("Шрифт", Новый Шрифт(WindowsШрифты.DefaultGUIFont, , , Ложь, Истина, Ложь, Ложь, ));

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.СписокНоменклатурыХарактеристика.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.Характеристика");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеЗаполнено;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ХарактеристикиИспользуются");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ЛюбыеХарактеристики");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<для любых характеристик>'"));

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.СписокНоменклатурыХарактеристика.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.Характеристика");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеЗаполнено;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ХарактеристикиИспользуются");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("СписокНоменклатуры.ЛюбыеХарактеристики");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;

	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<для выбранных характеристик>'"));

КонецПроцедуры

#Область НастройкаИзделий

&НаСервере
Процедура ЗаполнитьНастройкуИзделий()

	ХарактеристикиИспользуются = Ложь;
	
	// Получим изделия спецификации и добавим их в список выбора 
	Запрос = Новый Запрос;
	
	Если ЗначениеЗаполнено(Параметры.Номенклатура) И ЗначениеЗаполнено(Параметры.Характеристика) Тогда
		
		Запрос.Текст =
		"ВЫБРАТЬ
		|	&Номенклатура КАК Номенклатура,
		|	&Характеристика КАК Характеристика,
		|	&ИспользоватьХарактеристики КАК ХарактеристикиИспользуются";
		
		ИспользоватьХарактеристики = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Параметры.Номенклатура, "ВидНоменклатуры.ИспользоватьХарактеристики");
		
		Запрос.УстановитьПараметр("Номенклатура", Параметры.Номенклатура);
		Запрос.УстановитьПараметр("Характеристика", Параметры.Характеристика);
		Запрос.УстановитьПараметр("ИспользоватьХарактеристики", ИспользоватьХарактеристики);
		
	Иначе
		
		Запрос.Текст =
		"ВЫБРАТЬ
		|	РесурсныеСпецификацииВыходныеИзделия.Номенклатура КАК Номенклатура,
		|	РесурсныеСпецификацииВыходныеИзделия.Характеристика КАК Характеристика,
		|	РесурсныеСпецификацииВыходныеИзделия.Номенклатура.ВидНоменклатуры.ИспользоватьХарактеристики КАК ХарактеристикиИспользуются
		|ИЗ
		|	Справочник.к2РесурсныеСпецификации.ВыходныеИзделия КАК РесурсныеСпецификацииВыходныеИзделия
		|ГДЕ
		|	РесурсныеСпецификацииВыходныеИзделия.Ссылка = &Спецификация
		|
		|УПОРЯДОЧИТЬ ПО
		|	РесурсныеСпецификацииВыходныеИзделия.НомерСтроки";
		
		Запрос.УстановитьПараметр("Спецификация", Спецификация);
		
	КонецЕсли;
	
	Результат = Запрос.Выполнить();
	
	ВыборкаНоменклатуры = Результат.Выбрать();
	
	КоллекцияНоменклатуры = СписокНоменклатуры.ПолучитьЭлементы();
	Пока ВыборкаНоменклатуры.Следующий() Цикл
		
		НоваяНоменклатура = КоллекцияНоменклатуры.Добавить();
		НоваяНоменклатура.Номенклатура   = ВыборкаНоменклатуры.Номенклатура;
		НоваяНоменклатура.Характеристика = ВыборкаНоменклатуры.Характеристика;
		НоваяНоменклатура.ХарактеристикиИспользуются = ВыборкаНоменклатуры.ХарактеристикиИспользуются;
		НоваяНоменклатура.ЛюбыеХарактеристики = Истина;
		НоваяНоменклатура.Пометка = Истина;
		
		ХарактеристикиИспользуются = ХарактеристикиИспользуются Или НоваяНоменклатура.ХарактеристикиИспользуются;
		
	КонецЦикла;

	Если КоллекцияНоменклатуры.Количество() = 1 Тогда
		Элементы.СписокНоменклатурыПометка.Видимость = Ложь;
	КонецЕсли;
	
	// Если не используются характеристики то уберем колонку "Характеристика"
	Если Не ХарактеристикиИспользуются Тогда
		Элементы.СписокНоменклатурыХарактеристика.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

// Открывает форму выбора характеристики и добавляет выбранные в список
//
&НаКлиенте
Процедура ДобавитьХарактеристикиВСписок()

	СтрокаИзделия = Элементы.СписокНоменклатуры.ТекущиеДанные;
	Если СтрокаИзделия = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если СтрокаИзделия.ТипСтроки = 1 Тогда
		СтрокаИзделия = СтрокаИзделия.ПолучитьРодителя();
	КонецЕсли;
	
	Если НЕ СтрокаИзделия.Характеристика.Пустая() Тогда
		ПоказатьПредупреждение(Неопределено, НСтр("ru = 'Характеристика определена в спецификации.'"));
		Возврат;
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Номенклатура", СтрокаИзделия.Номенклатура);
	ПараметрыФормы.Вставить("ЗакрыватьПриВыборе", Ложь);
	
	ОткрытьФорму("Справочник.ХарактеристикиНоменклатуры.ФормаВыбора",
		ПараметрыФормы, Элементы.СписокНоменклатуры, , , , Неопределено, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьХарактеристикиИзСписка()

	ТекущиеДанные = Элементы.СписокНоменклатуры.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено ИЛИ ТекущиеДанные.ТипСтроки = 0 Тогда
		Возврат;
	КонецЕсли;
	
	СтрокаИзделия = ТекущиеДанные.ПолучитьРодителя();
	
	КоллекцияХарактеристик = СтрокаИзделия.ПолучитьЭлементы();
	КоллекцияХарактеристик.Удалить(ТекущиеДанные);
	
	Если КоллекцияХарактеристик.Количество() = 0 Тогда
		СтрокаИзделия.ЛюбыеХарактеристики = Истина;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ЗаписьНастройки

&НаСервере
Функция ЗаписатьНаСервере()
	
	ЕстьОшибки = Ложь;
	
	НачатьТранзакцию();
	
	Попытка
		
		НазначитьОсновнойДляПомеченныхИзделий(ЕстьОшибки);
		РассчитатьПлановуюСебестоимость(ЕстьОшибки);
		
		Если ЕстьОшибки Тогда
			ВызватьИсключение ТекстИсключенияЗаписи();
		КонецЕсли;
		
		ЗафиксироватьТранзакцию();
		
	Исключение
		
		ОтменитьТранзакцию();
		
		ЕстьОшибки = Истина;
		
		ЗарегистрироватьОшибкуВЖурналеРегистрации(ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
		
	КонецПопытки;
	
	Возврат Не ЕстьОшибки;
	
КонецФункции

&НаСервере
Процедура НазначитьОсновнойДляПомеченныхИзделий(ЕстьОшибки)
	
	КоллекцияНоменклатуры = СписокНоменклатуры.ПолучитьЭлементы();
	
	Для Каждого СтрокаИзделия Из КоллекцияНоменклатуры Цикл
		
		Если Не СтрокаИзделия.Пометка Тогда
			Продолжить;
		КонецЕсли;
		
		Если СтрокаИзделия.ЛюбыеХарактеристики Тогда
			
			ДобавитьНастройкуДляИзделия(СтрокаИзделия.Номенклатура, СтрокаИзделия.Характеристика, ЕстьОшибки);
			
		Иначе
			
			КоллекцияХарактеристик = СтрокаИзделия.ПолучитьЭлементы();
			
			Для Каждого СтрокаХарактеристики Из КоллекцияХарактеристик Цикл
				ДобавитьНастройкуДляИзделия(СтрокаИзделия.Номенклатура, СтрокаХарактеристики.Характеристика, ЕстьОшибки);
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьНастройкуДляИзделия(Номенклатура, Характеристика, ЕстьОшибки)

	СвойстваЗаписи = Новый Структура;
	СвойстваЗаписи.Вставить("Номенклатура",   Номенклатура);
	СвойстваЗаписи.Вставить("Характеристика", Характеристика);
	СвойстваЗаписи.Вставить("Спецификация",   Спецификация);
	
	Если Не к2УправлениеДаннымиОбИзделияхВызовСервера.НазначитьОсновнойСпецификацией(СвойстваЗаписи) Тогда
		ЕстьОшибки = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура РассчитатьПлановуюСебестоимость(ЕстьОшибки)
	
	Если ЕстьОшибки Тогда
		Возврат;
	КонецЕсли;
	
	Справочники.к2РесурсныеСпецификации.РассчитатьПлановуюСебестоимость(Спецификация, ЕстьОшибки);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиентеНаСервереБезКонтекста
Функция ТекстИсключенияЗаписи()
	
	Возврат НСтр("ru = 'Не удалось назначить основной спецификацией'");
	
КонецФункции

&НаСервере
Процедура ЗарегистрироватьОшибкуВЖурналеРегистрации(ТекстОшибки)
	
	ЗаписьЖурналаРегистрации(
		НаименованиеОперации(),
		УровеньЖурналаРегистрации.Ошибка,
		,
		,
		ТекстОшибки);
		
	ОбщегоНазначения.СообщитьПользователю( ТекстОшибки );
	
КонецПроцедуры

&НаСервере
Функция НаименованиеОперации()
	
	ШаблонНаименованияОперации = НСтр("ru = 'Назначение основной спецификации %1'");
	НаименованиеОперации = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		ШаблонНаименованияОперации, Спецификация);
	
	Возврат НаименованиеОперации;
	
КонецФункции

#КонецОбласти

#КонецОбласти
