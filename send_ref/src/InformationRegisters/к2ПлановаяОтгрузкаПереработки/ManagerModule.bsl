#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(Структура) Экспорт 
	
	мз = СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(мз, Структура);
	мз.Записать();
	
КонецПроцедуры	

#КонецОбласти

#КонецЕсли
