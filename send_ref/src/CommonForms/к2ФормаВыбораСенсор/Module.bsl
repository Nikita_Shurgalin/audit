#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("ТаблицаСписка") Тогда
		
		Список.ОсновнаяТаблица = Параметры.ТаблицаСписка;
		
		Если СтрНачинаетсяС(Список.ОсновнаяТаблица, "Документ") Тогда
			
			Список.УстановитьОбязательноеИспользование("Список.Номер", Истина);
			
			Элементы.СписокСсылка.Видимость = Ложь;
			
			ПолеНомер = к2ФормыСенсор.ПолеВвода(ЭтотОбъект, "Список.Номер", "СписокНомер", Элементы.Список);
			ПолеНомер.Заголовок = НСтр("ru = 'Номер'");
			ПолеНомер.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Нет;
			
		КонецЕсли;
		
		Если СтрНачинаетсяС(Список.ОсновнаяТаблица, "Справочник") Тогда
			
			ПозицияТочки = СтрНайти(Список.ОсновнаяТаблица, ".");
			ИмяМетаданных = Прав(Список.ОсновнаяТаблица, СтрДлина(Список.ОсновнаяТаблица) - ПозицияТочки);
			
			ПроверятьГруппы = Метаданные.Справочники[ИмяМетаданных].Иерархический;
			
		КонецЕсли;
		
		Список.Порядок.Элементы.Очистить();
		
	Иначе
		
		Отказ = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормыСписок

&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)

	Если ПроверятьГруппы И глРеквизит(ВыбраннаяСтрока, "ЭтоГруппа") Тогда
		Возврат;
	КонецЕсли;
	
	Закрыть(ВыбраннаяСтрока);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Отмена(Команда)
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура Готово(Команда)
	
	ВыбраннаяСтрока = Элементы.Список.ТекущаяСтрока;
	
	Если ВыбраннаяСтрока <> Неопределено И ПроверятьГруппы И глРеквизит(ВыбраннаяСтрока, "ЭтоГруппа") Тогда
		ВыбраннаяСтрока = Неопределено;
	КонецЕсли;
	
	Закрыть(ВыбраннаяСтрока);
	
КонецПроцедуры

#КонецОбласти