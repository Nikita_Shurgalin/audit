#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	ЭтикеткаПриИзмененииНаСервере();
	ШаблонЭтикеткиПриИзмененииНаСервере();

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ШаблонЭтикеткиПриИзменении(Элемент)

	ШаблонЭтикеткиПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЭтикеткаПриИзменении(Элемент)
	
	ЭтикеткаПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НапечататьОбразец(Команда)
	
	ОчиститьСообщения();
	
	Если Не ЗначениеЗаполнено(Этикетка) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Не заполнено поле ""Этикетка""'"), , "Этикетка");
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ДляЧего) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Не заполнено поле ""Для чего""'"), , "ДляЧего");
		Возврат;
	КонецЕсли;
	
	ПараметрКоманды = Новый Массив;
	ПараметрКоманды.Добавить(ПредопределенноеЗначение("Справочник.Номенклатура.ПустаяСсылка"));
	
	Если Запись.Назначение = ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЦенникДляТоваров") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЦенникТовары",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЦенникаТовара());
		
	ИначеЕсли Запись.Назначение = ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляТоваров") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЭтикеткаТовары",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиТовара());
		
	ИначеЕсли Запись.Назначение =
				ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляСкладскихЯчеек") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЭтикеткаСкладскиеЯчейки",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиСкладскойЯчейки());
		
	ИначеЕсли Запись.Назначение = ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляДоставки") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЭтикеткаДоставки",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиДоставки());
			
	ИначеЕсли Запись.Назначение =
				ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаСерииНоменклатуры") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЭтикеткаСерииНоменклатуры",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиСерииНоменклатуры());
			
	ИначеЕсли Запись.Назначение =
				ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаУпаковочныхЛистов") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЭтикеткаУпаковочныеЛисты",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиУпаковочныеЛисты());
			
	ИначеЕсли Запись.Назначение =
				ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляШтрихкодовУпаковок") Тогда
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Обработка.к2ПечатьЭтикетокИЦенников",
			"ЭтикеткаШтрихкодыУпаковки",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиШтрихкодовУпаковок());
			
	ИначеЕсли Запись.Назначение =
				ПредопределенноеЗначение("Перечисление.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаКодМаркировкиИСМП") Тогда
		
		ПараметрКоманды.Очистить();
		ПараметрКоманды.Добавить(ДляЧего);
	
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
			"Справочник.к2ШтрихкодыУпаковокТоваров",
			"ЭтикеткаКодМаркировкиИСМП",
			ПараметрКоманды,
			Неопределено,
			ПараметрыДляПечатиОбразцаЭтикеткиИСМП());
			
	Иначе
		
		ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Не заполнено поле ""Назначение""'"), , "Запись.Назначение");
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЭтикеткаПриИзмененииНаСервере()

	МассивТипов = Новый Массив;
	
	Элементы.ДляЧего.ВыбиратьТип = Ложь;
	Элементы.ДляЧего.Доступность = Истина;
	
	Если Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляТоваров
		Или Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЦенникДляТоваров Тогда
		МассивТипов.Добавить(Тип("СправочникСсылка.Номенклатура"));
	ИначеЕсли Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляСкладскихЯчеек Тогда
		МассивТипов.Добавить(Тип("СправочникСсылка.к2СкладскиеЯчейки"));
	ИначеЕсли Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляДоставки Тогда
		Элементы.ДляЧего.ВыбиратьТип = Истина;
		МассивТипов.Добавить(Тип("ДокументСсылка.РеализацияТоваровУслуг"));
		МассивТипов.Добавить(Тип("ДокументСсылка.к2РаспоряжениеНаПеремещение"));
	ИначеЕсли Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаСерииНоменклатуры Тогда
		МассивТипов.Добавить(Тип("СправочникСсылка.к2СерииНоменклатуры"));
	ИначеЕсли Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаУпаковочныхЛистов Тогда
		МассивТипов.Добавить(Тип("ДокументСсылка.к2УпаковочныйЛист"));
	ИначеЕсли Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляШтрихкодовУпаковок
		Или Запись.Назначение = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаКодМаркировкиИСМП Тогда
		МассивТипов.Добавить(Тип("СправочникСсылка.к2ШтрихкодыУпаковокТоваров"));
	Иначе
		Элементы.ДляЧего.Доступность = Ложь;
	КонецЕсли;
	
	Элементы.ДляЧего.ОграничениеТипа = Новый ОписаниеТипов(МассивТипов);

	ДляЧего = Элементы.ДляЧего.ОграничениеТипа.ПривестиЗначение(ДляЧего);

КонецПроцедуры

&НаСервере
Процедура ШаблонЭтикеткиПриИзмененииНаСервере()
	
	Элементы.Этикетка.Доступность = Не ЗначениеЗаполнено(Запись.ШаблонЭтикетки);
	
	Если ЗначениеЗаполнено(Запись.ШаблонЭтикетки) Тогда
		
		Этикетка = Запись.ШаблонЭтикетки;
		ЭтикеткаПриИзмененииНаСервере();
		
	КонецЕсли;

КонецПроцедуры

&НаСервере
Функция ПараметрыДляПечатиОбразцаЦенникаТовара()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЦенникаТовара(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиТовара()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиТовара(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
		
	товары = ПолучитьИзВременногоХранилища(ПараметрыПечати.Товары);
	товары[0].ШаблонЭтикетки = Этикетка;
	товары[0].ШаблонЦенника = Этикетка;
	ПоместитьВоВременноеХранилище(товары, ПараметрыПечати.Товары);
		
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиСкладскойЯчейки()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиСкладскойЯчейки(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиДоставки()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиДоставки(
		ДляЧего,
		УникальныйИдентификатор);
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиСерииНоменклатуры()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиСерииНоменклатуры(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиУпаковочныеЛисты()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиУпаковочныеЛисты(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиШтрихкодовУпаковок()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиШтрихкодыУпаковок(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ПараметрыДляПечатиОбразцаЭтикеткиИСМП()
	
	ПараметрыПечати = Справочники.к2ШаблоныЭтикетокИЦенников.ПолучитьПараметрыДляПечатиОбразцаЭтикеткиИСМП(
		ДляЧего,
		ТипКода(),
		УникальныйИдентификатор);
	
	Возврат ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати);
	
КонецФункции

&НаСервере
Функция ТипКода()
	
	// Для разовой печати можно пренебречь эффективностью
	Возврат Этикетка.Шаблон.Получить().ТипКода;
	
КонецФункции

&НаСервере
Функция ДополнитьПараметрыДляПечатиОбразца(ПараметрыПечати)
	
	ПараметрыПечати.Вставить("ШаблонЭтикетки", Этикетка);
	ПараметрыПечати.Вставить("ИмяПринтера", Запись.ИмяПринтера);
	ПараметрыПечати.Вставить("КоличествоЭкземпляров", 1);
	ПараметрыПечати.Вставить("СтруктураМакетаШаблона", Неопределено);
	
	Возврат ПараметрыПечати;
	
КонецФункции

#КонецОбласти