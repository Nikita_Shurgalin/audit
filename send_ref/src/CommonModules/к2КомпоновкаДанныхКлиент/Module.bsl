
#Область ПрограммныйИнтерфейс

// Процедура отрабатывает расшифровку в отчет путем формирования меню действий и отработки выбранного действия.
//
// Параметры:
//	 Форма - Форма - форма, в которой расположен отчет
//	 ПараметрыРасшифровки - Структура - параметр события ОбработкаРасшифровки
//	  * МенюОтчетов - Массив - массив, значения которого имеют тип Структура и описывают отчет-приемник
//	  * МенюДействий - Массив - массив, значения которого имеют тип Структура и описывают тип выполняемого действия
//	  * Расшифровка - Произвольный - значение расшифровки
//	    (см. события для "Расширение поля формы для поля табличного документа" в справке).
//	 СтандартнаяОбработка - Булево - параметр события ОбработкаРасшифровки.
//
Процедура ОбработатьРасшифровкуСДополнительнымМеню(Форма, ПараметрыРасшифровки, СтандартнаяОбработка) Экспорт
	
	ПараметрыДополнительногоМеню = ПараметрыДополнительногоМенюРасшифровки(
		Форма.ОтчетДанныеРасшифровки,
		ПараметрыРасшифровки,
		ПараметрыРасшифровки.Расшифровка);
	
	Если ПараметрыДополнительногоМеню.ДополнительноеМеню.Количество() <> 0 Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ДополнительныеПараметры = ДополнительныеПараметрыВыбораДействия(
			ПараметрыДополнительногоМеню,
			ПараметрыРасшифровки.Расшифровка,
			Форма.ОтчетДанныеРасшифровки,
			Форма.Отчет,
			Форма.НастройкиОтчета.ПолноеИмя);
		
		ПоказатьВыборДействияПриРасшифровке(
			ПараметрыДополнительногоМеню,
			ПараметрыРасшифровки.Расшифровка,
			Форма.ОтчетДанныеРасшифровки,
			Форма.Отчет,
			ДополнительныеПараметры,
			"ОбработатьРасшифровкуСДополнительнымМенюЗавершение");
		
	КонецЕсли;
	
КонецПроцедуры

// Открывает диалог сохранения настроек в файл и сохраняет в выбранный файл переданные настройки
//
// Параметры:
//  АдресХранилищаСНастройками - Строка - расположение данных во временном хранилище.
//
Процедура СохранитьНастройкиВФайл(Знач АдресХранилищаСНастройками) Экспорт
	
	Обработчик = Новый ОписаниеОповещения("СохранитьНастройкиВФайлЗавершение", ЭтотОбъект);
	
	ПараметрыСохранения = ФайловаяСистемаКлиент.ПараметрыСохраненияФайла();
	ПараметрыСохранения.Диалог.Фильтр = НСтр("ru = 'Документ XML (*.xml)|*.xml'");
	ПараметрыСохранения.Диалог.Заголовок = НСтр("ru = 'Сохранить настройки в файл'");
	
	ФайловаяСистемаКлиент.СохранитьФайл(Обработчик, АдресХранилищаСНастройками, , ПараметрыСохранения);
	
КонецПроцедуры

// Открывает диалог загрузки настроек из файла
// 
// Параметры:
//   ОбработчикЗавершения - ОписаниеОповещения - содержит описание процедуры, которая будет вызвана после
//                             загрузки файла со следующими параметрами:
//      * ПомещенныйФайл - Неопределено - пользователь отказался от выбора.
//                       - Структура    - пользователь выбрал файл:
//                           ** Хранение  - Строка - расположение данных во временном хранилище.
//                           ** Имя       - Строка - в тонком клиенте и в веб-клиенте с установленным
//                                        расширением работы с файлами - локальный путь, по которому
//                                        был получен файл. В веб-клиенте без расширения работы с файлами - имя файла с расширением.
//      * ДополнительныеПараметры - Произвольный - значение, которое было указано при создании объекта ОписаниеОповещения.
// 	ИдентификаторФормы - УникальныйИдентификатор - уникальный идентификатор формы, из
//                                          которой выполняется размещение файла. Если параметр не заполнен, необходимо вызывать
//                                          метод глобального контекста УдалитьИзВременногоХранилища после завершения работы
//                                          с полученными двоичными данными. Значение по умолчанию - Неопределено.
Процедура ПрочитатьФайлСНастройками(ОбработчикЗавершения, ИдентификаторФормы) Экспорт

	ПараметрыЗагрузки = ФайловаяСистемаКлиент.ПараметрыЗагрузкиФайла();

	ПараметрыЗагрузки.Диалог.Заголовок = НСтр("ru = 'Загрузить настройки из файла'");
	ПараметрыЗагрузки.Диалог.Фильтр = НСтр("ru='Документ XML (*.xml)|*.xml'");
	ПараметрыЗагрузки.Диалог.Расширение = "xml";
	ПараметрыЗагрузки.Интерактивно = Истина;
	ПараметрыЗагрузки.ИдентификаторФормы = ИдентификаторФормы;

	ФайловаяСистемаКлиент.ЗагрузитьФайл(ОбработчикЗавершения, ПараметрыЗагрузки);

КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Процедура ОбработатьРасшифровкуСДополнительнымМенюЗавершение(ВыполненноеДействие, ПараметрДействия, ДополнительныеПараметры) Экспорт
	
	Если ОбработанаНестандартнаяРасшифровка(ВыполненноеДействие, ДополнительныеПараметры) Тогда
		Возврат;
	КонецЕсли;
	
	Если ПараметрДействия <> Неопределено И ВыполненноеДействие = ДействиеОбработкиРасшифровкиКомпоновкиДанных.ОткрытьЗначение Тогда
		ПоказатьЗначение(, ПараметрДействия);
	КонецЕсли;
	
	Если ПараметрДействия <> Неопределено
		И ВыполненноеДействие <> ДействиеОбработкиРасшифровкиКомпоновкиДанных.Нет
		И ВыполненноеДействие <> ДействиеОбработкиРасшифровкиКомпоновкиДанных.ОткрытьЗначение Тогда
		
		ОбработатьРасшифровкуДругимОтчетом(ПараметрДействия, ДополнительныеПараметры);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область РасшифровкаСДопМеню

Функция ПараметрыДополнительногоМенюРасшифровки(АдресДанныхРасшифровки, ПараметрыРасшифровки, Расшифровка)
	
	ПараметрыМеню = Новый Структура;
	
	ПараметрыМеню.Вставить("ПараметрыФормыРасшифровки", ПараметрыФормыРасшифровки(
		АдресДанныхРасшифровки,
		ПараметрыРасшифровки,
		Расшифровка));
	ПараметрыМеню.Вставить("ПараметрыОтчетов", ПараметрыПунктаДополнительногоМеню(
		"МенюОтчетов",
		ПараметрыРасшифровки,
		ПараметрыМеню.ПараметрыФормыРасшифровки));
	ПараметрыМеню.Вставить("МенюОтчетов", ПунктыДополнительногоМеню(ПараметрыМеню.ПараметрыОтчетов));
	ПараметрыМеню.Вставить("ПараметрыДействий", ПараметрыПунктаДополнительногоМеню(
		"МенюДействий",
		ПараметрыРасшифровки,
		ПараметрыМеню.ПараметрыФормыРасшифровки,
		,
		Ложь));
	ПараметрыМеню.Вставить("МенюДействий", ПунктыДополнительногоМеню(ПараметрыМеню.ПараметрыДействий));
	ПараметрыМеню.Вставить("ДополнительноеМеню", ДополнительноеМеню(ПараметрыМеню.МенюОтчетов, ПараметрыМеню.МенюДействий));
	
	Возврат ПараметрыМеню;
	
КонецФункции

#Область ПараметрыФормыРасшифровки

Функция ПараметрыФормыРасшифровки(АдресДанныхРасшифровки, ПараметрыРасшифровки, Расшифровка)
	
	АтрибутыРасшифровки = АтрибутыРасшифровки();
	ЗаполнитьАтрибутыПоПараметрам(АтрибутыРасшифровки, ПараметрыРасшифровки);
	
	Возврат к2КомпоновкаДанныхВызовСервера.ПараметрыФормыРасшифровки(
		Расшифровка,
		АдресДанныхРасшифровки,
		АтрибутыРасшифровки.СписокПараметров,
		АтрибутыРасшифровки.ПоляРасшифровки);
	
КонецФункции

Функция АтрибутыРасшифровки()
	
	АтрибутыРасшифровки = Новый Структура;
	
	АтрибутыРасшифровки.Вставить("СписокПараметров", Новый Массив);
	АтрибутыРасшифровки.Вставить("ПоляРасшифровки", Новый Массив);
	
	Возврат АтрибутыРасшифровки;
	
КонецФункции

Процедура ЗаполнитьАтрибутыПоПараметрам(АтрибутыРасшифровки, ПараметрыРасшифровки)
	
	Для Каждого ИмяИсточника Из СписокИсточниковРасшифровки() Цикл
		
		Если Не ПараметрыРасшифровки.Свойство(ИмяИсточника) Тогда
			Продолжить;
		КонецЕсли;
		
		Для Каждого ПараметрыИсточника Из ПараметрыРасшифровки[ИмяИсточника] Цикл
			
			Для Каждого КлючИЗначение Из АтрибутыРасшифровки Цикл
				
				ЗаполнитьПоПараметрамИсточника(
					КлючИЗначение.Значение,
					ПараметрыИсточника,
					КлючИЗначение.Ключ);
				
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

Функция СписокИсточниковРасшифровки()
	
	СписокИсточников = Новый Массив;
	
	СписокИсточников.Добавить("МенюОтчетов");
	СписокИсточников.Добавить("МенюДействий");
	
	Возврат СписокИсточников;
	
КонецФункции

Процедура ЗаполнитьПоПараметрамИсточника(Атрибут, ПараметрыИсточника, ИмяПараметра)
	
	Если ПараметрыИсточника.Свойство(ИмяПараметра) Тогда
		
		Для Каждого ИмяПоля Из ПараметрыИсточника[ИмяПараметра] Цикл
			
			Если Атрибут.Найти(ИмяПоля) = Неопределено Тогда
				Атрибут.Добавить(ИмяПоля);
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ПараметрыПунктаДополнительногоМеню

Функция ПараметрыПунктаДополнительногоМеню(
	ИмяМеню,
	ПараметрыРасшифровки,
	ПараметрыФормыРасшифровки,
	ПроверятьНеобходимыеПараметры = Истина,
	ПроверятьДостаточныеПараметры = Истина)
	
	Параметры = Новый Соответствие;
	
	Если ПараметрыРасшифровки.Свойство(ИмяМеню) Тогда
		
		Для Каждого ПараметрыОтчета Из ПараметрыРасшифровки[ИмяМеню] Цикл
			
			Если Не ПроверятьНеобходимыеПараметры Или ЕстьНеобходимыеПараметрыИЗначения(ПараметрыОтчета, ПараметрыФормыРасшифровки)
				Или Не ПроверятьДостаточныеПараметры Или ЕстьДостаточныеПараметрыИЗначения(ПараметрыОтчета, ПараметрыФормыРасшифровки) Тогда
				
				Параметры.Вставить(ПараметрыОтчета.Имя, ПараметрыОтчета);
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат Параметры;
	
КонецФункции

Функция ЕстьНеобходимыеПараметрыИЗначения(ПараметрыМеню, ПараметрыФормыРасшифровки)
	
	Перем Параметры;
	
	ЕстьПараметрыИЗначения = Истина;
	Если ПараметрыМеню.Свойство("НеобходимыеПараметры", Параметры) Тогда
		
		Для Каждого КлючИЗначение Из Параметры Цикл
			
			Если Не ПараметрыФормыРасшифровки.Свойство(КлючИЗначение.Ключ) Тогда
				
				ЕстьПараметрыИЗначения = Ложь;
				
				Прервать;
				
			Иначе
				
				Если КлючИЗначение.Значение <> Неопределено
					И КлючИЗначение.Значение.Найти(ПараметрыФормыРасшифровки[КлючИЗначение.Ключ]) = Неопределено Тогда
					
					ЕстьПараметрыИЗначения = Ложь;
					
					Прервать;
					
				КонецЕсли;
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат ЕстьПараметрыИЗначения;

КонецФункции

Функция ЕстьДостаточныеПараметрыИЗначения(ПараметрыМеню, ПараметрыФормыРасшифровки)
	
	Перем Параметры;
	
	ЕстьПараметрыИЗначения = Истина;
	Если ПараметрыМеню.Свойство("ДостаточныеПараметры", Параметры) Тогда
		
		Для Каждого КлючИЗначение Из Параметры Цикл
			
			Если Не ПараметрыФормыРасшифровки.Свойство(КлючИЗначение.Ключ) Тогда
				
				ЕстьПараметрыИЗначения = Ложь;
				
			ИначеЕсли ПараметрыФормыРасшифровки.Свойство(КлючИЗначение.Ключ) Тогда
				
				ЕстьПараметрыИЗначения = Истина;
				
				Прервать;
				
			Иначе
				
				Если КлючИЗначение.Значение <> Неопределено
					И КлючИЗначение.Значение.Найти(ПараметрыФормыРасшифровки[КлючИЗначение.Ключ]) = Неопределено Тогда
					
					ЕстьПараметрыИЗначения = Ложь;
					
				Иначе
					
					ЕстьПараметрыИЗначения = Истина;
					
					Прервать;
					
				КонецЕсли;
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат ЕстьПараметрыИЗначения;
	
КонецФункции

#КонецОбласти

Функция ПунктыДополнительногоМеню(ПараметрыМеню)
	
	Пункты = Новый СписокЗначений;
	
	Для Каждого КлючИЗначение Из ПараметрыМеню Цикл
		Пункты.Добавить(КлючИЗначение.Ключ, КлючИЗначение.Значение.Заголовок);
	КонецЦикла;
	
	Возврат Пункты;
	
КонецФункции

Функция ДополнительноеМеню(МенюОтчетов, МенюДействий)
	
	ДополнительноеМеню = Новый СписокЗначений;
	
	Если МенюОтчетов.Количество() <> 0 Тогда
		
		ПунктМеню = НСтр("ru = 'Расшифровать другим отчетом'");
		ДополнительноеМеню.Добавить(МенюОтчетов, ПунктМеню);
		
	КонецЕсли;
	
	Если МенюДействий.Количество() <> 0 Тогда
		
		ПунктМеню = НСтр("ru = 'Перейти'");
		ДополнительноеМеню.Добавить(МенюДействий, ПунктМеню);
		
	КонецЕсли;
	
	Возврат ДополнительноеМеню;
	
КонецФункции

#КонецОбласти

#Область ВыборДействия

Функция ДополнительныеПараметрыВыбораДействия(
	ПараметрыДопМеню,
	Расшифровка,
	ДанныеРасшифровки,
	АдресСхемы,
	КлючОбъекта)
	
	ДополнительныеПараметры = Новый Структура;
	
	ДополнительныеПараметры.Вставить("Расшифровка", Расшифровка);
	ДополнительныеПараметры.Вставить("ДанныеРасшифровки", ДанныеРасшифровки);
	ДополнительныеПараметры.Вставить("АдресСхемы", АдресСхемы);
	ДополнительныеПараметры.Вставить("КлючОбъекта", КлючОбъекта);
	ДополнительныеПараметры.Вставить("МенюДействий", ПараметрыДопМеню.МенюДействий);
	ДополнительныеПараметры.Вставить("МенюОтчетов", ПараметрыДопМеню.МенюОтчетов);
	ДополнительныеПараметры.Вставить("ПараметрыФормыРасшифровки", ПараметрыДопМеню.ПараметрыФормыРасшифровки);
	ДополнительныеПараметры.Вставить("ПараметрыДействий", ПараметрыДопМеню.ПараметрыДействий);
	ДополнительныеПараметры.Вставить("ПараметрыОтчетов", ПараметрыДопМеню.ПараметрыОтчетов);
	
	Возврат ДополнительныеПараметры;
	
КонецФункции

Процедура ПоказатьВыборДействияПриРасшифровке(
	ПараметрыДопМеню,
	Расшифровка,
	ДанныеРасшифровки,
	АдресСхемы,
	ДополнительныеПараметры,
	ИмяОбработчика)
	
	ОписаниеОповещения = Новый ОписаниеОповещения(ИмяОбработчика, ЭтотОбъект, ДополнительныеПараметры);
	
	ОбработкаРасшифровки = Новый ОбработкаРасшифровкиКомпоновкиДанных(
		ДанныеРасшифровки,
		Новый ИсточникДоступныхНастроекКомпоновкиДанных(АдресСхемы));
	
	ОбработкаРасшифровки.ПоказатьВыборДействия(
		ОписаниеОповещения,
		Расшифровка,
		,
		ПараметрыДопМеню.ДополнительноеМеню);
	
КонецПроцедуры

#КонецОбласти

#Область НестандартнаяРасшифровка

Функция ОбработанаНестандартнаяРасшифровка(ВыполненноеДействие, ДополнительныеПараметры)

	Перем МенюДействий, МенюОтчетов;
	
	НестандартнаяРасшифровка = Ложь;
	
	Если ДополнительныеПараметры.Свойство("МенюОтчетов", МенюОтчетов)
		И МенюОтчетов <> Неопределено
		И МенюОтчетов.НайтиПоЗначению(ВыполненноеДействие) <> Неопределено Тогда
		
		ОбработатьВыборДействияИзМенюОтчетов(ВыполненноеДействие, ДополнительныеПараметры);
		
		НестандартнаяРасшифровка = Истина;
		
	КонецЕсли;
	
	Если ДополнительныеПараметры.Свойство("МенюДействий", МенюДействий)
		И МенюДействий <> Неопределено
		И МенюДействий.НайтиПоЗначению(ВыполненноеДействие) <> Неопределено Тогда
		
		ОбработатьВыборДействияИзМенюДействий(ВыполненноеДействие, ДополнительныеПараметры);
		
		НестандартнаяРасшифровка = Истина;
		
	КонецЕсли;
	
	Возврат НестандартнаяРасшифровка;
	
КонецФункции

#Область ДействиеИзМенюОтчетов

Процедура ОбработатьВыборДействияИзМенюОтчетов(ВыполненноеДействие, ДополнительныеПараметры)
	
	ПараметрыОтчета = ДополнительныеПараметры.ПараметрыОтчетов.Получить(ВыполненноеДействие);
	
	ПараметрыФормы = ПараметрыФормы(ВыполненноеДействие, ПараметрыОтчета, ДополнительныеПараметры);
	
	ОбработатьПереносОтборов(ПараметрыОтчета, ДополнительныеПараметры);
	
	ОбработатьЗаменуПараметров(ПараметрыФормы, ПараметрыОтчета, ДополнительныеПараметры);
	
	УстановитьОтборПоФиксированнымПараметрам(ПараметрыФормы, ПараметрыОтчета);
	
	ОткрытьФорму(ПараметрыОтчета.ИмяОтчета + ".Форма", ПараметрыФормы);
	
КонецПроцедуры

Функция ПараметрыФормы(ВыполненноеДействие, ПараметрыОтчета, ДополнительныеПараметры)
	
	ПараметрыФормы = Новый Структура;
	
	Если ПараметрыОтчета.Свойство("РежимРасшифровки") Тогда
		ПараметрыФормы.Вставить("РежимРасшифровки", ПараметрыОтчета.РежимРасшифровки);
	КонецЕсли;
	ПараметрыФормы.Вставить("КлючПользовательскихНастроек", ВыполненноеДействие);
	ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
	ПараметрыФормы.Вставить("КлючНазначенияИспользования", ВыполненноеДействие + Строка(Новый УникальныйИдентификатор));
	ПараметрыФормы.Вставить("КлючВарианта", ВыполненноеДействие);
	ПараметрыФормы.Вставить("Отбор", ДополнительныеПараметры.ПараметрыФормыРасшифровки);
	
	Возврат ПараметрыФормы;
	
КонецФункции

#Область ПереносОтборов

Процедура ОбработатьПереносОтборов(ПараметрыОтчета, ДополнительныеПараметры)
	
	Если ДополнительныеПараметры.ПараметрыФормыРасшифровки.Свойство("Отбор") Тогда
		
		Для Каждого Отбор Из ДополнительныеПараметры.ПараметрыФормыРасшифровки.Отбор.Элементы Цикл
			
			Если Не ТипЗнч(Отбор) = Тип("ЭлементОтбораКомпоновкиДанных") Тогда
				Продолжить;
			КонецЕсли;
			
			КлючОтбора = СтрЗаменить(Строка(Отбор.ЛевоеЗначение), ".", "_");
			Если ПараметрыОтчета.ПоляРасшифровки.Найти(КлючОтбора) <> Неопределено И Отбор.Использование Тогда
				ДополнительныеПараметры.ПараметрыФормыРасшифровки.Вставить(КлючОтбора, Отбор.ПравоеЗначение);
			КонецЕсли;
			
		КонецЦикла;
		
		ДополнительныеПараметры.ПараметрыФормыРасшифровки.Удалить("Отбор");
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ЗаменаПараметров

Процедура ОбработатьЗаменуПараметров(ПараметрыФормы, ПараметрыОтчета, ДополнительныеПараметры)
	
	Если Не ПараметрыОтчета.Свойство("ЗаменаПараметров") Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого КлючИЗначение Из ПараметрыОтчета.ЗаменаПараметров Цикл
		
		КлючИскомогоОтбора = КлючИЗначение.Ключ;
		КлючЦелевогоОтбора = КлючИЗначение.Значение;
		
		ОбработатьПрямуюЗаменуПараметра(ПараметрыФормы, ДополнительныеПараметры, КлючИскомогоОтбора, КлючЦелевогоОтбора);
		ОбработатьЗаменуПараметраОбращенияЧерезТочку(ПараметрыФормы, ДополнительныеПараметры, КлючИскомогоОтбора, КлючЦелевогоОтбора);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ОбработатьПрямуюЗаменуПараметра(ПараметрыФормы, ДополнительныеПараметры, КлючИскомогоОтбора, КлючЦелевогоОтбора)
	
	Если ДополнительныеПараметры.ПараметрыФормыРасшифровки.Свойство(КлючИскомогоОтбора) Тогда
		
		Если ТипЗнч(КлючЦелевогоОтбора) = Тип("Строка") Тогда
			
			ПараметрыФормы.Отбор.Вставить(
				КлючЦелевогоОтбора,
				ДополнительныеПараметры.ПараметрыФормыРасшифровки[КлючИскомогоОтбора]);
			
		КонецЕсли;
		
		Если ТипЗнч(КлючЦелевогоОтбора) = Тип("Соответствие") Тогда
			
			ЗаменяющиеПараметры = КлючЦелевогоОтбора.Получить(
				ДополнительныеПараметры.ПараметрыФормыРасшифровки[КлючИскомогоОтбора]);
			
			Если ЗаменяющиеПараметры <> Неопределено Тогда
				
				Для Каждого ЗаменяющийПараметр Из ЗаменяющиеПараметры Цикл
					ПараметрыФормы.Отбор.Вставить(ЗаменяющийПараметр.Ключ, ЗаменяющийПараметр.Значение);
				КонецЦикла;
				
			КонецЕсли;
			
		КонецЕсли;
		
		ПараметрыФормы.Отбор.Удалить(КлючИскомогоОтбора);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработатьЗаменуПараметраОбращенияЧерезТочку(ПараметрыФормы, ДополнительныеПараметры, КлючИскомогоОтбора, КлючЦелевогоОтбора)
	
	Для Каждого ПараметрФормы Из ДополнительныеПараметры.ПараметрыФормыРасшифровки Цикл
		
		Если СтрНайти(ПараметрФормы.Ключ, КлючИскомогоОтбора + "_") > 0 Тогда
			
			ПараметрыФормы.Отбор.Вставить(
				СтрЗаменить(ПараметрФормы.Ключ, КлючИскомогоОтбора + "_", КлючЦелевогоОтбора + "_"),
				ПараметрФормы.Значение);
			
			ПараметрыФормы.Отбор.Удалить(ПараметрФормы.Ключ);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

Процедура УстановитьОтборПоФиксированнымПараметрам(ПараметрыФормы, ПараметрыОтчета)
	
	Если ПараметрыОтчета.Свойство("ФиксированныеПараметры") Тогда
		
		Для Каждого ФиксированныйПараметр Из ПараметрыОтчета.ФиксированныеПараметры Цикл
			ПараметрыФормы.Отбор.Вставить(ФиксированныйПараметр.Ключ, ФиксированныйПараметр.Значение);
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ДействиеИзМенюДействий

Процедура ОбработатьВыборДействияИзМенюДействий(ВыполненноеДействие, ДополнительныеПараметры)
	
	ПараметрыДействия = ДополнительныеПараметры.ПараметрыДействий.Получить(ВыполненноеДействие);
	
	Действие = ИдентификаторДействияРасшифровки(ДополнительныеПараметры, ВыполненноеДействие);
	
	Если Действие = "ОткрытьФорму" Тогда
		
		ПараметрыФормы = ПараметрыОткрытияФормыПоДействию(ПараметрыДействия, ДополнительныеПараметры);
		
		ОткрытьФорму(ПараметрыДействия.ИмяФормы, ПараметрыФормы);
		
	Иначе
		
		ОбщегоНазначенияКлиент.ОбщийМодуль(ПараметрыДействия.ИмяОбщегоМодуля).ВыполнитьДействиеРасшифровки(
			ПараметрыДействия,
			ДополнительныеПараметры.ПараметрыФормыРасшифровки);
		
	КонецЕсли;
	
КонецПроцедуры

Функция ИдентификаторДействияРасшифровки(Параметры, ЗначениеПоУмолчанию)
	
	Если Параметры.Свойство("Действие") Тогда
		Действие = Параметры.Действие;
	Иначе
		Действие = ЗначениеПоУмолчанию;
	КонецЕсли;
	
	Возврат Действие;
	
КонецФункции

Функция ПараметрыОткрытияФормыПоДействию(ПараметрыДействия, ДополнительныеПараметры)
	
	Перем ПараметрыФормы;
	
	Если ПараметрыДействия.Свойство("ПараметрыФормы", ПараметрыФормы) Тогда
		
		ЗаполнитьЗначенияСвойств(ПараметрыФормы, ДополнительныеПараметры.ПараметрыФормыРасшифровки);
		
		Если ПараметрыДействия.Свойство("ЗаменаПараметров") Тогда
			
			Для Каждого КлючИЗначение Из ПараметрыДействия.ЗаменаПараметров Цикл
				
				Если ДополнительныеПараметры.ПараметрыФормыРасшифровки.Свойство(КлючИЗначение.Ключ) Тогда
					
					ПараметрыФормы.Вставить(
						КлючИЗначение.Значение,
						ДополнительныеПараметры.ПараметрыФормыРасшифровки[КлючИЗначение.Ключ]);
					
				КонецЕсли;
				
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ПараметрыФормы;
	
КонецФункции

#КонецОбласти

#КонецОбласти

Процедура ОбработатьРасшифровкуДругимОтчетом(ПараметрДействия, ДополнительныеПараметры)
	
	ОписаниеОбработкиРасшифровки = Новый ОписаниеОбработкиРасшифровкиКомпоновкиДанных(
		ДополнительныеПараметры.ДанныеРасшифровки,
		ДополнительныеПараметры.Расшифровка,
		ПараметрДействия);
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
	ПараметрыФормы.Вставить("КлючНазначенияИспользования", ДополнительныеПараметры.Расшифровка);
	ПараметрыФормы.Вставить("Расшифровка", ОписаниеОбработкиРасшифровки);
	
	ОткрытьФорму(ДополнительныеПараметры.КлючОбъекта + ".Форма", ПараметрыФормы);
	
КонецПроцедуры

Процедура СохранитьНастройкиВФайлЗавершение(Файлы, ДополнительныеПараметры) Экспорт 
	
	Если ТипЗнч(Файлы) = Тип("Массив")
		И Файлы.Количество() > 0 Тогда 
		
		ПоказатьОповещениеПользователя(НСтр("ru = 'Настройки сохранены в файл'"), , Файлы[0].ПолноеИмя);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
