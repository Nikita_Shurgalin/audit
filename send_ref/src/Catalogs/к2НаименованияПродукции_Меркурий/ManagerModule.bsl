
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает ссылку по структуре описания
//
// Параметры:
//  пСтруктура - Структура - структура описания
//  пСоздаватьЭлемент - Булево - Описание
// Возвращаемое значение:
//  СправочникСсылка.к2НаименованияПродукции_Меркурий - ссылка.
Функция ПолучитьПоОписанию( Знач пСтруктура, Знач пСоздаватьЭлемент = Истина ) Экспорт
	
	Если пСтруктура = Неопределено Тогда
		
		Возврат Справочники.к2НаименованияПродукции_Меркурий.ПустаяСсылка();
		
	КонецЕсли;
	
	ссылка = глЗначениеСтруктуры( пСтруктура, "Ссылка" );
	
	Если ЗначениеЗаполнено( ссылка ) Тогда
		
		Возврат ссылка;
		
	КонецЕсли;
	
	владелец = глЗначениеСтруктуры( пСтруктура, "Владелец" );
	
	ссылка = ПустаяСсылка();
	
	гуид = глЗначениеСтруктуры( пСтруктура, "ГУИД" );
	ууид = глЗначениеСтруктуры( пСтруктура, "УУИД" );
	
	Если ЗначениеЗаполнено( гуид ) Тогда
	
		ссылка = к2МеркурийВызовСервера.ЗначениеПоГуид_Кэш(
				гуид,
				"к2НаименованияПродукции_Меркурий",
				ууид,
				Не пСоздаватьЭлемент );
				
	КонецЕсли;
	
	Если ЗначениеЗаполнено( ссылка )
		ИЛИ Не пСоздаватьЭлемент Тогда
		
		Возврат ссылка;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( ууид ) Тогда
		
		Возврат к2МеркурийВызовСервера.ЗначениеПоУУИД_Кэш( пСтруктура.УУИД, "к2НаименованияПродукции_Меркурий" );
		
	КонецЕсли;
	
	наименование = глЗначениеСтруктуры( пСтруктура, "Наименование" );
	
	Если ЗначениеЗаполнено( наименование ) Тогда
		
		Запрос       = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	к2НаименованияПродукции_Меркурий.Ссылка КАК Ссылка,
		               |	к2НаименованияПродукции_Меркурий.НаименованиеПолное КАК НаименованиеПолное
		               |ИЗ
		               |	Справочник.к2НаименованияПродукции_Меркурий КАК к2НаименованияПродукции_Меркурий
		               |ГДЕ
		               |	к2НаименованияПродукции_Меркурий.НаименованиеПолное = &Наименование
		               |	И к2НаименованияПродукции_Меркурий.Владелец = &Владелец
		               |	И НЕ к2НаименованияПродукции_Меркурий.ПометкаУдаления
		               |	И к2НаименованияПродукции_Меркурий.ГУИД = &ПустойГУИД";
		Запрос.УстановитьПараметр( "Наименование", Наименование );
		Запрос.УстановитьПараметр( "Владелец", владелец );
		Запрос.УстановитьПараметр( "ПустойГУИД", Справочники.к2НаименованияПродукции_Меркурий.ПустаяСсылка().ГУИД );
		
		выборка = Запрос.Выполнить().Выбрать();
		
		Пока выборка.Следующий() Цикл
			
			Если выборка.НаименованиеПолное = Наименование Тогда
				
				Возврат выборка.Ссылка;
				
			КонецЕсли;
			
		КонецЦикла;
		
		Если пСоздаватьЭлемент Тогда
			
			УстановитьПривилегированныйРежим( Истина );
			
			об = Справочники.к2НаименованияПродукции_Меркурий.СоздатьЭлемент();
			об.ЗаполнитьПоСтруктуреОписаний( пСтруктура );
			об.Владелец                = владелец;
			об.ТребуетсяСинхронизация  = Истина;
			об.ВыполняетсяИмпортДанных = Ложь;
			об.Записать();
			
			Возврат об.Ссылка;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Справочники.к2НаименованияПродукции_Меркурий.ПустаяСсылка();
	
КонецФункции

// Заполняет описания вложенных объектов
//
// Параметры:	
//  пОписание - Структура - текущее описание
//  пПараметрыОбновления - Структура - Структура сопроводительных данных.
Процедура ЗаполнитьОписанияВложенныхОбъектов( Знач пОписание, пПараметрыОбновления ) Экспорт
	
	к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
		Метаданные.Справочники.к2КлассификаторПродукции_Меркурий.Имя,
		пОписание.Продукция,
		пПараметрыОбновления );
	к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
		Метаданные.Справочники.к2КлассификаторВидовПродукции_Меркурий.Имя,
		пОписание.ВидПродукции,
		пПараметрыОбновления );
	
	к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
		Метаданные.Справочники.к2ХозяйствующиеСубъекты_Меркурий.Имя,
		пОписание.Производитель,
		пПараметрыОбновления );
	
	к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
		Метаданные.Справочники.к2ХозяйствующиеСубъекты_Меркурий.Имя,
		пОписание.СобственникТорговойМарки,
		пПараметрыОбновления );
	
	Для каждого цСтрукт Из пОписание.ВыпускающиеПлощадки Цикл
		
		к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
			Метаданные.Справочники.к2Предприятия_Меркурий.Имя,
			цСтрукт.Предприятие,
			пПараметрыОбновления );
		
	КонецЦикла;
	
	Если Не пОписание.Фасовка = Неопределено Тогда
		
		к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
			Метаданные.Справочники.к2Упаковки_Меркурий.Имя,
			пОписание.Фасовка.ТипУпаковки,
			пПараметрыОбновления );
		
		к2Заполнение_Меркурий.ДобавитьВложенныеОбъекты(
			Метаданные.Справочники.к2КлассификаторЕдиницИзмерения_Меркурий.Имя,
			пОписание.Фасовка.ЕдиницаИзмеренияОбъема,
			пПараметрыОбновления );
		
	КонецЕсли;
	
КонецПроцедуры

// Возвращает срок годности продукции в днях
//
// Параметры:
//  пНаименованиеПродукции - СправочникСсылка.к2НаименованияПродукции_Меркурий - ссылка.
// Возвращаемое значение:
//  СрокГодности - число.
Функция ПолучитьСрокГодности(пНаименованиеПродукции) Экспорт
	
	СрокГодности =  к2МеркурийПовтИсп.ЗначениеРеквизитаОбъекта(пНаименованиеПродукции, "СрокГодности");
	
	Если Не ЗначениеЗаполнено(СрокГодности) Тогда
		
		СрокГодности = 0;
		
	КонецЕсли;
	
	Возврат СрокГодности;
	
КонецФункции

#КонецОбласти

#КонецЕсли
