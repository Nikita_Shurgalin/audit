#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда


Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если ИспользоватьВыражение Тогда
		
		ПроверяемыеРеквизиты.Добавить( "Выражение" );
		
	Иначе
		
		ПроверитьПредставлениеОтбораСКД(Отказ);
		
	КонецЕсли;
	
	Если ДоступенКонтекстЗадачи
		И ЗначениеЗаполнено( ИсточникРеквизитов ) Тогда
		
		ПроверяемыеРеквизиты.Добавить( "СвязанныеРеквизиты" );
		ПроверяемыеРеквизиты.Добавить( "Описание" );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьПредставлениеОтбораСКД( Отказ = Ложь ) Экспорт
	
	текстСообщения = "";
	
	Если Не ПредставлениеОтбораСКДКорректно( текстСообщения ) Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения , ЭтотОбъект , "ПредставлениеУсловия" , ,Отказ );
		
	КонецЕсли;
	
КонецПроцедуры

Функция ПредставлениеОтбораСКДКорректно( пТекстСообщения = "" ) Экспорт
	
	Если Модифицированность()
		ИЛИ Не ЗначениеЗаполнено( ПредставлениеУсловия ) Тогда
		
		// модифицированный объект еще не обновил СКД
		
		// не с чем сравнивать
		// возможно условие новое
		Возврат Истина;
	КонецЕсли;
	
	скд = хзСКД.Получить();
	
	Если скд = Неопределено Тогда
		Возврат Истина;
	КонецЕсли;
	
	скдПредставлениеУсловия = упСКДКлиентСервер.ПредставлениеОтбора( скд );
	
	Если Не ПредставлениеУсловия = скдПредставлениеУсловия Тогда
		
		шаблонТекста = НСтр( "ru='Сменилось условие в СКД по сравнению с сохраненным. Проверьте корректность.
		|Старое условие: %1
		|Новое условие:  %2'" );
		пТекстСообщения = стрЗаполнить( шаблонТекста, ПредставлениеУсловия, скдПредставлениеУсловия );
		
		Возврат Ложь;
		
	Иначе
		
		пТекстСообщения = "";
		
		Возврат Истина;
		
	КонецЕсли;
	
КонецФункции


Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч( ДанныеЗаполнения ) = Тип( "Структура" ) Тогда
		
		ЗаполнитьЗначенияСвойств( ЭтотОбъект , ДанныеЗаполнения );
		
		Если Не ЗначениеЗаполнено( ИсточникРеквизитов )
			И ЗначениеЗаполнено( ВерсияБП ) Тогда
			
			ИсточникРеквизитов = ВерсияБП;
			
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено( ВерсияБП )
			И упОбщегоНазначенияКлиентСервер.ЭтоВерсияСсылка( ИсточникРеквизитов ) Тогда
			
			ВерсияБП = ИсточникРеквизитов;
			
		КонецЕсли;
		
		Если ДанныеЗаполнения.Свойство( "ЭтоДобавлениеУсловия" ) Тогда
			
			Если СоздаватьСКД( ДанныеЗаполнения ) Тогда
				
				ИспользоватьВыражение = Ложь;
				
				СКД = упСКДКлиентСервер.СоздатьСКДПоОбъекту( ИсточникРеквизитов );
				
				ЭлементыОтбора = СКД.НастройкиПоУмолчанию.Отбор.Элементы;
				
				Для каждого цСтрока Из ДанныеЗаполнения.ТаблицаУсловий Цикл
					
					ЭлементОтбора = ЭлементыОтбора.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
					ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных( глРеквизит_Кэш( цСтрока.РеквизитБП , "Имя" ) );
					ЭлементОтбора.ВидСравнения = цСтрока.ТипСравнения;
					ЭлементОтбора.ПравоеЗначение = цСтрока.Значение;
					
					СвязанныеРеквизиты.Добавить().РеквизитБП = цСтрока.РеквизитБП;
					
				КонецЦикла;
				
				хзСКД = Новый ХранилищеЗначения( СКД );
				
				ПредставлениеУсловия = Строка( СКД.НастройкиПоУмолчанию.Отбор );
				
			Иначе
				
				ИспользоватьВыражение = Истина;
				
				Для каждого цСтрока Из ДанныеЗаполнения.ТаблицаУсловий Цикл
					
					СвязанныеРеквизиты.Добавить().РеквизитБП = цСтрока.РеквизитБП;
					
				КонецЦикла;
				
				лТекстАлгоритма = ПолучитьТекстАлгоритмаДляУсловия(ДанныеЗаполнения);
				Наименование = ПолучитьПредставлениеУсловия( ДанныеЗаполнения );
				ПредставлениеУсловия = лТекстАлгоритма;
				Выражение = СоздатьАлгоритм(ДанныеЗаполнения, лТекстАлгоритма);
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если ПустаяСтрока(Наименование) Тогда
		
		Наименование = ПолучитьПредставлениеУсловия( ДанныеЗаполнения );
		
	КонецЕсли;
	
	Если ПустаяСтрока(Описание) Тогда
		
		Описание = НСтр( "ru='Для выполнения условия необходимо, чтобы '" ) + ПолучитьПредставлениеУсловия( ДанныеЗаполнения );
		
	КонецЕсли;
	
	УстановитьНовыйКод();
	
КонецПроцедуры

Функция СоздаватьСКД( пДанныеЗаполнения )
	
	Для каждого цСтрока Из пДанныеЗаполнения.ТаблицаУсловий Цикл
		
		Если Не ( цСтрока.ТипСравнения = ВидСравненияКомпоновкиДанных.Заполнено
			ИЛИ цСтрока.ТипСравнения = ВидСравненияКомпоновкиДанных.Равно ) Тогда
			
			Возврат Истина;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Ложь;
	
КонецФункции

Функция СоздатьАлгоритм(ДанныеЗаполнения, ТекстАлгоритма)
	
	СтруктураЗаполнения = Новый Структура( "ВерсияБП, ИсточникРеквизитов, ДоступенКонтекстЗадачи,ТипАлгоритма,ЭтоПроверкаУсловия", ВерсияБП, ИсточникРеквизитов, ДоступенКонтекстЗадачи, Перечисления.упТипАлгоритма.ПроверкаУсловия, Истина);
	СтруктураЗаполнения.Вставить( "Алгоритм" , ТекстАлгоритма );
	СтруктураЗаполнения.Вставить( "Наименование" , Наименование );
	СтруктураЗаполнения.Вставить( "Комментарий" , НСтр( "ru='Алгоритм создан мастером условий.'" ) );
	
	найденныйАлгоритм = упОбщегоНазначенияСервер.НайтиСсылкуПоСтруктуре( "Справочник.упАлгоритмы", СтруктураЗаполнения );
	
	Если ЗначениеЗаполнено( найденныйАлгоритм ) Тогда
		
		Возврат найденныйАлгоритм;
		
	КонецЕсли;
	
	ОбАлгоритм = Справочники.упАлгоритмы.СоздатьЭлемент();
	
	ОбАлгоритм.ЛокальныеИменованныеКонстанты.Очистить();
	
	Для каждого цСтрока Из ДанныеЗаполнения.ТаблицаУсловий Цикл
		
		новСтрока = ОбАлгоритм.ЛокальныеИменованныеКонстанты.Добавить();
		новСтрока.Тип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьЭлементПоТипу( ТипЗнч( цСтрока.Значение ) );
		новСтрока.Имя = глРеквизит_Кэш(цСтрока.РеквизитБП , "Имя");
		новСтрока.Значение = цСтрока.Значение;
		
	КонецЦикла;
	
	ОбАлгоритм.Заполнить(СтруктураЗаполнения);
	ОбАлгоритм.Записать();
	
	Возврат ОбАлгоритм.Ссылка;
	
КонецФункции

Функция ПолучитьТекстАлгоритмаДляУсловия( ДанныеЗаполнения )
	
	шаблонАлгоритма = НСтр( "ru='%1.%2 = (%3)'" );
	шаблонАлгоритма = стрЗаполнить( шаблонАлгоритма, упИК.СтруктураРеквизитов(), упИК.УсловиеВыполнено(), "%1" );
	
	Если ДанныеЗаполнения.ТаблицаУсловий.Количество() = 0 Тогда
		
		Возврат стрЗаполнить( шаблонАлгоритма, НСтр( "ru='Истина'" ));
		
	КонецЕсли;
	
	массивУсловий = Новый Массив;
	
	Для каждого цСтрока Из ДанныеЗаполнения.ТаблицаУсловий Цикл
		
		массивУсловий.Добавить( ТекстУсловия(цСтрока) );
		
	КонецЦикла;
	
	шаблонУсловия = стрСобрать( массивУсловий, НСтр( "ru=' И '" ) );
	
	Возврат стрЗаполнить( шаблонАлгоритма, шаблонУсловия );
	
КонецФункции

Функция ТекстУсловия(СтруктураУсловия)
	
	текстУсловия = "";
	
	Если СтруктураУсловия.ТипСравнения = ВидСравненияКомпоновкиДанных.Заполнено Тогда
		
		шаблонУсловия = НСтр( "ru='ЗначениеЗаполнено( РеквизитБП(""%1"") )'" );
		
	ИначеЕсли СтруктураУсловия.ТипСравнения = ВидСравненияКомпоновкиДанных.Равно Тогда
		
		шаблонУсловия = НСтр( "ru='РеквизитБП(""%1"") = л_ИК( ""%1"" )'" );
		
	Иначе
		
		ВызватьИсключение НСтр( "ru='Попытка создания условия на основе алгоритма по неподдерживаемому типу сравнения: '" ) + СтруктураУсловия.ТипСравнения;
		
	КонецЕсли;
	
	Возврат стрЗаполнить( шаблонУсловия, глРеквизит_Кэш(СтруктураУсловия.РеквизитБП, "Имя" ) );
	
КонецФункции

Функция ПолучитьПредставлениеУсловия( пДанныеЗаполнения )
	
	Если ИспользоватьВыражение Тогда
		
		СКД = упСКДКлиентСервер.СоздатьСКДПоОбъекту( ИсточникРеквизитов );
		
		ЭлементыОтбора = СКД.НастройкиПоУмолчанию.Отбор.Элементы;
		
		Для каждого цСтрока Из пДанныеЗаполнения.ТаблицаУсловий Цикл
			
			ЭлементОтбора = ЭлементыОтбора.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
			ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных( глРеквизит_Кэш( цСтрока.РеквизитБП , "Наименование" ) );
			ЭлементОтбора.ВидСравнения = цСтрока.ТипСравнения;
			ЭлементОтбора.ПравоеЗначение = цСтрока.Значение;
			
		КонецЦикла;
		
		лПредставлениеУсловия = Строка( СКД.НастройкиПоУмолчанию.Отбор );
		
	Иначе
		
		лПредставлениеУсловия = ПредставлениеУсловия;
		
	КонецЕсли;
	
	Возврат лПредставлениеУсловия;
	
КонецФункции

#КонецЕсли