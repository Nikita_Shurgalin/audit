#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура Установить( пВерсия, пШаблон ) Экспорт
	
	нз = РегистрыСведений.упШаблоныИменСессий.СоздатьНаборЗаписей();
	нз.Отбор.ВерсияБП.Установить( пВерсия );
	
	Если ЗначениеЗаполнено( пШаблон ) Тогда
		
		новСтрока = нз.Добавить();
		новСтрока.ВерсияБП = пВерсия;
		новСтрока.ШаблонИмениСессии = пШаблон;
		
	КонецЕсли;
	
	нз.Записать();
	
КонецПроцедуры

Функция ПолучитьШаблон( пВерсияБП ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упШаблоныИменСессий.ШаблонИмениСессии
	|ИЗ
	|	РегистрСведений.упШаблоныИменСессий КАК упШаблоныИменСессий
	|ГДЕ
	|	упШаблоныИменСессий.ВерсияБП = &ВерсияБП";
	
	Запрос.УстановитьПараметр("ВерсияБП", пВерсияБП );
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Возврат Выборка.ШаблонИмениСессии;
		
	Иначе
		
		Возврат Справочники.упАлгоритмы.ПустаяСсылка();
		
	КонецЕсли;
	
КонецФункции


Функция ОбъектыВерсии( пВерсия ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упШаблоныИменСессий.ВерсияБП,
	               |	упШаблоныИменСессий.ШаблонИмениСессии
	               |ИЗ
	               |	РегистрСведений.упШаблоныИменСессий КАК упШаблоныИменСессий
	               |ГДЕ
	               |	упШаблоныИменСессий.ВерсияБП = &ВерсияБП";
	
	Запрос.УстановитьПараметр( "ВерсияБП" , пВерсия );
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

#КонецЕсли