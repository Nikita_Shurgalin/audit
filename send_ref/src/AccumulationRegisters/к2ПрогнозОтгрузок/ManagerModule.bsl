#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.Таблица_к2ПрогнозОтгрузок;
	
	Если Отказ Или Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.к2ПрогнозОтгрузок.Записывать = Истина;
	Движения.к2ПрогнозОтгрузок.Загрузить(Таблица);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли