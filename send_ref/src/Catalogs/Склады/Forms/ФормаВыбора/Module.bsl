
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьОтборПоРабочемуЦентру();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	Если ОтображатьСписком Тогда 
		Элементы.Список.Отображение = ОтображениеТаблицы.Список;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьОтборПоРабочемуЦентру()
	
	Если Параметры.Свойство("РабочийЦентр", РабочийЦентр) Тогда 
		
		Если РабочийЦентр.НесколькоСкладов Тогда 
			
			ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
											Список,
											"Ссылка",
											Справочники.к2РабочиеЦентры.СкладыРабочихЦентров(РабочийЦентр),
											ВидСравненияКомпоновкиДанных.ВСписке,
											,
											Истина);
			
		Иначе
						
			ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
											Список,
											"Ссылка",
											Справочники.к2РабочиеЦентры.ПриоритетныйСкладРабочегоЦентра(РабочийЦентр),
											ВидСравненияКомпоновкиДанных.Равно,
											,
											Истина);
			
		КонецЕсли;
		
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
											Список,
											"ЭтоГруппа",
											Ложь,
											ВидСравненияКомпоновкиДанных.Равно,
											,
											Истина);
											
											
		ОтображатьСписком = Истина;
	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти