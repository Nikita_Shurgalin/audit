
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СоздатьПанельОтборов();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПанельОтборов

&НаСервере
Процедура СоздатьПанельОтборов()
	
	параметрыПанели = к2Отборы.ПараметрыПанелиОтборов();
	параметрыПанели.Кнопка_НастроитьСписок           = Элементы.НастройкаСписка;
	параметрыПанели.Группа_ПользовательскиеНастройки = Элементы.СписокКомпоновщикНастроекПользовательскиеНастройки;
	параметрыПанели.Метаданные = Метаданные.РегистрыСведений.к2РазмерБуфера;
	
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Период");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Номенклатура");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Склад");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Организация");
	
	к2Отборы.СоздатьПанельОтборов(ЭтотОбъект, параметрыПанели);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбработкаПанелиОтборов(Элемент, СтандартнаяОбработка = Истина)
	
	к2ОтборыКлиент.ОбработкаПанелиОтборов(ЭтотОбъект, Элемент, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_УстановитьОтбор(Знач пИмяЭлемента, Знач пСохранять = Истина) Экспорт
	
	к2Отборы.УстановитьОтборСписку(ЭтотОбъект, пИмяЭлемента, пСохранять);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
