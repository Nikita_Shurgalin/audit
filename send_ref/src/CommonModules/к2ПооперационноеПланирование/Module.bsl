
#Область СлужебныйПрограммныйИнтерфейс

// Возвращает графики работы видов РЦ.
//
// Параметры:
//  Ссылки - Массив, СправочникСсылка.ВидыРабочихЦентров - ссылки на виды РЦ.
//
// Возвращаемое значение:
//  ТаблицаЗначений - таблица с колонками:
//		* Ссылка - СправочникСсылка.ВидыРабочихЦентров - вид РЦ.
//		* ГрафикРаботы - СправочникСсылка.Календари - график работы.
//
Функция ГрафикиРаботыВидовРЦ(Ссылки) Экспорт
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТГрафикиРаботыВидовРЦ(Ссылки, МенеджерВременныхТаблиц, "ВТГрафикиРаботы");
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
			|	ВТГрафикиРаботы.Ссылка       КАК Ссылка,
			|	ВТГрафикиРаботы.ГрафикРаботы КАК ГрафикРаботы
			|ИЗ
			|	ВТГрафикиРаботы КАК ВТГрафикиРаботы");
	
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

Функция ЗаполнитьДоступностьПоГрафикамРаботы(Знач СписокВидовРЦ,
											 Знач НачалоПериода,
											 Знач ОкончаниеПериода,
												  ПредупрежденияПоНСИ = Неопределено) Экспорт
	
	Если ПредупрежденияПоНСИ = Неопределено Тогда
		
		ПредупрежденияПоНСИ = Новый Соответствие;
		
	КонецЕсли;
	
	РасписаниеГрафиковРаботыРЦ = СоздатьТаблицуДоступностиРЦ();
	
	Если СписокВидовРЦ.Количество() = 0 Тогда
		
		Возврат РасписаниеГрафиковРаботыРЦ;
		
	КонецЕсли;
	
	// Получим данные выбранных видов РЦ
	
	ГрафикиРаботыРабочихЦентров = ГрафикиРаботыРЦ(СписокВидовРЦ);
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	к2РабочиеЦентры.ВидРабочегоЦентра КАК ВидРабочегоЦентра,
		|	к2РабочиеЦентры.Ссылка КАК РабочийЦентр,
		|	к2РабочиеЦентры.Наименование КАК РабочийЦентрПредставление
		|ИЗ
		|	Справочник.к2РабочиеЦентры КАК к2РабочиеЦентры
		|ГДЕ
		|	к2РабочиеЦентры.ВидРабочегоЦентра В(&СписокВидовРЦ)
		|	И НЕ к2РабочиеЦентры.ПометкаУдаления";
	Запрос.УстановитьПараметр("СписокВидовРЦ", СписокВидовРЦ);
	ДанныеРабочихЦентров = Запрос.Выполнить().Выгрузить();
	
	данныеЗаполнения = Новый Структура;
	данныеЗаполнения.Вставить("ГрафикиРаботыРабочихЦентров", ГрафикиРаботыРабочихЦентров);
	данныеЗаполнения.Вставить("ДанныеРабочихЦентров", ДанныеРабочихЦентров);
	данныеЗаполнения.Вставить("НачалоПериода", НачалоПериода);
	данныеЗаполнения.Вставить("ОкончаниеПериода", ОкончаниеПериода);
	данныеЗаполнения.Вставить("РасписаниеГрафиковРаботыРЦ", РасписаниеГрафиковРаботыРЦ);
	данныеЗаполнения.Вставить("ПредупрежденияПоНСИ", ПредупрежденияПоНСИ);
	
	Для каждого цВидРабочегоЦентра Из СписокВидовРЦ Цикл
		
		ЗаполнитьДоступностьПоГрафикамРаботыДляВидаРЦ(цВидРабочегоЦентра, данныеЗаполнения);
		
	КонецЦикла;
	
	РасписаниеГрафиковРаботыРЦ.Колонки.Удалить("ДатаГрафика");
	РасписаниеГрафиковРаботыРЦ.Колонки.Удалить("ВремяНачала");
	РасписаниеГрафиковРаботыРЦ.Колонки.Удалить("ВремяОкончания");
	
	Возврат РасписаниеГрафиковРаботыРЦ;
	
КонецФункции

Функция НайтиКонфликты(Знач пРасписание, массивИДКонфликтов = Неопределено) Экспорт
	
	массивИДКонфликтов = Новый Массив;
	
	тз = Новый ТаблицаЗначений;
	
	тз.Колонки.Добавить("РабочийЦентр");
	тз.Колонки.Добавить("Начало");
	тз.Колонки.Добавить("Окончание");
	
	Для каждого цСтрокаРЦ Из к2Коллекции.СгруппированнаяТаблица(пРасписание, "РабочийЦентр") Цикл
		
		цСтрокаРЦ.Таблица.Сортировать("Начало");
		
		вГраница = цСтрокаРЦ.Таблица.Количество() - 1;
		
		Для ц1 = 0 По вГраница Цикл
			
			Для ц2 = ц1 + 1 По вГраница Цикл
				
				интервал1 = цСтрокаРЦ.Таблица[ц1];
				интервал2 = цСтрокаРЦ.Таблица[ц2];
				
				Если ЕстьКонфликтУИнтервалов(интервал1, интервал2) Тогда
					
					массивИДКонфликтов.Добавить(интервал1.ИДСтроки);
					массивИДКонфликтов.Добавить(интервал2.ИДСтроки);
					
					новПересечение = тз.Добавить();
					новПересечение.РабочийЦентр = цСтрокаРЦ.РабочийЦентр;
					новПересечение.Начало       = интервал2.Начало;
					новПересечение.Окончание    = Мин(интервал1.Окончание, интервал2.Окончание);
					
				КонецЕсли;
				
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЦикла;
	
	тз.Свернуть("РабочийЦентр,Начало,Окончание");
	тз.Сортировать("Начало, Окончание");
	
	Возврат тз;
	
КонецФункции

Функция ФункционалДоступен() Экспорт

	Возврат ПолучитьФункциональнуюОпцию(Метаданные.ФункциональныеОпции.к2ПланированиеПереработкиДоступно.Имя);

КонецФункции

#Область ПартииЗапуска

Функция ИнициализироватьТаблицуДляПолученияПартийЗапуска() Экспорт

	тз = Новый ТаблицаЗначений;
	
	тз.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	тз.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	тз.Колонки.Добавить("Спецификация", Новый ОписаниеТипов("СправочникСсылка.к2РесурсныеСпецификации"));

	Возврат тз;
	
КонецФункции

Процедура ДополнитьПартиямиЗапуска(пТаблицаДанных) Экспорт

	отКоличество = Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3, ДопустимыйЗнак.Неотрицательный));
	
	пТаблицаДанных.Колонки.Добавить(
		"НомерСтроки",
		Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(10, 0, ДопустимыйЗнак.Неотрицательный)));
		
	пТаблицаДанных.Колонки.Добавить("МинимальнаяПартия", отКоличество);
	пТаблицаДанных.Колонки.Добавить("МаксимальнаяПартия", отКоличество);
	пТаблицаДанных.Колонки.Добавить("ОптимальнаяПартия", отКоличество);
	пТаблицаДанных.Колонки.Добавить("КратностьПартии", отКоличество);
	пТаблицаДанных.Колонки.Добавить("КоличествоВСпецификации", отКоличество);
	
	текНомерСтроки = 0;
	
	Для каждого цСтрока Из пТаблицаДанных Цикл
		
		цСтрока.НомерСтроки = текНомерСтроки;
		
		текНомерСтроки = текНомерСтроки + 1;
		
	КонецЦикла;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ВыходныеИзделия.Номенклатура КАК Номенклатура,
	               |	ВыходныеИзделия.Характеристика КАК Характеристика,
	               |	ВыходныеИзделия.Спецификация КАК Спецификация,
	               |	ВыходныеИзделия.НомерСтроки КАК НомерСтроки
	               |ПОМЕСТИТЬ втВыходныеИзделия
	               |ИЗ
	               |	&ВыходныеИзделия КАК ВыходныеИзделия
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	втВыходныеИзделия.Номенклатура КАК Номенклатура,
	               |	втВыходныеИзделия.Характеристика КАК Характеристика,
	               |	втВыходныеИзделия.Спецификация КАК Спецификация,
	               |	ЕСТЬNULL(к2РесурсныеСпецификацииВыходныеИзделия.МинимальнаяПартия, 0) КАК МинимальнаяПартия,
	               |	ЕСТЬNULL(к2РесурсныеСпецификацииВыходныеИзделия.МаксимальнаяПартия, 0) КАК МаксимальнаяПартия,
	               |	к2РесурсныеСпецификацииВыходныеИзделия.Количество КАК КоличествоВСпецификации,
	               |	ЕСТЬNULL(к2РесурсныеСпецификацииВыходныеИзделия.ОптимальнаяПартия, 0) КАК ОптимальнаяПартия,
	               |	ЕСТЬNULL(к2РесурсныеСпецификацииВыходныеИзделия.КратностьПартии, 0) КАК КратностьПартии,
	               |	втВыходныеИзделия.НомерСтроки КАК НомерСтроки
	               |ИЗ
	               |	втВыходныеИзделия КАК втВыходныеИзделия
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.к2РесурсныеСпецификации.ВыходныеИзделия КАК к2РесурсныеСпецификацииВыходныеИзделия
	               |		ПО втВыходныеИзделия.Спецификация = к2РесурсныеСпецификацииВыходныеИзделия.Ссылка
	               |			И втВыходныеИзделия.Номенклатура = к2РесурсныеСпецификацииВыходныеИзделия.Номенклатура
	               |			И (втВыходныеИзделия.Характеристика = к2РесурсныеСпецификацииВыходныеИзделия.Характеристика
	               |				ИЛИ к2РесурсныеСпецификацииВыходныеИзделия.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	НомерСтроки";
	Запрос.УстановитьПараметр("ВыходныеИзделия", пТаблицаДанных.Скопировать(, "Номенклатура,Характеристика,Спецификация,НомерСтроки"));
	
	выборка = Запрос.Выполнить().Выбрать();
	
	Пока выборка.Следующий() Цикл
		
		ЗаполнитьЗначенияСвойств(пТаблицаДанных[выборка.НомерСтроки],
								 выборка,
								 "МинимальнаяПартия,МаксимальнаяПартия,ОптимальнаяПартия,КратностьПартии,КоличествоВСпецификации");
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ГрафикиРаботы

// Возвращает графики работы рабочих центров.
//
// Параметры:
//  Ссылки - Массив, СправочникСсылка.РабочиеЦентры - ссылки на рабочие центры.
//
// Возвращаемое значение:
//  ТаблицаЗначений - таблица с колонками:
//		* Ссылка - СправочникСсылка.РабочиеЦентры - рабочий центр.
//		* ГрафикРаботы - СправочникСсылка.Календари - график работы.
//
Функция ГрафикиРаботыРЦ(Ссылки)
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТГрафикиРаботыРЦ(Ссылки, МенеджерВременныхТаблиц, "ВТГрафикиРаботы");
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
			|	ВТГрафикиРаботы.Ссылка       КАК Ссылка,
			|	ВТГрафикиРаботы.ГрафикРаботы КАК ГрафикРаботы
			|ИЗ
			|	ВТГрафикиРаботы КАК ВТГрафикиРаботы");
	
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

// Создает временную таблицу с графиками работы подразделений.
// Колонки таблицы: Ссылка, ГрафикРаботы.
//
// Параметры:
//  Ссылки					 - Массив, СправочникСсылка.Подразделения - ссылки на подразделения.
//  МенеджерВременныхТаблиц  - МенеджерВременныхТаблиц - менеджер, в котором будет создана временная таблица.
//  ИмяВТ					 - Строка - имя создаваемой временной таблицы.
//
Процедура СоздатьВТГрафикиРаботыПодразделений(Ссылки, МенеджерВременныхТаблиц, ИмяВТ = "ВТГрафикиРаботы")
	
	ТекстЗапроса = "ВЫБРАТЬ
		|	Подразделения.Ссылка КАК Ссылка,
		|	ВЫБОР
		|		КОГДА Подразделения.Календарь <> ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
		|			ТОГДА Подразделения.Календарь
		|		ИНАЧЕ ОсновнойКалендарьПредприятия.Значение
		|	КОНЕЦ КАК ГрафикРаботы
		|ПОМЕСТИТЬ ИмяВТ
		|ИЗ
		|	Справочник.Подразделения КАК Подразделения
		|		ЛЕВОЕ СОЕДИНЕНИЕ Константа.к2ОсновнойКалендарьПредприятия КАК ОсновнойКалендарьПредприятия
		|		ПО (ИСТИНА)
		|ГДЕ
		|	Подразделения.Ссылка В(&Ссылки)";
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "ИмяВТ", ИмяВТ);
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылки", к2КоллекцииКлиентСервер.ОбернутьВМассивПриНеобходимости(Ссылки));
	Запрос.Выполнить();
	
КонецПроцедуры

// Возвращает графики работы подразделений.
//
// Параметры:
//  Ссылки - Массив, СправочникСсылка.СтруктураПредприятия - ссылки на подразделения.
//
// Возвращаемое значение:
//  ТаблицаЗначений - таблица с колонками:
//		* Ссылка - СправочникСсылка.СтруктураПредприятия - подразделение.
//		* ГрафикРаботы - СправочникСсылка.Календари - график работы.
//
Функция ГрафикиРаботыПодразделений(Ссылки) Экспорт
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТГрафикиРаботыПодразделений(Ссылки, МенеджерВременныхТаблиц, "ВТГрафикиРаботы");
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
			|	ВТГрафикиРаботы.Ссылка       КАК Ссылка,
			|	ВТГрафикиРаботы.ГрафикРаботы КАК ГрафикРаботы
			|ИЗ
			|	ВТГрафикиРаботы КАК ВТГрафикиРаботы");
	
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

// Создает временную таблицу с графиками работы видов РЦ.
// Колонки таблицы: Ссылка, ГрафикРаботы.
//
// Параметры:
//  Ссылки					 - Массив, СправочникСсылка.ВидыРабочихЦентров - ссылки на виды РЦ.
//  МенеджерВременныхТаблиц  - МенеджерВременныхТаблиц - менеджер, в котором будет создана временная таблица.
//  ИмяВТ					 - Строка - имя создаваемой временной таблицы.
//
Процедура СоздатьВТГрафикиРаботыВидовРЦ(Ссылки, МенеджерВременныхТаблиц, ИмяВТ = "ВТГрафикиРаботы")
	
	ТекстЗапроса = "ВЫБРАТЬ
		|	к2ВидыРабочихЦентров.Ссылка КАК Ссылка,
		|	ВЫБОР
		|		КОГДА к2ВидыРабочихЦентров.Календарь <> ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
		|			ТОГДА к2ВидыРабочихЦентров.Календарь
		|		КОГДА Подразделения.Календарь <> ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
		|			ТОГДА Подразделения.Календарь
		|		ИНАЧЕ ОсновнойКалендарьПредприятия.Значение
		|	КОНЕЦ КАК ГрафикРаботы
		|ПОМЕСТИТЬ ИмяВТ
		|ИЗ
		|	Справочник.к2ВидыРабочихЦентров КАК к2ВидыРабочихЦентров
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Подразделения КАК Подразделения
		|		ПО к2ВидыРабочихЦентров.Подразделение = Подразделения.Ссылка
		|			И (к2ВидыРабочихЦентров.Календарь = ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка))
		|		ЛЕВОЕ СОЕДИНЕНИЕ Константа.к2ОсновнойКалендарьПредприятия КАК ОсновнойКалендарьПредприятия
		|		ПО (ИСТИНА)
		|ГДЕ
		|	к2ВидыРабочихЦентров.Ссылка В(&Ссылки)";
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "ИмяВТ", ИмяВТ);
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылки", к2КоллекцииКлиентСервер.ОбернутьВМассивПриНеобходимости(Ссылки));
	Запрос.Выполнить();
	
КонецПроцедуры

// Создает временную таблицу с графиками работы рабочих центров.
// Колонки таблицы: Ссылка, ГрафикРаботы.
//
// Параметры:
//  Ссылки					 - Массив, СправочникСсылка.РабочиеЦентры - ссылки на рабочие центры.
//  МенеджерВременныхТаблиц  - МенеджерВременныхТаблиц - менеджер, в котором будет создана временная таблица.
//  ИмяВТ					 - Строка - имя создаваемой временной таблицы.
//
Процедура СоздатьВТГрафикиРаботыРЦ(Ссылки, МенеджерВременныхТаблиц, ИмяВТ = "ВТГрафикиРаботы")
	
	ТекстЗапроса = "ВЫБРАТЬ
		|	к2РабочиеЦентры.Ссылка КАК Ссылка,
		|	ВЫБОР
		|		КОГДА к2РабочиеЦентры.Календарь <> ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
		|			ТОГДА к2РабочиеЦентры.Календарь
		|		КОГДА к2ВидыРабочихЦентров.Календарь <> ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
		|			ТОГДА к2ВидыРабочихЦентров.Календарь
		|		КОГДА ЕСТЬNULL(Подразделения.Календарь, ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)) <> ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка)
		|			ТОГДА Подразделения.Календарь
		|		ИНАЧЕ ОсновнойКалендарьПредприятия.Значение
		|	КОНЕЦ КАК ГрафикРаботы,
		|	к2РабочиеЦентры.Календарь КАК РЦ,
		|	к2ВидыРабочихЦентров.Календарь КАК ВидРЦ,
		|	Подразделения.Календарь КАК Подразделение,
		|	ОсновнойКалендарьПредприятия.Значение КАК Константа
		|ПОМЕСТИТЬ ВТГрафикиРаботы
		|ИЗ
		|	Справочник.к2РабочиеЦентры КАК к2РабочиеЦентры
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.к2ВидыРабочихЦентров КАК к2ВидыРабочихЦентров
		|		ПО к2РабочиеЦентры.ВидРабочегоЦентра = к2ВидыРабочихЦентров.Ссылка
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Подразделения КАК Подразделения
		|		ПО (к2ВидыРабочихЦентров.Подразделение = Подразделения.Ссылка)
		|			И (к2РабочиеЦентры.Календарь = ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка))
		|			И (к2ВидыРабочихЦентров.Календарь = ЗНАЧЕНИЕ(Справочник.Календари.ПустаяСсылка))
		|		ЛЕВОЕ СОЕДИНЕНИЕ Константа.к2ОсновнойКалендарьПредприятия КАК ОсновнойКалендарьПредприятия
		|		ПО (ИСТИНА)
		|ГДЕ
		|	(&ОтборПоРЦ
		|				И к2РабочиеЦентры.Ссылка В (&Ссылки)
		|			ИЛИ НЕ &ОтборПоРЦ
		|				И к2РабочиеЦентры.ВидРабочегоЦентра В (&Ссылки))";
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "ИмяВТ", ИмяВТ);
	
	МассивСсылок = к2КоллекцииКлиентСервер.ОбернутьВМассивПриНеобходимости(Ссылки);
	
	ОтборПоРЦ = Истина;
	
	Если МассивСсылок.Количество() > 0
		И ТипЗнч(МассивСсылок[0]) = Тип("СправочникСсылка.к2ВидыРабочихЦентров") Тогда
		
		ОтборПоРЦ = Ложь;
		
	КонецЕсли;
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылки", МассивСсылок);
	Запрос.УстановитьПараметр("ОтборПоРЦ", ОтборПоРЦ);
	Запрос.Выполнить();
	
КонецПроцедуры

#КонецОбласти

#Область ПоискКонфликтов

Функция ЕстьКонфликтУИнтервалов(Знач интервал1, Знач интервал2)
	
	Если интервал1.Этап = интервал2.Этап
		И интервал1.ЭтоВспомогательныйРЦ = интервал2.ЭтоВспомогательныйРЦ Тогда
		
		// допустимы пересечения операций в рамках одного этапа
		Возврат Ложь;
		
	КонецЕсли;
	
	Если интервал1.ПараллельнаяЗагрузка
		И интервал2.ПараллельнаяЗагрузка
		И интервал1.ВариантНаладки = интервал2.ВариантНаладки Тогда
		
		// допустимы пересечения операций при параллельной загрузке в одинаковой наладке
		Возврат Ложь;
		
	КонецЕсли;
	
	Возврат интервал1.Начало <= интервал2.Начало
		И интервал2.Начало < интервал1.Окончание;
	
КонецФункции

#КонецОбласти

Процедура ЗаполнитьДоступностьПоГрафикамРаботыДляВидаРЦ(Знач цВидРабочегоЦентра, Знач данныеЗаполнения)
	
	СтруктураПоиска = Новый Структура("ВидРабочегоЦентра", цВидРабочегоЦентра);
	списокРЦ        = данныеЗаполнения.ДанныеРабочихЦентров.НайтиСтроки(СтруктураПоиска);
	
	Если списокРЦ.Количество() = 0 Тогда
		
		лСообщения = к2КоллекцииКлиентСервер.ПолучитьИнициироватьЗначениеСоответствия(
			данныеЗаполнения.ПредупрежденияПоНСИ,
			цВидРабочегоЦентра,
			Новый Массив);
		лСообщения.Добавить(СтрШаблон(НСтр("ru = 'Для вида рабочих центров ""%1"" не введены рабочие центры.'"),
			цВидРабочегоЦентра));
		
		Возврат;
		
	КонецЕсли;
	
	ДанныеПоРЦ = Новый Массив;
	
	Для каждого ДанныеРЦ Из списокРЦ Цикл
		
		МассивГрафиков = Новый Массив;
		
		СтруктураПоиска = Новый Структура("Ссылка", ДанныеРЦ.РабочийЦентр);
		СтрокиГрафики   = данныеЗаполнения.ГрафикиРаботыРабочихЦентров.НайтиСтроки(СтруктураПоиска);
		
		Для каждого цСтрокаГрафика Из СтрокиГрафики Цикл
			
			Если ЗначениеЗаполнено(цСтрокаГрафика.ГрафикРаботы) Тогда
				
				МассивГрафиков.Добавить(цСтрокаГрафика.ГрафикРаботы);
				
			КонецЕсли;
			
		КонецЦикла;
		
		Если МассивГрафиков.Количество() > 0 Тогда
			
			ОписаниеРЦ = Новый Структура;
			ОписаниеРЦ.Вставить("РабочийЦентр", ДанныеРЦ.РабочийЦентр);
			ОписаниеРЦ.Вставить("ГрафикиРаботы", МассивГрафиков);
			ДанныеПоРЦ.Добавить(ОписаниеРЦ);
			
		Иначе
			
			лСообщения = к2КоллекцииКлиентСервер.ПолучитьИнициироватьЗначениеСоответствия(
				данныеЗаполнения.ПредупрежденияПоНСИ,
				ДанныеРЦ.РабочийЦентр,
				Новый Массив);
			лСообщения.Добавить(СтрШаблон(НСтр("ru = 'Для рабочего центра ""%1"" не указан график работы.'"),
				ДанныеРЦ.РабочийЦентрПредставление));
			
		КонецЕсли;
		
	КонецЦикла;
	
	Если ДанныеПоРЦ.Количество() = 0 Тогда
		
		Возврат;
		
	КонецЕсли;
	
	ЗаполнитьГрафикиРаботыРЦ(цВидРабочегоЦентра, ДанныеПоРЦ, данныеЗаполнения);

КонецПроцедуры

// Возвращает расписание работы заданное в графиках работы
//
// Параметры:
//	СписокГрафиков - Массив - список календарей
//	НачалоПериода - Дата - начало периода, за который нужно составить расписания
//	ОкончаниеПериода - Дата - окончания периода.
//
// Возвращаемое значение:
//   ТаблицаЗначений   - Расписание работы.
//
Функция РасписаниеРаботыПоГрафику(СписокГрафиков, НачалоПериода, ОкончаниеПериода)
	
	Расписание = КалендарныеГрафики.РасписанияРаботыНаПериод(СписокГрафиков, НачалоПериода, ОкончаниеПериода);
	
	Расписание.Колонки.Добавить("Длительность", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(10)));
	
	Для каждого цСтрокаРасписания Из Расписание Цикл
		
		Если цСтрокаРасписания.ВремяНачала = NULL Тогда
			
			цСтрокаРасписания.ВремяНачала = '000101010000';
			
		КонецЕсли;
		
		Если цСтрокаРасписания.ВремяОкончания = NULL Тогда
			
			цСтрокаРасписания.ВремяОкончания = '00010101235959';
			
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(цСтрокаРасписания.ВремяОкончания)
			ИЛИ цСтрокаРасписания.ВремяОкончания = '000101012359'
			ИЛИ цСтрокаРасписания.ВремяОкончания = '00010101235959' Тогда
			
			цСтрокаРасписания.ВремяОкончания = '00010101235959';
			Длительность                     = КонецДня(цСтрокаРасписания.ВремяОкончания) - цСтрокаРасписания.ВремяНачала + 1;
			
		Иначе
			
			Длительность = цСтрокаРасписания.ВремяОкончания - цСтрокаРасписания.ВремяНачала;
			
		КонецЕсли;
		
		цСтрокаРасписания.Длительность = Длительность;
		
	КонецЦикла;
	
	Возврат Расписание;
	
КонецФункции

Функция СоздатьТаблицуДоступностиРЦ()
	
	отДатаВремя = Новый ОписаниеТипов("Дата", , , Новый КвалификаторыДаты(ЧастиДаты.ДатаВремя));
	отВремя     = Новый ОписаниеТипов("Дата", , , Новый КвалификаторыДаты(ЧастиДаты.Время));
	
	тз = Новый ТаблицаЗначений;
	тз.Колонки.Добавить("ВидРабочегоЦентра", Новый ОписаниеТипов("СправочникСсылка.к2ВидыРабочихЦентров"));
	тз.Колонки.Добавить("РабочийЦентр", Новый ОписаниеТипов("СправочникСсылка.к2РабочиеЦентры"));
	тз.Колонки.Добавить("Окончание", отДатаВремя);
	тз.Колонки.Добавить("Начало", отДатаВремя);
	тз.Колонки.Добавить("ДатаГрафика", Новый ОписаниеТипов("Дата", , , Новый КвалификаторыДаты(ЧастиДаты.Дата)));
	тз.Колонки.Добавить("ВремяНачала", отВремя);
	тз.Колонки.Добавить("ВремяОкончания", отВремя);
	
	Возврат тз;
	
КонецФункции

Процедура ЗаполнитьГрафикиРаботыРЦ(ВидРабочегоЦентра, ДанныеПоРЦ, Знач данныеЗаполнения)
	
	РасписаниеГрафиковРаботыРЦКопия = данныеЗаполнения.РасписаниеГрафиковРаботыРЦ.Скопировать();
	
	// Удалим старые данные по указанным РЦ
	СтруктураПоиска      = Новый Структура("ВидРабочегоЦентра", ВидРабочегоЦентра);
	СтарыеДанныеПоВидуРЦ = данныеЗаполнения.РасписаниеГрафиковРаботыРЦ.НайтиСтроки(СтруктураПоиска);
	
	// Получим расписания работы
	СписокГрафиков = Новый Массив;
	
	Для каждого СтрокаРЦ Из ДанныеПоРЦ Цикл
		
		Для каждого ГрафикРаботы Из СтрокаРЦ.ГрафикиРаботы Цикл
			
			Если СписокГрафиков.Найти(ГрафикРаботы) = Неопределено Тогда
				
				СписокГрафиков.Добавить(ГрафикРаботы);
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЦикла;
	
	РасписанияРаботы = РасписаниеРаботыПоГрафику(СписокГрафиков, данныеЗаполнения.НачалоПериода, данныеЗаполнения.ОкончаниеПериода);
	
	отказ = Ложь;
	
	Для каждого СтрокаРЦ Из ДанныеПоРЦ Цикл
		
		ЗаполнитьГрафикиРаботыПоСтрокеРЦ(ВидРабочегоЦентра,
										 данныеЗаполнения,
										 РасписанияРаботы,
										 СтарыеДанныеПоВидуРЦ,
										 СтрокаРЦ,
										 отказ);
		
		Если отказ Тогда
			
			данныеЗаполнения.РасписаниеГрафиковРаботыРЦ = РасписаниеГрафиковРаботыРЦКопия;
			
			Возврат;
		
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьГрафикиРаботыПоСтрокеРЦ(Знач ВидРабочегоЦентра,
										   Знач данныеЗаполнения,
										   Знач РасписанияРаботы,
										   Знач СтарыеДанныеПоВидуРЦ,
										   Знач СтрокаРЦ,
												Отказ)
												
	// Удалим старые данные по РЦ
	МассивУдаленныхСтрок = Новый Массив;
	
	Для ИндексСтроки = 0 По СтарыеДанныеПоВидуРЦ.ВГраница() Цикл
		
		СтрокаРасписание = СтарыеДанныеПоВидуРЦ[ИндексСтроки];
		
		Если СтрокаРасписание.ДатаГрафика >= данныеЗаполнения.НачалоПериода
			И СтрокаРасписание.ДатаГрафика <= данныеЗаполнения.ОкончаниеПериода
			И СтрокаРасписание.РабочийЦентр = СтрокаРЦ.РабочийЦентр Тогда
			
			данныеЗаполнения.РасписаниеГрафиковРаботыРЦ.Удалить(СтрокаРасписание);
			МассивУдаленныхСтрок.Добавить(ИндексСтроки);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Для ИндексСтроки = -МассивУдаленныхСтрок.ВГраница() По 0 Цикл
		
		СтарыеДанныеПоВидуРЦ.Удалить(МассивУдаленныхСтрок[- ИндексСтроки]);
		
	КонецЦикла;
	
	предварительноеРасписание = данныеЗаполнения.РасписаниеГрафиковРаботыРЦ.СкопироватьКолонки();
	
	ДатаПериода = данныеЗаполнения.НачалоПериода;
	
	Пока ДатаПериода <= данныеЗаполнения.ОкончаниеПериода Цикл
		
		Интервалы = Новый Массив;
		
		СтруктураПоиска = Новый Структура;
		СтруктураПоиска.Вставить("ДатаГрафика", ДатаПериода);
		
		Для каждого ГрафикРаботы Из СтрокаРЦ.ГрафикиРаботы Цикл
			
			СтруктураПоиска.Вставить("ГрафикРаботы", ГрафикРаботы);
			РасписаниеПоГрафику = РасписанияРаботы.НайтиСтроки(СтруктураПоиска);
			
			Для каждого цСтрокаИнтервал Из РасписаниеПоГрафику Цикл
				
				// Проверка пересечения интервалов
				Если Не ПроверитьПересечениеГрафиковРаботы(цСтрокаИнтервал, Интервалы, СтрокаРЦ.РабочийЦентр, ДатаПериода) Тогда
					
					Отказ = Истина;
					
					Возврат;
					
				КонецЕсли;
				
				СтрокаРасписание = предварительноеРасписание.Добавить();
				СтрокаРасписание.ВидРабочегоЦентра = ВидРабочегоЦентра;
				СтрокаРасписание.РабочийЦентр      = СтрокаРЦ.РабочийЦентр;
				СтрокаРасписание.Начало            = ДатаПлюсВремя(цСтрокаИнтервал.ДатаГрафика, цСтрокаИнтервал.ВремяНачала);
				СтрокаРасписание.Окончание         = ДатаПлюсВремя(цСтрокаИнтервал.ДатаГрафика, цСтрокаИнтервал.ВремяОкончания);
				
				СтрокаРасписание.ВремяНачала    = цСтрокаИнтервал.ВремяНачала;
				СтрокаРасписание.ВремяОкончания = цСтрокаИнтервал.ВремяОкончания;
				
				Интервалы.Добавить(СтрокаРасписание);
				
			КонецЦикла;
			
		КонецЦикла;
		
		ДатаПериода = КонецДня(ДатаПериода) + 1;
		
	КонецЦикла;
	
	ПеренестиПредварительноеРасписаниеВОсновное(предварительноеРасписание, данныеЗаполнения.РасписаниеГрафиковРаботыРЦ);
	
КонецПроцедуры

Функция ПроверитьПересечениеГрафиковРаботы(Знач Интервал, Знач Интервалы, Знач пРабочийЦентр, Знач пДатаПериода)

	Для каждого цИнтервал Из Интервалы Цикл
		
		Если ИнтервалыПересекаются(Интервал, цИнтервал) Тогда
			
			ТекстСообщения = СтрШаблон(НСтр("ru = 'Графики работы рабочего центра ""%1"" пересекаются %2.'"),
				пРабочийЦентр,
				Формат(пДатаПериода, "ДЛФ=D"));
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, пРабочийЦентр);
			
			Возврат Ложь;
			
		КонецЕсли;
		
	КонецЦикла;

	Возврат Истина;
	
КонецФункции

Процедура ПеренестиПредварительноеРасписаниеВОсновное(Знач пПредварительноеРасписание, Знач пРасписание)
	
	Если пПредварительноеРасписание.Количество() = 0 Тогда
		
		Возврат;
	
	КонецЕсли;
	
	// Удалим промежутки в минуту и переходы через 00:00 для получения непрерывных интервалов.
	
	предСтрока = пРасписание.Добавить();
	ЗаполнитьЗначенияСвойств(предСтрока, пПредварительноеРасписание[0]);
	
	Для ц = 1 По пПредварительноеРасписание.Количество() - 1 Цикл
		
		текСтрока = пПредварительноеРасписание[ц];
		
		Если текСтрока.Начало - предСтрока.Окончание <= к2ИК.СекундВМинуте() Тогда
			
			предСтрока.Окончание = текСтрока.Окончание;
			
		Иначе
			
			предСтрока = пРасписание.Добавить();
			ЗаполнитьЗначенияСвойств(предСтрока, текСтрока);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Функция ИнтервалыПересекаются(Знач пИнтервал1, Знач пИнтервал2)

	Если пИнтервал1.ВремяНачала >= пИнтервал2.ВремяНачала
		И пИнтервал1.ВремяНачала < пИнтервал2.ВремяОкончания Тогда
		
		Возврат Истина;
	
	КонецЕсли;
	
	Если пИнтервал1.ВремяОкончания > пИнтервал2.ВремяНачала
		И пИнтервал1.ВремяОкончания <= пИнтервал2.ВремяОкончания Тогда
		
		Возврат Истина;
	
	КонецЕсли;
	
	Если пИнтервал1.ВремяНачала < пИнтервал2.ВремяНачала
		И пИнтервал1.ВремяОкончания > пИнтервал2.ВремяОкончания Тогда
		
		Возврат Истина;
	
	КонецЕсли;
	
	Возврат Ложь;

КонецФункции

Функция ДатаПлюсВремя(Знач пДата, Знач пВремя)
	
	секунд = пВремя - НачалоДня(пВремя);
	
	Возврат НачалоДня(пДата) + секунд;
	
КонецФункции

#КонецОбласти

