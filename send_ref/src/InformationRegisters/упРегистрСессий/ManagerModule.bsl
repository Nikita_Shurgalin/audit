#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура Заблокировать( пЗадача, пСессия, пСостояние ) Экспорт
	
	Блокировка = Новый БлокировкаДанных;
	ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.упРегистрСессий");
	ЭлементБлокировки.УстановитьЗначение( "Задача", пЗадача );
	ЭлементБлокировки.УстановитьЗначение( "Сессия", пСессия );
	ЭлементБлокировки.УстановитьЗначение( "Состояние", пСостояние );
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
	Блокировка.Заблокировать();
	
КонецПроцедуры


Процедура Удалить( пЗадача ) Экспорт
	
	нз = РегистрыСведений.упРегистрСессий.СоздатьНаборЗаписей();
	
	нз.Отбор.Задача.Установить( пЗадача );
	
	нз.Записать();
	
КонецПроцедуры

Процедура УдалитьПоСессии( пСессия ) Экспорт
	
	нз = РегистрыСведений.упРегистрСессий.СоздатьНаборЗаписей();
	
	нз.Отбор.Сессия.Установить( пСессия );
	
	нз.Записать();
	
КонецПроцедуры

#КонецЕсли