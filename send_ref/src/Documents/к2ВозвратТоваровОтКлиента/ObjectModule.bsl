#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	естьОснование = ТипЗнч(ДанныеЗаполнения) = Тип("Структура")
		И ДанныеЗаполнения.Свойство("Основание")
		И ЗначениеЗаполнено(ДанныеЗаполнения.Основание);
	
	Если естьОснование
		И ТипЗнч(ДанныеЗаполнения.Основание) = Тип("ДокументСсылка.к2ЗаявкаНаВозвратТоваровОтКлиента") Тогда

		основаниеЗаполнения = ДанныеЗаполнения.Основание;

	Иначе

		основаниеЗаполнения = ДанныеЗаполнения;

	КонецЕсли;
	
	ЗаполнитьПоЗаявке(основаниеЗаполнения);
	ЗаполнитьПоРеализации(основаниеЗаполнения);
	ЗаполнитьПоСтруктуре(основаниеЗаполнения);
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	к2Документы.ПриКопировании(ЭтотОбъект);
	
	Справочники.к2СоглашенияСКлиентами.ПроверитьДействиеСоглашения(ЭтотОбъект, ТекущаяДатаСеанса());
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ);
	
	НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
	ПараметрыПроверкиГрафикаОплаты = к2Взаиморасчеты.ПустаяСтруктураПроверкиЗаполнения();
	к2Взаиморасчеты.ПроверитьЗаполнениеГрафикаОплаты(ЭтотОбъект, ПараметрыПроверкиГрафикаОплаты, Отказ);
		
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);

	к2ПроведениеСервер.ПередЗаписью(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	параметрыДокумента = к2ДокументыКлиентСервер.ПараметрыДокумента();
	параметрыДокумента.ВернутьМногооборотнуюТару = ВернутьМногооборотнуюТару;
	к2Документы.ЗаполнитьИтоги(ЭтотОбъект, параметрыДокумента);

	к2Взаиморасчеты.ЗаполнитьЭтапыГрафикаОплаты(ЭтотОбъект);

КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	к2ПроведениеСервер.ОбработкаПроведения(ЭтотОбъект, Отказ, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	к2ПроведениеСервер.ОбработкаУдаленияПроведения(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Процедура СформироватьСписокРегистровДляКонтроля() Экспорт
	
	Массив = Новый Массив;
	
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ИнициализацияИЗаполнение

Процедура ЗаполнитьПоЗаявке(Знач ДанныеЗаполнения)
	
	Если Не ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.к2ЗаявкаНаВозвратТоваровОтКлиента") Тогда
		Возврат;
	КонецЕсли;

	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения, , "СпособСоздания,Ответственный,Комментарий,Номер,Дата,Проведен");

	Основание = ДанныеЗаполнения;
	Товары.Загрузить(ДанныеЗаполнения.Товары.Выгрузить());

КонецПроцедуры

Процедура ЗаполнитьПоРеализации(Знач ДанныеЗаполнения)
	
	Если Не ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда
		Возврат;
	КонецЕсли;

	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения, , "СпособСоздания,Ответственный,Комментарий,Номер,Дата,Проведен");

	Основание = ДанныеЗаполнения;
	Товары.Загрузить(ДанныеЗаполнения.Товары.Выгрузить());

КонецПроцедуры

Процедура ЗаполнитьПоСтруктуре(Знач ДанныеЗаполнения)

	к2ОбщегоНазначения.ЗаполнитьОбъектПоСтруктуре(ЭтотОбъект, ДанныеЗаполнения);

КонецПроцедуры

Процедура ЗаполнитьРеквизитыПоУмолчанию()

	к2Документы.ЗаполнитьРеквизитыПоУмолчанию(ЭтотОбъект);
	к2Продажи.ЗаполнитьКонтрагентаПоУмолчанию(ЭтотОбъект);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли