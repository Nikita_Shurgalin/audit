

//////////////////////////////////////////////////////////////////////////////////////
//ОБРАБОТЧИКИ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьПараметрыДинамическимСпискам();
	УстановитьНаименование();
	НастроитьВидимостьДоступностьДляМножественнойАдресации();
	
	Если ЭтоСтарт() Тогда
		
		// задача старт всегда назначается владельцу сессии
		Элементы.Группа_Адресация.Видимость = Ложь;
		
		Элементы.Группа_СдвигГруппы.Видимость = Ложь;
		
	КонецЕсли;
	
	НастроитьВидимостьЗапретаНаАвтоЗавершение();
	
	ЗаполнитьМоментВыполненияАлгоритмам();
	ЗаполнитьДеревоРеквизитов();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Не ( Объект.ТипСостояния = ПредопределенноеЗначение( "Перечисление.упТипыСостояний.Начало" )
		ИЛИ Объект.ТипСостояния = ПредопределенноеЗначение( "Перечисление.упТипыСостояний.Действие" ) ) Тогда
		
		Отказ = Истина;
		
		упГрафическаяСхемаКлиент.ОткрытьФормуСостояния( Объект.Ссылка , ВладелецФормы );
		
		Возврат;
		
	КонецЕсли;
	
	УстановитьСтаруюКоллекциюОбхода();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить( упИК.Событие_ОбновитьКартуМаршрута() , Объект.Владелец );
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = упИК.ОбновленаДинамическаяАдресация()
		И Параметр = Объект.Ссылка Тогда
		
		Элементы.ДинамическаяАдресация.Обновить();
		
	КонецЕсли;
	
	Если ИмяСобытия = упИК.Событие_ДобавленыАлгоритмы()
		И Параметр = Объект.Ссылка Тогда
		
		Прочитать();
		
	КонецЕсли;
	
	Если ИмяСобытия = упИК.Событие_ДобавленыУсловия() 
		И Параметр = Объект.Ссылка Тогда 
		
		Прочитать();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	Описание = ТекущийОбъект.хзОписание.Получить();
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	ТекущийОбъект.хзОписание = Новый ХранилищеЗначения( Описание );
	ТекущийОбъект.Описание = Описание.ПолучитьТекст();
	
КонецПроцедуры


&НаКлиенте
Процедура ИмяПриИзменении(Элемент)
	
	УстановитьНаименование();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьНаименование()
	
	Если Объект.Наименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( старИмя )
		ИЛИ ПустаяСтрока( Объект.Наименование ) Тогда
		
		Объект.Наименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( Объект.Имя );
		
	КонецЕсли;
	
	старИмя = Объект.Имя;
	
КонецПроцедуры	//УстановитьНаименование

&НаСервере
Процедура НастроитьВидимостьЗапретаНаАвтоЗавершение()
	
	Если ЭтоСтарт() Тогда
		
		Элементы.Группа_ЗапретНаАвтозавершение.Видимость = Истина;
		Элементы.Группа_ЗапретНаАвтозавершение.Доступность = ( Объект.Условия.Количество() = 0 );
		
	Иначе
		
		Элементы.Группа_ЗапретНаАвтозавершение.Видимость = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры

Функция ЭтоСтарт()
	
	Возврат Перечисления.упТипыСостояний.ЭтоСтарт( Объект.ТипСостояния );
	
КонецФункции


//////////////////////////////////////////////////////////////////////////////////////
//ДИНАМИЧЕСКАЯ АДРЕСАЦИЯ {

&НаСервере
Процедура УстановитьПараметрыДинамическимСпискам()
	
	ДинамическаяАдресация.Параметры.УстановитьЗначениеПараметра( "СостояниеПриемник" , Объект.Ссылка );
	
	ЭлементОтбора = СписокЭлементов.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных( "Владелец" );
	ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ЭлементОтбора.ПравоеЗначение = Объект.Ссылка;
	
КонецПроцедуры	//УстановитьПараметрыДинамическимСпискам

&НаКлиенте
Процедура НастроитьДинамическуюАдресацию(Команда)
	
	упОбщегоНазначенияКлиент.НастроитьДинамическуюАдресацию( Объект.Ссылка , ЭтотОбъект );
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьВДинАдресациюВсеСостоянияСтарт(Команда)
	
	ДобавитьВсеСостоянияСтарт();
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьВсеСостоянияСтарт()
	
	РегистрыСведений.упАдресация.ДобавитьВсеСостоянияСтарт( Объект.Ссылка , Объект.Владелец );
	
	Элементы.ДинамическаяАдресация.Обновить();
	
КонецПроцедуры	//ДобавитьВсеСостоянияСтарт


&НаКлиенте
Процедура ДобавитьВДинАдресациюВсеПредыдущиеСостояния(Команда)
	
	ДобавитьПредыдущиеСостояния();
	
	Элементы.ДинамическаяАдресация.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьПредыдущиеСостояния()
	
	РегистрыСведений.упАдресация.ДобавитьПредыдущиеСостояния( Объект.Ссылка , Объект.Владелец );
	
КонецПроцедуры	//ДобавитьВсеСостоянияСтарт

&НаКлиенте
Процедура ОчиститьДинАдресацию(Команда)
	
	ОчиститьДинАдресациюСервер();
	
	Элементы.ДинамическаяАдресация.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ОчиститьДинАдресациюСервер()
	
	РегистрыСведений.упАдресация.Очистить( Объект.Ссылка );
	
КонецПроцедуры	//ДобавитьВсеСостоянияСтарт

//}

//////////////////////////////////////////////////////////////////////////////////////
//УСЛОВИЯ {

&НаКлиенте
Процедура УсловияУсловиеНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	упУсловияКлиент.НачалоВыбораУсловияВСостоянии( Объект , Элемент );
	
КонецПроцедуры

&НаКлиенте
Процедура УсловияПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	упУсловияКлиент.ПередНачаломДобавления( ЭтотОбъект , Элемент , Отказ );
	
КонецПроцедуры

&НаКлиенте
Процедура УсловияОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	упУсловияКлиент.ОбработкаВыбора( Объект.Условия , ВыбранноеЗначение );
	
	НастроитьВидимостьЗапретаНаАвтоЗавершение();
	
КонецПроцедуры

&НаКлиенте
Процедура УсловияПриИзменении(Элемент)
	
	НастроитьВидимостьЗапретаНаАвтоЗавершение();
	
КонецПроцедуры

&НаКлиенте
Процедура ПодборУсловий(Команда)
	
	упУсловияКлиент.НачалоВыбораУсловияВСостоянии( Объект , Элементы.Условия, Истина );
	
КонецПроцедуры


//}

//////////////////////////////////////////////////////////////////////////////////////
//АЛГОРИТМЫ {

&НаКлиенте
Процедура ТаблицаАлгоритмовАлгоритмНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	упРаботаСФормойКлиент.ВыбратьАлгоритм( ЭтотОбъект , Элемент );
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаАлгоритмовПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	упРаботаСФормойКлиент.ТаблицаАлгоритмовПередНачаломДобавления( ЭтотОбъект , Элемент , Отказ );
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаАлгоритмовОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	упРаботаСФормойКлиент.ТаблицаАлгоритмовОбработкаВыбора( Объект.ТаблицаАлгоритмов , ВыбранноеЗначение );
	
	ЗаполнитьМоментВыполненияАлгоритмам();
	
КонецПроцедуры

Процедура ЗаполнитьМоментВыполненияАлгоритмам()
	
	Для Каждого цСтрока Из Объект.ТаблицаАлгоритмов Цикл
		
		Если Не ЗначениеЗаполнено( цСтрока.МоментВыполненияАлгоритма ) Тогда
			
			цСтрока.МоментВыполненияАлгоритма = Перечисления.упМоментВыполненияАлгоритма.ПриСозданииЗадачи;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры


//}

//////////////////////////////////////////////////////////////////////////////////////
//КОМАНДЫ ПРОЦЕССА {

&НаКлиенте
Процедура КомандыПроцессаКомандаПроцессаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	упРаботаСФормойКлиент.ВыбратьКомандуПроцесса( ЭтотОбъект , Элемент );
	
КонецПроцедуры

&НаКлиенте
Процедура КомандыПроцессаПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	упРаботаСФормойКлиент.Команды_ПередНачаломДобавления( ЭтотОбъект , Элемент , Отказ );
	
КонецПроцедуры

&НаКлиенте
Процедура КомандыПроцессаОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	упРаботаСФормойКлиент.Команды_ОбработкаВыбора( Объект.КомандыПроцесса , ВыбранноеЗначение );
	
КонецПроцедуры

//}



//////////////////////////////////////////////////////////////////////////////////////
//МНОЖЕСТВЕННАЯ АДРЕСАЦИЯ {

&НаСервере
Процедура НастроитьВидимостьДоступностьДляМножественнойАдресации()
	
	Элементы.Группа_ПараметрыМножественнойАдресации.Видимость = Объект.МножественнаяАдресация;
	
	Элементы.КоллекцияОбхода.СписокВыбора.ЗагрузитьЗначения( Справочники.упВерсии.ПолучитьМассивКоллекцийОбходаВерсии( Объект.Владелец ) );
	
КонецПроцедуры	//НастроитьВидимостьДоступностьДляМножественнойАдресации

&НаКлиенте
Процедура МножественнаяАдресацияПриИзменении(Элемент)
	
	НастроитьВидимостьДоступностьДляМножественнойАдресации();
	
	ОчиститьДанныеПриИзмененииИсточникаРеквизитов();
	
КонецПроцедуры

&НаКлиенте
Процедура КоллекцияОбходаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	упРеквизитыКлиент.НачалоВыбораТЧВерсииБП( Объект.Владелец , Элемент );
	
КонецПроцедуры

&НаКлиенте
Процедура КоллекцияОбходаОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = РеквизитКорректенДляКоллекцииОбхода( ВыбранноеЗначение );
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция РеквизитКорректенДляКоллекцииОбхода( пРеквизит )
	
	Возврат Справочники.упРеквизиты.РеквизитКорректенДляКоллекцииОбхода( пРеквизит );
	
КонецФункции	//РеквизитКорректенДляКоллекцииОбхода

&НаКлиенте
Процедура КоллекцияОбходаПриИзменении(Элемент)
	
	ПроверитьКоллекциюОбхода(старКоллекцияОбхода);
	
	Если Не старКоллекцияОбхода = Объект.КоллекцияОбхода Тогда
		
		ОчиститьДанныеПриИзмененииИсточникаРеквизитов();
		
		УстановитьСтаруюКоллекциюОбхода();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьКоллекциюОбхода( старКоллекцияОбхода )
	
	Если Не ЗначениеЗаполнено( Объект.КоллекцияОбхода ) Тогда
		Возврат;
	КонецЕсли;
	
	массивКоллекцийОбхода = Справочники.упВерсии.ПолучитьМассивКоллекцийОбходаВерсии( Объект.Владелец );
	
	Если массивКоллекцийОбхода.Найти( Объект.КоллекцияОбхода ) = Неопределено Тогда
		
		шаблонТекста = НСтр( "ru='Коллекция обхода ""%1"" не найден в реквизитах версии'" );
		текстСообщения = стрЗаполнить( шаблонТекста, Объект.КоллекцияОбхода );
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения ,,"Объект.КоллекцияОбхода");
		
		Объект.КоллекцияОбхода = старКоллекцияОбхода;
		
	КонецЕсли;
	
КонецПроцедуры


&НаКлиенте
Процедура ОчиститьДанныеПриИзмененииИсточникаРеквизитов()
	
	Объект.Условия.Очистить();
	Объект.ТаблицаАлгоритмов.Очистить();
	Объект.КомандыПроцесса.Очистить();
	
	ЗаполнитьДеревоРеквизитов();
	
КонецПроцедуры	//ОчиститьДанныеПриИзмененииИсточникаРеквизитов


&НаКлиенте
Процедура УстановитьСтаруюКоллекциюОбхода()
	
	старКоллекцияОбхода = Объект.КоллекцияОбхода;
	
КонецПроцедуры

&НаКлиенте
Процедура КоллекцияОбходаАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	ЗаполнитьДанныеВыбораКоллекцииОбходаПриАвтоПодборе( ДанныеВыбора, Текст, СтандартнаяОбработка );
	
КонецПроцедуры

&НаКлиенте
Процедура КоллекцияОбходаОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗаполнитьДанныеВыбораКоллекцииОбходаПриАвтоПодборе( ДанныеВыбора, Текст, СтандартнаяОбработка );
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеВыбораКоллекцииОбходаПриАвтоПодборе( ДанныеВыбора, Текст, СтандартнаяОбработка )
	
	ДанныеВыбора = Новый СписокЗначений;
	
	ДанныеВыбора.ЗагрузитьЗначения( упРеквизитыСервер.МассивАвтоПодбораКоллекцииОбхода( Объект.Владелец, Текст, СтандартнаяОбработка ) );
	
КонецПроцедуры




&НаКлиенте
Процедура ИмяФормыЗадачиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	упОбщегоНазначенияКлиент.ВыбратьИмяФормы( Элемент , Объект.ИмяФормыЗадачи , Истина );
	
КонецПроцедуры


////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ ПОДСИСТЕМЫ НАСТРОЙКИ ПОРЯДКА ЭЛЕМЕНТОВ

&НаКлиенте
Процедура ПереместитьЭлементВверх()
	
	ТекущиеДанные = Элементы.СписокЭлементов.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда 
		ПереместитьЭлементНаСервере(ТекущиеДанные.Ссылка, "Вверх");
	КонецЕсли;
	
	Элементы.СписокЭлементов.Обновить();
	
КонецПроцедуры

&НаКлиенте
Процедура ПереместитьЭлементВниз()
	
	ТекущиеДанные = Элементы.СписокЭлементов.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда
		ПереместитьЭлементНаСервере(ТекущиеДанные.Ссылка, "Вниз");
	КонецЕсли;
	
	Элементы.СписокЭлементов.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ПереместитьЭлементНаСервере(ТекущийЭлемент, Направление)

	НастройкаПорядкаЭлементовСлужебный.ПереместитьЭлемент(Элементы.СписокЭлементов, ТекущийЭлемент, Направление);

КонецПроцедуры

&НаКлиенте
Процедура ДобавитьЭлемент(Команда)
	
	Если Модифицированность Тогда
		
		текстВопроса = НСтр( "ru='Для добавление элемента необходимо записать состояние. Записать?'" );
		
		КнопкаВыполнитьДействие = НСтр("ru='Записать';en='Write'" );
		КнопкаОтмена = НСтр("ru='Отмена';en='Cancel'" );
		
		сзКнопки = Новый СписокЗначений;
		сзКнопки.Добавить( КнопкаВыполнитьДействие );
		сзКнопки.Добавить( КнопкаОтмена );
		
		ЗаголовокВопроса = НСтр( "ru='Записать и продолжить?'" );
		
		ПоказатьВопрос(
			Новый ОписаниеОповещения("ДобавитьЭлементЗавершение", ЭтотОбъект, Новый Структура("КнопкаВыполнитьДействие", КнопкаВыполнитьДействие)),
			текстВопроса,
			сзКнопки,
			,
			КнопкаВыполнитьДействие,
			ЗаголовокВопроса);
		
	Иначе
		
		ДобавлениеЭлемента();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьЭлементЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = ДополнительныеПараметры.КнопкаВыполнитьДействие Тогда
		
		Записать();
		ДобавлениеЭлемента();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавлениеЭлемента()
	
	текРодитель = Элементы.СписокЭлементов.ТекущаяСтрока;
	
	видРодителя = глРеквизит( текРодитель, "Вид" );
	
	Если упРаботаСФормойКлиентСервер.ЭтоПоле( видРодителя ) Тогда
		
		структРодителя = глСтруктураРеквизитов( текРодитель, "Родитель, Родитель.Вид" );
		
		текРодитель = структРодителя.Родитель;
		видРодителя = структРодителя.РодительВид;
		
	КонецЕсли;
	
	списокВыбора = упРаботаСФормойКлиент.ДопустимыеВидыЭлементов( видРодителя );
	
	выбранныйТип = списокВыбора.ВыбратьЭлемент( НСтр( "ru='Выберите тип элемента'" ) );
	
	Если выбранныйТип = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	новЭлемент = СоздатьЭлементФормы( Объект.Ссылка, текРодитель, выбранныйТип.Значение );
	
	Элементы.СписокЭлементов.Обновить();
	
	ПоказатьЗначение(, новЭлемент );
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция СоздатьЭлементФормы( пВладелец, пРодитель, пВид, пРеквизит = Неопределено )
	
	структЗаполнения = Новый Структура( "Владелец, Родитель, Вид, СсылкаНаРеквизит", пВладелец, пРодитель, пВид, пРеквизит );
	
	Если пВид = упИК.Поле() Тогда
		
		новЭлемент = Справочники.упЭлементыФормы.СоздатьЭлемент();
		
	Иначе
		
		новЭлемент = Справочники.упЭлементыФормы.СоздатьГруппу();
		
	КонецЕсли;
	
	новЭлемент.Заполнить( структЗаполнения );
	новЭлемент.Записать();
	
	Возврат новЭлемент.Ссылка;
	
КонецФункции


&НаКлиенте
Процедура ПроверитьФорму(Команда)
	
	ОбновитьПовторноИспользуемыеЗначения();
	
	параметрыЗадачи = упУправлениеЗадачамиКлиент.СтруктураЗаполненияТестовойФормы( Объект );
	
	Если ЗначениеЗаполнено( Объект.ИмяФормыЗадачи ) Тогда
		
		ОткрытьФорму( Объект.ИмяФормыЗадачи , параметрыЗадачи, ЭтотОбъект );
		
	Иначе
		
		ОткрытьФорму( "Задача.упЗадача.ФормаОбъекта" , параметрыЗадачи, ЭтотОбъект );
		
	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура СписокЭлементовПроверкаПеретаскивания(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка, Строка, Поле)
	
	Если Не РазрешитьДобавление( ПараметрыПеретаскивания.Значение, Строка ) Тогда
		
		ПараметрыПеретаскивания.Действие = ДействиеПеретаскивания.Отмена;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокЭлементовПеретаскивание(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка, Строка, Поле)
	
	Если ТипЗнч( ПараметрыПеретаскивания.Значение ) = Тип( "Массив" )
		И ПараметрыПеретаскивания.Значение.Количество() > 0
		И ТипЗнч( ПараметрыПеретаскивания.Значение[0] ) = Тип( "ДанныеФормыЭлементДерева" ) Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ДобавитьЭлементыИзДереваРеквизитов( ПараметрыПеретаскивания.Значение, Строка );
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Функция РазрешитьДобавление( пЗначениеПеретаскивания, пРодитель )
	
	разрешитьДобавление = Истина;
	
	видРодителя = глРеквизит_Кэш( пРодитель, "Вид" );
	
	Если упРаботаСФормойКлиентСервер.ЭтоСтраницы( видРодителя ) Тогда
		
		разрешитьДобавление = ПроверитьПеретаскиваниеВСтраницы( пЗначениеПеретаскивания );
		
	ИначеЕсли упРаботаСФормойКлиентСервер.ЭтоПоле( видРодителя ) Тогда
		
		разрешитьДобавление = Ложь;
		
	Иначе
		
		разрешитьДобавление = ПроверитьПеретаскиваниеМеждуТаблицами( пРодитель, пЗначениеПеретаскивания );
		
	КонецЕсли;
	
	Возврат разрешитьДобавление;
	
КонецФункции

&НаКлиенте
Функция ПроверитьПеретаскиваниеМеждуТаблицами( пРодитель, пЗначениеПеретаскивания )
	
	разрешитьПеретаскивание = Истина;
	
	видРодителя = глРеквизит_Кэш( пРодитель, "Вид" );
	
	Если упРаботаСФормойКлиентСервер.ЭтоТаблица( видРодителя ) Тогда
		
		ТЧРодителя = глРеквизит( пРодитель, "СсылкаНаРеквизит" );
		
	Иначе
		
		ТЧРодителя = глРеквизит( пРодитель, "СсылкаНаТЧ" );
		
	КонецЕсли;
	
	Для Каждого цЗначение Из пЗначениеПеретаскивания Цикл
		
		Если ТипЗнч( цЗначение ) = Тип("СправочникСсылка.упЭлементыФормы")
			И Не глРеквизит( цЗначение, "СсылкаНаТЧ" ) = ТЧРодителя Тогда
			
			разрешитьПеретаскивание = Ложь;
			Прервать;
			
		ИначеЕсли ТипЗнч( цЗначение ) = Тип("ДанныеФормыЭлементДерева") Тогда
			
			ТЧРеквизита = ПредопределенноеЗначение( "Справочник.упРеквизиты.ПустаяСсылка" );
			
			текРодитель = цЗначение.ПолучитьРодителя();
			
			Если Не текРодитель = Неопределено Тогда
				
				ТЧРеквизита = текРодитель.СсылкаНаРеквизит;
				
			КонецЕсли;
			
			Если Не ТЧРеквизита = ТЧРодителя Тогда
				
				разрешитьПеретаскивание = Ложь;
				Прервать;
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат разрешитьПеретаскивание;
	
КонецФункции

&НаКлиенте
Функция ПроверитьПеретаскиваниеВСтраницы( пЗначениеПеретаскивания )
	
	Для Каждого цЗначение Из пЗначениеПеретаскивания Цикл
		
		Если ТипЗнч( цЗначение ) = Тип("ДанныеФормыЭлементДерева") Тогда
			Возврат Ложь;
		КонецЕсли;
		
		Если ТипЗнч( цЗначение ) = Тип("СправочникСсылка.упЭлементыФормы")
			И Не упРаботаСФормойКлиентСервер.ЭтоСтраница( глРеквизит_Кэш( цЗначение, "Вид" ) )
			И Не упРаботаСФормойКлиентСервер.ЭтоОбычнаяГруппа( глРеквизит_Кэш( цЗначение, "Вид" ) ) Тогда
			
			Возврат Ложь;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Истина;
	
КонецФункции

&НаКлиенте
Процедура СкопироватьВДругиеСостояния(Команда)
	
	текРодитель = Элементы.СписокЭлементов.ТекущаяСтрока;
	
	ПараметрыФормы = Новый Структура( "МножественныйВыбор,Отбор", Истина, СтруктураОтбораДляКопированияЭлементов() );
	
	ОткрытьФорму("Справочник.упСостояния.ФормаВыбора", ПараметрыФормы, , , , , Новый ОписаниеОповещения("СкопироватьВДругиеСостоянияЗавершение", ЭтотОбъект, Новый Структура("текРодитель", текРодитель)));
	
КонецПроцедуры

&НаКлиенте
Процедура СкопироватьВДругиеСостоянияЗавершение(Состояния, ДополнительныеПараметры) Экспорт
	
	Если состояния = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ТипЗнч(состояния) = Тип("Массив") Тогда
		состояния = глСоздатьМассив( состояния );
	КонецЕсли;
	
	СкопироватьЭлементыВСостояния( состояния, ДополнительныеПараметры.текРодитель );
	
	Для Каждого цСостояние Из состояния Цикл
		
		ПоказатьЗначение(, цСостояние );
		
	КонецЦикла;
	
КонецПроцедуры

Функция СтруктураОтбораДляКопированияЭлементов()
	
	структураОтбора = Новый Структура;
	структураОтбора.Вставить( "Владелец", Объект.Владелец );
	
	типыСостояний = глСоздатьМассив( Перечисления.упТипыСостояний.Начало, Перечисления.упТипыСостояний.Действие );
	
	структураОтбора.Вставить( "ТипСостояния" , типыСостояний );
	
	Если Объект.МножественнаяАдресация Тогда
		
		структураОтбора.Вставить( "МножественнаяАдресация", Истина );
		структураОтбора.Вставить( "КоллекцияОбхода" , Объект.КоллекцияОбхода );
		
	Иначе
		
		структураОтбора.Вставить( "МножественнаяАдресация", Ложь );
		
	КонецЕсли;
	
	Возврат структураОтбора;
	
КонецФункции

Процедура СкопироватьЭлементыВСостояния( пМассивСостояний, пЭлементРодитель )
	
	соотвРодителей = Новый Соответствие;
	
	Для Каждого цНовВладелец Из пМассивСостояний Цикл
		
		Если цНовВладелец = Объект.Ссылка Тогда
			Продолжить;
		КонецЕсли;
		
		соотвРодителей.Очистить();
		
		Для Каждого цЭлемент Из Справочники.упЭлементыФормы.ОбъектыСостояния( Объект.Ссылка, пЭлементРодитель ) Цикл
			
			новЭлемент = цЭлемент.Скопировать();
			новЭлемент.Владелец = цНовВладелец;
			новЭлемент.Родитель = соотвРодителей[новЭлемент.Родитель];
			новЭлемент.Записать();
			
			соотвРодителей.Вставить( цЭлемент, новЭлемент.Ссылка );
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры


// }}

//////////////////////////////////////////////////////////////////////////////////////
//ДЕРЕВО РЕКВИЗИТОВ {{

Процедура ЗаполнитьДеревоРеквизитов()
	
	строки = деревоДоступныхРеквизитов.ПолучитьЭлементы();
	строки.Очистить();
	
	Если Объект.МножественнаяАдресация Тогда
		
		тзРеквизитов = Справочники.упРеквизиты.ПолучитьТаблицуКолонокРеквизита( Объект.КоллекцияОбхода );
		
	Иначе
		
		тзРеквизитов = Справочники.упВерсии.ПолучитьТаблицуДоступныхРеквизитов( Объект.Владелец );
		
	КонецЕсли;
	
	Для Каждого цСтрокаРеквизита Из тзРеквизитов Цикл
		
		Если Не Объект.МножественнаяАдресация
			И ЗначениеЗаполнено( цСтрокаРеквизита.СсылкаНаТЧ ) Тогда
			Продолжить;
		КонецЕсли;
		
		новСтрокаДерева = строки.Добавить();
		
		ЗаполнитьЗначенияСвойств( новСтрокаДерева, цСтрокаРеквизита );
		
		Если цСтрокаРеквизита.ЭтоТЧ Тогда
			
			строкиТЧ = новСтрокаДерева.ПолучитьЭлементы();
			
			Для Каждого цСтрокаРеквитиаТЧ Из тзРеквизитов.НайтиСтроки( Новый Структура( "СсылкаНаТЧ", новСтрокаДерева.СсылкаНаРеквизит ) ) Цикл
				
				ЗаполнитьЗначенияСвойств( строкиТЧ.Добавить() , цСтрокаРеквитиаТЧ );
				
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоДоступныхРеквизитовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	выбранныеСтроки = Новый Массив;
	
	Для Каждого цИД Из ВыбраннаяСтрока Цикл
		
		выбранныеСтроки.Добавить( деревоДоступныхРеквизитов.НайтиПоИдентификатору( цИД ) );
		
	КонецЦикла;
	
	текРодитель = Элементы.СписокЭлементов.ТекущаяСтрока;
	
	Пока Не РазрешитьДобавление( выбранныеСтроки, текРодитель ) Цикл
		
		Если Не ЗначениеЗаполнено( текРодитель ) Тогда
			// нельзя добавлять
			Возврат;
		КонецЕсли;
		
		текРодитель = глРеквизит( текРодитель, "Родитель" );
		
	КонецЦикла;
	
	ДобавитьЭлементыИзДереваРеквизитов( выбранныеСтроки, текРодитель );
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьЭлементыИзДереваРеквизитов( выбранныеСтроки, пРодитель )
	
	Для Каждого цВыбраннаяСтрока Из выбранныеСтроки Цикл
		
		Если цВыбраннаяСтрока.ЭтоТЧ Тогда
			
			видПоля = упИК.Таблица();
			
		Иначе
			
			видПоля = упИК.Поле();
			
		КонецЕсли;
		
		новЭлемент = СоздатьЭлементФормы( Объект.Ссылка, пРодитель , видПоля, цВыбраннаяСтрока.СсылкаНаРеквизит );
		
		Если цВыбраннаяСтрока.ЭтоТЧ Тогда
			
			шаблонТекста = НСтр( "ru='Добавить колонки таблицы %1'" );
			текстВопроса = стрЗаполнить( шаблонТекста, цВыбраннаяСтрока.СсылкаНаРеквизит );
			
			КнопкаВыполнитьДействие = НСтр("ru='Да';en='Yes'" );
			КнопкаОтмена = НСтр("ru='Нет';en='No'" );
			
			сзКнопки = Новый СписокЗначений;
			сзКнопки.Добавить( КнопкаВыполнитьДействие );
			сзКнопки.Добавить( КнопкаОтмена );
			
			ЗаголовокВопроса = НСтр( "ru='Добавить колонки?'" );
			
			ПоказатьВопрос(
				Новый ОписаниеОповещения("ДобавитьЭлементыИзДереваРеквизитовЗавершение", ЭтотОбъект, Новый Структура("КнопкаВыполнитьДействие, ВыбранныеСтроки, Родитель", КнопкаВыполнитьДействие, цВыбраннаяСтрока.ПолучитьЭлементы() , новЭлемент)),
				текстВопроса,
				сзКнопки,
				,
				КнопкаВыполнитьДействие,
				ЗаголовокВопроса);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Элементы.СписокЭлементов.Обновить();
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьЭлементыИзДереваРеквизитовЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = ДополнительныеПараметры.КнопкаВыполнитьДействие Тогда
		
		ДобавитьЭлементыИзДереваРеквизитов(ДополнительныеПараметры.ВыбранныеСтроки, ДополнительныеПараметры.Родитель);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьЭлементыПоУмолчанию(Команда)
	
	СоздатьЭлементыФормы( Объект.Ссылка );
	
	Элементы.СписокЭлементов.Обновить();
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура СоздатьЭлементыФормы( пСсылка )
	
	упРаботаСФормойСервер.СоздатьЭлементыФормыДляМассиваСостояний( глСоздатьМассив( пСсылка ) );
	
КонецПроцедуры


&НаКлиенте
Процедура СписокЭлементовПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	УдалитьЭлементы( Элемент.ВыделенныеСтроки );
	Элементы.СписокЭлементов.Обновить();
	
КонецПроцедуры

Процедура УдалитьЭлементы( Знач пМассив )
	
	УстановитьПривилегированныйРежим( Истина );
	
	Для Каждого цЭлемент Из пМассив Цикл
		
		цЭлемент.ПолучитьОбъект().Удалить();
		
	КонецЦикла;
	
	УстановитьПривилегированныйРежим( Ложь );
	
КонецПроцедуры



// }}