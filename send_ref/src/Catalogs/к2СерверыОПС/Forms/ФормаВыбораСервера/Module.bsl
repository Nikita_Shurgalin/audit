
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьЗначенияПоУмолчанию();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ОбновитьСерверы();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСерверы

&НаКлиенте
Процедура СерверыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ВыбратьСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура СерверыВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ВыбратьСервер();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	ИмяКомпьютера = Параметры.ИмяКомпьютера;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСерверы()
	
	Отказ = Ложь;
	ТекстОшибки = "";
	
	Для Каждого ИмяСервера Из к2РаботаСОПСКлиент.МассивСерверов(ИмяКомпьютера, ТекстОшибки, Отказ) Цикл 
		
		НоваяСтрока = Серверы.Добавить();
		НоваяСтрока.ИмяСервера = ИмяСервера;
		
	КонецЦикла;
	
	Если Отказ Тогда 
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстОшибки);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьСервер()
	
	ТекущиеДанные = Элементы.Серверы.ТекущиеДанные;
	
	Если Не ТекущиеДанные = Неопределено Тогда 
		ОповеститьОВыборе(ТекущиеДанные.ИмяСервера);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти