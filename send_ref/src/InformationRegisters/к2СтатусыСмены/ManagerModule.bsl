#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура Отразить(ДополнительныеСвойства, Движения, Отказ) Экспорт 
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.Таблица_к2СтатусыСмены;
	
	Если Отказ Или Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.к2СтатусыСмены.Записывать = Истина;
	Движения.к2СтатусыСмены.Загрузить(Таблица);
	
КонецПроцедуры

Функция СменаЗакрыта(ДатаСмены, Смена, РабочийЦентр) Экспорт 
	
	СтатусСмены = СтатусСмены(ДатаСмены, Смена, РабочийЦентр);
	
	Возврат Не МассивЗакрытыхСтатусов().Найти(СтатусСмены) = Неопределено;
	
КонецФункции

Функция СтатусСмены(ДатаСмены, Смена, РабочийЦентр) Экспорт 
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	к2СтатусыСмены.СтатусСмены КАК СтатусСмены
	|ИЗ
	|	РегистрСведений.к2СтатусыСмены КАК к2СтатусыСмены
	|ГДЕ
	|	к2СтатусыСмены.Активность
	|	И к2СтатусыСмены.ДатаСмены = &ДатаСмены
	|	И к2СтатусыСмены.Смена = &Смена
	|	И к2СтатусыСмены.РабочийЦентр = &РабочийЦентр";
	
	Запрос.УстановитьПараметр("ДатаСмены", ДатаСмены);
	Запрос.УстановитьПараметр("Смена", Смена);
	Запрос.УстановитьПараметр("РабочийЦентр", РабочийЦентр);
	
	СтатусСмены = к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).СтатусСмены;
	
	Если Не ЗначениеЗаполнено(СтатусСмены) Тогда 
		СтатусСмены = Перечисления.к2СтатусыСмены.НеЗакрыта;
	КонецЕсли;
	
	Возврат СтатусСмены;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция МассивЗакрытыхСтатусов()
	
	Массив = Новый Массив;
	Массив.Добавить(Перечисления.к2СтатусыСмены.Закрыта);
	Массив.Добавить(Перечисления.к2СтатусыСмены.ЗакрытаСОтклонением);
	
	Возврат Массив;
	
КонецФункции

#КонецОбласти

#КонецЕсли
