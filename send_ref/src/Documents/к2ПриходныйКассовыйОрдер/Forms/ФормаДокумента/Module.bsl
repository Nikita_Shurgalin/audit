#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	НастроитьФорму();
		
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	к2СобытияФорм.ОбработкаПроверкиЗаполненияНаСервере( ЭтотОбъект, Отказ, ПроверяемыеРеквизиты );
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ПриходныйОрдер", Объект.Ссылка, ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СостояниеПриИзменении(Элемент)
	
	ОбновитьОформлениеСостояния();
	
КонецПроцедуры

&НаКлиенте
Процедура КассаПриИзменении(Элемент)
	
	КассаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОперацияПриИзменении(Элемент)
	
	ОперацияПриИзмененииНаСервере();	
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаПриИзменении(Элемент)
	
	ДатаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОрганизацияПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПодразделениеПриИзменении(Элемент)
	
	ПодразделениеПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	КонтрагентПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыРасшифровкаПлатежа

&НаКлиенте
Процедура РасшифровкаПлатежаСуммаПриИзменении(Элемент)
	
	РассчитатьИтого();
	
КонецПроцедуры

&НаКлиенте
Процедура РасшифровкаПлатежаПослеУдаления(Элемент)
	
	РассчитатьИтого();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Заполнить(Команда)
	
	ЗаполнитьПоЗадолженностямНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Объект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область НастройкаФормы

&НаСервере
Процедура НастроитьФорму()
	
	УстановитьУсловноеОформление();
	
	НастроитьЭлементы();
	
КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементы()
	
	к2Документы.НастроитьЭлементыТабличнойЧасти(ЭтотОбъект, "РасшифровкаПлатежа");
	к2ИзменениеФорм.НастроитьКолонку_Ссылка(Элементы.РасшифровкаПлатежаОбъектРасчета);
	к2ИзменениеФорм.НастроитьКолонку_Ссылка(Элементы.РасшифровкаПлатежаОснование);
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	ЭтоПоступлениеОплаты = Объект.Операция = Перечисления.к2ОперацииПриходаДС.ПоступлениеОплаты;
	ЭтоПоступлениеОтПодотчетногоЛица = Объект.Операция = Перечисления.к2ОперацииПриходаДС.ПоступлениеОтПодотчетногоЛица;
	ЭтоИнвентаризация = Объект.Операция = Перечисления.к2ОперацииПриходаДС.ОприходованиеПоРезультатамИнвентаризации;
	
	Элементы.Контрагент.Видимость = ЭтоПоступлениеОплаты;
	Элементы.СтраницаРасшифровкаПлатежа.Видимость = ЭтоПоступлениеОплаты;	
	
	Элементы.ПодотчетноеЛицо.Видимость = ЭтоПоступлениеОтПодотчетногоЛица;
	
	Элементы.Инвентаризация.Видимость = ЭтоИнвентаризация;
	
КонецПроцедуры

#Область УсловноеОформление

&НаСервере
Процедура УстановитьУсловноеОформление()

	
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ОбновитьОформлениеСостояния();	
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьОформлениеСостояния()
	
	к2УО.ОбновитьОформлениеСостояния(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура КассаПриИзмененииНаСервере()
	
	РеквизитыКассы = глРеквизиты(Объект.Касса, "Валюта");
	 
	глПрисвоитьЕслиНеЗаполнено(Объект.Валюта, РеквизитыКассы.Валюта);
	
КонецПроцедуры

&НаСервере
Процедура ОперацияПриИзмененииНаСервере()
	
	УстановитьВидимостьДоступность();
	
	Документы.к2ПриходныйКассовыйОрдер.ЗаполнитьОснованиеДляПечати(Объект);
	
КонецПроцедуры

&НаСервере
Процедура ДатаПриИзмененииНаСервере()
	
	к2ОтветственныеЛицаСервер.ЗаполнитьРеквизитыВРеализацииТоваров(Объект);
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	
	к2ОтветственныеЛицаСервер.ЗаполнитьРеквизитыВРеализацииТоваров(Объект);
	
КонецПроцедуры

&НаСервере
Процедура ПодразделениеПриИзмененииНаСервере()
	
	к2ОтветственныеЛицаСервер.ЗаполнитьРеквизитыВРеализацииТоваров(Объект);
	
КонецПроцедуры

&НаСервере
Процедура КонтрагентПриИзмененииНаСервере()
	
	Документы.к2ПриходныйКассовыйОрдер.ЗаполнитьПринятоОт(Объект);
	
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьИтого()
	
	Объект.СуммаДокумента = Объект.РасшифровкаПлатежа.Итог("Сумма");
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПоЗадолженностямНаСервере()
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	
	к2Взаиморасчеты.РаспределитьВзаиморасчетыВДокументе(ДокументОбъект);
	
	Модифицированность = Истина;
	
	ЗначениеВРеквизитФормы(ДокументОбъект, "Объект");
	
КонецПроцедуры

#КонецОбласти