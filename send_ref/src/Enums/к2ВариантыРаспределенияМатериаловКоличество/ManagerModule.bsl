#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	Если Не Параметры.Свойство("ОсновнойМатериал") Тогда
		Возврат;
	КонецЕсли;
	
	СтандартнаяОбработка = Ложь;
	
	МассивСпискаВыбора = Новый Массив;
	МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериаловКоличество.НеРаспределять);
	МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериаловКоличество.ПоНормеНаВыпуск);
	
	Если Параметры.ОсновнойМатериал Тогда
		
		Если Параметры.Свойство("ИмяОбработкиКнопкиУчетныхТочек") Тогда
			
			Если Параметры.ИмяОбработкиКнопкиУчетныхТочек = Метаданные.Обработки.к2РаботаСЗаданиями.Имя Тогда
				
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериаловКоличество.ПоВыпускуМатериалИзСпецификации);
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериаловКоличество.ВсеОстатки);
				
			КонецЕсли;
			
			Если Параметры.ИмяОбработкиКнопкиУчетныхТочек = Метаданные.Обработки.к2ОперативныйУчетНаРабочемЦентре.Имя Тогда
				
				МассивСпискаВыбора.Добавить(Перечисления.к2ВариантыРаспределенияМатериаловКоличество.ПоВыпускуВыбранныйМатериал);
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	
	ДанныеВыбора = Новый СписокЗначений;
	ДанныеВыбора.ЗагрузитьЗначения(МассивСпискаВыбора);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли