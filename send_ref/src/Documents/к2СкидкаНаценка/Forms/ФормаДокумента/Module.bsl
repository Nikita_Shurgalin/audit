
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере( Отказ, СтандартнаяОбработка )
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан( Объект, ЭтотОбъект );
	
	к2СобытияФорм.ПриСозданииНаСервере( ЭтотОбъект, Отказ, СтандартнаяОбработка );
	
	УстановитьУсловноеОформление();
	
	к2КопированиеСтрокСервер.ПриСозданииНаСевере( Элементы, "Соглашения" );
	к2КопированиеСтрокСервер.ПриСозданииНаСевере( Элементы, "Товары" );
	
	Если Объект.Ссылка.Пустая() Тогда
		
		ПриСозданииЧтенииНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения( ИмяСобытия, Параметр, Источник )
	
	Если ИмяСобытия = "ПодборСоглашенийПроизведен"
		И ЗначениеЗаполнено( Параметр )
		И Источник = УникальныйИдентификатор Тогда
		
		ЗагрузитьСоглашенияИзХранилища( Параметр );
		
	КонецЕсли;
	
	Если ИмяСобытия = "ПодборНоменклатурыПроизведен"
		И ЗначениеЗаполнено( Параметр )
		И Источник = УникальныйИдентификатор Тогда
		
		ЗагрузитьНоменклатуруИзХранилища( Параметр );
		
	КонецЕсли;
	
	к2КопированиеСтрокКлиент.ОбработкаОповещения( ИмяСобытия, Параметр, Элементы );
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриСозданииЧтенииНаСервере();
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
	ЗаполнитьСлужебныеРеквизиты();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПериодДействияПриИзменении(Элемент)
	
	Объект.НачалоПериодаДействия = ПериодДействия.ДатаНачала;
	Объект.ОкончаниеПериодаДействия = ПериодДействия.ДатаОкончания;
	
КонецПроцедуры

&НаКлиенте
Процедура ТипПриИзменении(Элемент)
	
	НастроитьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ВалютаПриИзменении(Элемент)
	
	НастроитьКолонкуЦена();
	
КонецПроцедуры

&НаКлиенте
Процедура ПроцентСкидки_НоменклатураПриИзменении(Элемент)
	
	Для каждого цСтрока Из Объект.Товары Цикл
		
		цСтрока.ПроцентСкидки = ПроцентСкидки;
		
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ПроцентСкидки_ТоварныеКатегорииПриИзменении(Элемент)
	
	Для каждого цСтрока Из Объект.ТоварныеКатегории Цикл
		
		цСтрока.ПроцентСкидки = ПроцентСкидки;
		
	КонецЦикла;
	
	ПроцентСкидки = 0;
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора( Элемент, ДанныеВыбора, СтандартнаяОбработка )
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария( Элемент.ТекстРедактирования,
																   ЭтотОбъект,
																   "Объект.Комментарий" );
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормы_Соглашения

&НаКлиенте
Процедура СоглашенияПослеУдаления(Элемент)
	
	ПриИзмененииСоглашений();
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашенияСоглашениеПриИзменении(Элемент)
	
	ПриИзмененииСоглашений();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормы_Товары

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьХарактеристикуПоВладельцу", ТекущиеДанные.Характеристика);
	СтруктураДействий.Вставить("ПроверитьЗаполнитьУпаковкуПоВладельцу", ТекущиеДанные.Упаковка);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыХарактеристикаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Товары.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьЗаполнитьУпаковкуПоВладельцу", ТекущиеДанные.Упаковка);
	
	к2ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущиеДанные, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПодобратьСоглашения( Команда )
	
	параметрыПодбора = Новый Структура;
	параметрыПодбора.Вставить( "УникальныйИдентификаторФормыВладельца", УникальныйИдентификатор );
	параметрыПодбора.Вставить( "АдресПодобранныхСоглашений", АдресПодобранныхСоглашений() );
	
	ОткрытьФорму( "Справочник.к2СоглашенияСКлиентами.Форма.ФормаПодбора",
				  параметрыПодбора,
				  ЭтотОбъект,
				  ,
				  ,
				  ,
				  ,
				  РежимОткрытияОкнаФормы.БлокироватьОкноВладельца );
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашенияКопироватьСтроки( Команда )
	
	количествоСкопировано = СкопироватьСтрокиСоглашенийНаСервере();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОКопированииСтрок( количествоСкопировано, "Соглашения" );
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашенияВставитьСтроки( Команда )
	
	количествоВставлено = ПолучитьСтрокиСоглашенийИзБуфераОбмена();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОВставкеСтрок( количествоВставлено );
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьТовары( Команда )
	
	отбор = Новый Структура;
	отбор.Вставить( "ТипНоменклатуры", ПредопределенноеЗначение( "Перечисление.к2ТипыНоменклатуры.ГотоваяПродукция" ) );
	
	ДополнительныеПараметрыПодбора = НоменклатураКлиент.ДополнительныеПараметрыПодбора();
	ДополнительныеПараметрыПодбора.ПодборХарактеристикиАвтоматически = Ложь;
	ДополнительныеПараметрыПодбора.ИспользоватьХарактеристики = Объект.УточнятьЦенуДоХарактеристик;
	
	НоменклатураКлиент.ПодобратьНоменклатуруСПараметрами( ЭтотОбъект, АдресПодобраннойНоменклатуры(), отбор, ДополнительныеПараметрыПодбора );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКопироватьСтроки( Команда )
	
	количествоСкопировано = СкопироватьСтрокиТоваровНаСервере();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОКопированииСтрок( количествоСкопировано, "Товары" );
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыВставитьСтроки( Команда )
	
	количествоВставлено = ПолучитьСтрокиТоваровИзБуфераОбмена();
	к2КопированиеСтрокКлиент.ОповеститьПользователяОВставкеСтрок( количествоВставлено );
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область СтандартныеПодсистемыПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)

    ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);

КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт

    ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);

КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()

    ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);

КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область УсловноеОформление

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	к2УО.Характеристика( ЭтотОбъект );
	к2УО.ЕдиницаИзмерения( ЭтотОбъект );
	
КонецПроцедуры

#КонецОбласти

&НаСервере
Процедура ПриСозданииЧтенииНаСервере()
	
	ПериодДействия = Новый СтандартныйПериод( Объект.НачалоПериодаДействия, Объект.ОкончаниеПериодаДействия );
	
	НастроитьДоступность();
	ЗаполнитьСлужебныеРеквизиты();
	НастроитьКолонкуЦена();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизиты()
	
	ЗаполнитьДанныеХарактеристик();
	
	УстановитьСтатусДействия();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеХарактеристик()
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить( "ЗаполнитьПризнакХарактеристикиИспользуются",
								Новый Структура( "Номенклатура", "ХарактеристикиИспользуются" ) );
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции( Объект.Товары, СтруктураДействий );
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСтатусДействия()
	
	статусДействия = к2ЦенообразованиеВызовСервера.СтатусДействияСкидкиНаценки( Объект );
	цветФона = к2ЦенообразованиеВызовСервера.ЦветФонаПоСтатусуДействияСкидкиНаценки( статусДействия );
	
	Элементы.Группа_ПлашкаТекущегоСтатуса.ЦветФона = цветФона;
	
	ТекущийСтатусДействия = к2ЦенообразованиеВызовСервера.ПредставлениеСтатусаДействияСкидкиНаценки( Объект );
	
КонецПроцедуры

&НаСервере
Процедура НастроитьДоступность()
	
	этоТоварныеКатегории = Объект.Тип = Перечисления.к2ТипыСкидокНаценок.ПроцентомНаТоварнуюКатегорию;
	этоУстановкаПроцента = этоТоварныеКатегории ИЛИ Объект.Тип = Перечисления.к2ТипыСкидокНаценок.ПроцентомНаНоменклатуру;
	этоФиксированнаяЦена = Объект.Тип = Перечисления.к2ТипыСкидокНаценок.ФиксированнаяЦена;
	
	Элементы.Страница_ТоварныеКатегории.Видимость = этоТоварныеКатегории;
	Элементы.Страница_Номенклатура.Видимость = Не этоТоварныеКатегории;
	
	Элементы.Валюта.Видимость = этоФиксированнаяЦена;
	Элементы.ТоварыЦена.Видимость = этоФиксированнаяЦена;
	
	Элементы.ПроцентСкидки_Номенклатура.Видимость = этоУстановкаПроцента;
	Элементы.ТоварыПроцентСкидки.Видимость = этоУстановкаПроцента;
	
	Элементы.ПроцентСкидки_ТоварныеКатегории.Видимость = этоУстановкаПроцента;
	Элементы.ТоварныеКатегорииПроцентСкидки.Видимость = этоУстановкаПроцента;
	
	Элементы.ТоварыХарактеристика.Видимость = Объект.УточнятьЦенуДоХарактеристик;
	
КонецПроцедуры

&НаСервере
Процедура НастроитьКолонкуЦена()

	Если ЗначениеЗаполнено( Объект.Валюта ) Тогда
		
		Элементы.ТоварыЦена.Заголовок = СтрШаблон( НСтр( "ru='Цена, %1'" ), Объект.Валюта );
		
	Иначе
		
		Элементы.ТоварыЦена.Заголовок = НСтр( "ru='Цена'" );
		
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриИзмененииСоглашений()
	
	ЗаполнитьВариантУточненияЦены();
	
	Элементы.ТоварыХарактеристика.Видимость = Объект.УточнятьЦенуДоХарактеристик;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВариантУточненияЦены()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	к2СоглашенияСКлиентами.ВидЦен.УточнятьЦенуДоХарактеристик КАК УточнятьЦенуДоХарактеристик
		|ИЗ
		|	Справочник.к2СоглашенияСКлиентами КАК к2СоглашенияСКлиентами
		|ГДЕ
		|	к2СоглашенияСКлиентами.Ссылка В(&Соглашения)";
	Запрос.УстановитьПараметр( "Соглашения", Объект.Соглашения.Выгрузить().ВыгрузитьКолонку( "Соглашение" ) );
	
	выборка = Запрос.Выполнить().Выбрать();
	
	Если выборка.Количество() > 1 Тогда
		
		СообщитьОРазличииВариантовВУстановкеЦен();
		
	Иначе
		
		Если выборка.Следующий()
			И Не Объект.УточнятьЦенуДоХарактеристик = выборка.УточнятьЦенуДоХарактеристик Тогда
			
			Объект.УточнятьЦенуДоХарактеристик = выборка.УточнятьЦенуДоХарактеристик;
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура СообщитьОРазличииВариантовВУстановкеЦен()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	Соглашения.Соглашение КАК Соглашение,
	               |	Соглашения.НомерСтроки КАК НомерСтроки
	               |ПОМЕСТИТЬ втСоглашения
	               |ИЗ
	               |	&Соглашения КАК Соглашения
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	к2СоглашенияСКлиентами.ВидЦен.УточнятьЦенуДоХарактеристик КАК УточнятьЦенуДоХарактеристик,
	               |	втСоглашения.Соглашение КАК Соглашение,
	               |	втСоглашения.НомерСтроки КАК НомерСтроки
	               |ИЗ
	               |	втСоглашения КАК втСоглашения
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.к2СоглашенияСКлиентами КАК к2СоглашенияСКлиентами
	               |		ПО втСоглашения.Соглашение = к2СоглашенияСКлиентами.Ссылка
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	НомерСтроки";
	Запрос.УстановитьПараметр( "Соглашения", Объект.Соглашения.Выгрузить() );
	
	выборка = Запрос.Выполнить().Выбрать();
	
	вариантВПервойСтроке = Неопределено;
	
	сообщение   = НСтр( "ru='Вариант установки цен должен совпадать для всех соглашений'" );
	КлючДанных = к2ОбщегоНазначения.КлючДанныхДляСообщенияПользователю( Объект );
	
	Пока выборка.Следующий() Цикл
		
		Если вариантВПервойСтроке = Неопределено Тогда
			
			вариантВПервойСтроке = выборка.УточнятьЦенуДоХарактеристик;
			Продолжить;
			
		КонецЕсли;
		
		Если Не вариантВПервойСтроке = выборка.УточнятьЦенуДоХарактеристик Тогда
			
			путьКТЧ = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти( "Соглашения",
																		Выборка.НомерСтроки,
																		"Соглашение" );
			
			ОбщегоНазначения.СообщитьПользователю( сообщение, КлючДанных, путьКТЧ, "Объект" );
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#Область Подборы

&НаСервере
Функция АдресПодобранныхСоглашений()
	
	Возврат ПоместитьВоВременноеХранилище( Объект.Соглашения.Выгрузить() );
	
КонецФункции

&НаСервере
Процедура ЗагрузитьСоглашенияИзХранилища(Адрес)
	
	Если Не ЗначениеЗаполнено( Адрес ) Тогда
		Возврат;
	КонецЕсли;
	
	тзСоглашения = ПолучитьИзВременногоХранилища(Адрес);
	тзСоглашения.Свернуть("Соглашение");
	
	Объект.Соглашения.Очистить();
	
	Для каждого цСтрока Из тзСоглашения Цикл
		
		ЗаполнитьЗначенияСвойств( Объект.Соглашения.Добавить(), цСтрока );
		
	КонецЦикла;
	
	ПриИзмененииСоглашений();
	
КонецПроцедуры

&НаСервере
Функция АдресПодобраннойНоменклатуры()
	
	Возврат ПоместитьВоВременноеХранилище( Объект.Товары.Выгрузить() );
	
КонецФункции

&НаСервере
Процедура ЗагрузитьНоменклатуруИзХранилища(Адрес)
	
	Если Не ЗначениеЗаполнено( Адрес ) Тогда
		Возврат;
	КонецЕсли;
	
	тзНоменклатура = ПолучитьИзВременногоХранилища(Адрес);
	Если Объект.УточнятьЦенуДоХарактеристик Тогда
		тзНоменклатура.Свернуть("Номенклатура,Характеристика,Упаковка");
		структПоиска = Новый Структура("Номенклатура,Характеристика,Упаковка");
	Иначе
		тзНоменклатура.Свернуть("Номенклатура,Упаковка");
		структПоиска = Новый Структура("Номенклатура,Упаковка");
	КонецЕсли;
	
	старТовары = Объект.Товары.Выгрузить();
	
	Объект.Товары.Очистить();
	
	Для Каждого цСтрока Из тзНоменклатура Цикл
		
		новСтрока = Объект.Товары.Добавить();
		
		ЗаполнитьЗначенияСвойств( новСтрока, цСтрока );
		ЗаполнитьЗначенияСвойств( структПоиска, цСтрока );
		
		Для Каждого цНайденнаяСтрока Из старТовары.НайтиСтроки( структПоиска ) Цикл
			
			ЗаполнитьЗначенияСвойств( новСтрока, цНайденнаяСтрока );
			Прервать;
			
		КонецЦикла;
		
	КонецЦикла;
	
	ЗаполнитьДанныеХарактеристик();
	
КонецПроцедуры

#КонецОбласти

#Область КопированиеВставка

&НаСервере
Функция СкопироватьСтрокиСоглашенийНаСервере()
	
	Возврат к2КопированиеСтрокСервер.Копировать( Объект.Соглашения, Элементы.Соглашения.ВыделенныеСтроки, "Соглашения" );
	
КонецФункции

&НаСервере
Функция ПолучитьСтрокиСоглашенийИзБуфераОбмена()
	
	результатВставки = к2КопированиеСтрокСервер.Вставить( Объект.Соглашения, Элементы.Соглашения, "Соглашения", "Соглашение" );
	ПриИзмененииСоглашений();
	Возврат результатВставки;
	
КонецФункции

&НаСервере
Функция СкопироватьСтрокиТоваровНаСервере()
	
	Возврат к2КопированиеСтрокСервер.Копировать( Объект.Товары, Элементы.Товары.ВыделенныеСтроки, "Товары" );
	
КонецФункции

&НаСервере
Функция ПолучитьСтрокиТоваровИзБуфераОбмена()
	
	Возврат к2КопированиеСтрокСервер.Вставить( Объект.Товары, Элементы.Товары, "Товары", "Номенклатура,Характеристика" );
	
КонецФункции

#КонецОбласти

#КонецОбласти
