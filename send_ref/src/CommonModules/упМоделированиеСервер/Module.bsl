
Функция ТипСостояния_Слияние( пТип ) Экспорт
	
	Возврат пТип = Перечисления.упТипыСостояний.Слияние;
	
КонецФункции	//ТипСостояния_УсловнаяАктивность




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//МЕТОДЫ ДЛЯ РАБОТЫ С СОСТОЯНИЯМИ

// Ищет предыдущее действие или первое попавшееся разделение
//
// Возвращаемое значение:
//   Состояние   – СправочникСсылка.Состояние.
//
Функция НайтиДействие( лСостояние )
	
	Если глРеквизит_Кэш( лСостояние , "ТипСостояния" ) = Перечисления.упТипыСостояний.Начало Тогда
		Возврат лСостояние;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.НачалоПерехода,
	               |	упПереходы.НачалоПерехода.ТипСостояния КАК ТипСостояния
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.КонецПерехода = &КонецПерехода";
	
	Запрос.УстановитьПараметр("КонецПерехода", лСостояние);
	лТаблСостояний  = Запрос.Выполнить().Выгрузить();
	
	Для Каждого лСтрокаТабл Из лТаблСостояний Цикл
		
		Если лСтрокаТабл.ТипСостояния = Перечисления.упТипыСостояний.Действие
			ИЛИ лСтрокаТабл.ТипСостояния = Перечисления.упТипыСостояний.Разделение Тогда
			
			Возврат лСтрокаТабл.НачалоПерехода;
			
		КонецЕсли;
		
		Возврат НайтиДействие( лСтрокаТабл.НачалоПерехода );
		
	КонецЦикла;
	
КонецФункции // НайтиДействие()

// Ищет предыдущее действие
//
// Возвращаемое значение:
//   Состояние   – СправочникСсылка.Состояние.
//
Функция НайтиПредыдущиеДействия(пСтартовоеСостояние, массивПоиска = Неопределено, массивРезультат = Неопределено) Экспорт
	
	Если массивПоиска = Неопределено Тогда
		
		массивПоиска = Новый Массив;
		
		массивПоиска.Добавить( пСтартовоеСостояние );
		
	КонецЕсли;
	
	Если массивРезультат = Неопределено Тогда
		
		массивРезультат = Новый Массив;
		
	КонецЕсли;
	
	Если массивПоиска.Количество() = 0 Тогда
		
		Возврат упКоллекцииКлиентСервер.УдалитьДублиИзМассива( массивРезультат );
		
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.НачалоПерехода,
	               |	упПереходы.НачалоПерехода.ТипСостояния КАК ТипСостояния
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.КонецПерехода В(&КонецПерехода)
	               |	И упПереходы.ПометкаУдаления = ЛОЖЬ";
	Запрос.УстановитьПараметр( "КонецПерехода" , массивПоиска );
	
	лТаблСостояний = Запрос.Выполнить().Выгрузить();
	
	массивПоиска.Очистить();
	
	Для Каждого лСтрокаТабл Из лТаблСостояний Цикл
		
		Если лСтрокаТабл.ТипСостояния = Перечисления.упТипыСостояний.Действие
			ИЛИ лСтрокаТабл.ТипСостояния = Перечисления.упТипыСостояний.Начало Тогда
			
			массивРезультат.Добавить(лСтрокаТабл.НачалоПерехода);
			
		ИначеЕсли Не лСтрокаТабл.НачалоПерехода = пСтартовоеСостояние Тогда
			
			массивПоиска.Добавить(лСтрокаТабл.НачалоПерехода);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат НайтиПредыдущиеДействия(пСтартовоеСостояние, массивПоиска, массивРезультат);
	
КонецФункции // НайтиПредыдущееДействие()

// /ищет первое попавшееся разделение при движении вверх
Функция НайтиРазделение( пСостояние , пПроверенныеСостояния = Неопределено) Экспорт
	
	Если ЭтоРазделениеИлиСтарт( пСостояние ) Тогда
		
		Возврат пСостояние;
		
	КонецЕсли;
	
	Если пПроверенныеСостояния = Неопределено Тогда
		
		пПроверенныеСостояния = Новый Массив;
		
	КонецЕсли;
	
	пПроверенныеСостояния.Добавить( пСостояние );
	
	Для Каждого цСостояние Из Справочники.упСостояния.ПолучитьПредыдущиеСостояния( пСостояние, пПроверенныеСостояния ) Цикл
		
		НовСостояние = НайтиРазделение( цСостояние, пПроверенныеСостояния);
		
		Если ЭтоРазделениеИлиСтарт( НовСостояние ) Тогда
			
			Возврат НовСостояние;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Неопределено;
	
КонецФункции // НайтиРазделение()

Функция ЭтоРазделениеИлиСтарт( пСостояние )
	
	Возврат Справочники.упСостояния.ЭтоСтарт( пСостояние )
	ИЛИ Справочники.упСостояния.ЭтоРазделение( пСостояние );
	
КонецФункции


// ищет первое попавшееся слияние при движении вниз
Функция НайтиСлияние(лСостояние, пПроверенныеСостояния = Неопределено) Экспорт
	
	Если лСостояние.ТипСостояния = Перечисления.упТипыСостояний.Завершение
		ИЛИ (Не пПроверенныеСостояния = Неопределено
		И Не пПроверенныеСостояния.Найти(лСостояние) = Неопределено) Тогда
		
		Возврат лСостояние;
		
	КонецЕсли;
	
	Если пПроверенныеСостояния = Неопределено Тогда
		
		пПроверенныеСостояния =Новый Массив;
		пПроверенныеСостояния.Добавить(лСостояние);
		
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.КонецПерехода,
	               |	упПереходы.КонецПерехода.ТипСостояния КАК ТипСостояния
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.НачалоПерехода = &НачалоПерехода";
	
	Запрос.УстановитьПараметр("НачалоПерехода", лСостояние);
	лТаблСостояний  = Запрос.Выполнить().Выгрузить();
	
	пПроверенныеСостояния.Добавить(лСостояние);
	
	Для Каждого лСтрокаТабл Из лТаблСостояний Цикл
		
		Если лСтрокаТабл.ТипСостояния = Перечисления.упТипыСостояний.Слияние Тогда
			
			Возврат лСтрокаТабл.КонецПерехода;
			
		КонецЕсли;
		
		НовСостояние = НайтиСлияние(лСтрокаТабл.КонецПерехода, пПроверенныеСостояния);
		
		Пока пПроверенныеСостояния.Найти(лСостояние) = Неопределено
			И Не НовСостояние.ТипСостояния = Перечисления.упТипыСостояний.Слияние
			И Не НовСостояние.ТипСостояния = Перечисления.упТипыСостояний.Завершение Цикл
			
			НовСостояние = НайтиСлияние(НовСостояние, пПроверенныеСостояния);
			
		КонецЦикла;
		
		Если Не пПроверенныеСостояния.Найти(НовСостояние) = Неопределено Тогда
			
			Продолжить;
			
		Иначе
			
			Возврат НовСостояние;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат пПроверенныеСостояния[0];
	
КонецФункции // НайтиСлияние()



Функция ПолучитьИмяФормыДляСостояния( пСостояние ) Экспорт
	
	Возврат Справочники.упСостояния.ПолучитьИмяФормыДляСостояния( пСостояние );
	
КонецФункции



Функция СоздатьАлгоритм( пСостояние , пВыражение , пНаименование = "" ) Экспорт
	
	структСостояние = глСтруктураРеквизитов( пСостояние , "Ссылка,Владелец,ТипСостояния,МножественнаяАдресация,КоллекцияОбхода" );
	
	структураАлгоритма = упМоделированиеВызовСервера.ПолучитьСтруктуруЗаполненияАлгоритма( структСостояние );
	структураАлгоритма.Вставить( "Наименование" , пНаименование );
	структураАлгоритма.Вставить( "Алгоритм" , пВыражение );
	
	Возврат СоздатьАлгоритмПоСтруктуре( структураАлгоритма );
	
КонецФункции	//СоздатьАлгоритм

Функция СоздатьАлгоритмПоСтруктуре( пСтруктура ) Экспорт
	
	сущАлгоритм = упОбщегоНазначенияСервер.НайтиСсылкуПоСтруктуре( "Справочник.упАлгоритмы", пСтруктура );
	
	Если ЗначениеЗаполнено( сущАлгоритм ) Тогда
		Возврат сущАлгоритм;
	КонецЕсли;
	
	обАлгоритм = Справочники.упАлгоритмы.СоздатьЭлемент();
	обАлгоритм.Заполнить( пСтруктура );
	обАлгоритм.Записать();
	
	глСозданНовыйОбъект( обАлгоритм.Ссылка );
	
	Возврат обАлгоритм.Ссылка;
	
КонецФункции


Функция СоздатьАлгоритм_СлитьТЧ( пСостояние , пРеквизит ) Экспорт
	
	имяРеквизита = глРеквизит_Кэш( пРеквизит , "Имя" );
	
	Возврат СоздатьАлгоритм( пСостояние , упИК.Выражение__СлитьТЧ( имяРеквизита ) , НСтр( "ru='Объединение '" ) + имяРеквизита );
	
КонецФункции	//СоздатьАлгоритм_СлитьТЧ


Функция ЕстьСледующееСостояние( пСостояние ) Экспорт
	
	Возврат Справочники.упСостояния.СледующиеСостояния( пСостояние ).Количество() > 0;
	
КонецФункции



//////////////////////////////////////////////////////////////////////////////////////////
//МЕТОДЫ РАБОТЫ С РЕКВИЗИТАМИ

Функция СоздатьРеквизит( Знач пИмяРеквизита, Знач пНаименование , пТип , пРодитель , пМассивКолонок = Неопределено ) Экспорт
	
	Если Не ЗначениеЗаполнено( пНаименование ) Тогда
		
		пНаименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( пИмяРеквизита );
		
	КонецЕсли;
	
	// подготовка структуры для поиска/создания
	
	структРеквизита = Новый Структура;
	
	структРеквизита.Вставить( "Имя" , пИмяРеквизита );
	структРеквизита.Вставить( "Наименование" , пНаименование );
	структРеквизита.Вставить( "ТипРеквизита" , пТип );
	
	Если ТипЗнч( пРодитель ) = Тип( "Строка" ) Тогда
		
		структРеквизита.Вставить( "Родитель" , ПолучитьГруппуРеквизитов( пРодитель ) );
		
	Иначе
		
		структРеквизита.Вставить( "Родитель" , пРодитель );
		
	КонецЕсли;
	
	// поиск существующего реквизита
	
	сущРеквизит = упОбщегоНазначенияСервер.НайтиСсылкуПоСтруктуре( "Справочник.упРеквизиты", структРеквизита );
	
	Если ЗначениеЗаполнено( сущРеквизит )
		И Не пМассивКолонок = Неопределено Тогда
		
		сущМассивКолонок = сущРеквизит.РеквизитыТабличнойЧасти.ВыгрузитьКолонку( "РеквизитТабличнойЧасти" );
		
		Если Не упКоллекцииКлиентСервер.МассивыРавны( сущМассивКолонок, пМассивКолонок ) Тогда
			
			сущРеквизит = Неопределено;
			
		КонецЕсли;
		
	КонецЕсли;
		
	Если ЗначениеЗаполнено( сущРеквизит ) Тогда
		
		Возврат сущРеквизит;
		
	КонецЕсли;
	
	
	// реквизит не найден, заполняем данные для создания
	
	Если Не пМассивКолонок = Неопределено Тогда
		
		тз = Новый ТаблицаЗначений;
		тз.Колонки.Добавить( "Имя" );
		тз.Колонки.Добавить( "РеквизитТабличнойЧасти" );
		
		Для каждого цРеквизит Из пМассивКолонок Цикл
			
			тз.Добавить().РеквизитТабличнойЧасти = цРеквизит;
			
		КонецЦикла;
		
		структРеквизита.Вставить( "РеквизитыТабличнойЧасти" , тз );
		
	КонецЕсли;
	
	ВозможныеЗначенияДляТестирования = тзВозможныеЗначенияДляТестирования();
	
	Если пТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Число Тогда
		
		Для ц=1 По 10 Цикл
			
			новСтрока = ВозможныеЗначенияДляТестирования.Добавить();
			новСтрока.Значение = ц;
			новСтрока.Вероятность = 1;
			
		КонецЦикла;
		
	ИначеЕсли ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ЭтоДополнительныеЗначения( пТип ) Тогда
		
		Для каждого цЗначение Из упРеквизитыСервер.МассивЗначенийТипа( пТип ) Цикл
			
			новСтрока = ВозможныеЗначенияДляТестирования.Добавить();
			новСтрока.Значение = цЗначение;
			новСтрока.Вероятность = 1;
			
		КонецЦикла;
		
	КонецЕсли;
	
	структРеквизита.Вставить( "ВозможныеЗначенияДляТестирования" , ВозможныеЗначенияДляТестирования );
	
	Возврат СоздатьРеквизитПоСтруктуре( структРеквизита );
	
КонецФункции	//СоздатьРеквизит

Функция тзВозможныеЗначенияДляТестирования()
	
	ВозможныеЗначенияДляТестирования = Новый ТаблицаЗначений;
	ВозможныеЗначенияДляТестирования.Колонки.Добавить( "Значение" );
	ВозможныеЗначенияДляТестирования.Колонки.Добавить( "Вероятность" );
	
	Возврат ВозможныеЗначенияДляТестирования;
	
КонецФункции	//тзВозможныеЗначенияДляТестирования


Функция ПолучитьРеквизит( пИмяРеквизита , пТип , пРодитель ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упРеквизиты.Ссылка
	|ИЗ
	|	Справочник.упРеквизиты КАК упРеквизиты
	|ГДЕ
	|	упРеквизиты.Родитель = &Родитель
	|	И упРеквизиты.ПометкаУдаления = ЛОЖЬ
	|	И упРеквизиты.Имя = &Имя
	|	И упРеквизиты.ТипРеквизита = &ТипРеквизита";
	
	Запрос.УстановитьПараметр("Имя", пИмяРеквизита);
	Запрос.УстановитьПараметр("Родитель", пРодитель);
	Запрос.УстановитьПараметр("ТипРеквизита", пТип);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Возврат Выборка.Ссылка;
		
	Иначе
		
		Возврат Справочники.упРеквизиты.ПустаяСсылка();
		
	КонецЕсли;
	
КонецФункции	//ПолучитьРеквизит

Функция СоздатьТипРеквизита_Перечисление( пНаименование , пМассивЗначений, пКомментарий = "" ) Экспорт
	
	лРодитель = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Перечисления;
	
	// ищем тип
	найденныйЭлемент = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПолучитьТипПеречисление( пНаименование , лРодитель , пМассивЗначений, пКомментарий );
	
	Если ЗначениеЗаполнено( найденныйЭлемент ) Тогда
		Возврат найденныйЭлемент;
	КонецЕсли;
	
	// не нашли нужного типа - придется создавать
	
	НовыйТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.СоздатьЭлемент();
	НовыйТип.Заполнить( Новый Структура( "Родитель,Наименование,ТипЗначения,Комментарий" , лРодитель , пНаименование , ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Тип_Перечисление(), пКомментарий ) );
	НовыйТип.Записать();
	
	глСозданНовыйОбъект( НовыйТип.Ссылка );
	
	Для Каждого цВариант Из пМассивЗначений Цикл
		
		Справочники.упДополнительныеЗначенияХарактеристик.СоздатьНовоеЗначение( цВариант , НовыйТип.Ссылка );
		
	КонецЦикла;
	
	Возврат НовыйТип.Ссылка;
	
КонецФункции	//СоздатьТипРеквизита






Процедура Лог( пТекстПротокола , пОбъект = Неопределено, ЭтоОшибка = Истина , ИнформацияОбОшибке = Неопределено ) Экспорт
	
	глЛог( пТекстПротокола , Перечисления.упТипыОшибокДляЛога.Моделирование , ЭтоОшибка, ИнформацияОбОшибке, пОбъект );
	
КонецПроцедуры



//////////////////////////////////////////////////////////////////////////////////////
//НАПОЛНЕНИЕ СОСТОЯНИЙ

Процедура НаполнитьСлияние( пОбъект ) Экспорт
	
	Если ТипЗнч( пОбъект ) = Тип( "СправочникСсылка.упСостояния" ) Тогда
		
		лОбъект = пОбъект.ПолучитьОбъект();
		записывать = Истина;
		
	Иначе
		
		лОбъект = пОбъект;
		записывать = Ложь;
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено( лОбъект.СостояниеРазделение ) Тогда
		
		лОбъект.СостояниеРазделение = НайтиРазделение( лОбъект.Ссылка );
		
	КонецЕсли;
	
	массивРеквизитовВерсии = Справочники.упВерсии.ПолучитьМассивРеквизитовВерсии( лОбъект.Владелец );
	
	// добавляем новые реквизиты на форму
	
	Для Каждого цРеквизит Из массивРеквизитовВерсии Цикл
		
		Если лОбъект.ПравилаСлияния.НайтиСтроки( Новый Структура( "РеквизитБП" , цРеквизит ) ).Количество() = 0 Тогда
			
			лОбъект.ПравилаСлияния.Добавить().РеквизитБП = цРеквизит;
			
		КонецЕсли;
		
	КонецЦикла;
	
	// удаляем старые реквизиты
	
	массивСтрокНаУдаление = Новый Массив;
	
	Для каждого цСтрока Из лОбъект.ПравилаСлияния Цикл
		
		Если массивРеквизитовВерсии.Найти( цСтрока.РеквизитБП ) = Неопределено Тогда
			
			массивСтрокНаУдаление.Добавить( цСтрока );
			
		КонецЕсли;
		
	КонецЦикла;
	
	Для каждого цСтрока Из массивСтрокНаУдаление Цикл
		
		лОбъект.ПравилаСлияния.Удалить( цСтрока );
		
	КонецЦикла;
	
	Если записывать Тогда
		лОбъект.Записать();
	КонецЕсли;
	
КонецПроцедуры	//НаполнитьСлияние



//////////////////////////////////////////////////////////////////////////////////////
//МЕТОДЫ ПРОГРАММНОГО УПРАВЛЕНИЯ

Функция СоздатьБизнесПроцесс( пСтруктураПараметров ) Экспорт
	
	Возврат Справочники.упБизнесПроцессы.СоздатьБизнесПроцесс( пСтруктураПараметров );
	
КонецФункции	//СоздатьБизнесПроцесс

Функция НайтиБизнесПроцесс( пНаименование , пКодБП , пРодитель ) Экспорт
	
	Возврат Справочники.упБизнесПроцессы.НайтиБизнесПроцесс( пНаименование , пКодБП , пРодитель );
	
КонецФункции	//НайтиБизнесПроцесс


Функция СоздатьВерсиюБизнесПроцесса( пСтруктураПараметров ) Экспорт
	
	обВерсия = Справочники.упВерсии.СоздатьЭлемент();
	обВерсия.Заполнить( пСтруктураПараметров );
	обВерсия.ПроверитьЗаполнение();
	обВерсия.Записать();
	
	Возврат обВерсия.Ссылка;
	
КонецФункции	//СоздатьВерсиюБизнесПроцесса

Функция СоздатьРеквизитПоСтруктуре( пСтруктураПараметров ) Экспорт
	
	новОбъект = Справочники.упРеквизиты.СоздатьЭлемент();
	новОбъект.Заполнить( пСтруктураПараметров );
	новОбъект.ПроверитьЗаполнение();
	новОбъект.Записать();
	
	глСозданНовыйОбъект( новОбъект.Ссылка );
	
	Возврат новОбъект.Ссылка;
	
КонецФункции	//СоздатьРеквизит


Функция ПолучитьГруппуРеквизитов( пИмяГруппы , пРодитель = Неопределено ) Экспорт
	
	Если пРодитель = Неопределено Тогда
		
		пРодитель = Справочники.упРеквизиты.ПустаяСсылка();
		
	КонецЕсли;
	
	Если ПустаяСтрока( пИмяГруппы ) Тогда
		
		Возврат пРодитель;
		
	КонецЕсли;
	
	Если СтрНайти( пИмяГруппы , "/" ) > 0 Тогда
		
		имяГруппы = пИмяГруппы;
		имяРодителя = упКоллекцииКлиентСервер.РазделитьСтроку( имяГруппы , "/" );
		
		Возврат ПолучитьГруппуРеквизитов( имяГруппы , ПолучитьГруппуРеквизитов( имяРодителя , пРодитель ) );
		
	КонецЕсли;
	
	Если СтрНайти( пИмяГруппы , "\" ) > 0 Тогда
		
		// обрабатываем оба слеша
		
		имяГруппы = пИмяГруппы;
		имяРодителя = упКоллекцииКлиентСервер.РазделитьСтроку( имяГруппы , "\" );
		
		Возврат ПолучитьГруппуРеквизитов( имяГруппы , ПолучитьГруппуРеквизитов( имяРодителя , пРодитель ) );
		
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упРеквизиты.Ссылка
	               |ИЗ
	               |	Справочник.упРеквизиты КАК упРеквизиты
	               |ГДЕ
	               |	упРеквизиты.ПометкаУдаления = ЛОЖЬ
	               |	И упРеквизиты.ЭтоГруппа = ИСТИНА
	               |	И упРеквизиты.Наименование = &Наименование
	               |	И упРеквизиты.Родитель = &Родитель";
	
	Запрос.УстановитьПараметр( "Наименование" , пИмяГруппы );
	Запрос.УстановитьПараметр( "Родитель" , пРодитель );
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Возврат Выборка.Ссылка;
		
	Иначе
		
		новГруппа = Справочники.упРеквизиты.СоздатьГруппу();
		новГруппа.Наименование = пИмяГруппы;
		новГруппа.Родитель = пРодитель;
		новГруппа.ПроверитьЗаполнение();
		новГруппа.Записать();
		
		глСозданНовыйОбъект( новГруппа.Ссылка );
		
		Возврат новГруппа.Ссылка;
		
	КонецЕсли;
	
КонецФункции	//ПолучитьГруппуРеквизитов

Функция ПолучитьГруппуБизнесПроцесса( пИмяГруппы ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упБизнесПроцессы.Ссылка
	|ИЗ
	|	Справочник.упБизнесПроцессы КАК упБизнесПроцессы
	|ГДЕ
	|	упБизнесПроцессы.ПометкаУдаления = ЛОЖЬ
	|	И упБизнесПроцессы.ЭтоГруппа = ИСТИНА
	|	И упБизнесПроцессы.Наименование = &Наименование";
	
	Запрос.УстановитьПараметр( "Наименование" , пИмяГруппы );
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Возврат Выборка.Ссылка;
		
	Иначе
		
		новГруппа = Справочники.упБизнесПроцессы.СоздатьГруппу();
		новГруппа.Наименование = пИмяГруппы;
		новГруппа.ПроверитьЗаполнение();
		новГруппа.Записать();
		
		Возврат новГруппа.Ссылка;
		
	КонецЕсли;
	
КонецФункции	//ПолучитьГруппуБизнесПроцесса


//////////////////////////////////////////////////////////////////////////////////////
//МАРШРУТНЫЕ РЕКВИЗИТЫ

Процедура СоздатьМаршрутныеРеквизиты( пВерсияБП , пСостояния = Неопределено ) Экспорт
	
	обработкаСоздания = Обработки.упСозданиеМаршрутныхРеквизитов.Создать();
	обработкаСоздания.Версия = пВерсияБП;
	обработкаСоздания.мМассивСостояний = пСостояния;
	обработкаСоздания.СоздатьМаршрутныеРеквизиты();
	
КонецПроцедуры


//////////////////////////////////////////////////////////////////////////////////////
//ПРОВЕРКА КОРРЕКТНОСТИ ВЕРСИИ БП

Функция ПроверитьКорректностьВерсииБП( пВерсияБП ) Экспорт
	
	Отказ = Ложь;
	
	упОбщегоНазначенияКлиентСервер.СообщитьПользователюССервера( упИК.т__ПроверкаБП_ПроверкаКорректности( пВерсияБП ) , пВерсияБП );
	
	ПроверитьЭлементыВерсииБП( пВерсияБП, Отказ );
	ПроверитьСоответствияРазделенийСлияниям( пВерсияБП, Отказ );
	ПроверитьУникальностьИменРеквизитов( пВерсияБП, Отказ );
	
	Если Отказ Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюССервера( упИК.т__ПроверкаБП_ВерсияСодержитОшибки( пВерсияБП ) , пВерсияБП );
		
	Иначе
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюССервера( упИК.т__ПроверкаБП_ВерсияКорректна( пВерсияБП ) , пВерсияБП );
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

Процедура ПроверитьЭлементыВерсииБП( пВерсияБП, Отказ )
	
	Для Каждого цЭлемент Из Справочники.упВерсии.ВсеОбъектыВерсии( пВерсияБП ) Цикл
		
		обЭлемент = цЭлемент.ПолучитьОбъект();
		
		Если обЭлемент = Неопределено Тогда
			
			упОбщегоНазначенияКлиентСервер.СообщитьПользователю( упИК.т__ПроверкаЭлемента_ОшибкаПолученияОбъекта( цЭлемент ) , пВерсияБП ,,, Отказ );
			
			Отказ = Истина;
			
		ИначеЕсли обЭлемент.ПроверитьЗаполнение() Тогда
			
			// все хорошо
			
		Иначе
			
			// были ошибки
			Отказ = Истина;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ПроверитьСоответствияРазделенийСлияниям( пВерсияБП, Отказ )
	
	Если Не Справочники.упСостояния.ПолучитьМассивСлияний( пВерсияБП ).Количество() = Справочники.упСостояния.ПолучитьМассивРазделений( пВерсияБП ).Количество() Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюССервера( упИК.т__ПроверкаЭлемента_РазличноеКоличествоРазделенийИСлияний() , пВерсияБП );
		
		Отказ = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьУникальностьИменРеквизитов( пВерсияБП, Отказ, пИмя = "" ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упВерсииРеквизитыБизнесПроцесса.НомерСтроки,
	               |	упВерсииРеквизитыБизнесПроцесса.СсылкаНаРеквизит,
	               |	упВерсииРеквизитыБизнесПроцесса.СсылкаНаРеквизит.Имя КАК Имя
	               |ПОМЕСТИТЬ втРеквизиты
	               |ИЗ
	               |	Справочник.упВерсии.РеквизитыБизнесПроцесса КАК упВерсииРеквизитыБизнесПроцесса
	               |ГДЕ
	               |	упВерсииРеквизитыБизнесПроцесса.Ссылка = &ВерсияБП
	               |	И (&Имя = """"
	               |			ИЛИ упВерсииРеквизитыБизнесПроцесса.СсылкаНаРеквизит.Имя = &Имя)
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Имя
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	втРеквизиты.Имя КАК Имя,
	               |	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ втРеквизиты.СсылкаНаРеквизит) КАК Количество
	               |ПОМЕСТИТЬ втКоличестваРазличныхИмен
	               |ИЗ
	               |	втРеквизиты КАК втРеквизиты
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	втРеквизиты.Имя
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Имя
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	втРеквизиты.НомерСтроки,
	               |	втРеквизиты.СсылкаНаРеквизит,
	               |	КоличестваРазличныхИмен.Имя
	               |ИЗ
	               |	втКоличестваРазличныхИмен КАК КоличестваРазличныхИмен
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втРеквизиты КАК втРеквизиты
	               |		ПО КоличестваРазличныхИмен.Имя = втРеквизиты.Имя
	               |ГДЕ
	               |	КоличестваРазличныхИмен.Количество > 1
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ втКоличестваРазличныхИмен
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ втРеквизиты";
	
	Запрос.УстановитьПараметр("ВерсияБП", пВерсияБП);
	Запрос.УстановитьПараметр("Имя", пИмя);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	шаблонПутиКРеквизиту = "Объект.РеквизитыБизнесПроцесса[%1].СсылкаНаРеквизит";
	
	Пока Выборка.Следующий() Цикл
		
		текстСообщения =упИК.т__ПроверкаБП_ИмяРеквизитаНеУникально( Выборка.Имя, пВерсияБП );
		путьКРеквизиту = стрЗаполнить( шаблонПутиКРеквизиту, Выборка.НомерСтроки - 1 );
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения , пВерсияБП, путьКРеквизиту,, Отказ );
		
	КонецЦикла;
	
КонецПроцедуры


//////////////////////////////////////////////////////////////////////////////////////
//ПУБЛИКАЦИЯ ВЕРСИИ БП

Процедура ОпубликоватьВерсию( пВерсияБП ) Экспорт
	
	Если ПроверитьКорректностьВерсииБП( пВерсияБП ) Тогда
		
		обВерсия = пВерсияБП.ПолучитьОбъект();
		обВерсия.ОпубликоватьВерсию();
		
	КонецЕсли;
	
КонецПроцедуры




