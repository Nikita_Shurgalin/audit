#Область СлужебныйПрограммныйИнтерфейс

// Выполняет попытку обновления ключа сессии на сервере
// (на сервере предприятия должны быть установлены сертификаты для подписания и пароль).
// 
// Параметры:
// 	ПараметрыЗапроса - (См. ИнтерфейсАвторизацииИСМПКлиентСервер.ПараметрыЗапросаКлючаСессии) 
//						- Параметры запроса ключа сессии для организации для которой необходимо обновить ключ сессии.
// Возвращаемое значение:
// 	Булево - Истина, если обновление ключа сессии выполнено успешно.
Функция ОбновитьКлючСессииНаСервере(ПараметрыЗапроса) Экспорт
	
	СертификатыДляПодписанияНаСервере = к2ИнтерфейсАвторизацииИСМПСлужебный.СертификатыДляПодписанияНаСервере();
	Если СертификатыДляПодписанияНаСервере = Неопределено
		Или СертификатыДляПодписанияНаСервере.Сертификаты.Количество() = 0 Тогда
		Возврат Ложь;
	КонецЕсли;
	
	РезультатЗапроса = к2ИнтерфейсАвторизацииИСМПВызовСервера.ЗапроситьПараметрыАвторизации(ПараметрыЗапроса);
	Если РезультатЗапроса.ПараметрыАвторизации = Неопределено Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если ПараметрыЗапроса.Организация = Неопределено Тогда
		СтрокаСертификата = СертификатыДляПодписанияНаСервере.Сертификаты[0];
	Иначе
		СтрокаСертификата = СертификатыДляПодписанияНаСервере.Сертификаты.Найти(ПараметрыЗапроса.Организация, "Организация");
	КонецЕсли;
	
	Если СтрокаСертификата <> Неопределено Тогда
		
		// Для авторизации требуется прикрепленная подпись
		ПараметрыCMS = ЭлектроннаяПодпись.ПараметрыCMS();
		ПараметрыCMS.Открепленная = Ложь;
		
		СертификатыДляПодписанияНаСервере.МенеджерКриптографии.ПарольДоступаКЗакрытомуКлючу = СтрокаСертификата.Пароль;
		РезультатПодписания = к2ИнтерфейсАвторизацииИСМПСлужебный.Подписать(
			РезультатЗапроса.ПараметрыАвторизации.Данные,
			ПараметрыCMS,
			СтрокаСертификата.СертификатКриптографии,
			СертификатыДляПодписанияНаСервере.МенеджерКриптографии);
		
	КонецЕсли;
	
	Если СтрокаСертификата = Неопределено
		Или Не РезультатПодписания.Успех Тогда
		
		Возврат Ложь;
		
	Иначе
		
		Возврат ЗапроситьУстановитьКлючСессии(
			ПараметрыЗапроса,
			РезультатЗапроса.ПараметрыАвторизации,
			РезультатПодписания.Подпись).КлючСессииУстановлен;
		
	КонецЕсли;
	
КонецФункции

// Запрашивает ключ сессии и устанавливает его в параметры сеанса.
// 
// Параметры:
// 	ПараметрыЗапроса - (См. ИнтерфейсАвторизацииИСМПКлиентСервер.ПараметрыЗапросаКлючаСессии) 
//						- Параметры запроса ключа сессии для организации для которой необходимо обновить ключ сессии.
// 	ПараметрыАвторизации - (См. ИнтерфейсАвторизацииИСМПСлужебный.ПараметрыАвторизации).
// 	Подпись - Строка - Подпись.
// Возвращаемое значение:
// 	Булево - Ключ сессии успешно запрошен и установлен
Функция ЗапроситьУстановитьКлючСессии(ПараметрыЗапроса, ПараметрыАвторизации, Подпись) Экспорт
	
	ВозвращаемоеЗначение = Новый Структура;
	ВозвращаемоеЗначение.Вставить("КлючСессииУстановлен", Ложь);
	ВозвращаемоеЗначение.Вставить("ТекстОшибки",          "");
	
	СвойстваПодписи = Новый Структура("Подпись", Подпись);
	
	ПараметрыЗапросаПоОрганизации = Новый Структура;
	ПараметрыЗапросаПоОрганизации.Вставить("ПараметрыЗапроса",     ПараметрыЗапроса);
	ПараметрыЗапросаПоОрганизации.Вставить("ПараметрыАвторизации", ПараметрыАвторизации);
	ПараметрыЗапросаПоОрганизации.Вставить("СвойстваПодписи",      СвойстваПодписи);
	
	РезультатЗапросаКлючаСессии = к2ИнтерфейсАвторизацииСУЗВызовСервера.ЗапроситьКлючСессии(ПараметрыЗапросаПоОрганизации);
	Если РезультатЗапросаКлючаСессии.ПараметрыКлючаСессии <> Неопределено Тогда
		
		УстановитьКлючСессии(
			ПараметрыЗапросаПоОрганизации.ПараметрыЗапроса,
			РезультатЗапросаКлючаСессии.ПараметрыКлючаСессии);
		
		ВозвращаемоеЗначение.КлючСессииУстановлен = Истина;
		
		Возврат ВозвращаемоеЗначение;
		
	Иначе
		
		ВозвращаемоеЗначение.КлючСессииУстановлен = Ложь;
		ВозвращаемоеЗначение.ТекстОшибки          = РезультатЗапросаКлючаСессии.ТекстОшибки;
		
		Возврат ВозвращаемоеЗначение;
		
	КонецЕсли;
	
КонецФункции

// Устанавливает ключ сессии в параметры сеанса.
// 
// Параметры:
// 	ПараметрыЗапроса - Структура (См. ИнтерфейсАвторизацииИСМПКлиентСервер.ПараметрыЗапросаКлючаСессии) 
// 						- Параметры запроса ключа сессии для организации для которой необходимо обновить ключ сессии.
// 	ПараметрыКлючаСессии - структура - структура параметров ключа сессии 
Процедура УстановитьКлючСессии(ПараметрыЗапроса, ПараметрыКлючаСессии) Экспорт
	
	Попытка
		ДанныеКлючаСессии = ПараметрыСеанса[ПараметрыЗапроса.ИмяПараметраСеанса].Получить();
	Исключение
		ДанныеКлючаСессии = Неопределено;
	КонецПопытки;
	
	Если ДанныеКлючаСессии = Неопределено Тогда
		ДанныеКлючаСессии = Новый Соответствие;
	КонецЕсли;
	
	ДанныеКлючаСессии.Вставить(ПараметрыЗапроса.Организация, ПараметрыКлючаСессии);
	
	ПараметрыСеанса[ПараметрыЗапроса.ИмяПараметраСеанса] = Новый ХранилищеЗначения(ДанныеКлючаСессии);
	
КонецПроцедуры

// Возвращает ключ сессии для обмена с СУЗ.
// 
// Параметры:
// 	ПараметрыЗапроса - Структура (См. ИнтерфейсАвторизацииИСМПКлиентСервер.ПараметрыЗапросаКлючаСессии) 
//						- Параметры запроса ключа сессии.
// 	СрокДействия - Дата, Неопределено - Срок действия ключа сессии.
// 	ОбновлятьКлючСессииНаСервере - Булево - признак обновления ключа сессии на сервере.
//
// Возвращаемое значение:
// 	Строка, Неопределено - Действующий ключ сессии для организации.
Функция ПроверитьОбновитьКлючСессии(ПараметрыЗапроса, Знач СрокДействия = Неопределено, ОбновлятьКлючСессииНаСервере = Истина) Экспорт
	
	КлючСессии = к2ИнтерфейсАвторизацииСУЗВызовСервера.ТекущийКлючСессии(ПараметрыЗапроса, СрокДействия);
	
	ТребуетсяОбновлениеКлючаСессии = (КлючСессии = Неопределено);
	
	Если ТребуетсяОбновлениеКлючаСессии
		И ОбновлятьКлючСессииНаСервере
		И ОбновитьКлючСессииНаСервере(ПараметрыЗапроса) Тогда
		КлючСессии = к2ИнтерфейсАвторизацииСУЗВызовСервера.ТекущийКлючСессии(ПараметрыЗапроса);
	КонецЕсли;
	
	Возврат КлючСессии;
	
КонецФункции

#КонецОбласти