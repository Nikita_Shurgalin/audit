#Область СлужебныеПроцедурыИФункции

Функция StartExchange(MobileDeviceID)
	Возврат к2МобильноеПриложение_Склад.НачалоОбмена(MobileDeviceID);
КонецФункции

Функция GetSettings(MobileDeviceID)
	Возврат к2МобильноеПриложение_Склад.ВыгрузитьНастройки(MobileDeviceID);	
КонецФункции

Функция GetCatalogs(MobileDeviceID)
	Возврат к2МобильноеПриложение_Склад.ВыгрузитьСправочники(MobileDeviceID);	
КонецФункции

Функция GetNomenclature(MobileDeviceID, Address)
	Возврат к2МобильноеПриложение_Склад.ВыгрузитьНоменклатуру(MobileDeviceID, Address);	
КонецФункции

Функция GetPackingList(MobileDeviceID)
	Возврат к2МобильноеПриложение_Склад.ВыгрузитьУпаковочныеЛисты(MobileDeviceID);
КонецФункции

Функция FillRecounts(MobileDeviceID, MessageExchange)
	Возврат к2МобильноеПриложение_Склад.ЗагрузитьПересчеты(MobileDeviceID, MessageExchange);
КонецФункции

#КонецОбласти