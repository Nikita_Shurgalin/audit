
#Область СлужебныйПрограммныйИнтерфейс

// Валюта - рубль
// 
// Возвращаемое значение:
//  СправочникСсылка.Валюты - валюта RUB
//
Функция Валюта_Рубль() Экспорт
	
	Возврат Справочники.Валюты.НайтиПоКоду( "643" );
	
КонецФункции

// Имена форм, в которых используется версионирование
// 
// Возвращаемое значение:
//  Массив - Массив строк
//
Функция ФормыСВерсионированием() Экспорт
	
	возможныеФормы = Новый Массив;
	возможныеФормы.Добавить( ".Форма.ФормаДокумента" );
	возможныеФормы.Добавить( ".Форма.ФормаЭлемента" );
	возможныеФормы.Добавить( ".Форма.ФормаСписка" );
	
	формы = Новый Массив;
	
	Для каждого цТип Из Метаданные.ОпределяемыеТипы.ВерсионируемыеДанные.Тип.Типы() Цикл
		
		Для каждого цПостфикс Из возможныеФормы Цикл
			
			мета = Метаданные.НайтиПоТипу( цТип );
			
			Если Не мета = Неопределено Тогда
				
				имяФормы = мета.ПолноеИмя();
				
				формы.Добавить( имяФормы + цПостфикс );
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЦикла;
	
	Возврат формы;
	
КонецФункции

// Возвращает допустимые значения переданного показателя.
//
// Параметры:
//  Показатель - СправочникСсылка.к2ПоказателиАнализовНоменклатуры, СправочникСсылка.к2ПоказателиСкладов - Показатель анализа.
//
// Возвращаемое значение:
//   Массив - Список допустимых значений показателя.
//
Функция МассивЗначенийПоказателя(Показатель) Экспорт
	
	ТипЗначения = ТипЗнч(Показатель);
	
	Если ТипЗначения = Тип("СправочникСсылка.к2ПоказателиАнализовНоменклатуры") Тогда
		МассивЗначений = Справочники.к2ПоказателиАнализовНоменклатуры.МассивЗначенийПоказателя(Показатель);
	ИначеЕсли ТипЗначения = Тип("СправочникСсылка.к2ПоказателиСкладов") Тогда
		МассивЗначений = Справочники.к2ПоказателиСкладов.ДопустимыеЗначения(Показатель);
	Иначе
		ВызватьИсключение НСтр("ru = 'Неверный параметр ""Показатель"".
			|Ожидается: СправочникСсылка.к2ПоказателиАнализовНоменклатуры, СправочникСсылка.к2ПоказателиСкладов'");
	КонецЕсли;
	
	Возврат МассивЗначений;
	
КонецФункции

// Возвращает структуру по ответственному лицу указанного склада
//
// Параметры:
//  Склад - СправочникСсылка.Склады - склад
//
// Возвращаемое значение:
//  Структура - Структура с полями: "Ответственный" и "ОтветственныйДолжность".
//
Функция ПолучитьОтветственногоПоСкладу( Знач Склад ) Экспорт
	
	реквизитыСклада = глРеквизиты( Склад, "ТекущийОтветственный, ТекущаяДолжностьОтветственного" );
	
	Если ЗначениеЗаполнено( реквизитыСклада.ТекущийОтветственный )
		ИЛИ ЗначениеЗаполнено( реквизитыСклада.ТекущаяДолжностьОтветственного ) Тогда
		
		Возврат Новый Структура( "Ответственный, ОтветственныйДолжность",
								 реквизитыСклада.ТекущийОтветственный,
								 реквизитыСклада.ТекущаяДолжностьОтветственного );
		
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

// Возвращает структуру по ответственному лицу используемого последним в РТУ
//
// Параметры:
//  Склад - СправочникСсылка.Склады - склад
//
// Возвращаемое значение:
//  Структура - Структура с полями: "Ответственный", "ОтветственныйДолжность" и "Период".
//
Функция ПолучитьОтветственногоЗаОтгрузкуПоСтатистикеРТУ( Знач Склад ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	               |	РеализацияТоваровУслугТовары.Ссылка.Отпустил КАК Отпустил,
	               |	РеализацияТоваровУслугТовары.Ссылка.ОтпустилДолжность КАК ОтпустилДолжность,
	               |	РеализацияТоваровУслугТовары.Ссылка.Дата КАК Период
	               |ИЗ
	               |	Документ.РеализацияТоваровУслуг.Товары КАК РеализацияТоваровУслугТовары
	               |ГДЕ
	               |	РеализацияТоваровУслугТовары.Склад = &Склад
	               |	И РеализацияТоваровУслугТовары.Ссылка.Дата > &Дата
	               |	И РеализацияТоваровУслугТовары.Ссылка.Проведен
	               |	И (РеализацияТоваровУслугТовары.Ссылка.Отпустил <> ЗНАЧЕНИЕ(Справочник.к2ФизическиеЛица.ПустаяСсылка)
	               |			ИЛИ РеализацияТоваровУслугТовары.Ссылка.ОтпустилДолжность <> """")
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	РеализацияТоваровУслугТовары.Ссылка.Ссылка.Дата УБЫВ";
	Запрос.УстановитьПараметр("Склад", Склад );
	Запрос.УстановитьПараметр("Дата", ДобавитьМесяц( ТекущаяДатаСеанса(), -1) );
	
	УстановитьПривилегированныйРежим( Истина );
	РезультатЗапроса = Запрос.Выполнить();
	УстановитьПривилегированныйРежим( Ложь );
	
	выборка = РезультатЗапроса.Выбрать();
	
	Если выборка.Следующий() Тогда
		
		структ = Новый Структура( "Ответственный, ОтветственныйДолжность, Период" );
		ЗаполнитьЗначенияСвойств( структ, выборка );
		Возврат структ;
		
	Иначе
		
		Возврат Неопределено;
		
	КонецЕсли;
	
КонецФункции

// Возвращает массив реквизитов справочника.
//
// Параметры:
//		ИмяСправочника - Строка - имя справочника как оно задано в метаданных
// Возвращаемое значение:
//		Массив из строка - стандартные и обычные реквизиты справочника.
//
Функция РеквизитыСправочника(Знач пИмяСправочника) Экспорт
	
	реквизиты = Новый Массив;
	МетаданныеСправочника = Метаданные.Справочники[пИмяСправочника];
	
	Для Каждого цРеквизит Из МетаданныеСправочника.СтандартныеРеквизиты Цикл
		реквизиты.Добавить(цРеквизит.Имя);
	КонецЦикла;
	
	Для Каждого цРеквизит Из МетаданныеСправочника.Реквизиты Цикл
		реквизиты.Добавить(цРеквизит.Имя);
	КонецЦикла;
	
	Возврат реквизиты;
	
КонецФункции

// Значение константы "Разрешить изменение утвержденных спецификаций"
// 
// Возвращаемое значение:
//  Булево - разрешено ли изменять утвержденные спецификации
//
Функция РазрешитьИзменениеУтвержденныхСпецификаций() Экспорт

	УстановитьПривилегированныйРежим(Истина);
	
	значение = Константы.к2РазрешитьИзменениеУтвержденныхСпецификаций.Получить();
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат значение;

КонецФункции

// Значение константы "Склад отгрузки по умолчанию"
// 
// Возвращаемое значение:
//  СправочникСсылка.Склады - Склад отгрузки в заказе, если в ТЧ Товары не указан другой.
//
Функция СкладОтгрузкиПоУмолчанию() Экспорт

	УстановитьПривилегированныйРежим(Истина);
	
	значение = Константы.к2СкладОтгрузкиПоУмолчанию.Получить();
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат значение;

КонецФункции

// Значение константы "Единица измерения веса"
// 
// Возвращаемое значение:
//  СправочникСсылка.УпаковкиЕдиницыИзмерения - основная единица измерения веса.
//
Функция ЕдиницаИзмеренияВеса() Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Возврат Константы.к2ЕдиницаИзмеренияВеса.Получить();
	
КонецФункции

// Функция - Значения веса объема коэффициента прочих реквизитов упаковки
//
// Параметры:
//  Упаковка							 - СправочникСсылка.УпаковкиЕдиницыИзмерения - упаковка, реквизиты которой нужно получить
//  Номенклатура						 - СправочникСсылка.Номенклатура			 - обязательно для указания, если упаковка НЕ типа "Упаковка"
//  ПрочиеРеквизиты						 - Строка									 - названия других реквизитов через запятую. Не допускается вложенное обращение (например, "Родитель.Наименование" - нельзя).
// 
// Возвращаемое значение:
//  Структура - структура со значениями коэффициента, веса в константе - единице измерения веса, объема в константе -
//      единице измерения объема и прочих реквизитов упаковки.
//
Функция КоэффициентПрочиеРеквизитыУпаковки(Упаковка, Номенклатура, ПрочиеРеквизиты = "") Экспорт
	
	Значение = Справочники.УпаковкиЕдиницыИзмерения.КоэффициентПрочиеРеквизитыУпаковки(Упаковка, Номенклатура, ПрочиеРеквизиты);
	
	Возврат Значение;
	
КонецФункции

// Возвращает ссылку на номенклатуру, соответствующую переданному штрихкоду или Неопределено, если штрихкод не зарегистрирован.
//
// Параметры:
//  Штрихкод - Строка - штрихкод
// Возвращаемое значение:
//  Структура, Неопределено - Результат поиска номенклатуры по штрихкоду.
//
Функция СтруктураНоменклатурыПоШтрихкоду(Штрихкод) Экспорт
	
	Возврат РегистрыСведений.к2ШтрихкодыНоменклатуры.СтруктураНоменклатурыПоШтрихкоду(Штрихкод);
	
КонецФункции

// Возвращает значение реквизита, прочитанного из информационной базы по ссылке на объект.
//
// Параметры:
//  Ссылка - ЛюбаяСсылка, ЛюбойОбъект - объект, значения реквизитов которого необходимо получить.
//  ИмяРеквизита - Строка - имя получаемого реквизита.
//
// Возвращаемое значение:
//  Произвольный - зависит от типа значения прочитанного реквизита.
//
Функция ЗначениеРеквизита(Ссылка, ИмяРеквизита) Экспорт
	
	Возврат глРеквизит(Ссылка, ИмяРеквизита);
	
КонецФункции

// Возвращает значения реквизитов, прочитанные из информационной базы для нескольких объектов.
//
// Параметры:
//  Ссылка - ЛюбаяСсылка - объект, значения реквизитов которого необходимо получить.
//  ИменаРеквизитов - Строка - имена получаемых реквизитов, разделенные запятыми.
//
// Возвращаемое значение:
//  Структура - содержит имена (ключи) и значения затребованных реквизитов.
//
Функция ЗначенияРеквизитов(Ссылка, Знач ИменаРеквизитов) Экспорт
	
	Возврат глРеквизиты(Ссылка, ИменаРеквизитов);
	
КонецФункции

Функция ЗначениеКонстанты(Наименование, СообщатьОбОшибке = Ложь, ТекстОшибки = "", Отказ = Ложь) Экспорт
	
	Значение = Константы[Наименование].Получить();
	
	Если Не ЗначениеЗаполнено(Значение) Тогда
		
		ТекстСообщения = НСтр("ru = 'Константа ""%1"" не заполнена.'");
		ТекстСообщения = стрЗаполнить(ТекстСообщения, Метаданные.Константы[Наименование].Синоним);
		
		Если СообщатьОбОшибке Тогда
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		КонецЕсли;
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	КонецЕсли;
	
	Возврат Значение;
	
КонецФункции

#КонецОбласти

////////////////////////////////////////////////////////////////////////////////
// МЕТОДЫ ПОЛУЧЕНИЯ ЗНАЧЕНИЙ КОНСТАНТ

Функция ТекущийПользователь() Экспорт
	
	Возврат Пользователи.ТекущийПользователь();
	
КонецФункции

Функция ИнформационнаяБазаФайловая() Экспорт
	
	Возврат ОбщегоНазначения.ИнформационнаяБазаФайловая();
	
КонецФункции

Функция ПолучитьКлючАналитикиУчетаПоНоменклатуре( Знач Номенклатура, Знач Характеристика, Знач Серия ) Экспорт
	
	структ = Новый Структура( "Номенклатура,Характеристика,Серия", Номенклатура, Характеристика, Серия );
	
	Возврат РегистрыСведений.к2АналитикаУчетаНоменклатуры.ПолучитьКлючАналитикиУчетаПоНоменклатуре( структ );
	
КонецФункции

Функция ВесаДнейБалансировки_Вес( Знач ДеньНедели, Знач пТоварнаяКатегория, Знач пГруппаПланирования ) Экспорт

	Возврат РегистрыСведений.к2ВесаДнейБалансировки.Вес( ДеньНедели, пТоварнаяКатегория, пГруппаПланирования );

КонецФункции
