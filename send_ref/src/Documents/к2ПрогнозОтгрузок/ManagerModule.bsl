#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	// Создание запроса инициализации движений и заполнение его параметров.
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблица_к2ПрогнозОтгрузок(Запрос, ТекстыЗапроса, Регистры);
	
	// Исполнение запроса и выгрузка полученных таблиц для движений.
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	ИменаРеквизитов = "Ссылка";
	
	РеквизитыДокумента = глРеквизиты(ДокументСсылка, ИменаРеквизитов);
	
	Для Каждого цРеквизит Из РеквизитыДокумента Цикл
		
		Запрос.УстановитьПараметр(цРеквизит.Ключ, цРеквизит.Значение);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ПрогнозОтгрузок(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ПрогнозОтгрузок";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
    "ВЫБРАТЬ
    |	к2ПрогнозОтгрузокПоказатели.ГруппаПартнеров КАК ГруппаПартнеров,
    |	к2ПрогнозОтгрузокПоказатели.Номенклатура КАК Номенклатура,
    |	к2ПрогнозОтгрузокПоказатели.Характеристика КАК Характеристика,
    |	к2ПрогнозОтгрузокПоказатели.Дата КАК Период,
    |	к2ПрогнозОтгрузокПоказатели.ОбъемОтгрузок КАК ОбъемОтгрузок,
    |	к2ПрогнозОтгрузокПоказатели.Склад КАК Склад
    |ИЗ
    |	Документ.к2ПрогнозОтгрузок.Показатели КАК к2ПрогнозОтгрузокПоказатели
    |ГДЕ
    |	к2ПрогнозОтгрузокПоказатели.Ссылка = &Ссылка";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
