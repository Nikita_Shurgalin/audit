#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения( ДанныеЗаполнения, СтандартнаяОбработка )
	
	Если ТипЗнч( ДанныеЗаполнения ) = Тип( "Структура" ) Тогда
		
		ЗаполнитьПоСтруктуре( ДанныеЗаполнения );
		
	ИначеЕсли ТипЗнч( ДанныеЗаполнения ) = Тип( "ДокументСсылка.к2АнализыНоменклатуры" ) Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	к2АнализыНоменклатурыРезультатыАнализов.Показатель КАК Показатель,
		               |	к2АнализыНоменклатурыРезультатыАнализов.Ссылка.Номенклатура КАК Номенклатура,
		               |	к2АнализыНоменклатурыРезультатыАнализов.Ссылка.ВидАнализа КАК ВидАнализа,
		               |	к2АнализыНоменклатурыРезультатыАнализов.Показатель.ВидРезультатаАнализа КАК ВидРезультатаАнализа
		               |ИЗ
		               |	Документ.к2АнализыНоменклатуры.РезультатыАнализов КАК к2АнализыНоменклатурыРезультатыАнализов
		               |ГДЕ
		               |	к2АнализыНоменклатурыРезультатыАнализов.Ссылка = &Ссылка";  
		Запрос.УстановитьПараметр("Ссылка", ДанныеЗаполнения );
		
		выборка = Запрос.Выполнить().Выбрать();
		
		Пока выборка.Следующий() Цикл
			
			новСтрока = Нормативы.Добавить();
			
			ЗаполнитьЗначенияСвойств( новСтрока, выборка );
			
		КонецЦикла;
		
	КонецЕсли;
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ПриКопировании( ОбъектКопирования )
	
	СпособСоздания = Перечисления.к2СпособыСозданияДокументов.СозданВручную;
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ПередЗаписью( Отказ, РежимЗаписи, РежимПроведения )
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан( ЭтотОбъект );
	
	ЗаполнитьПредставленияВНормативах();
	
	к2ПроведениеСервер.ПередЗаписью(ЭтотОбъект, РежимЗаписи, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаПроведения( Отказ, РежимПроведения )
	
	к2ПроведениеСервер.ОбработкаПроведения(ЭтотОбъект, Отказ, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения( Отказ )
	
	к2ПроведениеСервер.ОбработкаУдаленияПроведения(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Процедура СформироватьСписокРегистровДляКонтроля() Экспорт 
	
	Массив = Новый Массив;
	
	ДополнительныеСвойства.ДляПроведения.Вставить( "РегистрыДляКонтроля", Массив );
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ИнициализацияИЗаполнение

Процедура ЗаполнитьПоСтруктуре( Знач ДанныеЗаполнения )
	
	к2ОбщегоНазначения.ЗаполнитьОбъектПоСтруктуре( ЭтотОбъект, ДанныеЗаполнения );
	
	Если ДанныеЗаполнения.Свойство("Номенклатура")
		И ЗначениеЗаполнено( ДанныеЗаполнения.Номенклатура )
		И ДанныеЗаполнения.Свойство("ВидАнализа")
		И ЗначениеЗаполнено( ДанныеЗаполнения.ВидАнализа ) Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		|	к2ВидыАнализовНоменклатурыПоказатели.Показатель КАК Показатель,
		|	к2ВидыАнализовНоменклатурыПоказатели.Показатель.ВидРезультатаАнализа КАК ВидРезультатаАнализа,
		|	к2ВидыАнализовНоменклатурыПоказатели.Ссылка КАК ВидАнализа,
		|	&Номенклатура КАК Номенклатура,
		|	&Характеристика КАК Характеристика
		|ИЗ
		|	Справочник.к2ВидыАнализовНоменклатуры.Показатели КАК к2ВидыАнализовНоменклатурыПоказатели
		|ГДЕ
		|	к2ВидыАнализовНоменклатурыПоказатели.Ссылка = &ВидАнализа";
		Запрос.УстановитьПараметр("ВидАнализа", ДанныеЗаполнения.ВидАнализа );
		Запрос.УстановитьПараметр("Номенклатура", ДанныеЗаполнения.Номенклатура );
		Запрос.УстановитьПараметр("Характеристика", ДанныеЗаполнения.Характеристика );
		
		Нормативы.Загрузить( Запрос.Выполнить().Выгрузить() );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьРеквизитыПоУмолчанию()
	
	Ответственный = глТекущийПользователь();
	
	Для каждого цСтрока Из Нормативы Цикл
		
		Если ЗначениеЗаполнено( цСтрока.ВидСоответствия ) Тогда
			Продолжить;
		КонецЕсли;
		
		Если цСтрока.ВидРезультатаАнализа.ТипЗначения.СодержитТип( Тип("Число") ) Тогда
			
			цСтрока.ВидСоответствия = Перечисления.к2АнализыНоменклатуры_ВидСоответствияНормативу.Диапазон;
			
		ИначеЕсли цСтрока.ВидРезультатаАнализа.ТипЗначения.СодержитТип( Тип("Булево") ) Тогда
			
			цСтрока.ВидСоответствия = Перечисления.к2АнализыНоменклатуры_ВидСоответствияНормативу.Равно;
			
		Иначе
			
			цСтрока.ВидСоответствия = Перечисления.к2АнализыНоменклатуры_ВидСоответствияНормативу.ВСписке;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Процедура ЗаполнитьПредставленияВНормативах()

	Для каждого цСтрока Из Нормативы Цикл
		
		Если цСтрока.ВидСоответствия = Перечисления.к2АнализыНоменклатуры_ВидСоответствияНормативу.Диапазон Тогда
			
			шаблон = "%1 <= X <= %2";
			
			цСтрока.Представление = СтрШаблон( шаблон, цСтрока.ЗначениеС, цСтрока.ЗначениеПо );
			
		ИначеЕсли цСтрока.ВидСоответствия = Перечисления.к2АнализыНоменклатуры_ВидСоответствияНормативу.Равно Тогда
			
			шаблон = "X = %1";
			
			цСтрока.Представление = СтрШаблон( шаблон, цСтрока.ЗначениеС );
			
		ИначеЕсли цСтрока.ВидСоответствия = Перечисления.к2АнализыНоменклатуры_ВидСоответствияНормативу.ВСписке Тогда
			
			структПоиска = Новый Структура("Номенклатура,ВидАнализа,Показатель");
			ЗаполнитьЗначенияСвойств( структПоиска, цСтрока );
			
			цСтрока.Представление = стрСобрать( ДопустимыеЗначения.Выгрузить( структПоиска, "Значение" ).ВыгрузитьКолонку(0), "; " );
			
		КонецЕсли;
		
	КонецЦикла;

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
