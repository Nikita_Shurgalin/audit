
#Область ПрограммныйИнтерфейс

// Устанавливает доступность кнопки вставки скопированных строк в зависимости от заполненности буфера обмена.
//
// Параметры:
//  ЭлементыФормы - ВсеЭлементыФормы - Элементы формы, на которой расположены кнопки копирования и вставки строк.
//  ИмяТЧ - Строка - Имя таблицы формы, в которой буду производиться вставка/копирование строк.
Процедура ПриСозданииНаСевере(ЭлементыФормы, ИмяТЧ = "Строки") Экспорт
	
	строкиВБуфере = ХранилищеОбщихНастроек.Загрузить("БуферОбменаТабличныеЧасти", ИмяТЧ);
	
	ЕстьСкопированныеСтроки = Не строкиВБуфере = Неопределено И строкиВБуфере.Количество() > 0;
	
	УстановитьВидимостьКнопок(ЭлементыФормы, ИмяТЧ, ЕстьСкопированныеСтроки);
	
КонецПроцедуры

// Копирует выделенные строки табличной части в буфер обмена.
//
// Параметры:
//  пТабличнаяЧасть - ДанныеФормыКоллекция - Табличная часть, в которой происходит копирование строк.
//  пВыделенныеСтроки - Массив - Массив идентификаторов выделенных строк табличной части.
//  пИмяТЧ - Строка - Имя табличной части для сохранения.
//
// Возвращаемое значение:
//  Число - Количество сохраненных строк.
//
Функция Копировать(Знач пТабличнаяЧасть, Знач пВыделенныеСтроки, Знач пИмяТЧ = "Строки") Экспорт
	
	кСохранению = пТабличнаяЧасть.Выгрузить();
	кСохранению.Очистить();
	
	Для Каждого цВыделеннаяСтрока Из пВыделенныеСтроки Цикл
		
		текСтрока = пТабличнаяЧасть.НайтиПоИдентификатору(цВыделеннаяСтрока);
		ЗаполнитьЗначенияСвойств(кСохранению.Добавить(), текСтрока);
		
	КонецЦикла;
	
	ХранилищеОбщихНастроек.Сохранить("БуферОбменаТабличныеЧасти", пИмяТЧ, кСохранению);
	
	Возврат кСохранению.Количество();
	
КонецФункции

// Вставляет строки табличной части из буфер обмена в табличную часть
//
// Параметры:
//  пТаблицаФормы		 - ДанныеФормыСтруктура - Данные объекта, в котором расположена табличная часть.
//  пТаблицаЭлемент		 - ЭлементФормы - Элемент формы, в который отображается табличная часть.
//  пИмяТЧ				 - Строка - Имя таблицы формы, в которой буду производиться вставка/копирование строк.
//  пКолонкиУникальности - Строка - колонки, уникальность которых нужно соблюдать.
//  пИсключаяКолонки	 - Строка - колонки, которые нужно оставить пустыми при вставке.
//
// Возвращаемое значение:
//  Число - количество вставленных строк.
//
Функция Вставить(пТаблицаФормы,
				 пТаблицаЭлемент,
				 пИмяТЧ               = "Строки",
				 пКолонкиУникальности = Неопределено,
				 пИсключаяКолонки     = Неопределено) Экспорт
	
	ВыделенныеСтроки = пТаблицаЭлемент.ВыделенныеСтроки;
	ВыделенныеСтроки.Очистить();
	
	строкиИзБуфера = ХранилищеОбщихНастроек.Загрузить("БуферОбменаТабличныеЧасти", пИмяТЧ);
	
	Если строкиИзБуфера = Неопределено Тогда
		
		Возврат 0;
	
	КонецЕсли;
	
	к2Коллекции.ОбеспечитьКолонки(строкиИзБуфера, пИсключаяКолонки);
	
	новСтрока = Неопределено;
	
	Если ЗначениеЗаполнено(пКолонкиУникальности) Тогда
		
		структПоиска = Новый Структура(пКолонкиУникальности);
		
	Иначе
		
		структПоиска = Неопределено;
		
	КонецЕсли;
	
	добавленоСтрок = 0;
	
	Для каждого цСтрока Из строкиИзБуфера Цикл
		
		Если Не структПоиска = Неопределено Тогда
			
			ЗаполнитьЗначенияСвойств(структПоиска, цСтрока);
			
			найденныеСтроки = пТаблицаФормы.НайтиСтроки(структПоиска);
			
			Для каждого цНайденнаяСтрока Из найденныеСтроки Цикл
				
				ВыделенныеСтроки.Добавить(цНайденнаяСтрока.ПолучитьИдентификатор());
				
			КонецЦикла;
			
			Если найденныеСтроки.Количество() > 0 Тогда
				
				Продолжить;
				
			КонецЕсли;
			
		КонецЕсли;
		
		новСтрока = пТаблицаФормы.Добавить();
		
		ЗаполнитьЗначенияСвойств(новСтрока, цСтрока, , пИсключаяКолонки);
		
		ВыделенныеСтроки.Добавить(новСтрока.ПолучитьИдентификатор());
		
		добавленоСтрок = добавленоСтрок + 1;
		
	КонецЦикла;
	
	Если Не новСтрока = Неопределено Тогда
		
		пТаблицаЭлемент.ТекущаяСтрока = новСтрока.ПолучитьИдентификатор();
		
	КонецЕсли;
	
	Возврат добавленоСтрок;
	
КонецФункции

// Помещает данные в буфер обмена.
//
// Параметры:
//   ЯчейкиБюджета - Массив - массив с копируемыми ячейками.
//
Процедура ПоместитьВыделенныеЯчейкиБюджетаВБуферОбмена(ЯчейкиБюджета) Экспорт
	
	СкопироватьТаблицуВБуферОбмена(ЯчейкиБюджета, "ЯчейкиБюджета");
	
КонецПроцедуры

// Получает данные из буфера обмена.
//
// Возвращаемое значение:
//   Массив - массив с вставляемыми ячейками.
//
Функция ПолучитьЯчейкиБюджетаИзБуфераОбмена() Экспорт
	
	ДанныеИзБуфераОбмена = ОбщегоНазначения.СтрокиИзБуфераОбмена();
	
	Если ДанныеИзБуфераОбмена = Неопределено Тогда
		
		Возврат Новый Массив;
	
	КонецЕсли;
	
	Если ДанныеИзБуфераОбмена.Источник <> "ЯчейкиБюджета" Тогда
		
		Возврат Новый Массив;
	
	КонецЕсли;
	
	Возврат ДанныеИзБуфераОбмена.Данные;
	
КонецФункции

#Область УстаревшиеПроцедурыИФункции

// Устарела. Следует использовать к2КопированиеСтрокСервер.Копировать
// Помещает данные в буфер обмена
//
// Параметры:
//  ВыделенныеСтроки - Массив - Массив идентификаторов выделенных строк
//  ТабЧасть -ТабличнаяЧасть - Табличная часть объекта с копируемыми строками.
//  ИмяТЧ - Строка - Имя таблицы формы, в которой буду производиться вставка/копирование строк.
//
Процедура ПоместитьВыделенныеСтрокиВБуферОбмена(ВыделенныеСтроки, ТабЧасть, ИмяТЧ = "Строки") Экспорт
	
	ТаблицаБуфераОбмена = ИнициализироватьТаблицуБуфераОбмена();
	
	ВыделенныеСтрокиТЧ = Новый Массив;
	
	Если ВыделенныеСтроки <> Неопределено Тогда
		
		Для Каждого ТекСтрока Из ВыделенныеСтроки Цикл
			
			СтрокаТаблицы = ТабЧасть.НайтиПоИдентификатору(ТекСтрока);
			ВыделенныеСтрокиТЧ.Добавить(СтрокаТаблицы);
			
		КонецЦикла;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ВыделенныеСтрокиТЧ) Тогда
		
		Для каждого СтрокаТаблицы Из ВыделенныеСтрокиТЧ Цикл
			
			НоваяСтрока = ТаблицаБуфераОбмена.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТаблицы);
			
		КонецЦикла;
	
	Иначе
		
		Для каждого СтрокаТаблицы Из ТабЧасть Цикл
			
			НоваяСтрока = ТаблицаБуфераОбмена.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТаблицы);
			
		КонецЦикла;
	
	КонецЕсли;
	
	СкопироватьТаблицуВБуферОбмена(ТаблицаБуфераОбмена, ИмяТЧ);
	
КонецПроцедуры

// Устарела. Следует использовать к2КопированиеСтрокСервер.Вставить
// Получает данные из буфера обмена.
//
// Параметры:
//  ИмяТЧ - Строка - Имя таблицы формы, в которой буду производиться вставка/копирование строк.
//
// Возвращаемое значение:
//   ТаблицаЗначений - Строки, полученные из буфера обмена.
//
Функция ПолучитьСтрокиИзБуфераОбмена(ИмяТЧ = "Строки") Экспорт
	
	СтрокиДляВставки = ХранилищеОбщихНастроек.Загрузить("БуферОбменаТабличныеЧасти", ИмяТЧ);
	
	Если СтрокиДляВставки = Неопределено Тогда
		
		Возврат Новый ТаблицаЗначений;
	
	Иначе
		
		Возврат СтрокиДляВставки;
	
	КонецЕсли;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ИнициализироватьТаблицуБуфераОбмена()
	
	ОписаниеТиповЧисло15_2 = к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(15, 2);
	ОписаниеТиповЧисло15_3 = к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(15, 3);
	
	ТаблицаСтрок = Новый ТаблицаЗначений();
	
	ТаблицаСтрок.Колонки.Добавить("НоменклатураНабора", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаСтрок.Колонки.Добавить("Номенклатура", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаСтрок.Колонки.Добавить("Соглашение", Новый ОписаниеТипов("СправочникСсылка.к2СоглашенияСКлиентами"));
	ТаблицаСтрок.Колонки.Добавить("УпаковочныйЛист", Новый ОписаниеТипов("ДокументСсылка.к2УпаковочныйЛист"));
	ТаблицаСтрок.Колонки.Добавить("ТипНоменклатуры", Новый ОписаниеТипов("ПеречислениеСсылка.к2ТипыНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("ХарактеристикаНабора", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	ТаблицаСтрок.Колонки.Добавить("Упаковка", Новый ОписаниеТипов("СправочникСсылка.УпаковкиЕдиницыИзмерения"));
	ТаблицаСтрок.Колонки.Добавить("КоличествоУпаковок", ОписаниеТиповЧисло15_3);
	ТаблицаСтрок.Колонки.Добавить("Количество", ОписаниеТиповЧисло15_3);
	ТаблицаСтрок.Колонки.Добавить("Цена", ОписаниеТиповЧисло15_2);
	ТаблицаСтрок.Колонки.Добавить("ПроцентРучнойСкидки", к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(5, 2));
	ТаблицаСтрок.Колонки.Добавить("СуммаРучнойСкидки", ОписаниеТиповЧисло15_2);
	ТаблицаСтрок.Колонки.Добавить("Сумма", ОписаниеТиповЧисло15_2);
	ТаблицаСтрок.Колонки.Добавить("СуммаБезНДС", ОписаниеТиповЧисло15_2);
		
	Возврат ТаблицаСтрок;
	
КонецФункции

Процедура СкопироватьТаблицуВБуферОбмена(Таблица, Источник = Неопределено)
	
	ХранилищеОбщихНастроек.Сохранить("БуферОбменаТабличныеЧасти", Источник, Таблица);
	
КонецПроцедуры

Процедура УстановитьВидимостьКнопок(Элементы, ИмяТЧ, ЕстьСкопированныеСтроки)
	
	Элементы[ИмяТЧ + "КопироватьСтроки"].Доступность = Истина;
	
	Если Не Элементы[ИмяТЧ + "ВставитьСтроки"].Доступность = ЕстьСкопированныеСтроки Тогда
		
		Элементы[ИмяТЧ + "ВставитьСтроки"].Доступность = ЕстьСкопированныеСтроки;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

