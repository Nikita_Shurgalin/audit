
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2Формы_Меркурий.ФормаСписка_ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПанельОтборов

&НаКлиенте
Процедура Подключаемый_ОбработкаПанелиОтборов(Элемент, СтандартнаяОбработка = Истина)
	
	к2ОтборыКлиент.ОбработкаПанелиОтборов(ЭтотОбъект, Элемент, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_УстановитьОтбор(Знач пИмяЭлемента, Знач пСохранять = Истина) Экспорт
	
	к2Отборы.УстановитьОтборСписку(ЭтотОбъект, пИмяЭлемента, пСохранять);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
