
#Область СлужебныйПрограммныйИнтерфейс

// Получает структуру параметров для подключения к сервисам Меркурия
//
// Параметры:
//  пПользователь - Неопределено, СправочникСсылка.к2Пользователи_Меркурий - Пользователь
// 
// Возвращаемое значение:
//  Структура - структура параметров.
//
Функция ПараметрыПодключенияПользователя( Знач пПользователь ) Экспорт
	
	параметрыПодключения = к2МеркурийВызовСервера.ПараметрыПодключения( пПользователь );
	
	#Если ВебКлиент Тогда
	
	параметрыПодключения.ВыполнятьОбменНаСервере = Истина;
	
	#КонецЕсли
	
	Возврат параметрыПодключения;
	
КонецФункции

// Получает структуру параметров для подключения к сервисам Меркурия на основе пользователя по умолчанию
// 
// Возвращаемое значение:
//  Структура - структура параметров.
//
Функция ПараметрыПодключенияПоУмолчанию() Экспорт
	
	пПользователь = к2МеркурийКлиентСерверПовтИсп.ПользовательПоУмолчанию();
	
	Возврат ПараметрыПодключенияПользователя( пПользователь );
	
КонецФункции

// Устарела. Следует использовать к2МеркурийКлиентСерверПовтИсп.ПараметрыПодключенияПользователя
// Получает структуру параметров для подключения к сервисам Меркурия
//
// Параметры:
//  пПользователь - Неопределено, СправочникСсылка.к2Пользователи_Меркурий - Пользователь
// 
// Возвращаемое значение:
//  Структура - структура параметров.
//
Функция ПараметрыПодключения( Знач пПользователь = Неопределено ) Экспорт
	
	Возврат ПараметрыПодключенияПоУмолчанию();
	
КонецФункции

// Получает параметры текущего пользователя для подключения к сервисам Меркурий
//
// Возвращаемое значение:
//   Структура - структура параметров.
//
Функция ПараметрыПользователя( Знач пПользователь ) Экспорт
	
	Возврат к2МеркурийВызовСервера.ПараметрыПользователя( пПользователь );
	
КонецФункции

// Получает текущего пользователя Меркурий для обмена
//
// Параметры:
//  пХозяйствующийСубъект - СправочникСсылка.к2ХозяйствующиеСубъекты_Меркурий - хозяйствующий субъект для обмена
// 
// Возвращаемое значение:
//  СправочникСсылка.к2Пользователи_Меркурий - текущий пользователь.
//
Функция ТекущийПользователь( Знач пХозяйствующийСубъект ) Экспорт
	
	Возврат к2МеркурийВызовСервера.ТекущийПользователь( пХозяйствующийСубъект );
	
КонецФункции

// Получает пользователя Меркурий для обмена, в которых инициатор не важен
//
// Возвращаемое значение:
//  СправочникСсылка.к2Пользователи_Меркурий - текущий пользователь.
//
Функция ПользовательПоУмолчанию() Экспорт
	
	Возврат ТекущийПользователь( Неопределено );
	
КонецФункции

// Все пользователи Меркурия, доступные этому пользователю
//
// Возвращаемое значение:
//   Массив - из СправочникСсылка.к2Пользователи_Меркурий
//
Функция ДоступныеПользователи() Экспорт
	
	Возврат к2МеркурийВызовСервера.ДоступныеПользователи();
	
КонецФункции

// Получает страну Российская Федерация
//
// Возвращаемое значение:
//  СправочникСсылка.к2КлассификаторСтранМеркурий - Страна РФ.
//
Функция Страна_Россия() Экспорт
	
	Возврат к2МеркурийВызовСервера.Страна_Россия();
	
КонецФункции

// Получает тип, продукцию и вид по наименованию
//
// Параметры:
//  пНаименованиеПродукции	 - СправочникСсылка.к2НаименованияПродукции_Меркурий - Наименование.
//
// Возвращаемое значение:
//  Структура -
//
Функция РеквизитыНаименованияПродукции( Знач пНаименованиеПродукции ) Экспорт
	
	Возврат к2МеркурийВызовСервера.РеквизитыНаименованияПродукции( пНаименованиеПродукции );
	
КонецФункции

// Получает тип, продукцию
//
// Параметры:
//  пВидПродукции - СправочникСсылка.к2КлассификаторВидовПродукции_Меркурий - Вид продукции.
//
// Возвращаемое значение:
//  Структура -
//
Функция РеквизитыВидаПродукции( Знач пВидПродукции ) Экспорт
	
	Возврат к2МеркурийВызовСервера.РеквизитыВидаПродукции( пВидПродукции );
	
КонецФункции

// Получает тип
//
// Параметры:
//  пПродукция - СправочникСсылка.к2КлассификаторПродукции_Меркурий - Продукция.
//
// Возвращаемое значение:
//  Структура -
//
Функция РеквизитыПродукции( Знач пПродукция ) Экспорт
	
	Возврат к2МеркурийВызовСервера.РеквизитыПродукции( пПродукция );
	
КонецФункции

// Значение константы "Меркурий: Режим отладки"
//
// Возвращаемое значение:
//  Булево - Режим отладки
//
Функция РежимОтладки() Экспорт
	
	Возврат к2МеркурийВызовСервера.РежимОтладки();
	
КонецФункции

// Значение константы "Меркурий: Запрет запросов с клиента"
//
// Возвращаемое значение:
//  Булево - Запрет запросов с клиента
//
Функция ЗапретЗапросовСКлиента() Экспорт
	
	Возврат к2МеркурийВызовСервера.ЗапретЗапросовСКлиента();
	
КонецФункции

// Количество секунд ожидания операции при обмене с ГИС Меркурий
//
// Возвращаемое значение:
//  Число - таймаут в секундах
//
Функция ТаймаутHTTPСоединения() Экспорт
	
	Возврат к2МеркурийВызовСервера.ТаймаутHTTPСоединения();
	
КонецФункции

// Количество часов для одного шага актуализации классификаторов
//
// Возвращаемое значение:
//  Число - шаг в часах
//
Функция ШагАктуализацииКлассификатора() Экспорт
	
	Возврат к2МеркурийВызовСервера.ШагАктуализацииКлассификатора();
	
КонецФункции

// Максимальное количество шагов для актуализации одного классификатора
//
// Возвращаемое значение:
//  Число - количество шагов
//
Функция КоличествоШаговАктуализацииКлассификатора() Экспорт
	
	Возврат к2МеркурийВызовСервера.КоличествоШаговАктуализацииКлассификатора();
	
КонецФункции

// Сохранение всех отправленных и принятых текстов запросов при обмене
//
// Возвращаемое значение:
//  Булево - сохранять
//
Функция СохранятьТекстыЗапросов() Экспорт
	
	Возврат к2МеркурийВызовСервера.СохранятьТекстыЗапросов() ИЛИ РежимОтладки();
	
КонецФункции

// Количество попыток выполнения запроса при ошибках интернета
//
// Возвращаемое значение:
//  Число - количество попыток
//
Функция ПопытокВыполненияЗапросаПриОшибкахИнтернета() Экспорт
	
	Возврат к2МеркурийВызовСервера.ПопытокВыполненияЗапросаПриОшибкахИнтернета();
	
КонецФункции

// Срок хранения устаревших записей
//
// Возвращаемое значение:
//  Число - срок в днях
//
Функция СрокХраненияУстаревшихЗаписей() Экспорт
	
	Возврат к2МеркурийВызовСервера.СрокХраненияУстаревшихЗаписей();
	
КонецФункции

#КонецОбласти
