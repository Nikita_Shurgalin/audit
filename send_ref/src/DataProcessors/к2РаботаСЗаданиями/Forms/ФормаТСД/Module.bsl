
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьУсловноеОформление();
	
	УстановитьЗначенияПоУмолчанию();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить(СкоростьПрокрутки);
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(, ЭтотОбъект, "СканерШтрихкода");
	
	ОбновитьФорму();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(, ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_к2Переработка" Тогда
		ОбновитьФорму();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыЗадания

&НаКлиенте
Процедура ЗаданияПриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаПараметрыТехнологическогоПроцесса(Команда)
	
	ОткрытьОбработку("к2УчетЛабораторныхАнализов");
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаИнгредиенты(Команда)
	
	ОткрытьОбработку("к2УчетРасходаИнгредиентов")
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаНачатьВарку(Команда)
	
	Серия = ОбщегоНазначенияКлиентСервер.СвойствоСтруктуры(Элементы.Задания.ТекущиеДанные, "Серия");
	
	Попытка
		
		НачатьВарку(Серия);
		
		ОбновитьФорму();
		
		УстановитьВидимостьДоступность();
		
	Исключение
		
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
		
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЗакончитьВарку(Команда)
	
	Серия = ОбщегоНазначенияКлиентСервер.СвойствоСтруктуры(Элементы.Задания.ТекущиеДанные, "Серия");
	
	Попытка
		
		ЗавершитьВарку(Серия);
		
		ОбновитьФорму();
		
	Исключение
		
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
		
	КонецПопытки;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеДанных

&НаКлиенте
Процедура ОбновитьФорму()
	
	к2КоллекцииКлиент.ДеревьяТаблицы_СохранитьСостояния(ЭтотОбъект, КлючиКоллекций());
	
	ОбновитьФормуНаСервере();
	
	к2КоллекцииКлиент.ДеревьяТаблицы_ВосстановитьСостояния(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьФормуНаСервере()
	
	Обработка = РеквизитФормыВЗначение("Объект");
	Обработка.ЗаполнитьЗадания(ЭтотОбъект);
	ЗначениеВРеквизитФормы(Обработка, "Объект");
	
	к2ПроизводствоСервер.УстановитьЗаголовокОбработки(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработкаЗадания

&НаКлиенте
Процедура ОткрытьОбработку(ИмяОбработки)
	
	Если к2ПроизводствоКлиент.ПроверитьЗаданиеДляОбработки(ИмяОбработки, Элементы.Задания.ТекущиеДанные, ЭтотОбъект) Тогда
		
		к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
		
		Попытка
			
			ОткрытьФорму(
				СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("Обработка.%1.Форма", ИмяОбработки),
				ПараметрыОбработки(ИмяОбработки, Элементы.Задания.ТекущаяСтрока),
				ЭтотОбъект,
				УникальныйИдентификатор,
				, ,
				Новый ОписаниеОповещения("ЗавершитьОбработкуЗадания", ЭтотОбъект));
				
		Исключение
			
			к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить(СкоростьПрокрутки);
			к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
			
		КонецПопытки;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ПараметрыОбработки(ИмяОбработки, ТекущаяСтрока)
	
	Возврат Обработки[ИмяОбработки].ПараметрыОбработкиЗадания(
		ЭтотОбъект,
		Объект.Задания.НайтиПоИдентификатору(ТекущаяСтрока));
	
КонецФункции

&НаКлиенте
Процедура ЗавершитьОбработкуЗадания(Результат, ДополнительныеПараметры) Экспорт
	
	ОбновитьФорму();
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить(СкоростьПрокрутки);
	
КонецПроцедуры

#КонецОбласти

#Область ИзменениеПериодаВарки

&НаСервереБезКонтекста
Процедура НачатьВарку(Серия)
	
	Обработки.к2РаботаСЗаданиями.НачатьВарку(Серия);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ЗавершитьВарку(Серия)
	
	Обработки.к2РаботаСЗаданиями.ЗавершитьВарку(Серия);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	Обработки.к2РаботаСЗаданиями.УстановитьОформлениеПоСостояниюЗадания(
		ЭтотОбъект,
		Элементы.Задания.Имя,
		Элементы.ЗаданияСостояние.ПутьКДанным);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	Обработки.к2РаботаСЗаданиями.УстановитьЗначенияПоУмолчанию(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьВидимостьДоступность()
	
	Элементы.КомандаПараметрыТехнологическогоПроцесса.Видимость = н_УказыватьПараметрыТехнологическогоПроцесса;
	Элементы.КомандаИнгредиенты.Видимость = н_ВидимостьКнопкиРасходИнгредиентов;
	
	Элементы.КомандаЗакончитьВарку.Доступность = Элементы.Задания.ТекущиеДанные <> Неопределено
		И ЗначениеЗаполнено(Элементы.Задания.ТекущиеДанные.НачалоВарки);
	Элементы.КомандаЗакончитьВарку.Видимость = Элементы.КомандаЗакончитьВарку.Доступность;
	Элементы.КомандаНачатьВарку.Видимость = Не Элементы.КомандаЗакончитьВарку.Доступность;
	
КонецПроцедуры

&НаКлиенте
Функция КлючиКоллекций()
	
	Ключи = Новый Соответствие;
	
	Свойства = к2КоллекцииКлиент.ДеревоТаблица_СвойстваКоллекцииВСтруктуру(
		Элементы.Задания.Имя,
		"ПроизводственноеЗадание, Номенклатура, Характеристика, Серия, Спецификация, СкладПолучатель",
		"Объект.Задания");
	
	Ключи.Вставить(Элементы.Задания.Имя, Свойства);
	
	Возврат Ключи;
	
КонецФункции

#КонецОбласти

#КонецОбласти