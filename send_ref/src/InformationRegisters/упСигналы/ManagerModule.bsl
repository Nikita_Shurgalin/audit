#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ЗаполнитьСигналы() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упСигналы.Ссылка
	|ИЗ
	|	Справочник.упСигналы КАК упСигналы
	|ГДЕ
	|	упСигналы.Активность
	|	И НЕ упСигналы.ПометкаУдаления";
	
	выборка = Запрос.Выполнить().Выбрать();
	
	нз = РегистрыСведений.упСигналы.СоздатьНаборЗаписей();
	
	нз.Прочитать();
	
	старДанные = нз.Выгрузить();
	
	нз.Очистить();
	нз.Записать();
	
	структПоиска = Новый Структура( "Источник, Описание" );
	
	Пока выборка.Следующий() Цикл
		
		старДанныеПоСигналу = старДанные.Скопировать( Новый Структура( "Сигнал", выборка.Ссылка ) ); 
		
		нз.Отбор.Сигнал.Установить(выборка.Ссылка);
		
		таблицаСигналов = упСКД.ТаблицаСКД(выборка.Ссылка);
		
		нз.Загрузить( таблицаСигналов );
		
		Для каждого цСтрока Из нз Цикл
			
			цСтрока.Сигнал = выборка.Ссылка;
			
			ЗаполнитьЗначенияСвойств( структПоиска, цСтрока );
			
			массивСтрок = старДанныеПоСигналу.НайтиСтроки( структПоиска );
			
			Если массивСтрок.Количество() > 0 Тогда
				
				цСтрока.ДатаОбновления = массивСтрок[0].ДатаОбновления;
				
			КонецЕсли;
			
			Если Не ЗначениеЗаполнено(цСтрока.ДатаОбновления) Тогда
				
				цСтрока.ДатаОбновления = ТекущаяДатаСеанса();
				
			КонецЕсли;
			
		КонецЦикла;
		
		Попытка
			нз.Записать();
		Исключение
			
			нз.Очистить();
			
			новСтрока = нз.Добавить();
			
			новСтрока.Сигнал = выборка.Ссылка;
			новСтрока.Описание = КраткоеПредставлениеОшибки(ИнформацияОбОшибке());
			
			нз.Записать();
			
		КонецПопытки;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецЕсли