
#Область ПрограммныйИнтерфейс

#Область ПанельОтборов

// Обработчик событий панели отборов
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма
//  Элемент	 - ПолеВвода, ПолеНадписи, Декорация - Элемент
//  СтандартнаяОбработка - Булево - признак выполнения стандартной обработки события
//
Процедура ОбработкаПанелиОтборов(Форма, Элемент, СтандартнаяОбработка = Истина) Экспорт
	
	обработано = Ложь;
	СтандартнаяОбработка = Ложь;
	
	ОбработатьНажатие_СвернутьРазвернутьПанель(Форма, Элемент, обработано);
	ОбработатьНажатие_СоздатьКоманду(Форма, Элемент, обработано);
	ОбработатьНажатие_ПредставлениеПериода(Форма, Элемент, обработано);
	
	Если Не обработано Тогда
		
		Форма.Подключаемый_УстановитьОтбор(Элемент.Имя);
		
	КонецЕсли;
	
КонецПроцедуры

// Обработчик одноименного события формы
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма-владелец
//  СтандартнаяОбработка - Булево - Признак стандартной обработки события
//
Процедура ПредставлениеПериодНажатие( Форма, СтандартнаяОбработка ) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	структПараметры = Новый Структура;
	
	структПараметры.Вставить( "Форма", Форма );
	структПараметры.Вставить( "ИмяПоля_Представление", "ПредставлениеПериод" );
	структПараметры.Вставить( "ИмяРеквизита_Период", "ОтборПериод" );
	структПараметры.Вставить( "Заголовок", Форма.Элементы.ПредставлениеПериод.Заголовок );
	структПараметры.Вставить( "ИмяПоляСписка", "Дата" );
	ВыбратьПериодДляОтбора( Форма, Форма.ОтборПериод, структПараметры );
	
КонецПроцедуры

// Обработчик одноименного события формы
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма-владелец
//  СтандартнаяОбработка - Булево - Признак стандартной обработки события
//
Процедура ПредставлениеПериодОтгрузкаНажатие( Форма, СтандартнаяОбработка ) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	структПараметры = Новый Структура;
	
	структПараметры.Вставить( "Форма", Форма );
	структПараметры.Вставить( "ИмяПоля_Представление", "ПредставлениеПериодОтгрузка" );
	структПараметры.Вставить( "ИмяРеквизита_Период", "ОтборПериодОтгрузка" );
	структПараметры.Вставить( "Заголовок", Форма.Элементы.ПредставлениеПериодОтгрузка.Заголовок );
	структПараметры.Вставить( "ИмяПоляСписка", "ДатаОтгрузки" );
	ВыбратьПериодДляОтбора( Форма, Форма.ОтборПериодОтгрузка, структПараметры );
	
КонецПроцедуры

// Обработчик одноименного события формы
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма-владелец
//  СтандартнаяОбработка - Булево - Признак стандартной обработки события
//
Процедура ПредставлениеПериодДоставкиНажатие( Форма, СтандартнаяОбработка ) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	структПараметры = Новый Структура;
	
	структПараметры.Вставить( "Форма", Форма );
	структПараметры.Вставить( "ИмяПоля_Представление", "ПредставлениеПериодДоставки" );
	структПараметры.Вставить( "ИмяРеквизита_Период", "ОтборПериодДоставки" );
	структПараметры.Вставить( "Заголовок", Форма.Элементы.ПредставлениеПериодДоставки.Заголовок );
	структПараметры.Вставить( "ИмяПоляСписка", "ДатаДоставки" );
	ВыбратьПериодДляОтбора( Форма, Форма.ОтборПериодДоставки, структПараметры );
	
КонецПроцедуры

// Открывает диалог выбора периода
//
// Параметры:
//  Форма		 - ФормаКлиентскогоПриложения - форма-владелец
//  Период		 - СтандартныйПериод - период
//  Параметры	 - Структура - дополнительные параметры.
//
Процедура ВыбратьПериодДляОтбора(Форма, Период, Параметры) Экспорт
	
	оповещение = Новый ОписаниеОповещения("ПредставлениеПериодаНажатиеЗавершение", к2ОтборыКлиент, Параметры);
	
	Диалог = Новый ДиалогРедактированияСтандартногоПериода;
	Диалог.Период = Период;
	Диалог.Показать(Оповещение);
	
КонецПроцедуры

// Открывает диалог выбора периода
//
// Параметры:
//  Форма		 - ФормаКлиентскогоПриложения - форма-владелец
//  Период		 - СтандартныйПериод - период
//  Параметры	 - Структура - дополнительные параметры.
//
Процедура ВыбратьИнтервалДляОтбора(Форма, Период, Параметры) Экспорт
	
	оповещение = Новый ОписаниеОповещения("ПредставлениеИнтервалНажатиеЗавершение", к2ОтборыКлиент, Параметры);
	
	Диалог = Новый ДиалогРедактированияСтандартногоПериода;
	Диалог.Период = Период;
	Диалог.Показать(Оповещение);
	
КонецПроцедуры

// Обработчик оповещения для ВыбратьПериодДляОтбора
//
// Параметры:
//  НовыйПериод	 - СтандартныйПериод - период
//  Параметры	 - Структура - дополнительные параметры.
//
Процедура ПредставлениеПериодаНажатиеЗавершение(НовыйПериод, Параметры) Экспорт
	
	Если НовыйПериод = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	установленныйПериод = УстановитьВыбранныйПериодВФорму(НовыйПериод, Параметры);
	
	к2ОтборыКлиентСервер.УстановитьОтборПоПериоду(
		Параметры.Форма.Список.КомпоновщикНастроек.Настройки.Отбор,
		установленныйПериод.ДатаНачала,
		установленныйПериод.ДатаОкончания,
		Параметры.ИмяПоляСписка);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПредставлениеИнтервалНажатиеЗавершение(НовыйПериод, Параметры) Экспорт
	
	Если НовыйПериод = Неопределено Тогда
		
		Возврат;
		
	КонецЕсли;
	
	УстановитьВыбранныйПериодВФорму(НовыйПериод, Параметры);
	
	Параметры.Форма.Подключаемый_УстановитьОтбор(Параметры.ИмяПоля_Представление);
	
КонецПроцедуры

Процедура ОбработатьНажатие_СвернутьРазвернутьПанель(Форма, Элемент, Обработано)
	
	Если Обработано Тогда
		
		Возврат;
	
	КонецЕсли;
	
	Для каждого цЭлемент Из Форма._ВсеСоответствияОтборов_ Цикл
		
		префикс = цЭлемент.Представление;
		
		Если Элемент.Имя = префикс + "СвернутьРазвернутьБоковуюПанельОтбора" Тогда
			
			к2ОтборыСлужебныйКлиентСервер.ПереключитьВидимостьПанелиОтборов(Форма, префикс);
			Обработано = Истина;
			Прервать;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ОбработатьНажатие_СоздатьКоманду(Форма, Элемент, Обработано)

	Если Обработано Тогда

		Возврат;

	КонецЕсли;

	Если Не ТипЗнч(Элемент) = Тип("КомандаФормы") Или Не Элемент.Имя = "СоздатьКомандуВПанелиПользователя" Тогда

		Возврат;

	КонецЕсли;

	Обработано = Истина;

	параметрыОткрытия = Новый Структура("Отбор", ТекущаяСтруктураОтбораФормы(Форма));

	к2ОбщегоНазначенияКлиент.СоздатьКомандуОткрытияАРМа(Форма.ИмяФормы, Форма.Заголовок, параметрыОткрытия);

КонецПроцедуры

Процедура ОбработатьНажатие_ПредставлениеПериода(Форма, Элемент, Обработано)
	
	Если Обработано Тогда
		
		Возврат;
	
	КонецЕсли;
	
	Если Не Элемент.Вид = ВидПоляФормы.ПолеНадписи Тогда
		
		Возврат;
	
	КонецЕсли;
	
	// Это гиперссылка выбора периода
	
	данныеОтбора = к2ОтборыСлужебныйКлиентСервер.ДанныеОтборов(Форма)[Элемент.Имя];
	
	параметрыВыбораПериода = Новый Структура;
	параметрыВыбораПериода.Вставить("Форма", Форма);
	параметрыВыбораПериода.Вставить("ИмяПоля_Представление", Элемент.Имя);
	параметрыВыбораПериода.Вставить("ИмяРеквизита_Период", данныеОтбора.Реквизит);
	параметрыВыбораПериода.Вставить("Заголовок", Элемент.Заголовок);
	
	ВыбратьИнтервалДляОтбора(Форма, Форма[данныеОтбора.Реквизит], параметрыВыбораПериода);
	Обработано = Истина;
	
КонецПроцедуры

Функция УстановитьВыбранныйПериодВФорму(НовыйПериод, Параметры)
	
	форма = Параметры.Форма;
	
	типПериода = ТипЗнч(НовыйПериод);
	
	Если типПериода = Тип("СтандартныйПериод") Тогда
		
		форма[Параметры.ИмяРеквизита_Период] = НовыйПериод;
		
	ИначеЕсли типПериода = Тип("Дата") Тогда
		
		форма[Параметры.ИмяРеквизита_Период].ДатаОкончания = НовыйПериод;
		
	Иначе
		
		ВызватьИсключение НСтр("ru='Некорректный тип у поля периода. Ожидали СтандартныйПериод или Дата.'");
		
	КонецЕсли;
	
	установленныйПериод = форма[Параметры.ИмяРеквизита_Период];
	
	форма[Параметры.ИмяПоля_Представление] = к2ОтборыКлиентСервер.ПолучитьПредставлениеПериода(
		установленныйПериод,
		Параметры.Заголовок);
	
	Возврат установленныйПериод;
	
КонецФункции

Функция ТекущаяСтруктураОтбораФормы(Форма)

	отбор = Новый Структура;

	Для Каждого цДанныеОтбора Из к2ОтборыСлужебныйКлиентСервер.ДанныеОтборов(Форма) Цикл

		параметрыОтбора = цДанныеОтбора.Значение;
		
		Если Не параметрыОтбора.ТипОтбора = "Обычный" Тогда
			Продолжить;
		КонецЕсли;
		
		поле = параметрыОтбора.Поле;
		имяРеквизита = параметрыОтбора.Реквизит;

		Если ЗначениеЗаполнено(Форма[имяРеквизита]) Тогда

			отбор.Вставить(поле, Форма[имяРеквизита]);

		КонецЕсли;
		
	КонецЦикла;

	Возврат отбор;

КонецФункции

#КонецОбласти