
#Область ПрограммныйИнтерфейс

// Добавляет новый элемент условного оформления
//
// Параметры:
//  Форма	 - ФормаКлиентскогоПриложения - форма.
//
// Возвращаемое значение:
//  ЭлементУсловногоОформленияКомпоновкиДанных - добавленный элемент условного оформления.
//
Функция НовоеОформлениеФормы(Форма) Экспорт
	
	новЭлемент = Форма.УсловноеОформление.Элементы.Добавить();
	новЭлемент.Использование = Истина;
	
	Возврат новЭлемент;
	
КонецФункции

// Добавляет новый элемент условного оформления для динамического списка
//
// Параметры:
//  Список - ДинамическийСписок - Список.
//
// Возвращаемое значение:
//  ЭлементУсловногоОформленияКомпоновкиДанных - добавленный элемент условного оформления.
//
Функция НовоеОформлениеСписка(Список) Экспорт
	
	ЭлементОформления = Список.КомпоновщикНастроек.Настройки.УсловноеОформление.Элементы.Добавить();
	ЭлементОформления.Использование = Истина;
	
	Возврат ЭлементОформления;
	
КонецФункции

#Область Отборы

// Добавляет группу отбора условного оформления
//
// Параметры:
//  УО - ЭлементУсловногоОформленияКомпоновкиДанных, ГруппаЭлементовОтбораКомпоновкиДанных - Элемент условного оформления
//  ТипГруппы - ТипГруппыЭлементовОтбораКомпоновкиДанных - значение отбора
//
// Возвращаемое значение:
//  ГруппаЭлементовОтбораКомпоновкиДанных - группа отбора
//
Функция ДобавитьГруппу(УО, Знач ТипГруппы = Неопределено) Экспорт
	
	Если ТипГруппы = Неопределено Тогда 
		ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИли;
	КонецЕсли;
	
	Если ТипЗнч(УО) = Тип("ГруппаЭлементовОтбораКомпоновкиДанных") Тогда 
		Группа = УО.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	Иначе 
		Группа = УО.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	КонецЕсли;
	
	Группа.ТипГруппы = ТипГруппы;
	Группа.Использование = Истина;
	
	Возврат Группа;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления
//
// Параметры:
//  пУО	- ЭлементУсловногоОформленияКомпоновкиДанных, ГруппаЭлементовОтбораКомпоновкиДанных - Элемент условного оформления
//  ПутьКДаннымПоля		- Строка - путь к данным поля
//  пЗначение			- Любое - значение отбора
//  ВидСравнения		- ВидСравненияКомпоновкиДанных - вид сравнения.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ДобавитьОтбор(пУО, Знач ПутьКДаннымПоля, Знач пЗначение, Знач ВидСравнения = Неопределено) Экспорт
	
	Если ВидСравнения = Неопределено Тогда
		
		ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		
	КонецЕсли;
	
	Если ТипЗнч(пУО) = Тип("ГруппаЭлементовОтбораКомпоновкиДанных") Тогда 
		Отбор = пУО.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	Иначе 
		Отбор = пУО.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	КонецЕсли;
	
	Отбор.ВидСравнения = ВидСравнения;
	Отбор.Использование = Истина;
	Отбор.ЛевоеЗначение = Новый ПолеКомпоновкиДанных(ПутьКДаннымПоля);
	Отбор.ПравоеЗначение = пЗначение;
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор на заполнение поля в группу отбора условного оформления
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_Заполнено(пУО, Знач ПутьКДаннымПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, Неопределено, ВидСравненияКомпоновкиДанных.Заполнено);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор на равенство поля Истина в группу отбора условного оформления
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_Истина(пУО, Знач ПутьКДаннымПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, Истина, ВидСравненияКомпоновкиДанных.Равно);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор на равенство поля Ложь в группу отбора условного оформления
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_Ложь(пУО, Знач ПутьКДаннымПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, Ложь, ВидСравненияКомпоновкиДанных.Равно);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор на НЕ заполнение поля в группу отбора условного оформления
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_НеЗаполнено(пУО, Знач ПутьКДаннымПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, Неопределено, ВидСравненияКомпоновкиДанных.НеЗаполнено);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления, что поле больше значения
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//  пЗначение - Любое - значение отбора.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_Больше(пУО, Знач ПутьКДаннымПоля, Знач пЗначение) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, пЗначение, ВидСравненияКомпоновкиДанных.Больше);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления, что поле меньше значения
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//  пЗначение - Любое - значение отбора.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_Меньше(пУО, Знач ПутьКДаннымПоля, Знач пЗначение) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, пЗначение, ВидСравненияКомпоновкиДанных.Меньше);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления, что поле равно значению
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//  пЗначение - Любое - значение отбора.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_Равно(пУО, Знач ПутьКДаннымПоля, Знач пЗначение) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, пЗначение, ВидСравненияКомпоновкиДанных.Равно);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления, что поле входит в список значений
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//  пЗначение - СписокЗначений - значение отбора.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_ВСписке(пУО, Знач ПутьКДаннымПоля, Знач пЗначение) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымПоля, пЗначение, ВидСравненияКомпоновкиДанных.ВСписке);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымЛевогоПоля - Строка - путь к данным поля
//  ПутьКДаннымПравогоПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_РавенствоПолей(пУО, Знач ПутьКДаннымЛевогоПоля, Знач ПутьКДаннымПравогоПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымЛевогоПоля, Новый ПолеКомпоновкиДанных(ПутьКДаннымПравогоПоля), ВидСравненияКомпоновкиДанных.Равно);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор в группу отбора условного оформления
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымЛевогоПоля - Строка - путь к данным поля
//  ПутьКДаннымПравогоПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_НеРавенствоПолей(пУО, Знач ПутьКДаннымЛевогоПоля, Знач ПутьКДаннымПравогоПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымЛевогоПоля, Новый ПолеКомпоновкиДанных(ПутьКДаннымПравогоПоля), ВидСравненияКомпоновкиДанных.НеРавно);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор, что левое поле больше правого поля в группу отбора условного оформления.
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымЛевогоПоля - Строка - путь к данным поля
//  ПутьКДаннымПравогоПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_ЛевоеБольшеПравого(пУО, Знач ПутьКДаннымЛевогоПоля, Знач ПутьКДаннымПравогоПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымЛевогоПоля, Новый ПолеКомпоновкиДанных(ПутьКДаннымПравогоПоля), ВидСравненияКомпоновкиДанных.Больше);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет отбор, что левое поле больше или равно правого поля в группу отбора условного оформления.
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных  - Элемент условного оформления
//  ПутьКДаннымЛевогоПоля - Строка - путь к данным поля
//  ПутьКДаннымПравогоПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Отбор_ЛевоеБольшеИлиРавноПравого(пУО, Знач ПутьКДаннымЛевогоПоля, Знач ПутьКДаннымПравогоПоля) Экспорт
	
	ДобавитьОтбор(пУО, ПутьКДаннымЛевогоПоля, Новый ПолеКомпоновкиДанных(ПутьКДаннымПравогоПоля), ВидСравненияКомпоновкиДанных.БольшеИлиРавно);
	
	Возврат к2УО;
	
КонецФункции

#КонецОбласти

#Область Поля

// Добавить оформляемое поле
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  ПутьКДаннымПоляИлиПоле - Строка, ПолеФормы - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ДобавитьПоле(пУО, Знач ПутьКДаннымПоляИлиПоле) Экспорт
	
	Если ТипЗнч(ПутьКДаннымПоляИлиПоле) = Тип("Строка") Тогда
		
		ПутьКДаннымПоля = ПутьКДаннымПоляИлиПоле;
		
	Иначе
		// Могли передать элемент
		
		ПутьКДаннымПоля = ПутьКДаннымПоляИлиПоле.Имя;
		
	КонецЕсли;
	
	ОформляемоеПоле = пУО.Поля.Элементы.Добавить();
	ОформляемоеПоле.Поле          = Новый ПолеКомпоновкиДанных(ПутьКДаннымПоля);
	ОформляемоеПоле.Использование = Истина;
	
	Возврат к2УО;
	
КонецФункции

// Добавляет несколько оформляемых полей
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  ПоляКомпоновкиДанных - Массив, строка - массив путей к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ДобавитьНесколькоПолей(пУО, Знач ПоляКомпоновкиДанных) Экспорт
	
	Если ТипЗнч(ПоляКомпоновкиДанных) = Тип("Массив") Тогда
		
		МассивПолей = ПоляКомпоновкиДанных;
		
	ИначеЕсли ТипЗнч(ПоляКомпоновкиДанных) = Тип("Строка") Тогда
		
		МассивПолей = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(СтрЗаменить(ПоляКомпоновкиДанных, Символы.ПС, ""));
		
	Иначе
		
		ВызватьИсключение "Неверный параметр ПоляКомпоновкиДанных. Ожидали массив или строку";
		
	КонецЕсли;
	
	Для каждого ИмяПоля Из МассивПолей Цикл
		
		ДобавитьПоле(пУО, ИмяПоля);
		
	КонецЦикла;
	
	Возврат к2УО;
	
КонецФункции

#КонецОбласти

#Область Оформление

// Добавляет элемент оформление в условное оформление
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  Идентификатор - Строка - Идентификатор параметра
//  Значение - Любое - значение оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ДобавитьОформление(пУО, Знач Идентификатор, Знач Значение) Экспорт
	
	пУО.Оформление.УстановитьЗначениеПараметра(Идентификатор, Значение);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет текста" с цветом подсказки
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция НедоступныйЦветТекста(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ЦветТекста", ЦветаСтиля.ТекстЗапрещеннойЯчейкиЦвет);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет фона"
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  пЦветФона - Цвет - устанавливаемое значение.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветФона(пУО, Знач пЦветФона) Экспорт
	
	ДобавитьОформление(пУО, "ЦветФона", пЦветФона);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет фона" с цветом к2ЦветВыделенияКрасный
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветФона_Красный(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ЦветФона", ЦветаСтиля.к2ЦветВыделенияКрасный);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет фона" Авто
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветФона_Авто(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ЦветФона", Новый Цвет); // // BSLLS:StyleElementConstructors-off
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет фона" с цветом к2ЦветВыделенияЗеленый
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветФона_Зеленый(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ЦветФона", ЦветаСтиля.к2ЦветВыделенияЗеленый);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет в оформление "Цвет фона" цвет к2ЦветФонаПоляРедактирования
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветРедактируемогоФона(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ЦветФона", ЦветаСтиля.к2ЦветФонаПоляРедактирования);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Шрифт", делая шрифт жирным
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  пИсходныйШрифт - Шрифт - исходный шрифт, который нужно сделать жирным.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЖирныйШрифт(пУО, Знач пИсходныйШрифт) Экспорт
	
	// АПК:1345-выкл Код не устанавливает новый шрифт, а всего лишь меняет
	// BSLLS:StyleElementConstructors-off
	ДобавитьОформление(пУО, "Шрифт", Новый Шрифт(пИсходныйШрифт, , , Истина));
	// АПК:1345-вкл
	// BSLLS:StyleElementConstructors-on
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Текст"
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  пТекст	 - Строка - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Текст(пУО, Знач пТекст) Экспорт
	
	ДобавитьОформление(пУО, "Текст", пТекст);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Текст" со значением из поля
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  ПутьКДаннымПоля - Строка - путь к данным поля.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ТекстИзПоля(пУО, Знач ПутьКДаннымПоля) Экспорт
	
	ДобавитьОформление(пУО, "Текст", Новый ПолеКомпоновкиДанных(ПутьКДаннымПоля));
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Отметка незаполненного" со значением Ложь
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ОтключитьОтметкуНезаполненного(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ОтметкаНезаполненного", Ложь);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Видимость" со значением Ложь
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ОтключитьВидимость(пУО) Экспорт
	
	ДобавитьОформление(пУО, "Видимость", Ложь);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Отображать" со значением Ложь
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ОтключитьОтображать(пУО) Экспорт
	
	ДобавитьОформление(пУО, "Отображать", Ложь);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Только просмотр"
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ТолькоПросмотр(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ТолькоПросмотр", Истина);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет текста"
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  пЦветТекст - Цвет - устанавливаемое значение.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветТекста(пУО, Знач пЦветТекст) Экспорт
	
	ДобавитьОформление(пУО, "ЦветТекста", пЦветТекст);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Цвет текста" - красный
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЦветТекста_Красный(пУО) Экспорт
	
	ДобавитьОформление(пУО, "ЦветТекста", ЦветаСтиля.к2ЦветТекста_Красный);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Доступность" со значением Ложь
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ОтключитьДоступность(пУО) Экспорт
	
	ДобавитьОформление(пУО, "Доступность", Ложь);
	
	Возврат к2УО;
	
КонецФункции

// Добавляет оформление "Формат"
//
// Параметры:
//  пУО - ЭлементУсловногоОформленияКомпоновкиДанных - Элемент условного оформления
//  пФормат	 - Строка - Элемент условного оформления.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ДобавитьФормат(пУО, Знач пФормат) Экспорт
	
	ДобавитьОформление(пУО, "Формат", пФормат);
	
	Возврат к2УО;
	
КонецФункции

#КонецОбласти

#Область ПредопределенныеОформления

// Устанавливает стандартное условное оформление для таблицы товары
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма документа.
//	ИмяТЧ - Строка - Имя табличной части для УО.
Процедура Товары(Форма, ИмяТЧ = "Товары") Экспорт
	
	элементы = Форма.Элементы;
	
	полеХарактеристики = элементы.Найти(ИмяТЧ + "Характеристика");
	
	Если Не полеХарактеристики = Неопределено Тогда

		Характеристика(Форма, полеХарактеристики.Имя, "Объект." + ИмяТЧ + ".ХарактеристикиИспользуются");

	КонецЕсли;
	
	полеСерия = элементы.Найти(ИмяТЧ + "Серия");
	
	Если Не полеСерия = Неопределено Тогда

		Серия(Форма, полеСерия.Имя, "Объект." + ИмяТЧ + ".СерииИспользуются");

	КонецЕсли;
	
	полеЕдиницаИзмерения = элементы.Найти(ИмяТЧ + "НоменклатураЕдиницаИзмерения");
	
	Если Не полеЕдиницаИзмерения = Неопределено Тогда
	
		ЕдиницаИзмерения(Форма, полеЕдиницаИзмерения.Имя, "Объект." + ИмяТЧ + ".Упаковка");
	
	КонецЕсли;
	
	НалогообложениеНДС(Форма, ИмяТЧ);
	Скидки(Форма, ИмяТЧ);
	
КонецПроцедуры

// Устанавливаем условное оформление для характеристик номенклатуры
//
// Параметры:
//  Форма - Форма - Содержит данную форму
//  ИмяПоляВводаХарактеристики - Строка - Наименование элемента формы, содержащего характеристики номенклатуры,
//											   если оно отличается от "ТоварыХарактеристика"
//  ПутьКПолюОтбора - Строка - Полный путь к реквизиту "характеристики используются",
//									если он отличается от "Объект.Товары.ХарактеристикиИспользуются".
//  УровеньОтображенияВДереве - Строка - уровень отображения в дереве.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Характеристика(Форма,
					   ИмяПоляВводаХарактеристики = "ТоварыХарактеристика",
					   ПутьКПолюОтбора            = "Объект.Товары.ХарактеристикиИспользуются",
					   УровеньОтображенияВДереве  = Неопределено) Экспорт
	
	новУО = НовоеОформлениеФормы(Форма);
	
	ДобавитьПоле(новУО, Форма.Элементы[ИмяПоляВводаХарактеристики].Имя);
	ДобавитьОтбор(новУО, ПутьКПолюОтбора, Ложь);
	
	Если Не УровеньОтображенияВДереве = Неопределено Тогда
		
		полеОтбора = СтрЗаменить(ПутьКПолюОтбора, "ХарактеристикиИспользуются", "Уровень");
		
		ДобавитьОтбор(новУО, полеОтбора, УровеньОтображенияВДереве);
		
	КонецЕсли;
	
	НедоступныйЦветТекста(новУО);
	Текст(новУО, НСтр("ru = '<не используются>'"));
	ТолькоПросмотр(новУО);
	ОтключитьОтметкуНезаполненного(новУО);
	
	Возврат к2УО;
	
КонецФункции

// Устанавливаем условное оформление для серий номенклатуры
//
// Параметры:
//  Форма - Форма - Содержит данную форму
//  ИмяПоляВводаСерии - Строка - Наименование элемента формы, содержащего серии номенклатуры,
//											   если оно отличается от "ТоварыСерия"
//  ПутьКПолюОтбора - Строка - Полный путь к реквизиту "серии используются",
//									если он отличается от "Объект.Товары.СерииИспользуются".
//  УровеньОтображенияВДереве - Строка - уровень отображения в дереве.
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция Серия(Форма,
			  ИмяПоляВводаСерии         = "ТоварыСерия",
			  ПутьКПолюОтбора           = "Объект.Товары.СерииИспользуются",
			  УровеньОтображенияВДереве = Неопределено) Экспорт
	
	новУО = НовоеОформлениеФормы(Форма);
	
	ДобавитьПоле(новУО, Форма.Элементы[ИмяПоляВводаСерии].Имя);
	ДобавитьОтбор(новУО, ПутьКПолюОтбора, Ложь);
	
	Если Не УровеньОтображенияВДереве = Неопределено Тогда
		
		полеОтбора = СтрЗаменить(ПутьКПолюОтбора, "СерииИспользуются", "Уровень");
		
		ДобавитьОтбор(новУО, полеОтбора, УровеньОтображенияВДереве);
		
	КонецЕсли;
	
	НедоступныйЦветТекста(новУО);
	Текст(новУО, НСтр("ru = '<не используются>'"));
	ТолькоПросмотр(новУО);
	ОтключитьОтметкуНезаполненного(новУО);
	
	Возврат к2УО;
	
КонецФункции

// Устанавливаем условное оформление для единиц измерения номенклатуры
//
// Параметры:
// 		Форма - Форма - Содержит данную форму
// 		ИмяПоляВводаЕдиницИзмерения - Строка - Наименование элемента формы, содержащего ед. измерения номенклатуры,
//									   			если оно отличается от "ТоварыНоменклатураЕдиницаИзмерения"
// 		ПутьКПолюОтбора - Строка - Полный путь к реквизиту "Упаковка",
//									если он отличается от "Объект.Товары.Упаковка".
//
//
// Возвращаемое значение:
//  ОбщийМодуль - модуль к2УО
Функция ЕдиницаИзмерения(Форма,
						 ИмяПоляВводаЕдиницИзмерения = "ТоварыНоменклатураЕдиницаИзмерения",
						 ПутьКПолюОтбора             = "Объект.Товары.Упаковка") Экспорт
	
	новУО = НовоеОформлениеФормы(Форма);
	
	ДобавитьПоле(новУО, Форма.Элементы[ИмяПоляВводаЕдиницИзмерения].Имя);
	
	ДобавитьОтбор(новУО, ПутьКПолюОтбора, Неопределено, ВидСравненияКомпоновкиДанных.Заполнено);
	
	ОтключитьОтображать(новУО);
	
	Возврат к2УО;
	
КонецФункции

// Устанавливает условное оформление поля доли стоимости.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма с элементами,
//  ИмяОформляемогоПоля - Строка - Имя поля доли стоимости,
//  ПутьКПолюОтбора - Строка - Путь к полю отбора.
//
Процедура ОформитьДолюСтоимости(Форма, ИмяОформляемогоПоля, ПутьКПолюОтбора) Экспорт
	
	ЭлементОформления = НовоеОформлениеФормы(Форма);
	
	ДобавитьПоле(ЭлементОформления, ИмяОформляемогоПоля);
	Отбор_Истина(ЭлементОформления, ПутьКПолюОтбора);
	Текст(ЭлементОформления, НСтр("ru = '<стоимость фиксирована>'"));
	НедоступныйЦветТекста(ЭлементОформления);
	ОтключитьОтметкуНезаполненного(ЭлементОформления);
	ТолькоПросмотр(ЭлементОформления);
	
КонецПроцедуры

// Устанавливает оформление значений показателей анализов.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма с элементами
//  ПутьКДаннымВидаРезультата - Строка -
//  ПоказателиЗначение - ПолеФормы -
//  ПоказателиЗначениеДополнительное - ПолеФормы -
//
Процедура ОформитьЗначенияПоказателей(Форма, ПутьКДаннымВидаРезультата, ПоказателиЗначение, ПоказателиЗначениеДополнительное) Экспорт
	
	ВидыРезультатаСДополнительнымЗначением = ВидыРезультатаАнализаСДополнительнымЗначением();
	
	ЭлементОформления = НовоеОформлениеФормы(Форма);
	
	ДобавитьПоле(ЭлементОформления, ПоказателиЗначениеДополнительное.Имя);
	Отбор_ВСписке(ЭлементОформления, ПутьКДаннымВидаРезультата, ВидыРезультатаСДополнительнымЗначением);
	ТолькоПросмотр(ЭлементОформления);
	
	Для Каждого КлючИЗначение Из ПодсказкиВводаЗначенийПоказателейАнализов() Цикл
		
		ВидРезультатаАнализа = КлючИЗначение.Ключ;
		Подсказки = КлючИЗначение.Значение;
		
		ЭлементОформления = НовоеОформлениеФормы(Форма);
		
		ДобавитьПоле(ЭлементОформления, ПоказателиЗначение.Имя);
		ДобавитьОтбор(ЭлементОформления, ПутьКДаннымВидаРезультата, ВидРезультатаАнализа);
		Отбор_НеЗаполнено(ЭлементОформления, ПоказателиЗначение.ПутьКДанным);
		НедоступныйЦветТекста(ЭлементОформления);
		Текст(ЭлементОформления, Подсказки.Значение);
		
		ЭлементОформления = НовоеОформлениеФормы(Форма);
		
		ДобавитьПоле(ЭлементОформления, ПоказателиЗначениеДополнительное.Имя);
		ДобавитьОтбор(ЭлементОформления, ПутьКДаннымВидаРезультата, ВидРезультатаАнализа);
		Отбор_НеЗаполнено(ЭлементОформления, ПоказателиЗначениеДополнительное.ПутьКДанным);
		НедоступныйЦветТекста(ЭлементОформления);
		Текст(ЭлементОформления, Подсказки.ЗначениеДополнительное);
		
	КонецЦикла;
	
КонецПроцедуры

// Настраивает УО для Дополнительных полей количеств.
// 
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма документа
//  ПараметрыПолей - Структура Параметры полей
Процедура ДополнительныеПоляКоличеств(Форма, ПараметрыПолей) Экспорт
	
	// Представление пустых значений
	
	элементУО = НовоеОформлениеФормы(Форма);

	ДобавитьПоле(элементУО, ПараметрыПолей.ИмяКоличество1);

	Отбор_НеЗаполнено(элементУО, ПараметрыПолей.ПутьКоличество1);

	НедоступныйЦветТекста(элементУО);
	Текст(элементУО, НСтр("ru='<Количество 1>'"));
	
	элементУО = НовоеОформлениеФормы(Форма);

	ДобавитьПоле(элементУО, ПараметрыПолей.ИмяКоличество2);

	Отбор_НеЗаполнено(элементУО, ПараметрыПолей.ПутьКоличество2);

	НедоступныйЦветТекста(элементУО);
	Текст(элементУО, НСтр("ru='<Количество 2>'"));
	
	// Настройка видимости

	элементУО = НовоеОформлениеФормы(Форма);

	ДобавитьПоле(элементУО, ПараметрыПолей.ИмяКоличество1);
	ДобавитьПоле(элементУО, ПараметрыПолей.ИмяЕдиницаИзмерения1);

	скрываемыеВарианты = Новый СписокЗначений();

	скрываемыеВарианты.Добавить(Перечисления.к2ИспользованиеДопЕдиницИзмерения.ПустаяСсылка());
	скрываемыеВарианты.Добавить(Перечисления.к2ИспользованиеДопЕдиницИзмерения.НеИспользовать);

	Отбор_ВСписке(элементУО, ПараметрыПолей.ПутьКРеквизитуИспользования, скрываемыеВарианты);

	ОтключитьВидимость(элементУО);
	
	элементУО = НовоеОформлениеФормы(Форма);

	ДобавитьПоле(элементУО, ПараметрыПолей.ИмяКоличество2);
	ДобавитьПоле(элементУО, ПараметрыПолей.ИмяЕдиницаИзмерения2);

	скрываемыеВарианты = Новый СписокЗначений();

	скрываемыеВарианты.Добавить(Перечисления.к2ИспользованиеДопЕдиницИзмерения.ПустаяСсылка());
	скрываемыеВарианты.Добавить(Перечисления.к2ИспользованиеДопЕдиницИзмерения.НеИспользовать);
	скрываемыеВарианты.Добавить(Перечисления.к2ИспользованиеДопЕдиницИзмерения.Одна);

	Отбор_ВСписке(элементУО, ПараметрыПолей.ПутьКРеквизитуИспользования, скрываемыеВарианты);

	ОтключитьВидимость(элементУО);
	
КонецПроцедуры

// Устанавливает условное оформление списку по цвету оформления
//
// Параметры:
//  УсловноеОформлениеКД - УсловноеОформленияКомпоновкиДанных - условное оформление
//	Отбор 	 - Структура - структура отборов
//  ИмяПоляЦвета		 - Строка - поле-источник
//  ОформляемоеПоле		 - Строка - поле для оформления
//
Процедура ПоСостоянию(УсловноеОформлениеКД, Отбор = Неопределено, ИмяПоляЦвета = "ЦветОформления", ОформляемоеПоле = Неопределено) Экспорт
	
	УдаляемыеЭлементы = Новый Массив;
	
	Для Каждого цУО Из УсловноеОформлениеКД.Элементы Цикл
		
		Если цУО.ИдентификаторПользовательскойНастройки = "ЦветОформления" Тогда
			
			УдаляемыеЭлементы.Добавить( цУО );
			
		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого цУОКУдалению Из УдаляемыеЭлементы Цикл
		
		УсловноеОформлениеКД.Элементы.Удалить( цУОКУдалению );
		
	КонецЦикла;
	
	Если Отбор = Неопределено Тогда
		Отбор = Новый Структура;
	КонецЕсли;
	
	ВсеПараметрыОтбора = Новый Структура;
	ВсеПараметрыОтбора.Вставить("ТипДокумента", "");
	
	ЗаполнитьЗначенияСвойств(ВсеПараметрыОтбора, Отбор);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	к2СостоянияДокументов.ЦветОформления КАК ЦветОформления
	|ИЗ
	|	Справочник.к2СостоянияДокументов КАК к2СостоянияДокументов
	|ГДЕ
	|	ВЫБОР
	|			КОГДА &ОтборПоТипамИспользуется
	|					И НЕ к2СостоянияДокументов.Предопределенный
	|				ТОГДА к2СостоянияДокументов.ТипДокумента ПОДОБНО &ТипДокумента
	|			ИНАЧЕ ИСТИНА
	|		КОНЕЦ";
	
	ОтборПоТипамИспользуется = ЗначениеЗаполнено(ВсеПараметрыОтбора.ТипДокумента);
	
	Запрос.УстановитьПараметр("ОтборПоТипамИспользуется", ОтборПоТипамИспользуется);
	Запрос.УстановитьПараметр("ТипДокумента", ВсеПараметрыОтбора.ТипДокумента);
		
	ТаблицаЦветов = Запрос.Выполнить().Выгрузить();
	
	ЦветаСостояний = ТаблицаЦветов.ВыгрузитьКолонку("ЦветОформления");
	
	Для Каждого ЦветОформления Из ЦветаСостояний Цикл
		
		Если ЗначениеЗаполнено( ЦветОформления ) Тогда
			
			цвет = к2ОбщегоНазначенияКлиентСервер.ЦветПоHEX( ЦветОформления );
			
		Иначе
			
			Продолжить;
			
		КонецЕсли;
		
		новУО = УсловноеОформлениеКД.Элементы.Добавить();
		
		ЦветФона( новУО, цвет );
		
		новУО.ИдентификаторПользовательскойНастройки = "ЦветОформления";
		новУО.РежимОтображения = РежимОтображенияЭлементаНастройкиКомпоновкиДанных.БыстрыйДоступ;
		новУО.Представление    = НСтр( "ru='Отображение цвета оформления'" );
		
		ДобавитьОтбор( новУО, ИмяПоляЦвета, ЦветОформления );
		
		Если ОформляемоеПоле <> Неопределено Тогда
			
			ДобавитьПоле( новУО, ОформляемоеПоле );
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

// Устанавливает видимость колонок, зависящих от Налогообложения НДС
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма документа.
//	ИмяТЧ - Строка - Имя табличной части для УО. 
//
Процедура НалогообложениеНДС(Форма, ИмяТЧ = "Товары") Экспорт

	Элементы = Форма.Элементы;

	ПолеСумма = Элементы.Найти(НазваниеПоля(ИмяТЧ, "Сумма"));
	ПолеСтавкаНДС = Элементы.Найти(НазваниеПоля(ИмяТЧ, "СтавкаНДС"));
	ПолеСуммаНДС = Элементы.Найти(НазваниеПоля(ИмяТЧ, "СуммаНДС"));

	Если ПолеСумма = Неопределено И ПолеСтавкаНДС = Неопределено И ПолеСуммаНДС = Неопределено Тогда

		Возврат;

	КонецЕсли;

	новУО = Форма.УсловноеОформление.Элементы.Добавить();

	сз = Новый СписокЗначений;
	сз.Добавить(Перечисления.к2НалогообложениеНДС.НеОблагаетсяНДС);
	сз.Добавить(Перечисления.к2НалогообложениеНДС.ОблагаетсяНДСУПокупателя);

	ДобавитьОтбор(новУО, "Объект.Налогообложение", сз, ВидСравненияКомпоновкиДанных.ВСписке);
	
	Если Не ПолеСумма = Неопределено Тогда

		ДобавитьПоле(новУО, ПолеСумма.Имя);

	КонецЕсли;
	
	Если Не ПолеСтавкаНДС = Неопределено Тогда

		ДобавитьПоле(новУО, ПолеСтавкаНДС.Имя);

	КонецЕсли;
	
	Если Не ПолеСуммаНДС = Неопределено Тогда

		ДобавитьПоле(новУО, ПолеСуммаНДС.Имя);

	КонецЕсли;
	
	ОтключитьВидимость(новУО);

КонецПроцедуры

// Устанавливает оформление колонкам со скидками
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма документа.
//	ИмяТЧ - Строка - Имя табличной части для УО. 
//
Процедура Скидки(Форма, ИмяТЧ = "Товары") Экспорт

	ПолеПроцентСкидкиНаценки = НазваниеПоля(ИмяТЧ, "ПроцентСкидкиНаценки");
	ПолеСуммаСкидкиНаценки = НазваниеПоля(ИмяТЧ, "СуммаСкидкиНаценки");
	ПолеДокументСкидкаНаценка = НазваниеПоля(ИмяТЧ, "СкидкаНаценка");

	ТЧВОбъекте = НазваниеПоля("Объект", ИмяТЧ, ".");

	найденныйЭлемент = Форма.Элементы.Найти(ПолеПроцентСкидкиНаценки); 

	Если Не найденныйЭлемент = Неопределено Тогда

		ПутьКПроцентСкидкиНаценки = НазваниеПоля(ТЧВОбъекте, "ПроцентСкидкиНаценки", ".");
	
		новУО = Форма.УсловноеОформление.Элементы.Добавить();
		Отбор_НеЗаполнено(новУО, ПутьКПроцентСкидкиНаценки);
		ДобавитьПоле(новУО, найденныйЭлемент.Имя);
		Текст(новУО, "%");
		НедоступныйЦветТекста(новУО);

	КонецЕсли;

	найденныйЭлемент = Форма.Элементы.Найти(ПолеСуммаСкидкиНаценки);

	Если Не найденныйЭлемент = Неопределено Тогда

		ПутьКСуммаСкидкиНаценки = НазваниеПоля(ТЧВОбъекте, "СуммаСкидкиНаценки", ".");
	
		новУО = Форма.УсловноеОформление.Элементы.Добавить();
		Отбор_НеЗаполнено(новУО, ПутьКСуммаСкидкиНаценки);
		ДобавитьПоле(новУО, найденныйЭлемент.Имя);
		Текст(новУО, НСтр("ru='сумма'"));
		НедоступныйЦветТекста(новУО);

	КонецЕсли;
	
	найденныйЭлемент = Форма.Элементы.Найти(ПолеДокументСкидкаНаценка);

	Если Не найденныйЭлемент = Неопределено Тогда

		ПутьКДокументу = НазваниеПоля(ТЧВОбъекте, "СкидкаНаценка", ".");
	
		новУО = Форма.УсловноеОформление.Элементы.Добавить();
		Отбор_НеЗаполнено(новУО, ПутьКДокументу);
		ДобавитьПоле(новУО, найденныйЭлемент.Имя);
		Текст(новУО, НСтр("ru='основание'"));
		НедоступныйЦветТекста(новУО);

	КонецЕсли;

КонецПроцедуры

// Изменяет оформление состояния на форме документа.
//
// Параметры:
//  Форма	 - ФормаКлиентскогоПриложения - Форма
//
Процедура ОбновитьОформлениеСостояния(Форма) Экспорт

	Объект = Форма.Объект;
	Элементы = Форма.Элементы;

	ЦветОформления = глРеквизит(Объект.Состояние, "ЦветОформления");

	Если ЗначениеЗаполнено(ЦветОформления) Тогда

		выбранныйЦвет = к2ОбщегоНазначенияКлиентСервер.ЦветПоHEX(ЦветОформления);

		Элементы.Состояние.ЦветФона = выбранныйЦвет;

	КонецЕсли;

КонецПроцедуры

// Устанавливает оформление для незаполненного поля
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма объекта.
//  Элемент - ПолеФормы - Настраиваемый элемент
//  ТекстЗаголовка	 - Строка - Отображаемый текст в поле
//
Процедура ТекстПустогоПоля(Форма, Элемент, ТекстЗаголовка = "") Экспорт
	
	новУО = НовоеОформлениеФормы(Форма);
	
	Отбор_НеЗаполнено(новУО, Элемент.ПутьКДанным);
	ДобавитьПоле(новУО, Элемент.Имя);
	НедоступныйЦветТекста(новУО);
	
	Если ТекстЗаголовка = "" Тогда
		
		ТекстЗаголовка = НСтр("ru = '<Не заполнено>'");
		
	КонецЕсли;
	
	Текст(новУО, ТекстЗаголовка);
	
КонецПроцедуры

// Устанавливает условное оформление в динамическом списке для колонки "Дата".
//
// Параметры:
//  Список - ДинамическийСписок - список
//  ИмяПоля - Строка - имя поля.
//
Процедура ДатаВСписке(Список, Знач ИмяПоля = "Дата") Экспорт
	
	ЭлементОформления = НовоеОформлениеСписка(Список);

	ЭлементОформления.Представление = НСтр("ru = 'Представление даты: ""02.04.2018""'");
	ДобавитьФормат(ЭлементОформления, "ДЛФ=D");
	ДобавитьПоле(ЭлементОформления, ИмяПоля);
	
	ЭлементОформления = НовоеОформлениеСписка(Список);
	ЭлементОформления.Представление = НСтр("ru='Формат поля Дата (сегодня - только время)'");
	
	ГруппаЭлементовОтбора = ДобавитьГруппу(ЭлементОформления, ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ);
	
	Отбор_Больше(ГруппаЭлементовОтбора, ИмяПоля, Новый СтандартнаяДатаНачала(ВариантСтандартнойДатыНачала.НачалоЭтогоДня));
	Отбор_Меньше(ГруппаЭлементовОтбора, ИмяПоля, Новый СтандартнаяДатаНачала(ВариантСтандартнойДатыНачала.НачалоСледующегоДня));
	ДобавитьФормат(ЭлементОформления, "ДФ=Ч:мм");
	
	ДобавитьПоле(ЭлементОформления, ИмяПоля);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция НазваниеПоля(ИмяТЧ, НазваниеПоля, Разделитель = "")

	Массив = Новый Массив;
	Массив.Добавить(ИмяТЧ);
	Массив.Добавить(НазваниеПоля);

	Возврат СтрСоединить(Массив, Разделитель);

КонецФункции 

Функция ПодсказкиВводаЗначенийПоказателейАнализов()
	
	Соответствие = Новый Соответствие;
	
	Ключ = ПредопределенноеЗначение("ПланВидовХарактеристик.к2ВидыРезультатовАнализов.ЗначениеИзСписка");
	Значение = Новый Структура("Значение, ЗначениеДополнительное", НСтр("ru='Значение'"), "");
	Соответствие.Вставить(Ключ, Значение);
	
	Ключ = ПредопределенноеЗначение("ПланВидовХарактеристик.к2ВидыРезультатовАнализов.Признак");
	Значение = Новый Структура("Значение, ЗначениеДополнительное", НСтр("ru='Значение'"), "");
	Соответствие.Вставить(Ключ, Значение);
	
	Ключ = ПредопределенноеЗначение("ПланВидовХарактеристик.к2ВидыРезультатовАнализов.ЧислоДробноеВДиапазоне");
	Значение = Новый Структура("Значение, ЗначениеДополнительное", НСтр("ru='Мин.'"), НСтр("ru='Макс.'"));
	Соответствие.Вставить(Ключ, Значение);
	
	Ключ = ПредопределенноеЗначение("ПланВидовХарактеристик.к2ВидыРезультатовАнализов.ЧислоЦелоеВДиапазоне");
	Значение = Новый Структура("Значение, ЗначениеДополнительное", НСтр("ru='Мин.'"), НСтр("ru='Макс.'"));
	Соответствие.Вставить(Ключ, Значение);
	
	Возврат Соответствие;
	
КонецФункции

Функция ВидыРезультатаАнализаСДополнительнымЗначением()
	
	СписокЗначений = Новый СписокЗначений;
	
	СписокЗначений.Добавить(ПланыВидовХарактеристик.к2ВидыРезультатовАнализов.ЗначениеИзСписка);
	СписокЗначений.Добавить(ПланыВидовХарактеристик.к2ВидыРезультатовАнализов.Признак);
	
	Возврат СписокЗначений;
	
КонецФункции

#КонецОбласти
