
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьЗначенияПоУмолчанию();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить(СкоростьПрокрутки);
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, ЭтотОбъект, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
	Если Событие = "Штрихкод" И ВводДоступен() Тогда
		ОбработатьШтрихКод(Данные);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ШтрихКодПриИзменении(Элемент)
	
	ОбработатьШтрихКод(ШтрихКод);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыПродукцияНЗП

&НаКлиенте
Процедура ПродукцияНЗППриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
КонецПроцедуры

&НаКлиенте
Процедура ПродукцияНЗПНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродукцияНЗПВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ОбработатьВыборЗначения(ВыбраннаяСтрока);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродукцияНЗПВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ОбработатьВыборЗначения(Значение);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();
	
	СканерШтрихКодовПодключен = к2РаботаСПодключаемымОборудованием.СканерШтрихКодовПодключен();
	
	ПредставлениеНоменклатуры = Параметры.ПредставлениеНоменклатуры;
	
	ПартииНЗП.Очистить();
	
	ТаблицаПродукцииНЗП = ПолучитьИзВременногоХранилища(Параметры.АдресТаблицыПродукцииНЗП);
	
	Для Каждого Строка Из ТаблицаПродукцииНЗП Цикл
		
		ПредставлениеСерии = Строка(Строка.Серия);
		
		Если ЗначениеЗаполнено(Строка.ТараПродукции) Тогда
			ПредставлениеСерии = ПредставлениеСерии + " | " + Строка(Строка.ТараПродукции);
		КонецЕсли;
		
		НоваяСтрока = ПартииНЗП.Добавить();
		НоваяСтрока.Переработка = Строка.Переработка;
		НоваяСтрока.Серия = Строка.Серия;
		НоваяСтрока.Тара = Строка.ТараПродукции;
		НоваяСтрока.ПредставлениеСерии = ПредставлениеСерии;
		
	КонецЦикла;
	
	СкоростьПрокрутки = Параметры.УчетнаяТочка.СкоростьПрокрутки;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьВыборЗначения(ТекущаяСтрока)
	
	ТекущиеДанные = ПартииНЗП.НайтиПоИдентификатору(ТекущаяСтрока);
	
	Структура = СтруктураПартииПоУмолчанию();
	Структура.Серия = ТекущиеДанные.Серия;
	Структура.Переработка = ТекущиеДанные.Переработка;
	Структура.ТараПродукции = ТекущиеДанные.Тара;
	
	Закрыть(Структура);
	
КонецПроцедуры

#Область ШтрихКод

&НаКлиенте
Процедура ОбработатьШтрихКод(Знач ОбрабатываемыйШтрихКод)
	
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(СокрЛП(ОбрабатываемыйШтрихКод));
	
	ШтрихКод = ПараметрыШтрихкода.Штрихкод;
	
	ТекстОшибки = "";
	
	СтруктураПартии = СтруктураПартииПоШтрихКоду(ПараметрыШтрихкода, ТекстОшибки);
	
	Если Не СтруктураПартии = Неопределено Тогда
		
		Закрыть(СтруктураПартии);
		
	Иначе
		
		Если ЗначениеЗаполнено(ТекстОшибки) Тогда
			к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция СтруктураПартииПоШтрихКоду(ПараметрыШтрихкода, ТекстОшибки)
	
	СтруктураПартии = Неопределено;
	
	Если к2ШтрихкодыУпаковокКлиентСервер.ЭтоПараметрыШтрихКодаЭтикеткиПартии(ПараметрыШтрихкода) Тогда
		
		СтруктураДанныхШтрихКода = к2ШтрихкодыУпаковок.СтруктураДанныхЭтикеткиПартии(ПараметрыШтрихкода, ТекстОшибки);
		
		Если Не СтруктураДанныхШтрихКода = Неопределено Тогда
			
			СтруктураПоиска = Новый Структура;
			СтруктураПоиска.Вставить("Серия", СтруктураДанныхШтрихКода.Серия);
			
			МассивСтрок = ПартииНЗП.НайтиСтроки(СтруктураПоиска);
			
			КоличествоСтрок = МассивСтрок.Количество();
			
			Если КоличествоСтрок = 1 Тогда
				
				ПерваяСтрока = МассивСтрок[0];
				
				СтруктураПартии = СтруктураПартииПоУмолчанию();
				СтруктураПартии.Серия = ПерваяСтрока.Серия;
				СтруктураПартии.Переработка = ПерваяСтрока.Переработка;
				СтруктураПартии.ТараПродукции = ПерваяСтрока.Тара;
				
			ИначеЕсли КоличествоСтрок > 1 Тогда
				
				ШаблонСообщения = НСтр("ru = 'Не удалось однозначно определить строку партии. В таблице %1 строк с партией ""%2"".
											|Выберите партию вручную.'");
				
				ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения
					, КоличествоСтрок
					, СтруктураДанныхШтрихКода.Серия);
				
				к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
				
			Иначе
				
				ШаблонСообщения = НСтр("ru = 'Отсутствует расход материала для партии продукции %1.'");
				ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения
					, СтруктураДанныхШтрихКода.Серия);
				
				к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
				
			КонецЕсли;
			
		КонецЕсли;
		
	Иначе
		
		СтруктураПартии = Неопределено;
		
		Если ПараметрыШтрихкода.Свойство("ТекстОшибки") И Не ПустаяСтрока(ПараметрыШтрихкода.ТекстОшибки) Тогда
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ПараметрыШтрихкода.ТекстОшибки, ТекстОшибки);
		КонецЕсли;
		
		ШаблонСообщения = НСтр("ru='Неверный формат штрихкода ""%1"".'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ШтрихКод);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	КонецЕсли;
	
	Возврат СтруктураПартии;
	
КонецФункции

#КонецОбласти

#Область Прочее

&НаКлиентеНаСервереБезКонтекста
Функция СтруктураПартииПоУмолчанию()
	
	Структура = Новый Структура;
	Структура.Вставить("Серия");
	Структура.Вставить("Переработка");
	Структура.Вставить("ТараПродукции");
	
	Возврат Структура;
	
КонецФункции

#КонецОбласти

#КонецОбласти
