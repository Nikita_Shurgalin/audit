#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОписаниеПеременных
	
Перем мПериодСезонности;
Перем мТочкаОтсчета;
Перем мПоследнийФакт;
Перем мЗначения;
	
#КонецОбласти


#Область ПрограммныйИнтерфейс

// Установка гиперпараметров
//
// Параметры:
//  пПериодичность		 - ПеречисленияСсылка.к2Периодичность	 - Периодичность истории и прогноза
//  пПериодСезонности	 - Число	 - количество периодов в цикле сезонности (для недели и периодичности = День это 7)
//  пКоличествоПоследних - Число								 - количество последних элементов истории, принимаемых для построения модели.
//
Процедура Инициализировать( Знач пПериодичность,
							Знач пПериодСезонности,
							Знач пКоличествоПоследних = 0 ) Экспорт
	
	КоличествоПоследних = пКоличествоПоследних;
	Периодичность       = пПериодичность;
	мПериодСезонности   = пПериодСезонности;
	
	Если КоличествоПоследних > 0
		И КоличествоПоследних < мПериодСезонности Тогда
		
		ВызватьИсключение НСтр( "ru='Критическая ошибка инициализации модели <Наивная>. Параметр <Количество последних> не может быть меньше параметра <ПериодСезонности>'" );
		
	КонецЕсли;
	
КонецПроцедуры

// Строит модель по переданой истории
//
// Параметры:
//  пТаблицаИстории	 - ТаблицаЗначений - история для построения модели:
//  * Период - Дата - период истории
//  * Количество - Число - продажи в этот период.
//
Процедура Построить( Знач пТаблицаИстории ) Экспорт
	
	Если пТаблицаИстории.Количество() < мПериодСезонности Тогда
		
		мТочкаОтсчета  = Неопределено;
		мПоследнийФакт = Неопределено;
		мЗначения      = Неопределено;
		
	Иначе
		
		тз = к2Коллекции.Последние( пТаблицаИстории, мПериодСезонности );
		
		мТочкаОтсчета  = тз[0].Период;
		мПоследнийФакт = пТаблицаИстории[тз.Количество() -1].Период;
		
		мЗначения = тз.ВыгрузитьКолонку( "Количество" );
		
	КонецЕсли;
	
КонецПроцедуры

// Подставляет в таблицу прогноза значения прогноза согласно текущей модели.
// Если нужно выполнить прогноз на 3 периода, то в переданной строке должно быть 3 строки.
//
// Параметры:
//  пТаблицаПрогноза	 - ТаблицаЗначений - таблица прогноза:
//  * Период - Дата - период истории
//  * Прогноз - Число - будет заполнен согласно модели.
//
Процедура ВыполнитьПрогноз( пТаблицаПрогноза ) Экспорт
	
	Если пТаблицаПрогноза.Количество() = 0 Тогда
		
		Возврат;
	
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено( мТочкаОтсчета )
		ИЛИ Не ЗначениеЗаполнено( мПоследнийФакт ) Тогда
		
		Для каждого цСтрока Из пТаблицаПрогноза Цикл
			
			цСтрока.Прогноз = 0;
			
		КонецЦикла;
		
		Возврат;
		
	КонецЕсли;
	
	последняяДата = пТаблицаПрогноза[пТаблицаПрогноза.Количество() -1].Период;
	
	размерПрогноза = к2Даты.Разность(мПоследнийФакт, последняяДата, Периодичность);
	
	результатРасчета = ВыполнитьРасчет( размерПрогноза );
	
	соотПрогноз = Новый Соответствие;
	
	Для ц = 0 По результатРасчета.ВГраница() Цикл
		
		текПериод = к2Даты.Добавить(мТочкаОтсчета, Периодичность, ц);
		
		соотПрогноз.Вставить(текПериод, результатРасчета[ц]);
		
	КонецЦикла;
	
	Для каждого цСтрока Из пТаблицаПрогноза Цикл
		
		цСтрока.Прогноз = соотПрогноз[цСтрока.Период];
		
	КонецЦикла;
	
КонецПроцедуры

// Возвращает параметры модели для сохранения
//
// Возвращаемое значение:
//  Структура - параметры модели
//
Функция ПолучитьПараметры() Экспорт
	
	структ = Новый Структура;
	структ.Вставить( "КоличествоПоследних", КоличествоПоследних );
	структ.Вставить( "Периодичность", Периодичность );
	структ.Вставить( "ТочкаОтсчета", мТочкаОтсчета );
	структ.Вставить( "ПоследнийФакт", мПоследнийФакт );
	структ.Вставить( "Значения", мЗначения );
	структ.Вставить( "ПериодСезонности", мПериодСезонности );
	
	Возврат структ;
	
КонецФункции

// Инициализирует модель по переданным параметрам
//
// Параметры:
//  пПараметрыМодели - Структура - параметры модели, полученные методом ПолучитьПараметры().
//
Процедура УстановитьПараметры( Знач пПараметрыМодели ) Экспорт
	
	КоличествоПоследних = пПараметрыМодели.КоличествоПоследних;
	Периодичность       = пПараметрыМодели.Периодичность;
	мТочкаОтсчета       = пПараметрыМодели.ТочкаОтсчета;
	мПоследнийФакт      = пПараметрыМодели.ПоследнийФакт;
	мЗначения           = пПараметрыМодели.Значения;
	мПериодСезонности   = пПараметрыМодели.ПериодСезонности;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ВыполнитьРасчет( Знач пРазмерПрогноза )
	
	результат = Новый Массив;
	
	количествоЗначений = мЗначения.Количество();
	
	Для ц = 0 По количествоЗначений + пРазмерПрогноза - 1 Цикл
		
		Если ц >= количествоЗначений Тогда
			
			результат.Добавить( результат[ц - мПериодСезонности] );
			
		Иначе
			
			результат.Добавить( мЗначения[ц] );
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат результат;
	
КонецФункции

#КонецОбласти

#КонецЕсли
