

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить( упИК.Событие_ОбновитьКартуМаршрута() , Объект.Владелец );
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьНаименование();
	
КонецПроцедуры

//////////////////////////////////////////////////////////////////////////////////////
//АЛГОРИТМЫ


&НаКлиенте
Процедура ТаблицаАлгоритмовПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	упРаботаСФормойКлиент.ТаблицаАлгоритмовПередНачаломДобавления( ЭтотОбъект , Элемент , Отказ );
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаАлгоритмовОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	упРаботаСФормойКлиент.ТаблицаАлгоритмовОбработкаВыбора( Объект.ТаблицаАлгоритмов , ВыбранноеЗначение );
	
КонецПроцедуры


&НаКлиенте
Процедура ИмяПриИзменении(Элемент)
	
	УстановитьНаименование();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьНаименование()
	
	Если Объект.Наименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( старИмя )
		ИЛИ ПустаяСтрока( Объект.Наименование ) Тогда
		
		Объект.Наименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( Объект.Имя );
		
	КонецЕсли;
	
	старИмя = Объект.Имя;
	
КонецПроцедуры	//УстановитьНаименование


&НаКлиенте
Процедура ТаблицаАлгоритмовАлгоритмНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	упРаботаСФормойКлиент.ВыбратьАлгоритм( ЭтотОбъект , Элемент );
	
КонецПроцедуры

