#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт

	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Пользователь)
	|	ИЛИ ЗначениеРазрешено(ГруппаПользователей)";
	
	Ограничение.ТекстДляВнешнихПользователей =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Пользователь)";

КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

Функция ПользовательЗадачи( пЗадача ) Экспорт
	
	Возврат глРеквизит( пЗадача , "Пользователь" );
	
КонецФункции

Функция ГруппаПользователейЗадачи( пЗадача ) Экспорт
	
	Возврат глРеквизит( пЗадача , "ГруппаПользователей" );
	
КонецФункции




// конструктор задачи
//
// Параметры:
//		пСтруктураПараметров - структура, состоящая из:
//
//			ВерсияБизнесПроцесса - СправочникСсылка.упВерсии
//			Сессия				 - СправочникСсылка.упСессии
//			Состояние 			 - СправочникСсылка.упСостояния
//			РеквизитыСессии		 - таблица значений, которая хранит текущие значения реквизитов сессии бизнес-процесса
//			ТабличныеЧастиСессии - таблица значений, которая хранит текущие значения реквизитов таб частей сессии
//			ВедущаяЗадача        - ЗадачаСсылка.упЗадача - ссылка на задачу ведущей сессии.
Функция СоздатьНовуюЗадачу( пДвижок ) Экспорт
	
	НоваяЗадача = Задачи.упЗадача.СоздатьЗадачу();
	НоваяЗадача.СоздатьЗадачу( пДвижок );
	
	Возврат НоваяЗадача.Ссылка;
	
КонецФункции // СоздатьНовуюЗадачу()



Функция СтруктураЭтоСсылкаИлиОбъект( пЗадача )
	
	структ = Новый Структура;
	
	Если упУправлениеЗадачамиКлиентСервер.ЭтоЗадачаСсылка( пЗадача ) Тогда
		
		структ.Вставить( "Ссылка", Истина );
		структ.Вставить( "Объект", Ложь );
		
	Иначе
		
		структ.Вставить( "Ссылка", Ложь );
		структ.Вставить( "Объект", упУправлениеЗадачамиКлиентСервер.ЭтоЗадачаОбъект( пЗадача ) );
		
	КонецЕсли;
	
	Возврат структ;
	
КонецФункции



// Процедура - запись реквизита "Табличная часть" в табличную часть "Табличные части сессии".
//
Процедура ЗаписатьТабличнуюЧастьРеквизита( пОбъект , пРеквизитТабличнаяЧасть , пТаблицаЗначений ) Экспорт
	
	Если Не ТипЗнч( пТаблицаЗначений ) = Тип( "ТаблицаЗначений" ) Тогда
		
		шаблонТекста = упИК.т__ПопыткаЗаписиВТаблРеквизитОшибочногоЗначения();
		текстСообщения = стрЗаполнить( шаблонТекста, пРеквизитТабличнаяЧасть, пТаблицаЗначений );
		
		ВызватьИсключение текстСообщения;
		
	КонецЕсли;
	
	// удалим старые значения таблицы
	
	упКоллекции.УдалитьСтрокиИзТЗПоОтбору( пОбъект.ТабличныеЧастиСессии , Новый Структура( "СсылкаНаТЧ" , пРеквизитТабличнаяЧасть ) );
	
	тзКолонок = Справочники.упРеквизиты.ПолучитьТаблицуКолонокРеквизита( пРеквизитТабличнаяЧасть );
	
	сущКолонки = пТаблицаЗначений.Колонки;
	
	НомерСтроки = 0;
	
	Для Каждого цСтрока Из пТаблицаЗначений Цикл
		
		НомерСтроки = НомерСтроки + 1;
		
		Для Каждого цКолонка Из тзКолонок Цикл
			
			Если сущКолонки.Найти( цКолонка.Имя ) = Неопределено Тогда
				Продолжить;
			КонецЕсли;
			
			НоваяСтрока = пОбъект.ТабличныеЧастиСессии.Добавить();
			
			НоваяСтрока.СсылкаНаТЧ             = пРеквизитТабличнаяЧасть;
			НоваяСтрока.НомерСтрокиВТабличнойЧасти = НомерСтроки;
			
			НоваяСтрока.СсылкаНаРеквизит = цКолонка.СсылкаНаРеквизит;
			
			НоваяСтрока.Значение = цСтрока[цКолонка.Имя];
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры


Процедура УстановитьВыполнениеУсловияРеквизитуБП( пОбъект , пРеквизит = Неопределено , пВыполнениеУсловия = Неопределено , УчитыватьПредыдущееЗначение = Ложь , пУсловие = Неопределено ) Экспорт
	
	Если Не ЗначениеЗаполнено( пВыполнениеУсловия ) Тогда
		
		пВыполнениеУсловия = Перечисления.упВидыВыполненияУсловий.УсловияНеНазначены;
		
	КонецЕсли;
	
	Если пРеквизит = Неопределено Тогда
		
		КоллекцияДляОбхода = пОбъект.ДанныеРеквизитов;
		
	Иначе
		
		КоллекцияДляОбхода = пОбъект.ДанныеРеквизитов.НайтиСтроки( Новый Структура( "СсылкаНаРеквизит,СсылкаНаТЧ" , пРеквизит, Справочники.упРеквизиты.ПустаяСсылка() ) );
		
	КонецЕсли;
	
	Для Каждого цСтрока Из КоллекцияДляОбхода Цикл
		
		// определение необходимого вида выполнения условия
		
		Если УчитыватьПредыдущееЗначение Тогда
			
			лВыполнениеУсловия = Перечисления.упВидыВыполненияУсловий.ПолучитьПриоритетноеЗначение( цСтрока.ВыполнениеУсловия , пВыполнениеУсловия );
			
		Иначе
			
			лВыполнениеУсловия = пВыполнениеУсловия;
			
		КонецЕсли;
		
		глПрисвоить( цСтрока.ВыполнениеУсловия , лВыполнениеУсловия );
		
		// заполнение описания нарушения условия
		
		лОписаниеУсловия = "";
		
		Если Перечисления.упВидыВыполненияУсловий.Выполнено( цСтрока.ВыполнениеУсловия ) Тогда
			
			// не добавляем информацию об условиях
			
		ИначеЕсли ЗначениеЗаполнено( пУсловие ) Тогда
			
			структОписания = глСтруктураРеквизитов_Кэш( пУсловие, "Описание, " + упЛокализацияКлиентСервер.ПРЕФИКС() + "Описание" );
			
			описаниеУсловия = упЛокализацияКлиентСервер.Представление( структОписания, "Описание" );
			
			лОписаниеУсловия = цСтрока.ОписаниеУсловия + описаниеУсловия + ";" + Символы.ПС;
			
		ИначеЕсли ЗначениеЗаполнено( пУсловие )
			И лВыполнениеУсловия = Перечисления.упВидыВыполненияУсловий.ОшибкаПроверкиУсловия Тогда
			
			структОписания = глСтруктураРеквизитов_Кэш( пУсловие, "Наименование, " + упЛокализацияКлиентСервер.ПРЕФИКС() + "Наименование" );
			
			описаниеУсловия = упЛокализацияКлиентСервер.Представление( структОписания, "Наименование" );
			
			шаблонТекста = упИК.т__ПриПроверкеУсловияВозниклиОшибки();
			текстОшибки = стрЗаполнить( шаблонТекста, описаниеУсловия );
			
			лОписаниеУсловия = цСтрока.ОписаниеУсловия + текстОшибки + Символы.ПС;
			
		КонецЕсли;
		
		цСтрока.ОписаниеУсловия = лОписаниеУсловия;
		
	КонецЦикла;
	
КонецПроцедуры


//////////////////////////////////////////////////////////////////////////////////////
//РАБОТА СО СТРУКТУРОЙ ЗАДАЧИ

Функция МассивРеквизитовЗадачиДоступныхДляИзменения() Экспорт
	
	массивРеквизитов = Новый Массив;
	
	массивРеквизитов.Добавить( "Адресация" );
	массивРеквизитов.Добавить( "Наименование" );
	массивРеквизитов.Добавить( "ДатаНачала" );
	массивРеквизитов.Добавить( "ДатаЗавершения" );
	массивРеквизитов.Добавить( "Важность" );
	массивРеквизитов.Добавить( "Стоимость" );
	массивРеквизитов.Добавить( "Описание" );
	массивРеквизитов.Добавить( "СледующееСостояние" );
	
	Возврат массивРеквизитов;
	
КонецФункции

// Получает структуру задачи с ссылкой на задачу, реквизитами документа Задача, реквизитами и таблицами процесса.
//
// Параметры
//  пЗадача  - Задача.упЗадача - задача, для которой получается структура
//  пПолучатьРеквизит_Задача  - Булево - добавлять ли в структуру ссылку на задачу
//  пПолучатьРеквизитыЗадачи  - Булево - добавлять ли в структуру реквизиты задачи для возможности их изменения.
//
// Возвращаемое значение:
//   Структура
//
Функция ПолучитьСтруктуруЗадачи( пЗадача , пПолучатьРеквизит_Задача = Ложь , пПолучатьРеквизитыЗадачи = Истина ) Экспорт
		
	ПроверитьКорректностьЗадачи( пЗадача );
	
	этаЗадача = СтруктураЭтоСсылкаИлиОбъект( пЗадача );
	
	// пробуем получить сохраненную структуру с объекта
	Если этаЗадача.Объект Тогда
		
		СтруктураЗадачи = Неопределено;
		
		имяСтруктурыВДопСвойствах = ИмяСтруктурыВДопСвойствах( пПолучатьРеквизит_Задача , пПолучатьРеквизитыЗадачи );
		
		Если пЗадача.ДополнительныеСвойства.Свойство( имяСтруктурыВДопСвойствах, СтруктураЗадачи ) Тогда
			
			Возврат СтруктураЗадачи;
			
		КонецЕсли;
		
	КонецЕсли;
	
	СтруктураЗадачиИзКеша = упКэшНаВызов.СоздатьСтруктуруРеквизитовВерсии( глРеквизит_Кэш( пЗадача , "ИсточникРеквизитов" ) , пПолучатьРеквизит_Задача , пПолучатьРеквизитыЗадачи );
	
	СтруктураЗадачи = упКоллекции.КопияСтруктуры( СтруктураЗадачиИзКеша );
	
	Если пПолучатьРеквизит_Задача Тогда
		
		ДобавитьВСтруктуруСсылкуНаЗадачу( СтруктураЗадачи, пЗадача );
		
	КонецЕсли;
	
	Если пПолучатьРеквизитыЗадачи Тогда
		
		ДобавитьВСтруктуруРеквизитыЗадачи( СтруктураЗадачи, пЗадача );
		
	КонецЕсли;
	
	СтруктРеквБП = упСтруктурыПроцессов.СтруктураРеквизитовБП( СтруктураЗадачи );
	
	Если этаЗадача.Объект Тогда
		
		тзРеквизитов = упРеквизитыКлиентСервер.ПолучитьТаблицуРеквизитов( пЗадача , пЗадача.ДанныеРеквизитов.Выгрузить(), пЗадача.РеквизитыСессии.Выгрузить() , пЗадача.ТабличныеЧастиСессии.Выгрузить() );
		
	ИначеЕсли этаЗадача.Ссылка Тогда
		
		тзРеквизитов = упРеквизитыКлиентСервер.ПолучитьТаблицуРеквизитов( пЗадача );
		
	КонецЕсли;
	
	Для Каждого цСтрока Из тзРеквизитов Цикл
		
		Если Не цСтрока.ЭтоРеквизитТЧ Тогда
			
			ЗаполнитьЗначенияСвойств( СтруктРеквБП[цСтрока.Имя] , цСтрока );
			
			Если цСтрока.ЭтоТЧ
				И СтруктРеквБП[цСтрока.Имя].Свойство( "Колонки" ) Тогда
				
				СтруктКолонки = СтруктРеквБП[цСтрока.Имя].Колонки;
				
				Для Каждого цКолонка Из тзРеквизитов.НайтиСтроки( Новый Структура( "СсылкаНаТЧ" , цСтрока[упИК.СсылкаНаРеквизит()] ) ) Цикл
					
					ЗаполнитьЗначенияСвойств( СтруктКолонки[ упКоллекцииКлиентСервер.ПолучитьПоследнююЧастьСтроки( цКолонка.Имя , "." ) ] , цКолонка );
					
				КонецЦикла;
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЦикла;
		
	Возврат СтруктураЗадачи;
	
КонецФункции

Функция ИмяСтруктурыВДопСвойствах( пПолучатьРеквизит_Задача , пПолучатьРеквизитыЗадачи )
	
	шаблонСтроки = "СтруктураЗадачи_%1_%2";
	
	Возврат стрЗаполнить( шаблонСтроки, Формат( пПолучатьРеквизит_Задача, "БЛ=0; БИ=1" ),  Формат( пПолучатьРеквизитыЗадачи, "БЛ=0; БИ=1" ) );
	
КонецФункции


Процедура ДобавитьВСтруктуруСсылкуНаЗадачу( пСтруктураЗадачи, пЗадача )
	
	пСтруктураЗадачи.Вставить( упИК.РеквизитЗадача() , пЗадача );
	
КонецПроцедуры	//ДобавитьВСтруктуруСсылкуНаЗадачу

Процедура ДобавитьВСтруктуруРеквизитыЗадачи( пСтруктураЗадачи, пЗадача )
	
	РеквизитыЗадачи = пСтруктураЗадачи[упИК.РеквизитыЗадачи()];
	
	структЗадачи = глСтруктураРеквизитов( пЗадача , стрСобрать( МассивРеквизитовЗадачиДоступныхДляИзменения() , "," ) );
	
	Для Каждого цЭлемент Из МассивРеквизитовЗадачиДоступныхДляИзменения() Цикл
		
		РеквизитыЗадачи[цЭлемент].Вставить( "Значение" , структЗадачи[цЭлемент] );
		
	КонецЦикла;
	
КонецПроцедуры	//ДобавитьВСтруктуруРеквизитыЗадачи



// Записывает в задачу значения из структуры
// Может быть записано только в не завершенную задачу
// Новые реквизиты в задачу НЕ добавляются, если в структуре нет значений для какого то реквизита, то оно игнорируется.
//
// Параметры
//  пСтруктура  – Структура – Структура задачи
//  пЗадача  – ЗадачаСсылка.упЗадача – Задача, в которую будут переданы реквизиты, если Неопределено, то задача будет
//  взята из структуры.
//
Процедура ЗаписатьСтруктуруВЗадачу( пСтруктура , пЗадача ) Экспорт
		
	ПроверитьКорректностьСтруктурыЗадачи( пСтруктура );
	ПроверитьКорректностьЗадачи( пЗадача );
	
	этоЗадачаСсылка = упУправлениеЗадачамиКлиентСервер.ЭтоЗадачаСсылка( пЗадача );
	
	Если этоЗадачаСсылка Тогда
		
		обЗадача = пЗадача.ПолучитьОбъект();
		
	Иначе
		
		обЗадача = пЗадача;
		
	КонецЕсли;
	
	// заполнение реквизитов задачи
	
	Если пСтруктура.Свойство( упИК.РеквизитыЗадачи() ) Тогда
		
		СтруктураРеквизитовЗадачи = пСтруктура[ упИК.РеквизитыЗадачи() ];
		
		Для Каждого цРеквизит Из МассивРеквизитовЗадачиДоступныхДляИзменения() Цикл
			
			ЗаполнитьРеквизитЗадачиИзСтруктуры( обЗадача , СтруктураРеквизитовЗадачи , цРеквизит );
			
		КонецЦикла;
		
		Если СтруктураРеквизитовЗадачи.Свойство( упИК.Автопереадресация() )
			И СтруктураРеквизитовЗадачи[упИК.Автопереадресация()] Тогда
			
			обЗадача.ПрименитьАвтопереадресацию();
			
		КонецЕсли;
		
	КонецЕсли;
	
	// заполнение реквизитов БП
	
	Для Каждого цЭлемент Из упСтруктурыПроцессов.СтруктураРеквизитовБП( пСтруктура ) Цикл
		
		ПроверитьКорректностьСтруктурыРеквизита( цЭлемент );
		
		цСтруктураРеквизита = цЭлемент.Значение;
		
		УстановитьЗначениеРеквизитаБП( обЗадача , цСтруктураРеквизита );
		УстановитьВидимостьДоступностьРеквизита( обЗадача , цСтруктураРеквизита );
		
	КонецЦикла;
	
	Если этоЗадачаСсылка Тогда
		
		обЗадача.Записать();
		
	Иначе
		
		// запишем в допсвойства структуру
		
		// пробуем получить сохраненную структуру с объекта
		имяСтруктурыВДопСвойствах = ИмяСтруктурыВДопСвойствах( пСтруктура.Свойство( упИК.РеквизитЗадача() ), пСтруктура.Свойство( упИК.РеквизитыЗадачи() ) );
		
		пЗадача.ДополнительныеСвойства.Вставить( имяСтруктурыВДопСвойствах, пСтруктура );
		
	КонецЕсли;
		
КонецПроцедуры // ЗаписатьСтруктуруВЗадачу()

Процедура ПроверитьКорректностьСтруктурыЗадачи( пСтруктура )
	
	Если Не ТипЗнч(пСтруктура) = Тип("Структура") Тогда
		
		шаблонТекста = упИК.т__ОшибкаЗаписиВСтруктуруЗадачи_НеверныйТип();
		ВызватьИсключение стрЗаполнить( шаблонТекста, ТипЗнч(пСтруктура) );
		
	КонецЕсли;
	
КонецПроцедуры	//ПроверитьКорректностьСтруктурыЗадачи

Процедура ПроверитьКорректностьЗадачи( пЗадача ) Экспорт
	
	этаЗадача = СтруктураЭтоСсылкаИлиОбъект( пЗадача );
	
	Если Не ( этаЗадача.Ссылка
		ИЛИ этаЗадача.Объект ) Тогда
		
		шаблонТекста = упИК.т__ПараметрЗадачаИмеетНекорректныйТип();
		текстСообщения = стрЗаполнить( шаблонТекста, ТипЗнч( пЗадача ) );
		
		ВызватьИсключение текстСообщения;
		
	КонецЕсли;
	
	Если этаЗадача.Ссылка
		И Не ЗначениеЗаполнено( пЗадача ) Тогда
		
		ВызватьИсключение упИК.т__ПараметрЗадачаНеЗаполнен();
		
	КонецЕсли;
	
КонецПроцедуры	//ПроверитьКорректностьЗадачи

Процедура ПроверитьКорректностьСтруктурыРеквизита( пЭлемент )
	
	Если Не ТипЗнч( пЭлемент.Значение ) = Тип("Структура") Тогда
		
		шаблонТекста = упИК.т__ОшибкаЗаписиВСтруктуруРеквизитов_НеверныйТип();
		текстСообщения = стрЗаполнить( шаблонТекста, пЭлемент.Ключ, пЭлемент.Значение );
		
		ВызватьИсключение текстСообщения;
		
	КонецЕсли;
	
КонецПроцедуры	//ПроверитьКорректностьСтруктурыЗадачи



Процедура УстановитьЗначениеРеквизитаБП( пОбъект , пСтруктураРеквизита ) Экспорт
	
	Если Не ТипЗнч( пСтруктураРеквизита ) = Тип( "Структура" ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	реквизит = пСтруктураРеквизита[упИК.СсылкаНаРеквизит()];
	
	Если Справочники.упРеквизиты.ЭтоТЧ( реквизит ) Тогда
		
		ЗаписатьТабличнуюЧастьРеквизита( пОбъект , реквизит , пСтруктураРеквизита.Значение );
		
	Иначе
		
		Для Каждого цСтрока Из пОбъект.РеквизитыСессии.НайтиСтроки( Новый Структура( "СсылкаНаРеквизит" , реквизит ) ) Цикл
			
			лЗначение = упКоллекции.ПривестиЗначениеКТипуРеквизиту( пСтруктураРеквизита.Значение , реквизит );
			
			глПрисвоить( цСтрока.Значение , лЗначение );
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура УстановитьВидимостьДоступностьРеквизита( пОбъект , пСтруктураРеквизита , пТЧ = Неопределено )
	
	Если пТЧ = Неопределено Тогда
		
		пТЧ = Справочники.упРеквизиты.ПустаяСсылка();
		
	КонецЕсли;
	
	Видимость = Неопределено;
	пСтруктураРеквизита.Свойство( "Видимость" , Видимость );
	
	Доступность = Неопределено;
	пСтруктураРеквизита.Свойство( "Доступность" , Доступность );
	
	УстановитьВДанныеРеквизитовВидимостьИДоступность( пОбъект.ДанныеРеквизитов , пСтруктураРеквизита[упИК.СсылкаНаРеквизит()] , пТЧ , Видимость , Доступность );
	
	Если пСтруктураРеквизита.Свойство( "Колонки" ) Тогда
		
		Для каждого цКолонка Из пСтруктураРеквизита.Колонки Цикл
			
			УстановитьВидимостьДоступностьРеквизита( пОбъект , цКолонка.Значение , пСтруктураРеквизита[упИК.СсылкаНаРеквизит()] );
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры	//УстановитьВидимостьДоступностьРеквизита

Процедура УстановитьВДанныеРеквизитовВидимостьИДоступность( пДанныеРеквизитов , пРеквизит , пТЧ , пВидимость , пДоступность )
	
	Для Каждого цСтрока Из пДанныеРеквизитов.НайтиСтроки( Новый Структура( "СсылкаНаРеквизит,СсылкаНаТЧ" , пРеквизит , пТЧ ) ) Цикл
		
		Если Не пВидимость = Неопределено Тогда
			
			глПрисвоить( цСтрока.Видимость , пВидимость );
			
		КонецЕсли;
		
		Если Не пДоступность = Неопределено Тогда
			
			глПрисвоить( цСтрока.Доступность , пДоступность );
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры	//УстановитьВДанныеРеквизитовВидимостьИДоступность




// Проверяет на существование в структуре необходимый реквизит и необходимость его присвоения реквизиту задачи.
//
// Параметры
//  пЗадача        – ЗадачаОбъект.упЗадача – Задача, в которую будет присвоено значение реквизита в случае успеха
//  пСтруктура     – Структура               – Структура, из которой будет получено значение реквизита задачи
//  пИмяРеквизита  - Строка                  - ключ структуры и имя реквизита задачи (они должны обязательно совпадать).
//
Процедура ЗаполнитьРеквизитЗадачиИзСтруктуры( пЗадача , пСтруктура , пИмяРеквизита )
	
	СтруктураЗначения = Неопределено;
	
	Если пСтруктура.Свойство( пИмяРеквизита , СтруктураЗначения ) Тогда
		
		лЗначение = Неопределено;
		
		Если ТипЗнч(СтруктураЗначения) = Тип( "Структура" )
			И СтруктураЗначения.Свойство( "Значение" , лЗначение ) Тогда
			
			глПрисвоить( пЗадача[пИмяРеквизита] , лЗначение );
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры // ЗаполнитьРеквизитЗадачиИзСтруктуры()


Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	лКлюч = Неопределено;
	
	Параметры.Свойство( "Ключ", лКлюч );
	
	Если Не ЗначениеЗаполнено( лКлюч ) Тогда
		Возврат;
	КонецЕсли;
	
	ИмяФормыЗадачи = глРеквизит( лКлюч , "Состояние.ИмяФормыЗадачи" );
	
	Если ЗначениеЗаполнено( ИмяФормыЗадачи ) Тогда
		
		ВыбраннаяФорма = ИмяФормыЗадачи;
		СтандартнаяОбработка = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецЕсли