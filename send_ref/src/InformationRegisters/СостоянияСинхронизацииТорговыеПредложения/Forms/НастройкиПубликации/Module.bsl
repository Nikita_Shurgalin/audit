
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Не ТорговыеПредложения.ПравоНастройкиТорговыхПредложений(Истина) Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
		
	Если Не Параметры.ТорговоеПредложение.Пустая() Тогда
		
		СвойстваПредложения = Новый Структура("Организация, Валюта");
		ТорговыеПредложенияПереопределяемый.ПолучитьСвойстваТорговогоПредложения(Параметры.ТорговоеПредложение, СвойстваПредложения);
		
		ТекущаяЗапись = ТорговыеПредложенияСлужебный.ЗаписьСостояниеСинхронизации(
			СвойстваПредложения.Организация, 
			Параметры.ТорговоеПредложение);
		
		Если ТекущаяЗапись = Неопределено Тогда
			Отказ = Истина;
		Иначе
			ЗначениеВРеквизитФормы(ТекущаяЗапись, "Запись");
		КонецЕсли;
		
	КонецЕсли;
	
	УстановитьВидимостьДоступность(Запись, Элементы);
	
	УстановитьЗаголовокСсылкиОткрытияАдресов();
	
	УстановитьДоступностьВариантаПубликацииОстатков();
	
	НастройкиПодсистемы = ТорговыеПредложения.НастройкиПодсистемы();
	Элементы.ПубликоватьОстатки.Видимость = НастройкиПодсистемы.ИспользоватьПубликациюОстатков;
	Элементы.ВариантПубликацииОстатков.СписокВыбора.ЗагрузитьЗначения(НастройкиПодсистемы.ВариантыПубликацииОстатков);
	Элементы.ВариантПубликацииСкидки.СписокВыбора.ЗагрузитьЗначения(НастройкиПодсистемы.ВариантыПубликацииСкидок);
	
	ЗаполнитьВариантПубликацииОстатков();
	
	ЗаполнитьВариантПубликацииСкидки();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	УстановитьЗаголовокСсылкиОткрытияАдресов();
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если Запись.УведомлятьОЗаказах Тогда
		Если ПустаяСтрока(Запись.АдресЭлектроннойПочты) Тогда
			ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Введите адрес электронной почты'"), , "АдресЭлектроннойПочты", , Отказ);
		ИначеЕсли Не ОбщегоНазначенияКлиентСервер.АдресЭлектроннойПочтыСоответствуетТребованиям(Запись.АдресЭлектроннойПочты, Истина) Тогда
			ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Адрес электронной почты введен неверно'"), , "АдресЭлектроннойПочты", , Отказ);
		КонецЕсли;
	КонецЕсли;
		
	ЗначенияЗаписи = Новый Структура;
	ЗначенияЗаписи.Вставить("ТорговоеПредложение"                  , Запись.ТорговоеПредложение);
	ЗначенияЗаписи.Вставить("Организация"                          , Запись.Организация);
	ЗначенияЗаписи.Вставить("АдресЭлектроннойПочты"                , Запись.АдресЭлектроннойПочты);
	ЗначенияЗаписи.Вставить("УведомлятьОЗаказах"                   , Запись.УведомлятьОЗаказах);
	ЗначенияЗаписи.Вставить("ПубликоватьОстатки"                   , Запись.ПубликоватьОстатки);
	ЗначенияЗаписи.Вставить("ПубликоватьСрокиПоставки"             , Запись.ПубликоватьСрокиПоставки);
	ЗначенияЗаписи.Вставить("ПубликоватьЦены"                      , Запись.ПубликоватьЦены);
	ЗначенияЗаписи.Вставить("ДополнительноеОписание"               , Запись.ДополнительноеОписание);
	ЗначенияЗаписи.Вставить("ВариантПубликацииОстатков"            , Запись.ВариантПубликацииОстатков);
	Оповестить("ТорговыеПредложения_ЗаписьДополнительныеНастройки" , ЗначенияЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ОповеститьОбИзменении(Запись.ТорговоеПредложение);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	Если Запись.ПубликоватьРегионыДоступностиТоваров
		И Не ЗначениеЗаполнено(Запись.РегионыДоставки)
		И Не ЗначениеЗаполнено(Запись.РегионыСамовывоза) Тогда
		
		Отказ = Истина;
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru = 'Не заполнены регионы доступности товаров'"));	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	ТекущийОбъект.Состояние = ПредопределенноеЗначение("Перечисление.СостоянияСинхронизацииТорговыеПредложения.ТребуетсяСинхронизация");
	
	Если Запись.ПубликоватьОстатки Тогда
		ТекущийОбъект.ВариантПубликацииОстатков = ВариантПубликацииОстатков;
	Иначе
		ТекущийОбъект.ВариантПубликацииОстатков = ПредопределенноеЗначение("Перечисление.ВариантыПубликацииОстатковТорговыеПредложения.ПустаяСсылка");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПубликоватьОстаткиПриИзменении(Элемент)
	
	УстановитьДоступностьВариантаПубликацииОстатков();
	
КонецПроцедуры

&НаКлиенте
Процедура УведомлятьОЗаказахПриИзменении(Элемент)
	
	УстановитьВидимостьДоступность(Запись, Элементы)
	
КонецПроцедуры

&НаКлиенте
Процедура СоставРегионовНажатие(Элемент)
	
	ПараметрыФормы = БизнесСетьСлужебныйКлиент.ОписаниеПараметровФормыНастройкиРегионов();
	
	ПараметрыФормы.Организация         = Запись.Организация;
	ПараметрыФормы.ТорговоеПредложение = Запись.ТорговоеПредложение;
	
	Оповещение = Новый ОписаниеОповещения("ПослеВыбораАдресов", ЭтотОбъект);
		
	БизнесСетьСлужебныйКлиент.ОткрытьФормуНастройкиРегионов(ПараметрыФормы, ЭтотОбъект, Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПубликоватьРегионыПриИзменении(Элемент)
	
	Элементы.СоставРегионов.Доступность = Запись.ПубликоватьРегионыДоступностиТоваров;
	
КонецПроцедуры

&НаКлиенте
Процедура ПубликоватьЦеныПриИзменении(Элемент)
	УстановитьВидимостьДоступность(Запись, Элементы)
КонецПроцедуры

&НаКлиенте
Процедура ВариантыПубликацииСкидкиНажатие(Элемент)
	
	ПараметрыОтбора = Новый Структура;
	ПараметрыОтбора.Вставить("ТорговоеПредложение" ,Запись.ТорговоеПредложение);
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Отбор", ПараметрыОтбора);
	
	
	Оповещение = Новый ОписаниеОповещения("ПослеВыбораВариантаПубликацииСкидок", ЭтотОбъект);
	Если Запись.ВариантПубликацииСкидки = 
		ПредопределенноеЗначение("Перечисление.ВариантыПубликацииСкидкиЗаРазовыйОбъемПродаж.ВидыЦен") Тогда
			Форма = "РегистрСведений.СкидкиТорговыхПредложенийВидыЦен.Форма.ФормаНабораЗаписей";
	ИначеЕсли Запись.ВариантПубликацииСкидки =
		ПредопределенноеЗначение("Перечисление.ВариантыПубликацииСкидкиЗаРазовыйОбъемПродаж.ВидыСкидок") Тогда
			Форма = "РегистрСведений.СкидкиТорговыхПредложений.Форма.ФормаНабораЗаписей";
	КонецЕсли;
	
	ОткрытьФорму(Форма, ПараметрыФормы, , , , , Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПубликоватьСкидкиЗаРазовыйОбъемПродажПриИзменении(Элемент)
	УстановитьВидимостьДоступность(Запись, Элементы);
КонецПроцедуры

&НаКлиенте
Процедура ВариантПубликацииСкидкиПриИзменении(Элемент)
	
	УстановитьЗаголовокСсылкиОткрытияВариантовПубликацииСкидок();
	УстановитьВидимостьДоступность(Запись, Элементы);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьВидимостьДоступность(Запись, Элементы)
	
	Элементы.АдресЭлектроннойПочты.Доступность               = Запись.УведомлятьОЗаказах;
	Элементы.АдресЭлектроннойПочты.АвтоОтметкаНезаполненного = Элементы.АдресЭлектроннойПочты.Доступность;
	Элементы.СоставРегионов.Доступность                      = Запись.ПубликоватьРегионыДоступностиТоваров;
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "ПубликоватьСкидки", "Доступность", Запись.ПубликоватьЦены);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "ПубликоватьЦенуДоСкидки",               "Доступность", Запись.ПубликоватьЦены);
	
	ПубликоватьСкидки = Запись.ПубликоватьЦены И Запись.ПубликоватьСкидкиЗаРазовыйОбъемПродаж;
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, 
		"ВариантПубликацииСкидки", 
		"Доступность", 
		ПубликоватьСкидки);
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, 
		"ВариантыПубликацииСкидки", 
		"Доступность", 
		ПубликоватьСкидки И Не Запись.ВариантПубликацииСкидки.Пустая());
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВариантПубликацииОстатков()
	
	Если ЗначениеЗаполнено(Запись.ВариантПубликацииОстатков) Тогда
		ВариантПубликацииОстатков = Запись.ВариантПубликацииОстатков;
	Иначе
		Если ЗначениеЗаполнено(Элементы.ВариантПубликацииОстатков.СписокВыбора) Тогда
			ВариантПубликацииОстатков = Элементы.ВариантПубликацииОстатков.СписокВыбора[0].Значение;
		КонецЕсли;
	КонецЕсли;
	
	Если Элементы.ВариантПубликацииОстатков.СписокВыбора.Количество() = 1 Тогда
		Элементы.ВариантПубликацииОстатков.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВариантПубликацииСкидки()
	
	Если НЕ ЗначениеЗаполнено(Запись.ВариантПубликацииСкидки) 
		И ЗначениеЗаполнено(Элементы.ВариантПубликацииСкидки.СписокВыбора) Тогда
			Запись.ВариантПубликацииСкидки = Элементы.ВариантПубликацииСкидки.СписокВыбора[0].Значение;
	КонецЕсли;
	
	Если Элементы.ВариантПубликацииСкидки.СписокВыбора.Количество() = 0 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы, "ГруппаМатрицаЦен", "Доступность", Ложь);
	ИначеЕсли Элементы.ВариантПубликацииСкидки.СписокВыбора.Количество() = 1 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы, "ВариантПубликацииСкидки", "Видимость", Ложь);
	КонецЕсли;
	УстановитьЗаголовокСсылкиОткрытияВариантовПубликацииСкидок();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеВыбораАдресов(Результат, ДополнительныеПараметры) Экспорт

	УстановитьЗаголовокСсылкиОткрытияАдресов(Истина);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗаголовокСсылкиОткрытияАдресов(ПеречитатьДанные = Ложь)
	
	Если ПеречитатьДанные Тогда
		ТекущаяЗапись = ТорговыеПредложенияСлужебный.ЗаписьСостояниеСинхронизации(Запись.Организация, Запись.ТорговоеПредложение);
		ЗаполнитьЗначенияСвойств(Запись, ТекущаяЗапись, "РегионыДоставки, РегионыСамовывоза");
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Запись.РегионыДоставки)
		ИЛИ ЗначениеЗаполнено(Запись.РегионыСамовывоза) Тогда
		
		Элементы.СоставРегионов.Заголовок = НСтр("ru = 'Изменить'");
	Иначе
		Элементы.СоставРегионов.Заголовок = "<" + НСтр("ru = 'не указаны'") + ">";
	КонецЕсли;
		
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьВариантаПубликацииОстатков()
	
	Элементы.ВариантПубликацииОстатков.Доступность = Запись.ПубликоватьОстатки;
		
КонецПроцедуры

&НаКлиенте
Процедура ПослеВыбораВариантаПубликацииСкидок(Результат, ДополнительныеПараметры) Экспорт
	
	УстановитьЗаголовокСсылкиОткрытияВариантовПубликацииСкидок();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗаголовокСсылкиОткрытияВариантовПубликацииСкидок(ПеречитатьДанные = Ложь)
	
	ТекущаяЗапись = ТорговыеПредложенияСлужебный.ЗаписьСостояниеСинхронизации(Запись.Организация, Запись.ТорговоеПредложение);
	
	Если ПеречитатьДанные Тогда
		ЗаполнитьЗначенияСвойств(Запись, ТекущаяЗапись, "ВариантПубликацииСкидки");
	КонецЕсли;
	
	Элементы.ВариантыПубликацииСкидки.Заголовок = "<" + НСтр("ru = 'не указаны'") + ">";
	Отбор = Новый Структура("ТорговоеПредложение", Запись.ТорговоеПредложение);
	
	Если Запись.ВариантПубликацииСкидки = Перечисления.ВариантыПубликацииСкидкиЗаРазовыйОбъемПродаж.ВидыЦен Тогда
		Выборка = РегистрыСведений.СкидкиТорговыхПредложенийВидыЦен.Выбрать(Отбор);
	ИначеЕсли Запись.ВариантПубликацииСкидки = Перечисления.ВариантыПубликацииСкидкиЗаРазовыйОбъемПродаж.ВидыСкидок Тогда
		Выборка = РегистрыСведений.СкидкиТорговыхПредложений.Выбрать(Отбор);
	Иначе
		Возврат;
	КонецЕсли;
	
	Если Выборка.Следующий() Тогда
		Элементы.ВариантыПубликацииСкидки.Заголовок = НСтр("ru = 'Настройки'");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
