
Функция ВыполнятьВФоне()
	
	Возврат Не ( упКэш.ИнформационнаяБазаФайловая()
		ИЛИ упКэш.РежимОтладки()
		ИЛИ ПревышенаОчередь() );
	
КонецФункции


// Запускает выполнение процедуры в фоновом задании.
//
// Параметры (название, тип, дифференцированное значение)
//
//  ИмяЭкспортнойПроцедуры - Строка - имя экспортной процедуры, 
//                           которую необходимо выполнить в фоне.
//  Параметры              - Структура - все необходимые параметры для 
//                           выполнения процедуры ИмяЭкспортнойПроцедуры.
//  Уникальный идентификатор задания - Строка - Ключ задания.
//                           Если ключ задан, то он должен быть уникальным среди ключей активных фоновых заданий,
//                           имеющих такое же имя метода, что и у данного фонового задания.
//  НаименованиеЗадания    - Строка - наименование фонового задания. 
//                           Если не задано, то будет равно ИмяЭкспортнойПроцедуры.
//
// Возвращаемое значение: 
//
Функция ВыполнитьВФоне( Знач пИмяЭкспортнойПроцедуры, Знач пПараметры, Знач пУникальныйИдентификаторЗадания = Неопределено ,Знач пНаименованиеЗадания = "" ) Экспорт
	
	Если Не ЗначениеЗаполнено(пНаименованиеЗадания) Тогда
		пНаименованиеЗадания = пИмяЭкспортнойПроцедуры;
	КонецЕсли;
	
	Если ВыполнятьВФоне() Тогда
		
		Возврат ФоновыеЗадания.Выполнить( пИмяЭкспортнойПроцедуры, пПараметры, пУникальныйИдентификаторЗадания , пНаименованиеЗадания ).УникальныйИдентификатор;
		
	Иначе
		
		упОбщегоНазначенияКлиентСервер.ВыполнитьПроцедуру( пИмяЭкспортнойПроцедуры, пПараметры );
		
		Возврат Неопределено;
		
	КонецЕсли;
	
КонецФункции //ВыполнитьВФоне

// Отменяет выполение фонового задания по переданному идентификатору.
// 
// Параметры:
//  ИдентификаторЗадания - УникальныйИдентификатор - идентификатор фонового задания. 
// 
Процедура ОтменитьВыполнениеЗадания(Знач ИдентификаторЗадания) Экспорт
	
	Если Не ВыполнятьВФоне() Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ИдентификаторЗадания) Тогда
		Возврат;
	КонецЕсли;
	
	Задание = ФоновыеЗадания.НайтиПоУникальномуИдентификатору(ИдентификаторЗадания);
	Если Задание = Неопределено
		ИЛИ Задание.Состояние <> СостояниеФоновогоЗадания.Активно Тогда
		
		Возврат;
	КонецЕсли;
	
	Попытка
		Задание.Отменить();
	Исключение
		
		глЛог( НСтр("ru = 'Длительные операции.Отмена выполнения фонового задания'" ) , Перечисления.упТипыОшибокДляЛога.ДвижокБП , Истина , ИнформацияОбОшибке() );
		
	КонецПопытки;
	
КонецПроцедуры

Функция ПревышенаОчередь()
	
	массивФоновыхЗаданий = ФоновыеЗадания.ПолучитьФоновыеЗадания( Новый Структура("Наименование, Состояние", упИК.ИмяЗадания__ВыполнениеДвиженияЗадачи(), СостояниеФоновогоЗадания.Активно));
	
	Возврат массивФоновыхЗаданий.Количество() > упИК.МаксимальноеКоличествоОдновременноВыполняемыхЗаданий();
	
КонецФункции



