///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020, ООО 1С-Софт
// Все права защищены. Эта программа и сопроводительные материалы предоставляются 
// в соответствии с условиями лицензии Attribution 4.0 International (CC BY 4.0)
// Текст лицензии доступен по ссылке:
// https://creativecommons.org/licenses/by/4.0/legalcode
///////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Подсистема "ИнтернетПоддержкаПользователей.ИнтеграцияСПлатежнымиСистемами".
// ОбщийМодуль.ИнтеграцияСПлатежнымиСистемамиКлиентПереопределяемый.
//
// Клиентские переопределяемые процедуры настройки интеграции с платежными системами:
//  - настройка связи между объектами БИП и прикладной конфигурации.
//
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Открывает форму настройки интеграции с платежными системами
// в момент настройки торговой точки.
//
// Параметры:
//  Владелец   - ОкноКлиентскогоПриложения - форма, из которой открываются настройки;
//  НастройкаИнтеграции - СправочникСсылка.НастройкиИнтеграцииСПлатежнымиСистемами - торговая точка
//                        для которой необходимо выполнить настройку;
//  ОписаниеОповещения - ОписаниеОповещения - оповещение, которое необходимо вызвать при
//                       завершении настройки интеграции;
//
//@skip-warning
Процедура ПриОткрытииФормыНастроекИнтеграции(Владелец, НастройкаИнтеграции, ОписаниеОповещения) Экспорт
	
	
КонецПроцедуры

#КонецОбласти
