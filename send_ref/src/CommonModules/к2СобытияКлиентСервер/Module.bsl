
#Область ПрограммныйИнтерфейс

// Устарела. См. к2ДокументыКлиентСервер.ОбработкаПолученияПолейПредставления
// Получает поля, необходимые для формирования представления документа.
// 
// Параметры:
//  Поля                  - Массив - имена полей, которые нужны для формирования представления объекта или ссылки.
//  СтандартнаяОбработка  - Булево - признак необходимости выполнения стандартной обработки.
//
Процедура ОбработкаПолученияПолейПредставленияДокумента(Поля, СтандартнаяОбработка) Экспорт
	
	к2ДокументыКлиентСервер.ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка);
	
КонецПроцедуры

// Устарела. См. к2ДокументыКлиентСервер.ОбработкаПолученияПредставления
// Формирует представление документа.
// 
// Параметры:
//  Синоним             - Строка - Представление документа
//  Данные              - Структура - содержит:
//                        * ДатаНачала - Дата - начало запланированного взаимодействия.
//  Представление        - Строка - формируемое представление.
//  СтандартнаяОбработка - Булево - признак необходимости стандартной обработки.
//
Процедура ОбработкаПолученияПредставленияДокумента(Синоним, Данные, Представление, СтандартнаяОбработка) Экспорт
	
	к2ДокументыКлиентСервер.ОбработкаПолученияПредставления(Синоним, Данные, Представление, СтандартнаяОбработка);
	 
КонецПроцедуры

#КонецОбласти