
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	значенияОтбора = Новый Массив;
	значенияОтбора.Добавить(ПараметрКоманды);
	значенияОтбора.Добавить(ПредопределенноеЗначение("Справочник.к2ХозяйствующиеСубъекты_Меркурий.ПустаяСсылка"));
	отбор = Новый Структура("ХС", значенияОтбора);
	ПараметрыФормы = Новый Структура("Отбор", отбор);
	ОткрытьФорму("РегистрСведений.к2ПравилаЗаполненияДокументов_Меркурий.ФормаСписка",
				 ПараметрыФормы,
				 ПараметрыВыполненияКоманды.Источник,
				 ПараметрыВыполненияКоманды.Уникальность,
				 ПараметрыВыполненияКоманды.Окно,
				 ПараметрыВыполненияКоманды.НавигационнаяСсылка);
				 
КонецПроцедуры

#КонецОбласти