
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Номенклатура = Неопределено;
	ВладелецХарактеристик = Неопределено;
	
	Если Параметры.Отбор.Свойство("Владелец") Тогда
		
		// Перенесем "стандартный" отбор по владельцу в свойство ПараметрВладелец
		Параметры.ПараметрВладелец = Параметры.Отбор.Владелец;
		Параметры.Отбор.Удалить("Владелец");
		
	КонецЕсли;
	
	Если Параметры.Свойство("ПараметрВладелец", ВладелецХарактеристик) И ЗначениеЗаполнено(ВладелецХарактеристик) Тогда
		
		к2ПодборТоваровСервер.УстановитьОтборПоВладельцуХарактеристик(ЭтотОбъект);
	
	КонецЕсли;
	
	Если Параметры.Свойство("Номенклатура", Номенклатура) И ЗначениеЗаполнено(Номенклатура) Тогда
		
		Если Справочники.Номенклатура.ПроверитьИспользованиеХарактеристикИПолучитьВладельцаДляВыбора(Номенклатура, ВладелецХарактеристик) Тогда
			
			Если ВладелецХарактеристик = Неопределено Тогда
				
				ТекстИсключения = НСтр("ru = 'Для данной номенклатуры характеристики не заданы.'");
				ВызватьИсключение ТекстИсключения;
				
			Иначе
				
				к2ПодборТоваровСервер.УстановитьОтборПоВладельцуХарактеристик(ЭтотОбъект);
				
			КонецЕсли;
			
		Иначе
			
			ТекстИсключения = НСтр("ru = 'Для данной номенклатуры отключено использование характеристик.'");
			ВызватьИсключение ТекстИсключения;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если ТипЗнч(ВладелецХарактеристик) = Тип("СправочникСсылка.ВидыНоменклатуры") Тогда
		
		ВидНоменклатуры = ВладелецХарактеристик;
		
	ИначеЕсли ТипЗнч(ВладелецХарактеристик) = Тип("СправочникСсылка.Номенклатура") Тогда
		
		ВидНоменклатуры = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВладелецХарактеристик, "ВидНоменклатуры");
		
	Иначе
		
		ВидНоменклатуры = Неопределено;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить();
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
КонецПроцедуры

&НаКлиенте
Процедура СписокНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

#КонецОбласти
