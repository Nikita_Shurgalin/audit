

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить( упИК.Событие_ОбновитьКартуМаршрута() , Объект.Владелец );
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Не ЗначениеЗаполнено( Объект.ПереходДляУсловия ) Тогда
		
		массивПереходов = упКэшНаВызов.ПолучитьМассивИсходящихПереходов( Объект.Ссылка );
		
		Если массивПереходов.Количество() > 0 Тогда
			
			Объект.ПереходДляУсловия = массивПереходов[0];
			Модифицированность = Истина;
			
		КонецЕсли;
		
	КонецЕсли;
	
	УстановитьНаименование();
	
КонецПроцедуры


&НаКлиенте
Процедура ИмяПриИзменении(Элемент)
	
	УстановитьНаименование();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьНаименование()
	
	Если Объект.Наименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( старИмя )
		ИЛИ ПустаяСтрока( Объект.Наименование ) Тогда
		
		Объект.Наименование = упКоллекцииКлиентСервер.ПолучитьНаименованиеПоИмени( Объект.Имя );
		
	КонецЕсли;
	
	старИмя = Объект.Имя;
	
КонецПроцедуры	//УстановитьНаименование

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Параметр = Объект.Владелец
		И ИмяСобытия = упИК.Событие_ОбновитьЭлементыВерсии() Тогда
		
		Прочитать();
		
	КонецЕсли;
	
КонецПроцедуры


