#Область СлужебныйПрограммныйИнтерфейс

Функция ДанныеЗаполненияЗаказаНаЭмиссиюКодовМаркировкиСУЗ(Организация) Экспорт
	
	ДанныеЗаполнения = Новый Структура;
	ДанныеЗаполнения.Вставить("Организация");
	ДанныеЗаполнения.Вставить("ВидПродукции", Перечисления.к2ВидыПродукцииИС.ПустаяСсылка());
	
	ВидыПродукцииУчета = к2ИнтеграцияИСМПКлиентСерверПовтИсп.УчитываемыеВидыМаркируемойПродукции();
	
	Если ВидыПродукцииУчета.Количество() = 0 Тогда
		Возврат ДанныеЗаполнения;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 3
	|	1                                                                    КАК Количество,
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗ.Организация                         КАК Организация,
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗ.ВидПродукции                        КАК ВидПродукции
	|ИЗ
	|	Документ.к2ЗаказНаЭмиссиюКодовМаркировкиСУЗ КАК ЗаказНаЭмиссиюКодовМаркировкиСУЗ
	|ГДЕ
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗ.Организация = &Организация Или &БезУчетаОрганизации
	|	И ЗаказНаЭмиссиюКодовМаркировкиСУЗ.Проведен
	|УПОРЯДОЧИТЬ ПО
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗ.Дата УБЫВ");
	
	Запрос.УстановитьПараметр("Организация",         Организация);
	Запрос.УстановитьПараметр("БезУчетаОрганизации", Не ЗначениеЗаполнено(Организация));
	
	РезультатЗапроса = Запрос.Выполнить();
	ДанныеПоследнихДокументов = РезультатЗапроса.Выгрузить();
	
	ДанныеЗаполнения.ВидПродукции = ВидПродукции(ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Организация",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	Возврат ДанныеЗаполнения;
	
КонецФункции

Функция СпособФормированияСерийногоНомераПоСтатистике(Номенклатура, Характеристика) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 3
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.СпособФормированияСерийногоНомера КАК СпособФормирования,
	|	1                                                                        КАК Количество
	|ИЗ
	|	Документ.к2ЗаказНаЭмиссиюКодовМаркировкиСУЗ.Товары КАК ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары
	|ГДЕ
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Номенклатура = &Номенклатура
	|	И ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Характеристика = &Характеристика
	|УПОРЯДОЧИТЬ ПО
	|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка.Дата УБЫВ");
	Запрос.УстановитьПараметр("Номенклатура",   Номенклатура);
	Запрос.УстановитьПараметр("Характеристика", Характеристика);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда
		Возврат Перечисления.к2СпособыФормированияСерийногоНомераСУЗ.Автоматически;
	КонецЕсли;
	
	ДанныеСтатистики = РезультатЗапроса.Выгрузить();
	ДанныеСтатистики.Свернуть("СпособФормирования", "Количество");
	ДанныеСтатистики.Сортировать("Количество Убыв");
	
	Возврат ДанныеСтатистики[0].СпособФормирования;
	
КонецФункции

Функция ДанныеЗаполненияМаркировкиТоваровИСМП(Организация) Экспорт
	
	ДанныеЗаполнения = Новый Структура;
	ДанныеЗаполнения.Вставить("Организация");
	ДанныеЗаполнения.Вставить("ВидПродукции", Перечисления.к2ВидыПродукцииИС.ПустаяСсылка());
	ДанныеЗаполнения.Вставить("Операция");
	
	ВидыПродукцииУчета = к2ИнтеграцияИСМПКлиентСерверПовтИсп.УчитываемыеВидыМаркируемойПродукции();
	
	Если ВидыПродукцииУчета.Количество() = 0 Тогда
		Возврат ДанныеЗаполнения;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 3
	|	1                                          КАК Количество,
	|	МаркировкаТоваровИСМП.Организация          КАК Организация,
	|	МаркировкаТоваровИСМП.Операция             КАК Операция,
	|	МаркировкаТоваровИСМП.ВидПродукции         КАК ВидПродукции
	|ИЗ
	|	Документ.к2МаркировкаТоваровИСМП КАК МаркировкаТоваровИСМП
	|ГДЕ
	|	МаркировкаТоваровИСМП.Организация = &Организация Или &БезУчетаОрганизации
	|	И МаркировкаТоваровИСМП.Проведен
	|УПОРЯДОЧИТЬ ПО
	|	МаркировкаТоваровИСМП.Дата УБЫВ");
	
	Запрос.УстановитьПараметр("Организация",         Организация);
	Запрос.УстановитьПараметр("БезУчетаОрганизации", Не ЗначениеЗаполнено(Организация));
	
	РезультатЗапроса = Запрос.Выполнить();
	ДанныеПоследнихДокументов = РезультатЗапроса.Выгрузить();
	
	ДанныеЗаполнения.ВидПродукции = ВидПродукции(ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Операция",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Организация",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	// Значения по-умолчанию
	Если Не ЗначениеЗаполнено(ДанныеЗаполнения.ВидПродукции) Тогда
		ДоступныеВидыПродукцииИС = к2ИнтеграцияИСКлиентСервер.ВидыПродукцииИСМП();
		Для Каждого ВидПродукцииУчета Из ВидыПродукцииУчета Цикл
			Если ДоступныеВидыПродукцииИС.Найти(ВидПродукцииУчета) <> Неопределено Тогда
				
				ДанныеЗаполнения.ВидПродукции = ВидПродукцииУчета;
				
				Если к2ИнтеграцияИСПовтИсп.ЭтоПродукцияИСМП(ДанныеЗаполнения.ВидПродукции) Тогда
					ДанныеЗаполнения.Операция = Перечисления.к2ВидыОперацийИСМП.ВводВОборотПроизводствоРФ;
				КонецЕсли;
				
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат ДанныеЗаполнения;
	
КонецФункции

Функция ДанныеЗаполненияСписанияКодовМаркировкиИСМП(Организация) Экспорт
	
	ДанныеЗаполнения = Новый Структура;
	ДанныеЗаполнения.Вставить("Организация");
	ДанныеЗаполнения.Вставить("ВидПродукции", Перечисления.к2ВидыПродукцииИС.ПустаяСсылка());
	
	ВидыПродукцииУчета = к2ИнтеграцияИСМПКлиентСерверПовтИсп.УчитываемыеВидыМаркируемойПродукции();
	
	Если ВидыПродукцииУчета.Количество() = 0 Тогда
		Возврат ДанныеЗаполнения;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 3
	|	1                                                               КАК Количество,
	|	СписаниеКодовМаркировкиИСМП.Организация                         КАК Организация,
	|	СписаниеКодовМаркировкиИСМП.ВидПродукции                        КАК ВидПродукции
	|ИЗ
	|	Документ.к2СписаниеКодовМаркировкиИСМП КАК СписаниеКодовМаркировкиИСМП
	|ГДЕ
	|	СписаниеКодовМаркировкиИСМП.Организация = &Организация Или &БезУчетаОрганизации
	|	И СписаниеКодовМаркировкиИСМП.Проведен
	|УПОРЯДОЧИТЬ ПО
	|	СписаниеКодовМаркировкиИСМП.Дата УБЫВ");
	
	Запрос.УстановитьПараметр("Организация",         Организация);
	Запрос.УстановитьПараметр("БезУчетаОрганизации", Не ЗначениеЗаполнено(Организация));
	
	РезультатЗапроса = Запрос.Выполнить();
	ДанныеПоследнихДокументов = РезультатЗапроса.Выгрузить();
	
	ДанныеЗаполнения.ВидПродукции = ВидПродукции(ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Организация",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	Возврат ДанныеЗаполнения;
	
КонецФункции

Функция ДанныеЗаполненияВыводаИзОборотаИСМП(Организация) Экспорт
	
	ДанныеЗаполнения = Новый Структура;
	ДанныеЗаполнения.Вставить("Организация");
	ДанныеЗаполнения.Вставить("ВидПродукции", Перечисления.к2ВидыПродукцииИС.ПустаяСсылка());
	ДанныеЗаполнения.Вставить("Операция");
	ДанныеЗаполнения.Вставить("ВидПервичногоДокумента");
	ДанныеЗаполнения.Вставить("НаименованиеПервичногоДокумента");
	
	ВидыПродукцииУчета = к2ИнтеграцияИСМПКлиентСерверПовтИсп.УчитываемыеВидыМаркируемойПродукции();
	
	Если ВидыПродукцииУчета.Количество() = 0 Тогда
		Возврат ДанныеЗаполнения;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 3
	|	1                                                  КАК Количество,
	|	ВыводИзОборотаИСМП.Организация                     КАК Организация,
	|	ВыводИзОборотаИСМП.ВидПродукции                    КАК ВидПродукции,
	|	ВыводИзОборотаИСМП.Операция                        КАК Операция,
	|	ВыводИзОборотаИСМП.ВидПервичногоДокумента          КАК ВидПервичногоДокумента,
	|	ВыводИзОборотаИСМП.НаименованиеПервичногоДокумента КАК НаименованиеПервичногоДокумента
	|ИЗ
	|	Документ.к2ВыводИзОборотаИСМП КАК ВыводИзОборотаИСМП
	|ГДЕ
	|	ВыводИзОборотаИСМП.Организация = &Организация Или &БезУчетаОрганизации
	|	И ВыводИзОборотаИСМП.Проведен
	|УПОРЯДОЧИТЬ ПО
	|	ВыводИзОборотаИСМП.Дата УБЫВ");
	
	Запрос.УстановитьПараметр("Организация",         Организация);
	Запрос.УстановитьПараметр("БезУчетаОрганизации", Не ЗначениеЗаполнено(Организация));
	
	РезультатЗапроса = Запрос.Выполнить();
	ДанныеПоследнихДокументов = РезультатЗапроса.Выгрузить();
	
	ДанныеЗаполнения.ВидПродукции = ВидПродукции(ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Операция",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Организация",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"ВидПервичногоДокумента, НаименованиеПервичногоДокумента",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	// Значения по-умолчанию
	Если Не ЗначениеЗаполнено(ДанныеЗаполнения.ВидПродукции) Тогда
		ДоступныеВидыПродукцииИС = к2ИнтеграцияИСКлиентСервер.ВидыПродукцииИСМП();
		Для Каждого ВидПродукцииУчета Из ВидыПродукцииУчета Цикл
			Если ДоступныеВидыПродукцииИС.Найти(ВидПродукцииУчета) <> Неопределено Тогда
				ДанныеЗаполнения.ВидПродукции = ВидПродукцииУчета;
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат ДанныеЗаполнения;
	
КонецФункции

Функция ДанныеЗаполненияОтгрузкиТоваровИСМП(Организация) Экспорт
	
	ДанныеЗаполнения = Новый Структура;
	ДанныеЗаполнения.Вставить("Организация");
	ДанныеЗаполнения.Вставить("Операция");
	
	ДанныеЗаполнения.Вставить("ВидПродукции", Перечисления.к2ВидыПродукцииИС.ПустаяСсылка());
	ДанныеЗаполнения.Вставить("Контрагент",   к2ИнтеграцияИС.ПустоеЗначениеОпределяемогоТипа("к2КонтрагентГосИС"));
	
	ВидыПродукцииУчета = к2ИнтеграцияИСМПКлиентСерверПовтИсп.УчитываемыеВидыМаркируемойПродукции();
	
	Если ВидыПродукцииУчета.Количество() = 0 Тогда
		Возврат ДанныеЗаполнения;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 3
	|	1                                                   КАК Количество,
	|	ОтгрузкаТоваровИСМП.Организация                     КАК Организация,
	|	ОтгрузкаТоваровИСМП.ВидПродукции                    КАК ВидПродукции,
	|	ОтгрузкаТоваровИСМП.Операция                        КАК Операция,
	|	ОтгрузкаТоваровИСМП.Контрагент                      КАК Контрагент
	|ИЗ
	|	Документ.к2ОтгрузкаТоваровИСМП КАК ОтгрузкаТоваровИСМП
	|ГДЕ
	|	ОтгрузкаТоваровИСМП.Организация = &Организация Или &БезУчетаОрганизации
	|	И ОтгрузкаТоваровИСМП.Проведен
	|УПОРЯДОЧИТЬ ПО
	|	ОтгрузкаТоваровИСМП.Дата УБЫВ");
	
	Запрос.УстановитьПараметр("Организация",         Организация);
	Запрос.УстановитьПараметр("БезУчетаОрганизации", Не ЗначениеЗаполнено(Организация));
	
	РезультатЗапроса = Запрос.Выполнить();
	ДанныеПоследнихДокументов = РезультатЗапроса.Выгрузить();
	
	ДанныеЗаполнения.ВидПродукции = ВидПродукции(ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Операция",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Организация",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	ЗаполнитьПоСтатистике(
		"Контрагент",
		ДанныеЗаполнения, ДанныеПоследнихДокументов);
	
	// Значения по-умолчанию
	Если Не ЗначениеЗаполнено(ДанныеЗаполнения.ВидПродукции) Тогда
		ДоступныеВидыПродукцииИС = к2ИнтеграцияИСКлиентСервер.ВидыПродукцииИСМП();
		Для Каждого ВидПродукцииУчета Из ВидыПродукцииУчета Цикл
			Если ДоступныеВидыПродукцииИС.Найти(ВидПродукцииУчета) <> Неопределено Тогда
				ДанныеЗаполнения.ВидПродукции = ВидПродукцииУчета;
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат ДанныеЗаполнения;
	
КонецФункции

Процедура ЗаполнитьПустойРеквизит(Объект, ДанныеСтатистики, ИмяРеквизита) Экспорт
	
	Если Не ЗначениеЗаполнено(Объект[ИмяРеквизита]) Тогда
		Объект[ИмяРеквизита] = ДанныеСтатистики[ИмяРеквизита];
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьТаблицуДаннымиЗаполнения(ИсходныеДанные, ДанныеДляЗаполнения, ПоляИсключения = "") Экспорт

	Для Каждого СтрокаДанных Из ДанныеДляЗаполнения Цикл
		СтрокаТовары = ИсходныеДанные.Получить(СтрокаДанных.ИндексИсходнойСтроки);
		Если ЗначениеЗаполнено(СтрокаТовары.Номенклатура) Тогда
			ЗаполнитьЗначенияСвойств(СтрокаТовары, СтрокаДанных,, ПоляИсключения);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьТабличнуюЧастьТоварыЗаказаНаЭмиссиюКодовМаркировкиСУЗ(Объект, ПропускатьЗаполнениеGTIN) Экспорт
	
	ЗаполнитьДанныеПоТоварамЗаказаНаЭмиссиюКодовМаркировкиСУЗ(Объект.Товары, Объект, Неопределено, ПропускатьЗаполнениеGTIN);
	
КонецПроцедуры

Процедура ЗаполнитьДанныеПоТоварамЗаказаНаЭмиссиюКодовМаркировкиСУЗ(ИсходныеДанные, Объект, ПолеОтслеживанияНовойСтроки = Неопределено, ПропускатьЗаполнениеGTIN = Ложь) Экспорт
	
	Если ТипЗнч(ИсходныеДанные) = Тип("ТаблицаЗначений") Тогда
		ВременнаяТаблица = ИсходныеДанные;
	Иначе
		ВременнаяТаблица = ИсходныеДанные.Выгрузить(
			, "Номенклатура, Характеристика, СпособФормированияСерийногоНомера");
	КонецЕсли;
	
	ПронумероватьИсходнуюТаблицуДанныхЗаполнения(ВременнаяТаблица, ПолеОтслеживанияНовойСтроки);
	
	ТаблицаДанных = ДанныеЗаполненияТоварыЗаказНаЭмиссиюСУЗ(ВременнаяТаблица, Объект.Организация);
	
	Если ПропускатьЗаполнениеGTIN Тогда
		ПоляИсключения = "GTIN";
	КонецЕсли;
	
	ЗаполнитьТаблицуДаннымиЗаполнения(ИсходныеДанные, ТаблицаДанных, ПоляИсключения);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ДанныеЗаполненияТоварыЗаказНаЭмиссиюСУЗ(ИсходнаяТаблица, Организация) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ТаблицаТовары.ИндексИсходнойСтроки КАК ИндексИсходнойСтроки,
		|	ТаблицаТовары.Номенклатура КАК Номенклатура,
		|	ТаблицаТовары.Характеристика КАК Характеристика
		|ПОМЕСТИТЬ ВременнаяТаблицаТовары
		|ИЗ
		|	&ТаблицаТовары КАК ТаблицаТовары
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	ВременнаяТаблицаТовары.ИндексИсходнойСтроки КАК ИндексИсходнойСтроки,
		|	ВременнаяТаблицаТовары.Номенклатура КАК Номенклатура,
		|	ВременнаяТаблицаТовары.Характеристика КАК Характеристика,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.СпособФормированияСерийногоНомера КАК СпособФормированияСерийногоНомера,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.СтатусУказанияСерии КАК СтатусУказанияСерии,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.GTIN КАК GTIN,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка КАК Ссылка,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка.Дата КАК Дата
		|ПОМЕСТИТЬ ДокументыПоНоменклатуре
		|ИЗ
		|	ВременнаяТаблицаТовары КАК ВременнаяТаблицаТовары
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.к2ЗаказНаЭмиссиюКодовМаркировкиСУЗ.Товары КАК ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары
		|		ПО ВременнаяТаблицаТовары.Номенклатура = ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Номенклатура
		|			И ВременнаяТаблицаТовары.Характеристика = ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Характеристика
		|			И (ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка.Проведен)
		|			И (ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка.Организация = &Организация)
		|
		|СГРУППИРОВАТЬ ПО
		|	ВременнаяТаблицаТовары.ИндексИсходнойСтроки,
		|	ВременнаяТаблицаТовары.Номенклатура,
		|	ВременнаяТаблицаТовары.Характеристика,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.СпособФормированияСерийногоНомера,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.СтатусУказанияСерии,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.GTIN,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка,
		|	ЗаказНаЭмиссиюКодовМаркировкиСУЗТовары.Ссылка.Дата
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика,
		|	Дата
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДокументыПоНоменклатуре.Номенклатура КАК Номенклатура,
		|	ДокументыПоНоменклатуре.Характеристика КАК Характеристика,
		|	МАКСИМУМ(ДокументыПоНоменклатуре.Дата) КАК Дата
		|ПОМЕСТИТЬ ГруппировкаПоМаксимальнойДатеДокумента
		|ИЗ
		|	ДокументыПоНоменклатуре КАК ДокументыПоНоменклатуре
		|
		|СГРУППИРОВАТЬ ПО
		|	ДокументыПоНоменклатуре.Характеристика,
		|	ДокументыПоНоменклатуре.Номенклатура
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика,
		|	Дата
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВременнаяТаблицаТовары.ИндексИсходнойСтроки КАК ИндексИсходнойСтроки,
		|	ВременнаяТаблицаТовары.Номенклатура КАК Номенклатура,
		|	ВременнаяТаблицаТовары.Характеристика КАК Характеристика,
		|	ЕСТЬNULL(ДокументыПоНоменклатуре.СпособФормированияСерийногоНомера, ЗНАЧЕНИЕ(Перечисление.к2СпособыФормированияСерийногоНомераСУЗ.Автоматически)) КАК СпособФормированияСерийногоНомера,
		|	ВЫБОР
		|		КОГДА ЕСТЬNULL(ДокументыПоНоменклатуре.СпособФормированияСерийногоНомера, ЗНАЧЕНИЕ(Перечисление.к2СпособыФормированияСерийногоНомераСУЗ.Автоматически)) = ЗНАЧЕНИЕ(Перечисление.к2СпособыФормированияСерийногоНомераСУЗ.Автоматически)
		|			ТОГДА 2
		|		ИНАЧЕ ЕСТЬNULL(ДокументыПоНоменклатуре.СтатусУказанияСерии, 2)
		|	КОНЕЦ КАК СтатусУказанияСерии,
		|	ДокументыПоНоменклатуре.GTIN КАК GTIN
		|ИЗ
		|	ВременнаяТаблицаТовары КАК ВременнаяТаблицаТовары
		|		ЛЕВОЕ СОЕДИНЕНИЕ ДокументыПоНоменклатуре КАК ДокументыПоНоменклатуре
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ ГруппировкаПоМаксимальнойДатеДокумента КАК ГруппировкаПоМаксимальнойДатеДокумента
		|			ПО ДокументыПоНоменклатуре.Номенклатура = ГруппировкаПоМаксимальнойДатеДокумента.Номенклатура
		|				И ДокументыПоНоменклатуре.Характеристика = ГруппировкаПоМаксимальнойДатеДокумента.Характеристика
		|				И ДокументыПоНоменклатуре.Дата = ГруппировкаПоМаксимальнойДатеДокумента.Дата
		|		ПО ВременнаяТаблицаТовары.Номенклатура = ДокументыПоНоменклатуре.Номенклатура
		|			И ВременнаяТаблицаТовары.Характеристика = ДокументыПоНоменклатуре.Характеристика";
	
	Запрос.УстановитьПараметр("ТаблицаТовары", ИсходнаяТаблица);
	Запрос.УстановитьПараметр("Организация",   Организация);
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

Процедура ПронумероватьИсходнуюТаблицуДанныхЗаполнения(ИсходнаяТаблица, ПолеОтслеживанияНовойСтроки = Неопределено) Экспорт
	
	Если ИсходнаяТаблица.Колонки.Найти("ИндексИсходнойСтроки") = Неопределено Тогда
		ИсходнаяТаблица.Колонки.Добавить("ИндексИсходнойСтроки", ОбщегоНазначения.ОписаниеТипаЧисло(5));
	КонецЕсли;
	
	МассивСтрокДляУдаления = Новый Массив;
	
	Для Каждого СтрокаТаблицы Из ИсходнаяТаблица Цикл
		СтрокаТаблицы.ИндексИсходнойСтроки = ИсходнаяТаблица.Индекс(СтрокаТаблицы);
		Если ЗначениеЗаполнено(ПолеОтслеживанияНовойСтроки)
			И ЗначениеЗаполнено(СтрокаТаблицы[ПолеОтслеживанияНовойСтроки]) Тогда
			МассивСтрокДляУдаления.Добавить(СтрокаТаблицы);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого УдаляемаяСтрока Из МассивСтрокДляУдаления Цикл
		ИсходнаяТаблица.Удалить(УдаляемаяСтрока.ИндексИсходнойСтроки);
	КонецЦикла;

КонецПроцедуры

Функция ВидПродукции(ДанныеПоследнихДокументов)
	
	ДанныеАнализа = ДанныеПоследнихДокументов.Скопировать(,"ВидПродукции, Количество");
	ДанныеАнализа.Свернуть("ВидПродукции", "Количество");
	ДанныеАнализа.Сортировать("Количество Убыв");
	Если ДанныеАнализа.Количество() > 0 Тогда
		ВидПродукции = ДанныеАнализа[0].ВидПродукции;
	КонецЕсли;
	
	Если Не к2ИнтеграцияИСМПКлиентСерверПовтИсп.ВестиУчетМаркируемойПродукции(ВидПродукции) Тогда
		ВидПродукции = Неопределено;
	КонецЕсли;
	
	Возврат ВидПродукции;
	
КонецФункции

Процедура ЗаполнитьПоСтатистике(Поля, ДанныеЗаполнения, ДанныеПоследнихДокументов)
	
	Если ДанныеЗаполнения.ВидПродукции <> Неопределено Тогда
		ОтборПоВидуПродукции = Новый Структура("ВидПродукции", ДанныеЗаполнения.ВидПродукции);
	Иначе
		ОтборПоВидуПродукции = Неопределено;
	КонецЕсли;
	
	ЗаполняемыеПоля = СтрРазделить(Поля, ",");
	
	РезультатыАнализа = ДанныеПоследнихДокументов.Скопировать(
		ОтборПоВидуПродукции, Поля + ", Количество");
	
	РезультатыАнализа.Свернуть(Поля, "Количество");
	РезультатыАнализа.Сортировать("Количество Убыв");
	Если РезультатыАнализа.Количество() > 0 Тогда
		Для Каждого Поле Из ЗаполняемыеПоля Цикл
			ДанныеЗаполнения[СокрЛП(Поле)] = РезультатыАнализа[0][СокрЛП(Поле)];
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти