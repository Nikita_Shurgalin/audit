
Функция ЗахватитьЗадачу( пЗадача ) Экспорт
	
	Если глРеквизит( пЗадача , "Адресация" ) = глТекущийПользователь() Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если упУправлениеЗадачамиКлиентСервер.ЗадачуМожноЗахватить( пЗадача ) Тогда
		
		УстановитьАдресациюЗадаче( глТекущийПользователь(), пЗадача );
		
		Возврат Истина;
		
	Иначе
		
		Возврат Ложь;
		
	КонецЕсли;
	
КонецФункции


// Используется для закрытия задачи, у которой не открыта форма документа.
//	Иначе задача не завершится из-за попытки заблокировать заблокированную формой задачу.
Функция ЗавершитьЗадачу( Знач пЗадача ) Экспорт
	
	Возврат упЯдроПроцесса.ЗавершитьЗадачу( пЗадача );
	
КонецФункции



Процедура СделатьТекущей(пЗадача) Экспорт
	
	РегистрыСведений.упТекущаяЗадачаПользователя.УстановитьТекущейЗадачей( пЗадача );
	
КонецПроцедуры

Функция ОтметитьПросмотренным( пЗадача ) Экспорт
	
	УстановитьПривилегированныйРежим( Истина );
	
	успешно = Ложь;
	
	Если Не РегистрыСведений.упПросмотренныеЗадачи.ЕстьЗадача( пЗадача ) Тогда
		
		РегистрыСведений.упПросмотренныеЗадачи.Добавить( пЗадача );
		
		успешно = Истина;
		
	КонецЕсли;
	
	УстановитьПривилегированныйРежим( Ложь );
	
	Если успешно Тогда
		
		упУправлениеЗадачамиПереопределяемый.ПриОтметкеПросмотренной( пЗадача );
		
	КонецЕсли;
	
	Возврат успешно;
	
КонецФункции


Функция КоличествоНовыхЗадачТекущегоПользователя() Экспорт
	
	Возврат упСтатистики.КоличествоНовыхЗадач( глТекущийПользователь() );
	
КонецФункции


//////////////////////////////////////////////////////////////////////////////////////
// {{АДРЕСАЦИЯ

Процедура УстановитьАдресациюЗадаче( пАдресация, пЗадача ) Экспорт
	
	Если Не ЗначениеЗаполнено( пЗадача )
		ИЛИ глРеквизит( пЗадача , "Адресация" ) = пАдресация Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим( Истина );
	
	обЗадача = пЗадача.ПолучитьОбъект();
	обЗадача.Адресация = пАдресация;
	обЗадача.Записать();
	
	шаблонТекста = упИК.т__Задача_УстановленаНоваяАдресация();
	текстСообщения = стрЗаполнить( шаблонТекста,  пАдресация, пЗадача );
	
	упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения , пЗадача );
	
	УстановитьПривилегированныйРежим( Ложь );
	
КонецПроцедуры

Функция ПользовательЗадачи( пЗадача ) Экспорт
	
	УстановитьПривилегированныйРежим( Истина );
	
	пользовательЗадачи = Задачи.упЗадача.ПользовательЗадачи( пЗадача );
	
	УстановитьПривилегированныйРежим( Ложь );
	
	Возврат пользовательЗадачи;
	
КонецФункции

Функция ГруппаПользователейЗадачи( пЗадача ) Экспорт
	
	УстановитьПривилегированныйРежим( Истина );
	
	группаПользователейЗадачи = Задачи.упЗадача.ГруппаПользователейЗадачи( пЗадача );
	
	УстановитьПривилегированныйРежим( Ложь );
	
	Возврат группаПользователейЗадачи;
	
КонецФункции

Функция АдресацияЗадачи( пЗадача ) Экспорт
	
	пользователь = упУправлениеЗадачамиВызовСервера.ПользовательЗадачи( пЗадача );
	
	Если ЗначениеЗаполнено( пользователь ) Тогда
		
		адресация = пользователь;
		
	Иначе
		
		адресация = упУправлениеЗадачамиВызовСервера.ГруппаПользователейЗадачи( пЗадача );
		
	КонецЕсли;
	
	Возврат адресация;
	
КонецФункции

// }}АДРЕСАЦИЯ


//////////////////////////////////////////////////////////////////////////////////////
// {{ЗАПУСК БП

// Проверяем необходимость открытия задачи
//
// Параметры
//  НоваяЗадача  – Созданная задача
//  ТекЗадача  – Задача, которая завершается.
//
// Возвращаемое значение:
//   Булево   – Открывать?
//
Функция ОткрыватьСледующуюЗадачу( пЗадача ) Экспорт
	
	Если Не упСистемаБезопасностиПовтИсп.ЕстьПравоНаЗадачи() Тогда
		
		Возврат Ложь;
		
	КонецЕсли;
	
	Открывать = Истина;
	
	Если Не ЗначениеЗаполнено( пЗадача )
		ИЛИ упУправлениеЗадачамиКлиентСервер.ЗадачаВыполнена( пЗадача ) Тогда
		
		Открывать = Ложь;
		
	Иначе
		
		Открывать = упКэш.ПользовательВходитВГруппу( глТекущийПользователь() , упУправлениеЗадачамиВызовСервера.АдресацияЗадачи( пЗадача ) );
		
	КонецЕсли;
	
	Возврат Открывать;
	
КонецФункции // ОткрыватьСледующуюЗадачу()

// }}ЗАПУСК БП