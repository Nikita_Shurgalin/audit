
Функция ТаблицаЗадачПоСтатистике( пПользователь, пПоказатьЗавершенные, пОтбор ) Экспорт
	
	СКД = упКэш.ПолучитьСКД_АналогДинамическогоСпискаЗадач( пПользователь );
	
	настройки = СКД.ВариантыНастроек.Сохранение.Настройки;
	
	упСКДКлиентСервер.УстановитьПараметр( настройки, "ПоказатьЗавершенные" , пПоказатьЗавершенные );
	упСКДКлиентСервер.УстановитьПараметр( настройки, "ПоказатьВсеДоступные", Истина );
	
	упСКДКлиентСервер.СкопироватьЭлементы( настройки.Отбор, пОтбор );
	
	Возврат упСКДКлиентСервер.ТаблицаСКД( СКД,, настройки );
	
КонецФункции

Функция ТаблицаСессийПоСтатистике( пПользователь, пПоказатьЗавершенные, пОтбор ) Экспорт
	
	СКД = упКэш.ПолучитьСКД_АналогДинамическогоСпискаСессий( пПользователь );
	
	настройки = СКД.ВариантыНастроек.Сохранение.Настройки;
	
	упСКДКлиентСервер.УстановитьПараметр( настройки, "ПоказатьЗавершенные" ,  пПоказатьЗавершенные );
	упСКДКлиентСервер.УстановитьПараметр( настройки, "ПоказатьВсеДоступные",  Истина );
	упСКДКлиентСервер.УстановитьПараметр( настройки, "ПоказатьСМоимУчастием", Ложь );
	
	упСКДКлиентСервер.СкопироватьЭлементы( настройки.Отбор, пОтбор );
	
	Возврат упСКДКлиентСервер.ТаблицаСКД( СКД,, настройки );
	
КонецФункции

Процедура упСохранениеСтатистик() Экспорт
	
	РегистрыСведений.упДинамикаСтатистик.Заполнить();
	
КонецПроцедуры



Функция КоличествоНовыхЗадач( пПользователь ) Экспорт
	
	СКД = упКэш.ПолучитьСКД_АналогДинамическогоСпискаЗадач( пПользователь );
	
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьЗавершенные" , Ложь );
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьВсеДоступные", Ложь );
	
	Для каждого цЭлемент Из СКД.НастройкиПоУмолчанию.Отбор.Элементы Цикл
		цЭлемент.Использование = Ложь;
	КонецЦикла;
	
	упОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора( СКД.НастройкиПоУмолчанию.Отбор, "Новая" , Истина, ВидСравненияКомпоновкиДанных.Равно,,Истина );
	
	Возврат ПолучитьЗначение( СКД );
	
КонецФункции

Функция ПредставлениеПоИмениТаблицыМетаданных( пИмяТаблицы ) Экспорт
	
	Если пИмяТаблицы = упИК.ИмяТаблицыМетаданныхСтатистикиЗадачи() Тогда
		
		Возврат НСтр("ru='Задачи';en='Tasks'" );
		
	ИначеЕсли пИмяТаблицы = упИК.ИмяТаблицыМетаданныхСтатистикиСессии() Тогда
		
		Возврат НСтр("ru='Сессии';en='Session'" );
		
	Иначе
		
		Возврат "";
		
	КонецЕсли;
	
КонецФункции



Процедура ЗаполнитьТаблицуСтатистики( пПользователь = Неопределено, пИмяТаблицыМетаданных = Неопределено) Экспорт
	
	таблицаСтатистик = ПолучитьТаблицуСтатистики( пПользователь, пИмяТаблицыМетаданных );
	
	тзОтборы = таблицаСтатистик.Скопировать(,"Пользователь,ИмяТаблицыМетаданных");
	тзОтборы.Свернуть("Пользователь,ИмяТаблицыМетаданных");
	
	нз = РегистрыСведений.упЗначенияСтатистик.СоздатьНаборЗаписей();
	
	структОтбора = Новый Структура( "Пользователь,ИмяТаблицыМетаданных" );
	
	текДата = НачалоДня( ТекущаяДатаСеанса() );
	
	Для каждого цОтбор Из тзОтборы Цикл
		
		ЗаполнитьЗначенияСвойств(структОтбора, цОтбор);
		
		нз.Отбор.ИмяТаблицыМетаданных.Установить(цОтбор.ИмяТаблицыМетаданных);
		нз.Отбор.Пользователь.Установить(цОтбор.Пользователь);
		нз.Отбор.Период.Установить(текДата);
		
		нз.Загрузить( таблицаСтатистик.Скопировать( структОтбора ) );
		
		Для каждого цСтрока Из нз Цикл
			цСтрока.Период = текДата;
		КонецЦикла;
		
		нз.Записать();
		
	КонецЦикла;
	
КонецПроцедуры

Функция ПолучитьТаблицуСтатистики( пПользователь, пИмяТаблицыМетаданных = Неопределено ) Экспорт
	
	УстановитьПривилегированныйРежим( Истина );
	
	тзСтатистики = ПолучитьЭлементыСтатистики( пПользователь, пИмяТаблицыМетаданных );
	
	Для Каждого цЭлемент Из тзСтатистики Цикл
		
		цЭлемент.Значение = ЗначениеСтатистики(цЭлемент.ИмяТаблицыМетаданных,
												цЭлемент.Пользователь,
												цЭлемент.ПоказатьЗавершенные,
												цЭлемент.ПоказатьВсеДоступные,
												упНастройкиСервер.ОтборПользовательскихНастроек(цЭлемент.ПользовательскиеНастройки.Получить()));
													
		цЭлемент.Представление = ПредставлениеСтатистики( цЭлемент.Наименование, цЭлемент.Значение, цЭлемент.ПредЗначение , цЭлемент.ЗначениеНаНачалоДня );
		
	КонецЦикла;
	
	УстановитьПривилегированныйРежим( Ложь );
	
	Возврат тзСтатистики;
	
КонецФункции

Функция ПолучитьЭлементыСтатистики( пПользователь = Неопределено, пИмяТаблицыМетаданных = Неопределено )
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упЭлементСтатистики.Ссылка КАК ЭлементСтатистики,
	               |	упЭлементСтатистики.Наименование,
	               |	упЭлементСтатистики.ПоказатьЗавершенные,
	               |	упЭлементСтатистики.ПоказатьВсеДоступные,
	               |	упЭлементСтатистики.ИмяТаблицыМетаданных,
	               |	упЭлементСтатистики.Код КАК ИД,
	               |	упЭлементСтатистики.Пользователь,
	               |	ЕСТЬNULL(ПоследниеЗначения.Значение, -1) КАК ПредЗначение,
	               |	ЕСТЬNULL(ЗначенияНаНачалоДня.Значение, -1) КАК ЗначениеНаНачалоДня,
	               |	упЭлементСтатистики.Наименование КАК Представление,
	               |	0 КАК Значение,
	               |	упЭлементСтатистики.ПользовательскиеНастройки
	               |ИЗ
	               |	Справочник.упЭлементСтатистики КАК упЭлементСтатистики
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.упЗначенияСтатистик.СрезПоследних(
	               |				,
	               |				Пользователь = &Пользователь
	               |					И ИмяТаблицыМетаданных = &ИмяТаблицыМетаданных) КАК ПоследниеЗначения
	               |		ПО упЭлементСтатистики.ИмяТаблицыМетаданных = ПоследниеЗначения.ИмяТаблицыМетаданных
	               |			И упЭлементСтатистики.Ссылка = ПоследниеЗначения.ЭлементСтатистики
	               |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.упЗначенияСтатистик.СрезПоследних(
	               |				&ПредДень,
	               |				Пользователь = &Пользователь
	               |					И ИмяТаблицыМетаданных = &ИмяТаблицыМетаданных) КАК ЗначенияНаНачалоДня
	               |		ПО упЭлементСтатистики.ИмяТаблицыМетаданных = ЗначенияНаНачалоДня.ИмяТаблицыМетаданных
	               |			И упЭлементСтатистики.Ссылка = ЗначенияНаНачалоДня.ЭлементСтатистики
	               |ГДЕ
	               |	упЭлементСтатистики.Пользователь = &Пользователь
	               |	И упЭлементСтатистики.ПометкаУдаления = ЛОЖЬ
	               |	И упЭлементСтатистики.Скрыть = ЛОЖЬ
	               |	И упЭлементСтатистики.ИмяТаблицыМетаданных = &ИмяТаблицыМетаданных
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	упЭлементСтатистики.РеквизитДопУпорядочивания";
	
	Запрос.УстановитьПараметр("ПредДень", КонецДня( ТекущаяДатаСеанса() - 24*60*60));
	
	Если пПользователь = Неопределено Тогда
		Запрос.Текст = СтрЗаменить( Запрос.Текст, "упЭлементСтатистики.Пользователь = &Пользователь", "Истина");
		Запрос.Текст = СтрЗаменить( Запрос.Текст, "Пользователь = &Пользователь", "Истина");
	Иначе
		Запрос.УстановитьПараметр( "Пользователь" , пПользователь);
	КонецЕсли;
	
	Если пИмяТаблицыМетаданных = Неопределено Тогда
		
		Запрос.Текст = СтрЗаменить( Запрос.Текст, "И упЭлементСтатистики.ИмяТаблицыМетаданных = &ИмяТаблицыМетаданных", "");
		Запрос.Текст = СтрЗаменить( Запрос.Текст, "И ИмяТаблицыМетаданных = &ИмяТаблицыМетаданных", "");
		
	Иначе
		
		Запрос.УстановитьПараметр("ИмяТаблицыМетаданных", пИмяТаблицыМетаданных);
		
	КонецЕсли;
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции	//ПолучитьЭлементыСтатистики

Функция ЗначениеСтатистики(пИмяТаблицыМетаданных, пПользователь, пПоказатьЗавершенные, пПоказатьВсеДоступные, пОтборКомпоновщика)
	
	Результат = 0;
	
	Если пИмяТаблицыМетаданных = упИК.ИмяТаблицыМетаданныхСтатистикиЗадачи() Тогда
		
		Результат = КоличествоЗадач( пПользователь, пПоказатьЗавершенные, пПоказатьВсеДоступные, пОтборКомпоновщика );
		
	ИначеЕсли пИмяТаблицыМетаданных = упИК.ИмяТаблицыМетаданныхСтатистикиСессии() Тогда
		
		Результат = КоличествоСессий( пПользователь, пПоказатьЗавершенные, пПоказатьВсеДоступные, пОтборКомпоновщика );
		
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции


Функция КоличествоЗадач( пПользователь, пПоказатьЗавершенные, пПоказатьВсеДоступные, пОтбор ) Экспорт
	
	СКД = упКэш.ПолучитьСКД_АналогДинамическогоСпискаЗадач( пПользователь );
	
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьЗавершенные" , пПоказатьЗавершенные );
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьВсеДоступные", пПоказатьВсеДоступные );
	
	Возврат ПолучитьЗначение( СКД, пОтбор );
	
КонецФункции

Функция КоличествоСессий( пПользователь, пПоказатьЗавершенные, пПоказатьВсеДоступные, пОтбор )Экспорт
	
	СКД = упКэш.ПолучитьСКД_АналогДинамическогоСпискаСессий( пПользователь );
	
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьЗавершенные" , пПоказатьЗавершенные );
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьВсеДоступные", пПоказатьВсеДоступные );
	упСКДКлиентСервер.УстановитьПараметр( СКД.НастройкиПоУмолчанию, "ПоказатьСМоимУчастием", Ложь);
	
	Возврат ПолучитьЗначение( СКД, пОтбор );
	
КонецФункции

Функция ПолучитьЗначение( СКД, пОтбор = Неопределено )
	
	Если Не пОтбор = Неопределено Тогда
		упСКДКлиентСервер.СкопироватьЭлементы( СКД.НастройкиПоУмолчанию.Отбор, пОтбор );
	КонецЕсли;
	
	упСКДКлиентСервер.БезопасноСкопироватьНастройки( СКД, СКД.НастройкиПоУмолчанию, СКД.НастройкиПоУмолчанию );
	
	тзРезультат = упСКДКлиентСервер.ТаблицаСКД( СКД );
	
	Возврат тзРезультат.Итог( "Количество" );
	
КонецФункции	//ПолучитьЗначение



Функция ПредставлениеСтатистики( пНаименование, пЗначение, пСтарЗначение, пЗначениеНаНачалоДня )
	
	шаблон = "%2 (%1)";
	
	лЗаголовок = стрЗаполнить( шаблон , пЗначение , пНаименование );
	
	Если Не пСтарЗначение = -1 Тогда
		
		Если пЗначение > пСтарЗначение Тогда
			
			лЗаголовок = лЗаголовок + "  +" + (пЗначение - пСтарЗначение);
			
		ИначеЕсли пЗначение < пСтарЗначение Тогда
			
			лЗаголовок = лЗаголовок + "  -" + (пСтарЗначение - пЗначение);
			
		КонецЕсли;
		
	КонецЕсли;	
	
	Если Не пЗначениеНаНачалоДня = -1 Тогда
		
		Если пЗначение > пЗначениеНаНачалоДня Тогда
			
			лЗаголовок = лЗаголовок + "  [+" + (пЗначение - пЗначениеНаНачалоДня) + "]";
			
		ИначеЕсли пЗначение < пЗначениеНаНачалоДня Тогда
			
			лЗаголовок = лЗаголовок + "  [-" + (пЗначениеНаНачалоДня - пЗначение) + "]";
			
		Иначе
			
			лЗаголовок = лЗаголовок + "  [0]";
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат лЗаголовок;
	
КонецФункции



