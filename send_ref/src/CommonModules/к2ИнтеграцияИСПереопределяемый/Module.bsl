// Универсальные механизмы интеграции ИС

#Область СлужебныйПрограммныйИнтерфейс

#Область ЗаполнениеИПроверкаЗаполнения

// Заполняет в табличной части служебные реквизиты, например: признак использования характеристик номенклатуры.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - Форма.
//  ТабличнаяЧасть - ДанныеФормыКоллекция - таблица для заполнения.
Процедура ЗаполнитьСлужебныеРеквизитыВКоллекции(Форма, ТабличнаяЧасть) Экспорт
	
	ПараметрыЗаполненияРеквизитов = Новый Структура;
	
	КолонкиРеквизитов = ТабличнаяЧасть.Выгрузить(Новый Массив).Колонки;
	
	Если КолонкиРеквизитов.Найти("ХарактеристикиИспользуются")<> Неопределено Тогда
		ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются",
			Новый Структура("Номенклатура", "ХарактеристикиИспользуются"));
	КонецЕсли;
	
	Если КолонкиРеквизитов.Найти("СерииИспользуются")<> Неопределено Тогда
		ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакСерииИспользуются",
			Новый Структура("Номенклатура", "СерииИспользуются"));
	КонецЕсли;
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		ТабличнаяЧасть, ПараметрыЗаполненияРеквизитов);
		
	Возврат;
	
КонецПроцедуры

// Обрабатывает строку табличной части в соостветствии с переданной структурой действий.
//
// Параметры:
//  СтрокаТабличнойЧасти - Структура, СтрокаТабличнойЧасти, ДокументСтрокаТабличнойЧасти - обрабатываемая строка.
//  СтруктураДействий - структура - структура выполняемых действий.
Процедура ОбработатьСтрокуТабличнойЧасти(СтрокаТабличнойЧасти, СтруктураДействий) Экспорт
	
	ТипСтрокиТабличнойЧасти = ТипЗнч(СтрокаТабличнойЧасти);
	
	Если ТипСтрокиТабличнойЧасти = Тип("Структура") Тогда
		СтрокаТабличнойЧастиСтруктурой = ОбщегоНазначения.СкопироватьРекурсивно(СтрокаТабличнойЧасти, Ложь);
	ИначеЕсли ТипСтрокиТабличнойЧасти = Тип("СтрокаТаблицыЗначений") Тогда
		СтрокаТабличнойЧастиСтруктурой = ОбщегоНазначения.СтрокаТаблицыЗначенийВСтруктуру(СтрокаТабличнойЧасти);
	ИначеЕсли Метаданные.НайтиПоТипу(ТипСтрокиТабличнойЧасти) <> Неопределено Тогда
		СтрокаТабличнойЧастиСтруктурой = Новый Структура();
		ПодстрокиИмени = СтрРазделить(Метаданные.НайтиПоТипу(ТипСтрокиТабличнойЧасти).ПолноеИмя(), ".");
		
		Если ПодстрокиИмени.Количество() = 4 Тогда
			РеквизитыСтроки = Метаданные.Документы[ПодстрокиИмени[1]].ТабличныеЧасти[ПодстрокиИмени[3]].Реквизиты;
			Для Каждого РеквизитСтроки Из РеквизитыСтроки Цикл
				СтрокаТабличнойЧастиСтруктурой.Вставить(РеквизитСтроки.Имя, СтрокаТабличнойЧасти[РеквизитСтроки.Имя]);
			КонецЦикла;
		Иначе
			Возврат;
		КонецЕсли;
	Иначе
		Возврат;
	КонецЕсли;

	к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(СтрокаТабличнойЧастиСтруктурой, СтруктураДействий, Неопределено);
	ЗаполнитьЗначенияСвойств(СтрокаТабличнойЧасти, СтрокаТабличнойЧастиСтруктурой);

	Возврат;
	
КонецПроцедуры

// Проверяет заполнение характеристик в таблице значений.
//
// Параметры:
//  ТаблицаТоваров - ТаблицаЗначений - таблица для проверки с колонками: Номенклатура, Характеристика.
//  Отказ - Булево - Истина - характеристики заполнены в требуемых строках, в противном случае - Ложь.
Процедура ПроверитьЗаполнениеХарактеристикВТаблицеЗначений(ТаблицаТоваров, Отказ) Экспорт
	
	Запрос = Новый Запрос;
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ТаблицаТоваров.Номенклатура КАК Номенклатура
	|ПОМЕСТИТЬ СтрокиСОшибками
	|ИЗ
	|	&ТаблицаТоваров КАК ТаблицаТоваров
	|ГДЕ
	|	ТаблицаТоваров.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВЫБОР
	|		КОГДА ВЫРАЗИТЬ(СтрокиСОшибками.Номенклатура КАК Справочник.Номенклатура).ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.к2ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры)
	|				ИЛИ ВЫРАЗИТЬ(СтрокиСОшибками.Номенклатура КАК Справочник.Номенклатура).ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.к2ВариантыИспользованияХарактеристикНоменклатуры.ИндивидуальныеДляНоменклатуры)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК НеЗаполненаХарактеристика
	|ИЗ
	|	СтрокиСОшибками КАК СтрокиСОшибками
	|ГДЕ
	|	ВЫРАЗИТЬ(СтрокиСОшибками.Номенклатура КАК Справочник.Номенклатура).ИспользованиеХарактеристик <> ЗНАЧЕНИЕ(Перечисление.к2ВариантыИспользованияХарактеристикНоменклатуры.НеИспользовать)";

	Запрос.Текст = ТекстЗапроса;
	
	Запрос.УстановитьПараметр("ТаблицаТоваров", ТаблицаТоваров);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		Если Выборка.НеЗаполненаХарактеристика Тогда
			Отказ = Истина;
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	Возврат;
	
КонецПроцедуры

// Необходимо заполнить информацию о сертификатах номенклатуры в виде соответствия из массива структур со свойствами:
//    "ВидСертификации", "НомерСертификации" и "ДатаСертификации". Информацию по сертификатам следует добавить в соответствие
//     "СертификатыНоменклатуры".
// 
// Параметры:
//  Номенклатура - ОпределяемыйТип.Номенклатура - элемент номенклатура.
//  СертификатыНоменклатуры - Соответствие из Массив Структур, Ключ - номенклатура, свойство - структура:
//   * ВидСертификации - ПеречислениеСсылка.к2ВидыДокументовОбязательнойСертификацииИСМП - Вид сертификата.
//   * НомерСертификации - Строка - Идентификационный номер сертификата.
//   * ДатаСертификации - Дата - Дата начала действия сертификата.
Процедура ПриЗаполненииСертификатовНоменклатуры(СписокНоменклатуры, СертификатыНоменклатуры) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Номенклатура.ВидНоменклатуры КАК ВидНоменклатуры
	|ПОМЕСТИТЬ ВидыНоменклатуры
	|ИЗ
	|	Справочник.Номенклатура КАК Номенклатура
	|ГДЕ
	|	Номенклатура.Ссылка В(&СписокНоменклатуры)
	|
	|СГРУППИРОВАТЬ ПО
	|	Номенклатура.ВидНоменклатуры
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ОбластиДействияСертификатовНоменклатуры.Номенклатура КАК Номенклатура,
	|	СертификатыНоменклатуры.Бессрочный КАК Бессрочный,
	|	СертификатыНоменклатуры.ДатаОкончанияСрокаДействия КАК ДатаОкончанияСрокаДействия,
	|	СертификатыНоменклатуры.ДатаНачалаСрокаДействия КАК ДатаСертификации,
	|	СертификатыНоменклатуры.ТипСертификата КАК ТипСертификата,
	|	СертификатыНоменклатуры.Номер КАК НомерСертификации
	|ИЗ
	|	РегистрСведений.к2ОбластиДействияСертификатовНоменклатуры КАК ОбластиДействияСертификатовНоменклатуры
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.к2СертификатыНоменклатуры КАК СертификатыНоменклатуры
	|		ПО ОбластиДействияСертификатовНоменклатуры.СертификатНоменклатуры = СертификатыНоменклатуры.Ссылка
	|ГДЕ
	|	(ОбластиДействияСертификатовНоменклатуры.Номенклатура В (&СписокНоменклатуры)
	|			ИЛИ ОбластиДействияСертификатовНоменклатуры.ВидНоменклатуры В
	|					(ВЫБРАТЬ
	|						ВидыНоменклатуры.ВидНоменклатуры
	|					ИЗ
	|						ВидыНоменклатуры КАК ВидыНоменклатуры)
	|				И ОбластиДействияСертификатовНоменклатуры.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
	|
	|УПОРЯДОЧИТЬ ПО
	|	Бессрочный УБЫВ,
	|	ДатаОкончанияСрокаДействия УБЫВ";
	
	Запрос.УстановитьПараметр("СписокНоменклатуры", СписокНоменклатуры);
	
	ДанныеСертификатов = Запрос.Выполнить().Выгрузить();
	
	Для Каждого СтрокаДанных Из ДанныеСертификатов Цикл
		
		ВидСертификации = Неопределено;
		
		Если СтрокаДанных.ТипСертификата = "Сертификат соответствия" Тогда
			ВидСертификации = Перечисления.к2ВидыДокументовОбязательнойСертификацииИСМП.СертификатСоответствия;
		ИначеЕсли СтрокаДанных.ТипСертификата = "Декларация о соответствии" Тогда
			ВидСертификации = Перечисления.к2ВидыДокументовОбязательнойСертификацииИСМП.ДекларацияСоответствия;
		КонецЕсли;
		
		Если ВидСертификации = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		
		ИнформацияОСертификате = Новый Структура;
		ИнформацияОСертификате.Вставить("ВидСертификации",   ВидСертификации);
		ИнформацияОСертификате.Вставить("НомерСертификации", СтрокаДанных.НомерСертификации);
		ИнформацияОСертификате.Вставить("ДатаСертификации",  СтрокаДанных.ДатаСертификации);
		
		СертификатыЭлемента = СертификатыНоменклатуры.Получить(СтрокаДанных.Номенклатура);
		
		Если СертификатыЭлемента = Неопределено Тогда
			СертификатыЭлемента = Новый Массив;
			СертификатыЭлемента.Добавить(ИнформацияОСертификате);
			СертификатыНоменклатуры.Вставить(СтрокаДанных.Номенклатура, СертификатыЭлемента);
		Иначе
			СертификатыЭлемента.Добавить(ИнформацияОСертификате);
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ПараметрыИспользованияНоменклатуры

// В процедуре требуется определить признак использования характеристик 
//   * для номенклатуры, если передана номенклатура,
//   * в программе, если не передана.
//
// Параметры:
//  Использование - Булево - Признак использования характеристик (для номенклатуры или общий)
//  Номенклатура - ОпределяемыйТип.Номенклатура, Неопределено - Ссылка на элемент номенклатуры.
//
Процедура ИспользованиеХарактеристик(Использование, Номенклатура) Экспорт
	
	Если Номенклатура = Неопределено Тогда
		Использование = Истина;
	ИначеЕсли НЕ ЗначениеЗаполнено(Номенклатура) Тогда
		Использование = Ложь;
	Иначе
		Использование = НоменклатураСервер.ХарактеристикиИспользуются(Номенклатура);
	КонецЕсли;
	Возврат;
	
КонецПроцедуры

// В процедуре требуется определить признак использования серий
//   * для номенклатуры, если передана номенклатура,
//   * в программе, если не передана.
//
// Параметры:
//  Использование - Булево - Признак использования серий (для номенклатуры или общий)
//  Номенклатура - ОпределяемыйТип.Номенклатура, Неопределено - Ссылка на элемент номенклатуры.
//
Процедура ИспользованиеСерий(Использование, Номенклатура) Экспорт
	
	Если Номенклатура = Неопределено Тогда
		Использование = Истина;
	ИначеЕсли НЕ ЗначениеЗаполнено(Номенклатура) Тогда
		Использование = Ложь;
	Иначе
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
		Запрос.Текст = "
		|ВЫБРАТЬ
		|	Номенклатура.ВидНоменклатуры.ИспользоватьСерии КАК ПризнакИспользованияСерий
		|ИЗ
		|	Справочник.Номенклатура КАК Номенклатура
		|ГДЕ
		|	Номенклатура.Ссылка = &Номенклатура";
		
		Выборка = Запрос.Выполнить().Выбрать();
		Использование = Ложь;
		Если Выборка.Следующий() Тогда
			Использование = Выборка.ПризнакИспользованияСерий;
		КонецЕсли;
	КонецЕсли;
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ОрганизацииИКонтрагенты

// Заполняет представление руководителя организации.
//
// Параметры:
//  ПредставлениеРуководителя - (см. ИнтеграцияИС.ДанныеРуководителяОрганизации)
//  Организация - ОпределяемыйТип.ОрганизацияКонтрагентГосИС - ссылка на собственную организацию или контрагента,
//  ДатаСведений - Дата - дата, на которую требуется получить информацию.
Процедура ОпределитьДанныеРуководителяОрганизации(ПредставлениеРуководителя, Организация, ДатаСведений) Экспорт
	
	ДанныеРуководителя = к2ОтветственныеЛицаСервер.ПолучитьДанныеОтветственногоЛица(Организация, ДатаСведений);
	ПредставлениеРуководителя.Руководитель = Строка(ДанныеРуководителя.ФизическоеЛицо);
	ПредставлениеРуководителя.Должность = Строка(ДанныеРуководителя.Должность);
	Возврат;
	
КонецПроцедуры

// В процедуре необходимо сформировать соответствие по коллекции ИНН. Ключ - ИНН, значение - Контрагент.
//
// Параметры:
//  КоллекцияИНН - Массив - Список ИНН.
//  Соответствие - Соответствие - Соответсвие вида:
//   * ИНН
//   * Контрагент
Процедура ЗаполнитьСоответствиеИННКонтрагентам(КоллекцияИНН, Соответствие) Экспорт
	
	Если Не ПравоДоступа("Чтение", Метаданные.Справочники.Контрагенты) Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	Контрагенты.Ссылка КАК Контрагент,
	|	Контрагенты.ИНН    КАК ИНН
	|ИЗ
	|	Справочник.Контрагенты КАК Контрагенты
	|ГДЕ
	|	Контрагенты.ИНН В (&КоллекцияИНН)
	|");
	Запрос.УстановитьПараметр("КоллекцияИНН", КоллекцияИНН);

	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		Соответствие.Вставить(Выборка.ИНН, Выборка.Контрагент);
	КонецЦикла;
	Возврат;
	
КонецПроцедуры

// В процедуре необходимо сформировать соответствие по коллекции ИНН. Ключ - ИНН, значение - Организация.
//
// Параметры:
//  КоллекцияИНН - Массив - Список ИНН.
//  Соответствие - Соответствие - Соответсвие вида:
//   * ИНН
//   * Организация
Процедура ЗаполнитьСоответствиеИННОрганизациям(КоллекцияИНН, Соответствие) Экспорт
	
	Если Не ПравоДоступа("Чтение", Метаданные.Справочники.Организации) Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	Организации.Ссылка КАК Организация,
	|	Организации.ИНН    КАК ИНН
	|ИЗ
	|	Справочник.Организации КАК Организации
	|ГДЕ
	|	Организации.ИНН В (&КоллекцияИНН)
	|");
	Запрос.УстановитьПараметр("КоллекцияИНН", КоллекцияИНН);

	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		Соответствие.Вставить(Выборка.ИНН, Выборка.Организация);
	КонецЦикла;
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ДлительныеОперации

//Особенности работы с длительными операциями в конкретном окружении
//
//Параметры:
//   Параметры - Структура - параметры фонового задания.
//   ПараметрыВыполнения - см. ДлительныеОперации.ПараметрыВыполненияВФоне - параметры выполнения фонового задания.
Процедура НастроитьДлительнуюОперацию(Параметры, ПараметрыВыполнения) Экспорт
	
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ОбщегоНазначенияУТ

// Возвращает представление объекта.
// 
// Параметры:
//  Представление - Строка - Исходящий параметр со значением представления.
//  ПараметрОбъект - Строка, Объект - Наименование типа объекта либо сам объект.
Процедура УстановитьПредставлениеОбъекта(Представление, ПараметрОбъект) Экспорт
	
	Представление = ОбщегоНазначения.ПредставлениеОбъекта(ПараметрОбъект);
	Возврат;
	
КонецПроцедуры

// Заполняет представление строки номенклатуры.
//
// Параметры:
//  Представление  - Строка                                     - представление для заполнения,
//  Номенклатура   - ОпределяемыйТип.Номенклатура               - ссылка на номенклатуру,
//  Характеристика - ОпределяемыйТип.ХарактеристикаНоменклатуры - ссылка на характеристику номенклатуры,
//  Упаковка       - ОпределяемыйТип.Упаковка                   - ссылка на упаковку.
//  Серия          - ОпределяемыйТип.СерияНоменклатуры          - ссылка на серию номенклатуры.
Процедура ЗаполнитьПредставлениеНоменклатуры(Представление, Номенклатура, Характеристика, Упаковка, Серия) Экспорт
	
	Представление = к2ПечатьКлиентСервер.ПредставлениеНоменклатурыДляПечати(
		СокрЛП(Номенклатура),
		СокрЛП(Характеристика),
		Упаковка,
		Серия);
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ВызовыИзБСП

// (См. ОбщийМодуль.ПодключаемыеКомандыПереопределяемый.ПриОпределенииКомандПодключенныхКОбъекту)
//
Процедура ПриОпределенииКомандПодключенныхКОбъекту(НастройкиФормы, Источники, ПодключенныеОтчетыИОбработки, Команды) Экспорт
	
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти