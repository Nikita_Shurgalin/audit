
Функция ПользователюРазрешеноЗапускатьБП( пБП , пТекстОшибки = "", привилегированныйРежим = Ложь ) Экспорт
	
	Если глРеквизит_Кэш( пБП, "ЭтоГруппа" ) Тогда
		Возврат Ложь;
	КонецЕсли;
	
	лТекстОшибки = "";
	
	Если упСистемаБезопасности.ЕстьОграниченияНаЗапуск( пБП, лТекстОшибки, привилегированныйРежим ) Тогда
		
		шаблонОшибки = упИК.т__ЗапускБП_ОтказВЗапуске();
		
		пТекстОшибки = стрЗаполнить( шаблонОшибки, Справочники.упБизнесПроцессы.Представление( пБП ) , лТекстОшибки );
		
		упСистемаБезопасности.Лог( пТекстОшибки , пБП );
		
		Возврат Ложь;
		
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

Процедура ПроверитьВозможностьЗапускаВложенногоБП( пБП ) Экспорт
	
	упСистемаБезопасности.ПроверитьВозможностьЗапускаВложенногоБП( пБП );
	
КонецПроцедуры

Функция ПолучитьСтруктуруПравНаЗадачу( пЗадача, пЗаполнятьЛог = Ложь, пТекстЛога = "" ) Экспорт
	
	Возврат упСистемаБезопасности.ПолучитьСтруктуруПравНаЗадачу( пЗадача, пЗаполнятьЛог, пТекстЛога );
	
КонецФункции

Функция ПолучитьСтруктуруПравНаЗадачуПоСтруктуреЗадачи( структЗадача, пЗаполнятьЛог = Ложь, пТекстЛога = "" ) Экспорт
	
	Возврат упСистемаБезопасности.ПолучитьСтруктуруПравНаЗадачуПоСтруктуреЗадачи( структЗадача, пЗаполнятьЛог, пТекстЛога );
	
КонецФункции


Процедура ЗаполнитьПраваДоступаНаБП( пБП ) Экспорт
	
	упСистемаБезопасности.ЗаполнитьПраваДоступаНаБП( пБП );
	
КонецПроцедуры

Функция ТаблицаРазрешенийНаЗадачуПоБП( пБП ) Экспорт
	
	Возврат упСистемаБезопасности.ТаблицаРазрешенийНаЗадачуПоБП( пБП );
	
КонецФункции

Функция ТаблицаРазрешенийНаЗадачуПоПользователю( пПользователь ) Экспорт
	
	Возврат упСистемаБезопасности.ТаблицаРазрешенийНаЗадачуПоПользователю( пПользователь );
	
КонецФункции


Функция ПодчиненныеТекПользователя() Экспорт
	
	Возврат упСистемаБезопасности.ПолучитьМассивПодчиненныхАдресаций( глТекущийПользователь() );
	
КонецФункции
