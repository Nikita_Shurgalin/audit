
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПомещениеПриИзменении(Элемент)
	ПриИзмененииСоставляющихАдреса();
КонецПроцедуры

&НаКлиенте
Процедура РядПриИзменении(Элемент)
	ПриИзмененииСоставляющихАдреса();
КонецПроцедуры

&НаКлиенте
Процедура СтеллажПриИзменении(Элемент)
	ПриИзмененииСоставляющихАдреса();
КонецПроцедуры

&НаКлиенте
Процедура ЯрусПриИзменении(Элемент)
	ПриИзмененииСоставляющихАдреса();	
КонецПроцедуры

&НаКлиенте
Процедура СкладскоеПомещениеПриИзменении(Элемент)
	
	Объект.Помещение = глРеквизит(Объект.СкладскоеПомещение, "НомерПомещения");
	ПриИзмененииСоставляющихАдреса();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаКлиенте
Процедура ПриИзмененииСоставляющихАдреса()
	
	СформироватьНаименование();
	СформироватьАдрес(); 
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьНаименование()
	
	Объект.Наименование = СтрШаблон(НСтр("ru = '%1-%2-%3-%4'"), Объект.Помещение,
						  СтроковыеФункцииКлиентСервер.ДополнитьСтроку(Объект.Ряд, 2),
						  СтроковыеФункцииКлиентСервер.ДополнитьСтроку(Объект.Стеллаж, 3),
						  Объект.Ярус); 
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьАдрес()
	
	Объект.Код = СтрШаблон(НСтр("ru = '%1%2%3%4'"), Объект.Помещение,
						  СтроковыеФункцииКлиентСервер.ДополнитьСтроку(Объект.Ряд, 2),
						  СтроковыеФункцииКлиентСервер.ДополнитьСтроку(Объект.Стеллаж, 3),
						  Объект.Ярус); 

КонецПроцедуры

#КонецОбласти

#КонецОбласти
