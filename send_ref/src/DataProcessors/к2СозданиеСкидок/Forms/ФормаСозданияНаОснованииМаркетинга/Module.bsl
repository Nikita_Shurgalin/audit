
#Область ОписаниеПеременных

&НаКлиенте
Перем ЗакрытьФорму;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Параметры.Свойство("МаркетинговоеМероприятие", МаркетинговоеМероприятие);
	
	Если Не ЗначениеЗаполнено( МаркетинговоеМероприятие ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	НастроитьФорму();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения( ИмяСобытия, Параметр, Источник )
	
	Если ИмяСобытия = "ПодборСоглашенийПроизведен"
		И ЗначениеЗаполнено( Параметр )
		И Источник = УникальныйИдентификатор Тогда
		
		ЗагрузитьСоглашенияИзХранилища( Параметр );
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если ЗавершениеРаботы Тогда
		
		Отказ = Истина;
		
	ИначеЕсли Не ЗакрытьФорму = Истина Тогда 
		
		Отказ = Истина;
		
		ПоказатьВопросПередЗакрытием();
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТаблицаКонтрагентов

&НаКлиенте
Процедура ТаблицаКонтрагентовПриАктивизацииСтроки(Элемент)
	
	 ПриАктивизацииСтрокиКонтрагентов();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТаблицаНоменклатуры

&НаКлиенте
Процедура ТаблицаНоменклатурыАкционнаяЦенаПриИзменении(Элемент)
	
	ЦеновыеПоказателиПриИзменении();
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаНоменклатурыПроцентСкидкиПриИзменении(Элемент)
	
	ПроцентСкидкиПриИзменении();	
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСозданныеСкидки

&НаКлиенте
Процедура СозданныеСкидкиВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.СозданныеСкидки.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Поле = Элементы.СозданныеСкидкиЗначение Тогда
		ПоказатьЗначение(, ТекущиеДанные.Значение);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОформитьСкидки(Команда)
	
	СоздатьДокументыСкидкиНаценки();
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьСоглашение(Команда)
	
	ТекущиеДанные = Элементы.ТаблицаКонтрагентов.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ОткрытьФормуПодбораСоглашений(ТекущиеДанные.Организация, ТекущиеДанные.Контрагент);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область НастройкаФормы

&НаСервере
Процедура НастроитьФорму()
	
	ЗаполнитьЗначения();
	УстановитьУсловноеОформление();
		
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();	
	
	к2УО.ЕдиницаИзмерения(ЭтотОбъект, "ТаблицаНоменклатурыЕдиницаИзмерения", "ТаблицаНоменклатуры.Упаковка");
	к2УО.Характеристика(ЭтотОбъект, "ТаблицаНоменклатурыХарактеристика", "ТаблицаНоменклатуры.ХарактеристикиИспользуются");
	
КонецПроцедуры

#КонецОбласти

#Область Заполнение

&НаСервере
Процедура ЗаполнитьЗначения()
	
	СпособОформленияСкидки = Перечисления.к2ТипыСкидокНаценок.ПроцентомНаНоменклатуру;
	
	РезультатЗапроса = ДанныеДляЗаполненияТаблиц();
	ЗаполнитьТаблицы(РезультатЗапроса);	
		
КонецПроцедуры

&НаСервере
Функция ДанныеДляЗаполненияТаблиц()
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	к2МаркетинговоеМероприятие.Ссылка КАК Ссылка,
		|	к2МаркетинговоеМероприятие.Организация КАК Организация
		|ПОМЕСТИТЬ МаркетинговоеМероприятие
		|ИЗ
		|	Документ.к2МаркетинговоеМероприятие КАК к2МаркетинговоеМероприятие
		|ГДЕ
		|	к2МаркетинговоеМероприятие.Ссылка = &Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	к2МаркетинговоеМероприятиеКонтрагенты.Контрагент КАК Контрагент,
		|	МаркетинговоеМероприятие.Организация КАК Организация
		|ИЗ
		|	МаркетинговоеМероприятие КАК МаркетинговоеМероприятие
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.к2МаркетинговоеМероприятие.Контрагенты КАК к2МаркетинговоеМероприятиеКонтрагенты
		|		ПО МаркетинговоеМероприятие.Ссылка = к2МаркетинговоеМероприятиеКонтрагенты.Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	к2МаркетинговоеМероприятиеПоказатели.Контрагент КАК Контрагент,
		|	к2МаркетинговоеМероприятиеПоказатели.ОтпускнаяЦена КАК ОтпускнаяЦена,
		|	к2МаркетинговоеМероприятиеПоказатели.АкционнаяЦена КАК АкционнаяЦена,
		|	к2МаркетинговоеМероприятиеПоказатели.ПроцентСкидки КАК ПроцентСкидки,
		|	к2МаркетинговоеМероприятиеПоказатели.Номенклатура КАК Номенклатура,
		|	к2МаркетинговоеМероприятиеПоказатели.Характеристика КАК Характеристика,
		|	к2МаркетинговоеМероприятиеПоказатели.Номенклатура.ВидНоменклатуры.ИспользоватьХарактеристики КАК ХарактеристикиИспользуются,
		|	к2МаркетинговоеМероприятиеПоказатели.ЕдиницаИзмерения КАК Упаковка
		|ИЗ
		|	МаркетинговоеМероприятие КАК МаркетинговоеМероприятие
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.к2МаркетинговоеМероприятие.Показатели КАК к2МаркетинговоеМероприятиеПоказатели
		|		ПО МаркетинговоеМероприятие.Ссылка = к2МаркетинговоеМероприятиеПоказатели.Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	к2СкидкаНаценка.Ссылка КАК Ссылка
		|ИЗ
		|	Документ.к2СкидкаНаценка КАК к2СкидкаНаценка
		|ГДЕ
		|	к2СкидкаНаценка.Основание = &Ссылка
		|	И НЕ к2СкидкаНаценка.ПометкаУдаления";	
	#КонецОбласти
	
	Запрос.УстановитьПараметр("Ссылка", МаркетинговоеМероприятие);
	
	Возврат Запрос.ВыполнитьПакет();
		
КонецФункции

&НаСервере
Процедура ЗаполнитьТаблицы(РезультатЗапроса)
	
	Номенклатуры = РезультатЗапроса[РезультатЗапроса.ВГраница() - 1].Выгрузить();
	Контрагенты = РезультатЗапроса[РезультатЗапроса.ВГраница() - 2].Выгрузить();
	Скидки = РезультатЗапроса[РезультатЗапроса.ВГраница()].Выгрузить().ВыгрузитьКолонку("Ссылка"); 
	
	ТаблицаНоменклатуры.Загрузить(Номенклатуры);
	ТаблицаКонтрагентов.Загрузить(Контрагенты);
	СозданныеСкидки.ЗагрузитьЗначения(Скидки);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиенте
Процедура ПриАктивизацииСтрокиКонтрагентов()
	
	ПодключитьОбработчикОжидания("ПриАктивизацииСтрокиКонтрагентовЗавершение", 0.5, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриАктивизацииСтрокиКонтрагентовЗавершение()
	
	ТекущиеДанные = Элементы.ТаблицаКонтрагентов.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьОтборДляТаблицыНоменклатуры(ТекущиеДанные);
	УстановитьОтборДляТаблицыСоглашений(ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборДляТаблицыНоменклатуры(ТекущиеДанные)
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Контрагент", ТекущиеДанные.Контрагент);
	
	Элементы.ТаблицаНоменклатуры.ОтборСтрок = Новый ФиксированнаяСтруктура(СтруктураОтбора);	
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборДляТаблицыСоглашений(ТекущиеДанные)
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Организация", ТекущиеДанные.Организация);
	СтруктураОтбора.Вставить("Контрагент", ТекущиеДанные.Контрагент);
	
	Элементы.ТаблицаСоглашений.ОтборСтрок = Новый ФиксированнаяСтруктура(СтруктураОтбора);		
	
КонецПроцедуры

&НаКлиенте
Процедура ЦеновыеПоказателиПриИзменении()
	
	ТекущиеДанные = Элементы.ТаблицаНоменклатуры.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.ПроцентСкидки = 100 * (1 - к2КоллекцииКлиентСервер.Разделить(ТекущиеДанные.АкционнаяЦена, ТекущиеДанные.ОтпускнаяЦена));
	
КонецПроцедуры

&НаКлиенте
Процедура ПроцентСкидкиПриИзменении()
	
	ТекущиеДанные = Элементы.ТаблицаНоменклатуры.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.АкционнаяЦена = ТекущиеДанные.ОтпускнаяЦена * (1 - ТекущиеДанные.ПроцентСкидки / 100);
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьДокументыСкидкиНаценки()
	
	СтруктураВозврата = ПроверитьЗаполнениеСоглашений();
	
	Если СтруктураВозврата.Отказ Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю( СтруктураВозврата.ТекстОшибки );
		Возврат;
	КонецЕсли;
	
	СоздатьДокументыСкидкиНаценкиНаСервере();	
	
КонецПроцедуры

&НаКлиенте
Функция ПроверитьЗаполнениеСоглашений()
	
	СтруктураВозврата = ПустаяСтруктураВозвратаПроверкиСоглашений();
	СтруктураОтбора = Новый Структура("Контрагент, Организация");
	
	Для Каждого Строка Из ТаблицаКонтрагентов Цикл
		
		ЗаполнитьЗначенияСвойств(СтруктураОтбора, Строка);
		НайденныеСтроки = ТаблицаСоглашений.НайтиСтроки(СтруктураОтбора);
		
		Если НайденныеСтроки.Количество() > 0 Тогда
			
			УточнятьЦенуДоХарактеристик = НайденныеСтроки[0].УточнятьЦенуДоХарактеристик;
			
			Для Каждого СтрокаСоглашения Из НайденныеСтроки Цикл
				
				Если Не СтрокаСоглашения.УточнятьЦенуДоХарактеристик = УточнятьЦенуДоХарактеристик Тогда				
					ТекстСообщения = НСтр( "ru = 'Для Контрагента %1 признаки уточнения цены различаются.'" );	
					СформироватьОшибку(ТекстСообщения, Строка.Контрагент, СтруктураВозврата);
					Прервать;
				КонецЕсли;
				
			КонецЦикла;
			
		Иначе
			
			ТекстСообщения = НСтр( "ru = 'Для Контрагента %1 не выбраны соглашения для скидки.'" );
			СформироватьОшибку(ТекстСообщения, Строка.Контрагент, СтруктураВозврата);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат СтруктураВозврата;
	
КонецФункции

&НаКлиенте
Процедура СформироватьОшибку(ТекстСообщения, Контрагент, СтруктураВозврата)
	
	СтруктураВозврата.Отказ = Истина;	
	ТекстОшибки            = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку( ТекстСообщения, Контрагент );
	
	МассивСтрок            = Новый Массив;
	МассивСтрок.Добавить( СтруктураВозврата.ТекстОшибки );
	МассивСтрок.Добавить( ТекстОшибки );
	
	СтруктураВозврата.ТекстОшибки = СтрСоединить( МассивСтрок, Символы.ПС );	
		
КонецПроцедуры

&НаСервере
Процедура СоздатьДокументыСкидкиНаценкиНаСервере()
	
	СтруктураПоискаСоглашений = Новый Структура("Контрагент, Организация");
	СтруктураПоискаНоменклатуры = Новый Структура("Контрагент");
	
	ТекстСообщения = "";
	ШаблонСообщения = НСтр( "ru = 'Создан документ %1'" );
	
	Для Каждого СтрокаКонтрагентов Из ТаблицаКонтрагентов Цикл
		
		ДокОбъект = Документы.к2СкидкаНаценка.СоздатьДокумент();
		ДокОбъект.Заполнить(МаркетинговоеМероприятие);
		ДокОбъект.Дата = ТекущаяДатаСеанса();
		ДокОбъект.Тип = СпособОформленияСкидки;
		ДокОбъект.СтатусСогласования = Перечисления.к2СтатусыСогласованияСкидкиНаценки.Согласован;
		
		ЗаполнитьЗначенияСвойств(СтруктураПоискаСоглашений, СтрокаКонтрагентов);
		ЗаполнитьЗначенияСвойств(СтруктураПоискаНоменклатуры, СтрокаКонтрагентов);
		
		НайденныеСтрокиСоглашений = ТаблицаСоглашений.НайтиСтроки(СтруктураПоискаСоглашений);
		
		Если НайденныеСтрокиСоглашений.Количество() > 0 Тогда
			ДокОбъект.УточнятьЦенуДоХарактеристик = НайденныеСтрокиСоглашений[0].УточнятьЦенуДоХарактеристик;
		КонецЕсли;
		
		Для Каждого СтрокаСоглашения Из НайденныеСтрокиСоглашений Цикл
			
			НоваяСтрока = ДокОбъект.Соглашения.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаСоглашения);
			
		КонецЦикла;
		
		НайденныеСтрокиНоменклатуры = ТаблицаНоменклатуры.НайтиСтроки(СтруктураПоискаНоменклатуры);
		
		Для Каждого СтрокаНоменклатуры Из НайденныеСтрокиНоменклатуры Цикл
			
			НоваяСтрокаНоменклатуры = ДокОбъект.Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаНоменклатуры, СтрокаНоменклатуры);
			НоваяСтрокаНоменклатуры.Цена = СтрокаНоменклатуры.АкционнаяЦена;
			
		КонецЦикла;
		
		к2ПроведениеСервер.ПровестиИлиЗаписатьДокумент(ДокОбъект, , , Истина);
		
		Сообщение = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ДокОбъект.Ссылка);
		
		МассивСтрок = Новый Массив;
		МассивСтрок.Добавить(ТекстСообщения);
		МассивСтрок.Добавить(Сообщение);
		
		ТекстСообщения = СтрСоединить(МассивСтрок, Символы.ПС);
		
		СозданныеСкидки.Добавить(ДокОбъект.Ссылка);
		
	КонецЦикла;
	
	Если ЗначениеЗаполнено(ТекстСообщения) Тогда
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПустаяСтруктураВозвратаПроверкиСоглашений()
	
	СтруктураВозврата = Новый Структура;
	СтруктураВозврата.Вставить("Отказ", Ложь);
	СтруктураВозврата.Вставить("ТекстОшибки", "");
	
	Возврат СтруктураВозврата;
	
КонецФункции

#КонецОбласти

#Область Подбор

&НаКлиенте
Процедура ОткрытьФормуПодбораСоглашений(Организация, Контрагент)
	
	ПараметрыОтбора = Новый Структура;
	ПараметрыОтбора.Вставить( "Организация", Организация );
	ПараметрыОтбора.Вставить( "Контрагент", Контрагент );
	
	ПараметрыПодбора = Новый Структура;
	ПараметрыПодбора.Вставить( "УникальныйИдентификаторФормыВладельца", УникальныйИдентификатор );
	ПараметрыПодбора.Вставить( "АдресПодобранныхСоглашений", АдресПодобранныхСоглашений( ПараметрыОтбора ) );
	ПараметрыПодбора.Вставить( "Отбор", ПараметрыОтбора );
	
	ОткрытьФорму( "Справочник.к2СоглашенияСКлиентами.Форма.ФормаПодбора",
				  ПараметрыПодбора,
				  ЭтотОбъект,
				  ,
				  ,
				  ,
				  ,
				  РежимОткрытияОкнаФормы.БлокироватьОкноВладельца );
	
КонецПроцедуры

&НаСервере
Функция АдресПодобранныхСоглашений(ПараметрыОтбора)
	
	Возврат ПоместитьВоВременноеХранилище( ТаблицаСоглашений.Выгрузить( ПараметрыОтбора ) );
	
КонецФункции

&НаСервере
Процедура ЗагрузитьСоглашенияИзХранилища(Адрес)
	
	Если Не ЗначениеЗаполнено( Адрес ) Тогда
		Возврат;
	КонецЕсли;
	
	тзСоглашения = ПолучитьИзВременногоХранилища(Адрес);
	
	СтруктураПоиска = Новый Структура("Контрагент,Организация,Соглашение");
	
	Для Каждого цСтрока Из тзСоглашения Цикл
		
		ЗаполнитьЗначенияСвойств(СтруктураПоиска, цСтрока);
		
		НайденныеСтроки = ТаблицаСоглашений.НайтиСтроки(СтруктураПоиска);
		
		Если НайденныеСтроки.Количество() = 0 Тогда
			НоваяСтрока = ТаблицаСоглашений.Добавить();
			ЗаполнитьЗначенияСвойств( НоваяСтрока, цСтрока );
		КонецЕсли;	
			
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ЗакрытиеФормы

&НаКлиенте
Процедура ПоказатьВопросПередЗакрытием()

	Оповещение = Новый ОписаниеОповещения("ПоказатьВопросПередЗакрытием_Завершение", ЭтотОбъект);

	ПоказатьВопрос(Оповещение, НСтр("ru = 'Закрыть форму?'"), РежимДиалогаВопрос.ДаНет, , , НСтр("ru = 'Закрытие'"));

КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВопросПередЗакрытием_Завершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если Не РезультатВопроса = КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	ЗакрытьФорму();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗакрытьФорму()
	
	ЗакрытьФорму = Истина;
	
	Закрыть();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
