#Область ПрограммныйИнтерфейс

Функция ТекстЗапросаПлан() Экспорт 
	
	Возврат  "ВЫБРАТЬ РАЗЛИЧНЫЕ
	         |	к2ГруппыПартнеровПланирования.Ссылка КАК ГруппаПартнеров,
	         |	к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.ТочкаДоставки КАК ТочкаДоставки,
	         |	к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.Менеджер КАК Менеджер,
	         |	к2ОтделыПродажМенеджеры.Ссылка КАК Отдел
	         |ПОМЕСТИТЬ ТаблицаОтветственных
	         |ИЗ
	         |	Справочник.к2ГруппыПартнеровПланирования КАК к2ГруппыПартнеровПланирования
	         |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ИсторияИзмененияМенеджеровТочекДоставки.СрезПоследних КАК к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних
	         |			ЛЕВОЕ СОЕДИНЕНИЕ Справочник.к2ОтделыПродаж.Менеджеры КАК к2ОтделыПродажМенеджеры
	         |			ПО (к2ОтделыПродажМенеджеры.Менеджер = к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.Менеджер)
	         |		ПО (к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.ТочкаДоставки.Владелец.ГруппаПланирования = к2ГруппыПартнеровПланирования.Ссылка)
	         |ГДЕ
	         |	НЕ к2ГруппыПартнеровПланирования.ПометкаУдаления
	         |;
	         |
	         |////////////////////////////////////////////////////////////////////////////////
	         |ВЫБРАТЬ
	         |	ТаблицаОтветственных.ГруппаПартнеров КАК ГруппаПартнеров,
	         |	ТаблицаОтветственных.ТочкаДоставки КАК ТочкаДоставки,
	         |	ТаблицаОтветственных.Менеджер КАК Менеджер,
	         |	ТаблицаОтветственных.Отдел КАК Отдел,
	         |	к2ЗоныОтветственностиОтделовПродаж.ТоварнаяКатегория КАК ТоварнаяКатегория
	         |ПОМЕСТИТЬ РазрезыПланирования
	         |ИЗ
	         |	ТаблицаОтветственных КАК ТаблицаОтветственных
	         |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ЗоныОтветственностиОтделовПродаж КАК к2ЗоныОтветственностиОтделовПродаж
	         |		ПО (к2ЗоныОтветственностиОтделовПродаж.ГруппаПланирования = ТаблицаОтветственных.ГруппаПартнеров)
	         |			И (к2ЗоныОтветственностиОтделовПродаж.ОтделПродаж = ТаблицаОтветственных.Отдел)
	         |ГДЕ
	         |	&ОтборПоГруппамПланирования
	         |	И НЕ ТаблицаОтветственных.ГруппаПартнеров.ПометкаУдаления
	         |	И &ОтборПоТоварнымКатегориям
	         |	И НЕ к2ЗоныОтветственностиОтделовПродаж.ТоварнаяКатегория.ПометкаУдаления
	         |;
	         |
	         |////////////////////////////////////////////////////////////////////////////////
	         |ВЫБРАТЬ
	         |	РазрезыПланирования.ГруппаПартнеров КАК ГруппаПартнеров,
	         |	РазрезыПланирования.ТочкаДоставки КАК ТочкаДоставки,
	         |	РазрезыПланирования.Менеджер КАК Менеджер,
	         |	РазрезыПланирования.Отдел КАК ОтделПродаж,
	         |	СУММА(ЕСТЬNULL(к2ПланыПродажОбороты.КоличествоОборот, 0)) КАК КоличествоПлан,
	         |	РазрезыПланирования.ТоварнаяКатегория КАК ТоварнаяКатегория,
	         |	СУММА(ЕСТЬNULL(к2ЕмкостьТочекДоставкиСрезПоследних.Емкость, 0)) КАК Емкость
	         |ИЗ
	         |	РазрезыПланирования КАК РазрезыПланирования
	         |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.к2ПланыПродаж.Обороты(
	         |				,
	         |				,
	         |				,
	         |				Сценарий = &Сценарий
	         |					И ПериодПланирования = &ПериодПланирования) КАК к2ПланыПродажОбороты
	         |		ПО (к2ПланыПродажОбороты.ТочкаДоставки = РазрезыПланирования.ТочкаДоставки)
	         |			И (к2ПланыПродажОбороты.ГруппаПартнеров = РазрезыПланирования.ГруппаПартнеров)
	         |			И РазрезыПланирования.ТоварнаяКатегория = к2ПланыПродажОбороты.ТоварнаяКатегория
	         |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ЕмкостьТочекДоставки.СрезПоследних(, ) КАК к2ЕмкостьТочекДоставкиСрезПоследних
	         |		ПО РазрезыПланирования.ТочкаДоставки = к2ЕмкостьТочекДоставкиСрезПоследних.ТочкаДоставки
	         |			И РазрезыПланирования.ТоварнаяКатегория.ВидЕмкости = к2ЕмкостьТочекДоставкиСрезПоследних.ВидЕмкости
	         |
	         |СГРУППИРОВАТЬ ПО
	         |	РазрезыПланирования.ГруппаПартнеров,
	         |	РазрезыПланирования.Менеджер,
	         |	РазрезыПланирования.ТочкаДоставки,
	         |	РазрезыПланирования.Отдел,
	         |	РазрезыПланирования.ТоварнаяКатегория";
КонецФункции

Функция ТекстЗапросаЦель() Экспорт 
	
	Возврат  "ВЫБРАТЬ РАЗЛИЧНЫЕ
	         |	к2ГруппыПартнеровПланирования.Ссылка КАК ГруппаПартнеров,
	         |	к2ТоварныеКатегории.Ссылка КАК ТоварнаяКатегория
	         |ПОМЕСТИТЬ РазрезыПланирования
	         |ИЗ
	         |	Справочник.к2ГруппыПартнеровПланирования КАК к2ГруппыПартнеровПланирования,
	         |	Справочник.к2ТоварныеКатегории КАК к2ТоварныеКатегории
	         |ГДЕ
	         |	&ОтборПоГруппамПланирования
	         |	И НЕ к2ГруппыПартнеровПланирования.ПометкаУдаления
	         |	И &ОтборПоТоварнымКатегориям
	         |	И НЕ к2ТоварныеКатегории.ПометкаУдаления
	         |;
	         |
	         |////////////////////////////////////////////////////////////////////////////////
	         |ВЫБРАТЬ
	         |	РазрезыПланирования.ГруппаПартнеров КАК ГруппаПартнеров,
	         |	СУММА(ЕСТЬNULL(к2ПланыПродажОбороты.КоличествоОборот, 0)) КАК КоличествоЦель,
	         |	РазрезыПланирования.ТоварнаяКатегория КАК ТоварнаяКатегория
	         |ИЗ
	         |	РазрезыПланирования КАК РазрезыПланирования
	         |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.к2ПланыПродаж.Обороты(
	         |				,
	         |				,
	         |				,
	         |				Сценарий = &ЦелевойСценарий
	         |					И ПериодПланирования = &ПериодПланирования) КАК к2ПланыПродажОбороты
	         |		ПО (к2ПланыПродажОбороты.ГруппаПартнеров = РазрезыПланирования.ГруппаПартнеров)
	         |			И РазрезыПланирования.ТоварнаяКатегория = к2ПланыПродажОбороты.ТоварнаяКатегория
	         |
	         |СГРУППИРОВАТЬ ПО
	         |	РазрезыПланирования.ГруппаПартнеров,
	         |	РазрезыПланирования.ТоварнаяКатегория";
КонецФункции

Функция ТекстЗапросаГруппировки() Экспорт 
	
	Возврат  "ВЫБРАТЬ РАЗЛИЧНЫЕ
	         |	к2ГруппыПартнеровПланирования.Ссылка КАК ГруппаПартнеров,
	         |	к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.ТочкаДоставки КАК ТочкаДоставки,
	         |	к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.Менеджер КАК Менеджер,
	         |	к2ОтделыПродажМенеджеры.Ссылка КАК Отдел
	         |ПОМЕСТИТЬ РазрезыПланирования
	         |ИЗ
	         |	Справочник.к2ГруппыПартнеровПланирования КАК к2ГруппыПартнеровПланирования
	         |		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ИсторияИзмененияМенеджеровТочекДоставки.СрезПоследних КАК к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних
	         |			ЛЕВОЕ СОЕДИНЕНИЕ Справочник.к2ОтделыПродаж.Менеджеры КАК к2ОтделыПродажМенеджеры
	         |			ПО (к2ОтделыПродажМенеджеры.Менеджер = к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.Менеджер)
	         |		ПО (к2ИсторияИзмененияМенеджеровТочекДоставкиСрезПоследних.ТочкаДоставки.Владелец.ГруппаПланирования = к2ГруппыПартнеровПланирования.Ссылка)
	         |ГДЕ
	         |	&ОтборПоГруппамПланирования
	         |	И НЕ к2ГруппыПартнеровПланирования.ПометкаУдаления
	         |;
	         |
	         |////////////////////////////////////////////////////////////////////////////////
	         |ВЫБРАТЬ
	         |	РазрезыПланирования.ГруппаПартнеров КАК ГруппаПартнеров,
	         |	РазрезыПланирования.ТочкаДоставки КАК ТочкаДоставки,
	         |	РазрезыПланирования.Менеджер КАК Менеджер,
	         |	РазрезыПланирования.Отдел КАК ОтделПродаж
	         |ИЗ
	         |	РазрезыПланирования КАК РазрезыПланирования
	         |
	         |СГРУППИРОВАТЬ ПО
	         |	РазрезыПланирования.ГруппаПартнеров,
	         |	РазрезыПланирования.Менеджер,
	         |	РазрезыПланирования.ТочкаДоставки,
	         |	РазрезыПланирования.Отдел
	         |ИТОГИ ПО
	         |	ГруппаПартнеров,
	         |	ОтделПродаж,
	         |	Менеджер";
	
КонецФункции

#КонецОбласти
