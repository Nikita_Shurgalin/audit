
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьЗначенияПоУмолчанию();
	
	ПостроитьДеревоОператоров();
	
	НастроитьДекорациюПоШаблону();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура НаименованиеГИСМПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Запись.НаименованиеПродукции) Тогда
		
		Запись.ВидПродукции = глРеквизит(Запись.НаименованиеПродукции, "Владелец");
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НаименованиеГИСМНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("РежимВыбора", Истина);
	ПараметрыФормы.Вставить("ЗакрыватьПриВыборе", Истина);
	
	Если ЗначениеЗаполнено(Запись.ВидПродукции) Тогда
		
		СтруктураОтбор = Новый Структура;
		СтруктураОтбор.Вставить("Владелец", Запись.ВидПродукции);
		ПараметрыФормы.Вставить("Отбор", СтруктураОтбор);
		
	КонецЕсли;
	
	ОткрытьФорму("Справочник.к2НаименованияПродукции_Меркурий.ФормаВыбора"
		, ПараметрыФормы
		, Элемент);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаРедактироватьШаблонИдентификатора(Команда)
	
	ОткрытьРедакторФормул();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	Если ТипЗнч(Параметры.ЗначенияЗаполнения) = Тип("Структура")
		И Параметры.ЗначенияЗаполнения.Свойство("НаименованиеПродукции") Тогда 
		
		Запись.ВидПродукции = глРеквизит(Запись.НаименованиеПродукции, "Владелец");
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПостроитьДеревоОператоров()
	
	Дерево = к2РаботаСФормулами.ПолучитьПустоеДеревоОператоров();
	
	ГруппаОператоров = к2РаботаСФормулами.ДобавитьГруппуОператоров(Дерево, НСтр("ru='Разделители'"));
	
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "/", " + ""/"" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "\", " + ""\"" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "|", " + ""|"" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "_", " + ""_"" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, ",", " + "", "" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, ".", " + "". "" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Пробел'"), " + "" "" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "(", " + "" ("" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, ")", " + "") "" + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, """", " + """""""" + ");
	
	ГруппаОператоров = к2РаботаСФормулами.ДобавитьГруппуОператоров(Дерево, НСтр("ru='Операторы'"));
	
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "+", " + ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "-", " - ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "*", " * ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "/", " / ");
	
	ГруппаОператоров = к2РаботаСФормулами.ДобавитьГруппуОператоров(Дерево, НСтр("ru='Логические операторы и константы'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "<", " < ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, ">", " > ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "<=", " <= ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, ">=", " >= ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "=", " = ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, "<>", " <> ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='И'"),      " " + НСтр("ru='И'")      + " ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Или'"),    " " + НСтр("ru='Или'")    + " ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Не'"),     " " + НСтр("ru='Не'")     + " ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='ИСТИНА'"), " " + НСтр("ru='ИСТИНА'") + " ");
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='ЛОЖЬ'"),   " " + НСтр("ru='ЛОЖЬ'")   + " ");
	
	ГруппаОператоров = к2РаботаСФормулами.ДобавитьГруппуОператоров(Дерево, НСтр("ru='Числовые функции'"));
	
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Максимум'"),    НСтр("ru='Макс(,)'"), 2);
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Минимум'"),     НСтр("ru='Мин(,)'"),  2);
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Округление'"),  НСтр("ru='Окр(,)'"),  2);
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Целая часть'"), НСтр("ru='Цел()'"),   1);
	
	ГруппаОператоров = к2РаботаСФормулами.ДобавитьГруппуОператоров(Дерево, НСтр("ru='Строковые функции'"));
	
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Строка'"), НСтр("ru='Строка()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='ВРег'"), НСтр("ru='ВРег()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Лев'"), НСтр("ru='Лев()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='НРег'"), НСтр("ru='НРег()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Прав'"), НСтр("ru='Прав()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='СокрЛ'"), НСтр("ru='СокрЛ()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='СокрЛП'"), НСтр("ru='СокрЛП()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='СокрП'"), НСтр("ru='СокрП()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='ТРег'"), НСтр("ru='ТРег()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='СтрЗаменить'"), НСтр("ru='СтрЗаменить(,,)'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='СтрДлина'"), НСтр("ru='СтрДлина()'"));
	
	ГруппаОператоров = к2РаботаСФормулами.ДобавитьГруппуОператоров(Дерево, НСтр("ru='Прочие функции'"));
	
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Условие'"), "?(,,)", 3);
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Предопределенное значение'"), НСтр("ru='ПредопределенноеЗначение()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Значение заполнено'"), НСтр("ru='ЗначениеЗаполнено()'"));
	к2РаботаСФормулами.ДобавитьОператор(Дерево, ГруппаОператоров, НСтр("ru='Формат'"), НСтр("ru='Формат(,)'"));
	
	АдресХранилищаДереваОператоров = ПоместитьВоВременноеХранилище(Дерево, УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьРедакторФормул()
	
	НазначениеФормулы = НазначениеФормулы();
	
	ПараметрыШаблона = Новый Структура;
	ПараметрыШаблона.Вставить("Формула", Запись.ШаблонИдентификатора);
	ПараметрыШаблона.Вставить("ОперандыЗаголовок", НСтр("ru = 'Доступные реквизиты'"));
	ПараметрыШаблона.Вставить("Операторы", АдресХранилищаДереваОператоров);
	ПараметрыШаблона.Вставить("СтроковаяФормула", Истина);
	ПараметрыШаблона.Вставить("НазначениеФормулы", НазначениеФормулы);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьРедакторФормулЗавершение", ЭтотОбъект);
	
	ОткрытьФорму("ОбщаяФорма.к2КонструкторФормул"
		, ПараметрыШаблона
		, ЭтотОбъект
		,
		,
		,
		, ОписаниеОповещения
		, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция НазначениеФормулы()
	
	Возврат Метаданные.Справочники.к2ПартииГИСМ.Имя;
	
КонецФункции

&НаКлиенте
Процедура ОткрытьРедакторФормулЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Шаблон = Результат;
	
	Если Шаблон = Неопределено Тогда 
		Возврат;
	КонецЕсли;
	
	Если Не Запись.ШаблонИдентификатора = Шаблон Тогда
		
		Модифицированность = Истина;
		
		Запись.ШаблонИдентификатора = Шаблон;
		
		НастроитьДекорациюПоШаблону();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура НастроитьДекорациюПоШаблону()
	
	Если ПустаяСтрока(Запись.ШаблонИдентификатора) Тогда
		
		Элементы.ДекорацияШаблонИдентификатора.Заголовок = НСтр("ru = '<шаблон не задан>'");
		Элементы.ДекорацияШаблонИдентификатора.ЦветТекста = ЦветаСтиля.ТекстЗапрещеннойЯчейкиЦвет;
		
	Иначе 
		
		Элементы.ДекорацияШаблонИдентификатора.Заголовок = Запись.ШаблонИдентификатора;
		Элементы.ДекорацияШаблонИдентификатора.ЦветТекста = ЦветаСтиля.ЦветТекстаФормы; 
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
