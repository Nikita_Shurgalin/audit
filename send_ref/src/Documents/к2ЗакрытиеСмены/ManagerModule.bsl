#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	// Создание запроса инициализации движений и заполнение его параметров.
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблица_к2СтатусыСмены(Запрос, ТекстыЗапроса, Регистры);
	
	// Исполнение запроса и выгрузка полученных таблиц для движений.
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	СтруктураИменПараметров = Новый Структура;
	СтруктураИменПараметров.Вставить("Ссылка");
	СтруктураИменПараметров.Вставить("Дата");
	СтруктураИменПараметров.Вставить("ДатаСмены");
	СтруктураИменПараметров.Вставить("Смена");
	СтруктураИменПараметров.Вставить("РабочийЦентр");
	СтруктураИменПараметров.Вставить("СтатусСмены");
	
	Параметры = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДокументСсылка, СтруктураИменПараметров);
	
	Для Каждого Параметр Из Параметры Цикл
		
		Запрос.УстановитьПараметр(Параметр.Ключ, Параметр.Значение);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2СтатусыСмены(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2СтатусыСмены";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	&ДатаСмены КАК ДатаСмены,
	|	&Смена КАК Смена,
	|	&РабочийЦентр КАК РабочийЦентр,
	|	&СтатусСмены КАК СтатусСмены";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

КонецПроцедуры

#КонецОбласти

#Область СозданиеНаОсновании

// Определяет список команд создания на основании.
//
// Параметры:
//   КомандыСозданияНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры к2СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры к2СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСоздатьНаОсновании, Параметры) Экспорт
	
	Документы.к2АктПереработки.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	
КонецПроцедуры

#КонецОбласти

Функция ПодчиненныйДокумент(ЗакрытиеСмены) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ЗакрытиеСмены", ЗакрытиеСмены);
	
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	к2АктПереработки.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.к2АктПереработки КАК к2АктПереработки
	|ГДЕ
	|	к2АктПереработки.Основание = &ЗакрытиеСмены
	|	И НЕ к2АктПереработки.Основание = ЗНАЧЕНИЕ(Документ.к2ЗакрытиеСмены.ПустаяСсылка)";
	
	Возврат к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).Ссылка;
	
КонецФункции

#КонецОбласти

#КонецЕсли
