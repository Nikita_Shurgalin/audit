
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Журнал
		, "ПакетДанныхУИД"
		, Запись.ПакетДанныхУИД
		,
		,
		, Истина);
	
КонецПроцедуры

#КонецОбласти
