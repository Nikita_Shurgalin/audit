
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ПараметрыФормы = Новый Структура("Отбор", Новый Структура( "Адресация", МассивГруппПользователя( ПараметрКоманды ) ) );
	ОткрытьФорму("РегистрСведений.упРазрешенияНаЗадачи.ФормаСписка", ПараметрыФормы, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность, ПараметрыВыполненияКоманды.Окно);
	
КонецПроцедуры

&НаСервере
Функция МассивГруппПользователя( пПользователь )
	
	Возврат упПользователи.ПолучитьМассивГруппПользователя( пПользователь );
	
КонецФункции

