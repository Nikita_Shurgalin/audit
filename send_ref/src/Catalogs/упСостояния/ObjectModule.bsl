#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("СтрокаТаблицыЗначений") Тогда
		
		// заполняем все необходимые данные и записываем объект
		глПрисвоить( Наименование , ДанныеЗаполнения.Наименование );
		глПрисвоить( Имя , ДанныеЗаполнения.Имя );
		глПрисвоить( Владелец , ДанныеЗаполнения.ВерсияБизнесПроцесса );
		глПрисвоить( ТипСостояния , Перечисления.упТипыСостояний.ПолучитьТипПоЭлементуСхемы( ДанныеЗаполнения.ТипЭлемента ) );
		глПрисвоить( ПометкаУдаления , Ложь );
		глПрисвоить( МножественнаяАдресация , ДанныеЗаполнения.Групповая );
		УстановитьАдресациюПоУмолчанию( ДанныеЗаполнения.Пояснение );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура УстановитьАдресациюПоУмолчанию( пНаименованиеГруппы )
	
	пНаименованиеГруппы = СокрЛП( пНаименованиеГруппы );
	
	лАдресацияПоУмолчанию = Неопределено;
	
	Если Не ПустаяСтрока( пНаименованиеГруппы ) Тогда
		
		лАдресацияПоУмолчанию = Справочники.ГруппыПользователей.НайтиПоНаименованию( пНаименованиеГруппы );
		
	ИначеЕсли Не ЗначениеЗаполнено(АдресацияПоУмолчанию) Тогда
		
		лАдресацияПоУмолчанию = Справочники.ГруппыПользователей.ВсеПользователи;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( лАдресацияПоУмолчанию ) Тогда
		
		глПрисвоить( АдресацияПоУмолчанию , лАдресацияПоУмолчанию );
		
	КонецЕсли;
	
КонецПроцедуры	//УстановитьАдресациюПоУмолчанию


Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если Перечисления.упТипыСостояний.ЭтоСлияние( ТипСостояния )Тогда
		
		ПроверяемыеРеквизиты.Добавить( "СостояниеРазделение" );
		
		ПроверитьПравилаСлияния( Отказ );
		
	ИначеЕсли Перечисления.упТипыСостояний.ЭтоПользовательскаяАктивность( ТипСостояния )
		И МножественнаяАдресация Тогда
		
		ПроверяемыеРеквизиты.Добавить( "ТипМножественнойАдресации" );
		ПроверяемыеРеквизиты.Добавить( "КоллекцияОбхода" );
		
		ПроверитьКоллекциюОбхода( Отказ );
		
	ИначеЕсли Перечисления.упТипыСостояний.ЭтоВложенныйБизнесПроцесс( ТипСостояния ) Тогда
		
		ПроверяемыеРеквизиты.Добавить( "ВложенныйБизнесПроцесс" );
		ПроверяемыеРеквизиты.Добавить( "ТаблицаАлгоритмов.НаправлениеПотока" );
		
	ИначеЕсли Перечисления.упТипыСостояний.ЭтоУсловнаяАктивность( ТипСостояния ) Тогда
		
		ПроверяемыеРеквизиты.Добавить( "ПереходДляУсловия" );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьПравилаСлияния( Отказ )
	
	массивДопустимыхПереходов = Справочники.упПереходы.ПолучитьМассивВходящихПереходов( Ссылка );
	массивДопустимыхПереходов.Добавить( Справочники.упПереходы.ПустаяСсылка() );
	
	массивРеквизитовВерсии = Справочники.упВерсии.ПолучитьМассивРеквизитовВерсии( Владелец );
	
	Для каждого цПравило Из ПравилаСлияния Цикл
		
		Если массивДопустимыхПереходов.Найти( цПравило.Переход ) = Неопределено Тогда
			
			шаблонТекста = НСтр( "ru='Ошибка заполнения простых правил слияния. Переход ""%1"" НЕ корректен'" );
			текстСообщения = стрЗаполнить( шаблонТекста, цПравило.Переход );
			
			ПутьКДанным = упОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти( "ПравилаСлияния" , цПравило.НомерСтроки , "Переход" );
			
			упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения , ЭтотОбъект , , ПутьКДанным , Отказ );
			
		КонецЕсли;
		
		Если массивРеквизитовВерсии.Найти( цПравило.РеквизитБП ) = Неопределено Тогда
			
			шаблонТекста = НСтр( "ru='Реквизит правила слияния ""%1"" не найден в реквизитах версии'" );
			текстСообщения = стрЗаполнить( шаблонТекста, цПравило.РеквизитБП );
			
			ПутьКДанным = упОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти( "ПравилаСлияния" , цПравило.НомерСтроки , "РеквизитБП" );
			
			упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения , ЭтотОбъект , , ПутьКДанным , Отказ );
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры	//ПроверитьПравилаСлияния

Процедура ПроверитьКоллекциюОбхода( Отказ )
	
	Если Не ЗначениеЗаполнено( КоллекцияОбхода ) Тогда
		// проверяется в другом месте
		Возврат;
	КонецЕсли;
	
	массивДопустимыхКоллекций = Справочники.упВерсии.ПолучитьМассивКоллекцийОбходаВерсии( Владелец );
	
	Если массивДопустимыхКоллекций.Найти( КоллекцияОбхода ) = Неопределено Тогда
		
		шаблонТекста = НСтр( "ru='Коллекция обхода ""%1"" не найден в реквизитах версии'" );
		текстСообщения = стрЗаполнить( шаблонТекста, КоллекцияОбхода );
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( текстСообщения , ЭтотОбъект , "КоллекцияОбхода" ,, Отказ );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	// Очистка лишних реквизитов
	
	Если Не Перечисления.упТипыСостояний.ЭтоСлияние( ТипСостояния )Тогда
		
		СостояниеРазделение = Неопределено;
		
		ПравилаСлияния.Очистить();
		
	КонецЕсли;
		
	Если Не (Перечисления.упТипыСостояний.ЭтоПользовательскаяАктивность( ТипСостояния )
		И МножественнаяАдресация) Тогда
		
		КоллекцияОбхода = Неопределено;
		
	КонецЕсли;
	
	Если Не Перечисления.упТипыСостояний.ЭтоПользовательскаяАктивность( ТипСостояния ) Тогда
		
		КомандыПроцесса.Очистить();
		
	КонецЕсли;
		
	Если Не Перечисления.упТипыСостояний.ЭтоВложенныйБизнесПроцесс( ТипСостояния ) Тогда
		
		ВложенныйБизнесПроцесс = Неопределено;
		
		Для каждого цСтрока Из ТаблицаАлгоритмов Цикл
			цСтрока.НаправлениеПотока = Неопределено;
		КонецЦикла;
		
	КонецЕсли;
		
	Если Не Перечисления.упТипыСостояний.ЭтоУсловнаяАктивность( ТипСостояния ) Тогда
		
		ПереходДляУсловия = Неопределено;
		
	КонецЕсли;
	
	Если Не Перечисления.упТипыСостояний.ЭтоВыборВарианта( ТипСостояния ) Тогда
		Для каждого цСтрока Из Условия Цикл
			цСтрока.Вариант = Неопределено;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

#КонецЕсли