#Область ОписаниеПеременных

&НаКлиенте
Перем ПередЗаписьюПовторныйВызов; // Используется для не модальных вопросов перед записью
&НаКлиенте
Перем ПараметрыОбработчикаОжидания;
&НаКлиенте
Перем ФормаДлительнойОперации;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Периодичность = Константы.к2ПериодичностьВводаСезонныхКоэффициентов.Получить();
	
	АдресПользовательскихНастроек = ПоместитьВоВременноеХранилище(Неопределено, УникальныйИдентификатор);
	
	Если Параметры.ТолькоПросмотр ИЛИ НЕ ПравоДоступа("Изменение", Метаданные.РегистрыСведений.к2СезонныеКоэффициентыТоварныхКатегорий) Тогда
		ЭтотОбъект.ТолькоПросмотр = Истина;
	КонецЕсли; 
	
	Элементы.СезонныеКоэффициентыИзменить.Доступность = НЕ ЭтотОбъект.ТолькоПросмотр;
	Элементы.СезонныеКоэффициентыСоздать.Доступность = ПравоДоступа("Добавление", Метаданные.Справочники.к2ТоварныеКатегории);
	
	Если ТипЗнч(Параметры.ТоварныеКатегории) = Тип("Массив") Тогда
	
		ТоварныеКатегории.ЗагрузитьЗначения(Параметры.ТоварныеКатегории);
		
	ИначеЕсли ТипЗнч(Параметры.ТоварныеКатегории) = Тип("СписокЗначений") Тогда
	
		ТоварныеКатегории.ЗагрузитьЗначения(Параметры.ТоварныеКатегории.ВыгрузитьЗначения());
		
	ИначеЕсли ТипЗнч(Параметры.ТоварныеКатегории) = Тип("СправочникСписок.к2ТоварныеКатегории") Тогда
	
		ТоварныеКатегории.Добавить(Параметры.ТоварныеКатегории);
		
	КонецЕсли; 
	
	к2Планирование.СоздатьТаблицуПериодов(ЭтотОбъект, "Периоды");
	
	ТаблицаПериоды = РеквизитФормыВЗначение("Периоды", Тип("ТаблицаЗначений"));
	
	к2Даты.ЗаполнитьТаблицуПериодов(ТаблицаПериоды, 
		Периодичность, 
		Дата("00010101"), 
		Дата("00011231235959"), 
		Периодичность <> Перечисления.к2Периодичность.Неделя);
	
	Если Периодичность = Перечисления.к2Периодичность.Неделя Тогда
	
		НайденныеСтроки = ТаблицаПериоды.НайтиСтроки(Новый Структура("ДатаНачала", Дата("00011231000000")));
		Если НайденныеСтроки.Количество() > 0 Тогда
		
			Период = НайденныеСтроки[0];
			Период.Заголовок = НСтр("ru = '31.12'");
		
		КонецЕсли; 
	КонецЕсли; 
	
	ЗначениеВРеквизитФормы(ТаблицаПериоды,"Периоды");
	
	ПараметрыВывода = Новый Структура;
	ПараметрыВывода.Вставить("ИмяРеквизитаКроссТаблицы", "СезонныеКоэффициенты");
	ПараметрыВывода.Вставить("ЭлементФормыКроссТаблицы", "СезонныеКоэффициенты");
	ПараметрыВывода.Вставить("ТаблицаПериодов", ТаблицаПериоды);
	ПараметрыВывода.Вставить("Периодичность", Периодичность);
	
	ПараметрыВывода.Вставить("ГруппироватьПоля", Ложь);
	ПараметрыВывода.Вставить("ЗаголовокПоляГруппировки", НСтр("ru = 'Периоды планирования'"));
	
	ПараметрыВывода.Вставить("Поля", Новый Массив());
	
	СтруктураПоля = Новый Структура;
	СтруктураПоля.Вставить("ПрефиксРеквизитаКолонки", "Коэффициент_");
	СтруктураПоля.Вставить("УдалятьРеквизитыТаблицы", Ложь);
	СтруктураПоля.Вставить("СоздаватьЭлемент", Истина);
	СтруктураПоля.Вставить("ТипЭлемента", к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(15, 3));
	СтруктураПоля.Вставить("СоздаватьИтоговыеРеквизиты", Ложь);
	СтруктураПоля.Вставить("ШиринаЭлемента", 6);
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПриИзменении", "Подключаемый_СезонныеКоэффициентыПриИзмененииКоэффициента");
	СтруктураПоля.Вставить("СтруктураДействий", СтруктураДействий);
	
	ПараметрыВывода.Поля.Добавить(СтруктураПоля);
	
	к2Планирование.ОбновитьСтруктуруВыводаКроссТаблицы(ЭтотОбъект, ПараметрыВывода);
	
	ЗаполнитьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если НЕ ЗначениеЗаполнено(Периодичность) Тогда
	
		Отказ = Истина;
		ПоказатьПредупреждение(,НСтр("ru = 'Не указана настройка ""Периодичность указания сезонных коэффициентов"" в разделе Администрирование - Продажи.'"));
	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если ПередЗаписьюПовторныйВызов = Истина Тогда
		ПередЗаписьюПовторныйВызов = Ложь;
		Возврат;
	КонецЕсли; 
	
	Если Модифицированность Тогда
		
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Сохранить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Не сохранять'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Отмена'"));
		
		Оповещение = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение,
			НСтр("ru = 'Коэффциенты были изменены. Сохранить изменения?'"),
			Кнопки);
		
		Отказ = Истина;
		ПередЗаписьюПовторныйВызов = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СезонныеГруппыПриИзменении(Элемент)
	
	Если Модифицированность Тогда
		
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Записать и обновить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Обновить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Не обновлять'"));
		
		Оповещение = Новый ОписаниеОповещения("ТоварныеКатегорииПриИзмененииЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение,
			НСтр("ru = 'Коэффциенты были изменены. Записать изменения и обновить?'"),
			Кнопки);
		
	Иначе
	
		ЗаполнитьНаСервере();
	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ГруппаСтраницыПриСменеСтраницы(Элемент, ТекущаяСтраница)
	
	Если Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаГрафик Тогда
	
		ГрафикНаСервере();
	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ТипДанныхПриИзменении(Элемент)
	
	ГрафикНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура График(Команда)
	
	ГрафикНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура Обновить(Команда)
	
	Если Модифицированность Тогда
		
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Обновить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Не обновлять'"));
		
		Оповещение = Новый ОписаниеОповещения("ОбновитьЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение,
			НСтр("ru = 'Коэффциенты были изменены. При обновлении данные не будут сохранены. Обновить?'"),
			Кнопки);
		
	Иначе
	
		ЗаполнитьНаСервере();
	
	КонецЕсли; 
	
	
КонецПроцедуры

&НаКлиенте
Процедура Записать(Команда)
	
	ЗаписатьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	ЗаписатьНаСервере();
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьПоСтатистике(Команда)
	
	Если Элементы.СезонныеКоэффициенты.ВыделенныеСтроки.Количество() = 0 Тогда
	
		ПоказатьПредупреждение(, НСтр("ru = 'Необходимо выделить строки для расчета по статистике продаж.'"));
		Возврат;
	
	КонецЕсли;
	
	Оповещение = Новый ОписаниеОповещения("РассчитатьПоСтатистикеЗавершение",ЭтотОбъект);
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Периодичность", Периодичность);
	ПараметрыФормы.Вставить("АдресПользовательскихНастроек", АдресПользовательскихНастроек);
	
	ОткрытьФорму("РегистрСведений.к2СезонныеКоэффициентыТоварныхКатегорий.Форма.ФормаПериода",
		ПараметрыФормы,
		ЭтотОбъект,
		УникальныйИдентификатор,
		,
		,
		Оповещение,
		РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура Создать(Команда)
	
	ФормаОбъекта = ОткрытьФорму("Справочник.к2ТоварныеКатегории.ФормаОбъекта", 
		, 
		ЭтотОбъект, 
		УникальныйИдентификатор,
		,
		,
		, 
		РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
	ПараметрыОповещения = Новый Структура;
	ПараметрыОповещения.Вставить("ФормаОбъекта", ФормаОбъекта);
	
	Оповещение = Новый ОписаниеОповещения("СоздатьЗавершение", ЭтотОбъект, ПараметрыОповещения);
	
	ФормаОбъекта.ОписаниеОповещенияОЗакрытии = Оповещение;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтобратьПоГруппам(Команда)
	
	Если Модифицированность Тогда
		
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Записать и обновить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Обновить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Не обновлять'"));
		
		Оповещение = Новый ОписаниеОповещения("ОтобратьПоГруппамЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение,
			НСтр("ru = 'Коэффциенты были изменены. Записать изменения и обновить?'"),
			Кнопки);
		
	Иначе
		
		ТоварныеКатегории.Очистить();
		
		Для каждого ВыбраннаяСтрока Из Элементы.СезонныеКоэффициенты.ВыделенныеСтроки Цикл
		
			СтрокаТЧ = СезонныеКоэффициенты.НайтиПоИдентификатору(ВыбраннаяСтрока);
			Если СтрокаТЧ <> Неопределено И ЗначениеЗаполнено(СтрокаТЧ.ТоварнаяКатегория) Тогда
			
				ТоварныеКатегории.Добавить(СтрокаТЧ.ТоварнаяКатегория);
			
			КонецЕсли; 
		
		КонецЦикла; 
		ЗаполнитьНаСервере();
	
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
КонецПроцедуры

&НаСервере
Процедура ГрафикНаСервере()
	
	Диаграмма.ТипДиаграммы = ТипДиаграммы.График;
	Диаграмма.РежимСглаживания = РежимСглаживанияДиаграммы.ГладкаяКривая;
	
	Если ТипДанных = 1 Тогда
		
		ИсточникДанных = ПолучитьНомированныеКоэффициенты();
	
	Иначе
	
		ИсточникДанных = СезонныеКоэффициенты.Выгрузить();
	
	КонецЕсли; 
	
	ИсточникДанных.Колонки.Вставить(0, "Заголовок");
	
	Для Каждого Источник Из ИсточникДанных Цикл
		
		Источник.Заголовок = Строка(Источник.ТоварнаяКатегория);
	
	КонецЦикла; 
	
	ИсточникДанных.Колонки.Удалить("ТоварнаяКатегория");
	
	Диаграмма.ИсточникДанных = ИсточникДанных;
	
	Для каждого Серия Из Диаграмма.Серии Цикл
	
		Серия.Маркер = ТипМаркераДиаграммы.Нет;
	
	КонецЦикла; 
	
	Для каждого Точка Из Диаграмма.Точки Цикл
		
		Индекс = Диаграмма.Точки.Индекс(Точка);
		НайденныеСтроки = ЭтотОбъект.Периоды.НайтиСтроки(Новый Структура("Активная, НомерКолонки", Истина, Индекс + 1));
		Если НайденныеСтроки.Количество() > 0 Тогда
		
			Точка.Текст = НайденныеСтроки[0].Заголовок;
		
		КонецЕсли; 
	
	КонецЦикла;
	
	Диаграмма.ОбластьПостроения.ОтображатьПодписиШкалыЗначений = Истина;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьНомированныеКоэффициенты()
	
	НомированныеКоэфффициенты = СезонныеКоэффициенты.Выгрузить();
	
	КоличествоПериодов = ЭтотОбъект.Периоды.НайтиСтроки(Новый Структура("Активная", Истина)).Количество();
	
	Для каждого СтрокаТЧ Из НомированныеКоэфффициенты Цикл
		
		СуммаКоэффициентов = 0;
		
		Для каждого Период Из ЭтотОбъект.Периоды Цикл
			Если НЕ Период.Активная Тогда
				Продолжить;
			КонецЕсли; 
			
			СуммаКоэффициентов = СуммаКоэффициентов + СтрокаТЧ["Коэффициент_" + Период.ИмяКолонки];
		
		КонецЦикла; 
		
		Если СуммаКоэффициентов = 0 Тогда
			Продолжить;
		КонецЕсли; 
		
		СреднийКоэффициент = СуммаКоэффициентов / КоличествоПериодов;
		
		Для каждого Период Из ЭтотОбъект.Периоды Цикл
			Если НЕ Период.Активная Тогда
				Продолжить;
			КонецЕсли; 
			
			СтрокаТЧ["Коэффициент_" + Период.ИмяКолонки] =  СтрокаТЧ["Коэффициент_" + Период.ИмяКолонки] / СреднийКоэффициент;
		
		КонецЦикла; 
	
	КонецЦикла; 
	
	Возврат НомированныеКоэфффициенты;
	
КонецФункции

&НаКлиенте
Процедура ОбновитьЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	Если Результат = КодВозвратаДиалога.Да Тогда
	
		ЗаполнитьНаСервере();
	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварныеКатегорииПриИзмененииЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	Если Результат = КодВозвратаДиалога.Да Тогда
	
		ЗаписатьИОбновитьНаСервере();
		
	ИначеЕсли Результат = КодВозвратаДиалога.Нет Тогда
	
		ЗаполнитьНаСервере();
	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ОтобратьПоГруппамЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	Если Результат = КодВозвратаДиалога.Да ИЛИ Результат = КодВозвратаДиалога.Нет Тогда
		
		ТоварныеКатегории.Очистить();
		
		Для каждого ВыбраннаяСтрока Из Элементы.СезонныеКоэффициенты.ВыделенныеСтроки Цикл
		
			СтрокаТЧ = СезонныеКоэффициенты.НайтиПоИдентификатору(ВыбраннаяСтрока);
			Если СтрокаТЧ <> Неопределено И ЗначениеЗаполнено(СтрокаТЧ.ТоварнаяКатегория) Тогда
			
				ТоварныеКатегории.Добавить(СтрокаТЧ.ТоварнаяКатегория);
			
			КонецЕсли; 
		
		КонецЦикла;
	
	КонецЕсли; 
	
	Если Результат = КодВозвратаДиалога.Да Тогда
	
		ЗаписатьИОбновитьНаСервере();
		
	ИначеЕсли Результат = КодВозвратаДиалога.Нет Тогда
	
		ЗаполнитьНаСервере();
	
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьЗавершение(Результат, ПараметрыОповещения) Экспорт
	
	ТоварнаяКатегория = ПараметрыОповещения.ФормаОбъекта.Объект.Ссылка;
	
	Если Не ЗначениеЗаполнено(ТоварнаяКатегория) Тогда
		Возврат;
	КонецЕсли;
	
	Если ТоварныеКатегории.Количество() > 0 Тогда
	
		ТоварныеКатегории.Добавить(ТоварнаяКатегория);
	
	КонецЕсли;
	
	НоваяСтрока = СезонныеКоэффициенты.Добавить();
	НоваяСтрока.ТоварнаяКатегория = ТоварнаяКатегория;
	
	ПараметрыОповещения.ФормаОбъекта = Неопределено;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	Если Результат = КодВозвратаДиалога.Да Тогда
	
		ЗаписатьНаСервере();
		Закрыть();
		
	ИначеЕсли Результат = КодВозвратаДиалога.Нет Тогда
	
		Закрыть();
		
	Иначе
		
		ПередЗаписьюПовторныйВызов = Ложь;
	
	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьИОбновитьНаСервере()

	ЗаписатьНаСервере();
	
	ЗаполнитьНаСервере();
	
КонецПроцедуры
 
&НаСервере
Процедура ЗаполнитьНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	СезонныеГруппы.Ссылка КАК ТоварнаяКатегория,
	|	ВЫБОР &Периодичность
	|		КОГДА ЗНАЧЕНИЕ(Перечисление.к2Периодичность.Неделя)
	|			ТОГДА ДОБАВИТЬКДАТЕ(&НачалоГода, НЕДЕЛЯ, СезонныеКоэффициенты.НомерПериода - 1)
	|		КОГДА ЗНАЧЕНИЕ(Перечисление.к2Периодичность.Месяц)
	|			ТОГДА ДОБАВИТЬКДАТЕ(&НачалоГода, МЕСЯЦ, СезонныеКоэффициенты.НомерПериода - 1)
	|		КОГДА ЗНАЧЕНИЕ(Перечисление.к2Периодичность.Квартал)
	|			ТОГДА ДОБАВИТЬКДАТЕ(&НачалоГода, КВАРТАЛ, СезонныеКоэффициенты.НомерПериода - 1)
	|		КОГДА ЗНАЧЕНИЕ(Перечисление.к2Периодичность.Год)
	|			ТОГДА ДОБАВИТЬКДАТЕ(&НачалоГода, ГОД, СезонныеКоэффициенты.НомерПериода - 1)
	|		ИНАЧЕ ДОБАВИТЬКДАТЕ(&НачалоГода, ДЕНЬ, СезонныеКоэффициенты.НомерПериода - 1)
	|	КОНЕЦ КАК Период,
	|	ЕСТЬNULL(СезонныеКоэффициенты.Коэффициент, 0) КАК Коэффициент
	|ИЗ
	|	Справочник.к2ТоварныеКатегории КАК СезонныеГруппы
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2СезонныеКоэффициентыТоварныхКатегорий КАК СезонныеКоэффициенты
	|		ПО (СезонныеКоэффициенты.Периодичность = &Периодичность)
	|			И СезонныеГруппы.Ссылка = СезонныеКоэффициенты.ТоварнаяКатегория
	|ГДЕ
	|	1 = 1
	|   %ДополнительныОтбор%
	|
	|УПОРЯДОЧИТЬ ПО
	|	СезонныеГруппы.Наименование,
	|	Период
	|ИТОГИ ПО
	|	ТоварнаяКатегория";
	
	
	ТекстОтбора = "";
	
	Запрос.УстановитьПараметр("НачалоГода", Дата("00010101"));
	Запрос.УстановитьПараметр("Периодичность", Периодичность);
	
	ЗаполнятьОтборСезонныхГрупп = Ложь;
	
	Если ТоварныеКатегории.Количество() > 0 Тогда
	
		ТекстОтбора = ТекстОтбора + " И СезонныеГруппы.Ссылка В(&СезонныеГруппы)";
		Запрос.УстановитьПараметр("СезонныеГруппы", ТоварныеКатегории);
		ЗаполнятьОтборСезонныхГрупп = Истина;
	
	КонецЕсли; 
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "%ДополнительныОтбор%", ТекстОтбора);

	РезультатЗапроса = Запрос.Выполнить();

	ВыборкаТоварнаяКатегория = РезультатЗапроса.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);

	СезонныеКоэффициенты.Очистить();
	ТоварныеКатегории.Очистить();
	
	Пока ВыборкаТоварнаяКатегория.Следующий() Цикл
		
		Если ЗаполнятьОтборСезонныхГрупп Тогда
		
			ТоварныеКатегории.Добавить(ВыборкаТоварнаяКатегория.ТоварнаяКатегория);
		
		КонецЕсли;
		
			
		НоваяСтрока = СезонныеКоэффициенты.Добавить();
		
		ВыборкаДетальныеЗаписи = ВыборкаТоварнаяКатегория.Выбрать();
		
		ЗаполнитьЗначенияСвойств(НоваяСтрока, ВыборкаТоварнаяКатегория);

		Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
			Отбор = Новый Структура("Активная, ДатаНачала", Истина, ВыборкаДетальныеЗаписи.Период); 
			НайденныеСтроки = ЭтотОбъект.Периоды.НайтиСтроки(Отбор);
			Если НайденныеСтроки.Количество() > 0 Тогда
				
				НоваяСтрока["Коэффициент_" + НайденныеСтроки[0].ИмяКолонки] = ВыборкаДетальныеЗаписи.Коэффициент;
				
			КонецЕсли; 
		КонецЦикла;
		
		
	КонецЦикла;
	
	Модифицированность = Ложь;
	
	Если Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаГрафик Тогда
	
		ГрафикНаСервере();
	
	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьНаСервере(Отказ = Ложь)
	
	НаборЗаписей = РегистрыСведений.к2СезонныеКоэффициентыТоварныхКатегорий.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Периодичность.Установить(Периодичность);
	
	НаборЗаписейДень = РегистрыСведений.к2СезонныеКоэффициентыТоварныхКатегорий.СоздатьНаборЗаписей();
	НаборЗаписейДень.Отбор.Периодичность.Установить(Перечисления.к2Периодичность.День);
	
	Для каждого СтрокаТЧ Из СезонныеКоэффициенты Цикл
		
		Если НЕ ЗначениеЗаполнено(СтрокаТЧ.ТоварнаяКатегория) Тогда
			Продолжить;
		КонецЕсли; 
		
		НаборЗаписей.Отбор.ТоварнаяКатегория.Установить(СтрокаТЧ.ТоварнаяКатегория);
		НаборЗаписей.Очистить();
		
		НаборЗаписейДень.Отбор.ТоварнаяКатегория.Установить(СтрокаТЧ.ТоварнаяКатегория);
		НаборЗаписейДень.Очистить();
		
		Для каждого Период Из ЭтотОбъект.Периоды Цикл
			Если НЕ Период.Активная Тогда
				Продолжить;
			КонецЕсли;
			
			РСЗапись = НаборЗаписей.Добавить();
			РСЗапись.Периодичность               = Периодичность;
			РСЗапись.ТоварнаяКатегория           = СтрокаТЧ.ТоварнаяКатегория;
			РСЗапись.НомерПериода                = Период.НомерКолонки;
			РСЗапись.Коэффициент                 = СтрокаТЧ["Коэффициент_" + Период.ИмяКолонки];
			
			НомерПериода = ДеньГода(Период.ДатаНачала);
			
			КоличествоДней = 1;
			Если Периодичность = Перечисления.к2Периодичность.Месяц Тогда
				КоличествоДней = (Период.ДатаОкончания + 1 - Период.ДатаНачала) / 86400;
			ИначеЕсли Периодичность = Перечисления.к2Периодичность.Неделя Тогда
				КоличествоДней = 7;
				Если НомерПериода + КоличествоДней > 365 Тогда
				
					КоличествоДней = 365 - НомерПериода + 1;
				
				КонецЕсли; 
				
			КонецЕсли;
			
			КоэффициентКРаспределению = СтрокаТЧ["Коэффициент_" + Период.ИмяКолонки];
			
			Для Индекс = 1 По КоличествоДней Цикл
				
				Коэффициент = КоэффициентКРаспределению / (КоличествоДней - Индекс + 1);
				
				РСЗапись = НаборЗаписейДень.Добавить();
				РСЗапись.Периодичность               = Перечисления.к2Периодичность.День;
				РСЗапись.ТоварнаяКатегория           = СтрокаТЧ.ТоварнаяКатегория;
				РСЗапись.НомерПериода                = НомерПериода;
				РСЗапись.Коэффициент                 = Коэффициент;
				
				НомерПериода = НомерПериода + 1;
				
				КоэффициентКРаспределению = КоэффициентКРаспределению - РСЗапись.Коэффициент;
			КонецЦикла; 
			
		
		КонецЦикла; 
		
		НачатьТранзакцию();
		Попытка
			
			НаборЗаписей.Записать();
			НаборЗаписейДень.Записать();
			
			ЗафиксироватьТранзакцию();
			
		Исключение
			ОтменитьТранзакцию();
			ЗаписьЖурналаРегистрации(НСтр("ru = 'Запись сезонных коэффициентов товарных категорий'"), 
			УровеньЖурналаРегистрации.Ошибка,
			,
			, 
			ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			
			ТекстОшибки = НСтр("ru = 'Не удалось записать коэффициенты по группе: %ТоварнаяКатегория%'");
			ТекстОшибки = СтрЗаменить(ТекстОшибки , "%ТоварнаяКатегория%", Строка(СтрокаТЧ.ТоварнаяКатегория));
			
			ОбщегоНазначения.СообщитьПользователю(ТекстОшибки,,,"СезонныеКоэффициенты", Отказ);
			
		КонецПопытки; 
		
		
	КонецЦикла; 
	
	Если НЕ Отказ Тогда
	
		Модифицированность = Ложь;
	
	КонецЕсли; 
	
	
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьПоСтатистикеЗавершение(Настройки, ДополнительныеПараметры) Экспорт 
	
	Если Настройки <> Неопределено Тогда
		
		Результат = РассчитатьПоСтатистикеПродажНаСервере(Настройки);
		
		Если НЕ Результат.ЗаданиеВыполнено Тогда
			ИдентификаторЗадания = Результат.ИдентификаторЗадания;
			АдресХранилища       = Результат.АдресХранилища;
			
			ДлительныеОперацииКлиент.ИнициализироватьПараметрыОбработчикаОжидания(ПараметрыОбработчикаОжидания);
			
			ПодключитьОбработчикОжидания("Подключаемый_ПроверитьВыполнениеЗадания", 1, Истина);
			ФормаДлительнойОперации = ДлительныеОперацииКлиент.ОткрытьФормуДлительнойОперации(ЭтотОбъект, ИдентификаторЗадания);
		Иначе
			КоличествоСтрок = ПолучитьРезультатРасчетаНаСервере();
			ОповеститьОбОкончанииРасчетаПоСтатистикеПродаж(Элементы.СезонныеКоэффициенты.ВыделенныеСтроки.Количество(), 
				КоличествоСтрок);
		КонецЕсли;
	КонецЕсли; 
	
КонецПроцедуры

// Унифицированная процедура проверки выполнения фонового задания
&НаКлиенте
Процедура Подключаемый_ПроверитьВыполнениеЗадания()
	
	Попытка
		Если ФормаДлительнойОперации.Открыта() 
			И ФормаДлительнойОперации.ИдентификаторЗадания = ИдентификаторЗадания Тогда
			Если ЗаданиеВыполнено(ИдентификаторЗадания) Тогда 
				КоличествоСтрок = ПолучитьРезультатРасчетаНаСервере();
				ДлительныеОперацииКлиент.ЗакрытьФормуДлительнойОперации(ФормаДлительнойОперации);
				ОповеститьОбОкончанииРасчетаПоСтатистикеПродаж(Элементы.СезонныеКоэффициенты.ВыделенныеСтроки.Количество(), 
					КоличествоСтрок);
			Иначе
				ДлительныеОперацииКлиент.ОбновитьПараметрыОбработчикаОжидания(ПараметрыОбработчикаОжидания);
				ПодключитьОбработчикОжидания(
					"Подключаемый_ПроверитьВыполнениеЗадания",
					ПараметрыОбработчикаОжидания.ТекущийИнтервал,
					Истина);
			КонецЕсли;
		КонецЕсли;
	Исключение
		ДлительныеОперацииКлиент.ЗакрытьФормуДлительнойОперации(ФормаДлительнойОперации);
		ВызватьИсключение;
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура ОповеститьОбОкончанииРасчетаПоСтатистикеПродаж(КоличествоСтрокВсего, КоличествоСтрок)

	Текст = НСтр("ru = 'Обновлено: (%1) из (%2) выделенных строк'");
	Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		Текст, 
		КоличествоСтрок, 
		КоличествоСтрокВсего);
	ПоказатьОповещениеПользователя(Текст);

КонецПроцедуры 
 
&НаСервереБезКонтекста
Функция ЗаданиеВыполнено(ИдентификаторЗадания)
	
	Возврат ДлительныеОперации.ЗаданиеВыполнено(ИдентификаторЗадания);
	
КонецФункции

&НаСервере
Функция РассчитатьПоСтатистикеПродажНаСервере(ПараметрыЗадания)
	
	ОтборТоварныеКатегории = Новый СписокЗначений;
	Для каждого Строка Из Элементы.СезонныеКоэффициенты.ВыделенныеСтроки Цикл
		СтрокаТЧ = СезонныеКоэффициенты.НайтиПоИдентификатору(Строка);
		Если ЗначениеЗаполнено(СтрокаТЧ.ТоварнаяКатегория) Тогда
			ОтборТоварныеКатегории.Добавить(СтрокаТЧ.ТоварнаяКатегория);
		КонецЕсли; 
	КонецЦикла; 
	
	ПараметрыЗадания.Вставить("УникальныйИдентификатор", УникальныйИдентификатор);
	ПараметрыЗадания.Вставить("Периодичность",           Периодичность);
	ПараметрыЗадания.Вставить("КлючОбщихНастроек",       "РегистрСведений.к2СезонныеКоэффициентыТоварныхКатегорий");
	
	Настройки = Новый Структура;
	Настройки.Вставить("ОтборТоварныеКатегории", ОтборТоварныеКатегории);
	Настройки.Вставить("ПользовательскиеНастройки", ПолучитьИзВременногоХранилища(АдресПользовательскихНастроек));
	
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить(ПараметрыЗадания.КлючОбщихНастроек, 
		"НастройкиФоновогоЗадания_"+ПараметрыЗадания.УникальныйИдентификатор, 
		Настройки);
	
	НаименованиеЗадания = НСтр("ru = 'Расчет сезонных коэффициентов по статистике продаж'");
		
	Результат = ДлительныеОперации.ЗапуститьВыполнениеВФоне(
		УникальныйИдентификатор,
		"РегистрыСведений.к2СезонныеКоэффициентыТоварныхКатегорий.РассчитатьКоэффициентыПоСтатистикеПродаж",
		ПараметрыЗадания,
		НаименованиеЗадания);
	
	АдресХранилища = Результат.АдресХранилища;
	
	Возврат Результат;
	
КонецФункции

&НаСервере
Функция ПолучитьРезультатРасчетаНаСервере()
	
	ТаблицаКоэффициентов = ПолучитьИзВременногоХранилища(АдресХранилища);
	
	ТаблицаКоэффициентов.Сортировать("ТоварнаяКатегория, Период");
	
	ТоварнаяКатегория              = Неопределено;
	СтрокаТЧ                    = Неопределено;
	
	КоличествоСтрок = 0;
	
	Для каждого СтрокаКоэффициентов Из ТаблицаКоэффициентов Цикл
		
		Если ТоварнаяКатегория <> СтрокаКоэффициентов.ТоварнаяКатегория Тогда
			
			КоличествоСтрок = КоличествоСтрок + 1;
			ТоварнаяКатегория              = СтрокаКоэффициентов.ТоварнаяКатегория;
			
			НайденныеСтроки = СезонныеКоэффициенты.НайтиСтроки(Новый Структура("ТоварнаяКатегория", 
			                                                         СтрокаКоэффициентов.ТоварнаяКатегория));
			Если НайденныеСтроки.Количество() > 0 Тогда
				
				СтрокаТЧ = НайденныеСтроки[0];
				Для каждого Период Из ЭтотОбъект.Периоды Цикл
					Если НЕ Период.Активная Тогда
						Продолжить;
					КонецЕсли; 
					СтрокаТЧ["Коэффициент_"+Период.ИмяКолонки] = 0;
				КонецЦикла;
			Иначе
				СтрокаТЧ = Неопределено;
			КонецЕсли; 
		КонецЕсли;
		
		Если СтрокаТЧ = Неопределено Тогда
			Продолжить;
		КонецЕсли; 
		
		Отбор = Новый Структура("Активная, ДатаНачала", Истина, СтрокаКоэффициентов.Период); 
		НайденныеСтроки = ЭтотОбъект.Периоды.НайтиСтроки(Отбор);
		Если НайденныеСтроки.Количество() > 0 Тогда
			
			СтрокаТЧ["Коэффициент_"+НайденныеСтроки[0].ИмяКолонки] = СтрокаКоэффициентов.Коэффициент;
			
		КонецЕсли;
	КонецЦикла; 
	
	Возврат КоличествоСтрок;
	
КонецФункции

#КонецОбласти

