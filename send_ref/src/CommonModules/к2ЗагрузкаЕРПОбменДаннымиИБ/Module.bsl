
#Область СлужебныйПрограммныйИнтерфейс

Процедура СоответствиеЗагружаемыхТиповДополнить(СоответствиеТипов) Экспорт
	
	СтруктураТипа = Обработки.к2ЗагрузкаОбменДаннымиИБ.СтруктураТипаДанныхПоУмолчанию();
	СтруктураТипа.ТипДанных = Метаданные.Справочники.УпаковкиЕдиницыИзмерения.ПолноеИмя();
	СоответствиеТипов.Вставить("Справочник.УпаковкиЕдиницыИзмерения", СтруктураТипа);
	
	СтруктураТипа = Обработки.к2ЗагрузкаОбменДаннымиИБ.СтруктураТипаДанныхПоУмолчанию();
	СтруктураТипа.ТипДанных = Метаданные.Справочники.Склады.ПолноеИмя();
	СтруктураТипа.Порядок = 200;
	СоответствиеТипов.Вставить("Справочник.Склады", СтруктураТипа);
	
	СтруктураТипа = Обработки.к2ЗагрузкаОбменДаннымиИБ.СтруктураТипаДанныхПоУмолчанию();
	СтруктураТипа.ТипДанных = Метаданные.Справочники.Номенклатура.ПолноеИмя();
	СтруктураТипа.Порядок = 400;
	СоответствиеТипов.Вставить("Справочник.Номенклатура", СтруктураТипа);
	
	СтруктураТипа = Обработки.к2ЗагрузкаОбменДаннымиИБ.СтруктураТипаДанныхПоУмолчанию();
	СтруктураТипа.ТипДанных = Метаданные.Документы.к2РаспоряжениеНаПеремещение.ПолноеИмя();
	СтруктураТипа.Порядок = 1000;
	СоответствиеТипов.Вставить("Документ.ПеремещениеТоваров", СтруктураТипа);
	
КонецПроцедуры

Процедура ЗагрузитьДанные(ОбъектОбработки, ТипДанных, МенеджерОбъекта, СтруктураПакета) Экспорт
	
	Если ТипДанных = Метаданные.Справочники.Склады.ПолноеИмя() Тогда
		
		СкладыЗагрузить(ОбъектОбработки, МенеджерОбъекта, СтруктураПакета);
		
	ИначеЕсли ТипДанных = Метаданные.Справочники.Номенклатура.ПолноеИмя() Тогда
		
		НоменклатураЗагрузить(ОбъектОбработки, МенеджерОбъекта, СтруктураПакета);
		
	ИначеЕсли ТипДанных = Метаданные.Документы.к2РаспоряжениеНаПеремещение.ПолноеИмя() Тогда
		
		РаспоряжениеНаПеремещениеЗагрузить(ОбъектОбработки, МенеджерОбъекта, СтруктураПакета);
		
	Иначе
		
		ШаблонИсключения = НСтр("ru = 'Не определен обработчик загрузки для типа данных ""%1"".'");
		ТекстИсключения = к2ОбменДаннымиИБКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонИсключения, ТипДанных);
		ВызватьИсключение ТекстИсключения;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура СкладыЗагрузить(ОбъектОбработки, Менеджер, СтруктураПакета)
	
	ОбъектДжсон = СтруктураПакета.ОбъектИБСериализованный;
	
	СправочникОбъект = ОбъектОбработки.НайтиСоздатьСправочникОбъект(Менеджер, ОбъектДжсон);
	
	ОбъектОбработки.СправочникЗаполнитьСтандартныеРеквизиты(СправочникОбъект, СтруктураПакета);
	
	СправочникОбъект.Родитель = ОбъектОбработки.СсылкаПоПараметрам(ОбъектДжсон.Родитель);
	СправочникОбъект.Наименование = ОбъектДжсон.Наименование;
	
	Если Не СправочникОбъект.ЭтоГруппа Тогда
		
		Реквизиты = ОбъектДжсон.Реквизиты;
		
		СправочникОбъект.ИспользоватьАдресноеХранение = Реквизиты.ИспользоватьАдресноеХранение;
		СправочникОбъект.ДатаНачалаАдресногоХраненияОстатков = Дата(Реквизиты.ДатаНачалаАдресногоХраненияОстатков);
		
	КонецЕсли;
	
	ОбъектОбработки.ОтразитьИзмененияСправочника(СправочникОбъект);
	
	Если Не ОбъектОбработки.ЕстьОшибкиЗагрузки() Тогда
		
		ОбъектОбработки.РезультатОбработкиОбъектыДобавить(СправочникОбъект.Ссылка);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура НоменклатураЗагрузить(ОбъектОбработки, Менеджер, СтруктураПакета)
	
	ОбъектДжсон = СтруктураПакета.ОбъектИБСериализованный;
	
	СправочникОбъект = ОбъектОбработки.НайтиСоздатьСправочникОбъект(Менеджер, ОбъектДжсон);
	
	Если СправочникОбъект.ЭтоНовый() Тогда
		СправочникОбъект.УстановитьНовыйКод();
	КонецЕсли;
	
	ОбъектОбработки.СправочникЗаполнитьСтандартныеРеквизиты(СправочникОбъект, СтруктураПакета);
	
	Правило = ОбъектОбработки.ПравилоЗагрузки(СправочникОбъект._Отправитель, СтруктураПакета.ТипПакета);
	
	СправочникОбъект.Родитель = ОбъектОбработки.СсылкаПоПараметрам(ОбъектДжсон.Родитель);
	СправочникОбъект.Наименование = ОбъектДжсон.Наименование;
	
	Если Не СправочникОбъект.ЭтоГруппа Тогда
		
		СправочникОбъект.ИспользоватьУпаковки = Истина;
		СправочникОбъект.НаборУпаковок = Справочники.к2НаборыУпаковок.ИндивидуальныйДляНоменклатуры;
		
		Реквизиты = ОбъектДжсон.Реквизиты;
		
		РеквизитыПравила = глРеквизиты(Правило, "ВидНоменклатуры");
		
		СправочникОбъект.ВидНоменклатуры = РеквизитыПравила.ВидНоменклатуры;
		Справочники.Номенклатура.ЗаполнитьРеквизитыПоВидуНоменклатуры(СправочникОбъект);
		
		СправочникОбъект.НаименованиеПолное = Реквизиты.НаименованиеПолное;
		СправочникОбъект.Артикул = Реквизиты.Артикул;
		СправочникОбъект.ЕдиницаИзмерения = ОбъектОбработки.СсылкаПоПараметрам(Реквизиты.ЕдиницаИзмерения);
		СправочникОбъект.ИспользованиеДопЕдиницИзмерения = Перечисления.к2ИспользованиеДопЕдиницИзмерения.НеИспользовать;
		СправочникОбъект.ТипНоменклатуры = Перечисления.к2ТипыНоменклатуры.ГотоваяПродукция;
		СправочникОбъект.СырьеваяСтатьяЗатрат = Справочники.к2СырьевыеСтатьиЗатрат.Сырье;
		СправочникОбъект.СрокГодности = Реквизиты.СрокГодности;
		
	КонецЕсли;
	
	ОбъектОбработки.ОтразитьИзмененияСправочника(СправочникОбъект);
	
	Если Не ОбъектОбработки.ЕстьОшибкиЗагрузки() Тогда
		
		ОбъектОбработки.РезультатОбработкиОбъектыДобавить(СправочникОбъект.Ссылка);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура РаспоряжениеНаПеремещениеЗагрузить(ОбъектОбработки, Менеджер, СтруктураПакета)
	
	ОбъектДжсон = СтруктураПакета.ОбъектИБСериализованный;
	
	ДокументОбъект = ОбъектОбработки.НайтиСоздатьДокументОбъект(Менеджер, ОбъектДжсон);
	
	ОбъектОбработки.ДокументЗаполнитьСтандартныеРеквизиты(ДокументОбъект, СтруктураПакета);
	
	Реквизиты = ОбъектДжсон.Реквизиты;
	ТабличныеЧасти = ОбъектДжсон.ТабличныеЧасти;
	
	ДокументОбъект.Статус = Перечисления.к2СтатусыРаспоряженийНаПеремещение.Выполнено;
	ДокументОбъект.СкладОтправитель = ОбъектОбработки.СсылкаПоПараметрам(Реквизиты.СкладОтправитель);
	ДокументОбъект.СкладПолучатель = ОбъектОбработки.СсылкаПоПараметрам(Реквизиты.СкладПолучатель);
	ДокументОбъект.СпособСоздания = Перечисления.к2СпособыСозданияДокументов.ОбменДанными;
	ДокументОбъект.ТипПеремещения = Перечисления.к2ТипыРаспоряженийНаПеремещение.Перемещение;
	ДокументОбъект.ФактическаяДатаПеремещения = ДокументОбъект.Дата;
	
	ДокументОбъект.Товары.Очистить();
	
	Для Каждого Строка Из ТабличныеЧасти.Товары Цикл
		
		НоваяСтрока = ДокументОбъект.Товары.Добавить();
		НоваяСтрока.Номенклатура = ОбъектОбработки.СсылкаПоПараметрам(Строка.Номенклатура);
		НоваяСтрока.Количество = Строка.Количество;
		
		СтруктураДействий = Новый Структура();
		СтруктураДействий.Вставить("ПроверитьХарактеристикуПоВладельцу", НоваяСтрока.Характеристика);
		СтруктураДействий.Вставить("ПроверитьСериюПоВидуНоменклатуры", НоваяСтрока.Серия);
		СтруктураДействий.Вставить("ПроверитьЗаполнитьУпаковкуПоВладельцу", НоваяСтрока.Упаковка);
		СтруктураДействий.Вставить("ПересчитатьКоличествоУпаковок");
		
		к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, Неопределено);
		
	КонецЦикла;
	
	ОбъектОбработки.ОтразитьИзмененияДокумента(ДокументОбъект);
	
	Если Не ОбъектОбработки.ЕстьОшибкиЗагрузки() Тогда
		
		ОбъектОбработки.РезультатОбработкиОбъектыДобавить(ДокументОбъект.Ссылка);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
