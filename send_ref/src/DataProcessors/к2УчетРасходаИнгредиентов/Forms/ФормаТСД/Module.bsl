
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьЗначенияПоУмолчанию();
	
	ОбновитьФормуНаСервере();
	
	УстановитьТекущуюСтраницу();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, ЭтотОбъект, "СканерШтрихкода");
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить(СкоростьПрокрутки);
	
	ПроверитьВозможностьОткрытияФормы(Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
	Если Событие = "Штрихкод" И ВводДоступен() Тогда
		
		ОбработатьШтрихкод(Данные);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	ИсключитьНепроверяемыеРеквизиты(ПроверяемыеРеквизиты);
	
	ПроверитьУказаниеОсновногоМатериала(Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтотОбъект);
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ШтрихКодПриИзменении(Элемент)
	
	ОбработатьШтрихкод(ШтрихКод);
	
КонецПроцедуры

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	СкладПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура НоменклатураМатериалОсновнойПриИзменении(Элемент)
	
	НоменклатураМатериалОсновнойПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыРаспределениеМатериалов

&НаКлиенте
Процедура РаспределениеМатериаловПриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловПриАктивизацииПоля(Элемент)
	
	Элементы.РаспределениеМатериалов.ТекущийЭлемент = Элементы.РаспределениеМатериаловДатаРасхода;
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеМатериаловДатаРасходаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.РаспределениеМатериалов.ТекущаяСтрока;
	
	Если ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Элементы.РаспределениеМатериалов.ТекущаяСтрока = ТекущаяСтрока + 1;
	
	Если Элементы.РаспределениеМатериалов.ТекущаяСтрока = Неопределено Тогда
		Элементы.РаспределениеМатериалов.ТекущаяСтрока = ТекущаяСтрока;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаСохранить(Команда)
	
	Сохранить();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаУказатьСмесь(Команда)
	
	КомандаУказатьСмесьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаУказатьВспомогательныеМатериалы(Команда)
	
	КомандаУказатьВспомогательныеМатериалыНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбработчикиСобытийЭлементовФормы

&НаСервере
Процедура СкладПриИзмененииНаСервере()
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура НоменклатураМатериалОсновнойПриИзмененииНаСервере()
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура КомандаУказатьСмесьНаСервере()
	
	ПерейтиНаСтраницу(Элементы.СтраницаОсновнойМатериал);
	
КонецПроцедуры

&НаСервере
Процедура КомандаУказатьВспомогательныеМатериалыНаСервере()
	
	ПерейтиНаСтраницу(Элементы.СтраницаВспомогательныеМатериалы);
	
КонецПроцедуры

#КонецОбласти

#Область ЗначенияПоУмолчанию

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();
	
	СканерШтрихКодовПодключен = к2РаботаСПодключаемымОборудованием.СканерШтрихКодовПодключен();
	
	Обработки.к2УчетРасходаИнгредиентов.УстановитьЗначенияПоУмолчанию(ЭтотОбъект, Параметры);
	
	Обработка = РеквизитФормыВЗначение("Объект");
	Обработка.УстановитьЗначенияПоУмолчанию(Параметры);
	
	ЗначениеВРеквизитФормы(Обработка, "Объект");
	
КонецПроцедуры

#КонецОбласти

#Область ОбновитьФорму

&НаСервере
Процедура ОбновитьФормуНаСервере()
	
	Модифицированность = Ложь;
	
	Материалы = ФактРаспределения();
	
	УстановитьОсновнойМатериал(Материалы);
	
	УстановитьВспомогательныеМатериалы(Материалы);
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Функция ФактРаспределения()
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Переработка"    , Объект.Переработка);
	Запрос.УстановитьПараметр("Спецификация"   , Объект.Спецификация);
	Запрос.УстановитьПараметр("Номенклатура"   , Объект.Номенклатура);
	Запрос.УстановитьПараметр("Характеристика" , Объект.Характеристика);
	
	#Область ТекстЗапроса
	Запрос.Текст =
	"ВЫБРАТЬ
	|	к2РесурсныеСпецификацииВыходныеИзделия.Ссылка КАК Спецификация,
	|	к2РесурсныеСпецификацииВыходныеИзделия.Этап КАК Этап
	|ПОМЕСТИТЬ Изделия
	|ИЗ
	|	Справочник.к2РесурсныеСпецификации.ВыходныеИзделия КАК к2РесурсныеСпецификацииВыходныеИзделия
	|ГДЕ
	|	к2РесурсныеСпецификацииВыходныеИзделия.Ссылка = &Спецификация
	|	И к2РесурсныеСпецификацииВыходныеИзделия.Номенклатура = &Номенклатура
	|	И к2РесурсныеСпецификацииВыходныеИзделия.Характеристика = &Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Материалы.Номенклатура КАК Номенклатура,
	|	Материалы.Характеристика КАК Характеристика,
	|	Материалы.Основное КАК Основное
	|ПОМЕСТИТЬ Материалы
	|ИЗ
	|	Справочник.к2РесурсныеСпецификации.МатериалыИУслуги КАК Материалы
	|ГДЕ
	|	(Материалы.Ссылка, Материалы.Этап) В
	|			(ВЫБРАТЬ
	|				Изделия.Спецификация,
	|				Изделия.Этап
	|			ИЗ
	|				Изделия КАК Изделия)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Распределение.Номенклатура КАК Номенклатура,
	|	ПРЕДСТАВЛЕНИЕ(Распределение.Номенклатура) КАК НоменклатураПредставление,
	|	Распределение.Характеристика КАК Характеристика,
	|	ПРЕДСТАВЛЕНИЕ(Распределение.Характеристика) КАК ХарактеристикаПредставление,
	|	Распределение.ДатаРасхода КАК ДатаРасхода,
	|	Распределение.Склад КАК Склад,
	|	Распределение.Ячейка КАК Ячейка,
	|	Распределение.Ссылка.РабочийЦентр КАК РабочийЦентр,
	|	Распределение.Количество КАК Количество,
	|	Распределение.Количество1 КАК Количество1,
	|	Распределение.Количество2 КАК Количество2,
	|	ЕСТЬNULL(Материалы.Основное, ЛОЖЬ) КАК Основное,
	|	Распределение.Серия КАК Серия,
	|	Распределение.НомерПакета КАК НомерПакета
	|ИЗ
	|	Документ.к2Переработка.РаспределениеМатериалов КАК Распределение
	|		ЛЕВОЕ СОЕДИНЕНИЕ Материалы КАК Материалы
	|		ПО (Распределение.Номенклатура = Материалы.Номенклатура
	|					И Распределение.Характеристика = Материалы.Характеристика
	|				ИЛИ Распределение.НоменклатураОригинал = Материалы.Номенклатура
	|					И Распределение.ХарактеристикаОригинал = Материалы.Характеристика)
	|ГДЕ
	|	Распределение.Ссылка = &Переработка
	|
	|СГРУППИРОВАТЬ ПО
	|	Распределение.Номенклатура,
	|	Распределение.Характеристика,
	|	Распределение.ДатаРасхода,
	|	Распределение.Склад,
	|	Распределение.Ячейка,
	|	Распределение.Ссылка.РабочийЦентр,
	|	Распределение.Количество,
	|	Распределение.Количество1,
	|	Распределение.Количество2,
	|	Распределение.НомерПакета,
	|	ЕСТЬNULL(Материалы.Основное, ЛОЖЬ),
	|	Распределение.Серия";
	#КонецОбласти
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

&НаСервере
Процедура УстановитьОсновнойМатериал(Материалы)
	
	ОсновнойМатериал = Материалы.НайтиСтроки(Новый Структура("Основное", Истина));
	
	ИмеетсяОсновнойМатериал = Не ОсновнойМатериал.Количество() = 0;
	
	Если ОсновнойМатериал.Количество() = 1 Тогда
		
		СтрокаМатериалОсновной = ОсновнойМатериал[0];
		
		НоменклатураМатериалОсновной = СтрокаМатериалОсновной.Номенклатура;
		ХарактеристикаМатериалОсновной = СтрокаМатериалОсновной.Характеристика;
		СкладМатериалОсновной = СтрокаМатериалОсновной.Склад;
		ЯчейкаМатериалОсновной = СтрокаМатериалОсновной.Ячейка;
		КоличествоМатериалОсновной = СтрокаМатериалОсновной.Количество;
		Количество1МатериалОсновной = СтрокаМатериалОсновной.Количество1;
		Количество2МатериалОсновной = СтрокаМатериалОсновной.Количество2;
		СерияМатериалОсновной = СтрокаМатериалОсновной.Серия;
		
		УстановитьДанныеДляВыпускаСмеси();
		
	ИначеЕсли ОсновнойМатериал.Количество() = 0 Тогда
		
		Отбор = Новый Структура("Этап, Основное", Объект.Этап, Истина);
		
		ОсновнойМатериалСпецификации = Объект.Спецификация.МатериалыИУслуги.НайтиСтроки(Отбор);
		
		Если ОсновнойМатериалСпецификации.Количество() = 1 Тогда
			
			НоменклатураМатериалОсновной = ОсновнойМатериалСпецификации[0].Номенклатура;
			ХарактеристикаМатериалОсновной = ОсновнойМатериалСпецификации[0].Характеристика;
			
			УстановитьДанныеДляВыпускаСмеси();
			
		Иначе
			
			ВызватьИсключение НСтр("ru = 'В спецификации должен быть единственный основной материал.'");
			
		КонецЕсли;
		
	Иначе
		
		ОшибкаНесколькоОсновныхМатериалов = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДанныеДляВыпускаСмеси()
	
	СтруктураСерии = СтруктураЗаполнения_Серия();
	Если Не ЗначениеЗаполнено(СерияМатериалОсновной) Тогда
		СерияМатериалОсновной = Справочники.к2СерииНоменклатуры.НайтиСерию(СтруктураСерии);
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ПроизводственноеЗадание", Объект.ПроизводственноеЗадание);
	Запрос.УстановитьПараметр("Номенклатура", НоменклатураМатериалОсновной);
	Запрос.УстановитьПараметр("Характеристика", ХарактеристикаМатериалОсновной);
	Запрос.УстановитьПараметр("Серия", СерияМатериалОсновной);
	
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	к2ПроизводственныеЗадания.Задание КАК Задание,
	|	к2ПроизводственныеЗадания.РабочийЦентр КАК РабочийЦентр,
	|	к2ПроизводственныеЗадания.Спецификация КАК Спецификация
	|ИЗ
	|	РегистрНакопления.к2ПроизводственныеЗадания КАК к2ПроизводственныеЗадания
	|ГДЕ
	|	к2ПроизводственныеЗадания.Задание.Основание = &ПроизводственноеЗадание
	|	И к2ПроизводственныеЗадания.Номенклатура = &Номенклатура
	|	И к2ПроизводственныеЗадания.Характеристика = &Характеристика
	|	И к2ПроизводственныеЗадания.Серия = &Серия";
	
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	
	Если РезультатЗапроса.Количество() = 1 Тогда
		
		ПроизводственноеЗаданиеМатериалОсновной = РезультатЗапроса[0].Задание;
		РабочийЦентрМатериалОсновной = РезультатЗапроса[0].РабочийЦентр;
		СпецификацияМатериалОсновной = РезультатЗапроса[0].Спецификация;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция СтруктураЗаполнения_Серия()
	
	СтруктураСерии = Справочники.к2СерииНоменклатуры.СтруктураЗаполненияСерии();
	ЗаполнитьЗначенияСвойств(СтруктураСерии, Объект.Серия);
	СтруктураСерии.Владелец = НоменклатураМатериалОсновной;
	
	Возврат СтруктураСерии;
	
КонецФункции

&НаСервере
Процедура УстановитьВспомогательныеМатериалы(Материалы)
	
	ВспомогательныеМатериалы = Материалы.НайтиСтроки(Новый Структура("Основное", Ложь));
	
	ИмеютсяВспомогательныеМатериалы = Не ВспомогательныеМатериалы.Количество() = 0;
	
	Объект.РаспределениеМатериалов.Очистить();
	
	Для Каждого РаспределениеМатериала Из ВспомогательныеМатериалы Цикл
		
		НоваяСтрока = Объект.РаспределениеМатериалов.Добавить();
		
		ЗаполнитьЗначенияСвойств(НоваяСтрока, РаспределениеМатериала);
		
		НоваяСтрока.НоменклатураПредставление = НоменклатураКлиентСервер.ПредставлениеНоменклатуры(
			РаспределениеМатериала.НоменклатураПредставление,
			РаспределениеМатериала.ХарактеристикаПредставление);
		
	КонецЦикла;
	
	Объект.РаспределениеМатериалов.Сортировать("НомерПакета, Номенклатура");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработкаШтрихкода

&НаКлиенте
Процедура ОбработатьШтрихкод(Знач Штрихкод)
	
	ТекстОшибки = "";
	
	Если Не ОбработатьШтрихкодНаСервере(Штрихкод, ТекстОшибки) Тогда
		
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ОбработатьШтрихкодНаСервере(Штрихкод, ТекстОшибки)
	
	Отказ = Ложь;
	
	ДанныеШтрихкода = Обработки.к2УчетРасходаИнгредиентов.ДанныеШтрихкодаЭтикеткиПакета(Штрихкод);
	
	Если Обработки.к2УчетРасходаИнгредиентов.ПроверитьДанныеШтрихкода(ДанныеШтрихкода, Параметры.Переработка, ТекстОшибки, Отказ) Тогда
		
		Отбор = Новый Структура;
		Отбор.Вставить("НомерПакета", ДанныеШтрихкода.НомерПакета);
		
		СтрокиМатериала = Объект.РаспределениеМатериалов.НайтиСтроки(Отбор);
		
		Для Каждого СтрокаМатериала Из СтрокиМатериала Цикл
			
			СтрокаМатериала.ДатаРасхода = ТекущаяДатаСеанса();
			
			Модифицированность = Истина;
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

#КонецОбласти

#Область Сохранить

&НаКлиенте
Процедура Сохранить()
	
	Если Не Модифицированность Тогда
		Возврат;
	КонецЕсли;
	
	Отказ = Ложь;
	ТекстОшибки = "";
	
	Попытка
		
		СохранитьНаСервере(ТекстОшибки, Отказ);
		
		Если Отказ Тогда
			ВызватьИсключение ТекстОшибки;
		Иначе
			ЗавершениеОперацииСохранение();
		КонецЕсли;
		
	Исключение
		
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки);
		
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Процедура СохранитьНаСервере(ТекстОшибки, Отказ)
	
	Если Не ПроверитьЗаполнениеФормыНаСервере(ТекстОшибки, Отказ) Тогда
		Возврат;
	КонецЕсли;
	
	НачатьТранзакцию();
	
	Попытка
		
		Если СоздаватьДокументВыпуска() Тогда
			
			СтруктураЗаполненияВыпуска = СтруктураЗаполнения_ПереработкаВыпускаМатериала();
			СоздатьПереработкаВыпускаМатериала(СтруктураЗаполненияВыпуска, ТекстОшибки, Отказ);
			
		КонецЕсли;
		
		СтруктураЗаполненияРасхода = СтруктураЗаполнения_ПереработкаРасходаМатериала();
		ДокументРасхода = СоздатьПереработкаРасходаМатериала(СтруктураЗаполненияРасхода, ТекстОшибки, Отказ);
		
		Если Отказ Тогда
			
			ВызватьИсключение ТекстОшибки;
			
		Иначе
			
			Объект.Переработка = ДокументРасхода;
			
			ОбновитьФормуНаСервере();
			
			ЗафиксироватьТранзакцию();
			
		КонецЕсли;
		
	Исключение
		
		Если ТранзакцияАктивна() Тогда
			ОтменитьТранзакцию();
		КонецЕсли;
		
		ИнформацияОбОшибке = ИнформацияОбОшибке();
		
		ИмяСобытия = НСтр("ru = 'к2УчетРасходаИнгредиентов.Сохранить'", ОбщегоНазначения.КодОсновногоЯзыка());
		
		ЗаписьЖурналаРегистрации(ИмяСобытия
			, УровеньЖурналаРегистрации.Ошибка
			, Метаданные.Документы.к2Переработка
			,
			, ПодробноеПредставлениеОшибки(ИнформацияОбОшибке));
			
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Функция ПроверитьЗаполнениеФормыНаСервере(ТекстОшибки, Отказ)
	
	Если Не ПроверитьЗаполнение() Тогда
		ТекстОшибки = к2ОбщегоНазначения.ТекстСообщенийПользователю(Истина);
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

&НаСервере
Функция СоздаватьДокументВыпуска()
	
	Возврат н_СоздаватьВыпускОсновногоМатериала И Не ВыпускСуществует();
	
КонецФункции

&НаСервере
Функция ВыпускСуществует()
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Номенклатура", НоменклатураМатериалОсновной);
	Запрос.УстановитьПараметр("Характеристика", ХарактеристикаМатериалОсновной);
	Запрос.УстановитьПараметр("Серия", СерияМатериалОсновной);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	к2ВыпускОбороты.Номенклатура КАК Номенклатура,
	|	к2ВыпускОбороты.Характеристика КАК Характеристика,
	|	к2ВыпускОбороты.Серия КАК Серия,
	|	к2ВыпускОбороты.Спецификация КАК Спецификация,
	|	к2ВыпускОбороты.Этап КАК Этап
	|ИЗ
	|	РегистрНакопления.к2Выпуск.Обороты КАК к2ВыпускОбороты
	|ГДЕ
	|	к2ВыпускОбороты.Номенклатура = &Номенклатура
	|	И к2ВыпускОбороты.Характеристика = &Характеристика
	|	И к2ВыпускОбороты.Серия = &Серия";
	
	Возврат Не Запрос.Выполнить().Пустой();
	
КонецФункции

&НаКлиенте
Процедура ЗавершениеОперацииСохранение()
	
	Если Не ИмеютсяВспомогательныеМатериалы
		Или Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаВспомогательныеМатериалы Тогда
		
		Закрыть();
		
	Иначе
		
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаВспомогательныеМатериалы;
		
	КонецЕсли;
	
КонецПроцедуры

#Область Выпуск

&НаСервере
Функция СтруктураЗаполнения_ПереработкаВыпускаМатериала()
	
	СтруктураРеквизитовСмены = СтруктураРеквизитовСмены();
	
	Структура = Новый Структура;
	Структура.Вставить("СпособСоздания", Перечисления.к2СпособыСозданияДокументов.УчетРасходаИнгредиентов);
	Структура.Вставить("ВидОперации", Перечисления.к2ВидыОперацийПереработки.Переработка);
	Структура.Вставить("Статус", Перечисления.к2СтатусыПереработки.Выполнено);
	Структура.Вставить("Дата", ДатаПереработки(СтруктураРеквизитовСмены));
	Структура.Вставить("ДатаСмены", Объект.ДатаСмены);
	Структура.Вставить("Смена", Объект.Смена);
	Структура.Вставить("РабочийЦентр", РабочийЦентрМатериалОсновной);
	Структура.Вставить("Ответственный", Пользователи.ТекущийПользователь());
	Структура.Вставить("Выпуск", СтруктураЗаполнения_Выпуск_ПереработкаВыпускаМатериала());
	
	Возврат Структура;
	
КонецФункции

&НаСервере
Функция СтруктураЗаполнения_Выпуск_ПереработкаВыпускаМатериала()
	
	ТаблицаВыпуск = к2ОбщегоНазначения.ШаблонТабличнойЧасти(Метаданные.Документы.к2Переработка.ТабличныеЧасти.Выпуск);
	
	НоваяСтрока = ТаблицаВыпуск.Добавить();
	НоваяСтрока.Номенклатура = НоменклатураМатериалОсновной;
	НоваяСтрока.Характеристика = ХарактеристикаМатериалОсновной;
	НоваяСтрока.Серия = СерияМатериалОсновной;
	НоваяСтрока.Количество = КоличествоМатериалОсновной;
	НоваяСтрока.Количество1 = Количество1МатериалОсновной;
	НоваяСтрока.Количество2 = Количество2МатериалОсновной;
	НоваяСтрока.Склад = СкладМатериалОсновной;
	НоваяСтрока.Ячейка = ЯчейкаМатериалОсновной;
	НоваяСтрока.Спецификация = СпецификацияМатериалОсновной;
	НоваяСтрока.ПроизводственноеЗадание = ПроизводственноеЗаданиеМатериалОсновной;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПроверитьЭтапПоВладельцуВыходномуИзделию", НоваяСтрока.Этап);
	СтруктураДействий.Вставить("ЗаполнитьАналитикуУчетаНоменклатуры");
	
	к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, Неопределено);
	
	Возврат ТаблицаВыпуск;
	
КонецФункции

&НаСервере
Функция СоздатьПереработкаВыпускаМатериала(ДанныеЗаполнения, ТекстОшибки, Отказ)
	
	Если Отказ Тогда
		Возврат Документы.к2Переработка.ПустаяСсылка();
	КонецЕсли;
	
	ДокументОбъект = Документы.к2Переработка.СоздатьДокумент();
	
	ДокументОбъект.Заполнить(ДанныеЗаполнения);
	
	к2ПроведениеСервер.ПровестиДокумент(ДокументОбъект, , ТекстОшибки, Отказ);
	
	Возврат ДокументОбъект.Ссылка;
	
КонецФункции

#КонецОбласти

#Область Расход

&НаСервере
Функция СтруктураЗаполнения_ПереработкаРасходаМатериала()
	
	СтруктураРеквизитовСмены = СтруктураРеквизитовСмены();
	
	Структура = Новый Структура;
	Структура.Вставить("СпособСоздания", Перечисления.к2СпособыСозданияДокументов.УчетРасходаИнгредиентов);
	Структура.Вставить("ВидОперации", Перечисления.к2ВидыОперацийПереработки.Переработка);
	Структура.Вставить("Статус", Перечисления.к2СтатусыПереработки.Начат);
	Структура.Вставить("Дата", ДатаПереработки(СтруктураРеквизитовСмены));
	Структура.Вставить("ДатаСмены", Объект.ДатаСмены);
	Структура.Вставить("Смена", Объект.Смена);
	Структура.Вставить("РабочийЦентр", Объект.РабочийЦентр);
	Структура.Вставить("Ответственный", Пользователи.ТекущийПользователь());
	Структура.Вставить("Выпуск", СтруктураЗаполнения_Выпуск_ПереработкаРасходаМатериала());
	
	Возврат Структура;
	
КонецФункции

&НаСервере
Функция СтруктураЗаполнения_Выпуск_ПереработкаРасходаМатериала()
	
	ТаблицаВыпуск = к2ОбщегоНазначения.ШаблонТабличнойЧасти(Метаданные.Документы.к2Переработка.ТабличныеЧасти.Выпуск);
	
	НоваяСтрока = ТаблицаВыпуск.Добавить();
	НоваяСтрока.Номенклатура = Объект.Номенклатура;
	НоваяСтрока.Характеристика = Объект.Характеристика;
	НоваяСтрока.Серия = Объект.Серия;
	НоваяСтрока.Спецификация = Объект.Спецификация;
	НоваяСтрока.Этап = Объект.Этап;
	НоваяСтрока.ПроизводственноеЗадание = Объект.ПроизводственноеЗадание;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьАналитикуУчетаНоменклатуры");
	
	к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, Неопределено);
	
	Возврат ТаблицаВыпуск;
	
КонецФункции

&НаСервере
Функция СоздатьПереработкаРасходаМатериала(ДанныеЗаполнения, ТекстОшибки, Отказ)
	
	Документ = Документы.к2Переработка.ПустаяСсылка();
	
	Если Отказ Тогда
		Возврат Документ;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.Переработка) Тогда
		
		ДокументОбъект = Объект.Переработка.ПолучитьОбъект();
		ДатаРасходаВспомогательныеМатериалыЗаполнить(ДокументОбъект);
		
	Иначе
		
		ДокументОбъект = Документы.к2Переработка.СоздатьДокумент();
		ДокументОбъект.Заполнить(ДанныеЗаполнения);
		
	КонецЕсли;
	
	Если н_УказыватьОсновнойМатериал И Не ИмеетсяОсновнойМатериал Тогда
		ОсновнойМатериалДобавить(ДокументОбъект);
	КонецЕсли;
	
	Если к2ПроведениеСервер.ПровестиДокумент(ДокументОбъект, , ТекстОшибки, Отказ) Тогда
		Документ = ДокументОбъект.Ссылка;
	КонецЕсли;
	
	Возврат Документ;
	
КонецФункции

&НаСервере
Процедура ДатаРасходаВспомогательныеМатериалыЗаполнить(ДокументОбъект)
	
	Для Каждого Строка Из Объект.РаспределениеМатериалов Цикл
		
		Отбор = Новый Структура("Номенклатура, Характеристика, НомерПакета");
		ЗаполнитьЗначенияСвойств(Отбор, Строка);
		
		Для Каждого Распределение Из ДокументОбъект.РаспределениеМатериалов.НайтиСтроки(Отбор) Цикл
			Распределение.ДатаРасхода = Строка.ДатаРасхода;
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ОсновнойМатериалДобавить(ДокументОбъект)
	
	НоваяСтрока = ДокументОбъект.РаспределениеМатериалов.Добавить();
	
	НоваяСтрока.Номенклатура = НоменклатураМатериалОсновной;
	НоваяСтрока.Характеристика = ХарактеристикаМатериалОсновной;
	НоваяСтрока.Серия = СерияМатериалОсновной;
	НоваяСтрока.Количество = КоличествоМатериалОсновной;
	НоваяСтрока.Количество1 = Количество1МатериалОсновной;
	НоваяСтрока.Количество2 = Количество2МатериалОсновной;
	НоваяСтрока.Склад = СкладМатериалОсновной;
	НоваяСтрока.Ячейка = ЯчейкаМатериалОсновной;
	НоваяСтрока.АналитикаУчетаВыпуска = АналитикаУчетаВыпуска();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьАналитикуУчетаНоменклатуры");
	
	к2ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТЧ(НоваяСтрока, СтруктураДействий, Неопределено);
	
КонецПроцедуры

&НаСервере
Функция АналитикаУчетаВыпуска()
	
	Структура = Новый Структура;
	Структура.Вставить("Номенклатура", Объект.Номенклатура);
	Структура.Вставить("Характеристика", Объект.Характеристика);
	Структура.Вставить("Серия", Объект.Серия);
	
	Возврат РегистрыСведений.к2АналитикаУчетаНоменклатуры.ПолучитьКлючАналитикиУчетаПоНоменклатуре(Структура);
	
КонецФункции

#КонецОбласти

#Область Сохранить_Прочее

&НаСервере
Функция СтруктураРеквизитовСмены()
	
	Возврат Справочники.к2Смены.СтруктураПериодаСмены(Объект.ДатаСмены, Объект.Смена);
	
КонецФункции

&НаСервере
Функция ДатаПереработки(СтруктураРеквизитовСмены)
	
	Если ТекущаяДатаСеанса() > СтруктураРеквизитовСмены.ДатаОкончанияСмены Тогда
		Дата = СтруктураРеквизитовСмены.ДатаОкончанияСмены;
	Иначе
		Дата = ТекущаяДатаСеанса();
	КонецЕсли;
	
	Возврат Дата;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область ПереходСтраниц

&НаСервере
Процедура ПерейтиНаСтраницу(Знач Страница)
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Страница;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

#Область УправлениеЭлементамиФормы

&НаСервере
Процедура УстановитьТекущуюСтраницу()
	
	Если Не ИмеютсяВспомогательныеМатериалы Тогда
		ПерейтиНаСтраницу(Элементы.СтраницаОсновнойМатериал);
	Иначе
		ПерейтиНаСтраницу(Элементы.СтраницаВспомогательныеМатериалы);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	ИспользоватьХарактеристики = глРеквизит(НоменклатураМатериалОсновной, "ВидНоменклатуры.ИспользоватьХарактеристики");
	Элементы.ХарактеристикаМатериалОсновной.Видимость = ИспользоватьХарактеристики;
	
	ИспользоватьСерии = глРеквизит(НоменклатураМатериалОсновной, "ВидНоменклатуры.ИспользоватьСерии");
	Элементы.СерияМатериалОсновной.Видимость = ИспользоватьСерии;
	
	СкладИспользоватьАдресноеХранение = глРеквизит(СкладМатериалОсновной, "ИспользоватьАдресноеХранение");
	Элементы.ЯчейкаМатериалОсновной.Видимость = СкладИспользоватьАдресноеХранение;
	
	ВариантДопЕдиниц = глРеквизит(НоменклатураМатериалОсновной, "ИспользованиеДопЕдиницИзмерения");
	
	ОднаДопЕдиница = ВариантДопЕдиниц = Перечисления.к2ИспользованиеДопЕдиницИзмерения.Одна;
	ДвеДопЕдиницы = ВариантДопЕдиниц = Перечисления.к2ИспользованиеДопЕдиницИзмерения.Две;
	
	Элементы.ГруппаКоличество1.Видимость = ОднаДопЕдиница Или ДвеДопЕдиницы;
	Элементы.ГруппаКоличество2.Видимость = ДвеДопЕдиницы;
	
	Элементы.КомандаУказатьВспомогательныеМатериалы.Видимость = ЗначениеЗаполнено(Объект.Переработка)
		И Не Объект.РаспределениеМатериалов.Количество() = 0;
	Элементы.КомандаУказатьСмесь.Видимость = н_УказыватьОсновнойМатериал;
	
	Элементы.СтраницаОсновнойМатериал.ТолькоПросмотр = ИмеетсяОсновнойМатериал;
	
	Элементы.ГруппаШтрихКод.Видимость = Не СканерШтрихКодовПодключен;
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ПроверитьВозможностьОткрытияФормы(Отказ)
	
	ТекстОшибки = "";
	
	Если (Не н_УказыватьОсновнойМатериал
			Или ИмеетсяОсновнойМатериал)
		И Не ИмеютсяВспомогательныеМатериалы Тогда
		
		ТекстСообщения = НСтр("ru = 'Невозможно открыть вкладку Ингредиенты.'");
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	КонецЕсли;
	
	Если н_УказыватьОсновнойМатериал И ОшибкаНесколькоОсновныхМатериалов Тогда
		
		ТекстСообщения = НСтр("ru = 'Выбрана неверная переработка.'");
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	КонецЕсли;
	
	Если н_СоздаватьВыпускОсновногоМатериала Тогда
		
		Если Не ЗначениеЗаполнено(ПроизводственноеЗаданиеМатериалОсновной) Тогда
			
			ТекстСообщения = НСтр("ru = 'Не определено производственное задание для выпуска смеси.'");
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
			
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(РабочийЦентрМатериалОсновной) Тогда
			
			ТекстСообщения = НСтр("ru = 'Не определен рабочий центр для выпуска смеси.'");
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
			
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(СпецификацияМатериалОсновной) Тогда
			
			ТекстСообщения = НСтр("ru = 'Не определена спецификация для выпуска смеси.'");
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ТекстОшибки) Тогда
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки, Отказ);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ИсключитьНепроверяемыеРеквизиты(ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	НепроверяемыеРеквизиты.Добавить("Объект");
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьУказаниеОсновногоМатериала(Отказ)
	
	Если Не н_УказыватьОсновнойМатериал Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(НоменклатураМатериалОсновной) Тогда
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Материал""'"), , , , Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(СкладМатериалОсновной) Тогда
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Склад""'"), , , , Отказ);
	КонецЕсли;
	
	ИспользоватьХарактеристики = глРеквизит(НоменклатураМатериалОсновной, "ВидНоменклатуры.ИспользоватьХарактеристики");
	
	Если ИспользоватьХарактеристики И Не ЗначениеЗаполнено(ХарактеристикаМатериалОсновной) Тогда
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Характеристика""'"), , , , Отказ);
	КонецЕсли;
	
	ИспользоватьСерии = глРеквизит(НоменклатураМатериалОсновной, "ВидНоменклатуры.ИспользоватьСерии");
	
	Если ИспользоватьСерии И Не ЗначениеЗаполнено(СерияМатериалОсновной) Тогда
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Серия""'"), , , , Отказ);
	КонецЕсли;
	
	ВариантДопЕдиниц = глРеквизит(НоменклатураМатериалОсновной, "ИспользованиеДопЕдиницИзмерения");
	
	Если Не ЗначениеЗаполнено(КоличествоМатериалОсновной) Тогда
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Количество""'"), , , , Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Количество1МатериалОсновной)
		И (ВариантДопЕдиниц = Перечисления.к2ИспользованиеДопЕдиницИзмерения.Одна
		Или ВариантДопЕдиниц = Перечисления.к2ИспользованиеДопЕдиницИзмерения.Две) Тогда
		
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Количество1""'"), , , , Отказ);
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Количество2МатериалОсновной)
		И ВариантДопЕдиниц = Перечисления.к2ИспользованиеДопЕдиницИзмерения.Две Тогда
		
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Количество2""'"), , , , Отказ);
		
	КонецЕсли;
	
	СкладИспользоватьАдресноеХранение = глРеквизит(СкладМатериалОсновной, "ИспользоватьАдресноеХранение");
	
	Если СкладИспользоватьАдресноеХранение И Не ЗначениеЗаполнено(ЯчейкаМатериалОсновной) Тогда
		ОбщегоНазначения.СообщитьПользователю(НСтр("ru='Не заполнено поле ""Ячейка""'"), , , , Отказ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти