#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает значение типа ВСД по ссылке. Значение используется для обмена со шлюзом.
//
// Параметры:
//  пТип - Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий - Тип.
//
// Возвращаемое значение:
//  Строка - значение.
//
Функция ЗначениеПоСсылке( Знач пТип ) Экспорт
	
	Если пТип = Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.СопроводительныйДокумент Тогда
		
		Возврат 1;
	
	ИначеЕсли пТип = Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.ПредшествующийДокумент Тогда
		
		Возврат 2;
	
	ИначеЕсли пТип = Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.СледующийДокумент Тогда
		
		Возврат 3;
	
	ИначеЕсли пТип = Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.ЗаменяемыйДокумент Тогда
		
		Возврат 4;
	
	ИначеЕсли пТип = Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.ЗамещающийДокумент Тогда
		
		Возврат 5;
	
	ИначеЕсли пТип = Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.СвязанныйДокумент Тогда
		
		Возврат 6;
	
	Иначе
		
		ВызватьИсключение СтрШаблон( НСтр( "ru='Не задано значение для типа ВСД %1'" ), пТип );
	
	КонецЕсли;
	
КонецФункции

// Возвращает ссылку по значению
//
// Параметры:
//  пЗначение - Строка - значение.
//
// Возвращаемое значение:
//  Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий - Неопределено, если не удалось установить соответствие.
//
Функция СсылкаПоЗначению( Знач пЗначение ) Экспорт
	
	Если ТипЗнч( пЗначение ) = Тип( "Строка" ) Тогда
		
		пЗначение = Число( пЗначение );
		
	КонецЕсли;
	
	Если пЗначение = 1 Тогда
		
		Возврат Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.СопроводительныйДокумент;
	
	ИначеЕсли пЗначение = 2 Тогда
		
		Возврат Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.ПредшествующийДокумент;
	
	ИначеЕсли пЗначение = 3 Тогда
		
		Возврат Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.СледующийДокумент;
	
	ИначеЕсли пЗначение = 4 Тогда
		
		Возврат Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.ЗаменяемыйДокумент;
	
	ИначеЕсли пЗначение = 5 Тогда
		
		Возврат Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.ЗамещающийДокумент;
	
	ИначеЕсли пЗначение = 6 Тогда
		
		Возврат Перечисления.к2ТипОтношенияМеждуДокументами_Меркурий.СвязанныйДокумент;
	
	Иначе
		
		ВызватьИсключение СтрШаблон( НСтр( "ru='Не задана ссылка для типа ВСД %1'" ), пЗначение );
	
	КонецЕсли;
	
КонецФункции

#КонецОбласти

#КонецЕсли