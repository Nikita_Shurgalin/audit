#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем

// Заполняет список команд создания на основании.
//
// Параметры:
//   КомандыСоздатьНаОсновании - ТаблицаЗначений - состав полей см. в функции ВводНаОсновании.СоздатьКоллекциюКомандСоздатьНаОсновании.
//	 Параметры - Структура - Дополнительные параметры.
//
Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСоздатьНаОсновании, Параметры) Экспорт
	
	Документы.КорректировкаПоступления.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	Документы.к2ВозвратТоваровПоставщику.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	Документы.к2ПеремещениеТоваров.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	Документы.к2ПоступлениеДополнительныхЗатрат.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	
КонецПроцедуры

// Определяет список команд создания на основании.
//
// Параметры:
//   КомандыСоздатьНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры к2СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
// Возвращаемое значение:
//  ТаблицаЗначений - Команды создания на основании.
//
Функция ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании) Экспорт
	
	Если ПравоДоступа("Добавление", Метаданные.Документы.ПоступлениеТоваровУслуг) Тогда
		
		КомандаСоздатьНаОсновании = КомандыСоздатьНаОсновании.Добавить();
		КомандаСоздатьНаОсновании.Менеджер = Метаданные.Документы.ПоступлениеТоваровУслуг.ПолноеИмя();
		КомандаСоздатьНаОсновании.Представление = ОбщегоНазначения.ПредставлениеОбъекта(Метаданные.Документы.ПоступлениеТоваровУслуг);
		КомандаСоздатьНаОсновании.РежимЗаписи = "Проводить";
		
		Возврат КомандаСоздатьНаОсновании;
		
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

// Процедура инициализации данных документа для механизма проведения.
//
// Параметры:
//  ДокументСсылка - ДокументСсылка - Ссылка на документ.
//  ДополнительныеСвойства - Структура - Дополнительные свойства для проведения.
//  Регистры - Строка, Структура, Неопределено - список регистров, разделенных запятой, или структура, в ключах которой
//                                                  - имена регистров Если неопределено - то всегда возвращается ИСТИНА.
//
Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	////////////////////////////////////////////////////////////////////////////
	// Создадим запрос инициализации движений
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДополнительныеСвойства);
	
	////////////////////////////////////////////////////////////////////////////
	// Сформируем текст запроса
	
	ТекстыЗапроса = Новый СписокЗначений;
	
	ТекстЗапроса_ПодготовитьВременныеТаблицы(Запрос, ТекстыЗапроса);
	
	ТекстЗапросаТаблица_к2ПоступленияТоваровПлановые(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2ПоступленияТоваров(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2ТоварыНаСкладах(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2СвободныеОстатки(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2ТоварыБезТары(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2ЗаказыПоставщикам(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблица_к2МногооборотнаяТараКлиента(Запрос, ТекстыЗапроса, Регистры);
	
	Если ДополнительныеСвойства.ДляПроведения.ВестиУчетПоТоварамОрганизации Тогда
		
		ТекстЗапросаТаблица_к2ТоварыОрганизации(Запрос, ТекстыЗапроса, Регистры);
		
	КонецЕсли;
	
	// Финансовый результат
	Если ДополнительныеСвойства.ДляПроведения.ВключенФинансовыйРезультат Тогда
		
		ТекстЗапросаТаблица_к2Запасы(Запрос, ТекстыЗапроса, Регистры);
		ТекстЗапросаТаблица_к2Затраты(Запрос, ТекстыЗапроса, Регистры);
		
	КонецЕсли;
	
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

#КонецОбласти

// Функция определяет есть ли в документе отклонение заявленных количеств от фактических
//
// Параметры:
//  ДокументСсылка - ДокументСсылка.ПоступлениеТоваровУслуг - ссылка на документ поступления.
// 
// Возвращаемое значение:
//  Булево - признак наличия отклонений в документе.
//
Функция ВДокументеЕстьОтклонениеЗаявленныхЗначенийОтФактических(ДокументСсылка) Экспорт
	
	ЕстьОтклонения = Ложь;
	
	Для Каждого Строка Из ДокументСсылка.ТоварыЗаявлено Цикл
		
		ФактическиеДанные = Документы.ПоступлениеТоваровУслуг.ФактическиеДанныеПоКлючуСтрокиЗаявлено(ДокументСсылка, Строка.КлючСтроки);
		
		Если Не ФактическиеДанные.СуммаСНДС - Строка.СуммаСНДС = 0 Тогда
			
			ЕстьОтклонения = Истина;
			Прервать;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ЕстьОтклонения;
	
КонецФункции

// Возвращает строковое представление строки табличной части.
// Строка должна содержать поля "Номенклатура, Характеристика, Серия, Упаковка, НомерСтроки"
//
// Параметры:
//  ДанныеСтроки - ДанныеФормыЭлементКоллекции, Структура - Источник данных для получения представления.
// 
// Возвращаемое значение:
//  Строка - представление строки таблицы.
//
Функция ПредставлениеСтрокиТаблицы(ДанныеСтроки) Экспорт
	
	ШаблонПредставления = НСтр("ru = '%1 / %2'");
	
	ПредставлениеНоменклатуры = НоменклатураКлиентСервер.ПредставлениеНоменклатуры(ДанныеСтроки.Номенклатура
		, ДанныеСтроки.Характеристика
		, ДанныеСтроки.Серия
		, ДанныеСтроки.Упаковка);
	
	Представление = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонПредставления
		, ДанныеСтроки.НомерСтроки
		, ПредставлениеНоменклатуры);
	
	Возврат Представление;
	
КонецФункции

#КонецОбласти

#КонецЕсли

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)

	к2ДокументыКлиентСервер.ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка);

КонецПроцедуры

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)

	к2ДокументыКлиентСервер.ОбработкаПолученияПредставления(НСтр("ru='Поступление товаров и услуг'"),
															Данные,
															Представление,
															СтандартнаяОбработка);

КонецПроцедуры

#КонецОбласти

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

#Область СлужебныйПрограммныйИнтерфейс_Печать

Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	к2Печать.ДобавитьКомандуПечатиЭтикеткиПартии(КомандыПечати);
	к2Печать.ДобавитьКомандуПечатиЭтикеткиПартииПолутуши(КомандыПечати);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс_Прочее

Функция ФактическиеДанныеПоКлючуСтрокиЗаявлено(Ссылка, КлючСтрокиЗаявлено) Экспорт
	
	СтруктураПоиска = Новый Структура("КлючСтрокиТоварыЗаявлено", КлючСтрокиЗаявлено);
	
	МассивСтрок = Ссылка.Товары.НайтиСтроки(СтруктураПоиска);
	
	СуммаСНДС = к2КоллекцииКлиентСервер.ИтогМассиваСтрок(МассивСтрок, "СуммаСНДС");
	Количество = к2КоллекцииКлиентСервер.ИтогМассиваСтрок(МассивСтрок, "Количество");
	
	Структура = Новый Структура;
	Структура.Вставить("СуммаСНДС", СуммаСНДС);
	Структура.Вставить("Количество", Количество);
	
	Возврат Структура;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДополнительныеСвойства)
	
	СтруктураИменПараметров = Новый Структура;
	СтруктураИменПараметров.Вставить("Ссылка");
	СтруктураИменПараметров.Вставить("Дата");
	СтруктураИменПараметров.Вставить("Статус");
	СтруктураИменПараметров.Вставить("Склад");
	СтруктураИменПараметров.Вставить("Ячейка");
	СтруктураИменПараметров.Вставить("Организация");
	СтруктураИменПараметров.Вставить("Поставщик");
	СтруктураИменПараметров.Вставить("ПлановаяДатаПоставки");
	СтруктураИменПараметров.Вставить("НастройкаКонтроляОстатков", "Склад.НастройкаКонтроляОстатков");
	СтруктураИменПараметров.Вставить("ОтключитьУчетПоДопЕдиницамИзмерения", "Склад.ОтключитьУчетПоДопЕдиницамИзмерения");
	СтруктураИменПараметров.Вставить("Подразделение");
	СтруктураИменПараметров.Вставить("Заказ");
	СтруктураИменПараметров.Вставить("ФактическаяДатаПоставки");
	СтруктураИменПараметров.Вставить("ТочкаДоставки");
	СтруктураИменПараметров.Вставить("ВернутьМногооборотнуюТару");
	СтруктураИменПараметров.Вставить("Валюта");
	СтруктураИменПараметров.Вставить("СуммаДокумента");
	СтруктураИменПараметров.Вставить("ОтнестиУслугиВСебестоимость");
	
	Параметры = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДополнительныеСвойства.ДляПроведения.Ссылка, СтруктураИменПараметров);
	
	Для Каждого Параметр Из Параметры Цикл
		
		Запрос.УстановитьПараметр(Параметр.Ключ, Параметр.Значение);
		
	КонецЦикла;
	
	ДополнительныеСвойства.ДляПроведения.Вставить("ВестиУчетПоТоварамОрганизации",
												  ПолучитьФункциональнуюОпцию("к2ВестиУчетПоТоварамОрганизации"));
	ДополнительныеСвойства.ДляПроведения.Вставить("ВключенФинансовыйРезультат", к2ФинансовыйРезультат.Включен());
	
КонецПроцедуры

Процедура ТекстЗапроса_ПодготовитьВременныеТаблицы(Запрос, ТекстыЗапроса)
	
	Запрос.УстановитьПараметр("СчетУчета", ПланыСчетов.к2ФинансовыйРезультат.СырьеИМатериалы);
	
	Запрос.УстановитьПараметр("Операция", Справочники.к2ОперацииДокументов.ПоступлениеОтПоставщика);
	Запрос.УстановитьПараметр("ПояснениеТоваров", к2ФинансовыйРезультат.Пояснение_ПоступлениеТоваров());
	Запрос.УстановитьПараметр("ПояснениеУслуги", к2ФинансовыйРезультат.Пояснение_ПоступлениеУслуг());
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	Товары.НомерСтроки КАК НомерСтроки,
	|	&Дата КАК Период,
	|	&ФактическаяДатаПоставки КАК ДатаПоставки,
	|	&Ссылка КАК ДокументПоступление,
	|	&Организация КАК Организация,
	|	&Поставщик КАК Поставщик,
	|	&Склад КАК Склад,
	|	&Подразделение КАК Подразделение,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Упаковка КАК Упаковка,
	|	Товары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	Товары.Количество КАК Количество,
	|	Товары.Количество1 КАК Количество1,
	|	Товары.Количество2 КАК Количество2,
	|	ВЫБОР
	|		КОГДА &ОтнестиУслугиВСебестоимость
	|			ТОГДА Товары.СуммаСНДС + Товары.СуммаУслуг
	|		ИНАЧЕ Товары.СуммаСНДС
	|	КОНЕЦ КАК Сумма,
	|	Товары.СтавкаНДС КАК СтавкаНДС,
	|	Товары.ЦенаСУчетомСкидок КАК Цена,
	|	ВЫБОР
	|		КОГДА Товары.Номенклатура.СчетУчетаЗапасов = ЗНАЧЕНИЕ(ПланСчетов.к2ФинансовыйРезультат.ПустаяСсылка)
	|			ТОГДА &СчетУчета
	|		ИНАЧЕ Товары.Номенклатура.СчетУчетаЗапасов
	|	КОНЕЦ КАК СчетУчета,
	|	&ПояснениеТоваров КАК Пояснение,
	|	&Операция КАК Операция
	|ПОМЕСТИТЬ втТовары
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И НЕ &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.НеПринято)";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, "");
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	Услуги.НомерСтроки КАК НомерСтроки,
	|	&Дата КАК Период,
	|	&Организация КАК Организация,
	|	&Ссылка КАК Регистратор,
	|	&Заказ КАК Заказ,
	|	&Склад КАК Склад,
	|	&Поставщик КАК Поставщик,
	|	Услуги.Номенклатура КАК Номенклатура,
	|	Услуги.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	Услуги.Количество КАК Количество,
	|	Услуги.СуммаСНДС КАК Сумма,
	|	Услуги.ЦенаСУчетомСкидок КАК Цена,
	|	Услуги.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	Услуги.Подразделение КАК Подразделение,
	|	Услуги.СтатьяЗатрат КАК СтатьяЗатрат,
	|	Услуги.АналитикаЗатрат КАК АналитикаЗатрат,
	|	&Операция КАК Операция,
	|	&ПояснениеУслуги КАК Пояснение,
	|	&ОтнестиУслугиВСебестоимость КАК ОтнестиУслугиВСебестоимость
	|ПОМЕСТИТЬ втУслуги
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Услуги КАК Услуги
	|ГДЕ
	|	Услуги.Ссылка = &Ссылка
	|	И НЕ &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.НеПринято)";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, "");
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ПоступленияТоваровПлановые(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ПоступленияТоваровПлановые";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	#Область ТекстЗапроса
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	&Ссылка КАК ДокументПоступление,
	|	&Организация КАК Организация,
	|	&Поставщик КАК Поставщик,
	|	&Склад КАК Склад,
	|	&ПлановаяДатаПоставки КАК ДатаПоставки,
	|	ТоварыПлан.Номенклатура КАК Номенклатура,
	|	ТоварыПлан.Характеристика КАК Характеристика,
	|	ТоварыПлан.Упаковка КАК Упаковка,
	|	ТоварыПлан.КоличествоУпаковок КАК КоличествоУпаковок,
	|	ТоварыПлан.Количество КАК Количество,
	|	ТоварыПлан.СуммаСНДС КАК Сумма,
	|	ТоварыПлан.ЦенаСУчетомСкидок КАК Цена
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.ТоварыПлан КАК ТоварыПлан
	|ГДЕ
	|	ТоварыПлан.Ссылка = &Ссылка";
	#КонецОбласти
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ПоступленияТоваров(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ПоступленияТоваров";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	&ФактическаяДатаПоставки КАК ДатаПоставки,
	|	&Ссылка КАК ДокументПоступление,
	|	&Организация КАК Организация,
	|	&Поставщик КАК Поставщик,
	|	&Склад КАК Склад,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Упаковка КАК Упаковка,
	|	Товары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	Товары.Количество КАК Количество,
	|	Товары.Количество1 КАК Количество1,
	|	Товары.Количество2 КАК Количество2,
	|	Товары.СуммаСНДС КАК Сумма,
	|	Товары.ЦенаСУчетомСкидок КАК Цена
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И НЕ &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.НеПринято)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	втУслуги.Период КАК Период,
	|	&ФактическаяДатаПоставки КАК ДатаПоставки,
	|	&Ссылка КАК ДокументПоступление,
	|	втУслуги.Организация КАК Организация,
	|	втУслуги.Поставщик КАК Поставщик,
	|	втУслуги.Склад КАК Склад,
	|	втУслуги.Номенклатура КАК Номенклатура,
	|	Значение(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка) КАК Характеристика,
	|	Значение(Справочник.к2СерииНоменклатуры.ПустаяСсылка) КАК Серия,
	|	ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка) КАК Упаковка,
	|	втУслуги.Количество,
	|	втУслуги.Количество,
	|	0,
	|	0,
	|	втУслуги.Сумма КАК Сумма,
	|	втУслуги.Цена КАК Цена
	|ИЗ
	|	втУслуги КАК втУслуги";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ТоварыНаСкладах(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ТоварыНаСкладах";
	
	#Область ТекстЗапроса
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&ФактическаяДатаПоставки КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Склад КАК Склад,
	|	&Ячейка КАК Ячейка,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Количество КАК Количество,
	|	ВЫБОР
	|		КОГДА &ОтключитьУчетПоДопЕдиницамИзмерения
	|			ТОГДА 0
	|		ИНАЧЕ Товары.Количество1
	|	КОНЕЦ КАК Количество1,
	|	ВЫБОР
	|		КОГДА &ОтключитьУчетПоДопЕдиницамИзмерения
	|			ТОГДА 0
	|		ИНАЧЕ Товары.Количество2
	|	КОНЕЦ КАК Количество2,
	|	&НастройкаКонтроляОстатков КАК НастройкаКонтроляОстатков
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И НЕ &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.НеПринято)";
	#КонецОбласти
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2СвободныеОстатки(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2СвободныеОстатки";
	
	#Область ТекстЗапроса
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&ФактическаяДатаПоставки КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Склад КАК Склад,
	|	&Ячейка КАК Ячейка,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Количество КАК Количество,
	|	ВЫБОР
	|		КОГДА &ОтключитьУчетПоДопЕдиницамИзмерения
	|			ТОГДА 0
	|		ИНАЧЕ Товары.Количество1
	|	КОНЕЦ КАК Количество1,
	|	ВЫБОР
	|		КОГДА &ОтключитьУчетПоДопЕдиницамИзмерения
	|			ТОГДА 0
	|		ИНАЧЕ Товары.Количество2
	|	КОНЕЦ КАК Количество2,
	|	&НастройкаКонтроляОстатков КАК НастройкаКонтроляОстатков
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И НЕ &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.НеПринято)";
	#КонецОбласти
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ТоварыБезТары(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ТоварыБезТары";
	
	#Область ТекстЗапроса
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&ФактическаяДатаПоставки КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Склад КАК Склад,
	|	&Ячейка КАК Ячейка,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Количество КАК Количество,
	|	ВЫБОР
	|		КОГДА &ОтключитьУчетПоДопЕдиницамИзмерения
	|			ТОГДА 0
	|		ИНАЧЕ Товары.Количество1
	|	КОНЕЦ КАК Количество1,
	|	ВЫБОР
	|		КОГДА &ОтключитьУчетПоДопЕдиницамИзмерения
	|			ТОГДА 0
	|		ИНАЧЕ Товары.Количество2
	|	КОНЕЦ КАК Количество2,
	|	&НастройкаКонтроляОстатков КАК НастройкаКонтроляОстатков
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И НЕ &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.НеПринято)";
	#КонецОбласти
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ТоварыОрганизации(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ТоварыОрганизации";
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	&Склад КАК Склад,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Организация КАК Организация,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Количество КАК Количество
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И &Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПриемки.Подтверждено)
	|	И Товары.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.к2ТипыНоменклатуры.ГотоваяПродукция)";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ЗаказыПоставщикам(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ЗаказыПоставщикам";
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&ПлановаяДатаПоставки КАК Период,
	|	&Заказ КАК Заказ,
	|	&Поставщик КАК Поставщик,
	|	&Подразделение КАК Подразделение,
	|	&ТочкаДоставки КАК ТочкаДоставки,
	|	ТоварыПлан.Упаковка КАК Упаковка,
	|	ТоварыПлан.КоличествоУпаковок КАК КоличествоУпаковок,
	|	ТоварыПлан.Количество КАК Количество,
	|	&Организация КАК Организация,
	|	&Склад КАК Склад,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	&Ссылка КАК Регистратор,
	|	ТоварыПлан.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.ТоварыПлан КАК ТоварыПлан
	|ГДЕ
	|	ТоварыПлан.Ссылка = &Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	втУслуги.Период КАК Период,
	|	втУслуги.Заказ КАК Заказ,
	|	втУслуги.Поставщик КАК Поставщик,
	|	&Подразделение КАК Подразделение,
	|	&ТочкаДоставки,
	|	ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка),
	|	втУслуги.Количество,
	|	втУслуги.Количество,
	|	втУслуги.Организация КАК Организация,
	|	втУслуги.Склад КАК Склад,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	втУслуги.Регистратор КАК Регистратор,
	|	втУслуги.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры
	|ИЗ
	|	втУслуги КАК втУслуги
	|ГДЕ
	|	НЕ втУслуги.Заказ = ЗНАЧЕНИЕ(Документ.ЗаказПоставщику.ПустаяСсылка)";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2МногооборотнаяТараКлиента(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2МногооборотнаяТараКлиента";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	#Область ТекстЗапроса
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Дата КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Количество КАК Количество,
	|	&Поставщик КАК Контрагент,
	|	&ТочкаДоставки КАК ТочкаДоставки
	|ИЗ
	|	Документ.ПоступлениеТоваровУслуг.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка = &Ссылка
	|	И Товары.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.к2ТипыНоменклатуры.МногооборотнаяТара)
	|	И &ВернутьМногооборотнуюТару";
	#КонецОбласти
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2Запасы(Запрос, ТекстыЗапроса, Регистры)

	ИмяРегистра = "к2Запасы";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	1 КАК Порядок,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	втТовары.НомерСтроки КАК НомерСтроки,
	|	втТовары.Период КАК Период,
	|	втТовары.Организация КАК Организация,
	|	втТовары.Склад КАК Склад,
	|	втТовары.СчетУчета КАК СчетУчета,
	|	втТовары.Номенклатура КАК Номенклатура,
	|	втТовары.Характеристика КАК Характеристика,
	|	втТовары.Серия КАК Серия,
	|	втТовары.Количество КАК Количество,
	|	втТовары.Сумма КАК Сумма,
	|	втТовары.СтавкаНДС КАК СтавкаНДС,
	|	втТовары.Подразделение КАК Подразделение,
	|	втТовары.Операция КАК Операция,
	|	втТовары.Пояснение КАК Пояснение,
	|	ИСТИНА КАК ФиксированнаяСтоимость
	|ИЗ
	|	втТовары КАК втТовары";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2Затраты(Запрос, ТекстыЗапроса, Регистры)

	ИмяРегистра = "к2Затраты";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	втУслуги.НомерСтроки КАК НомерСтроки,
	|	втУслуги.Период КАК Период,
	|	втУслуги.Организация КАК Организация,
	|	втУслуги.Подразделение КАК Подразделение,
	|	втУслуги.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	втУслуги.СтатьяЗатрат КАК СтатьяЗатрат,
	|	втУслуги.АналитикаЗатрат КАК АналитикаЗатрат,
	|	втУслуги.Сумма КАК Сумма,
	|	втУслуги.Пояснение КАК Пояснение,
	|	втУслуги.Операция КАК Операция
	|ИЗ
	|	втУслуги КАК втУслуги
	|ГДЕ
	|	НЕ втУслуги.ОтнестиУслугиВСебестоимость";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
