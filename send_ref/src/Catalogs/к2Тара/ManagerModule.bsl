#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

Функция НайтиТаруПоНомеру(Номер) Экспорт
	
	Возврат НайтиПоКоду(Номер, Истина);
	
КонецФункции

Функция НайтиПоШтрихкоду(ШтрихКод) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	к2Тара.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.к2Тара КАК к2Тара
	|ГДЕ
	|	к2Тара.ШтрихКод = &ШтрихКод
	|	И НЕ к2Тара.ПометкаУдаления";
	
	Запрос.УстановитьПараметр("ШтрихКод", ШтрихКод);
	
	Возврат к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).Ссылка;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт

	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.МенеджерПечати = "Обработка.к2ПечатьЭтикеткаТары";
	КомандаПечати.Идентификатор = "ПФ_MXL_ЭтикеткаТары";
	КомандаПечати.Представление = НСтр("ru = 'Этикетка тары'");
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Поля.Добавить("ШтрихКод");
	
КонецПроцедуры

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	Представление = Строка(Данные.ШтрихКод);
	
КонецПроцедуры

#КонецОбласти
