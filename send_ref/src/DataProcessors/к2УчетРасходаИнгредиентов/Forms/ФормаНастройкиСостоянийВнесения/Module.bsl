
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Параметры.Свойство("УровеньСкоро", УровеньСкоро);
	Параметры.Свойство("УровеньИстекает", УровеньИстекает);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	Если Не УровеньСкоро < УровеньИстекает Тогда 
		
		ТекстСообщения = НСтр("ru='Указаны некорректные значения'");
		
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаСохранить(Команда)
	
	Если Не ПроверитьЗаполнение() Тогда 
		Возврат;
	КонецЕсли;
	
	СтруктураЗакрытия = Новый Структура;
	СтруктураЗакрытия.Вставить("УровеньСкоро", УровеньСкоро);
	СтруктураЗакрытия.Вставить("УровеньИстекает", УровеньИстекает);
	
	Закрыть(СтруктураЗакрытия);
	
КонецПроцедуры

#КонецОбласти