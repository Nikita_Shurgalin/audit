
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Если Параметры.Свойство("Отбор") И Параметры.Отбор.Свойство("Владелец") Тогда
		Спецификация = Параметры.Отбор.Владелец;
		Элементы.Спецификация.ТолькоПросмотр = Истина;
	Иначе
		УстановитьОтборПоСпецификации(ЭтотОбъект);
	КонецЕсли;
	
	Если Параметры.Свойство("МассивЭтаповИсключения") Тогда 
		
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список
			, "Ссылка"
			, Параметры.МассивЭтаповИсключения
			, ВидСравненияКомпоновкиДанных.НеВСписке
			,
			, Истина);
		
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СпецификацияПриИзменении(Элемент)
	
	УстановитьОтборПоСпецификации(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьОтборПоСпецификации(Форма)

	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Форма.Список, "Владелец", Форма.Спецификация, , , Истина);

КонецПроцедуры

#КонецОбласти

#КонецОбласти
