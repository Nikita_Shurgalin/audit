#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ПередЗаписью(Отказ)
	
	Если ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьНомерВерсии();
	УстановитьНаименование();
	
	ОбработкаПометкиУдаления();
	
	Если ПометкаУдаления Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ГруппыПользователей.Количество() = 1 Тогда
		
		ГруппыПользователей.Очистить();
		ГруппыПользователей.Добавить().ГруппаПользователей = Справочники.ГруппыПользователей.ВсеПользователи;
		
	КонецЕсли;
	
	ДополнительныеСвойства.Вставить( "ЭтоНовый" , ЭтоНовый() );
	ДополнительныеСвойства.Вставить( "ИзмененРеквизитныйСостав" , ИзмененРеквизитныйСостав() );
	
	Если ИзмененРеквизитныйСостав() Тогда
		
		ЗаполнитьСКД();
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ЭтоГруппа
		ИЛИ ПометкаУдаления Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Если Не ОбменДанными.Загрузка Тогда
		
		Если ЗначениеЗаполнено( ПредыдущаяВерсия )
			И ДополнительныеСвойства.ЭтоНовый Тогда
			
			Обработки.упКопированиеВерсии.Скопировать( ПредыдущаяВерсия, Ссылка );
			
		КонецЕсли;
		
		СинхронизироватьКартуИМетаданные();
		
	КонецЕсли;
	
	Если ДополнительныеСвойства.ИзмененРеквизитныйСостав Тогда
		
		ОбновитьСКДВУсловиях();
		РегистрыСведений.упТаблицаРеквизитовВерсии.Заполнить( Ссылка );
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	НомерВерсии = 0;
	Статус = Перечисления.упСтатусыВерсий.Разрабатываемая;
	ПредыдущаяВерсия = ОбъектКопирования.Ссылка;
	
	Заполнить(ОбъектКопирования);
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ИсторияИзменения.Очистить();
	
	Если ТипЗнч( ДанныеЗаполнения ) = Тип( "Структура" )Тогда
		
		ЗаполнитьЗначенияСвойств( ЭтотОбъект , ДанныеЗаполнения );
		
		Если ДанныеЗаполнения.Свойство( "КартаМаршрута" )
			И Не ТипЗнч( ДанныеЗаполнения.КартаМаршрута ) = Тип("ХранилищеЗначения" ) Тогда
			
			КартаМаршрута = упГрафическаяСхема.ПолучитьХранилищеЗначенийПоСхеме( ДанныеЗаполнения.КартаМаршрута );
			
		КонецЕсли;
		
		Если ДанныеЗаполнения.Свойство( "РеквизитыБизнесПроцесса" ) Тогда
			
			РеквизитыБизнесПроцесса.Загрузить( ДанныеЗаполнения.РеквизитыБизнесПроцесса );
			
		КонецЕсли;
		
		ЗафиксироватьИзменение( НСтр( "ru='Программное создание'" ) );
		
	Иначе
		
		шаблонТекста = НСтр( "ru='Создание на основе ""%1""'" );
		текстСообщения = стрЗаполнить( шаблонТекста, ДанныеЗаполнения );
		
		ЗафиксироватьИзменение( текстСообщения );
		
	КонецЕсли;
	
	УстановитьНомерВерсии();
	УстановитьНаименование();
	
КонецПроцедуры

Процедура ОбработкаПометкиУдаления()
	
	Если ПометкаУдаления Тогда
		
		ЗафиксироватьИзменение( НСтр("ru='Пометка удаления';en='Deletion mark'" ) );
		
		СписатьВерсию( ЭтотОбъект );
		
		Если упДанныеБПВызовСервера.ОпубликованнаяВерсия( Владелец ) = Ссылка Тогда
			
			Справочники.упБизнесПроцессы.УстановитьОпубликованнуюВерсию( Владелец, Неопределено );
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если Не ПометкаУдаления = глРеквизит( Ссылка , "ПометкаУдаления" ) Тогда
		
		Для Каждого цОбъект Из Справочники.упВерсии.ВсеОбъектыВерсии( Ссылка, Ложь ) Цикл
			
			Если ТипЗнч( цОбъект ) = Тип("СправочникСсылка.упСостояния")
				ИЛИ ТипЗнч( цОбъект ) = Тип("СправочникСсылка.упПереходы") Тогда
				
				// сами пометятся
				
			Иначе
				
				объект = цОбъект.ПолучитьОбъект();
				
				Если Не объект = Неопределено Тогда
					
					объект.УстановитьПометкуУдаления( ПометкаУдаления ,Истина );
					
				КонецЕсли;
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры



Процедура СинхронизироватьКартуИМетаданные()
	
	лКартаМаршрута = Новый ГрафическаяСхема;
	упГрафическаяСхема.УстановитьСхемуПоХранилищуЗначений( КартаМаршрута , лКартаМаршрута );
	
	Если Не лКартаМаршрута = Неопределено
		И Справочники.упВерсии.ПолучитьТаблицуЭлементов( Ссылка ).Количество() = 0 Тогда
		
		// Карта и метаданные еще не синхронизированы
		// возникает при программной установке карты маршрута.
		упГрафическаяСхема.СинхронизацияРедактораИМетаданных( Ссылка , лКартаМаршрута );
		
	КонецЕсли;
	
КонецПроцедуры


Процедура УстановитьНомерВерсии()
	
	Если Не ЗначениеЗаполнено( НомерВерсии ) Тогда
		
		НомерВерсии = Справочники.упБизнесПроцессы.ПолучитьНомерПоследнейВерсии( Владелец , Ссылка ) + 1;
		
	КонецЕсли;
	
КонецПроцедуры	//УстановитьНомерВерсии

Процедура УстановитьНаименование()
	
	Если ЗначениеЗаполнено( Владелец ) Тогда
		
		Наименование = глРеквизит( Владелец , "Наименование" ) + ". Версия " + НомерВерсии;
		
	КонецЕсли;
	
КонецПроцедуры	//УстановитьНаименование

Процедура ЗафиксироватьИзменение( пИмяОперации ) Экспорт
	
	новСтрока = ИсторияИзменения.Добавить();
	новСтрока.Операция = пИмяОперации;
	новСтрока.ДатаОперации = ТекущаяДатаСеанса();
	новСтрока.Пользователь = глТекущийПользователь();
	
КонецПроцедуры	//ЗафиксироватьИзменение





Процедура СписатьВерсию( пВерсия = Неопределено ) Экспорт
	
	Если пВерсия = Неопределено Тогда
		
		лВерсия = ЭтотОбъект;
		
	Иначе
		
		лВерсия = пВерсия;
		
	КонецЕсли;
	
	Если упДанныеБПВызовСервера.ОпубликованнаяВерсия( Владелец ) = глРеквизит( лВерсия, "Ссылка" ) Тогда
		
		Справочники.упБизнесПроцессы.УстановитьОпубликованнуюВерсию( Владелец, Неопределено );
		
	КонецЕсли;
	
	СменитьСтатусВерсии( лВерсия, Перечисления.упСтатусыВерсий.Устаревшая, НСтр( "ru='Списание'" ) );
	
КонецПроцедуры

Процедура ВосстановитьВерсию( пВерсия = Неопределено ) Экспорт
	
	Если пВерсия = Неопределено Тогда
		
		лВерсия = ЭтотОбъект;
		
	Иначе
		
		лВерсия = пВерсия;
		
	КонецЕсли;
	
	Если Не глРеквизит( лВерсия , "Статус" ) = Перечисления.упСтатусыВерсий.Устаревшая Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	|	упСессии.Ссылка
	|ИЗ
	|	Справочник.упСессии КАК упСессии
	|ГДЕ
	|	упСессии.Владелец = &Владелец";
	
	Запрос.УстановитьПараметр("Владелец", Ссылка );
	
	Если Не Запрос.Выполнить().Пустой() Тогда
		
		ВызватьИсключение НСтр( "ru='Запрещено восстанавливать версии, по которым существуют сессии'" );
		
	КонецЕсли;
	
	СменитьСтатусВерсии( лВерсия, Перечисления.упСтатусыВерсий.Разрабатываемая, НСтр( "ru='Восстановление'" ) );
	
КонецПроцедуры

Процедура ОпубликоватьВерсию() Экспорт
	
	СписатьПредыдущиеВерсии();
	
	Справочники.упБизнесПроцессы.УстановитьОпубликованнуюВерсию( Владелец, Ссылка );
	
	СменитьСтатусВерсии( ЭтотОбъект, Перечисления.упСтатусыВерсий.Опубликованная, НСтр( "ru='Публикация'" ) );
	
	Записать();
	
КонецПроцедуры



Процедура СменитьСтатусВерсии( пВерсия, пНовСтатус, пСтрОперация )
	
	Если глРеквизит( пВерсия , "Статус" ) = пНовСтатус Тогда
		Возврат;
	КонецЕсли;
	
	Если упОбщегоНазначенияКлиентСервер.ЭтоВерсияСсылка( пВерсия ) Тогда
		
		ВерсияОбъект = пВерсия.ПолучитьОбъект();
		записывать = Истина;
		
	Иначе
		
		ВерсияОбъект = пВерсия;
		записывать = Ложь;
		
	КонецЕсли;
	
	ВерсияОбъект.Статус = пНовСтатус;
	
	ВерсияОбъект.ЗафиксироватьИзменение( пСтрОперация );
	
	Если записывать Тогда
		
		ВерсияОбъект.Записать();
		
	КонецЕсли;
	
КонецПроцедуры

Процедура СписатьПредыдущиеВерсии()
	
	Запрос = Новый Запрос();
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	упВерсии.Ссылка
	|ИЗ
	|	Справочник.упВерсии КАК упВерсии
	|ГДЕ
	|	упВерсии.Владелец = &БП
	|	И упВерсии.Статус = ЗНАЧЕНИЕ(Перечисление.упСтатусыВерсий.Опубликованная)
	|	И упВерсии.Ссылка <> &ЭтаВерсия";
	
	Запрос.УстановитьПараметр("БП", Владелец);
	Запрос.УстановитьПараметр("ЭтаВерсия", Ссылка);
	Выборка = Запрос.Выполнить().Выбрать();
	
	НачатьТранзакцию();
	
	Пока Выборка.Следующий() Цикл
		
		СписатьВерсию( Выборка.Ссылка );
		
	КонецЦикла;
	
	ЗафиксироватьТранзакцию();
	
КонецПроцедуры

#КонецЕсли




Процедура ДобавитьРеквизит( пРеквизит ) Экспорт
	
	Если Не ЗначениеЗаполнено( пРеквизит ) Тогда
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч( пРеквизит ) = Тип( "Массив" ) Тогда
		
		Для Каждого цЭлемент Из пРеквизит Цикл
			
			ДобавитьРеквизит( цЭлемент );
			
		КонецЦикла;
		
	ИначеЕсли пРеквизит.ЭтоГруппа Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		|	упРеквизиты.Ссылка
		|ИЗ
		|	Справочник.упРеквизиты КАК упРеквизиты
		|ГДЕ
		|	упРеквизиты.Родитель = &Родитель";
		
		Запрос.УстановитьПараметр("Родитель", пРеквизит);
		
		Выборка = Запрос.Выполнить().Выбрать();
		
		Пока Выборка.Следующий() Цикл
			
			ДобавитьРеквизит(Выборка.Ссылка);
			
		КонецЦикла;
		
	ИначеЕсли пРеквизит.Предопределенный Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюВОкно( НСтр( "ru='Запрещено добавлять предопределенные реквизиты'" ) );
		
	Иначе
		
		НовСтрока = РеквизитыБизнесПроцесса.Найти(пРеквизит, "СсылкаНаРеквизит");
		
		Если НовСтрока = Неопределено Тогда
			
			НовСтрока = РеквизитыБизнесПроцесса.Добавить();
			
			НовСтрока.СсылкаНаРеквизит = пРеквизит;
			НовСтрока.ИндексКартинки = пРеквизит.ИндексКартинки;
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры





Функция ИзмененРеквизитныйСостав()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	РеквизитыНаОбъекте.СсылкаНаРеквизит,
	               |	1 КАК ФлагИзменения
	               |ПОМЕСТИТЬ втРеквизитыНаОбъекте
	               |ИЗ
	               |	&РеквизитыНаОбъекте КАК РеквизитыНаОбъекте
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	упВерсииРеквизитыБизнесПроцесса.СсылкаНаРеквизит КАК СсылкаНаРеквизит,
	               |	-1 КАК ФлагИзменения
	               |ПОМЕСТИТЬ втРазность
	               |ИЗ
	               |	Справочник.упВерсии.РеквизитыБизнесПроцесса КАК упВерсииРеквизитыБизнесПроцесса
	               |ГДЕ
	               |	упВерсииРеквизитыБизнесПроцесса.Ссылка = &Ссылка
	               |
	               |ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ
	               |	втРеквизитыНаОбъекте.СсылкаНаРеквизит,
	               |	втРеквизитыНаОбъекте.ФлагИзменения
	               |ИЗ
	               |	втРеквизитыНаОбъекте КАК втРеквизитыНаОбъекте
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	Разность.СсылкаНаРеквизит КАК СсылкаНаРеквизит,
	               |	СУММА(Разность.ФлагИзменения) КАК ФлагИзменения
	               |ПОМЕСТИТЬ втСверткаРазности
	               |ИЗ
	               |	втРазность КАК Разность
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	Разность.СсылкаНаРеквизит
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	ФлагИзменения
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	СверткаРазности.СсылкаНаРеквизит,
	               |	СверткаРазности.ФлагИзменения КАК ФлагИзменения
	               |ИЗ
	               |	втСверткаРазности КАК СверткаРазности
	               |ГДЕ
	               |	(НЕ СверткаРазности.ФлагИзменения = 0)
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ втРазность
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ втСверткаРазности
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ втРеквизитыНаОбъекте";
	
	Запрос.УстановитьПараметр( "Ссылка" , Ссылка );
	Запрос.УстановитьПараметр( "РеквизитыНаОбъекте" , РеквизитыБизнесПроцесса.Выгрузить() );
	
	Возврат Не Запрос.Выполнить().Пустой(); 
	
КонецФункции	//ИзмененРеквизитныйСостав



Процедура ЗаполнитьСКД()
	
	СКД = упСКДКлиентСервер.СоздатьСКДПоВерсии( ЭтотОбъект );
	хзСКД = Новый ХранилищеЗначения( СКД );
	
КонецПроцедуры

Процедура ОбновитьСКДВУсловиях()
	
	Для каждого цУсловие Из Справочники.упУсловия.ОбъектыВерсии( Ссылка ) Цикл
		
		Справочники.упУсловия.АктуализироватьСКД( цУсловие );
		
	КонецЦикла;
	
КонецПроцедуры	//ОбновитьСКДВУсловиях

