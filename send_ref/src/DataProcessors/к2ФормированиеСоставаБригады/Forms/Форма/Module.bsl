
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьУсловноеОформление();
	
	ЗаполнитьПараметрыПоУмолчанию();
	
	УстановитьВидимостьДоступность();
	
	УстановитьТекущийСоставБригады();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено
		, ЭтотОбъект
		, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
	Если Событие = "Штрихкод" И ВводДоступен() Тогда
		
		ОбработатьШтрихКод(СокрЛП(Данные));
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СменаПриИзменении(Элемент)
	
	УстановитьТекущийСоставБригады();
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаСменыПриИзменении(Элемент)
	
	УстановитьТекущийСоставБригады();
	
КонецПроцедуры

&НаКлиенте
Процедура РабочийЦентрПриИзменении(Элемент)
	
	УстановитьТекущийСоставБригады();
	
КонецПроцедуры

&НаКлиенте
Процедура ШтрихкодПриИзменении(Элемент)
	
	ОбработатьШтрихКод(ШтрихКод);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСотрудники

&НаКлиенте
Процедура СотрудникиВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если ПроверитьЗаполнение() Тогда
		
		ТекущаяДата = ТекущееВремяСмены();
		НоваяСтрока = Бригада.Добавить();
		НоваяСтрока.Сотрудник = ВыбраннаяСтрока;
		Если Объект.ДатаСмены = НачалоДня(ТекущаяДата) Тогда
			НоваяСтрока.ВремяНачала = ТекущаяДата;
		Иначе
			НоваяСтрока.ВремяНачала = СтруктураДатСмены(Объект.ДатаСмены, Объект.Смена).ДатаНачалаСмены;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыБригада

&НаКлиенте
Процедура БригадаВремяНачалаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Бригада.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.ВремяНачала = ДатаРаботыПоСмене(ТекущиеДанные.ВремяНачала, Объект.ДатаСмены, Объект.Смена);
	
КонецПроцедуры

&НаКлиенте
Процедура БригадаВремяОкончанияПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Бригада.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.ВремяОкончания = ДатаРаботыПоСмене(ТекущиеДанные.ВремяОкончания, Объект.ДатаСмены, Объект.Смена);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Сохранить(Команда)
	
	Если ПроверитьЗаполнение() Тогда
		СохранитьИзмененияТекущейСмены();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область УсловноеОформление

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработкаШтрихкода

&НаКлиенте
Процедура ОбработатьШтрихКод(Знач ОбрабатываемыйШтрихКод)
	
	ТекстОшибки = "";
	
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(ОбрабатываемыйШтрихКод);
	
	ОбработатьШтрихКодНаСервере(ПараметрыШтрихкода, ТекстОшибки);
	
	Если Не ПустаяСтрока(ТекстОшибки) Тогда
		
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки);
		
	Иначе
		
		ВремяОкончанияРаботыУстановлено = Истина;
		УстановитьВидимостьДоступность();
		ПодключитьОбработчикОжидания("НачатьЗакрытиеФормы", 5, Истина);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихКодНаСервере(ПараметрыШтрихкода, ТекстОшибки)
	
	МассивПользователей = к2ШтрихкодыДокументов.ПолучитьСсылкуПоШтрихкодуТабличногоДокумента(ПараметрыШтрихкода.ШтрихКод
		, глСоздатьМассив(Справочники.Пользователи.ПустаяСсылка()));
	
	Если МассивПользователей.Количество() = 1 Тогда
		
		Сотрудник = глРеквизит(МассивПользователей[0], "ФизическоеЛицо");
		
		Если ЗначениеЗаполнено(Сотрудник) Тогда
			
			ПроставитьВремяОкончанияРаботы(Сотрудник, ТекстОшибки);
			
		Иначе
			
			ШаблонСообщения = НСтр("ru='У пользователя ""%1"" не заполнено физическое лицо.'");
			ТекстСообщения = СтрШаблон(ШаблонСообщения, МассивПользователей[0]);
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
			
		КонецЕсли;
		
	ИначеЕсли МассивПользователей.Количество() > 1 Тогда
		
		ШаблонСообщения = НСтр("ru='По штрихкоду ""%1"" найдено больше 1 пользователя.'");
		ТекстСообщения = СтрШаблон(ШаблонСообщения, ПараметрыШтрихкода.ШтрихКод);
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	Иначе
		
		ШаблонСообщения = НСтр("ru='Штрихкод ""%1"" не распознан.'");
		ТекстСообщения = СтрШаблон(ШаблонСообщения, ПараметрыШтрихкода.ШтрихКод);
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПроставитьВремяОкончанияРаботы(Сотрудник, ТекстОшибки)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если Не ЗначениеЗаполнено(Сотрудник) Тогда
		Возврат;
	КонецЕсли;
	
	ТекущееВремя = ТекущаяДатаСеанса();
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ДатыСмен", глСоздатьМассив(НачалоДня(ТекущееВремя), НачалоДня(ТекущееВремя) - к2ИК.СекундВДне()));
	Запрос.УстановитьПараметр("Сотрудник", Сотрудник);
	Запрос.УстановитьПараметр("ТекущееВремя", ТекущееВремя);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	к2УстановкаСоставаБригадыБригада.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.к2УстановкаСоставаБригады.Бригада КАК к2УстановкаСоставаБригадыБригада
	|ГДЕ
	|	к2УстановкаСоставаБригадыБригада.Ссылка.ДатаСмены В(&ДатыСмен)
	|	И к2УстановкаСоставаБригадыБригада.Ссылка.ВидБригады = ЗНАЧЕНИЕ(Перечисление.к2ВидыБригад.СУчетомВремениРаботы)
	|	И к2УстановкаСоставаБригадыБригада.Ссылка.Проведен
	|	И к2УстановкаСоставаБригадыБригада.ВремяОкончания > &ТекущееВремя
	|	И к2УстановкаСоставаБригадыБригада.Сотрудник = &Сотрудник";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	НачатьТранзакцию();
	
	Попытка
		
		Пока Выборка.Следующий() Цикл
			
			ДокументОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
			Для Каждого СтрокаБригады Из ДокументОбъект.Бригада.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник)) Цикл
				СтрокаБригады.ВремяОкончания = ТекущееВремя;
			КонецЦикла;
			
			к2УправлениеДаннымиСервер.ПровестиДокумент_СообщенияИсключением(ДокументОбъект);
			
		КонецЦикла;
		
		ЗафиксироватьТранзакцию();
		
	Исключение
		
		ОтменитьТранзакцию();
		
		ПредставлениеОшибки = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ПредставлениеОшибки, ТекстОшибки);
		
	КонецПопытки;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиенте
Процедура НачатьЗакрытиеФормы()
	
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПараметрыПоУмолчанию()
	
	ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();
	СканерШтрихКодовПодключен = к2РаботаСПодключаемымОборудованием.СканерШтрихКодовПодключен();
	
	ВремяОкончанияРаботыУстановлено = Ложь;
	
	Параметры.Свойство("РежимСозданияОтчетаОСоставеСмены", РежимСозданияОтчетаОСоставеСмены); 
	Параметры.Свойство("ДатаСмены", Объект.ДатаСмены);
	Параметры.Свойство("Смена", Объект.Смена);
	Параметры.Свойство("РабочийЦентр", Объект.РабочийЦентр);
	
	ЗаполнитьСмену();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность() 
	
	Если Не РежимСозданияОтчетаОСоставеСмены Тогда
		
		Элементы.Страницы.ТекущаяСтраница = Элементы.СтраницаУдаленияИзСостава;
		Элементы.СтраницаСоставаБригады.Видимость = Ложь;
		Элементы.ФормаСохранить.Видимость = Ложь;
		
	КонецЕсли;
	
	Элементы.Декорация_УстановкаВремениОкончания.Видимость = ВремяОкончанияРаботыУстановлено;
	Элементы.Декорация_ОтсканируйтеБейдж.Видимость = Не ВремяОкончанияРаботыУстановлено;
	Элементы.Штрихкод.Видимость = Не СканерШтрихКодовПодключен;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьТекущийСоставБригады()
	
	УстановкаСоставаБригады = Документы.к2УстановкаСоставаБригады.ПустаяСсылка();
	
	Если Не (ЗначениеЗаполнено(Объект.ДатаСмены)
		Или ЗначениеЗаполнено(Объект.Смена)
		Или ЗначениеЗаполнено(Объект.РабочийЦентр)) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ДатаСмены", Объект.ДатаСмены);
	Запрос.УстановитьПараметр("Смена", Объект.Смена);
	Запрос.УстановитьПараметр("РабочийЦентр", Объект.РабочийЦентр);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	к2УстановкаСоставаБригады.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.к2УстановкаСоставаБригады КАК к2УстановкаСоставаБригады
	|ГДЕ
	|	к2УстановкаСоставаБригады.Проведен
	|	И к2УстановкаСоставаБригады.ВидБригады = ЗНАЧЕНИЕ(Перечисление.к2ВидыБригад.СУчетомВремениРаботы)
	|	И к2УстановкаСоставаБригады.ДатаСмены = &ДатаСмены
	|	И к2УстановкаСоставаБригады.Смена = &Смена
	|	И к2УстановкаСоставаБригады.РабочийЦентр = &РабочийЦентр";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		УстановкаСоставаБригады = Выборка.Ссылка;
		Бригада.Загрузить(УстановкаСоставаБригады.Бригада.Выгрузить());
		
	Иначе
		
		УстановкаСоставаБригады = Документы.к2УстановкаСоставаБригады.ПустаяСсылка();
		Бригада.Очистить();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура СохранитьИзмененияТекущейСмены()
	
	Если ЗначениеЗаполнено(УстановкаСоставаБригады) Тогда
		
		ДокументОбъект = УстановкаСоставаБригады.ПолучитьОбъект();
		
	Иначе
		
		ДокументОбъект = Документы.к2УстановкаСоставаБригады.СоздатьДокумент();
		ДокументОбъект.Дата = ТекущаяДатаСеанса();
		
	КонецЕсли;
	
	СтруктураЗаполнения = Новый Структура;
	СтруктураЗаполнения.Вставить("ВидБригады", Перечисления.к2ВидыБригад.СУчетомВремениРаботы);
	СтруктураЗаполнения.Вставить("ДатаСмены", Объект.ДатаСмены);
	СтруктураЗаполнения.Вставить("Смена", Объект.Смена);
	СтруктураЗаполнения.Вставить("РабочийЦентр", Объект.РабочийЦентр);
	СтруктураЗаполнения.Вставить("Бригада", Бригада.Выгрузить());
	
	ДокументОбъект.Заполнить(СтруктураЗаполнения);
	
	к2ПроведениеСервер.ПровестиДокумент(ДокументОбъект);
	
	УстановкаСоставаБригады = ДокументОбъект.Ссылка;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ДатаРаботыПоСмене(Дата, ДатаСмены, Смена)
	
	Возврат Справочники.к2Смены.ДатаРаботыПоСмене(Дата, ДатаСмены, Смена);
	
КонецФункции

&НаСервереБезКонтекста
Функция СтруктураДатСмены(ДатаСмены, Смена)
	
	Возврат Справочники.к2Смены.СтруктураПериодаСмены(ДатаСмены, Смена);
	
КонецФункции

&НаСервере
Процедура ЗаполнитьСмену()
	
	Если ПолучитьФункциональнуюОпцию(Метаданные.ФункциональныеОпции.к2ВестиУчетПоСменам.Имя) Тогда
		Возврат;
	КонецЕсли;
	
	Объект.Смена = Справочники.к2Смены.Смена24;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ТекущееВремяСмены()
	
	Возврат ТекущаяДатаСеанса();
	
КонецФункции

#КонецОбласти

#КонецОбласти