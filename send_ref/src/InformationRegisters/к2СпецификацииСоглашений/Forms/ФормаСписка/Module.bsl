
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере( Отказ, СтандартнаяОбработка )
	
	к2СобытияФорм.ПриСозданииНаСервере( ЭтотОбъект, Отказ, СтандартнаяОбработка );
		
	СоздатьПанельОтборов();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ПодборНоменклатурыПроизведен"
		И ЗначениеЗаполнено( Параметр )
		И Источник = УникальныйИдентификатор Тогда
		
		ЗагрузитьНоменклатуруИзХранилища( Параметр );
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды	
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подобрать(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПодборНовыхЗаписейЗавершение", ЭтотОбъект);
	
	ПодобратьСоглашение(ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьСоглашение(Команда)
	
	ВыделенныеСтроки = Элементы.Список.ВыделенныеСтроки;
	
	Если ВыделенныеСтроки.Количество() = 0 Тогда
		ТекстСообщения = НСтр( "ru = 'Выделите строки у которых хотите изменить соглашение.'" );
		ОбщегоНазначенияКлиент.СообщитьПользователю( ТекстСообщения );
		Возврат;
	КонецЕсли;
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ИзменитьСоглашениеЗавершение", ЭтотОбъект);
	
	ПодобратьСоглашение(ОписаниеОповещения);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область СтандартныеПодсистемыПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Элементы.Список);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Элементы.Список, Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область ПанельОтборов

&НаСервере
Процедура СоздатьПанельОтборов()
	
	параметрыПанели = к2Отборы.ПараметрыПанелиОтборов();
	параметрыПанели.Кнопка_НастроитьСписок           = Элементы.НастройкаСписка;
	параметрыПанели.Группа_ПользовательскиеНастройки = Элементы.СписокКомпоновщикНастроекПользовательскиеНастройки;
	параметрыПанели.Метаданные = Метаданные.РегистрыСведений.к2СпецификацииСоглашений;
		
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Номенклатура");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Характеристика");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Соглашение");
	
	к2Отборы.СоздатьПанельОтборов(ЭтотОбъект, параметрыПанели);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбработкаПанелиОтборов(Элемент, СтандартнаяОбработка = Истина)
	
	к2ОтборыКлиент.ОбработкаПанелиОтборов(ЭтотОбъект, Элемент, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_УстановитьОтбор(Знач пИмяЭлемента, Знач пСохранять = Истина) Экспорт
	
	к2Отборы.УстановитьОтборСписку(ЭтотОбъект, пИмяЭлемента, пСохранять);
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ПодобратьСоглашение(ОписаниеОповещения)
	
	ПараметрыОткрытия = Новый Структура;
	ПараметрыОткрытия.Вставить("РежимВыбора", Истина);
	ПараметрыОткрытия.Вставить("ЗакрыватьПриВыборе", Истина);
	
	ОткрытьФорму("Справочник.к2СоглашенияСКлиентами.ФормаВыбора",
				 ПараметрыОткрытия,
				 ЭтотОбъект,
				 УникальныйИдентификатор,
				 ,
				 ,
				 ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьСоглашениеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Не ЗначениеЗаполнено(Результат) Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьСоглашениеЗаписямРегистра(Элементы.Список.ВыделенныеСтроки, Результат);
	
	Элементы.Список.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСоглашениеЗаписямРегистра(Знач ВыделенныеСтроки, Соглашение)

	НачатьТранзакцию();
	
	Для Каждого ВыделеннаяСтрока Из ВыделенныеСтроки Цикл
		
		МенеджерЗаписи = РегистрыСведений.к2СпецификацииСоглашений.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, ВыделеннаяСтрока);
		МенеджерЗаписи.Прочитать();
		
		МенеджерЗаписи.Соглашение = Соглашение;
		МенеджерЗаписи.Записать();
		
	КонецЦикла;
	
	Попытка
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ОбщегоНазначения.СообщитьПользователю(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура ПодборНовыхЗаписейЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Не ЗначениеЗаполнено(Результат) Тогда
		Возврат;
	КонецЕсли;
	
	СоглашениеДляПодбора = Результат;
	Контрагент = глРеквизит(СоглашениеДляПодбора, "Контрагент");
	
	отбор = Новый Структура;
	отбор.Вставить( "ТипНоменклатуры", ПредопределенноеЗначение( "Перечисление.к2ТипыНоменклатуры.ГотоваяПродукция" ) );
	
	ДополнительныеПараметрыПодбора = НоменклатураКлиент.ДополнительныеПараметрыПодбора();
	Если ЗначениеЗаполнено(Контрагент) Тогда
		ДополнительныеПараметрыПодбора.Контрагент = Контрагент;
	Иначе
		ДополнительныеПараметрыПодбора.ПодборХарактеристикиАвтоматически = Ложь;
	КонецЕсли;
		
	НоменклатураКлиент.ПодобратьНоменклатуруСПараметрами( ЭтотОбъект, , отбор, ДополнительныеПараметрыПодбора );
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьНоменклатуруИзХранилища(Адрес)
	
	Если Не ЗначениеЗаполнено(Адрес) Тогда
		
		Возврат;
	
	КонецЕсли;
	
	тзНоменклатура = ПолучитьИзВременногоХранилища(Адрес);
	тзНоменклатура = к2Коллекции.ПолучитьСвернутуюТаблицуПоВсемКолонкам(тзНоменклатура);
	
	НачатьТранзакцию();
	
	Для Каждого СтрокаНоменклатура Из тзНоменклатура Цикл
		
		МенеджерЗаписи = РегистрыСведений.к2СпецификацииСоглашений.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(МенеджерЗаписи, СтрокаНоменклатура);
		МенеджерЗаписи.Соглашение = СоглашениеДляПодбора;
		МенеджерЗаписи.Прочитать();
		
		Если Не МенеджерЗаписи.Выбран() Тогда
			
			ЗаполнитьЗначенияСвойств(МенеджерЗаписи, СтрокаНоменклатура);
			МенеджерЗаписи.Соглашение = СоглашениеДляПодбора;
			МенеджерЗаписи.Записать();
			
		КонецЕсли;
		
	КонецЦикла;
	
	Попытка
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию();
		ОбщегоНазначения.СообщитьПользователю(КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
	Элементы.Список.Обновить();
	
КонецПроцедуры

#КонецОбласти