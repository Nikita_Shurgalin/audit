#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	разрешенныеПериодичности = к2Планирование.РазрешенныеПериодичности();
	
	Если разрешенныеПериодичности.Найти(Периодичность) = Неопределено Тогда
		
		шаблонТекста   = НСтр("ru='Периодичность <%1> указана не верно. Допустимо указывать: %2.'");
		текстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(шаблонТекста,
																				 Периодичность,
																				 СтрСоединить(разрешенныеПериодичности, ", "));
		
		ОбщегоНазначения.СообщитьПользователю(текстСообщения, , , "Периодичность", Отказ);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Процедура ЗаполнитьРасчет() Экспорт
	
	Если Не ПроверитьЗаполнение() Тогда
		
		Возврат;
		
	КонецЕсли;
	
	отборы = СтруктураОтборов();
	
	тзПрогноз = Обработки.к2Планирование_Потребностей.ПрогнозОтгрузок(
		НачалоПериода,
		ОкончаниеПериода,
		Периодичность,
		отборы);
		
	тзПрогноз.ЗаполнитьЗначения(Справочники.Контрагенты.ПустаяСсылка(), "Контрагент");
	
	Если ОтключитьДетализациюДоВолны Тогда
		
		тзПрогноз.ЗаполнитьЗначения(Справочники.к2Волны.ПустаяСсылка(), "ВолнаОтгрузки");
		
	КонецЕсли;
	
	Если ОтключитьДетализациюДоСклада Тогда
		
		тзПрогноз.ЗаполнитьЗначения(Справочники.Склады.ПустаяСсылка(), "Склад");
		
	КонецЕсли;
	
	Расчет.Загрузить(Обработки.к2Планирование_Потребностей.Расчет(тзПрогноз, ЭтотОбъект, отборы));
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция СтруктураОтборов()
	
	структураОтборов = Новый Структура();
	
	структураОтборов.Вставить("ОтборТоварныеКатегории", ОтборТоварныеКатегории.ВыгрузитьЗначения());
	структураОтборов.Вставить("ОтборПроизводители", ОтборПроизводители.ВыгрузитьЗначения());
	структураОтборов.Вставить("МассивДобавляемойНоменклатуры", СписокДобавляемойНоменклатуры.ВыгрузитьЗначения());
	структураОтборов.Вставить("МассивИсключаемойНоменклатуры", СписокИсключаемойНоменклатуры.ВыгрузитьЗначения());
	структураОтборов.Вставить("ОтборСкладыОтгрузки", ОтборСкладыОтгрузки.ВыгрузитьЗначения());
	структураОтборов.Вставить("ОтборВолныОтгрузки", ОтборВолныОтгрузки.ВыгрузитьЗначения());
	
	Возврат структураОтборов;
	
КонецФункции

#КонецОбласти

#КонецЕсли