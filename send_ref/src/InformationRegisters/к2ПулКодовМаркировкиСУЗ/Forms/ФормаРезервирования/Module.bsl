#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
		
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ОбработатьИПроверитьПереданныеПараметры(Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ЗаполнитьДанныеФормы();
	
	ИспользоватьХарактеристикиНоменклатуры = к2ИнтеграцияИС.ХарактеристикиИспользуются();
	
	УправлениеЭлементамиФормыПриСоздании();
	
	к2СобытияФормИСПереопределяемый.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Обновить(Команда)
	
	ЗаполнитьДанныеФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура Печать(Команда)
	
	ПараметрыФормы = Новый Структура();
	ПараметрыФормы.Вставить("Документ", Документ);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПриВыполненииКоманды", ЭтотОбъект, Команда);
	
	ОткрытьФорму("РегистрСведений.к2ПулКодовМаркировкиСУЗ.Форма.ФормаПечати",
		ПараметрыФормы, ЭтотОбъект,,,, ОписаниеОповещения, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарезервироватьИЗаказать(Команда)
	
	ДлительнаяОперация = НачатьРезервированиеКодов();
	ОповещениеОЗавершении = Новый ОписаниеОповещения("РезервированиеКодовЗавершение", ЭтотОбъект);
	
	ПараметрыОжидания = ДлительныеОперацииКлиент.ПараметрыОжидания(ЭтотОбъект);
	ПараметрыОжидания.ТекстСообщения = НСтр("ru = 'Резервирование кодов маркировки.'");
	
	ДлительныеОперацииКлиент.ОжидатьЗавершение(ДлительнаяОперация, ОповещениеОЗавершении, ПараметрыОжидания);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление();
	
	УсловноеОформление.Элементы.Очистить();
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИПроверитьПереданныеПараметры(Отказ)
	
	Если Не ЗначениеЗаполнено(Параметры.Документ) Тогда
		ТекстОшибки = НСтр("ru = 'В форму заказа и резервирования кодов маркировки не передан документ.'");
		ОбщегоНазначения.СообщитьПользователю(ТекстОшибки, , , , Отказ);
		Возврат;
	КонецЕсли;
	
	Если НЕ Метаданные.ОпределяемыеТипы.к2ОснованиеЗаказНаЭмиссиюКодовМаркировкиИСМП.Тип.СодержитТип(ТипЗнч(Параметры.Документ)) Тогда
		ТекстОшибки = НСтр("ru = 'В форму заказа и резервирования кодов маркировки передан документ неправильного типа.'");
		ОбщегоНазначения.СообщитьПользователю(ТекстОшибки, , , , Отказ);
		Возврат;
	КонецЕсли;
		
	Документ = Параметры.Документ;
	
КонецПроцедуры

&НаСервере
Процедура УправлениеЭлементамиФормыПриСоздании()
	
	Если НЕ ИспользоватьХарактеристикиНоменклатуры Тогда
		Элементы.ПулКодовМаркировкиХарактеристика.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеФормы()
	
	ДанныеПоДокументу = РегистрыСведений.к2ПулКодовМаркировкиСУЗ.РассчитатьТаблицуСостоянияПоЗаказу(Документ);
	ПулКодовМаркировки.Загрузить(ДанныеПоДокументу);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриВыполненииКоманды(Результат, ДополнительныеПараметры) Экспорт
	
	ЗаполнитьДанныеФормы();
	
КонецПроцедуры

#Область РезервированиеИЗаказКодовМаркировки

&НаСервере
Функция НачатьРезервированиеКодов()
	
	ПараметрыПроцедуры = Новый Структура;
	ПараметрыПроцедуры.Вставить("Документ", Документ);
	
	ПараметрыВыполнения = ДлительныеОперации.ПараметрыВыполненияВФоне(ЭтотОбъект.УникальныйИдентификатор);
	ПараметрыВыполнения.НаименованиеФоновогоЗадания = НСтр("ru = 'Резервирование кодов маркировки в пуле кодов СУЗ.'");
	ПараметрыВыполнения.ЗапуститьВФоне = Истина;
	
	к2ИнтеграцияИСПереопределяемый.НастроитьДлительнуюОперацию(ПараметрыПроцедуры, ПараметрыВыполнения);
	
	Возврат ДлительныеОперации.ВыполнитьВФоне("РегистрыСведений.к2ПулКодовМаркировкиСУЗ.ЗарезервироватьКодыПодЗаказ",
		ПараметрыПроцедуры, ПараметрыВыполнения);
	
КонецФункции

&НаКлиенте
Процедура РезервированиеКодовЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда  // отменено пользователем
		Закрыть();
	ИначеЕсли Результат.Статус = "Ошибка" Тогда
		ПоказатьПредупреждение(, Результат.КраткоеПредставлениеОшибки);
	ИначеЕсли Результат.Статус = "Выполнено" Тогда
		РезервированиеКодовЗавершениеНаСервере(Результат.АдресРезультата);
		ЗаказатьКодыМаркировки();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура РезервированиеКодовЗавершениеНаСервере(АдресРезультата)
	
	Если ЭтоАдресВременногоХранилища(АдресРезультата) Тогда
		РезультатРезервирования = ПолучитьИзВременногоХранилища(АдресРезультата);
		Если ТипЗнч(РезультатРезервирования) = Тип("Структура") Тогда
			Если РезультатРезервирования.Свойство("ТаблицаСостояниеКодов")
				И ТипЗнч(РезультатРезервирования.ТаблицаСостояниеКодов) = Тип("ТаблицаЗначений") Тогда
				ПулКодовМаркировки.Загрузить(РезультатРезервирования.ТаблицаСостояниеКодов);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры
	
&НаКлиенте
Процедура ЗаказатьКодыМаркировки()
	
	ВидыПродукции = Новый Соответствие();
	
	Для Каждого СтрокаТЧ Из ПулКодовМаркировки Цикл
		Если СтрокаТЧ.КоличествоЗаказать > 0 Тогда
			ДанныеСтроки = Новый Структура("Номенклатура,Характеристика,Количество,КоличествоУпаковок");
			ЗаполнитьЗначенияСвойств(ДанныеСтроки, СтрокаТЧ);
			ДанныеСтроки.Количество = СтрокаТЧ.КоличествоЗаказать;
			ДанныеСтроки.КоличествоУпаковок = СтрокаТЧ.КоличествоЗаказать;
			
			Товары = ВидыПродукции[СтрокаТЧ.ВидПродукции];
			
			Если Товары = Неопределено Тогда
				Товары = Новый Массив();
			КонецЕсли;
			
			Товары.Добавить(ДанныеСтроки);
			ВидыПродукции.Вставить(СтрокаТЧ.ВидПродукции, Товары);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого КлючИЗначение Из ВидыПродукции Цикл
		ДанныеЗаполнения = Новый Структура;
		ДанныеЗаполнения.Вставить("ДанныеПула");
		ДанныеЗаполнения.Вставить("ВидПродукции", КлючИЗначение.Ключ);
		ДанныеЗаполнения.Вставить("ДокументОснование", Документ);
		ДанныеЗаполнения.Вставить("Товары", КлючИЗначение.Значение);
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПриВыполненииКоманды", ЭтотОбъект, Команды.ЗарезервироватьИЗаказать);
		
		к2ИнтеграцияИСМПКлиент.ОткрытьФормуСозданияДокумента(
			"Документ.к2ЗаказНаЭмиссиюКодовМаркировкиСУЗ",
			ДанныеЗаполнения,
			ЭтотОбъект,
			ОписаниеОповещения);
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти