
#Область ПрограммныйИнтерфейс

#Область ЭтикеткиИЦенники

// Получает данные для печати и открывает форму обработки печати этикеток и ценников.
//
// Параметры:
//	ОписаниеКоманды - Структура - структура с описанием команды.
//
// Возвращаемое значение:
//	Неопределено
//
Функция ПечатьШтрихкодовУпаковок(ОписаниеКоманды) Экспорт
	
	ДанныеДляПечати = к2УправлениеПечатьюВызовСервера.ДанныеДляПечатиШтрихкодовУпаковок(ОписаниеКоманды.Идентификатор, ОписаниеКоманды.ОбъектыПечати);
	
	Если НЕ ДанныеДляПечати.ЕстьЭтикеткиДляПечати Тогда
		ОчиститьСообщения();
		ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru='Нет данных для печати штрихкодов упаковок.'"));
		Возврат Неопределено;
	КонецЕсли;
	
	ОткрытьФорму(
		"Обработка.к2ПечатьЭтикетокИЦенников.Форма.ФормаШтрихкодыУпаковок",
		Новый Структура("АдресВХранилище", ДанныеДляПечати.АдресВХранилище),
		ОписаниеКоманды.Форма,
		Новый УникальныйИдентификатор);
		
КонецФункции

Функция ПечатьЭтикетокСкладскиеЯчейки(ОписаниеКоманды) Экспорт 
	
	АдресВХранилище =
		к2УправлениеПечатьюВызовСервера.ДанныеДляПечатиЭтикетокСкладскиеЯчейки(
			ОписаниеКоманды.Идентификатор,
			ОписаниеКоманды.ОбъектыПечати);
	
	ОткрытьФорму(
		"Обработка.к2ПечатьЭтикетокИЦенников.Форма.ФормаСкладскиеЯчейки",
		Новый Структура("АдресВХранилище, НазначениеШаблона", АдресВХранилище, Неопределено),
		ОписаниеКоманды.Форма,
		Новый УникальныйИдентификатор);
	
КонецФункции

#КонецОбласти

#Область КомплектыДокументов

// Отправляет на печать комплект документов
//
// Параметры:
//  ОписаниеКоманды - Структура - сведения о выполняемой команде.
//
Функция ПечатьКомплектаДокументов( ОписаниеКоманды ) Экспорт
	
	ЗапрашиватьПодтверждение = ОбщегоНазначенияВызовСервера.ХранилищеОбщихНастроекЗагрузить( "ОбщиеНастройкиПользователя",
																							 "ЗапрашиватьПодтверждениеПриПечатиКомплектаДокументов" );
	Если ЗапрашиватьПодтверждение = Неопределено Тогда
		ЗапрашиватьПодтверждениеПриПечатиКомплектаДокументов = Истина;
	Иначе
		ЗапрашиватьПодтверждениеПриПечатиКомплектаДокументов = ЗапрашиватьПодтверждение;
	КонецЕсли;
	
	Если ЗапрашиватьПодтверждениеПриПечатиКомплектаДокументов Тогда
		
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить( "КодВозвратаДиалога.Да", НСтр( "ru = 'Распечатать'" ) );
		Кнопки.Добавить( "КодВозвратаДиалога.Нет", НСтр( "ru = 'Не печатать'" ) );
		
		Параметры           = СтандартныеПодсистемыКлиент.ПараметрыВопросаПользователю();
		Параметры.КнопкаПоУмолчанию          = КодВозвратаДиалога.Да;
		Параметры.Заголовок = НСтр( "ru = 'Печать комплекта документов'" );
		Параметры.КнопкаТаймаута             = КодВозвратаДиалога.Нет;
		Параметры.БольшеНеЗадаватьЭтотВопрос = ЗапрашиватьПодтверждениеПриПечатиКомплектаДокументов;
		
		СтандартныеПодсистемыКлиент.ПоказатьВопросПользователю(
			Новый ОписаниеОповещения( "ПечатьКомплектаДокументовЗавершение", ЭтотОбъект, Новый Структура( "ОписаниеКоманды", ОписаниеКоманды ) ),
			НСтр( "ru = 'Распечатать комплект документов на принтере?'" ),
			Кнопки,
			Параметры );
			
		Возврат Ложь;
		
	КонецЕсли;
	
	ПоказатьОповещениеПользователя(
		НСтр( "ru = 'Документы отправлены на печать'" ),
		,
		,
		БиблиотекаКартинок.Информация32 );
	
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечатиНаПринтер(
		"РегистрСведений.к2НастройкиПечатиОбъектов",
		"КомплектДокументов",
		ОписаниеКоманды.ОбъектыПечати,
		Новый Структура );
	
КонецФункции

// Служебная процедура, обработка оповещения.
//
// Параметры:
//  Результат				 - Структура - результат после вопроса пользователю.
//  ДополнительныеПараметры	 - Структура - дополнительные параметры оповещения.
//
Процедура ПечатьКомплектаДокументовЗавершение( Результат, ДополнительныеПараметры ) Экспорт
	
	ОписаниеКоманды = ДополнительныеПараметры.ОписаниеКоманды;
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ОбщегоНазначенияВызовСервера.ХранилищеОбщихНастроекСохранить( "ОбщиеНастройкиПользователя",
																  "ЗапрашиватьПодтверждениеПриПечатиКомплектаДокументов",
																  Не Результат.БольшеНеЗадаватьЭтотВопрос );
	
	Если Не Результат.Значение = КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	ПоказатьОповещениеПользователя(
		НСтр( "ru = 'Документы отправлены на печать'" ),
		,
		,
		БиблиотекаКартинок.Информация32 );
	
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечатиНаПринтер(
		"РегистрСведений.к2НастройкиПечатиОбъектов",
		"КомплектДокументов",
		ОписаниеКоманды.ОбъектыПечати,
		Новый Структура );
	
КонецПроцедуры

// Отправляет на печать комплект документов с настройкой
//
// Параметры:
//  ОписаниеКоманды - Структура - сведения о выполняемой команде.
//
Функция ПечатьКомплектаДокументовСНастройкой( ОписаниеКоманды ) Экспорт
	
	СтруктураПараметров = Новый Структура;
	
	МассивыДокументов = к2УправлениеПечатьюВызовСервера.РазделениеДокументовПечати( ОписаниеКоманды.ОбъектыПечати );
	
	Для Каждого МассивДокументовНаПечать Из МассивыДокументов Цикл
		
		СтруктураПараметров.Вставить("Объекты", МассивДокументовНаПечать);
		
		ОткрытьФорму( "РегистрСведений.к2НастройкиПечатиОбъектов.Форма.НастройкаПечатиКомплекта",
			СтруктураПараметров,
			ОписаниеКоманды.Форма,
			Новый УникальныйИдентификатор );
			
	КонецЦикла;
	
КонецФункции

#КонецОбласти

#КонецОбласти