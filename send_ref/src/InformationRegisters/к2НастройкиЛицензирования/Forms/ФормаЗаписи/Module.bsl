
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Сообщение = НСтр("ru = 'Форма не доступна для открытия'");
	ОбщегоНазначения.СообщитьПользователю(Сообщение, , , ,Отказ);
	
КонецПроцедуры

#КонецОбласти
