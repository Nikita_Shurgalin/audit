#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Функция ПолучитьМассивИсходящихПереходов( пСостояние ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.Ссылка
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.НачалоПерехода = &НачалоПерехода
	               |	И упПереходы.ПометкаУдаления = ЛОЖЬ
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	упПереходы.ПорядокВарианта";
	
	Запрос.УстановитьПараметр( "НачалоПерехода" , пСостояние );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции

Функция ПолучитьМассивВходящихПереходов( пСостояние ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.Ссылка
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.КонецПерехода = &КонецПерехода";
	
	Запрос.УстановитьПараметр( "КонецПерехода" , пСостояние );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции

Функция ПолучитьМассивИменВходящихПереходов( пСостояние ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.Имя
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.КонецПерехода = &КонецПерехода
	               |	И упПереходы.ПометкаУдаления = ЛОЖЬ";
	
	Запрос.УстановитьПараметр( "КонецПерехода" , пСостояние );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции


Функция ПолучитьВариантыПерехода( пСостояние ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упПереходы.ПредставлениеВарианта
	               |ИЗ
	               |	Справочник.упПереходы КАК упПереходы
	               |ГДЕ
	               |	упПереходы.НачалоПерехода = &НачалоПерехода
	               |	И упПереходы.ПометкаУдаления = ЛОЖЬ
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	упПереходы.ПорядокВарианта";
	
	Запрос.УстановитьПараметр( "НачалоПерехода" , пСостояние );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции	//ПолучитьВариантыПерехода



Функция ПолучитьПереходПоИмени( пИмя , пВерсияБП ) Экспорт
	
	Запрос = Новый Запрос();
	Запрос.Текст =
	"ВЫБРАТЬ
	|	упПереходы.Ссылка
	|ИЗ
	|	Справочник.упПереходы КАК упПереходы
	|ГДЕ
	|	упПереходы.Имя = &Имя
	|	И упПереходы.Владелец = &ВерсияБП
	|	И упПереходы.ПометкаУдаления = ЛОЖЬ";
	Запрос.УстановитьПараметр( "Имя" , пИмя );
	Запрос.УстановитьПараметр( "ВерсияБП" , пВерсияБП );
	
	ВыборкаПереходов = Запрос.Выполнить().Выбрать();
	
	Если ВыборкаПереходов.Следующий() Тогда
		
		Возврат ВыборкаПереходов.Ссылка;
		
	Иначе
		
		Возврат ПустаяСсылка();
		
	КонецЕсли;
	
КонецФункции

Функция ОбъектыВерсии( пВерсияБП ) Экспорт
	
	Запрос = Новый Запрос();
	Запрос.Текст =
	"ВЫБРАТЬ
	|	упПереходы.Ссылка
	|ИЗ
	|	Справочник.упПереходы КАК упПереходы
	|ГДЕ
	|	упПереходы.Владелец = &ВерсияБП
	|	И упПереходы.ПометкаУдаления = ЛОЖЬ";
	
	Запрос.УстановитьПараметр( "ВерсияБП" , пВерсияБП );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции	//


Функция ПолучитьСостояниеОкончания( пПереход ) Экспорт
	
	Возврат упКэшНаВызов.ПолучитьЗначениеРеквизита_Кэш( пПереход , "КонецПерехода" );
	
КонецФункции	//ПолучитьСостояниеОкончания

#КонецЕсли