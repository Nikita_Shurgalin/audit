#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	Если к2ТерминалСбораДанныхСервер.РежимРаботыСТСД()
		И ПолучитьФункциональнуюОпцию(Метаданные.ФункциональныеОпции.к2РабочееМестоСыроделаТСД.Имя) Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ВыбраннаяФорма = Метаданные.Обработки.к2РаботаСЗаданиями.Формы.ФормаТСД;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

Функция МассивИспользуемыхПараметров() Экспорт
	
	МассивПараметров = Новый Массив;
	МассивПараметров.Добавить("н_ВыпускНесколькимиДокументами");
	МассивПараметров.Добавить("н_УказыватьПараметрыТехнологическогоПроцесса");
	МассивПараметров.Добавить("н_УказыватьРасходИнгредиентов");
	МассивПараметров.Добавить("н_ВыполнятьНаборкуПоРецептуре");
	МассивПараметров.Добавить("н_ВидимостьКнопкиВыпуск");
	МассивПараметров.Добавить("н_ВидимостьКнопкиРасходИнгредиентов");
	МассивПараметров.Добавить("н_СоздаватьПереработкуБрак");
	МассивПараметров.Добавить("н_СписаниеМатериаловВНЗП");
	МассивПараметров.Добавить("н_МаркировкаЧестныйЗнак");
	
	ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивПараметров, Обработки.к2НаборкаПоРецептуре.МассивИспользуемыхПараметров(), Истина);
	ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивПараметров, Обработки.к2ПриемкаИПередача.МассивИспользуемыхПараметров(), Истина);
	ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивПараметров, Обработки.к2УчетЛабораторныхАнализов.МассивИспользуемыхПараметров(), Истина);
	ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивПараметров, Обработки.к2УчетРасходаИнгредиентов.МассивИспользуемыхПараметров(), Истина);
	
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивПараметров, "н_СоздаватьОднуСериюНаСмену");
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивПараметров, "н_ВыпускатьБракОтдельнойНоменклатурой");
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивПараметров, "н_НоменклатураВыпускаБрака");
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивПараметров, "н_ПеремещениеСоВзвешиванием");
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивПараметров, "н_Переупаковка");
	
	Обработки.к2МенюУчетныхТочек.ДополнитьМассивОбщимиПараметрами(МассивПараметров);
	
	к2УчетныеТочкиПереопределяемый.МассивИспользуемыхПараметров(Метаданные.Обработки.к2РаботаСЗаданиями.Имя
		, МассивПараметров);
	
	Возврат МассивПараметров;
	
КонецФункции

#Область ОбработкаЗаданий

Функция КлючиЗадания() Экспорт
	
	Ключи = Новый Массив;
	
	Ключи.Добавить("ПроизводственноеЗадание");
	Ключи.Добавить("Переработка");
	Ключи.Добавить("Номенклатура");
	Ключи.Добавить("Характеристика");
	Ключи.Добавить("Серия");
	Ключи.Добавить("Спецификация");
	Ключи.Добавить("СкладПолучатель");
	Ключи.Добавить("ЗаявкаНаАнализы");
	Ключи.Добавить("АнализыНоменклатуры");
	Ключи.Добавить("н_Организация");
	Ключи.Добавить("ЗаданиеНаМаркировку");
	
	Возврат Ключи;
	
КонецФункции

#КонецОбласти

#Область НастройкаФорм

Процедура УстановитьОформлениеПоСостояниюЗадания(Форма, ИмяТаблицы, ПутьКДаннымСостояния) Экспорт
	
	Для Каждого КлючИЗначение Из СоответствиеЦветаФонаСостояниюЗадания() Цикл
		
		ЭлементОформления = Форма.УсловноеОформление.Элементы.Добавить();
		
		ОформляемоеПоле = ЭлементОформления.Поля.Элементы.Добавить();
		ОформляемоеПоле.Поле = Новый ПолеКомпоновкиДанных(ИмяТаблицы);
		
		ЭлементОтбора = ЭлементОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных(ПутьКДаннымСостояния);
		ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ЭлементОтбора.ПравоеЗначение = КлючИЗначение.Ключ;
		
		ЭлементОформления.Оформление.УстановитьЗначениеПараметра("ЦветФона", КлючИЗначение.Значение);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура УстановитьЗначенияПоУмолчанию(Форма) Экспорт
	
	к2УчетныеТочки.ЗаполнитьПараметрыОбработки(Форма.Параметры, Форма);
	
	Форма.ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();
	
	Форма.Исполнитель = Пользователи.ТекущийПользователь();
	
	Форма.СкоростьПрокрутки = Форма.Параметры.УчетнаяТочка.СкоростьПрокрутки;
	
КонецПроцедуры

#КонецОбласти

#Область ИзменениеПериодаВарки

Процедура НачатьВарку(Серия) Экспорт
	
	ТекстОшибки = "";
	
	Если ПроверитьВыборСерии(Серия, ТекстОшибки) И ПроверитьВозможностьНачалаВарки(Серия, ТекстОшибки) Тогда
		ИзменитьПериодВарки(Серия, "НачалоВарки");
	Иначе
		ВызватьИсключение ТекстОшибки;
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗавершитьВарку(Серия) Экспорт
	
	ТекстОшибки = "";
	
	Если ПроверитьВыборСерии(Серия, ТекстОшибки) И ПроверитьВозможностьЗавершенияВарки(Серия, ТекстОшибки) Тогда
		ИзменитьПериодВарки(Серия, "ОкончаниеВарки");
	Иначе
		ВызватьИсключение ТекстОшибки;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область УсловноеОформление

Функция СоответствиеЦветаФонаСостояниюЗадания()
	
	Соответствие = Новый Соответствие;
	
	Соответствие.Вставить(Перечисления.к2СостоянияСтрокПроизводственныхЗаданий.Выполнено, ЦветаСтиля.к2ЦветВыделенияЗеленый);
	Соответствие.Вставить(Перечисления.к2СостоянияСтрокПроизводственныхЗаданий.Начато, ЦветаСтиля.к2ЦветВыделенияЖелтый);
	Соответствие.Вставить(Перечисления.к2СостоянияСтрокПроизводственныхЗаданий.ВПроцессе, ЦветаСтиля.к2ЦветВыделенияКрасный);
	
	Возврат Соответствие;
	
КонецФункции

#КонецОбласти

#Область ИзменениеПериодаВарки

Функция ПроверитьВыборСерии(Серия, ТекстОшибки)
	
	Отказ = Ложь;
	
	Если Не ЗначениеЗаполнено(Серия) Тогда
		
		СтрокаОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Партия не выбрана'"),
			Серия);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(СтрокаОшибки, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

Функция ПроверитьВозможностьНачалаВарки(Серия, ТекстОшибки)
	
	Отказ = Ложь;
	
	Если ЗначениеЗаполнено(глРеквизит(Серия, "НачалоВарки")) Тогда
		
		СтрокаОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Варка партии %1 уже начата'"),
			Серия);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(СтрокаОшибки, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

Функция ПроверитьВозможностьЗавершенияВарки(Серия, ТекстОшибки)
	
	Отказ = Ложь;
	
	Если Не ЗначениеЗаполнено(Серия.НачалоВарки)
		И Не ЗначениеЗаполнено(Серия.ОкончаниеВарки) Тогда
		
		СтрокаОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Варка %1 еще не начата'"), Серия);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(СтрокаОшибки, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Серия.ОкончаниеВарки) Тогда
		
		СтрокаОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Варка %1 уже завершена'"), Серия);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(СтрокаОшибки, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Возврат Не Отказ;
	
КонецФункции

Процедура ИзменитьПериодВарки(Серия, ИмяРеквизита)
	
	СправочникОбъект = Серия.ПолучитьОбъект();
	
	СправочникОбъект[ИмяРеквизита] = ТекущаяДатаСеанса();
	
	СправочникОбъект.Заблокировать();
	
	СправочникОбъект.Записать();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли