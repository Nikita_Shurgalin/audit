#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Функция ПолучитьМВТНастроекРеквизитов( Знач пСостояние, Знач пВерсияБП = Неопределено ) Экспорт
	
	Если пВерсияБП = Неопределено Тогда
		пВерсияБП = глРеквизит_Кэш( пСостояние, "Владелец" );
	КонецЕсли;
	
	Возврат РегистрыСведений.упТаблицаРеквизитовВерсии.МВТ( пВерсияБП );
	
КонецФункции


Функция ЭтоТЧ( пРеквизит ) Экспорт
	
	ПроверитьКорректностьРеквизита( пРеквизит );
	
	данныеРеквизита = ДанныеРеквизита( пРеквизит );
	
	Если данныеРеквизита = Неопределено Тогда
		
		Возврат упКэш.ЗначениеРеквизита( пРеквизит , "ЭтоТЧ" );
		
	Иначе
		
		Возврат данныеРеквизита.ЭтоТЧ;
		
	КонецЕсли;
	
КонецФункции // ЭтоТЧ()

Процедура ПроверитьКорректностьРеквизита( пРеквизит )
	
	Если Не ЗначениеЗаполнено( пРеквизит ) Тогда
		
		ВызватьИсключение НСтр( "ru='Не заполнен реквизит'" );
		
	КонецЕсли;
	
КонецПроцедуры	//ПроверитьКорректностьРеквизита


Функция УдалитьИзМассиваРеквизитыТЧ( пМассивРеквизитов ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упРеквизиты.Ссылка
	|ИЗ
	|	Справочник.упРеквизиты КАК упРеквизиты
	|ГДЕ
	|	упРеквизиты.Ссылка В (&массивРеквизитов)
	|	И упРеквизиты.ЭтоТЧ = ЛОЖЬ";
	
	Запрос.УстановитьПараметр( "массивРеквизитов", пМассивРеквизитов );
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции	//УдалитьИзМассиваРеквизитыТЧ


Функция ПолучитьИмяРеквизита( Знач пПредложенноеИмяРеквизита , пПрефиксИмени = "" , пИмяПоУмолчанию = Неопределено , пПроверямаяКоллекция = Неопределено , пИмяКолонкиВКоллекции = "Имя" ) Экспорт
	
	Если пИмяПоУмолчанию = Неопределено Тогда
		
		пИмяПоУмолчанию = упИК.ИмяРеквизита();
		
	КонецЕсли;
	
	ИмяРеквизита = пПредложенноеИмяРеквизита;
	
	// Проверяем имя на корректность
	
	ИмяРеквизита = СтрЗаменить( ИмяРеквизита , "." , "_" );
	
	Если упКоллекцииКлиентСервер.ЭтоКорректноеИмяПеременной( ИмяРеквизита ) Тогда
		
		ИмяРеквизита = пПрефиксИмени + ИмяРеквизита;
		
	Иначе
		
		ИмяРеквизита = пПрефиксИмени + пИмяПоУмолчанию;
		
	КонецЕсли;
	
	//  и уникальность
	
	Если Не пПроверямаяКоллекция = Неопределено Тогда
		
		ИмяРеквизита = упКоллекцииКлиентСервер.ПолучитьУникальноеИмяПеременной( ИмяРеквизита , пПроверямаяКоллекция , пИмяКолонкиВКоллекции );
		
	КонецЕсли;
	
	Возврат ИмяРеквизита;
	
КонецФункции // ПолучитьИмяРеквизита()



Функция ПолучитьТаблицуКолонокРеквизита( пРеквизит ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упРеквизитыРеквизитыТабличнойЧасти.РеквизитТабличнойЧасти КАК СсылкаНаРеквизит
	               |ИЗ
	               |	Справочник.упРеквизиты.РеквизитыТабличнойЧасти КАК упРеквизитыРеквизитыТабличнойЧасти
	               |ГДЕ
	               |	упРеквизитыРеквизитыТабличнойЧасти.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр( "Ссылка" , пРеквизит );
	
	Возврат ТаблицаПараметровРеквизитов( Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0) );
	
КонецФункции

Функция ПолучитьМассивКолонокРеквизита( пРеквизит ) Экспорт
	
	Возврат ПолучитьТаблицуКолонокРеквизита( пРеквизит ).ВыгрузитьКолонку( "СсылкаНаРеквизит" );
	
КонецФункции

Функция ПолучитьТипПредопределенногоЭлемента( пИмяЭлемента ) Экспорт
	
	лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.ПустаяСсылка();
	
	Если пИмяЭлемента = "KPI"
		ИЛИ пИмяЭлемента = "Версия"
		ИЛИ пИмяЭлемента = "ВыборкаСессий"
		ИЛИ пИмяЭлемента = "Действие"
		ИЛИ пИмяЭлемента = "Задача"
		ИЛИ пИмяЭлемента = "ОбработчикСобытия"
		ИЛИ пИмяЭлемента = "Пользователь"
		ИЛИ пИмяЭлемента = "Сессия"
		ИЛИ пИмяЭлемента = "Событие" Тогда
		
		лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов[пИмяЭлемента];
		
	ИначеЕсли пИмяЭлемента = "Задача_Адресация" Тогда
		
		лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Адресация;
		
	ИначеЕсли пИмяЭлемента = "Задача_ДатаЗавершения"
		ИЛИ пИмяЭлемента = "Задача_ДатаНачала" Тогда
		
		лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Дата;
		
	ИначеЕсли пИмяЭлемента = "Задача_Важность" Тогда
		
		лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Важность;
		
	ИначеЕсли пИмяЭлемента = "Задача_Стоимость" Тогда
		
		лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Число;
		
	ИначеЕсли пИмяЭлемента = "ЗаписьШиныОбмена"
		ИЛИ пИмяЭлемента = "ИнформацияОПолученииСообщения"
		ИЛИ пИмяЭлемента = "ОтправительСообщения" Тогда
		
		лТип = ПланыВидовХарактеристик.упТипыЗначенийРеквизитов.Булево;
		
	КонецЕсли;
	
	Возврат лТип;
	
КонецФункции


Функция ПолучитьСлучайноеЗначениеРеквизита( пРеквизит ) Экспорт
	
	Если ЭтоТЧ( пРеквизит ) Тогда
		
		соотвКолонок = Неопределено;
		
		тз = упРеквизитыКлиентСервер.ИнициализацияТаблицыБП( пРеквизит, соотвКолонок );
		
		Для ц = 0 По СлучайноеЧисло(,10) Цикл
			
			новСтрока = тз.Добавить();
			
			Для Каждого цЭлемент Из соотвКолонок Цикл
				
				новСтрока[цЭлемент.Значение] = ПолучитьСлучайноеЗначениеРеквизита( цЭлемент.Ключ );
				
			КонецЦикла;
			
		КонецЦикла;
		
		Возврат тз;
		
	Иначе
		
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
		|	упРеквизитыВозможныеЗначенияДляТестирования.Значение
		|ИЗ
		|	Справочник.упРеквизиты.ВозможныеЗначенияДляТестирования КАК упРеквизитыВозможныеЗначенияДляТестирования
		|ГДЕ
		|	упРеквизитыВозможныеЗначенияДляТестирования.Ссылка = &СсылкаНаРеквизит
		|	И упРеквизитыВозможныеЗначенияДляТестирования.ДиапазонПосле >= &СлучайноеЧисло
		|	И упРеквизитыВозможныеЗначенияДляТестирования.ДиапазонДо < &СлучайноеЧисло";
		
		Запрос.УстановитьПараметр( "СлучайноеЧисло" , СлучайноеЧисло() );
		Запрос.УстановитьПараметр( "СсылкаНаРеквизит" , пРеквизит );
		
		Выборка = Запрос.Выполнить().Выбрать();
		
		Если Выборка.Следующий() Тогда
			
			Возврат Выборка.Значение;
			
		Иначе
			
			Возврат глРеквизит_Кэш( пРеквизит , "ПустоеЗначениеТипа" );
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецФункции

Функция СлучайноеЧисло( пНачалоДиапазона = 1, пКонецДиапазона = 100)
	
	ГСЧ = Новый ГенераторСлучайныхЧисел;
	Возврат ГСЧ.СлучайноеЧисло( пНачалоДиапазона , пКонецДиапазона );
	
КонецФункции


Функция МассивДопустимыхЗначенийПеречисления( пРеквизит ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	упРеквизитыДопустимыеЗначения.ДопустимоеЗначение
	|ИЗ
	|	Справочник.упРеквизиты.ДопустимыеЗначения КАК упРеквизитыДопустимыеЗначения
	|ГДЕ
	|	упРеквизитыДопустимыеЗначения.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	упРеквизитыДопустимыеЗначения.НомерСтроки";
	
	Запрос.УстановитьПараметр("Ссылка", пРеквизит);
	
	массивДопустимыхЗначений = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
	Если массивДопустимыхЗначений.Количество() = 0 Тогда
		// значения не заданы на карточке, считаем, что доступны все
		
		массивДопустимыхЗначений = упКэш.ДополнительныеЗначенияХарактеристик( глРеквизит_Кэш( пРеквизит, "ТипРеквизита" ) );
		
	КонецЕсли;
	
	Возврат массивДопустимыхЗначений;
	
КонецФункции


Функция ПустаяТаблицаПараметровРеквизитов() Экспорт
	
	тз = Новый ТаблицаЗначений;
	
	тз.Колонки.Добавить( "СсылкаНаРеквизит", Новый ОписаниеТипов("СправочникСсылка.упРеквизиты") );
	тз.Колонки.Добавить( "Наименование", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(150, ДопустимаяДлина.Переменная)) );
	тз.Колонки.Добавить( "ТипРеквизита", Новый ОписаниеТипов("ПланВидовХарактеристикСсылка.упТипыЗначенийРеквизитов") );
	тз.Колонки.Добавить( "ЭтоТЧ", Новый ОписаниеТипов("Булево") );
	тз.Колонки.Добавить( "ИндексКартинки", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(2, 0, ДопустимыйЗнак.Неотрицательный)) );
	тз.Колонки.Добавить( "Имя", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(100, ДопустимаяДлина.Переменная)) );
	тз.Колонки.Добавить( "ТипЗначения", Новый ОписаниеТипов("ОписаниеТипов") );
	тз.Колонки.Добавить( "ИмяФормыВыбора", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(500, ДопустимаяДлина.Переменная)) );
	тз.Колонки.Добавить( "СвязьПоВладельцу", Новый ОписаниеТипов("СправочникСсылка.упРеквизиты") );
	тз.Колонки.Добавить( "МногострочныйРежим", Новый ОписаниеТипов("Булево") );
	тз.Колонки.Добавить( "Формат", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(50, ДопустимаяДлина.Переменная)) );
	тз.Колонки.Добавить( "Подсказка", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(500, ДопустимаяДлина.Переменная)) );
	тз.Колонки.Добавить( "Маска", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(150, ДопустимаяДлина.Переменная)) );
	тз.Колонки.Добавить( "Видимость", Новый ОписаниеТипов("Булево") );
	тз.Колонки.Добавить( "Доступность", Новый ОписаниеТипов("Булево") );
	
	тз.Индексы.Добавить( "СсылкаНаРеквизит" );
	
	Возврат тз;
	
КонецФункции


Функция ТаблицаПараметровРеквизитов( пМассивРеквизитов ) Экспорт
	
	тзПараметровРеквизитов = ПустаяТаблицаПараметровРеквизитов();
	
	// поиск в закешированных данных
	
	массивОтсутствующихРеквизитов = Новый Массив;
	сущДанные = упКэшНаВызов.ДанныеОРеквизитах();
	
	Для Каждого цРеквизит Из пМассивРеквизитов Цикл
		
		найденнаяСтрока = сущДанные.Найти( цРеквизит, "СсылкаНаРеквизит" );
		
		Если найденнаяСтрока = Неопределено Тогда
			
			массивОтсутствующихРеквизитов.Добавить( цРеквизит );
			
		Иначе
			
			ЗаполнитьЗначенияСвойств( тзПараметровРеквизитов.Добавить(), найденнаяСтрока );
			
		КонецЕсли;
		
	КонецЦикла;
	
	// все, что не нашли в кеше оптом берем с базы
	
	Если массивОтсутствующихРеквизитов.Количество() > 0 Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	упРеквизиты.Ссылка КАК СсылкаНаРеквизит,
		               |	упРеквизиты.Наименование,
		               |	упРеквизиты.ТипРеквизита,
		               |	упРеквизиты.ЭтоТЧ КАК ЭтоТЧ,
		               |	упРеквизиты.ИндексКартинки,
		               |	упРеквизиты.Имя,
		               |	упРеквизиты.ТипРеквизита.ТипЗначения КАК ТипЗначения,
		               |	упРеквизиты.ИмяФормыВыбора,
		               |	упРеквизиты.СвязьПоВладельцу,
		               |	упРеквизиты.МногострочныйРежим,
		               |	упРеквизиты.Формат,
		               |	упРеквизиты.Подсказка,
		               |	упРеквизиты.Маска,
		               |	ИСТИНА КАК Видимость,
		               |	ИСТИНА КАК Доступность
		               |ИЗ
		               |	Справочник.упРеквизиты КАК упРеквизиты
		               |ГДЕ
		               |	упРеквизиты.Ссылка В(&МассивРеквизитов)";
		
		Запрос.УстановитьПараметр("МассивРеквизитов", массивОтсутствующихРеквизитов );
		
		Для Каждого цСтрока Из Запрос.Выполнить().Выгрузить() Цикл
			
			ЗаполнитьЗначенияСвойств( тзПараметровРеквизитов.Добавить(), цСтрока );
			ЗаполнитьЗначенияСвойств( упКэшНаВызов.ДанныеОРеквизитах().Добавить(), цСтрока );  //положим в кеш новые данные
			
		КонецЦикла;
		
	КонецЕсли;
	
	Возврат тзПараметровРеквизитов;
	
КонецФункции



Функция ТипЗначения( пРеквизит ) Экспорт
	
	ПроверитьКорректностьРеквизита( пРеквизит );
	
	данныеРеквизита = ДанныеРеквизита( пРеквизит );
	
	Если данныеРеквизита = Неопределено Тогда
		
		Возврат глРеквизит_Кэш( пРеквизит , "ТипРеквизита.ТипЗначения" );
		
	Иначе
		
		Возврат данныеРеквизита.ТипЗначения;
		
	КонецЕсли;
	
КонецФункции	//ТипЗначения

Функция ДанныеРеквизита( пРеквизит )
	
	тзДанныеОРеквизитах = упКэшНаВызов.ДанныеОРеквизитах();
	
	найденнаяСтрока = тзДанныеОРеквизитах.Найти( пРеквизит, "СсылкаНаРеквизит" );
	
	Если найденнаяСтрока = Неопределено Тогда
		
		тз = ТаблицаПараметровРеквизитов( глСоздатьМассив( пРеквизит ) );
		
		Если тз.Количество() > 0 Тогда
			Возврат тз[0];
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат найденнаяСтрока;
	
КонецФункции




Функция РеквизитыПоИмениДляСписка( пИмяДляСписка, пИсключаяСсылку ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	упРеквизиты.Ссылка
	               |ИЗ
	               |	Справочник.упРеквизиты КАК упРеквизиты
	               |ГДЕ
	               |	упРеквизиты.Ссылка <> &Ссылка
	               |	И упРеквизиты.ИмяДляСписка = &ИмяДляСписка
	               |	И упРеквизиты.ИмяДляСписка <> """"";
	
	Запрос.УстановитьПараметр("Ссылка", пИсключаяСсылку);
	Запрос.УстановитьПараметр("ИмяДляСписка", пИмяДляСписка);
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции


//КОЛЛЕКЦИЯ ОБХОДА {

Функция РеквизитКорректенДляКоллекцииОбхода( пРеквизит ) Экспорт
	
	ПроверитьКорректностьРеквизита( пРеквизит );
	
	Возврат ЭтоТЧ( пРеквизит );
	
КонецФункции	//РеквизитКорректенДляКоллекцииОбхода

Функция МассивРеквизитовКоллекцииОбхода( пКоллекцияОбхода ) Экспорт
	
	Возврат ПолучитьМассивКолонокРеквизита( пКоллекцияОбхода );
	
КонецФункции	//МассивРеквизитовКоллекцииОбхода

//}

#КонецЕсли

//////////////////////////////////////////////////////////////////////////////////////
// {{ЛОКАЛИЗАЦИЯ

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)
	
	упЛокализацияКлиентСервер.ОбработкаПолученияПолейПредставления( Поля, СтандартнаяОбработка );
	
КонецПроцедуры

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	
	упЛокализацияКлиентСервер.ОбработкаПолученияПредставления( Данные, Представление, СтандартнаяОбработка );
	
КонецПроцедуры

// }}ЛОКАЛИЗАЦИЯ
