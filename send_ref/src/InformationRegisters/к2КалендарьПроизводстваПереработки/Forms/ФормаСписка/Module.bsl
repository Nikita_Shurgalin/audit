
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НастроитьРасписание(Команда)
	
	ОткрытьФорму("РегистрСведений.к2КалендарьПроизводстваПереработки.Форма.ФормаНастройкиРасписания");
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьКалендарь(Команда)
	
	ОткрытьФорму("РегистрСведений.к2КалендарьПроизводстваПереработки.Форма.ЗаполнениеРасписанияНаПериод");
	
КонецПроцедуры

#КонецОбласти

