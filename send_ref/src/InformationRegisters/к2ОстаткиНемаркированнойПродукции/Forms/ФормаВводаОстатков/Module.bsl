
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("ДатаОстатков") Тогда
		
		Дата = Параметры.ДатаОстатков;
		
	КонецЕсли;
	
	ЗаполнитьШапкуТабличногоДокумента();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПрочитатьДанные(Команда)
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаПодтверждения;
	ПрочитатьДанныеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьДанные(Команда)
	
	Если ПроверитьЗаполнение() Тогда
		ЗагрузитьНаСервере();
		Закрыть(Истина);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеРеквизитовФормы

&НаСервере
Процедура ЗаполнитьШапкуТабличногоДокумента()
	
	ТабличныйДокумент.Очистить();
	
	Макет =	РегистрыСведений.к2ОстаткиНемаркированнойПродукции.ПолучитьМакет("МакетВводаОстатков");
	
	к2ОбщегоНазначенияКлиентСервер.ВывестиОбласть(ТабличныйДокумент, Макет, "Шапка");
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПрочитатьДанныеНаСервере()
	
	КоличествоСтрок = ТабличныйДокумент.ВысотаТаблицы;
	
	Для цСтр = 2 По КоличествоСтрок Цикл

        новСтрока = Остатки.Добавить();
		новСтрока.Наименование = Знч(цСтр, 1);
		новСтрока.Номенклатура = ПолучитьНоменклатуру(новСтрока.Наименование);
		новСтрока.ДатаПартии = СтроковыеФункцииКлиентСервер.СтрокаВДату(Знч(цСтр, 2));
		новСтрока.Остаток = Знч(цСтр, 3);

    КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция Знч( пНомерСтроки, пНомерКолонки )

    Возврат ТабличныйДокумент.Область("R" + Формат( пНомерСтроки , "ЧГ=" ) + "C" + Формат(пНомерКолонки , "ЧГ=" ) ).Текст;

КонецФункции

&НаСервере
Функция ПолучитьНоменклатуру(Наименование)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	Номенклатура.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.Номенклатура КАК Номенклатура
		|ГДЕ
		|	Номенклатура.Наименование = &Наименование
		|	И НЕ Номенклатура.ПометкаУдаления";
	
	Запрос.УстановитьПараметр("Наименование", Наименование);
	
	Возврат к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).Ссылка;
	
КонецФункции

#КонецОбласти

#Область ЗаписьДанных

&НаСервере
Процедура ЗагрузитьНаСервере()
	
	тз = Остатки.Выгрузить(,"Номенклатура,ДатаПартии,Остаток");
	тз.Свернуть("Номенклатура,ДатаПартии", "Остаток");
	
	Для Каждого Строка Из тз Цикл
		Если НЕ ЗначениеЗаполнено(Строка.Номенклатура) Тогда
			Продолжить;
		КонецЕсли;
		
		Структура = Новый Структура();
		Структура.Вставить("Период", Дата);
		Структура.Вставить("Номенклатура", Строка.Номенклатура);
		Структура.Вставить("ДатаПартии", Строка.ДатаПартии);
		Структура.Вставить("Количество", Строка.Остаток);
		
		РегистрыСведений.к2ОстаткиНемаркированнойПродукции.Отразить(Структура);
		
	КонецЦикла;
	
КонецПроцедуры
	
#КонецОбласти

#КонецОбласти
