#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Печать

// Сформировать печатные формы объектов
//
// ВХОДЯЩИЕ:
//   ИменаМакетов    - Строка    - Имена макетов, перечисленные через запятую
//   МассивОбъектов  - Массив    - Массив ссылок на объекты которые нужно распечатать
//   ПараметрыПечати - Структура - Структура дополнительных параметров печати.
//
// ИСХОДЯЩИЕ:
//   КоллекцияПечатныхФорм - Таблица значений - Сформированные табличные документы
//   ПараметрыВывода       - Структура        - Параметры сформированных табличных документов.
//
Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "СчетНаОплату") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"СчетНаОплату",
			НСтр("ru='Счет на оплату'"),
			СформироватьПечатнуюФормуСчетНаОплату(МассивОбъектов, ОбъектыПечати, ПараметрыПечати));
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СчетНаОплату

Функция СформироватьПечатнуюФормуСчетНаОплату(МассивОбъектов, ОбъектыПечати, ПараметрыПечати) Экспорт
	
	СтруктураТипов = к2ОбщегоНазначения.СоответствиеМассивовПоТипамОбъектов(МассивОбъектов);
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.АвтоМасштаб = Истина;
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_СЧЕТНАОПЛАТУ";
	
	НомерТипаДокумента = 0;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Для Каждого СтруктураОбъектов Из СтруктураТипов Цикл
		
		НомерТипаДокумента = НомерТипаДокумента + 1;
		Если НомерТипаДокумента > 1 Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(СтруктураОбъектов.Ключ);
		ДанныеДляПечати = МенеджерОбъекта.ПолучитьДанныеДляПечатнойФормыСчетаНаОплату(ПараметрыПечати, СтруктураОбъектов.Значение);
		
		ЗаполнитьТабличныйДокументСчетаНаОплату(ТабличныйДокумент, ДанныеДляПечати, ОбъектыПечати, ПараметрыПечати);
		
	КонецЦикла;
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат ТабличныйДокумент;
	
КонецФункции

Процедура ЗаполнитьРеквизитыШапкиСчетаНаОплату(ДанныеПечати, Макет, ТабличныйДокумент, ТаблицаЭтапыОплаты, ТаблицаТовары)
	
	СведенияОПоставщике = к2Печать.СведенияОЮрФизЛице(ДанныеПечати.Организация);
	
	ОбластьМакета = Макет.ПолучитьОбласть("ЗаголовокСчета");
	к2ШтрихкодыДокументов.ВывестиШтрихкодВТабличныйДокумент(ТабличныйДокумент, Макет, ОбластьМакета, ДанныеПечати.Ссылка);
	
	Если ТаблицаЭтапыОплаты.Количество() = 0 Тогда
		ДатаПлатежа = '00010101';
	ИначеЕсли ТаблицаЭтапыОплаты.Количество() = 1 Тогда
		ДатаПлатежа = ТаблицаЭтапыОплаты[0].ДатаПлатежа;
	Иначе
		ДатаПлатежа = ТаблицаЭтапыОплаты[ТаблицаЭтапыОплаты.Количество()-1].ДатаПлатежа;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ДатаПлатежа) Тогда
		СтруктураДанныхЗаголовок = Новый Структура;
		НадписьСрокДействия = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru='Счет действителен до %1.'"), Формат(ДатаПлатежа, "ДЛФ=D")) + " ";
		СтруктураДанныхЗаголовок.Вставить("СрокДействия", НадписьСрокДействия);
		ОбластьМакета.Параметры.Заполнить(СтруктураДанныхЗаголовок);
	КонецЕсли;
	
	ТабличныйДокумент.Вывести(ОбластьМакета);
	
	Если ДанныеПечати.ПлатежЗаРубеж Тогда
		
		ОбластьМакета = Макет.ПолучитьОбласть("ОбразецЗаполненияРеквизитыБанка");
		СтруктураДанныхШапки = Новый Структура;
		ПредставлениеПоставщикаДляПлатПоручения = "";
		
		Если ЗначениеЗаполнено(ДанныеПечати.БанковскийСчет) Тогда
			СтруктураДанныхШапки.Вставить("СчетБанкаПолучателяПредставление", ДанныеПечати.НомерБанковскогоСчета);
			СтруктураДанныхШапки.Вставить("БанкПолучателяПредставление", ДанныеПечати.НаименованиеБанка);
			СтруктураДанныхШапки.Вставить("АдресБанкаПолучателяПредставление", ДанныеПечати.АдресБанка);
			СтруктураДанныхШапки.Вставить("СВИФТБанка", ДанныеПечати.СВИФТБанка);
			ПредставлениеПоставщикаДляПлатПоручения = ДанныеПечати.БанковскийСчетТекстКорреспондента;
		КонецЕсли;
		
		Если ПустаяСтрока(ПредставлениеПоставщикаДляПлатПоручения) Тогда
			ПредставлениеПоставщикаДляПлатПоручения = к2Печать.ОписаниеОрганизации(СведенияОПоставщике, "ПолноеНаименование,");
		КонецЕсли;
		СтруктураДанныхШапки.Вставить("ПредставлениеПоставщикаДляПлатПоручения", ПредставлениеПоставщикаДляПлатПоручения);
		ОбластьМакета.Параметры.Заполнить(СтруктураДанныхШапки);
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		Если Не ПустаяСтрока(ДанныеПечати.НаименованиеБанкаДляРасчетов) Тогда
			ОбластьМакета = Макет.ПолучитьОбласть("ОбразецЗаполненияРеквизитыБанкаКорреспондента");
			
			СтруктураДанныхШапки.Очистить();
			СтруктураДанныхШапки.Вставить("БанкКорреспондентПолучателяПредставление",
				ДанныеПечати.НаименованиеБанкаДляРасчетов + " " + ДанныеПечати.АдресБанкаДляРасчетов);
			СтруктураДанныхШапки.Вставить("СВИФТБанкаДляРасчетов", ДанныеПечати.СВИФТБанкаДляРасчетов);
			СтруктураДанныхШапки.Вставить("СчетБанкаДляРасчетовПредставление", ДанныеПечати.СчетВБанкеДляРасчетов);
			
			ОбластьМакета.Параметры.Заполнить(СтруктураДанныхШапки);
			ТабличныйДокумент.Вывести(ОбластьМакета);
		КонецЕсли;
		
		ОбластьМакета = Макет.ПолучитьОбласть("ОбразецЗаполненияНазначениеПлатежа");
		СтруктураДанныхШапки.Очистить();
		
		СтруктураДанныхШапки.Вставить("НазначениеПлатежа", ДанныеПечати.НазначениеПлатежа);
		ОбластьМакета.Параметры.Заполнить(СтруктураДанныхШапки);
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
	Иначе
	
		Если ДанныеПечати.КонтрагентЮрФизЛицо = Перечисления.к2ЮрФизЛицо.ФизЛицо
			И ЗначениеЗаполнено(ДанныеПечати.БанковскийСчет) Тогда
			ОбластьМакета = Макет.ПолучитьОбласть("ОбразецЗаполненияППСКодом");
		Иначе
			ОбластьМакета = Макет.ПолучитьОбласть("ОбразецЗаполненияПП");
		КонецЕсли;
		
		СтруктураДанныхШапки = Новый Структура;
		СтруктураДанныхШапки.Вставить("ИНН", СведенияОПоставщике.ИНН);
		СтруктураДанныхШапки.Вставить("КПП",СведенияОПоставщике.КПП);
		ПредставлениеПоставщикаДляПлатПоручения = "";
		
		СтруктураДанныхШапки.Вставить("ИдентификаторПлатежа", ДанныеПечати.ИдентификаторПлатежа);
		
		Если ЗначениеЗаполнено(ДанныеПечати.БанковскийСчет) Тогда
			
			Если ЗначениеЗаполнено(ДанныеПечати.БИКБанкаДляРасчетов) Тогда
				Банк		= ДанныеПечати.НаименованиеБанкаДляРасчетов;
				БИК         = ДанныеПечати.БИКБанкаДляРасчетов;
				КоррСчет    = ДанныеПечати.КоррСчетБанкаДляРасчетов;
				ГородБанка  = ДанныеПечати.ГородБанкаДляРасчетов;
				НомерСчета  = ДанныеПечати.КоррСчетБанка;
			Иначе
				Банк		= ДанныеПечати.НаименованиеБанка;
				БИК         = ДанныеПечати.БИКБанк;
				КоррСчет    = ДанныеПечати.КоррСчетБанка;
				ГородБанка  = ДанныеПечати.ГородБанка;
				НомерСчета  = ДанныеПечати.НомерБанковскогоСчета;
			КонецЕсли;
			
			СтруктураДанныхШапки.Вставить("БИКБанкаПолучателя", БИК);
			СтруктураДанныхШапки.Вставить("БанкПолучателя", Банк);
			СтруктураДанныхШапки.Вставить("БанкПолучателяПредставление", СокрЛП(Банк) + " " + ГородБанка);
			СтруктураДанныхШапки.Вставить("СчетБанкаПолучателя", КоррСчет);
			СтруктураДанныхШапки.Вставить("СчетБанкаПолучателяПредставление", КоррСчет);
			СтруктураДанныхШапки.Вставить("СчетПолучателяПредставление", НомерСчета);
			СтруктураДанныхШапки.Вставить("СчетПолучателя", НомерСчета);
			ПредставлениеПоставщикаДляПлатПоручения = ДанныеПечати.БанковскийСчетТекстКорреспондента;
			
		КонецЕсли;
	
		Если ПустаяСтрока(ПредставлениеПоставщикаДляПлатПоручения) Тогда
			ПредставлениеПоставщикаДляПлатПоручения = к2Печать.ОписаниеОрганизации(СведенияОПоставщике, "ПолноеНаименование,");
		КонецЕсли;
		
		СтруктураДанныхШапки.Вставить("НазначениеПлатежа", ДанныеПечати.НазначениеПлатежа);
		СтруктураДанныхШапки.Вставить("ПредставлениеПоставщикаДляПлатПоручения", ПредставлениеПоставщикаДляПлатПоручения);
		ОбластьМакета.Параметры.Заполнить(СтруктураДанныхШапки);
		
		Если ДанныеПечати.КонтрагентЮрФизЛицо = Перечисления.к2ЮрФизЛицо.ФизЛицо
			И ЗначениеЗаполнено(ДанныеПечати.БанковскийСчет) Тогда
			
			РеквизитыПлатежа = РеквизитыПлатежаQRКод(СтруктураДанныхШапки);
			РеквизитыПлатежа.СуммаЧислом = СуммаКОплатеПоСчету(ДанныеПечати, ТаблицаТовары);
			
			ВывестиQRКод(РеквизитыПлатежа, ДанныеПечати, ОбластьМакета);
			
		КонецЕсли;
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
	КонецЕсли;
	
	КолонкаКодов = "Код";
	ВыводитьКоды = ЗначениеЗаполнено(КолонкаКодов);
	
	Смещать = ТипСмещенияТабличногоДокумента.ПоВертикали;
	ОбластьПервойКолонкиТоваров = Макет.Область("ПерваяКолонкаТовара");
	Если НЕ ВыводитьКоды Тогда
		ОбластьПервойКолонкиТоваров.ШиринаКолонки = ОбластьПервойКолонкиТоваров.ШиринаКолонки + Макет.Область("КолонкаКодов").ШиринаКолонки;
		Макет.УдалитьОбласть(Макет.Область("КолонкаКодов"), Смещать);
	КонецЕсли;
	
	ОбластьМакета = Макет.ПолучитьОбласть("Заголовок");
	
	ТекстЗаголовка = к2ПечатьКлиентСервер.СформироватьЗаголовокДокумента(ДанныеПечати, НСтр("ru='Счет на оплату'"));
	СтруктураДанныхШапки = Новый Структура;
	СтруктураДанныхШапки.Вставить("ТекстЗаголовка", ТекстЗаголовка);
	ОбластьМакета.Параметры.Заполнить(СтруктураДанныхШапки);
	ТабличныйДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("Поставщик");
	
	СтруктураДанныхПоставщик = Новый Структура;
	СтруктураДанныхПоставщик.Вставить("ПредставлениеПоставщика",
		к2Печать.ОписаниеОрганизации(к2Печать.СведенияОЮрФизЛице(ДанныеПечати.ОрганизацияПоставщик),
			"ПолноеНаименование,ИНН,КПП,ЮридическийАдрес,Телефоны,"));
	ОбластьМакета.Параметры.Заполнить(СтруктураДанныхПоставщик);
	ТабличныйДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("Покупатель");
	СтруктураДанныхПокупатель = Новый Структура;
	СтруктураДанныхПокупатель.Вставить("ПредставлениеПолучателя",
		к2Печать.ОписаниеОрганизации(к2Печать.СведенияОЮрФизЛице(ДанныеПечати.Контрагент),
			"ПолноеНаименование,ИНН,КПП,ЮридическийАдрес,Телефоны,"));
	ОбластьМакета.Параметры.Заполнить(СтруктураДанныхПокупатель);
	ТабличныйДокумент.Вывести(ОбластьМакета);
	
	Если ЗначениеЗаполнено(ДанныеПечати.Грузоотправитель) Тогда
		ОбластьМакета = Макет.ПолучитьОбласть("Грузоотправитель");
		СтруктураДанныхГрузоотправитель = Новый Структура;
		СтруктураДанныхГрузоотправитель.Вставить("ПредставлениеГрузоотправителя", ОписаниеОрганизации(ДанныеПечати, "Грузоотправитель"));
		ОбластьМакета.Параметры.Заполнить(СтруктураДанныхГрузоотправитель);
		ТабличныйДокумент.Вывести(ОбластьМакета);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ДанныеПечати.Грузополучатель) Тогда
		ОбластьМакета = Макет.ПолучитьОбласть("Грузополучатель");
		СтруктураДанныхГрузополучатель = Новый Структура;
		СтруктураДанныхГрузополучатель.Вставить("ПредставлениеГрузополучателя", ОписаниеОрганизации(ДанныеПечати, "Грузополучатель"));
		ОбластьМакета.Параметры.Заполнить(СтруктураДанныхГрузополучатель);
		ТабличныйДокумент.Вывести(ОбластьМакета);
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьРеквизитыПодвалаСчетаНаОплату(ДанныеПечати, Макет, ТабличныйДокумент, ТаблицаЭтапыОплаты, ПараметрыПечати)
	
	МассивПроверкиВывода = Новый Массив;
	
	// Вывести этапы графика оплаты
	Если ТаблицаЭтапыОплаты.Количество() > 1 Тогда
		
		ИмяКолонкиДатыОплаты =  НСтр("ru = 'Дата оплаты'");
		
		ОбластьШапкаТаблицы = Макет.ПолучитьОбласть("ШапкаТаблицыЭтапыОплаты");
		ОбластьПодвалТаблицы = Макет.ПолучитьОбласть("ИтогоЭтапыОплаты");
		СтруктураДанныхШапки = Новый Структура("ИмяКолонкиДатыОплаты", ИмяКолонкиДатыОплаты);
		ОбластьШапкаТаблицы.Параметры.Заполнить(СтруктураДанныхШапки);
		МассивПроверкиВывода.Добавить(ОбластьШапкаТаблицы);
		МассивПроверкиВывода.Добавить(ОбластьПодвалТаблицы);
		
		ОбластьСтрокаТаблицы = Макет.ПолучитьОбласть("СтрокаТаблицыЭтапыОплаты");
		
		НомерЭтапа = 1;
		Для Каждого ТекЭтап Из ТаблицаЭтапыОплаты Цикл
			
			ПараметрыСтроки = НовыеПараметрыСтрокиЭтапа();
			ЗаполнитьЗначенияСвойств(ПараметрыСтроки, ТекЭтап);
			ПараметрыСтроки.НомерСтроки = НомерЭтапа;
			Если Не ПараметрыСтроки.ЭтоЗалогЗаТару Тогда
				ПараметрыСтроки.ТекстНДС = СформироватьТекстНДСЭтапаОплаты(
					ТекЭтап.СуммаНДС,
					ТекЭтап.ПроцентПлатежа);		
			Иначе
				ПараметрыСтроки.ПроцентПлатежа = "-";
				ПараметрыСтроки.ТекстНДС = НСтр("ru = 'Залог за тару. Без налога (НДС).'");
			КонецЕсли;
			
			ОбластьСтрокаТаблицы.Параметры.Заполнить(ПараметрыСтроки);
			
			МассивПроверкиВывода.Добавить(ОбластьСтрокаТаблицы);
			
			Если ТабличныйДокумент.ПроверитьВывод(МассивПроверкиВывода) Тогда
				Если НомерЭтапа = 1 Тогда
					ТабличныйДокумент.Вывести(ОбластьШапкаТаблицы);
					МассивПроверкиВывода.Удалить(0);
				КонецЕсли;
			Иначе
				ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
				ТабличныйДокумент.Вывести(ОбластьШапкаТаблицы);
			КонецЕсли;
			
			ТабличныйДокумент.Вывести(ОбластьСтрокаТаблицы);
			МассивПроверкиВывода.Удалить(МассивПроверкиВывода.ВГраница());
			
			НомерЭтапа = НомерЭтапа + 1;
			
		КонецЦикла;
		
		ТабличныйДокумент.Вывести(ОбластьПодвалТаблицы);
		
	КонецЕсли;
	
	// Вывести дополнительную информацию
	Если ЗначениеЗаполнено(ДанныеПечати.ДополнительнаяИнформация) Тогда
		
		Область = Макет.ПолучитьОбласть("ДополнительнаяИнформация");
		Область.Параметры.Заполнить(ДанныеПечати);
		ТабличныйДокумент.Вывести(Область);
		
	КонецЕсли;
	
	// Вывести подписи
	Область = Макет.ПолучитьОбласть("ПодвалСчета");
	СтруктураДанныхПодвал = Новый Структура;
	СтруктураДанныхПодвал.Вставить("ФИОРуководителя", ДанныеПечати.Руководитель);
	СтруктураДанныхПодвал.Вставить("ДолжностьРуководителя", ДанныеПечати.ДолжностьРуководителя);
	СтруктураДанныхПодвал.Вставить("ФИОБухгалтера", ДанныеПечати.ГлавныйБухгалтер);
	СтруктураДанныхПодвал.Вставить("ФИОМенеджер", ДанныеПечати.Менеджер);
	Область.Параметры.Заполнить(СтруктураДанныхПодвал);
	
	МассивПроверкиВывода.Очистить();
	МассивПроверкиВывода.Добавить(Область);
	Если НЕ ТабличныйДокумент.ПроверитьВывод(МассивПроверкиВывода) Тогда
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	КонецЕсли;
	ТабличныйДокумент.Вывести(Область);
	
КонецПроцедуры

Процедура ЗаполнитьТабличныйДокументСчетаНаОплату(ТабличныйДокумент, ДанныеДляПечати, ОбъектыПечати, ПараметрыПечати)
	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Обработка.ПечатьСчетовНаОплату.ПФ_MXL_СчетНаОплату");
	типДенежнаяСумма = Метаданные.ОпределяемыеТипы.к2ДенежнаяСумма.Тип;
	
	ШаблоныОшибок = Новый Структура;
	ШаблоныОшибок.Вставить("Товары", НСтр("ru = 'В документе %1 отсутствуют товары. Печать счета на оплату не требуется'"));
	ШаблоныОшибок.Вставить("ЗаменяющиеТовары", НСтр("ru = 'В документе %1 отсутствуют заменяющие товары. Печать счета на оплату не требуется'"));
	ШаблоныОшибок.Вставить("Этапы", НСтр("ru = 'В документе %1 отсутствуют этапы оплаты. Печать счета на оплату не требуется'"));
	
	СтруктураИмяДопКолонки = Новый Структура("ИмяКолонкиКодов", "Код");
	
	ДанныеПечати = ДанныеДляПечати.РезультатПоШапке.Выбрать();
	ЭтапыОплаты = ДанныеДляПечати.РезультатПоЭтапамОплаты.Выгрузить();
	Товары = ДанныеДляПечати.РезультатПоТабличнойЧасти.Выгрузить();
	
	ЭтапыЗалоговойТары = ЭтапыОплаты.НайтиСтроки(Новый Структура("ЭтоЗалогЗаТару", Истина));
	ТолькоЗалогЗаТару = ЭтапыЗалоговойТары.Количество() = ЭтапыОплаты.Количество() И ЭтапыЗалоговойТары.Количество() > 0;
	
	ПоказыватьНДС = Истина;
	ЕстьСкидки = Истина;
	ПервыйДокумент = Истина;
	
	Пока ДанныеПечати.Следующий() Цикл
				
		СтруктураПоиска = Новый Структура("Ссылка", ДанныеПечати.Ссылка);
		
		ТаблицаТовары = Товары.НайтиСтроки(СтруктураПоиска);
		ТаблицаЭтапыОплаты = ЭтапыОплаты.НайтиСтроки(СтруктураПоиска);
		
		Если ПервыйДокумент Тогда
			ПервыйДокумент = Ложь;
		Иначе
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		СуффиксОбласти = ?(ДанныеПечати.УчитыватьНДС И НЕ ТолькоЗалогЗаТару И ПоказыватьНДС, "СНДС", "") + ?(ЕстьСкидки, "СоСкидкой", "");
		
		ЗаполнитьРеквизитыШапкиСчетаНаОплату(ДанныеПечати, Макет, ТабличныйДокумент, ТаблицаЭтапыОплаты, ТаблицаТовары);
			
		// Таблица "Товары"
		ОбластьШапкаТаблицы          = Макет.ПолучитьОбласть("ШапкаТаблицы" + СуффиксОбласти);
		ОбластьСтрокаТаблицы 		 = Макет.ПолучитьОбласть("СтрокаТаблицы" + СуффиксОбласти);
		ОбластьПодвалТаблицы         = Макет.ПолучитьОбласть("ПодвалТаблицы" + СуффиксОбласти);
		ОбластьПодвалНДС             = Макет.ПолучитьОбласть("ПодвалТаблицыНДС");
			
		ВыводШапки = 0;
			
		Если ДанныеПечати.УчитыватьНДС И НЕ ТолькоЗалогЗаТару Тогда
			СуффиксОбластиПодвал = "СНДС" + ?(ЕстьСкидки, "СоСкидкой", "");
			ОбластьПодвалСНДС = Макет.ПолучитьОбласть("ПодвалТаблицыВсего" + СуффиксОбластиПодвал);
		КонецЕсли;
			 
		ОбластьШапкаТаблицы.Параметры.Заполнить(СтруктураИмяДопКолонки);
		ОбластьСуммаПрописью = Макет.ПолучитьОбласть(?(ДанныеПечати.СчетКВозврату, "СуммаПрописьюКВозврату", "СуммаПрописью"));
		
		МассивПроверкиВывода = Новый Массив;
			
		Сумма = 0;
		СуммаНДС = 0;
		ВсегоСкидок = 0;
		ВсегоБезСкидок = 0;
		НомерСтроки = 0;
		СоответствиеСтавокНДС = Новый Соответствие;
		
		Для Каждого СтрокаТовары Из ТаблицаТовары Цикл
		
			НомерСтроки = НомерСтроки + 1;
			НомерСтрокиПечать = НомерСтроки;
				
			Если НомерСтроки = 0 И ВыводШапки <> 2 Тогда
				ВыводШапки = 1;
			КонецЕсли;
			
			СтрокаТовары.Сумма = типДенежнаяСумма.ПривестиЗначение(СтрокаТовары.Сумма);
			СтрокаТовары.СуммаНДС = типДенежнаяСумма.ПривестиЗначение(СтрокаТовары.СуммаНДС);
			СтрокаТовары.СуммаСкидки = типДенежнаяСумма.ПривестиЗначение(СтрокаТовары.СуммаСкидки);
			СтрокаТовары.СуммаБезСкидки = типДенежнаяСумма.ПривестиЗначение(СтрокаТовары.СуммаБезСкидки);
				
			ДополнительныеПараметрыПолученияНаименованияДляПечати = к2ПечатьКлиентСервер.ДополнительныеПараметрыПредставлениеНоменклатурыДляПечати();
	
			ДополнительныеПараметрыПолученияНаименованияДляПечати.ВозвратнаяТара = СтрокаТовары.ЭтоВозвратнаяТара;
				
			Товар = к2ПечатьКлиентСервер.ПредставлениеНоменклатурыДляПечати(
															СтрокаТовары.НаименованиеПолное,
															СтрокаТовары.Характеристика,
															,
															,
															ДополнительныеПараметрыПолученияНаименованияДляПечати);
						
			СтруктураДанныхСтроки = Новый Структура;
			СтруктураДанныхСтроки.Вставить("Товар", Товар);
			СтруктураДанныхСтроки.Вставить("НомерСтроки", НомерСтрокиПечать);
			ОбластьСтрокаТаблицы.Параметры.Заполнить(СтрокаТовары);
			Если ЕстьСкидки Тогда
				СтруктураДанныхСтроки.Вставить("СуммаСкидки", СтрокаТовары.СуммаСкидки);
			КонецЕсли;
				
			Если ЗначениеЗаполнено(СтруктураИмяДопКолонки.ИмяКолонкиКодов) Тогда
				СтруктураДанныхСтроки.Вставить("Артикул", СтрокаТовары[СтруктураИмяДопКолонки.ИмяКолонкиКодов]);
			КонецЕсли;
				
			ОбластьСтрокаТаблицы.Параметры.Заполнить(СтруктураДанныхСтроки);
								
			МассивПроверкиВывода.Очистить();
			МассивПроверкиВывода.Добавить(ОбластьСтрокаТаблицы);
			Если НомерСтроки = ТаблицаТовары.Количество() Тогда
				МассивПроверкиВывода.Добавить(ОбластьПодвалТаблицы);
				МассивПроверкиВывода.Добавить(ОбластьПодвалНДС);
				МассивПроверкиВывода.Добавить(ОбластьСуммаПрописью);
			КонецЕсли;
				
			Если ТабличныйДокумент.ПроверитьВывод(МассивПроверкиВывода) Тогда
				Если (НомерСтроки = 1 И ВыводШапки = 0) ИЛИ (НомерСтроки = 0 И ВыводШапки = 1) Тогда
					ВыводШапки = 2;
					ТабличныйДокумент.Вывести(ОбластьШапкаТаблицы);
				КонецЕсли;
			Иначе
				ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
				ТабличныйДокумент.Вывести(ОбластьШапкаТаблицы);
			КонецЕсли;
				
			ТабличныйДокумент.Вывести(ОбластьСтрокаТаблицы);
					
			Сумма = Сумма + СтрокаТовары.Сумма;
			СуммаНДС = СуммаНДС + СтрокаТовары.СуммаНДС;
					
			Если ЕстьСкидки Тогда
				ВсегоСкидок = ВсегоСкидок + СтрокаТовары.СуммаСкидки;
				ВсегоБезСкидок = ВсегоБезСкидок + СтрокаТовары.СуммаБезСкидки;
			КонецЕсли;
				
			Если ДанныеПечати.УчитыватьНДС И НЕ ТолькоЗалогЗаТару Тогда
				СуммаНДСПоСтавке = СоответствиеСтавокНДС[СтрокаТовары.СтавкаНДС];
				Если СуммаНДСПоСтавке = Неопределено Тогда
					СуммаНДСПоСтавке = 0;
				КонецЕсли;
				СоответствиеСтавокНДС.Вставить(СтрокаТовары.СтавкаНДС, СуммаНДСПоСтавке + СтрокаТовары.СуммаНДС);
			КонецЕсли;
				
		КонецЦикла;
			
		СтруктураДанныхВсегоСкидки = Новый Структура;
			
		// Подвал таблицы "Товары"
		Если ЕстьСкидки Тогда
			СтруктураДанныхВсегоСкидки.Вставить("ВсегоСкидок", ВсегоСкидок);
			СтруктураДанныхВсегоСкидки.Вставить("ВсегоБезСкидок", ВсегоБезСкидок);
			Если ДанныеПечати.УчитыватьНДС И НЕ ТолькоЗалогЗаТару Тогда
				СтруктураДанныхВсегоСкидки.Вставить("ВсегоСуммаНДС", СуммаНДС);
			КонецЕсли;
		КонецЕсли;
		СтруктураДанныхВсегоСкидки.Вставить("Всего", ФорматСумм(Сумма));
		ОбластьПодвалТаблицы.Параметры.Заполнить(СтруктураДанныхВсегоСкидки);
		ТабличныйДокумент.Вывести(ОбластьПодвалТаблицы);
		
		УпорядоченныеСтавкиНДС = Перечисления.СтавкиНДС.СтавкиНДСВПорядкеУбывания();
		
		// Область "ПодвалТаблицыНДС"
		Если ДанныеПечати.УчитыватьНДС И НЕ ТолькоЗалогЗаТару Тогда
				
			Для Каждого СтавкаНДС Из УпорядоченныеСтавкиНДС Цикл
				
				ТекСуммаНДС = СоответствиеСтавокНДС.Получить(СтавкаНДС);
				
				Если ТекСуммаНДС = Неопределено Тогда
					Продолжить;
				КонецЕсли;
				
				СтруктураДанныхПодвалНДС = Новый Структура;
				СтруктураДанныхПодвалНДС.Вставить("НДС", ТекстНДСПоСтавке(СтавкаНДС, ДанныеПечати.ЦенаВключаетНДС));
				СтруктураДанныхПодвалНДС.Вставить("ВсегоНДС", ФорматСумм(ТекСуммаНДС, , "-"));
				ОбластьПодвалНДС.Параметры.Заполнить(СтруктураДанныхПодвалНДС);
				ТабличныйДокумент.Вывести(ОбластьПодвалНДС);
			
			КонецЦикла;
			
			СтруктураДанныхПодвалВсегоСНДС = Новый Структура;
			СтруктураДанныхПодвалВсегоСНДС.Вставить("ВсегоСНДС", ФорматСумм(Сумма + ?(ДанныеПечати.ЦенаВключаетНДС, 0, СуммаНДС)));
			ОбластьПодвалСНДС.Параметры.Заполнить(СтруктураДанныхПодвалВсегоСНДС);
			ТабличныйДокумент.Вывести(ОбластьПодвалСНДС);
				
		Иначе
			СтруктураДанныхПодвалНДС = Новый Структура;
			СтруктураДанныхПодвалНДС.Вставить("НДС", НСтр("ru='Без налога (НДС)'"));
			СтруктураДанныхПодвалНДС.Вставить("ВсегоНДС", "-");
			ОбластьПодвалНДС.Параметры.Заполнить(СтруктураДанныхПодвалНДС);
			ТабличныйДокумент.Вывести(ОбластьПодвалНДС);
		КонецЕсли;
			
		// Вывести Сумму прописью
		СуммаКПрописи = Сумма + ?(ДанныеПечати.ЦенаВключаетНДС, 0, СуммаНДС);
		ИтоговаяСтрока = НСтр("ru='Всего наименований %Количество%, на сумму %Сумма%'");
		ИтоговаяСтрока = СтрЗаменить(ИтоговаяСтрока, "%Количество%", НомерСтроки);
		ИтоговаяСтрока = СтрЗаменить(ИтоговаяСтрока, "%Сумма%",      ФорматСумм(СуммаКПрописи, ДанныеПечати.Валюта));
			
		СтруктураДанныхСуммаПрописью = Новый Структура;
		Если ДанныеПечати.СчетКВозврату Тогда
				
			СуммаКВозврату = ДанныеПечати.СуммаКВозврату;
			СуммаИтого = СуммаКПрописи-СуммаКВозврату;
			Если СуммаИтого < 0 Тогда
				СуммаИтого = 0;
			КонецЕсли;
			СтруктураДанныхСуммаПрописью.Вставить("СуммаВозврата", ФорматСумм(СуммаКВозврату, ДанныеПечати.Валюта));
			СтруктураДанныхСуммаПрописью.Вставить("СуммаИтогоКОплате", ФорматСумм(СуммаИтого, ДанныеПечати.Валюта, "0"));
			СтруктураДанныхСуммаПрописью.Вставить("СуммаПрописью", РаботаСКурсамиВалют.СформироватьСуммуПрописью(СуммаИтого, ДанныеПечати.Валюта));
		Иначе
				
			СтруктураДанныхСуммаПрописью.Вставить("СуммаПрописью", РаботаСКурсамиВалют.СформироватьСуммуПрописью(СуммаКПрописи, ДанныеПечати.Валюта));
		КонецЕсли;
		
		СтруктураДанныхСуммаПрописью.Вставить("ИтоговаяСтрока", ИтоговаяСтрока);
		ОбластьСуммаПрописью.Параметры.Заполнить(СтруктураДанныхСуммаПрописью);
		ТабличныйДокумент.Вывести(ОбластьСуммаПрописью);
		ЗаполнитьРеквизитыПодвалаСчетаНаОплату(ДанныеПечати, Макет, ТабличныйДокумент, ТаблицаЭтапыОплаты, ПараметрыПечати);
			
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, ДанныеПечати.Ссылка);
	
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Функция ФорматСумм(Знач Сумма, Валюта = Неопределено, ЧН = "", ЧРГ = "")
	
	Сумма = ?(Сумма < 0, -Сумма, Сумма);
	ФорматнаяСтрока = "ЧДЦ=2"
					  + ?(НЕ ЗначениеЗаполнено(ЧН), "", ";" + "ЧН=" + ЧН)
					  + ?(НЕ ЗначениеЗаполнено(ЧРГ),"", ";" + "ЧРГ=" + ЧРГ);
	РезультирующаяСтрока = СокрЛ(Формат(Сумма, ФорматнаяСтрока));
	
	Если ЗначениеЗаполнено(Валюта) Тогда
		РезультирующаяСтрока = РезультирующаяСтрока + " " + СокрП(Валюта);
	КонецЕсли;

	Возврат РезультирующаяСтрока;

КонецФункции

Функция СформироватьТекстНДСЭтапаОплаты(СуммаНДС, ПроцентПлатежа)
	
	ТекстНДС = "";
			
	Если СуммаНДС <> 0 Тогда
				
		ТекстНДС = ТекстНДС + ?(ПустаяСтрока(ТекстНДС), НСтр("ru = 'НДС %СуммаНДС%'"), НСтр("ru = ', НДС %СуммаНДС%'"));
		ТекстНДС = СтрЗаменить(ТекстНДС, "%СуммаНДС%",  Формат(СуммаНДС / 100 * ПроцентПлатежа, "ЧДЦ=2"));
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ТекстНДС) Тогда
		ТекстНДС = НСтр("ru = 'В т.ч.'") + " " + ТекстНДС;
	Иначе
		ТекстНДС = НСтр("ru = 'Без налога (НДС)'");
	КонецЕсли;
	
	Возврат ТекстНДС;
	
КонецФункции

Функция ТекстНДСПоСтавке(СтавкаНДС, ЦенаВключаетНДС)
	
	ТекстНДСПоСтавке = ?(ЦенаВключаетНДС, НСтр("ru='В т.ч. НДС (%СтавкаНДС%):'"), НСтр("ru='НДС (%СтавкаНДС%):'"));
	ТекстНДСПоСтавке = СтрЗаменить(ТекстНДСПоСтавке, "%СтавкаНДС%", СтавкаНДС);
	
	Возврат ТекстНДСПоСтавке;
	
КонецФункции

Функция СуммаКОплатеПоСчету(ДанныеПечати, ТаблицаТовары)
		
	СуммаКОплатеПоСчету = 0;
	Для Каждого СтрокаТовары Из ТаблицаТовары Цикл
		СуммаКОплатеПоСчету = СуммаКОплатеПоСчету
		+ СтрокаТовары.Сумма + ?(ДанныеПечати.ЦенаВключаетНДС, 0, СтрокаТовары.СуммаНДС);
	КонецЦикла;
	
	Если ДанныеПечати.СчетКВозврату Тогда
		СуммаКОплатеПоСчету = СуммаКОплатеПоСчету - ДанныеПечати.СуммаКВозврату;
		Если СуммаКОплатеПоСчету < 0 Тогда
			СуммаКОплатеПоСчету = 0;
		КонецЕсли;
	КонецЕсли;
	
	Возврат СуммаКОплатеПоСчету;
	
КонецФункции

Процедура ВывестиQRКод(РеквизитыПлатежа, ДанныеПечати, ОбластьМакета)
	
	QRСтрока = УправлениеПечатьюРФ.ФорматнаяСтрокаУФЭБС(РеквизитыПлатежа);
	
	Если Не ПустаяСтрока(QRСтрока) Тогда
		
		ДанныеQRКода = УправлениеПечатью.ДанныеQRКода(QRСтрока, 0, 190);
		
		Если ТипЗнч(ДанныеQRКода) = Тип("ДвоичныеДанные") Тогда
			КартинкаQRКода = Новый Картинка(ДанныеQRКода);
			ОбластьМакета.Рисунки.QRКод.Картинка = КартинкаQRКода;
		Иначе
			Шаблон = НСтр("ru = 'Не удалось сформировать QR-код для документа %1.
				|Технические подробности см. в журнале регистрации.'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(Шаблон, ДанныеПечати.Ссылка);
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Функция ОписаниеОрганизации(ДанныеПечати, ИмяОрганизации)
	
	Сведения = к2Печать.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации]);
	РеквизитыОрганизации = "ПолноеНаименование,ИНН,КПП,ФактическийАдрес,Телефоны,";
	
	Возврат к2Печать.ОписаниеОрганизации(Сведения, РеквизитыОрганизации);
	
КонецФункции

Функция НовыеПараметрыСтрокиЭтапа()
	
	СтруктураСтрокиЭтапа = Новый Структура;
	СтруктураСтрокиЭтапа.Вставить("НомерСтроки", 0);
	СтруктураСтрокиЭтапа.Вставить("ДатаПлатежа", '00010101');
	СтруктураСтрокиЭтапа.Вставить("ПроцентПлатежа", 0);
	СтруктураСтрокиЭтапа.Вставить("СуммаПлатежа", 0);
	СтруктураСтрокиЭтапа.Вставить("ТекстНДС", "");
	СтруктураСтрокиЭтапа.Вставить("ЭтоЗалогЗаТару", Ложь);
	
	Возврат СтруктураСтрокиЭтапа;
	
КонецФункции

Функция РеквизитыПлатежаQRКод(СтруктураДанныхШапки)
	
	РеквизитыПлатежа = Новый Структура;
	РеквизитыПлатежа.Вставить("ТекстПолучателя", СтруктураДанныхШапки.ПредставлениеПоставщикаДляПлатПоручения);
	РеквизитыПлатежа.Вставить("НомерСчетаПолучателя", СтруктураДанныхШапки.СчетПолучателяПредставление);
	РеквизитыПлатежа.Вставить("НаименованиеБанкаПолучателя", СтруктураДанныхШапки.БанкПолучателяПредставление);
	РеквизитыПлатежа.Вставить("БИКБанкаПолучателя", СтруктураДанныхШапки.БИКБанкаПолучателя);
	РеквизитыПлатежа.Вставить("СчетБанкаПолучателя", СтруктураДанныхШапки.СчетБанкаПолучателяПредставление);
	РеквизитыПлатежа.Вставить("СуммаЧислом");
	РеквизитыПлатежа.Вставить("НазначениеПлатежа", СтруктураДанныхШапки.НазначениеПлатежа);
	РеквизитыПлатежа.Вставить("ИННПолучателя", СтруктураДанныхШапки.ИНН);
	РеквизитыПлатежа.Вставить("КПППолучателя", СтруктураДанныхШапки.КПП);
	
	Возврат РеквизитыПлатежа;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли