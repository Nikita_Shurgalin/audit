
#Область ПрограммныйИнтерфейс

// Возвращает структуру, содержащую информацию об ответственном лице организации.
//
// Параметры:
//	Организация - СправочникСсылка.Организации - отбор по организации
//	Дата - Дата - отбор по дате
//  ОтветственноеЛицо - Перечисления.к2ОтветственныеЛицаОрганизаций - "вид" ответственного лица
//	Подразделение - СправочникСсылка.Подразделения - отбор по подразделению
//
// Возвращаемое значение:
//  Структура с ключами
//	- СтруктурнаяЕдиница
//	- Подразделение
//	- ОтветственноеЛицо
//	- ФизическоеЛицо
//	- Должность
//
Функция ПолучитьДанныеОтветственногоЛица(Организация, Дата = Неопределено, ОтветственноеЛицо = Неопределено, Подразделение = Неопределено) Экспорт
	
	Если НЕ ЗначениеЗаполнено(ОтветственноеЛицо) Тогда
		ОтветственноеЛицо = Перечисления.к2ОтветственныеЛицаОрганизаций.Руководитель;
	КонецЕсли;
	
	Если Подразделение = Неопределено Тогда
		Подразделение = Справочники.Подразделения.ПустаяСсылка();
	КонецЕсли;
	
	СтруктураОтветственных = ПолучитьОтветственныеЛицаОрганизации(Организация, Дата, Подразделение);
	Результат 			   = Новый Структура("СтруктурнаяЕдиница, ОтветственноеЛицо, Подразделение", Организация, ОтветственноеЛицо, Подразделение);
	
	Для Каждого МетаВидОтветственного Из Метаданные.Перечисления.к2ОтветственныеЛицаОрганизаций.ЗначенияПеречисления Цикл
		
		Ключ = МетаВидОтветственного.Имя;
		Если ОтветственноеЛицо = Перечисления.к2ОтветственныеЛицаОрганизаций[Ключ] Тогда
			Результат.Вставить("ФизическоеЛицо", СтруктураОтветственных[Ключ]);
			Результат.Вставить("Должность", 	 СтруктураОтветственных[Ключ + "Должность"]);
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

// Возвращает структуру, содержащую информацию об ответственных лицах организации.
//
// Параметры:
//	Организация - СправочникСсылка.Организации - отбор по организации
//	Дата - Дата - отбор по дате
//	Подразделение - СправочникСсылка.Подразделения - отбор по подразделению
//
// Возвращаемое значение:
// Структура с ключами
//	- %ВидОтветственного% - СправочникСсылка.к2ФизическиеЛица - физическое лицо, соответствующее ответственному лицу
//	- %ВидОтветственного%Должность - Строка - должность ответственного лица
//
Функция ПолучитьОтветственныеЛицаОрганизации(Организация, Дата = Неопределено, Подразделение = Неопределено) Экспорт
	
	Если Дата = Неопределено Тогда
		Дата = ТекущаяДатаСеанса();
	КонецЕсли;
	
	Если Подразделение = Неопределено Тогда
		Подразделение = Справочники.Подразделения.ПустаяСсылка();
	КонецЕсли;
	
	СтруктураОтветственных = Новый Структура;
	
	СтруктураОтбора 	 = Новый Структура("Дата, Владелец, Подразделение", Дата, Организация, Подразделение);
	ТаблицаОтветственных = ПолучитьТаблицуОтветственныхЛицПоОтбору(СтруктураОтбора);
	ТаблицаОтветственных.Индексы.Добавить("ОтветственноеЛицо, ПравоПодписиПоДоверенности");
	
	Для Каждого МетаВидОтветственного Из Метаданные.Перечисления.к2ОтветственныеЛицаОрганизаций.ЗначенияПеречисления Цикл
		
		Ключ 			  = МетаВидОтветственного.Имя;
		ВидОтветственного = Перечисления.к2ОтветственныеЛицаОрганизаций[Ключ];
		СтрокаТаблицы 	  = Неопределено; // ответственный пока не определен
		
		// Попытаемся найти подходящее ответственное лицо
		СтруктураОтбора = Новый Структура("ОтветственноеЛицо", ВидОтветственного);
		СтрокиТаблицы = ТаблицаОтветственных.НайтиСтроки(СтруктураОтбора);
		
		// Если Количество строк = 2 Ответственный присутствует как для подразделения, так и для организации
		
		Если СтрокиТаблицы.Количество() = 1
			Или СтрокиТаблицы.Количество() = 2 Тогда
			
			// Если есть только один ответственный данного вида - вернем его
			СтрокаТаблицы = СтрокиТаблицы[0];
				
		ИначеЕсли СтрокиТаблицы.Количество() > 2 Тогда
			
			СтруктураОтбора.Вставить("ПравоПодписиПоДоверенности", Ложь);
			
			СтрокиТаблицы = ТаблицаОтветственных.НайтиСтроки(СтруктураОтбора);
			Если СтрокиТаблицы.Количество() = 1
				Или СтрокиТаблицы.Количество() = 2 Тогда
				
				// Если есть основной ответственный данного вида - вернем его
				СтрокаТаблицы = СтрокиТаблицы[0];
				
			КонецЕсли;
			
		КонецЕсли;
		
		Если СтрокаТаблицы = Неопределено Тогда
			
			// Ответственное лицо этого вида не удалось определить
			СтруктураОтветственных.Вставить(Ключ, 				         Справочники.к2ФизическиеЛица.ПустаяСсылка());
			СтруктураОтветственных.Вставить(Ключ + "Должность",          "");
			СтруктураОтветственных.Вставить(Ключ + "Наименование",       "");
			СтруктураОтветственных.Вставить(Ключ + "Ссылка", 	         Справочники.к2ОтветственныеЛицаОрганизаций.ПустаяСсылка());
		Иначе
			
			// Основное ответственное лицо этого вида найдено
			СтруктураОтветственных.Вставить(Ключ, 				         СтрокаТаблицы.ФизическоеЛицо);
			СтруктураОтветственных.Вставить(Ключ + "Должность",          СтрокаТаблицы.Должность);
			СтруктураОтветственных.Вставить(Ключ + "Наименование",       СтрокаТаблицы.Наименование);
			СтруктураОтветственных.Вставить(Ключ + "Ссылка", 	         СтрокаТаблицы.Ссылка);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат СтруктураОтветственных;
	
КонецФункции

// Возвращает таблицу ответственных лиц, сформированную в соответствии с произвольным отбором.
//
// Параметры:
//  Отбор - Структура - структура, содержащая параметры отбора данных справочника ОтветственныеЛицаОрганизаций.
//		Ключ - имя реквизита справочника. Возможны дополнительные ключи:
//			- Дата 		  - для отбора по периоду ДатаНачала - ДатаОкончания
//			- Организация - синоним ключа Владелец
//		Значение - значение для отбора по ключу
//	ДопустимыПомеченныеНаУдаление - Булево - выводить в результирующую таблицу помеченные на удаление элементы.
//	СтрокаПоиска - Строка - строка поиска.
//	
// Возвращаемое значение:
//  Таблица значений - таблица, содержащая данные справочника ОтветственныеЛицаОрганизаций.
//		Имена колонок таблицы совпадают с именами реквизитов справочника.
//
Функция ПолучитьТаблицуОтветственныхЛицПоОтбору(Знач Отбор, ДопустимыПомеченныеНаУдаление = Ложь, СтрокаПоиска = "") Экспорт
	
	Запрос = Новый Запрос;
	
	ТекстВыбираемыеПоля = "";
	ТекстОтбора = "";
	НомерОтбора = 0;
	
	ПоляОтбора   = Новый Структура;
	МетаданныеСправочника = Метаданные.Справочники.к2ОтветственныеЛицаОрганизаций;
	
	Для Каждого Реквизит Из МетаданныеСправочника.СтандартныеРеквизиты Цикл
		ПоляОтбора.Вставить(Реквизит.Имя, Реквизит.Синоним);
	КонецЦикла;
	
	Для Каждого Реквизит Из МетаданныеСправочника.Реквизиты Цикл
		ПоляОтбора.Вставить(Реквизит.Имя, Реквизит.Синоним);
	КонецЦикла;
	
	Если Отбор.Свойство("Дата") Тогда
		
		// Отбор по периоду действия записи об ответственном лице
		ТекстОтбора =
			ТекстОтбора
			+ ?(ТекстОтбора = "", "", " И ") + "ОтветственныеЛицаОрганизаций.ДатаНачала <= &ОтборПоДате
			| И (ОтветственныеЛицаОрганизаций.ДатаОкончания >= &ОтборПоДате
			|	 ИЛИ ОтветственныеЛицаОрганизаций.ДатаОкончания = ДАТАВРЕМЯ(1,1,1,0,0,0))
			|";
		Запрос.УстановитьПараметр("ОтборПоДате", НачалоДня(Отбор.Дата));
		
		Отбор.Удалить("Дата");
		
	КонецЕсли;
	
	Если Отбор.Свойство("Организация") Тогда
		Отбор.Вставить("Владелец", Отбор.Организация);
		Отбор.Удалить("Организация");
	КонецЕсли;
	
	Если Отбор.Свойство("Подразделение") Тогда
		
		// Отбор по подразделению и общий по организации
		ТекстОтбора =
			ТекстОтбора
			+ ?(ТекстОтбора = "", "", " И ") + "(ОтветственныеЛицаОрганизаций.Подразделение В (&ОтборПоПодразделению)
			|	 ИЛИ ОтветственныеЛицаОрганизаций.Подразделение = Значение(Справочник.Подразделения.ПустаяСсылка))
			|";
			
		ТипОтбораПодразделения = ТипЗнч(Отбор.Подразделение);
			
		Если ТипОтбораПодразделения = Тип("Массив") Тогда
			ОтборПоПодразделению = Отбор.Подразделение;
		ИначеЕсли ТипОтбораПодразделения = Тип("СправочникСсылка.Подразделения") Тогда
			ОтборПоПодразделению = Новый Массив;
			ОтборПоПодразделению.Добавить(Отбор.Подразделение);
		Иначе
			ОтборПоПодразделению = Новый Массив;
			ОтборПоПодразделению.Добавить(Справочники.Подразделения.ПустаяСсылка());
		КонецЕсли;
		
		Запрос.УстановитьПараметр("ОтборПоПодразделению", ОтборПоПодразделению);
		
		Отбор.Удалить("Подразделение");
		
	КонецЕсли;
	
	Если НЕ ДопустимыПомеченныеНаУдаление Тогда
		Отбор.Вставить("ПометкаУдаления", Ложь);
	КонецЕсли;
	
	// Формируем секцию запроса "ГДЕ", устанавливаем параметры запроса
	Для Каждого ТекущийОтбор Из Отбор Цикл
		
		Если НЕ ПоляОтбора.Свойство(ТекущийОтбор.Ключ) Тогда
			Продолжить;
		КонецЕсли;
		
		НомерОтбора = НомерОтбора + 1;
		
		ТекстОтбора =
			ТекстОтбора
			+ ?(ТекстОтбора = "", "", " И ") + "ОтветственныеЛицаОрганизаций." + ТекущийОтбор.Ключ
				+ ?(ТипЗнч(ТекущийОтбор.Значение) = Тип("Массив"), " В ", " = ") + "(&ЗначениеОтбора" + СокрЛП(НомерОтбора) + ")
			|";
		
		Запрос.УстановитьПараметр("ЗначениеОтбора" + СокрЛП(НомерОтбора), ТекущийОтбор.Значение);
		
	КонецЦикла;
	
	Если ЗначениеЗаполнено(СтрокаПоиска) Тогда
		
		ТекстОтбора =
			ТекстОтбора
			+ ?(ТекстОтбора = "", "", " И ") + "ОтветственныеЛицаОрганизаций.Наименование ПОДОБНО &ОтборПоПредставлению
			|";
		Запрос.УстановитьПараметр("ОтборПоПредставлению", СтрокаПоиска + "%");
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ТекстОтбора) Тогда
		ТекстОтбора =
			"ГДЕ
			| " + ТекстОтбора;
	КонецЕсли;
	
	Для Каждого Поле Из ПоляОтбора Цикл
		ТекстВыбираемыеПоля = ТекстВыбираемыеПоля
			+ ?(ТекстВыбираемыеПоля = "", "", ",") + "
			|ОтветственныеЛицаОрганизаций." + Поле.Ключ + " КАК " + Поле.Ключ;
	КонецЦикла;
	
	// Формируем запрос, получаем данные
	ТекстЗапроса =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	%ТекстВыбираемыеПоля%
	|ИЗ
	|	Справочник.к2ОтветственныеЛицаОрганизаций КАК ОтветственныеЛицаОрганизаций
	|%ТекстОтбора%
	|УПОРЯДОЧИТЬ ПО
	|	Владелец,
	|	Подразделение УБЫВ,
	|	ОтветственноеЛицо,
	|	ПравоПодписиПоДоверенности,
	|	ДатаНачала,
	|	ДатаОкончания,
	|	ФизическоеЛицо";
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "%ТекстВыбираемыеПоля%", ТекстВыбираемыеПоля);
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "%ТекстОтбора%", 		  ТекстОтбора);
	Запрос.Текст = ТекстЗапроса;
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

// Обработчик события "ПриИзменении" ключевых реквизитов документа (Дата, Организация, Подразделение).
// Проверяет соответствие ответственных лиц документа и ключевых реквизитов.
//
// Параметры:
//  Объект - ДанныеФормыСтруктура - свойство "Объект" формы документа.
//
Процедура ЗаполнитьРеквизитыВРеализацииТоваров( Объект ) Экспорт
	
	Если Не ЗначениеЗаполнено( Объект.Организация ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Объект.Дата ) Тогда
		
		текДата = Объект.Дата;
		
	Иначе
		
		текДата = ТекущаяДатаСеанса();
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Объект.Подразделение ) Тогда
		
		Подразделение = Объект.Подразделение;
		
	Иначе
		
		Подразделение = Справочники.Подразделения.ПустаяСсылка();
		
	КонецЕсли;
	
	структОтбора = Новый Структура("Дата, Владелец, Подразделение", текДата, Объект.Организация, Подразделение);
	
	доступныеОтветственныеЛица = ПолучитьТаблицуОтветственныхЛицПоОтбору(структОтбора);
	
	Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(Объект, "Руководитель") Тогда
	
		структПоиска = Новый Структура;
		структПоиска.Вставить( "ОтветственноеЛицо", Перечисления.к2ОтветственныеЛицаОрганизаций.Руководитель );
			
		найденныеСтроки = доступныеОтветственныеЛица.НайтиСтроки( структПоиска );
			
		Если найденныеСтроки.Количество() > 0 Тогда
				
			Объект.Руководитель = найденныеСтроки[0].Ссылка;
				
		КонецЕсли;
		
	КонецЕсли;
	
	Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(Объект, "ГлавныйБухгалтер") Тогда
	
		структПоиска = Новый Структура;
		структПоиска.Вставить( "ОтветственноеЛицо", Перечисления.к2ОтветственныеЛицаОрганизаций.ГлавныйБухгалтер );
			
		найденныеСтроки = доступныеОтветственныеЛица.НайтиСтроки( структПоиска );
			
		Если найденныеСтроки.Количество() > 0 Тогда
				
			Объект.ГлавныйБухгалтер = найденныеСтроки[0].Ссылка;
				
		КонецЕсли;
		
	КонецЕсли;
	
	Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(Объект, "Кассир")
		И ТипЗнч(Объект.Кассир) = Тип("СправочникСсылка.к2ОтветственныеЛицаОрганизаций") Тогда
	
		структПоиска = Новый Структура;
		структПоиска.Вставить( "ОтветственноеЛицо", Перечисления.к2ОтветственныеЛицаОрганизаций.Кассир );
			
		найденныеСтроки = доступныеОтветственныеЛица.НайтиСтроки( структПоиска );
			
		Если найденныеСтроки.Количество() > 0 Тогда
				
			Объект.Кассир = найденныеСтроки[0].Ссылка;
				
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#Область УстаревшиеПроцедурыИФункции

// Устарела. Ответственные лица определяются на документах в специальных реквизитах (Руководитель, ГлавныйБухгалтер)
// Заполнение новых реквизитов происходит автоматически с помощью ЗаполнитьРеквизитыВРеализацииТоваров()
//
// Формирует временную таблицу, содержащую информацию об ответственных лицах в разрезе документов.
// Используется для формирования печатных форм документов подсистемой печати
//	- перед выборкой данных из шапки документа вызвать эту процедуру
//	- при выборке данных создать левое соединение таблицы документов с временной таблицей ТаблицаОтветственныеЛица по полю Ссылка.
//
// Параметры:
//	ОтборДокументы - Массив, ДокументСсылка - ссылки на документы, по которым необходимо получить ответственных лиц.
//		Важно: если передан массив, то все его элементы должны иметь одинаковый тип.
//	МенеджерВременныхТаблиц - МенеджерВременныхТаблиц - в него помещается сформированная временная таблица ТаблицаОтветственныеЛица
//  ИмяРеквизитаОрганизация - имя поля документа для получения организации-владельца ответственного лица
//	РеквизитыОтветственныеЛица - Структура, Соответствие
//		Ключ - Строка - имя поля в запросе для получения значения ответственного лица
//		Значение - ПеречислениеСсылка.к2ОтветственныеЛицаОрганизаций - "вид" ответственного лица
//	ИмяТаблицыОтветственныеЛица - позволяет задать имя для формируемой временной таблицы.
// Количество строк в таблице "ТаблицаОтветственныеЛица" соответствует количеству элементов в ОтборДокументы.
//
// Структура временной таблицы "ТаблицаОтветственныеЛица"
//	- Ссылка - ссылка на документ из ОтборДокументы
//	- %Реквизит% - СправочникСсылка.к2ОтветственныеЛицаОрганизаций - значение реквизита ответственного лица из документа
//	- %Реквизит%ФизическоеЛицо - СправочникСсылка.к2ФизическиеЛица - физическое лицо, соответствующее ответственному лицу
//	- %Реквизит%Наименование - Строка - наименование для печати ответственного лица
//	- %Реквизит%Должность - Строка - должность ответственного лица
//	- %Реквизит%ОснованиеПраваПодписи - Строка - основание права подписи для неосновного ответственного лица
// Для полей, начинающихся с %Реквизит%
// 	- вместо %Реквизит% подставляется имя реквизита ответственного лица как оно задано в конфигураторе
//	- количество каждого из этих полей соответствует количеству реквизитов ответственных лиц документа.
//
// Например, если в метаданных документа есть два реквизита ответственных лиц - Руководитель и Бухгалтер,
// то таблица ТаблицаОтветственныеЛица будет иметь следующую структуру:
//	- Ссылка
//	- Руководитель
//	- РуководительФизическоеЛицо
//	- РуководительНаименование
//	- РуководительДолжность
//	- РуководительОснованиеПраваПодписи
//	- Бухгалтер
//	- БухгалтерФизическоеЛицо
//	- БухгалтерНаименование
//	- БухгалтерДолжность
//	- БухгалтерОснованиеПраваПодписи.
//
Процедура СформироватьВременнуюТаблицуОтветственныхЛицДокументов(ОтборДокументы, МенеджерВременныхТаблиц,
			ИмяРеквизитаОрганизация = "Организация", РеквизитыОтветственныеЛица = Неопределено,
			ИмяТаблицыОтветственныеЛица = "ТаблицаОтветственныеЛица") Экспорт
	
	Если (ТипЗнч(ОтборДокументы) = Тип("Массив") ИЛИ ТипЗнч(ОтборДокументы) = Тип("ФиксированныйМассив"))
	 И ОтборДокументы.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(ОтборДокументы) <> Тип("Массив") И ТипЗнч(ОтборДокументы) <> Тип("ФиксированныйМассив") Тогда // это ссылка на документ
		МассивОбъектов = Новый Массив;
		МассивОбъектов.Добавить(ОтборДокументы);
	Иначе // это массив ссылок на документы
		МассивОбъектов = ОтборДокументы;
	КонецЕсли;
	
	МетаданныеДокумента = МассивОбъектов[0].Метаданные();
	
	ИменаПолейПереданыВПараметрах = (РеквизитыОтветственныеЛица <> Неопределено);
	Если НЕ ИменаПолейПереданыВПараметрах Тогда
		РеквизитыОтветственныеЛица = к2ОтветственныеЛицаПовтИсп.РеквизитыОтветственныхЛицДокумента(МетаданныеДокумента.Имя);
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(РеквизитыОтветственныеЛица) Тогда
		Возврат;
	КонецЕсли;
	
	ИмяРеквизитаОрганизация = "ДокументДляПечати." + ИмяРеквизитаОрганизация;
	
	Если МенеджерВременныхТаблиц = Неопределено Тогда
		МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	// Получим данные из документов
	ШаблонЗапроса =
	"ВЫБРАТЬ
	|	ДокументДляПечати.Ссылка 					КАК Ссылка,
	|	НАЧАЛОПЕРИОДА(ДокументДляПечати.Дата, ДЕНЬ) КАК Дата,
	|	%Организация%								КАК Организация,
	|	%ИмяРеквизита% 								КАК ВыбранноеОтветственноеЛицо,
	|	ЗНАЧЕНИЕ(%ОтветственноеЛицо%) 				КАК ОтветственноеЛицо
	|%СозданиеВременнойТаблицы%
	|ИЗ
	|	Документ." + МетаданныеДокумента.Имя + " КАК ДокументДляПечати
	|ГДЕ
	|	ДокументДляПечати.Ссылка В(&МассивОбъектов)";
	
	Индекс = 0;
	Для Каждого КлючИЗначение Из РеквизитыОтветственныеЛица Цикл
		
		Индекс 		 = Индекс + 1;
		ТекстЗапроса = ШаблонЗапроса;
		
		Если Индекс > 1 Тогда
			ТекстЗапроса = "
			|ОБЪЕДИНИТЬ ВСЕ
			|" + ТекстЗапроса;
		КонецЕсли;
		
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса , "%Организация%", 			 	ИмяРеквизитаОрганизация);
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса , "%ИмяРеквизита%",
			?(ИменаПолейПереданыВПараметрах,
				"ЗНАЧЕНИЕ(Справочник.к2ОтветственныеЛицаОрганизаций.ПустаяСсылка)", // будут выбраны значения по умолчанию
				"ДокументДляПечати." + КлючИЗначение.Ключ));
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса , "%ОтветственноеЛицо%", 		ПолучитьПолноеИмяПредопределенногоЗначения(КлючИЗначение.Значение));
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса , "%СозданиеВременнойТаблицы%", ?(Индекс = 1, "ПОМЕСТИТЬ ВТДанныеДокументов", ""));
		
		Запрос.Текст = Запрос.Текст + ТекстЗапроса;
		
	КонецЦикла;
	
	Запрос.Текст = Запрос.Текст + "
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Организация,
	|	ОтветственноеЛицо,
	|	Дата
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	// Заполним пустые поля ответственных лиц значениями по умолчанию
	Запрос.Текст = Запрос.Текст +
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТДанныеДокументов.Организация КАК Организация,
	|	ВТДанныеДокументов.ОтветственноеЛицо КАК ОтветственноеЛицо,
	|	ВТДанныеДокументов.Дата КАК Дата
	|ПОМЕСТИТЬ ВТНезаполненныеОтветственные
	|ИЗ
	|	ВТДанныеДокументов КАК ВТДанныеДокументов
	|ГДЕ
	|	ВТДанныеДокументов.ВыбранноеОтветственноеЛицо = ЗНАЧЕНИЕ(Справочник.к2ОтветственныеЛицаОрганизаций.ПустаяСсылка)
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Организация,
	|	ОтветственноеЛицо,
	|	Дата
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТНезаполненныеОтветственные.Организация КАК Организация,
	|	ВТНезаполненныеОтветственные.ОтветственноеЛицо КАК ОтветственноеЛицо,
	|	ВТНезаполненныеОтветственные.Дата КАК Дата,
	|	ОтветственныеЛицаОрганизаций.Ссылка КАК Ответственный,
	|	ОтветственныеЛицаОрганизаций.ПравоПодписиПоДоверенности КАК ПравоПодписиПоДоверенности
	|ПОМЕСТИТЬ ВТКандидатыВОтветственныеПоУмолчанию
	|ИЗ
	|	ВТНезаполненныеОтветственные КАК ВТНезаполненныеОтветственные
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.к2ОтветственныеЛицаОрганизаций КАК ОтветственныеЛицаОрганизаций
	|		ПО ВТНезаполненныеОтветственные.Организация = ОтветственныеЛицаОрганизаций.Владелец
	|			И ВТНезаполненныеОтветственные.ОтветственноеЛицо = ОтветственныеЛицаОрганизаций.ОтветственноеЛицо
	|			И ВТНезаполненныеОтветственные.Дата >= ОтветственныеЛицаОрганизаций.ДатаНачала
	|			И (ВТНезаполненныеОтветственные.Дата <= ОтветственныеЛицаОрганизаций.ДатаОкончания
	|				ИЛИ ОтветственныеЛицаОрганизаций.ДатаОкончания = ДАТАВРЕМЯ(1, 1, 1))
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТКандидатыВОтветственныеПоУмолчанию.Организация КАК Организация,
	|	ВТКандидатыВОтветственныеПоУмолчанию.ОтветственноеЛицо КАК ОтветственноеЛицо,
	|	ВТКандидатыВОтветственныеПоУмолчанию.Дата КАК Дата,
	|	ВТКандидатыВОтветственныеПоУмолчанию.ОтветственноеЛицоПоУмолчанию КАК ОтветственноеЛицоПоУмолчанию
	|ПОМЕСТИТЬ ВТОтветственныеПоУмолчанию
	|ИЗ
	|	(ВЫБРАТЬ
	|		ВТКандидатыВОтветственныеПоУмолчанию.Организация КАК Организация,
	|		ВТКандидатыВОтветственныеПоУмолчанию.ОтветственноеЛицо КАК ОтветственноеЛицо,
	|		ВТКандидатыВОтветственныеПоУмолчанию.Дата КАК Дата,
	|		ВТКандидатыВОтветственныеПоУмолчанию.Ответственный КАК ОтветственноеЛицоПоУмолчанию
	|	ИЗ
	|		ВТКандидатыВОтветственныеПоУмолчанию КАК ВТКандидатыВОтветственныеПоУмолчанию
	|	ГДЕ
	|		НЕ ВТКандидатыВОтветственныеПоУмолчанию.ПравоПодписиПоДоверенности
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ
	|		ВТКандидатыВОтветственныеПоУмолчанию.Организация,
	|		ВТКандидатыВОтветственныеПоУмолчанию.ОтветственноеЛицо,
	|		ВТКандидатыВОтветственныеПоУмолчанию.Дата,
	|		МИНИМУМ(ВТКандидатыВОтветственныеПоУмолчанию.Ответственный)
	|	ИЗ
	|		ВТКандидатыВОтветственныеПоУмолчанию КАК ВТКандидатыВОтветственныеПоУмолчанию
	|
	|	СГРУППИРОВАТЬ ПО
	|		ВТКандидатыВОтветственныеПоУмолчанию.Организация,
	|		ВТКандидатыВОтветственныеПоУмолчанию.ОтветственноеЛицо,
	|		ВТКандидатыВОтветственныеПоУмолчанию.Дата
	|
	|	ИМЕЮЩИЕ
	|		КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ВТКандидатыВОтветственныеПоУмолчанию.Ответственный) = 1) КАК ВТКандидатыВОтветственныеПоУмолчанию
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ВТКандидатыВОтветственныеПоУмолчанию.Организация,
	|	ВТКандидатыВОтветственныеПоУмолчанию.ОтветственноеЛицо,
	|	ВТКандидатыВОтветственныеПоУмолчанию.Дата
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТДанныеДокументов.Ссылка КАК Ссылка,
	|	ВТДанныеДокументов.Дата КАК Дата,
	|	ВТДанныеДокументов.ОтветственноеЛицо КАК ОтветственноеЛицо,
	|	ВЫБОР
	|		КОГДА ВТДанныеДокументов.ВыбранноеОтветственноеЛицо = ЗНАЧЕНИЕ(Справочник.к2ОтветственныеЛицаОрганизаций.ПустаяСсылка)
	|			ТОГДА ЕСТЬNULL(ВТОтветственныеПоУмолчанию.ОтветственноеЛицоПоУмолчанию, ЗНАЧЕНИЕ(Справочник.к2ОтветственныеЛицаОрганизаций.ПустаяСсылка))
	|		ИНАЧЕ ВТДанныеДокументов.ВыбранноеОтветственноеЛицо
	|	КОНЕЦ КАК ОтветственноеЛицоПоУмолчанию
	|ПОМЕСТИТЬ ВТДокументыСОтветственными
	|ИЗ
	|	ВТДанныеДокументов КАК ВТДанныеДокументов
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТОтветственныеПоУмолчанию КАК ВТОтветственныеПоУмолчанию
	|		ПО ВТДанныеДокументов.Организация = ВТОтветственныеПоУмолчанию.Организация
	|			И ВТДанныеДокументов.ОтветственноеЛицо = ВТОтветственныеПоУмолчанию.ОтветственноеЛицо
	|			И ВТДанныеДокументов.Дата = ВТОтветственныеПоУмолчанию.Дата
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо КАК ФизическоеЛицо,
	|	ВТДокументыСОтветственными.Ссылка КАК Ссылка,
	|	ВЫБОР
	|		КОГДА ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо.ФамилияИнициалы = """"
	|			ТОГДА ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо.Наименование
	|		ИНАЧЕ ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо.ФамилияИнициалы
	|	КОНЕЦ КАК ФИО
	|ПОМЕСТИТЬ ВТФИОФизЛиц
	|ИЗ
	|	ВТДокументыСОтветственными КАК ВТДокументыСОтветственными
	|ГДЕ
	|	ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо <> ЗНАЧЕНИЕ(Справочник.к2ФизическиеЛица.ПустаяСсылка)
	|	И ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо ЕСТЬ НЕ NULL
	|
	|СГРУППИРОВАТЬ ПО
	|	ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо,
	|	ВТДокументыСОтветственными.Ссылка,
	|	ВЫБОР
	|		КОГДА ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо.ФамилияИнициалы = """"
	|			ТОГДА ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо.Наименование
	|		ИНАЧЕ ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо.ФамилияИнициалы
	|	КОНЕЦ
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТДокументыСОтветственными.Ссылка КАК Ссылка,
	|	ВТДокументыСОтветственными.ОтветственноеЛицо КАК ОтветственноеЛицо,
	|	ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию КАК ОтветственноеЛицоПоУмолчанию,
	|	ЕСТЬNULL(ВТФИОФизЛиц.ФИО, """") КАК ФИО
	|ПОМЕСТИТЬ ВТДокументыСФИООтветственных
	|ИЗ
	|	ВТДокументыСОтветственными КАК ВТДокументыСОтветственными
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТФИОФизЛиц КАК ВТФИОФизЛиц
	|		ПО ВТДокументыСОтветственными.ОтветственноеЛицоПоУмолчанию.ФизическоеЛицо = ВТФИОФизЛиц.ФизическоеЛицо
	|			И ВТДокументыСОтветственными.Ссылка = ВТФИОФизЛиц.Ссылка";
	
	Запрос.Текст = Запрос.Текст + "
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	// Свернем результат - поместим всех ответственных в одну запись выборки запроса
	ШаблонЗапроса = "
	|ВЫБРАТЬ
	|	ВТДокументыСФИООтветственных.Ссылка%СтрокаВыборкаЗапроса%
	|ПОМЕСТИТЬ ВТДокументыСФИООтветственныхСвернутая
	|ИЗ
	|(%ВложенныйЗапрос%) КАК ВТДокументыСФИООтветственных
	|
	|СГРУППИРОВАТЬ ПО
	|	ВТДокументыСФИООтветственных.Ссылка
	|";
	
	ШаблонВложенногоЗапроса = "
	|ВЫБРАТЬ
	|	ВТДокументыСФИООтветственных.Ссылка%СтрокаВыборкаВложенногоЗапроса%
	|ИЗ
	|	ВТДокументыСФИООтветственных КАК ВТДокументыСФИООтветственных
	|ГДЕ
	|	ВТДокументыСФИООтветственных.ОтветственноеЛицо = ЗНАЧЕНИЕ(%ОтветственноеЛицо%)
	|";
	
	Индекс = 0;
	СтрокаВыборкаЗапроса = "";
	ТекстВложенногоЗапроса = "";
	
	Для Каждого КлючИЗначение Из РеквизитыОтветственныеЛица Цикл
		
		Индекс = Индекс + 1;
		
		СтрокаВыборкаВложенногоЗапроса = "";
		Для Каждого КлючИЗначение2 Из РеквизитыОтветственныеЛица Цикл
			
			Если КлючИЗначение.Ключ = КлючИЗначение2.Ключ Тогда
				СтрокаВыборкаВложенногоЗапроса = СтрокаВыборкаВложенногоЗапроса + ",
				|	ВТДокументыСФИООтветственных.ОтветственноеЛицоПоУмолчанию КАК " + КлючИЗначение2.Ключ + ",
				|	ВТДокументыСФИООтветственных.ФИО КАК " + КлючИЗначение2.Ключ + "ФИО";
			Иначе
				СтрокаВыборкаВложенногоЗапроса = СтрокаВыборкаВложенногоЗапроса + ",
				|	ЗНАЧЕНИЕ(Справочник.к2ОтветственныеЛицаОрганизаций.ПустаяСсылка) КАК " + КлючИЗначение2.Ключ + ",
				|	"""" КАК " + КлючИЗначение2.Ключ + "ФИО";
			КонецЕсли;
			
		КонецЦикла;
		
		Если Индекс > 1 Тогда
			ТекстВложенногоЗапроса = ТекстВложенногоЗапроса + "
			|ОБЪЕДИНИТЬ ВСЕ
			|";
		КонецЕсли;
		ТекстВложенногоЗапроса = ТекстВложенногоЗапроса + ШаблонВложенногоЗапроса;
		
		ТекстВложенногоЗапроса = СтрЗаменить(ТекстВложенногоЗапроса , "%СтрокаВыборкаВложенногоЗапроса%", СтрокаВыборкаВложенногоЗапроса);
		ТекстВложенногоЗапроса = СтрЗаменить(ТекстВложенногоЗапроса , "%ОтветственноеЛицо%", 			  ПолучитьПолноеИмяПредопределенногоЗначения(КлючИЗначение.Значение));
		
		СтрокаВыборкаЗапроса = СтрокаВыборкаЗапроса + ",
		|	МАКСИМУМ(ВТДокументыСФИООтветственных." + КлючИЗначение.Ключ + ") КАК " + КлючИЗначение.Ключ + ",
		|	МАКСИМУМ(ВТДокументыСФИООтветственных." + КлючИЗначение.Ключ + "ФИО) КАК " + КлючИЗначение.Ключ + "ФИО";

	КонецЦикла;
	
	ТекстЗапроса = СтрЗаменить(ШаблонЗапроса , "%СтрокаВыборкаЗапроса%", СтрокаВыборкаЗапроса);
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса ,  "%ВложенныйЗапрос%", 	 ТекстВложенногоЗапроса);
	
	Запрос.Текст = Запрос.Текст + ТекстЗапроса;
	
	// Расшифруем данные - выберем нужные реквизиты ответственных лиц в отдельные поля
	ШаблонЗапроса = "
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТСвернутая.Ссылка КАК Ссылка%СтрокаВыборка%
	|ПОМЕСТИТЬ %ИмяТаблицыОтветственныеЛица%
	|ИЗ
	|	ВТДокументыСФИООтветственныхСвернутая КАК ВТСвернутая
	|";
	
	ШаблонСтрокиВыборки = ",
	|	ВТСвернутая.%Реквизит% КАК %Реквизит%,
	|	ЕСТЬNULL(ВТСвернутая.%Реквизит%.ФизическоеЛицо, ЗНАЧЕНИЕ(Справочник.к2ФизическиеЛица.ПустаяСсылка)) КАК %Реквизит%ФизическоеЛицо,
	|	ВТСвернутая.%Реквизит%ФИО
	|		+ ВЫБОР
	|			КОГДА ВТСвернутая.%Реквизит%.ОснованиеПраваПодписи ЕСТь NULL
	|				ИЛИ ВТСвернутая.%Реквизит%.ОснованиеПраваПодписи = """"
	|			ТОГДА """"
	|			ИНАЧЕ "", "" + ВТСвернутая.%Реквизит%.ОснованиеПраваПодписи
	|		  КОНЕЦ КАК %Реквизит%Наименование,
	|	ЕСТЬNULL(ВТСвернутая.%Реквизит%.Должность, """") КАК %Реквизит%Должность,
	|	ЕСТЬNULL(ВТСвернутая.%Реквизит%.ОснованиеПраваПодписи, """") КАК %Реквизит%ОснованиеПраваПодписи";

	СтрокаВыборка = "";
	Для Каждого КлючИЗначение Из РеквизитыОтветственныеЛица Цикл
		СтрокаВыборка = СтрокаВыборка + СтрЗаменить(ШаблонСтрокиВыборки , "%Реквизит%", КлючИЗначение.Ключ);
	КонецЦикла;
	
	ТекстЗапроса = СтрЗаменить(ШаблонЗапроса , "%СтрокаВыборка%", СтрокаВыборка);
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса , "%ИмяТаблицыОтветственныеЛица%", ИмяТаблицыОтветственныеЛица);
	
	// Удалим ненужные временные таблицы
	Запрос.Текст = Запрос.Текст + ТекстЗапроса + "
	|;
	|УНИЧТОЖИТЬ ВТДанныеДокументов
	|;
	|УНИЧТОЖИТЬ ВТНезаполненныеОтветственные
	|;
	|УНИЧТОЖИТЬ ВТОтветственныеПоУмолчанию
	|;
	|УНИЧТОЖИТЬ ВТКандидатыВОтветственныеПоУмолчанию
	|;
	|УНИЧТОЖИТЬ ВТДокументыСОтветственными
	|;
	|УНИЧТОЖИТЬ ВТФИОФизЛиц
	|;
	|УНИЧТОЖИТЬ ВТДокументыСФИООтветственных
	|;
	|УНИЧТОЖИТЬ ВТДокументыСФИООтветственныхСвернутая
	|";
	
	Запрос.Выполнить();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

