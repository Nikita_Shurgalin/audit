
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
		
	к2ТерминалСбораДанныхСервер.ПриСозданииНаСервере(ЭтотОбъект, СтандартнаяОбработка, Отказ);
	
	Если Не УчетПоЯчейкамНаСкладе Тогда
		Отказ = Истина;
	КонецЕсли;
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
		
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(, ЭтотОбъект, "СканерШтрихкода");

	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить();
	
	ПерейтиНаСтраницу(Элементы.СтраницаВыборЗадания);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" Тогда
		Если ИмяСобытия = "ScanData" Тогда
			
			ОбработатьШтрихкод(Параметр[0]);
						
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование

	Если ИмяСобытия = "ЗакрытьФормуТерминалСбораДанных" Тогда
		Объект.ПодтвержденоЗакрытиеФормы = Истина;
		Закрыть();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	к2ТерминалСбораДанныхКлиент.ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка, Объект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(, ЭтотОбъект);

	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

#Область СтраницаЗадания

&НаКлиенте
Процедура ЗаданияВверх(Команда)
	
	Если Задания.Количество() > 0 Тогда
		Индекс = Задания.Индекс(Элементы.Задания.ТекущиеДанные);
		Элементы.Задания.ТекущаяСтрока = ?(Индекс - 1 < 0,
										   Задания[0].ПолучитьИдентификатор(),
										   Задания[Индекс - 1].ПолучитьИдентификатор());
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияВниз(Команда)
	
	Если Задания.Количество() > 0 Тогда
		Индекс = Задания.Индекс(Элементы.Задания.ТекущиеДанные);
		Элементы.Задания.ТекущаяСтрока = ?(Задания.Количество() > Индекс + 1,
										   Задания[Индекс + 1].ПолучитьИдентификатор(),
										   Задания[Задания.Количество() - 1].ПолучитьИдентификатор());
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияСоздать(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаСканирование);	
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияВыбрать(Команда)
	
	ТекущиеДанные = Элементы.Задания.ТекущиеДанные;
	
	ТекстОшибки = НСтр("ru = 'Необходимо выбрать план отгрузки'");
	Если ТекущиеДанные = Неопределено 
		ИЛИ Не ЗначениеЗаполнено(ТекущиеДанные.ЗаданиеСсылка) Тогда
		к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(ТекстОшибки);
	Иначе
		ПланОтгрузки = ТекущиеДанные.ЗаданиеСсылка;
		ПерейтиНаСтраницу(Элементы.СтраницаРаспоряженияНаПеремещение);
	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияОбновить(Команда)
	
	ОбновитьТаблицуЗаданий();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияПерейтиВОсновноеМеню(Команда)
	
	Закрыть();	
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаСканирование

&НаКлиенте
Процедура СканированиеЯчейка(Команда)
	
	ЯчейкаОтправитель = Неопределено;
	ОбъектСканирования = ОбъектСканированияЯчейка();
	УстановитьСостояниеВыполненияСканирования();
	УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеТовар(Команда)
		
	Если Не ЗначениеЗаполнено(ЯчейкаОтправитель) Тогда
		Возврат;
	КонецЕсли;
	
	ОчиститьТекущиеЗначения(ЭтотОбъект);
	ОбъектСканирования = ОбъектСканированияТовары();
	УстановитьСостояниеВыполненияСканирования();
	УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеНазад(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаРаспоряженияНаПеремещение);
		
КонецПроцедуры

&НаКлиенте
Процедура СканированиеЗавершить(Команда)
	
	Если ПроверитьПланФакт() Тогда
		ПерейтиНаСтраницу(Элементы.СтраницаВводаУпаковочногоЛиста);
	КонецЕсли;	
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеПросмотрСоставаТоваров(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаСоставТоваров);		

КонецПроцедуры

&НаКлиенте
Процедура СканированиеПланФакт(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаПланФакт);	
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеДалее(Команда)
	
	Если Объект.ТоварыДляЗаписи.Количество() > 0 Тогда
		
		ТекущаяСтрокаТоваров = Объект.ТоварыПланФакт.НайтиПоИдентификатору(ТекущаяСтрокаОтбора);
		Если Не ТекущаяСтрокаТоваров = Неопределено Тогда
			ТекущаяСтрокаТоваров.СтрокаОбработана = Истина;
		КонецЕсли;
				
		СканированиеДалееНаСервере();
				
	КонецЕсли;
			
	УстановитьСледующийОбъектСканирования();
	УстановитьСостояниеВыполненияСканирования();
	УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);
	
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеДалееНаСервере()
			
	СохранитьДокументОтбор(, Ложь);
	ОчиститьЯчейки(ЭтотОбъект);
	ОчиститьТекущиеЗначения(ЭтотОбъект);	
	УстановитьСледующуюСтрокуДляОтбора();
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаСоставТоваров

&НаКлиенте
Процедура СоставТоваровНазад(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаСканирование);
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаПланФакт

&НаКлиенте
Процедура ПланФактНазад(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаСканирование);
	
КонецПроцедуры

&НаКлиенте
Процедура ПланФактПеревернуть(Команда)
	
	ПланФактПеревернутьТаблицу();	
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаРаспоряжениеНаПеремещение

&НаКлиенте
Процедура РаспоряженияНаПеремещениеНазад(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаВыборЗадания);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспоряженияНаПеремещениеВверх(Команда)
	
	Если РаспоряженияНаПеремещение.Количество() > 0 Тогда
		Индекс = РаспоряженияНаПеремещение.Индекс(Элементы.РаспоряженияНаПеремещение.ТекущиеДанные);
		Элементы.РаспоряженияНаПеремещение.ТекущаяСтрока = ?(Индекс - 1 < 0,
															 РаспоряженияНаПеремещение[0].ПолучитьИдентификатор(),
															 РаспоряженияНаПеремещение[Индекс - 1].ПолучитьИдентификатор());
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РаспоряженияНаПеремещенияВниз(Команда)
	
	Если РаспоряженияНаПеремещение.Количество() > 0 Тогда
		Индекс = РаспоряженияНаПеремещение.Индекс(Элементы.РаспоряженияНаПеремещение.ТекущиеДанные);
		Элементы.РаспоряженияНаПеремещение.ТекущаяСтрока = ?(РаспоряженияНаПеремещение.Количество() > Индекс + 1,
															 РаспоряженияНаПеремещение[Индекс + 1].ПолучитьИдентификатор(),
															 РаспоряженияНаПеремещение[Задания.Количество() - 1].ПолучитьИдентификатор());
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РаспоряженияНаПеремещениеВыбрать(Команда)
	
	ТекущиеДанные = Элементы.РаспоряженияНаПеремещение.ТекущиеДанные;
	
	ТекстОшибки = НСтр("ru = 'Необходимо выбрать распоряжение на перемещение'");
	Если ТекущиеДанные = Неопределено Тогда
		к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(ТекстОшибки);
		Возврат;
	КонецЕсли;
	
	ТекущееРаспоряжение = ТекущиеДанные.Распоряжение;
	
	Если ЗначениеЗаполнено(ТекущееРаспоряжение) Тогда	
		ПерейтиНаСтраницу(Элементы.СтраницаСканирование);		
	Иначе		
		к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(ТекстОшибки);		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РаспоряженияНаПеремещениеОбновить(Команда)
	
	ОбновитьТаблицуРаспоряженийНаПеремещение();
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаВводаУпаковочногоЛиста

&НаКлиенте
Процедура ВводУпаковочногоЛистаОК(Команда)
	
	ОбработатьШтрихкод(Штрихкод);
	
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

&НаКлиенте
Процедура ВводУпаковочногоЛистаОтмена(Команда)
	
	ПерейтиНаСтраницу(Элементы.СтраницаСканирование);	
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

#Область ОбработчикиСобытийЭлементовТаблицыФормыТоварыДляЗаписи

&НаКлиенте
Процедура ТоварыДляЗаписиПриИзменении(Элемент)
	
	ТоварыДляЗаписиПриИзмененииНаСервере();
	УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ТоварыДляЗаписиПриИзмененииНаСервере()
	
	ДобавитьТоварыДляЗаписиВТоварыПланФакт();
	РассчитатьТекущееКоличествоПоНоменклатуре();		
	
	ОчиститьТекущиеЗначения(ЭтотОбъект);	
	УстановитьСледующуюСтрокуДляОтбора();
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ШтрихкодПриИзменении(Элемент)
	
	ОбработатьШтрихкод(Штрихкод);
	
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Функция ПроверитьПланФакт()
	
	ПродолжитьЗавершение = Ложь;
	
	Если Объект.ПроверитьСоответствиеПланаИФакта Тогда		
		
		ПродолжитьЗавершение = к2ТерминалСбораДанныхКлиент.ФактСоответствуетПлану(Объект);
		Если Не ПродолжитьЗавершение Тогда
			
			ТекстВопроса = НСтр("ru = 'Факт не соответствует плану. Продолжить?'");
			ОписаниеОповещенияВопроса = Новый ОписаниеОповещения("ВопросЗавершитьОперациюЗавершение", ЭтотОбъект);
			к2ТерминалСбораДанныхКлиент.ПоказатьВопросПользователю(ТекстВопроса, ОписаниеОповещенияВопроса);
		
		КонецЕсли;
		
	Иначе
		ПродолжитьЗавершение = Истина;	
	КонецЕсли;
	
	Возврат ПродолжитьЗавершение;
	
КонецФункции

&НаКлиенте
Процедура ВопросЗавершитьОперациюЗавершение(Результат, ДополнительныеПараметры) Экспорт
			   
	Если Результат = КодВозвратаДиалога.Да Тогда
		
		ПерейтиНаСтраницу(Элементы.СтраницаВводаУпаковочногоЛиста);
		
	КонецЕсли;
	
КонецПроцедуры

#Область ПереходСтраниц

&НаКлиенте
Процедура ПерейтиНаСтраницу(Знач Страница)
	
	Если Страница = Элементы.СтраницаВыборЗадания Или Страница = Элементы.СтраницаРаспоряженияНаПеремещение Тогда
		
		ОчиститьФорму(ЭтотОбъект);
		ПерейтиНаСтраницуНаСервере(Страница.Имя);
		
	ИначеЕсли Страница = Элементы.СтраницаСканирование Тогда
			
		ПерейтиНаСтраницуНаСервере(Страница.Имя);
		УстановитьСледующийОбъектСканирования();
		УстановитьСостояниеВыполненияСканирования();
		УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);		
		Объект.ТекстОшибки = "";
		
	ИначеЕсли Страница = Элементы.СтраницаВводаУпаковочногоЛиста Тогда
		
		Объект.ТекстОшибки = "";
	
	КонецЕсли;		
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Страница;
	
	УстановитьВидимостьДоступность();	
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиНаСтраницуНаСервере(ИмяСтраницы)
	
	Если ИмяСтраницы = Элементы.СтраницаВыборЗадания.Имя Тогда
		
		ОбновитьТаблицуЗаданий();
		
	ИначеЕсли ИмяСтраницы = Элементы.СтраницаРаспоряженияНаПеремещение.Имя Тогда
		
		ОбновитьТаблицуРаспоряженийНаПеремещение();
		
	ИначеЕсли ИмяСтраницы = Элементы.СтраницаСканирование.Имя Тогда
			
		Если Элементы.ГруппаСтраницы.ТекущаяСтраница.Имя = Элементы.СтраницаРаспоряженияНаПеремещение.Имя Тогда
			ЗаполнитьПлановыеПоказателиОтбора();
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработкаШтрихкода

&НаКлиенте
Процедура ОбработатьШтрихкод(ТекущийШтрихкод)
	
	Объект.ТекстОшибки = "";
	
	ТекущаяСтраница = Элементы.ГруппаСтраницы.ТекущаяСтраница;
	Если ТекущаяСтраница = Элементы.СтраницаСканирование Тогда
		
		ОбработатьШтрихкод_СканированиеСтраницаСканирование(ТекущийШтрихкод);
				
	ИначеЕсли ТекущаяСтраница = Элементы.СтраницаВыборЗадания Тогда
		
		ОбработатьШтрихкод_СканированиеСтраницаВыборЗадания(ТекущийШтрихкод);
				
	ИначеЕсли ТекущаяСтраница = Элементы.СтраницаВводаУпаковочногоЛиста Тогда
		
		ОбработатьШтрихкод_СканированиеСтраницаВводУпаковочногоЛиста(ТекущийШтрихкод);
					
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.ТекстОшибки) Тогда 		
		к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(Объект.ТекстОшибки);	    
	КонецЕсли;        		
		
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьШтрихкод_СканированиеСтраницаСканирование(ТекущийШтрихкод)
					
	Если ОбъектСканирования = ОбъектСканированияТовары() Тогда 
		
		ОбработатьШтрихкод_СканированиеТовары(ТекущийШтрихкод);
		
	ИначеЕсли ОбъектСканирования = ОбъектСканированияЯчейка() Тогда
		
		ОбработатьШтрихкод_ЯчейкиОтправительСервер(ТекущийШтрихкод);
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.ТекстОшибки) Тогда 					
		УстановитьСледующийОбъектСканирования();
		УстановитьСостояниеВыполненияСканирования();
		УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);
	КонецЕсли;
							
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьШтрихкод_СканированиеТовары(ТекущийШтрихкод)
					
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(ТекущийШтрихкод);
	Если к2ТерминалСбораДанныхКлиентСервер.ПроверитьПараметрыШтрихкода(ПараметрыШтрихкода, Объект.ТекстОшибки) Тогда			
		
		ОбработатьШтрихкод_СканированиеТоваровСервер(ПараметрыШтрихкода);
		
	КонецЕсли;
							
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихкод_СканированиеТоваровСервер(ПараметрыШтрихкода)

	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");	
	
	ОбработкаОбъект.ОбработатьПараметрыШтрихкода_СканированиеТоваров(ПараметрыШтрихкода);
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
	
	к2ТерминалСбораДанныхСервер.ПроверитьТекущуюНоменклатуру(ЭтотОбъект);
	
	Если Не ЗначениеЗаполнено(Объект.ТекстОшибки)
		И Объект.ТекущиеТовары.Количество() > 0 Тогда
		ДобавитьТекущиеТоварыВТабличныеЧасти();
		УстановитьЗаголовкиКомандСканирования(ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихкод_ЯчейкиОтправительСервер(Штрихкод)
	
	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");	
	
	ОбработкаОбъект.ОбработатьШтрихкод_СканированиеЯчейки(Штрихкод);
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
		
	Если ЗначениеЗаполнено(Объект.ТекущаяЯчейка) Тогда
		
		ЯчейкаОтправитель = Объект.ТекущаяЯчейка;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьШтрихкод_СканированиеСтраницаВыборЗадания(ТекущийШтрихкод)
					
	ОбработатьШтрихкод_СканированиеДокументаСервер(ТекущийШтрихкод);			
	
	Если ЗначениеЗаполнено(Объект.ТекущийДокумент) Тогда
		
		СтрокиЗадания = Задания.НайтиСтроки(Новый Структура("ЗаданиеСсылка", Объект.ТекущийДокумент));
		Если СтрокиЗадания.Количество() > 0 Тогда
			Элементы.Задания.ТекущаяСтрока = СтрокиЗадания[0];
		Иначе
			Объект.ТекстОшибки = НСтр("ru = 'Документ не найден в доступных заданиях'");
		КонецЕсли;
		
	Иначе
		Объект.ТекстОшибки = НСтр("ru = 'Документ не найден в доступных заданиях'");
	КонецЕсли;
							
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихкод_СканированиеДокументаСервер(Штрихкод)

	Массив = Новый Массив;
	Массив.Добавить("к2ПланОтгрузки");
	
	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");	
	
	ОбработкаОбъект.ОбработатьШтрихкод_СканированиеДокумента(Штрихкод, Массив);
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьШтрихкод_СканированиеСтраницаВводУпаковочногоЛиста(ТекущийШтрихкод)
					
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(ТекущийШтрихкод);
	Если к2ТерминалСбораДанныхКлиентСервер.ПроверитьПараметрыШтрихкода(ПараметрыШтрихкода, Объект.ТекстОшибки) Тогда			
		
		ОбработатьШтрихкод_ВводУпаковочногоЛистаНаСервере(ПараметрыШтрихкода);
		
		Если Не ЗначениеЗаполнено(Объект.ТекстОшибки) Тогда 	
			
			ЗавершитьОперацию_Отбор();
			
		КонецЕсли;
		
	КонецЕсли;
						
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихкод_ВводУпаковочногоЛистаНаСервере(ПараметрыШтрихкода)
	
	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");	
	
	ОбработкаОбъект.ОбработатьШтрихкод_СканированиеУпаковочногоЛиста(ПараметрыШтрихкода);
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
	
	Если ЗначениеЗаполнено(Объект.ТекущийУпаковочныйЛист) Тогда
		
		ЗаполнитьУпаковочныйЛистТоварами();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьУпаковочныйЛистТоварами(Отказ = Ложь)
	
	УпаковочныйЛистОбъект = Объект.ТекущийУпаковочныйЛист.ПолучитьОбъект();
	
	УпаковочныйЛистОбъект.Товары.Загрузить(Объект.ТоварыДляЗаписи.Выгрузить());
		
	Если УпаковочныйЛистОбъект.Товары.Количество() > 0 Тогда
		к2УправлениеДаннымиСервер.ПровестиДокумент(УпаковочныйЛистОбъект,, Объект.ТекстОшибки, Отказ, Истина);	
	КонецЕсли;
	
	Если Не Отказ Тогда
		
		Для Каждого Строка Из Объект.ТоварыДляЗаписи Цикл
			
			Строка.УпаковочныйЛист = Объект.ТекущийУпаковочныйЛист;
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ЗавершитьОперацию

&НаКлиенте
Процедура ЗавершитьОперацию_Отбор()

	Если ЗавершитьОперацию_ОтборСервер() Тогда
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ЗавершениеОперации", ЭтотОбъект);
		ТекстСообщения = НСтр("ru = 'Заполнен документ %1'"); 
		к2ТерминалСбораДанныхКлиент.ПоказатьОповещениеПользователю(стрЗаполнить(ТекстСообщения, ТекущееРаспоряжение), ОписаниеОповещения);	
		
	Иначе
		
		Если ЗначениеЗаполнено(Объект.ТекстОшибки) Тогда
			к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(Объект.ТекстОшибки);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершениеОперации(Результат, ДополнительныеПараметры) Экспорт
	
	ПерейтиНаСтраницу(Элементы.СтраницаРаспоряженияНаПеремещение);
		
КонецПроцедуры

&НаСервере
Функция ЗавершитьОперацию_ОтборСервер()

	Отказ = Ложь;
	
	СохранитьДокументОтбор(Отказ, Истина);
	
	Возврат Не Отказ;	

КонецФункции

&НаСервере
Процедура СохранитьДокументОтбор(Отказ = Ложь, Выполнено = Истина)
	
	ДокументОбъект = ТекущееРаспоряжение.ПолучитьОбъект();
	
	Если Выполнено Тогда
		ДокументОбъект.Статус = Перечисления.к2СтатусыРаспоряженийНаПеремещение.Выполнено;
	Иначе
		ДокументОбъект.Статус = Перечисления.к2СтатусыРаспоряженийНаПеремещение.Выполняется;
	КонецЕсли;
		
	ЯчейкаПолучатель = глРеквизит(Объект.Склад, "ЯчейкаОтгрузки");
	
	ДокументОбъект.Товары.Загрузить(Объект.ТоварыДляЗаписи.Выгрузить());
	
	Для Каждого Строка Из ДокументОбъект.Товары Цикл
		
		Строка.ЯчейкаПолучатель = ЯчейкаПолучатель;
		
	КонецЦикла;
		
	к2УправлениеДаннымиСервер.ПровестиДокумент(ДокументОбъект,, Объект.ТекстОшибки, Отказ, Истина);
	
	ТекущееРаспоряжение = ДокументОбъект.Ссылка;	

КонецПроцедуры

#КонецОбласти

#Область ТаблицаЗаданий

&НаСервере
Процедура ОбновитьТаблицуЗаданий()

	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	к2ПланОтгрузки.Ссылка КАК Ссылка,
		|	к2ПланОтгрузки.Номер КАК Номер,
		|	к2ПланОтгрузки.Дата КАК Дата,
		|	СУММА(к2ПланОтгрузкиТовары.Количество) КАК Количество
		|ИЗ
		|	Документ.к2ПланОтгрузки.Товары КАК к2ПланОтгрузкиТовары
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.к2ПланОтгрузки КАК к2ПланОтгрузки
		|		ПО к2ПланОтгрузкиТовары.Ссылка = к2ПланОтгрузки.Ссылка
		|ГДЕ
		|	к2ПланОтгрузки.Проведен
		|	И к2ПланОтгрузки.Склад = &СкладОтправитель
		|	И (к2ПланОтгрузки.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПланаОтгрузки.ВНаборку)
		|			ИЛИ к2ПланОтгрузки.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыПланаОтгрузки.Набирается))
		|                                                                        
		|СГРУППИРОВАТЬ ПО
		|	к2ПланОтгрузки.Ссылка,
		|	к2ПланОтгрузки.Номер,
		|	к2ПланОтгрузки.Дата";
	#КонецОбласти
	
	Запрос.УстановитьПараметр("СкладОтправитель", Объект.Склад);
	
	ТаблицаЗаданий = Запрос.Выполнить().Выгрузить();
	
	Задания.Очистить();
	Для Каждого Строка Из ТаблицаЗаданий Цикл
		
		НоваяСтрока = Задания.Добавить();
		НоваяСтрока.ЗаданиеСсылка = Строка.Ссылка;
		НоваяСтрока.ЗаданиеПредставление = СтрШаблон(НСтр("ru = '№ %1 от %2 %3'"),
										   ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Строка.Номер),
										   Формат(Строка.Дата, "ДЛФ=D"),
										   Строка.Количество);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ТаблицаРаспоряженийНаПеремещение

&НаСервере
Процедура ОбновитьТаблицуРаспоряженийНаПеремещение()
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	к2РаспоряжениеНаПеремещение.Ссылка КАК Ссылка,
		|	к2РаспоряжениеНаПеремещение.Номер КАК Номер,
		|	к2РаспоряжениеНаПеремещение.Дата КАК Дата,
		|	к2РаспоряжениеНаПеремещение.Статус КАК Статус
		|ПОМЕСТИТЬ втРаспоряжения
		|ИЗ
		|	Документ.к2РаспоряжениеНаПеремещение КАК к2РаспоряжениеНаПеремещение
		|ГДЕ
		|	к2РаспоряжениеНаПеремещение.Проведен
		|	И к2РаспоряжениеНаПеремещение.СкладОтправитель = &СкладОтправитель
		|	И к2РаспоряжениеНаПеремещение.ТипПеремещения = ЗНАЧЕНИЕ(Перечисление.к2ТипыРаспоряженийНаПеремещение.Отбор)
		|	И НЕ к2РаспоряжениеНаПеремещение.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыРаспоряженийНаПеремещение.Запланировано)
		|	И к2РаспоряжениеНаПеремещение.Основание = &Основание
		|
		|СГРУППИРОВАТЬ ПО
		|	к2РаспоряжениеНаПеремещение.Ссылка,
		|	к2РаспоряжениеНаПеремещение.Номер,
		|	к2РаспоряжениеНаПеремещение.Дата,
		|	к2РаспоряжениеНаПеремещение.Статус
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	втРаспоряжения.Ссылка КАК Ссылка,
		|	втРаспоряжения.Номер КАК Номер,
		|	втРаспоряжения.Дата КАК Дата,
		|	СУММА(к2РаспоряжениеНаПеремещениеТоварыПлан.Количество) КАК Количество,
		|	втРаспоряжения.Статус КАК Статус
		|ПОМЕСТИТЬ втРаспоряженияПлан
		|ИЗ
		|	втРаспоряжения КАК втРаспоряжения
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.к2РаспоряжениеНаПеремещение.ТоварыПлан КАК к2РаспоряжениеНаПеремещениеТоварыПлан
		|		ПО втРаспоряжения.Ссылка = к2РаспоряжениеНаПеремещениеТоварыПлан.Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	втРаспоряжения.Ссылка,
		|	втРаспоряжения.Номер,
		|	втРаспоряжения.Дата,
		|	втРаспоряжения.Статус
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	втРаспоряженияПлан.Ссылка КАК Распоряжение,
		|	втРаспоряженияПлан.Номер КАК Номер,
		|	втРаспоряженияПлан.Дата КАК Дата,
		|	втРаспоряженияПлан.Количество КАК Количество,
		|	втРаспоряженияПлан.Статус КАК Статус
		|ИЗ
		|	втРаспоряженияПлан КАК втРаспоряженияПлан";
	
	#КонецОбласти

	Запрос.УстановитьПараметр("Основание", ПланОтгрузки);
	Запрос.УстановитьПараметр("СкладОтправитель", Объект.Склад);
	
	ТаблицаРаспоряжений = Запрос.Выполнить().Выгрузить();
		
	РаспоряженияНаПеремещение.Очистить();
	
	Для Каждого Строка Из ТаблицаРаспоряжений Цикл
		
		НоваяСтрока = РаспоряженияНаПеремещение.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
		НоваяСтрока.РаспоряжениеПредставление = СтрШаблон(НСтр("ru = '№ %1 от %2 %3 %4'"), 
														  ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Строка.Номер),
														  Формат(Строка.Дата, "ДЛФ=D"),
														  Строка.Количество,
														  Строка.Статус);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ОбновлениеРеквизитовФормы

&НаКлиентеНаСервереБезКонтекста
Процедура ОчиститьФорму(Форма)	

	Форма.Объект.ТекстОшибки = "";
	
	ОчиститьТекущиеЗначения(Форма);
	ОчиститьТовары(Форма);
	ОчиститьЯчейки(Форма);
	УстановитьЗаголовкиКомандСканирования(Форма);
		
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОчиститьТекущиеЗначения(Форма)
	
	Форма.ТекущаяНоменклатура = Неопределено;
	Форма.ТекущаяХарактеристика = Неопределено;
	Форма.ТекущаяСерия = Неопределено;
	Форма.ТекущееКоличество = Неопределено;	
	Форма.ТекущееКоличествоПлан = Неопределено;	
	Форма.Объект.ТекущийУпаковочныйЛист = Неопределено;	
	Форма.ТекущееКоличествоПоНоменклатуре = 0;
	Форма.НоменклатураПлан = Неопределено;
	
	Форма.Объект.ТекущиеТовары.Очистить();
	Форма.Объект.ТекстОшибки = Неопределено;
	
КонецПроцедуры 

&НаКлиентеНаСервереБезКонтекста
Процедура ОчиститьТовары(Форма)

	Форма.Объект.ТоварыПланФакт.Очистить();
	Форма.Объект.ТоварыДляЗаписи.Очистить();
	Форма.Объект.ТекущиеТовары.Очистить();
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОчиститьЯчейки(Форма)

	Форма.ЯчейкаОтправитель = Неопределено;
	Форма.ЯчейкаОтправительПлан = Неопределено;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

#Область ПеревернутьТаблицу

&НаКлиенте
Процедура ПланФактПеревернутьТаблицу()
		
	СортировкаЯчеекПоУбыванию = Не СортировкаЯчеекПоУбыванию;
	
	ОчиститьЯчейки(ЭтотОбъект);
	ОчиститьТекущиеЗначения(ЭтотОбъект);
	
	ПланФактПеревернутьНаСервере();		
	
КонецПроцедуры

&НаСервере
Процедура ПланФактПеревернутьНаСервере()
	
	ТаблицаТовары = Объект.ТоварыПланФакт.Выгрузить();
	
	Если СортировкаЯчеекПоУбыванию Тогда
		ТаблицаТовары.Сортировать("Приоритет Убыв");
	Иначе
		ТаблицаТовары.Сортировать("Приоритет Возр");
	КонецЕсли;
	
	Объект.ТоварыПланФакт.Загрузить(ТаблицаТовары);
	
	УстановитьСледующуюСтрокуДляОтбора();
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура УстановитьСледующийОбъектСканирования()

	Если ЗначениеЗаполнено(ЯчейкаОтправитель) Тогда				
		ОбъектСканирования = ОбъектСканированияТовары();
	Иначе
		ОбъектСканирования = ОбъектСканированияЯчейка();
	КонецЕсли;	
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьТекущиеТоварыВТабличныеЧасти()
	
	ДобавитьТекущиеТоварыВТоварыДляЗаписи();
	ДобавитьТоварыДляЗаписиВТоварыПланФакт();
	
	РассчитатьТекущееКоличествоПоНоменклатуре();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСледующуюСтрокуДляОтбора()
	
	Для Каждого Строка Из Объект.ТоварыПланФакт Цикл
		
		Если Не Строка.СтрокаОбработана Тогда
			
			НоменклатураПлан = Строка.Номенклатура;
			ЯчейкаОтправительПлан = Строка.ЯчейкаОтправитель;
			ТекущееКоличествоПлан = Строка.КоличествоПлан;
			ТекущаяСтрокаОтбора = Строка.ПолучитьИдентификатор();
			Прервать;
			
		КонецЕсли;
		
	КонецЦикла;	
		
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПлановыеПоказателиОтбора()
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	к2РаспоряжениеНаПеремещение.Ссылка КАК Ссылка
		|ПОМЕСТИТЬ втРаспоряжения
		|ИЗ
		|	Документ.к2РаспоряжениеНаПеремещение КАК к2РаспоряжениеНаПеремещение
		|ГДЕ
		|	к2РаспоряжениеНаПеремещение.Ссылка = &Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	к2РаспоряжениеНаПеремещение.Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	втРаспоряжения.Ссылка КАК Ссылка,
		|	СУММА(к2РаспоряжениеНаПеремещениеТоварыПлан.Количество) КАК КоличествоПлан,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.Номенклатура КАК Номенклатура,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.Характеристика КАК Характеристика,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.Серия КАК Серия,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.ЯчейкаОтправитель КАК ЯчейкаОтправитель,
		|	СУММА(к2РаспоряжениеНаПеремещениеТоварыПлан.КоличествоУпаковок) КАК КоличествоУпаковокПлан
		|ПОМЕСТИТЬ ТоварыПлан
		|ИЗ
		|	втРаспоряжения КАК втРаспоряжения
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.к2РаспоряжениеНаПеремещение.ТоварыПлан КАК к2РаспоряжениеНаПеремещениеТоварыПлан
		|		ПО втРаспоряжения.Ссылка = к2РаспоряжениеНаПеремещениеТоварыПлан.Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	втРаспоряжения.Ссылка,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.Номенклатура,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.Характеристика,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.Серия,
		|	к2РаспоряжениеНаПеремещениеТоварыПлан.ЯчейкаОтправитель
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ТоварыПлан.Ссылка КАК Ссылка,
		|	ТоварыПлан.КоличествоПлан КАК КоличествоПлан,
		|	ТоварыПлан.Номенклатура КАК Номенклатура,
		|	ТоварыПлан.Характеристика КАК Характеристика,
		|	ТоварыПлан.Серия КАК Серия,
		|	ТоварыПлан.ЯчейкаОтправитель КАК ЯчейкаОтправитель,
		|	ТоварыПлан.КоличествоУпаковокПлан КАК КоличествоУпаковокПлан,
		|	ЕСТЬNULL(к2ПриоритетыСкладскихЯчеек.Приоритет, 0) КАК Приоритет
		|ИЗ
		|	ТоварыПлан КАК ТоварыПлан
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ПриоритетыСкладскихЯчеек КАК к2ПриоритетыСкладскихЯчеек
		|		ПО ТоварыПлан.ЯчейкаОтправитель = к2ПриоритетыСкладскихЯчеек.Ячейка
		|
		|УПОРЯДОЧИТЬ ПО
		|	&СтрокаЗаменыУпорядочивания";
	#КонецОбласти
	
	Запрос.УстановитьПараметр("Ссылка", ТекущееРаспоряжение);
	
	Если СортировкаЯчеекПоУбыванию Тогда
		СтрокаЗаменыУпорядочивания = "Приоритет УБЫВ";
	Иначе
		СтрокаЗаменыУпорядочивания = "Приоритет";
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&СтрокаЗаменыУпорядочивания", СтрокаЗаменыУпорядочивания);
	
	ТаблицаТовары = Запрос.Выполнить().Выгрузить();
	Объект.ТоварыПланФакт.Загрузить(ТаблицаТовары);
	
	Если Объект.ТоварыПланФакт.Количество() > 0 Тогда
		
		ЯчейкаОтправительПлан = Объект.ТоварыПланФакт[0].ЯчейкаОтправитель;
		НоменклатураПлан = Объект.ТоварыПланФакт[0].Номенклатура;
		ТекущаяСтрокаОтбора = Объект.ТоварыПланФакт[0].ПолучитьИдентификатор();
		ТекущееКоличествоПлан = Объект.ТоварыПланФакт[0].КоличествоПлан;
			
	КонецЕсли;
		
КонецПроцедуры

&НаСервере
Процедура ДобавитьТоварыДляЗаписиВТоварыПланФакт()
	
	ЗаполнитьПлановыеПоказателиОтбора();
	
	Для Каждого Строка Из Объект.ТоварыДляЗаписи Цикл
		
		ДобавитьФактВСтрокуВТоварыПланФакт(Строка);
		
	КонецЦикла;
	
	ДополнитьТоварыПланФактПриоритетомЯчеек();
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьФактВСтрокуВТоварыПланФакт(ИсходнаяСтрока)

	СтруктураПоиска = к2ТерминалСбораДанныхКлиентСервер.ПустаяСтруктураПоискаСтрокиПланФакт();
	СтруктураПоиска.Вставить("ЯчейкаОтправитель");
	
	ЗаполнитьЗначенияСвойств(СтруктураПоиска, ИсходнаяСтрока, "Номенклатура, Характеристика, Серия, ЯчейкаОтправитель"); 
			
	НайденныеСтроки = Объект.ТоварыПланФакт.НайтиСтроки(СтруктураПоиска);
	
	Если НайденныеСтроки.Количество() > 0 Тогда
		Строка = НайденныеСтроки[0];
	Иначе 
		Строка = Объект.ТоварыПланФакт.Добавить();
		ЗаполнитьЗначенияСвойств(Строка, ИсходнаяСтрока, "Номенклатура, Характеристика, Серия, ЯчейкаОтправитель");
	КонецЕсли;	
	
	Строка.Количество = Строка.Количество + ИсходнаяСтрока.Количество;
	Строка.КоличествоУпаковок = Строка.КоличествоУпаковок + ИсходнаяСтрока.КоличествоУпаковок;
		
	ТекущееКоличествоПлан = Строка.КоличествоПлан;
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьТекущиеТоварыВТоварыДляЗаписи()
	
	Для Каждого Строка Из Объект.ТекущиеТовары Цикл
		
		НоваяСтрокаДляЗаписи = Объект.ТоварыДляЗаписи.Добавить();
		
		ЗаполнитьЗначенияСвойств(НоваяСтрокаДляЗаписи, Строка);
		
		НоваяСтрокаДляЗаписи.ЯчейкаОтправитель = ЯчейкаОтправитель;
		
		ТекущаяНоменклатура = НоваяСтрокаДляЗаписи.Номенклатура;
		ТекущаяХарактеристика = НоваяСтрокаДляЗаписи.Характеристика; 
		ТекущаяСерия = НоваяСтрокаДляЗаписи.Серия;
		ТекущееКоличество = НоваяСтрокаДляЗаписи.Количество;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ДополнитьТоварыПланФактПриоритетомЯчеек()
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	ТоварыПланФакт.Номенклатура КАК Номенклатура,
		|	ТоварыПланФакт.Характеристика КАК Характеристика,
		|	ТоварыПланФакт.Серия КАК Серия,
		|	ТоварыПланФакт.Упаковка КАК Упаковка,
		|	ТоварыПланФакт.Количество КАК Количество,
		|	ТоварыПланФакт.КоличествоУпаковок КАК КоличествоУпаковок,
		|	ТоварыПланФакт.КоличествоПлан КАК КоличествоПлан,
		|	ТоварыПланФакт.КоличествоУпаковокПлан КАК КоличествоУпаковокПлан,
		|	ТоварыПланФакт.СтрокаОбработана КАК СтрокаОбработана,
		|	ТоварыПланФакт.Спецификация КАК Спецификация,
		|	ТоварыПланФакт.ЯчейкаОтправитель КАК ЯчейкаОтправитель,
		|	ТоварыПланФакт.ЯчейкаПолучатель КАК ЯчейкаПолучатель
		|ПОМЕСТИТЬ втТоварыПланФакт
		|ИЗ
		|	&ТоварыПланФакт КАК ТоварыПланФакт
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	втТоварыПланФакт.Номенклатура КАК Номенклатура,
		|	втТоварыПланФакт.Характеристика КАК Характеристика,
		|	втТоварыПланФакт.Серия КАК Серия,
		|	втТоварыПланФакт.Упаковка КАК Упаковка,
		|	втТоварыПланФакт.Количество КАК Количество,
		|	втТоварыПланФакт.КоличествоУпаковок КАК КоличествоУпаковок,
		|	втТоварыПланФакт.КоличествоПлан КАК КоличествоПлан,
		|	втТоварыПланФакт.КоличествоУпаковокПлан КАК КоличествоУпаковокПлан,
		|	втТоварыПланФакт.СтрокаОбработана КАК СтрокаОбработана,
		|	втТоварыПланФакт.Спецификация КАК Спецификация,
		|	втТоварыПланФакт.ЯчейкаОтправитель КАК ЯчейкаОтправитель,
		|	втТоварыПланФакт.ЯчейкаПолучатель КАК ЯчейкаПолучатель,
		|	ЕСТЬNULL(к2ПриоритетыСкладскихЯчеек.Приоритет, 0) КАК Приоритет
		|ИЗ
		|	втТоварыПланФакт КАК втТоварыПланФакт
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.к2ПриоритетыСкладскихЯчеек КАК к2ПриоритетыСкладскихЯчеек
		|		ПО втТоварыПланФакт.ЯчейкаОтправитель = к2ПриоритетыСкладскихЯчеек.Ячейка
		|
		|УПОРЯДОЧИТЬ ПО
		|	&СтрокаЗаменыУпорядочивания";
	#КонецОбласти
	
	Запрос.УстановитьПараметр("ТоварыПланФакт", Объект.ТоварыПланФакт.Выгрузить());
	
	Если СортировкаЯчеекПоУбыванию Тогда
		СтрокаЗаменыУпорядочивания = "Приоритет УБЫВ";
	Иначе
		СтрокаЗаменыУпорядочивания = "Приоритет";
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&СтрокаЗаменыУпорядочивания", СтрокаЗаменыУпорядочивания);
	
	Объект.ТоварыПланФакт.Загрузить(Запрос.Выполнить().Выгрузить());
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСостояниеВыполненияСканирования()
		
	Если ОбъектСканирования = ОбъектСканированияЯчейка() Тогда		
		Если ЗначениеЗаполнено(ЯчейкаОтправительПлан) Тогда
			СтрокаЯчейки = стрЗаполнить(НСтр("ru = '. (Ячейка сканирования %1)'"), ЯчейкаОтправительПлан);
		Иначе
			СтрокаЯчейки = "";
		КонецЕсли;	
		Элементы.ДекорацияСостояниеВыполненияСканирования.Заголовок = стрЗаполнить(НСтр("ru = 'Сканируйте ячейку%1'"), СтрокаЯчейки);
	ИначеЕсли ОбъектСканирования = ОбъектСканированияТовары() Тогда			
		Если ЗначениеЗаполнено(НоменклатураПлан) Тогда 
			СтрокаНоменклатуры = стрЗаполнить(НСтр("ru = '. (%1)'"), НоменклатураПлан);
		КонецЕсли;
		Элементы.ДекорацияСостояниеВыполненияСканирования.Заголовок = стрЗаполнить(НСтр("ru = 'Сканируйте упаковку%1'"), СтрокаНоменклатуры);
	КонецЕсли;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьЗаголовкиКомандСканирования(Форма)
	
	Если ЗначениеЗаполнено(Форма.ЯчейкаОтправитель) Тогда
		Форма.Элементы.СканированиеЯчейка.Заголовок = Форма.ЯчейкаОтправитель;
	Иначе
		Форма.Элементы.СканированиеЯчейка.Заголовок = "";
	КонецЕсли;	
	Если ЗначениеЗаполнено(Форма.ТекущаяНоменклатура) Тогда
		Форма.Элементы.СканированиеТовар.Заголовок = Форма.ТекущаяНоменклатура;
	Иначе
		Форма.Элементы.СканированиеТовар.Заголовок = "";
	КонецЕсли;
	Если ЗначениеЗаполнено(Форма.Объект.ТекущийУпаковочныйЛист)  Тогда
		Форма.Элементы.СканированиеУпаковочныйЛист.Заголовок = Форма.Объект.ТекущийУпаковочныйЛист;
	Иначе
		Форма.Элементы.СканированиеУпаковочныйЛист.Заголовок = "";
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	#Область ВидимостьДополнительногоПоляШтрихкод
	ОтобразитьПолеШтрихкода = Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаСканирование
							ИЛИ Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаВыборЗадания;
							
	Элементы.ГруппаРучнойВводШтрихкода.Видимость = ОтобразитьПолеШтрихкода;
	#КонецОбласти
	
	#Область ВидимостьДополнительногоПоляШтрихкод
	ОтобразитьЗаголовокРежимаРаботы = Не Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.СтраницаСканирование;
							
	Элементы.ЗаголовокРежимаРаботы.Видимость = ОтобразитьЗаголовокРежимаРаботы;
	#КонецОбласти
	
КонецПроцедуры

&НаСервере
Процедура РассчитатьТекущееКоличествоПоНоменклатуре()
	
	СтруктураПоиска = Новый Структура;
	СтруктураПоиска.Вставить("Номенклатура", ТекущаяНоменклатура);
	СтруктураПоиска.Вставить("Характеристика", ТекущаяХарактеристика);
	
	ТаблицаТекущейНоменклатуры = Объект.ТоварыПланФакт.Выгрузить().Скопировать(СтруктураПоиска, "Количество");
	ТекущееКоличествоПоНоменклатуре = ТаблицаТекущейНоменклатуры.Итог("Количество");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьТекущийЭлемент()
	
	Штрихкод = "";
	ТекущийЭлемент = Элементы.Штрихкод;
		
КонецПроцедуры

&НаКлиенте
Процедура УстановитьТекущийЭлементФормы(Команда)
	
	УстановитьТекущийЭлемент();
	
КонецПроцедуры

#КонецОбласти

#Область ОбъектыСканирования

&НаКлиентеНаСервереБезКонтекста
Функция ОбъектСканированияЯчейка()
	
	Возврат "Ячейка";
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ОбъектСканированияТовары()
	
	Возврат "Товары";
	
КонецФункции

#КонецОбласти

#КонецОбласти