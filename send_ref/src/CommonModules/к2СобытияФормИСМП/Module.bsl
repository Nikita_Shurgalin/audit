#Область ПрограммныйИнтерфейс

#Область Локализация

Процедура МодификацияРеквизитовФормы(Форма, ПараметрыИнтеграции, ДобавляемыеРеквизиты) Экспорт
	
	ДобавитьОбщиеНастройкиВстраивания(Форма, ПараметрыИнтеграции);
	ДобавитьРеквизитТекстСостояниеИСМП(Форма, ПараметрыИнтеграции, ДобавляемыеРеквизиты);
	ДобавитьЭлементыПроверкиПодбораИСМП(Форма, ПараметрыИнтеграции, ДобавляемыеРеквизиты);
	
КонецПроцедуры

Процедура МодификацияЭлементовФормы(Форма) Экспорт
	
	к2ПроверкаИПодборПродукцииИСМП.МодификацияЭлементовФормы(Форма);
	к2СобытияФормИС.ВстроитьСтрокуИнтеграцииВДокументОснованиеПоПараметрам(Форма, "ИСМП.ДокументОснование");
	
КонецПроцедуры

Процедура ЗаполнениеРеквизитовФормы(Форма) Экспорт
	
	к2ПроверкаИПодборПродукцииИСМП.ЗаполнитьКешШтрихкодовУпаковок(Форма);
	к2ПроверкаИПодборПродукцииИСМП.ПрименитьКешШтрихкодовУпаковок(Форма);
	к2ПроверкаИПодборПродукцииИСМП.УправлениеВидимостьюЭлеменовПроверкиИПодбора(Форма);
	
	ИмяРеквизитаФормыОбъект = Форма.ПараметрыИнтеграцииГосИС.Получить("ИСМП").ИмяРеквизитаФормыОбъект;
	ПараметрыИнтеграции = Форма.ПараметрыИнтеграцииГосИС.Получить("ИСМП.ДокументОснование");
	Если ПараметрыИнтеграции <> Неопределено И ЗначениеЗаполнено(ПараметрыИнтеграции.ИмяРеквизитаФормы) Тогда
		ТекстНадписи = к2ИнтеграцияИСМПВызовСервера.ТекстНадписиПоляИнтеграцииВФормеДокументаОснования(Форма[ИмяРеквизитаФормыОбъект].Ссылка);
		Форма[ПараметрыИнтеграции.ИмяРеквизитаФормы] = ТекстНадписи;
		Форма.Элементы[ПараметрыИнтеграции.ИмяЭлементаФормы].Видимость = ЗначениеЗаполнено(ТекстНадписи);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПослеЗаписиНаСервере(Форма) Экспорт
	
	
КонецПроцедуры

Процедура ПриСозданииНаСервере(Форма, Отказ, СтандартнаяОбработка) Экспорт
	
	
КонецПроцедуры

Процедура ПриЧтенииНаСервере(Форма, ТекущийОбъект) Экспорт
	
	
КонецПроцедуры

Процедура ПодключитьОбработкуКодовМаркировки(Форма, ЕстьТабличнаяЧастьШтрихкодыУпаковок=Истина, ДополнительныеКлючиШтрихкодовУпаковок = "") Экспорт
	
	Форма.КэшМаркируемойПродукции = ПоместитьВоВременноеХранилище(
		к2ШтрихкодированиеИС.ИнициализацияКэшаМаркируемойПродукции(), Форма.УникальныйИдентификатор);

	Если ЕстьТабличнаяЧастьШтрихкодыУпаковок Тогда
		ДобавляемыеРеквизиты = Новый Массив;
		к2ПроверкаИПодборПродукцииИС.ДобавитьТаблицуШтрихкодовУпаковок(Форма, Новый Соответствие, ДобавляемыеРеквизиты, ДополнительныеКлючиШтрихкодовУпаковок);
		Форма.ИзменитьРеквизиты(ДобавляемыеРеквизиты);
	КонецЕсли;

	к2ШтрихкодированиеИС.ОбновитьКэшМаркируемойПродукции(Форма);
	
КонецПроцедуры

// Обработчик команды формы, требующей контекстного вызова сервера.
//
// Параметры:
//   Форма - ФормаКлиентскогоПриложения - форма, из которой выполняется команда.
//   ПараметрыВызова - Структура - параметры вызова.
//   Источник - ТаблицаФормы, ДанныеФормыСтруктура - объект или список формы с полем "Ссылка".
//   Результат - Структура - результат выполнения команды.
//
Процедура ВыполнитьПереопределяемуюКоманду(Знач Форма, Знач ПараметрыВызова, Знач Источник, Результат) Экспорт
	
	
КонецПроцедуры

#Область СобытияЭлементовФорм

// Серверная переопределяемая процедура, вызываемая из обработчика события элемента.
//
// Параметры:
//   Форма                   - ФормаКлиентскогоПриложения - форма, из которой происходит вызов процедуры.
//   Элемент                 - Строка           - имя элемента-источника события "При изменении"
//   ДополнительныеПараметры - Структура        - значения дополнительных параметров влияющих на обработку.
//
Процедура ПриИзмененииЭлемента(Форма, Элемент, ДополнительныеПараметры) Экспорт
	
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ФормыСписковДокументовИСМП

Процедура ЗаполнитьСписокВыбораОрганизацииПоСохраненнымНастройкам(Форма, Знач ЗначениеПрефиксы = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если ЗначениеПрефиксы = Неопределено Тогда
		Префиксы = Новый Массив;
		Префиксы.Добавить("Оформлено");
		Префиксы.Добавить("КОформлению");
	Иначе
		Если ТипЗнч(ЗначениеПрефиксы) = Тип("Строка") Тогда
			Префиксы = Новый Массив();
			Префиксы.Добавить(ЗначениеПрефиксы);
		Иначе
			Префиксы = ЗначениеПрефиксы;
		КонецЕсли;
	КонецЕсли;
	
	Для Каждого Значение Из Префиксы Цикл
		Форма.Элементы[Значение + "Организация"].СписокВыбора.Очистить();
		Форма.Элементы[Значение + "Организации"].СписокВыбора.Очистить();
	КонецЦикла;
	
	ПараметрыОтбора = Новый Структура("КлючОбъекта", "ОбщаяФорма.к2ФормаВыбораСпискаОрганизацийИСМП");
	
	Выборка = ХранилищеНастроекДанныхФорм.Выбрать(ПараметрыОтбора);
	
	Пока Выборка.Следующий() Цикл
		
		Данные = Новый Массив;
		Значение = Выборка.Настройки.Получить("ТаблицаОрганизации");
		Если Значение <> Неопределено Тогда
			
			ПараметрыОтбора = Новый Структура;
			ПараметрыОтбора.Вставить("Выбрана", Истина);
			НайденныеСтроки = Значение.НайтиСтроки(ПараметрыОтбора);
			
			Для Каждого СтрокаТЧ Из НайденныеСтроки Цикл
				Данные.Добавить(СтрокаТЧ.Организация);
			КонецЦикла;
			
			Для Каждого Значение Из Префиксы Цикл
				ЭлементОтбораОрганизация = Форма.Элементы.Найти(Значение + "Организация");
				Если ЭлементОтбораОрганизация <> Неопределено Тогда
					ЭлементОтбораОрганизация.СписокВыбора.Добавить(Данные, СтрСоединить(Данные, "; "));
				КонецЕсли;
		
				ЭлементОтбораОрганизации = Форма.Элементы.Найти(Значение + "Организации");
				Если ЭлементОтбораОрганизации <> Неопределено Тогда
					ЭлементОтбораОрганизации.СписокВыбора.Добавить(Данные, СтрСоединить(Данные, "; "));
				КонецЕсли;
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

// Дорабатывает форму списка документов:
//   * Добавляет необходимые отборы
//   * Скрывает списки к оформлению при необходимости.
//
// Параметры:
//   Форма     - ФормаКлиентскогоПриложения - форма списка документов ИСМП.
//   Настройки - Структура        - (См. ИнтеграцияИС.НастройкиФормыСпискаДокументов).
//             - Неопределено     - будут использованы значения по умолчанию описанные здесь.
//
Процедура ПриСозданииНаСервереФормыСпискаДокументов(Форма, Настройки = Неопределено) Экспорт
	
	Если Настройки = Неопределено Тогда
		Настройки = к2ИнтеграцияИС.НастройкиФормыСпискаДокументов();
		Настройки.ТипыКОформлению = Метаданные.ОпределяемыеТипы.к2ДокументыИСМППоддерживающиеСтатусыОформления;
		Настройки.ТипыКОбмену     = Метаданные.ОпределяемыеТипы.к2ДокументыИСМП;
	КонецЕсли;
	к2ИнтеграцияИС.ПриСозданииНаСервереФормыСпискаДокументов(Форма, Настройки);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ДобавитьОбщиеНастройкиВстраивания(Форма, ПараметрыИнтеграции)
	
	ОбщиеНастройки = к2СобытияФормИС.ОбщиеПараметрыИнтеграции("к2СобытияФормИСМП");
	ОбщиеНастройки.Вставить("ВидыПродукции", Новый Массив);
	
	ВидыПродукцииИСМП = к2ИнтеграцияИСКлиентСервер.ВидыПродукцииИСМП();
	Для Каждого ВидПродукции Из ВидыПродукцииИСМП Цикл
		Если к2ИнтеграцияИСМПКлиентСерверПовтИсп.ВестиУчетМаркируемойПродукции(ВидПродукции) Тогда
			ОбщиеНастройки.ВидыПродукции.Добавить(ВидПродукции);
		КонецЕсли;
	КонецЦикла;
	
	ПараметрыИнтеграции.Вставить("ИСМП", ОбщиеНастройки);
	
КонецПроцедуры

// Встраивает реквизит - форматированную строку перехода к ИСМП в прикладные формы
// 
// Параметры:
//   Форма                - ФормаКлиентскогоПриложения - форма в которую происходит встраивание
//   ПараметрыИнтеграции  - Структура        - (См. ПараметрыИнтеграцииИСМП)
//   ДобавляемыеРеквизиты - Массив           - массив реквизитов формы к добавлению
Процедура ДобавитьРеквизитТекстСостояниеИСМП(Форма, ПараметрыИнтеграции, ДобавляемыеРеквизиты)
	
	ПараметрыИнтеграцииИСМП = ПараметрыИнтеграцииГиперссылкиИСМП(Форма);
	
	Если ЗначениеЗаполнено(ПараметрыИнтеграцииИСМП.ИмяРеквизитаФормы) Тогда
		ПараметрыИнтеграции.Вставить("ИСМП.ДокументОснование", ПараметрыИнтеграцииИСМП);
		Реквизит = Новый РеквизитФормы(
			ПараметрыИнтеграцииИСМП.ИмяРеквизитаФормы,
			Новый ОписаниеТипов("ФорматированнаяСтрока"),,
			ПараметрыИнтеграцииИСМП.Заголовок);
		ДобавляемыеРеквизиты.Добавить(Реквизит);
	КонецЕсли;
	
КонецПроцедуры

Процедура ДобавитьЭлементыПроверкиПодбораИСМП(Форма, ПараметрыИнтеграции, ДобавляемыеРеквизиты)
	
	Для Каждого ВидПродукции Из ПараметрыИнтеграции.Получить("ИСМП").ВидыПродукции Цикл
		к2ПроверкаИПодборПродукцииИСМП.МодификацияРеквизитовФормы(Форма, ПараметрыИнтеграции, ДобавляемыеРеквизиты, ВидПродукции);
	КонецЦикла;
	
КонецПроцедуры

// Возвращает структуру, заполненную значениями по умолчанию, используемую для интеграции реквизитов ИС МП
//   в прикладные формы конфигураци - потребителя библиотеки ГосИС. Если передана форма - сразу заполняет ее
//   специфику в переопределяемом модуле.
//
// Параметры:
//   Форма - ФормаКлиентскогоПриложения, Неопределено - форма для которой возвращаются параметры интеграции
//
// ВозвращаемоеЗначение:
//   Структура - (См. СобытияФормИС.ПараметрыИнтеграцииДляДокументаОснования).
//
Функция ПараметрыИнтеграцииГиперссылкиИСМП(Форма = Неопределено)
	
	ПараметрыНадписи = к2СобытияФормИС.ПараметрыИнтеграцииДляДокументаОснования();
	ПараметрыНадписи.Вставить("Ключ",             "ЗаполнениеТекстаДокументаИСМП");
	ПараметрыНадписи.Вставить("МодульЗаполнения", "к2СобытияФормИСМП");
	
	Возврат ПараметрыНадписи;
	
КонецФункции

Процедура НастроитьВидПродукцииПриСозданииНаСервере(Форма, ДоступныеВидыПродукции) Экспорт
	
	Если Не ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(Форма.Элементы, "ВидПродукции") Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого ВидПродукции Из ДоступныеВидыПродукции Цикл
		
		Если к2ИнтеграцияИСПовтИсп.ЭтоПродукцияИСМП(ВидПродукции)
			И к2ИнтеграцияИСМПКлиентСерверПовтИсп.ВестиУчетМаркируемойПродукции(ВидПродукции) Тогда
			Форма.Элементы.ВидПродукции.СписокВыбора.Добавить(ВидПродукции);
		КонецЕсли;
		
	КонецЦикла;
	
	Если ЗначениеЗаполнено(Форма.Объект.ВидПродукции)
		И Форма.Элементы.ВидПродукции.СписокВыбора.НайтиПоЗначению(Форма.Объект.ВидПродукции) = Неопределено Тогда
		Форма.Элементы.ВидПродукции.СписокВыбора.Добавить(Форма.Объект.ВидПродукции);
	КонецЕсли;
	
	Если Форма.Элементы.ВидПродукции.СписокВыбора.Количество() = 1 Тогда
		Форма.Объект.ВидПродукции = Форма.Элементы.ВидПродукции.СписокВыбора[0].Значение;
		Форма.Элементы.ВидПродукции.Видимость = Ложь;
	КонецЕсли;
	
	Форма.Элементы.ВидПродукции.СписокВыбора.СортироватьПоПредставлению();
	
КонецПроцедуры

#КонецОбласти