 
#Область ПрограммныйИнтерфейс

// Процедура, вызываемая из одноименного события таблицы формы Уточнения по доставке.
//
// Параметры:
//	Элемент - ТаблицаФормы - Таблица формы, в которой происходит добавление строки.
//	НоваяСтрока - Булево - Признак редактирования новой строки. Имеет значение Истина, если строка была добавлена или скопирована.
//	Копирование - Булево - Определяет режим копирования. Если установлено Истина, то происходит копирование строки.
//
Процедура УточненияПоДоставкеПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование) Экспорт
	
	ТекущиеДанные = Элемент.ТекущиеДанные;
	Если НоваяСтрока И Не Копирование Тогда
		
		ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница,Суббота,Воскресенье";
		УстановитьЗначениеРеквизитам(ТекущиеДанные, ИменаРеквизитов);
		
	КонецЕсли;
	
КонецПроцедуры

// Процедура, вызываемая из одноименной команды формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура ВыбратьВсеДни(Форма) Экспорт
	
	ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница,Суббота,Воскресенье";	
	УстановитьЗначениеРеквизитам(Форма, ИменаРеквизитов);
	
КонецПроцедуры

// Процедура, вызываемая из одноименной команды формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура ВыбратьБудни(Форма) Экспорт
	
	ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница";
	УстановитьЗначениеРеквизитам(Форма, ИменаРеквизитов);
	
	ИменаРеквизитов = "Суббота,Воскресенье";
	УстановитьЗначениеРеквизитам(Форма, ИменаРеквизитов, Ложь);
	
КонецПроцедуры

// Процедура, вызываемая из одноименной команды формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура ОчиститьДни(Форма) Экспорт
	
	ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница,Суббота,Воскресенье";
	УстановитьЗначениеРеквизитам(Форма, ИменаРеквизитов, Ложь);
	
КонецПроцедуры

// Процедура, вызываемая из одноименной команды формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура УточненияВыбратьВсеДни(Форма) Экспорт
	
	ТекущиеДанные = Форма.Элементы.УточненияПоДоставке.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница,Суббота,Воскресенье";
	УстановитьЗначениеРеквизитам(ТекущиеДанные, ИменаРеквизитов);
	
КонецПроцедуры

// Процедура, вызываемая из одноименной команды формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура УточненияВыбратьБудни(Форма) Экспорт
	
	ТекущиеДанные = Форма.Элементы.УточненияПоДоставке.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница";
	УстановитьЗначениеРеквизитам(ТекущиеДанные, ИменаРеквизитов);
	
	ИменаРеквизитов = "Суббота,Воскресенье";
	УстановитьЗначениеРеквизитам(ТекущиеДанные, ИменаРеквизитов, Ложь);
	
КонецПроцедуры

// Процедура, вызываемая из одноименной команды формы.
//
// Параметры:
//  Форма - ФормаКлиентскогоПриложения - форма, из обработчика события которой происходит вызов процедуры.
//
Процедура УточненияОчиститьДни(Форма) Экспорт
	
	ТекущиеДанные = Форма.Элементы.УточненияПоДоставке.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ИменаРеквизитов = "Понедельник,Вторник,Среда,Четверг,Пятница,Суббота,Воскресенье";
	УстановитьЗначениеРеквизитам(ТекущиеДанные, ИменаРеквизитов, Ложь);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура УстановитьЗначениеРеквизитам(Родитель, ИменаРеквизитов, Значение = Истина)

	МассивИменРеквизитов = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(ИменаРеквизитов, , Истина, Истина);
	
	Для Каждого ИмяРеквизита Из МассивИменРеквизитов Цикл
		
		Родитель[ИмяРеквизита] = Значение;	
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти