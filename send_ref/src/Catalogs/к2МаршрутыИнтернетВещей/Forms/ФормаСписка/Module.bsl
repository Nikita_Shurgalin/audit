
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ФормаСписка_ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаЗагрузитьИзВнешнегоИсточника(Команда)
	
	ЗагрузитьИзВнешнегоИсточникаНаСервере();
	
	Элементы.Список.Обновить();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗагрузитьИзВнешнегоИсточникаНаСервере()
	
	ТаблицаМаршрутов = МаршрутыВнешнегоИсточника();
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Маршруты", ТаблицаМаршрутов);
	#Область ТекстЗапроса
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Маршруты.Идентификатор,
	|	Маршруты.Наименование,
	|	Маршруты.МестоХраненияОтправитель,
	|	Маршруты.МестоХраненияПолучатель
	|ПОМЕСТИТЬ Маршруты
	|ИЗ
	|	&Маршруты КАК Маршруты
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	МестаХранения.ИдентификаторSCADA,
	|	МестаХранения.Склад
	|ПОМЕСТИТЬ МестаХранения
	|ИЗ
	|	Справочник.к2МестаХраненияИнтернетВещей КАК МестаХранения
	|ГДЕ
	|	МестаХранения.ИдентификаторSCADA В
	|		(ВЫБРАТЬ
	|			Маршруты.МестоХраненияОтправитель
	|		ИЗ
	|			Маршруты)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	МестаХранения.ИдентификаторSCADA,
	|	МестаХранения.Склад
	|ИЗ
	|	Справочник.к2МестаХраненияИнтернетВещей КАК МестаХранения
	|ГДЕ
	|	МестаХранения.ИдентификаторSCADA В
	|		(ВЫБРАТЬ
	|			Маршруты.МестоХраненияПолучатель
	|		ИЗ
	|			Маршруты)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Маршруты.Идентификатор,
	|	Маршруты.Наименование,
	|	Маршруты.МестоХраненияОтправитель,
	|	Маршруты.МестоХраненияПолучатель,
	|	ЕСТЬNULL(МестаХраненияОтправитель.Склад, ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка)) КАК СкладОтправитель,
	|	ЕСТЬNULL(МестаХраненияПолучатель.Склад, ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка)) КАК СкладПолучатель
	|ИЗ
	|	Маршруты
	|		ЛЕВОЕ СОЕДИНЕНИЕ МестаХранения КАК МестаХраненияОтправитель
	|		ПО Маршруты.МестоХраненияОтправитель = МестаХраненияОтправитель.ИдентификаторSCADA
	|		ЛЕВОЕ СОЕДИНЕНИЕ МестаХранения КАК МестаХраненияПолучатель
	|		ПО Маршруты.МестоХраненияПолучатель = МестаХраненияПолучатель.ИдентификаторSCADA";
	#КонецОбласти
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		Если Не СоответствиеСкладовУстановлено(Выборка) Тогда
			Продолжить;
		КонецЕсли;
		
		НайтиСоздатьМаршрут(Выборка);
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция МаршрутыВнешнегоИсточника()
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Маршруты.Идентификатор,
	|	Маршруты.Наименование,
	|	Маршруты.МестоХраненияОтправитель.Идентификатор КАК МестоХраненияОтправитель,
	|	Маршруты.МестоХраненияПолучатель.Идентификатор КАК МестоХраненияПолучатель
	|ИЗ
	|	ВнешнийИсточникДанных.ПромышленныйИнтернетВещей.Таблица.Маршруты КАК Маршруты";
	#КонецОбласти
	
	Таблица = Запрос.Выполнить().Выгрузить();
	
	Возврат Таблица;
	
КонецФункции

&НаСервере
Функция СоответствиеСкладовУстановлено(Выборка)
	
	ЕстьОшибки = Ложь;
	
	ШаблонСообщения = НСтр("ru = 'Маршрут ""%1"" (%2) не записан.
							| Для места хранения с идентификатором ""%3"" не установлен склад.'");
	
	Если Не ЗначениеЗаполнено(Выборка.СкладОтправитель) Тогда
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения
			, Выборка.Наименование
			, Выборка.Идентификатор
			, Выборка.МестоХраненияОтправитель);
			
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , ЕстьОшибки);
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Выборка.СкладПолучатель) Тогда
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения
			, Выборка.Наименование
			, Выборка.Идентификатор
			, Выборка.МестоХраненияПолучатель);
			
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , ЕстьОшибки);
		
	КонецЕсли;
	
	Возврат Не ЕстьОшибки;
	
КонецФункции

&НаСервере
Функция НайтиСоздатьМаршрут(Выборка)
	
	Маршрут = Справочники.к2МаршрутыИнтернетВещей.НайтиПоРеквизиту("ИдентификаторSCADA", Выборка.Идентификатор);
	
	Если ЗначениеЗаполнено(Маршрут) Тогда
		СправочникОбъект = Маршрут.ПолучитьОбъект();
	Иначе
		СправочникОбъект = Справочники.к2МаршрутыИнтернетВещей.СоздатьЭлемент();
	КонецЕсли;
	
	СправочникОбъект.ИдентификаторSCADA = Выборка.Идентификатор;
	СправочникОбъект.Наименование = Выборка.Наименование;
	СправочникОбъект.СкладОтправитель = Выборка.СкладОтправитель;
	СправочникОбъект.СкладПолучатель = Выборка.СкладПолучатель;
	
	к2ПромышленныйИнтернетВещей.ЗаписатьСправочник(СправочникОбъект);
	
	Возврат СправочникОбъект.Ссылка;
	
КонецФункции

#КонецОбласти
