
Процедура ЗагрузитьПравилаОбмена(ОбработкаОбъект, птдПравилаОбмена)
	
	// Производим загрузку правил обмена
	
	ИмяВременногоФайлаПравилОбмена = ПолучитьИмяВременногоФайла(упИК.Расширение__ФайлОбмена());
	
	птдПравилаОбмена.Записать(ИмяВременногоФайлаПравилОбмена);
	
	ОбработкаОбъект.ИмяФайлаПравилОбмена = ИмяВременногоФайлаПравилОбмена;
	ОбработкаОбъект.ЗагрузитьПравилаОбмена();
	
	УдалитьФайлы(ИмяВременногоФайлаПравилОбмена);  // Удаляем временный файл правил
	
КонецПроцедуры

Функция ВыгрузитьЭлементы( пМассивСсылок, пУИД, пИмяМенеджераСПравилами ) Экспорт
	
	имяЭлемента = пИмяМенеджераСПравилами; //после разделения сюда упадет как раз вторая часть
	имяОбъектаМетаданных = упКоллекцииКлиентСервер.РазделитьСтроку( имяЭлемента, "." );
	
	Если имяОбъектаМетаданных = "Справочники" Тогда
		
		правилаОбмена = Справочники[имяЭлемента].ПолучитьМакет( "ПравилаОбмена" );
		ДополнитьПланОбмена(правилаОбмена);
		Возврат ВыгрузитьСписокОбъектов( правилаОбмена, пМассивСсылок, пУИД);
		
	Иначе
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( упИК.т__Обмен_НеподдерживаемоеИмяМенеджера( пИмяМенеджераСПравилами ) );
		Возврат "";
		
	КонецЕсли;
	
КонецФункции

Процедура ЗагрузитьЭлементы( пАдресФайла, пИмяПравил, пЗагружаемыйТип, пСозданныеЭлементы = Неопределено) Экспорт
	
	ИмяФайлаОбмена = ПолучитьИмяВременногоФайла( упИК.Расширение__ФайлОбмена() );
	
	двоичныеДанные = ПолучитьИзВременногоХранилища( пАдресФайла );
	двоичныеДанные.Записать( ИмяФайлаОбмена );
	
	УниверсальныйОбменДаннымиXML = Обработки.УниверсальныйОбменДаннымиXML.Создать();
	
	УниверсальныйОбменДаннымиXML.РежимОбмена = "Загрузка";
	УниверсальныйОбменДаннымиXML.ИмяФайлаОбмена = ИмяФайлаОбмена;
	УниверсальныйОбменДаннымиXML.НеВыводитьНикакихИнформационныхСообщенийПользователю = Истина;
	
	УниверсальныйОбменДаннымиXML.ОткрытьФайлЗагрузки(Истина);
	
	Если СтрНайти( УниверсальныйОбменДаннымиXML.Конвертация.Наименование, пИмяПравил ) > 0 Тогда
		
		УниверсальныйОбменДаннымиXML.ВыполнитьЗагрузку();
		
	Иначе
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователюВОкно( упИК.т__Обмен_ОшибочноеИмяПравил( УниверсальныйОбменДаннымиXML.Конвертация.Наименование , пИмяПравил ) );
		
	КонецЕсли;
	
	пСозданныеЭлементы = Новый Массив;
	
	Для Каждого цЭлемент Из УниверсальныйОбменДаннымиXML.ЗагруженныеОбъекты Цикл
		
		текСсылка = цЭлемент.Значение.СсылкаНаОбъект;
		
		Если упКэш.РежимОтладки() Тогда
			глСозданНовыйОбъект( текСсылка );
		КонецЕсли;
		
		Если ТипЗнч( текСсылка ) = пЗагружаемыйТип Тогда
			
			пСозданныеЭлементы.Добавить( текСсылка );
			
		КонецЕсли;
		
	КонецЦикла;
	
	УдалитьФайлы(имяФайлаОбмена);
	
КонецПроцедуры


Функция ВыгрузитьСписокОбъектов( птдПравилаОбмена, пМассивВыгружаемыхОбъектов, пУИД)
	
	УстановитьПривилегированныйРежим( Истина );
	
	УниверсальнаяВыгрузкаДанных = Обработки.УниверсальныйОбменДаннымиXML.Создать();
	
	// Загрузка правил обмена
	ЗагрузитьПравилаОбмена(УниверсальнаяВыгрузкаДанных, птдПравилаОбмена);
	
	Если УниверсальнаяВыгрузкаДанных.ФлагОшибки Тогда
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( упИК.т__Обмен_ОшибкаПриЗагрузкеПравил() );
		Возврат "";
	КонецЕсли;
	
	УниверсальнаяВыгрузкаДанных.Параметры.Вставить( "МассивСсылок" , пМассивВыгружаемыхОбъектов );
	
	имяФайлаОбмена = ПолучитьИмяВременногоФайла(упИК.Расширение__ФайлОбмена());
	
	УниверсальнаяВыгрузкаДанных.ИмяФайлаОбмена = имяФайлаОбмена;
	УниверсальнаяВыгрузкаДанных.НеВыводитьНикакихИнформационныхСообщенийПользователю = Истина;
	
	УниверсальнаяВыгрузкаДанных.ВыполнитьВыгрузку();
	
	Если УниверсальнаяВыгрузкаДанных.ФлагОшибки Тогда
		
		упОбщегоНазначенияКлиентСервер.СообщитьПользователю( упИК.т__Обмен_ОшибкаПриПереносеДанных() );
		Возврат "";
		
	КонецЕсли;
	
	двоичныеДанныеФайла = Новый ДвоичныеДанные( имяФайлаОбмена );
	
	УдалитьФайлы(имяФайлаОбмена);
	
	Возврат ПоместитьВоВременноеХранилище( двоичныеДанныеФайла, пУИД);
	
КонецФункции



Процедура ДополнитьПланОбмена(ПравилаОбмена)
	
	ДополнитьПланОбменаПравиламиКонвертицииПеречислений(ПравилаОбмена);
	ДополнитьПланОбменаПравиламиКонвертицииСправочников(ПравилаОбмена);
	
КонецПроцедуры

Процедура ДополнитьПланОбменаПравиламиКонвертицииПеречислений(ПравилаОбмена)
	
	ТекстПравила = ПравилаОбмена.ПолучитьТекст();
	
	ТекстПравилПеречислений = ТекстПравилПеречислений();
	
	Если ЗначениеЗаполнено(ТекстПравилПеречислений) Тогда
		
		ТекстДополнения =
		"<Группа>
		|	<Код>Перечисления</Код>
		|	<Наименование>Перечисления</Наименование>
		|	<Порядок>101</Порядок>
		|	%1
		|</Группа>";
		ТекстДополнения = стрЗаполнить(ТекстДополнения, ТекстПравилПеречислений);
		
		ТекстПравила = СтрЗаменить(ТекстПравила,
			"</ПравилаКонвертацииОбъектов>",
			ТекстДополнения + Символы.ПС + "</ПравилаКонвертацииОбъектов>");
		
	КонецЕсли;
	
	ПравилаОбмена.УстановитьТекст(ТекстПравила);
	
КонецПроцедуры

Функция ТекстПравилПеречислений()
	
	МассивТекстовПравил = Новый Массив;
	
	
	Порядок = 1;
	
	Для Каждого ПеречислениеМетаданные Из Метаданные.Перечисления Цикл
		
		ТекстПеречисления =
		"<Правило>
		|	<Код>%1</Код>
		|	<Наименование>Перечисление: %2</Наименование>
		|	<Порядок>%3</Порядок>
		|	<Источник>ПеречислениеСсылка.%1</Источник>
		|	<Приемник>ПеречислениеСсылка.%1</Приемник>
		|	<Свойства/>
		|	<Значения>
		|		%4
		|	</Значения>
		|</Правило>";
		
		
		МассивТекстовЗначений = Новый Массив;
		Код = 1;
		
		Для Каждого ЗначениеМетаданные Из ПеречислениеМетаданные.ЗначенияПеречисления Цикл
			
			ТекстЗначения =
			"		<Значение>
			|			<Код>%1</Код>
			|			<Наименование>%2 --&gt; %2</Наименование>
			|			<Порядок>%3</Порядок>
			|			<Источник>%2</Источник>
			|			<Приемник>%2</Приемник>
			|		</Значение>";
			ТекстЗначения = стрЗаполнить(ТекстЗначения,
				Код,
				ЗначениеМетаданные.Имя,
				Код * 50);
			
			МассивТекстовЗначений.Добавить(ТекстЗначения);
			
			Код = Код + 1;
			
		КонецЦикла;
		
		ТекстЗначений = стрСобрать(МассивТекстовЗначений, Символы.ПС);
		
		
		ТекстПеречисления = стрЗаполнить(ТекстПеречисления,
			ПеречислениеМетаданные.Имя,
			ПеречислениеМетаданные.Синоним,
			Порядок * 50,
			ТекстЗначений);
		
		МассивТекстовПравил.Добавить(ТекстПеречисления);
		
		
		Порядок = Порядок + 1;
		
	КонецЦикла;
	
	Возврат стрСобрать(МассивТекстовПравил, Символы.ПС);
	
КонецФункции

Процедура ДополнитьПланОбменаПравиламиКонвертицииСправочников(ПравилаОбмена)
	
	ТекстПравила = ПравилаОбмена.ПолучитьТекст();
	
	МассивИменСправочниковИсключения = МассивИменСправочниковИспользованныхВПравиле(ТекстПравила);
	ТекстПравилСправочников = ТекстПравилСправочников(МассивИменСправочниковИсключения);
	
	Если ЗначениеЗаполнено(ТекстПравилСправочников) Тогда
		
		ТекстДополнения =
		"<Группа>
		|	<Код>Справочники</Код>
		|	<Наименование>Справочники</Наименование>
		|	<Порядок>201</Порядок>
		|	%1
		|</Группа>";
		ТекстДополнения = стрЗаполнить(ТекстДополнения, ТекстПравилСправочников);
		
		ТекстПравила = СтрЗаменить(ТекстПравила,
			"</ПравилаКонвертацииОбъектов>",
			ТекстДополнения + Символы.ПС + "</ПравилаКонвертацииОбъектов>");
		
	КонецЕсли;
	
	ПравилаОбмена.УстановитьТекст(ТекстПравила);
	
КонецПроцедуры

Функция МассивИменСправочниковИспользованныхВПравиле(Знач ТекстПравила)
	
	Массив = Новый Массив;
	
	Пока Истина Цикл
		
		СтрокаПоиска = "<Источник>СправочникСсылка.";
		ПозицияПоиска = СтрНайти(ТекстПравила, СтрокаПоиска);
		Если ПозицияПоиска = 0 Тогда
			Прервать;
		КонецЕсли;
		
		ТекстПравила = Сред(ТекстПравила, ПозицияПоиска + СтрДлина(СтрокаПоиска));
		
		ПозицияПоиска = СтрНайти(ТекстПравила, "</Источник>");
		Если Не ПозицияПоиска = 0 Тогда
			Имя = Лев(ТекстПравила, ПозицияПоиска - 1);
			Массив.Добавить(Имя);
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Массив;
	
КонецФункции

Функция ТекстПравилСправочников(МассивИменСправочниковИсключения)
	
	МассивТекстовПравил = Новый Массив;
	
	
	Порядок = 1;
	
	Для Каждого СправочникМетаданные Из МетаданныеСправочники() Цикл
		
		Если Не МассивИменСправочниковИсключения.Найти(СправочникМетаданные.Имя) = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		
		ТекстСправочника =
		"<Правило>
		|	<Код>%1</Код>
		|	<Наименование>Справочник: %2</Наименование>
		|	<Порядок>%3</Порядок>
		|	<НеЗамещать>true</НеЗамещать>
		|	<СинхронизироватьПоИдентификатору>true</СинхронизироватьПоИдентификатору>
		|	<ПродолжитьПоискПоПолямПоискаЕслиПоИдентификаторуНеНашли>true</ПродолжитьПоискПоПолямПоискаЕслиПоИдентификаторуНеНашли>
		|	<НеСоздаватьЕслиНеНайден>true</НеСоздаватьЕслиНеНайден>
		|	<Источник>СправочникСсылка.%1</Источник>
		|	<Приемник>СправочникСсылка.%1</Приемник>
		|	<Свойства>
		|		%4
		|	</Свойства>
		|</Правило>";
		
		Если СправочникМетаданные.ДлинаНаименования = 0 Тогда
			
			ТекстСвойств =
			"		<Свойство Поиск=""true"">
			|			<Код>2</Код>
			|			<Наименование>Код --&gt; Код</Наименование>
			|			<Порядок>100</Порядок>
			|			<Источник Имя=""Код"" Вид=""Свойство"" Тип=""%1""/>
			|			<Приемник Имя=""Код"" Вид=""Свойство"" Тип=""%1""/>
			|		</Свойство>";
			
		ИначеЕсли СправочникМетаданные.ДлинаКода = 0 Тогда
			
			ТекстСвойств =
			"		<Свойство Поиск=""true"">
			|			<Код>3</Код>
			|			<Наименование>Наименование --&gt; Наименование</Наименование>
			|			<Порядок>150</Порядок>
			|			<Источник Имя=""Наименование"" Вид=""Свойство"" Тип=""Строка""/>
			|			<Приемник Имя=""Наименование"" Вид=""Свойство"" Тип=""Строка""/>
			|		</Свойство>";
			
		Иначе
			
			ТекстСвойств =
			"		<Свойство>
			|			<Код>2</Код>
			|			<Наименование>Код --&gt; Код</Наименование>
			|			<Порядок>100</Порядок>
			|			<Источник Имя=""Код"" Вид=""Свойство"" Тип=""%1""/>
			|			<Приемник Имя=""Код"" Вид=""Свойство"" Тип=""%1""/>
			|		</Свойство>
			|		<Свойство Поиск=""true"">
			|			<Код>3</Код>
			|			<Наименование>Наименование --&gt; Наименование</Наименование>
			|			<Порядок>150</Порядок>
			|			<Источник Имя=""Наименование"" Вид=""Свойство"" Тип=""Строка""/>
			|			<Приемник Имя=""Наименование"" Вид=""Свойство"" Тип=""Строка""/>
			|		</Свойство>";
			
		КонецЕсли;
		
		Если Не СправочникМетаданные.Владельцы.Количество() = 0 Тогда
			
			ТекстСвойств =
			"		<Свойство Поиск=""true"">
			|			<Код>1</Код>
			|			<Наименование>Владелец --&gt; Владелец</Наименование>
			|			<Порядок>50</Порядок>
			|			<Источник Имя=""Владелец"" Вид=""Свойство""/>
			|			<Приемник Имя=""Владелец"" Вид=""Свойство""/>
			|		</Свойство>
			|"
			+ ТекстСвойств;
			
		КонецЕсли;
		
		ТекстСвойств = стрЗаполнить(ТекстСвойств, Строка(СправочникМетаданные.ТипКода));
		
		ТекстСправочника = стрЗаполнить(ТекстСправочника,
			СправочникМетаданные.Имя,
			СправочникМетаданные.Синоним,
			Порядок * 50,
			ТекстСвойств);
		
		МассивТекстовПравил.Добавить(ТекстСправочника);
		
		
		Порядок = Порядок + 1;
		
	КонецЦикла;
	
	Возврат стрСобрать(МассивТекстовПравил, Символы.ПС);
	
КонецФункции

Функция МетаданныеСправочники()
	
	МетаданныеСправочники = Новый Массив;
	
	Для Каждого СправочникМетаданные Из Метаданные.Справочники Цикл
		
		Если СправочникМетаданные.Владельцы.Количество() = 0 Тогда
			МетаданныеСправочники.Добавить(СправочникМетаданные);
		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого СправочникМетаданные Из Метаданные.Справочники Цикл
		
		Если Не СправочникМетаданные.Владельцы.Количество() = 0 Тогда
			МетаданныеСправочники.Добавить(СправочникМетаданные);
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат МетаданныеСправочники;
	
КонецФункции
