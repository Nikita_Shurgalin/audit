#Область ПрограммныйИнтерфейс

Функция ПолучитьГуидыПоОрганизации(Организация) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	к2ГуидыОрганизацийДляОборудования_GPS.ГуидПолучателя_GPS КАК ГуидПолучателя,
		|	к2ГуидыОрганизацийДляОборудования_GPS.ГуидОтправителя_GPS КАК ГуидОтправителя
		|ИЗ
		|	РегистрСведений.к2ГуидыОрганизацийДляОборудования_GPS КАК к2ГуидыОрганизацийДляОборудования_GPS
		|ГДЕ
		|	к2ГуидыОрганизацийДляОборудования_GPS.Организация = &Организация";
	
	Запрос.УстановитьПараметр("Организация", Организация);
	
	СтруктураОтвета = Новый Структура;
	СтруктураОтвета.Вставить("ГуидПолучателя", Неопределено);
	СтруктураОтвета.Вставить("ГуидОтправителя", Неопределено);
	
	РезультатЗапроса = к2ОбщегоНазначения.СтруктураПервойВыборкиРезультатаЗапроса(Запрос.Выполнить());
	СтруктураОтвета.ГуидПолучателя = РезультатЗапроса.ГуидПолучателя;
	СтруктураОтвета.ГуидОтправителя = РезультатЗапроса.ГуидОтправителя;

	Возврат СтруктураОтвета;

КонецФункции

#КонецОбласти