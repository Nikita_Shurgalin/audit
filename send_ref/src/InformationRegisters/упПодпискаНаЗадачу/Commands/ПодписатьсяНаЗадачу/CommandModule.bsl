
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ДобавитьЗадачи( ПараметрКоманды );
	
КонецПроцедуры

Процедура ДобавитьЗадачи( пМассивЗадач )
	
	Для каждого цЗадача Из пМассивЗадач Цикл
		
		РегистрыСведений.упПодпискаНаЗадачу.Добавить( глТекущийПользователь(), цЗадача );
		
	КонецЦикла;
	
КонецПроцедуры	//ДобавитьЗадачи

