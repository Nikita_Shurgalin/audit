
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("ИсключитьИзВыбораСпецификацию") Тогда
		ИсключитьИзВыбораСпецификацию = Параметры.ИсключитьИзВыбораСпецификацию;
	КонецЕсли;
	
	Если Параметры.Свойство("ПоказыватьСпецификацииПоФиксированнойСтоимости") Тогда
		ПоказыватьСпецификацииПоФиксированнойСтоимости = Параметры.ПоказыватьСпецификацииПоФиксированнойСтоимости;
	КонецЕсли;
	
	Если Параметры.Свойство("СпецификацииГдеИзделиеЯвляетсяОсновным") Тогда
		СпецификацииГдеИзделиеЯвляетсяОсновным = Параметры.СпецификацииГдеИзделиеЯвляетсяОсновным;
	КонецЕсли;
	
	Если НЕ Параметры.Свойство("Заголовок", Заголовок) Тогда
		Заголовок = НСтр("ru = 'Ресурсные спецификации'");
	КонецЕсли;
	
	Если Параметры.Свойство("Номенклатура") Тогда
		
		Номенклатура = Параметры.Номенклатура;
		
		Параметры.Свойство("Характеристика",     Характеристика);
		Параметры.Свойство("НачалоПроизводства", НачалоПроизводства);
		Параметры.Свойство("ВсеСпецификации",    ВсеСпецификации);
		
		Если Параметры.Свойство("ДоступныеСтатусы") Тогда
			ДоступныеСтатусы.ЗагрузитьЗначения(Параметры.ДоступныеСтатусы);
		Иначе
			ДоступныеСтатусы.Добавить(Перечисления.к2СтатусыСпецификаций.Действует);
		КонецЕсли;
		
		СписокСпецификацийДляЗаданнойНоменклатуры();
		
	ИначеЕсли Параметры.Свойство("СписокИзделий") Тогда
		
		СписокСпецификацийИзделий(Параметры.СписокИзделий);
		
	ИначеЕсли Параметры.Свойство("АдресТаблицыПродукции") Тогда
		
		НастроитьФормуПоТаблицеПродукции();
		
	Иначе
		
		ВызватьИсключение НСтр("ru = 'Для открытия формы необходимо передать параметры.'");
		
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список,
		"НачалоПроизводства",
		НачалоПроизводства);
		
	Элементы.Статус.Видимость = (ВсеСпецификации ИЛИ ДоступныеСтатусы.Количество() > 1);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_к2ОсновныеСпецификации" Тогда
		УстановитьСпецификацияПоУмолчанию();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВыбратьИНазначитьОсновной(Команда)
	
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ПроверитьНазначитьСпецификациюОсновной(ТекущиеДанные.Ссылка) Тогда
		
		ОповеститьОВыборе(ТекущиеДанные.Ссылка);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НазначитьОсновной(Команда)
	
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ПроверитьНазначитьСпецификациюОсновной(ТекущиеДанные.Ссылка);
	
КонецПроцедуры

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда, НавигационнаяСсылка = Неопределено, СтандартнаяОбработка = Неопределено)
	
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ВыборкаСпецификаций

&НаСервере
Процедура СписокСпецификацийДляЗаданнойНоменклатуры()

	Запрос = Новый Запрос(
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	Спецификации.Ссылка
	|ИЗ
	|	(ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		ВыходныеИзделияСпецификации.Ссылка КАК Ссылка
	|	ИЗ
	|		Справочник.к2РесурсныеСпецификации.ВыходныеИзделия КАК ВыходныеИзделияСпецификации
	|	ГДЕ
	|		НЕ ВыходныеИзделияСпецификации.Ссылка.ПометкаУдаления
	|		И (ВыходныеИзделияСпецификации.Ссылка.Статус В (&ДоступныеСтатусы)
	|				ИЛИ &ВсеСпецификации)
	|		И ВыходныеИзделияСпецификации.Номенклатура = &Номенклатура
	|		И (ВыходныеИзделияСпецификации.Характеристика = &Характеристика
	|				ИЛИ ВыходныеИзделияСпецификации.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
	|				ИЛИ &Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|		И (&НачалоПроизводства = ДАТАВРЕМЯ(1, 1, 1)
	|				ИЛИ (ВыходныеИзделияСпецификации.Ссылка.НачалоДействия = ДАТАВРЕМЯ(1, 1, 1)
	|					ИЛИ ВыходныеИзделияСпецификации.Ссылка.НачалоДействия <= &НачалоПроизводства)
	|					И (ВыходныеИзделияСпецификации.Ссылка.КонецДействия = ДАТАВРЕМЯ(1, 1, 1)
	|						ИЛИ ВыходныеИзделияСпецификации.Ссылка.КонецДействия > &НачалоПроизводства))
	|		И (ВыходныеИзделияСпецификации.НомерСтроки = 1
	|				ИЛИ НЕ &СпецификацииГдеИзделиеЯвляетсяОсновным)
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		РесурсныеСпецификацииВозвратныеОтходы.Ссылка
	|	ИЗ
	|		Справочник.к2РесурсныеСпецификации.ВозвратныеОтходы КАК РесурсныеСпецификацииВозвратныеОтходы
	|	ГДЕ
	|		НЕ РесурсныеСпецификацииВозвратныеОтходы.Ссылка.ПометкаУдаления
	|		И (РесурсныеСпецификацииВозвратныеОтходы.Ссылка.Статус В (&ДоступныеСтатусы)
	|				ИЛИ &ВсеСпецификации)
	|		И РесурсныеСпецификацииВозвратныеОтходы.Номенклатура = &Номенклатура
	|		И (РесурсныеСпецификацииВозвратныеОтходы.Характеристика = &Характеристика
	|				ИЛИ РесурсныеСпецификацииВозвратныеОтходы.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
	|				ИЛИ &Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|		И (&НачалоПроизводства = ДАТАВРЕМЯ(1, 1, 1)
	|				ИЛИ (РесурсныеСпецификацииВозвратныеОтходы.Ссылка.НачалоДействия = ДАТАВРЕМЯ(1, 1, 1)
	|					ИЛИ РесурсныеСпецификацииВозвратныеОтходы.Ссылка.НачалоДействия <= &НачалоПроизводства)
	|					И (РесурсныеСпецификацииВозвратныеОтходы.Ссылка.КонецДействия = ДАТАВРЕМЯ(1, 1, 1)
	|						ИЛИ РесурсныеСпецификацииВозвратныеОтходы.Ссылка.КонецДействия > &НачалоПроизводства))
	|		И &ПоказыватьСпецификацииПоФиксированнойСтоимости
	|	) КАК Спецификации
	|
	|");
	
	ТекстОтбора = "";
	
	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
	Запрос.УстановитьПараметр("Характеристика", Характеристика);
	Запрос.УстановитьПараметр("НачалоПроизводства", НачалоПроизводства);
	
	Запрос.УстановитьПараметр("ВсеСпецификации", ВсеСпецификации);
	Запрос.УстановитьПараметр("ДоступныеСтатусы", ДоступныеСтатусы.ВыгрузитьЗначения());
	
	Запрос.УстановитьПараметр("ПоказыватьСпецификацииПоФиксированнойСтоимости", ПоказыватьСпецификацииПоФиксированнойСтоимости);
	Запрос.УстановитьПараметр("СпецификацииГдеИзделиеЯвляетсяОсновным", СпецификацииГдеИзделиеЯвляетсяОсновным);
	
	Результат = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
	ЗаполнитьСписокСпецификаций(Результат);
	
	УстановитьСпецификацияПоУмолчанию();
	
	ЗаполнитьИнформациюПоПараметрамВыбораСпецификации();
	
КонецПроцедуры

&НаСервере
Процедура СписокСпецификацийДляТаблицыПродукции()
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТаблицаИзделий.Номенклатура,
	|	ТаблицаИзделий.Характеристика,
	|	ТаблицаИзделий.НачалоПроизводства
	|ПОМЕСТИТЬ ВТИзделия
	|ИЗ
	|	&ТаблицаИзделий КАК ТаблицаИзделий
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТИзделия.Номенклатура,
	|	ВТИзделия.Характеристика,
	|	ВыходныеИзделия.Ссылка КАК Спецификация
	|ПОМЕСТИТЬ ВТСпецификацииИзделий
	|ИЗ
	|	ВТИзделия КАК ВТИзделия
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.к2РесурсныеСпецификации.ВыходныеИзделия КАК ВыходныеИзделия
	|		ПО (ВыходныеИзделия.Номенклатура = ВТИзделия.Номенклатура)
	|			И (ВыходныеИзделия.Характеристика = ВТИзделия.Характеристика
	|				ИЛИ ВыходныеИзделия.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|			И (ВыходныеИзделия.Ссылка.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыСпецификаций.Действует))
	|				И (ВыходныеИзделия.Ссылка.НачалоДействия = ДАТАВРЕМЯ(1, 1, 1)
	|					ИЛИ НачалоПериода(ВыходныеИзделия.Ссылка.НачалоДействия, Месяц) <= ВТИзделия.НачалоПроизводства)
	|				И (ВыходныеИзделия.Ссылка.КонецДействия = ДАТАВРЕМЯ(1, 1, 1)
	|					ИЛИ ВыходныеИзделия.Ссылка.КонецДействия > ВТИзделия.НачалоПроизводства)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВТИзделия.Номенклатура,
	|	ВТИзделия.Характеристика,
	|	ВозвратныеОтходы.Ссылка
	|ИЗ
	|	ВТИзделия КАК ВТИзделия
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.к2РесурсныеСпецификации.ВозвратныеОтходы КАК ВозвратныеОтходы
	|		ПО (ВозвратныеОтходы.Номенклатура = ВТИзделия.Номенклатура)
	|			И (ВозвратныеОтходы.Характеристика = ВТИзделия.Характеристика
	|				ИЛИ ВозвратныеОтходы.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|			И (ВозвратныеОтходы.Ссылка.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыСпецификаций.Действует))
	|				И (ВозвратныеОтходы.Ссылка.НачалоДействия = ДАТАВРЕМЯ(1, 1, 1)
	|					ИЛИ НачалоПериода(ВозвратныеОтходы.Ссылка.НачалоДействия, Месяц) <= ВТИзделия.НачалоПроизводства)
	|				И (ВозвратныеОтходы.Ссылка.КонецДействия = ДАТАВРЕМЯ(1, 1, 1)
	|					ИЛИ ВозвратныеОтходы.Ссылка.КонецДействия > ВТИзделия.НачалоПроизводства)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТСпецификации.Спецификация
	|ПОМЕСТИТЬ ВТСпецификации
	|ИЗ
	|	ВТСпецификацииИзделий КАК ВТСпецификации
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТИзделия.Номенклатура,
	|	ВТИзделия.Характеристика,
	|	ВТСпецификации.Спецификация
	|ПОМЕСТИТЬ ВТКомбинации
	|ИЗ
	|	ВТИзделия КАК ВТИзделия,
	|	ВТСпецификации КАК ВТСпецификации
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТКомбинации.Спецификация
	|ПОМЕСТИТЬ ВтИсключения
	|ИЗ
	|	ВТКомбинации КАК ВТКомбинации
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСпецификацииИзделий КАК ВТСпецификацииИзделий
	|		ПО ВТКомбинации.Номенклатура = ВТСпецификацииИзделий.Номенклатура
	|			И ВТКомбинации.Характеристика = ВТСпецификацииИзделий.Характеристика
	|			И ВТКомбинации.Спецификация = ВТСпецификацииИзделий.Спецификация
	|ГДЕ
	|	ВТСпецификацииИзделий.Спецификация ЕСТЬ NULL
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТСпецификации.Спецификация
	|ИЗ
	|	ВТСпецификации КАК ВТСпецификации
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВтИсключения КАК ВтИсключения
	|		ПО (ВтИсключения.Спецификация = ВТСпецификации.Спецификация)
	|ГДЕ
	|	ВтИсключения.Спецификация ЕСТЬ NULL ";
	
	Запрос.УстановитьПараметр("ТаблицаИзделий", ТаблицаПродукции.Выгрузить());
	
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	СписокСпецификаций = Новый СписокЗначений;
	
	Пока Выборка.Следующий() Цикл
		СписокСпецификаций.Добавить(Выборка.Спецификация);
	КонецЦикла;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "СписокСпецификаций", СписокСпецификаций.ВыгрузитьЗначения());
	
КонецПроцедуры

&НаСервере
Процедура СписокСпецификацийИзделий(СписокИзделий)

	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	СписокИзделий.Номенклатура КАК Номенклатура,
	|	СписокИзделий.Характеристика КАК Характеристика
	|ПОМЕСТИТЬ СписокИзделий
	|ИЗ
	|	&СписокИзделий КАК СписокИзделий
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Номенклатура,
	|	Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	Т.Ссылка
	|ИЗ
	|	(ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		ВыходныеИзделияСпецификации.Ссылка КАК Ссылка
	|	ИЗ
	|		СписокИзделий КАК СписокИзделий
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.к2РесурсныеСпецификации.ВыходныеИзделия КАК ВыходныеИзделияСпецификации
	|			ПО (ВыходныеИзделияСпецификации.Номенклатура = СписокИзделий.Номенклатура)
	|				И (ВыходныеИзделияСпецификации.Характеристика = СписокИзделий.Характеристика
	|					ИЛИ ВыходныеИзделияСпецификации.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
	|					ИЛИ СписокИзделий.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|				И (НЕ ВыходныеИзделияСпецификации.Ссылка.ПометкаУдаления)
	|				И (ВыходныеИзделияСпецификации.Ссылка.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыСпецификаций.Действует))
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|
	|	ВЫБРАТЬ РАЗЛИЧНЫЕ
	|		РесурсныеСпецификацииВозвратныеОтходы.Ссылка
	|	ИЗ
	|		СписокИзделий КАК СписокИзделий
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.к2РесурсныеСпецификации.ВозвратныеОтходы КАК РесурсныеСпецификацииВозвратныеОтходы
	|			ПО (РесурсныеСпецификацииВозвратныеОтходы.Номенклатура = СписокИзделий.Номенклатура)
	|				И (РесурсныеСпецификацииВозвратныеОтходы.Характеристика = СписокИзделий.Характеристика
	|					ИЛИ РесурсныеСпецификацииВозвратныеОтходы.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
	|					ИЛИ СписокИзделий.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка))
	|				И (НЕ РесурсныеСпецификацииВозвратныеОтходы.Ссылка.ПометкаУдаления)
	|				И (РесурсныеСпецификацииВозвратныеОтходы.Ссылка.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыСпецификаций.Действует))
	|	ГДЕ
	|		&ПоказыватьСпецификацииПоФиксированнойСтоимости) КАК Т";

	ТаблицаСписокИзделий = Новый ТаблицаЗначений;
	ТаблицаСписокИзделий.Колонки.Добавить("Номенклатура",   Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаСписокИзделий.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	
	Для каждого СтруктураДанных Из СписокИзделий Цикл
		ЗаполнитьЗначенияСвойств(ТаблицаСписокИзделий.Добавить(), СтруктураДанных);
	КонецЦикла;
	
	Запрос.УстановитьПараметр("СписокИзделий", ТаблицаСписокИзделий);
	Запрос.УстановитьПараметр("ПоказыватьСпецификацииПоФиксированнойСтоимости", ПоказыватьСпецификацииПоФиксированнойСтоимости);
	
	Результат = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
	ЗаполнитьСписокСпецификаций(Результат);
	
	ТаблицаСписокИзделий.Свернуть("Номенклатура,Характеристика");
	Если ТаблицаСписокИзделий.Количество() = 1 Тогда
		
		ДанныеНоменклатуры = ТаблицаСписокИзделий[0];
		
		Номенклатура   = ДанныеНоменклатуры.Номенклатура;
		Характеристика = ДанныеНоменклатуры.Характеристика;
		
		УстановитьСпецификацияПоУмолчанию();
		
		ЗаполнитьИнформациюПоПараметрамВыбораСпецификации();
		
	Иначе
		
		Элементы.ФормаВыбратьИНазначитьОсновной.Видимость = Ложь;
		Элементы.ПараметрыВыбора.Видимость = Ложь;
		Элементы.Список.ПоложениеКоманднойПанели = ПоложениеКоманднойПанелиЭлементаФормы.Нет;
		
		СпецификацияПоУмолчанию = Справочники.к2РесурсныеСпецификации.ПустаяСсылка();
	
		ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
					Список,
					"СпецификацияПоУмолчанию",
					СпецификацияПоУмолчанию);
				
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();
	
	к2УправлениеДаннымиОбИзделиях.УстановитьУсловноеОформлениеСпискаСпецификаций(Список.УсловноеОформление);

КонецПроцедуры

&НаСервере
Функция НазначитьОсновнойТекущуюСпецификацию(СпецификацияЗначение)
	
	НаборДанных = Новый Массив;
	
	Если ТаблицаПродукции.Количество() > 0 Тогда
		
		Для каждого Строка Из ТаблицаПродукции Цикл
			
			СтруктураЗаписи = РегистрыСведений.к2ОсновныеСпецификации.СтруктураЗаписи();
			
			СтруктураЗаписи.Номенклатура   = Строка.Номенклатура;
			СтруктураЗаписи.Характеристика = Строка.Характеристика;
			СтруктураЗаписи.Спецификация   = СпецификацияЗначение;
			
			НаборДанных.Добавить(СтруктураЗаписи);
			
		КонецЦикла;
		
	Иначе
		
		СтруктураЗаписи = РегистрыСведений.к2ОсновныеСпецификации.СтруктураЗаписи();
		
		СтруктураЗаписи.Номенклатура   = Номенклатура;
		СтруктураЗаписи.Характеристика = Характеристика;
		СтруктураЗаписи.Спецификация   = СпецификацияЗначение;
		
		НаборДанных.Добавить(СтруктураЗаписи);
		
	КонецЕсли;
	
	ДействиеВыполнено = к2УправлениеДаннымиОбИзделияхВызовСервера.НазначитьОсновныеСпецификацииДляИзделий(НаборДанных);
	
	Если ДействиеВыполнено Тогда
		
		ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "СпецификацияПоУмолчанию", СпецификацияЗначение);
		
	КонецЕсли;
	
	Возврат ДействиеВыполнено;

КонецФункции

&НаСервере
Процедура УстановитьСпецификацияПоУмолчанию()
	
	Если ТаблицаПродукции.Количество() > 0 Тогда
		
		СпецификацияПоУмолчанию = ОсновнаяСпецификацияПоТаблицеПродукции();
		
	Иначе
		
		СпецификацияПоУмолчанию = к2УправлениеДаннымиОбИзделияхВызовСервера.ПолучитьОсновнуюСпецификацию(
									Номенклатура,
									Характеристика,
									НачалоПроизводства);

	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "СпецификацияПоУмолчанию", СпецификацияПоУмолчанию);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокСпецификаций(Массив)
	
	СписокСпецификаций.ЗагрузитьЗначения(Массив);

	Если НЕ ИсключитьИзВыбораСпецификацию.Пустая() Тогда
		НайденныйИндекс = СписокСпецификаций.НайтиПоЗначению(ИсключитьИзВыбораСпецификацию);
		Если НайденныйИндекс <> Неопределено Тогда
			СписокСпецификаций.Удалить(НайденныйИндекс);
		КонецЕсли;
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "СписокСпецификаций", СписокСпецификаций.ВыгрузитьЗначения());
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьИнформациюПоПараметрамВыбораСпецификации()
	
	Если ТаблицаПродукции.Количество() > 0 Тогда
		
		МассивИзделий = Новый Массив;
		Для Каждого Строка Из ТаблицаПродукции Цикл
			Если МассивИзделий.Найти(Строка.Номенклатура) = Неопределено Тогда
				МассивИзделий.Добавить(Строка.Номенклатура);
			КонецЕсли;
		КонецЦикла;
		
		ПараметрыВыбора = НСтр("ru = 'Изделия:'") + " ";
		
		Для Индекс = 0 По МассивИзделий.Количество()-1 Цикл
			
			Если Индекс = МассивИзделий.Количество() - 1 Тогда
				ПараметрыВыбора = ПараметрыВыбора + "%" + Строка(Индекс + 1) + ".";
			Иначе
				ПараметрыВыбора = ПараметрыВыбора + "%" + Строка(Индекс + 1) + ", ";
			КонецЕсли;
			
		КонецЦикла;
		
		ПараметрыВыбора = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтрокуИзМассива(ПараметрыВыбора, МассивИзделий);
		
	Иначе
		
		ПараметрыВыбора = НСтр("ru = 'Номенклатура:'");
		
		Если НЕ ЗначениеЗаполнено(Номенклатура) Тогда
			ПараметрыВыбора = ПараметрыВыбора + " " + НСтр("ru = '<не выбрана>'");
		Иначе
			ПараметрыВыбора = ПараметрыВыбора + " """ + Строка(Номенклатура) + ?(Характеристика.Пустая(), "", ", " + Строка(Характеристика)) + """";
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура НастроитьФормуПоТаблицеПродукции()
	
	Таблица = ПолучитьИзВременногоХранилища(Параметры.АдресТаблицыПродукции).Скопировать();
	Таблица.Свернуть("Номенклатура, Характеристика, НачалоПроизводства");
	ТаблицаПродукции.Загрузить(Таблица);
	
	СписокСпецификацийДляТаблицыПродукции();
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "СпецификацияПоУмолчанию", Неопределено);
	
	ЗаполнитьИнформациюПоПараметрамВыбораСпецификации();
	
	УстановитьСпецификацияПоУмолчанию();
	
	Заголовок = НСтр("ru = 'Заполнение / замена спецификации в выпусках'");
	Элементы.ФормаВыбрать.Заголовок = НСтр("ru = 'Заменить'");
	Элементы.ФормаВыбратьИНазначитьОсновной.Заголовок = НСтр("ru = 'Заменить и назначить основной'");
	Элементы.ФормаИзменитьПрименениеСпецификации.Видимость = Ложь;
	
КонецПроцедуры

&НаСервере
Функция ОсновнаяСпецификацияПоТаблицеПродукции()
	
	ПараметрыЗапроса = Новый Структура("ДляСпискаНоменклатуры");
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТаблицаИзделий.Номенклатура,
	|	ТаблицаИзделий.Характеристика,
	|	ТаблицаИзделий.НачалоПроизводства
	|ПОМЕСТИТЬ СписокНоменклатуры
	|ИЗ
	|	&ТаблицаИзделий КАК ТаблицаИзделий
	|;
	|" + к2УправлениеДаннымиОбИзделиях.ПолучитьТекстЗапросаОсновнойСпецификации(ПараметрыЗапроса);
	
	Запрос.УстановитьПараметр("ТаблицаИзделий", ТаблицаПродукции.Выгрузить());
	
	ТаблицаОсновныхСпецификаций = Запрос.Выполнить().Выгрузить();
	
	СтруктураОтбора = Новый Структура("Номенклатура, Характеристика, Спецификация");
	
	СпецификацияПоУмолчанию = Неопределено;
	
	Для Каждого Строка Из СписокСпецификаций Цикл
		
		ЭтоОсновнаяСпецификация = Истина;
		СтруктураОтбора.Спецификация = Строка.Значение;
		
		Для Каждого СтрокаПродукции Из ТаблицаПродукции Цикл
			
			ЗаполнитьЗначенияСвойств(СтруктураОтбора, СтрокаПродукции);
			
			Если ТаблицаОсновныхСпецификаций.НайтиСтроки(СтруктураОтбора).Количество() = 0 Тогда
				ЭтоОсновнаяСпецификация = Ложь;
				Прервать;
			КонецЕсли;
			
		КонецЦикла;
		
		Если ЭтоОсновнаяСпецификация Тогда
			СпецификацияПоУмолчанию = Строка.Значение;
		КонецЕсли;
		
		Прервать;
		
	КонецЦикла;
	
	Возврат СпецификацияПоУмолчанию;
	
КонецФункции

&НаКлиенте
Функция ПроверитьНазначитьСпецификациюОсновной(Ссылка)
	
	СтруктураОшибок = Новый Структура();
	
	ЕстьОшибки = Не СпецификациюМожноНазначитьОсновной(Ссылка, СтруктураОшибок);
	
	Если ЕстьОшибки Тогда
		
		Если СтруктураОшибок.Свойство("ТекстОшибки") Тогда
			ПоказатьПредупреждение(, СтруктураОшибок.ТекстОшибки);
		Иначе
			ПоказатьПредупреждение(, НСтр("ru = 'Действие не может быть выполнено для выбранного объекта.'"));
		КонецЕсли;
		
		Результат = Ложь;
		
	Иначе
		
		Результат = НазначитьОсновнойТекущуюСпецификацию(Ссылка);
		
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

&НаСервереБезКонтекста
Функция СпецификациюМожноНазначитьОсновной(Ссылка, СтруктураОшибок)
	
	СвойстваСпецификации = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Ссылка, "Статус, НачалоДействия");
	
	Результат = к2УправлениеДаннымиОбИзделияхКлиентСервер.СпецификациюМожноНазначитьОсновной(
		СвойстваСпецификации,
		СтруктураОшибок);
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти

#КонецОбласти
