#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда


#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	// Создание запроса инициализации движений и заполнение его параметров.
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблица_к2ПредварительныеЗаказы(Запрос, ТекстыЗапроса, Регистры);
	
	// Исполнение запроса и выгрузка полученных таблиц для движений.
	к2ПроведениеСервер.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	ИменаРеквизитов = "Ссылка";
	
	РеквизитыДокумента = глРеквизиты(ДокументСсылка, ИменаРеквизитов);
	
	Для Каждого цРеквизит Из РеквизитыДокумента Цикл
		
		Запрос.УстановитьПараметр(цРеквизит.Ключ, цРеквизит.Значение);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ТекстЗапросаТаблица_к2ПредварительныеЗаказы(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "к2ПредварительныеЗаказы";
	
	Если Не к2ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса =
    "ВЫБРАТЬ
    |	к2ПредварительныйЗаказПоказатели.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
    |	к2ПредварительныйЗаказПоказатели.Контрагент КАК Контрагент,
    |	к2ПредварительныйЗаказПоказатели.Дата КАК Период,
    |	к2ПредварительныйЗаказПоказатели.ОбъемОтгрузок КАК ОбъемОтгрузок,
    |	к2ПредварительныйЗаказПоказатели.Склад КАК Склад,
    |	к2ПредварительныйЗаказПоказатели.ТочкаДоставки КАК ТочкаДоставки
    |ИЗ
    |	Документ.к2ПредварительныйЗаказ.Показатели КАК к2ПредварительныйЗаказПоказатели
    |ГДЕ
    |	к2ПредварительныйЗаказПоказатели.Ссылка = &Ссылка";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
