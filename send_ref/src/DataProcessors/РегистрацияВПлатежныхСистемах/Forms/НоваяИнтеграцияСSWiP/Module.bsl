///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020, ООО 1С-Софт
// Все права защищены. Эта программа и сопроводительные материалы предоставляются 
// в соответствии с условиями лицензии Attribution 4.0 International (CC BY 4.0)
// Текст лицензии доступен по ссылке:
// https://creativecommons.org/licenses/by/4.0/legalcode
///////////////////////////////////////////////////////////////////////////////////////////////////////

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ДоступнаРегистрация = Ложь;
	
	ИнтеграцияПодсистемБИП.ПриОпределенииДоступностиРегистрацииSWiP(
		ДоступнаРегистрация);
	
	ИнтеграцияСПлатежнымиСистемамиПереопределяемый.ПриОпределенииДоступностиРегистрацииSWiP(
		ДоступнаРегистрация);
	
	Если Не ДоступнаРегистрация Тогда
		Регистрация = Ложь;
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеУчетнойЗаписи;
		УстановитьВидимостьДоступность(Элементы);
	КонецЕсли;
	
	АнглийскиеСимволыИЦифры = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ДекорацияОписаниеОшибкиОбработкаНавигационнойСсылки(
		Элемент,
		НавигационнаяСсылкаФорматированнойСтроки,
		СтандартнаяОбработка)
	
	ИнтеграцияСПлатежнымиСистемамиКлиент.ДекорацияОшибкаОбработкаНавигационнойСсылки(
		ЭтотОбъект,
		Элемент,
		НавигационнаяСсылкаФорматированнойСтроки,
		СтандартнаяОбработка)
	
КонецПроцедуры

&НаКлиенте
Процедура ДекорацияНадписьУспешноеЗавершениеОбработкаНавигационнойСсылки(
		Элемент,
		НавигационнаяСсылкаФорматированнойСтроки,
		СтандартнаяОбработка)
	
	Если НавигационнаяСсылкаФорматированнойСтроки = "new:store" Тогда
		СтандартнаяОбработка = Ложь;
		Закрыть(УчетнаяЗапись);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НадписьПоясненияПодключенияАвторизацияОбработкаНавигационнойСсылки(
		Элемент,
		НавигационнаяСсылкаФорматированнойСтроки,
		СтандартнаяОбработка)
	
	Если НавигационнаяСсылкаФорматированнойСтроки = "action:openPortal" Тогда
		СтандартнаяОбработка = Ложь;
		ИнтернетПоддержкаПользователейКлиент.ОткрытьВебСтраницу(
			ИнтернетПоддержкаПользователейКлиентСервер.URLСтраницыСервисаLogin(
				,
				ИнтернетПоддержкаПользователейКлиент.НастройкиСоединенияССерверами()));
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДекорацияПользовательскоеСоглашениеОбработкаНавигационнойСсылки(
		Элемент,
		НавигационнаяСсылкаФорматированнойСтроки,
		СтандартнаяОбработка)
	
	Если НавигационнаяСсылкаФорматированнойСтроки = "open:userAgreement" Тогда
		СтандартнаяОбработка = Ложь;
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("ИдентификаторСистемы", ИнтеграцияСПлатежнымиСистемамиКлиент.ИдентификаторSWiP());
		ОткрытьФорму(
			"Обработка.РегистрацияВПлатежныхСистемах.Форма.ПользовательскоеСоглашение",
			ПараметрыФормы,
			ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДекорацияПодсказкаОбработкаНавигационнойСсылки(
		Элемент,
		НавигационнаяСсылкаФорматированнойСтроки,
		СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ФайловаяСистемаКлиент.ОткрытьНавигационнуюСсылку(
		ИнтеграцияСПлатежнымиСистемамиКлиент.АдресСтраницыПрофиляПользователяSWiP());

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Зарегистрироваться(Команда)
	
	ЗарегистрироватьНовогоМерчанта();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключить(Команда)
	
	ПодключитьМерчанта();
	
КонецПроцедуры

&НаКлиенте
Процедура Вход(Команда)
	
	Регистрация = Ложь;
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеУчетнойЗаписи;
	УстановитьВидимостьДоступность(Элементы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОК(Команда)
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура Назад(Команда)
	
	ОтобразитьРегистрацию = ((Не Регистрация И Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеУчетнойЗаписи)
		Или Регистрация) И ДоступнаРегистрация;
	
	Если ОтобразитьРегистрацию Тогда
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаРегистрация;
	Иначе
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеУчетнойЗаписи;
	КонецЕсли;
	
	УстановитьВидимостьДоступность(Элементы);
	
КонецПроцедуры

&НаКлиенте
Процедура ПовторитьПопытку(Команда)
	
	Если Регистрация Тогда
		ЗарегистрироватьНовогоМерчанта();
	Иначе
		ПодключитьМерчанта();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Далее(Команда)
	
	Отказ = Ложь;
	Если Не ЗначениеЗаполнено(ЛогинИПП) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Логин"".'"),
			,
			"ЛогинИПП",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ПарольИПП) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Пароль"".'"),
			,
			"ПарольИПП",
			,
			Отказ);
	КонецЕсли;
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ДанныеАутентификацииЗаписаны() Тогда
		Возврат;
	КонецЕсли;
	
	Если Регистрация Тогда
		ЗарегистрироватьНовогоМерчанта();
	Иначе
		ПодключитьМерчанта();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПодключениеМерчанта

&НаКлиенте
Процедура ПодключитьМерчанта()
	
	Отказ = Ложь;
	ПроверитьЗаполнениеДанныхПодключения(Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыОжидания = ДлительныеОперацииКлиент.ПараметрыОжидания(ЭтотОбъект);
	ПараметрыОжидания.ВыводитьОкноОжидания = Ложь;
	
	РезультатВыполнения = ПодключитьМерчантаНаСервере();
	
	Регистрация = Ложь;
	Элементы.ДекорацияНадписьДлительнаяОперация.Заголовок = НСтр("ru = 'Подключение учетной записи к системе SWiP. Пожалуйста, подождите...'");
	Если РезультатВыполнения.Статус = "Выполнено" Тогда
		ПодключитьМерчантаЗавершение(РезультатВыполнения, Неопределено);
		Возврат;
	ИначеЕсли РезультатВыполнения.Статус = "ПодключитьИПП" Тогда
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеИПП;
		УстановитьВидимостьДоступность(Элементы);
		Возврат;
	КонецЕсли;
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаДлительнаяОперация;
	УстановитьВидимостьДоступность(Элементы);
	
	ОповещениеОЗавершении = Новый ОписаниеОповещения(
		"ПодключитьМерчантаЗавершение",
		ЭтотОбъект);
	
	ДлительныеОперацииКлиент.ОжидатьЗавершение(
		РезультатВыполнения,
		ОповещениеОЗавершении,
		ПараметрыОжидания);
	
КонецПроцедуры

&НаСервере
Функция ПодключитьМерчантаНаСервере()
	
	Если Не ИнтернетПоддержкаПользователей.ЗаполненыДанныеАутентификацииПользователяИнтернетПоддержки() Тогда
		РезультатВыполнения = Новый Структура;
		РезультатВыполнения.Вставить("Статус", "ПодключитьИПП");
		РезультатВыполнения.Вставить("КраткоеПредставлениеОшибки", "");
		РезультатВыполнения.Вставить("ПодробноеПредставлениеОшибки", "");
		Возврат РезультатВыполнения;
	КонецЕсли;
	
	ДанныеАутентификации = Новый Структура;
	ДанныеАутентификации.Вставить("password", Пароль);
	ДанныеАутентификации.Вставить("username", Логин);
	
	ПараметрыПроцедуры = Новый Структура;
	ПараметрыПроцедуры.Вставить("ИдентификаторМерчанта", ИдентификаторМерчанта);
	ПараметрыПроцедуры.Вставить("ТокенАутентификации",   ТокенАутентификации);
	ПараметрыПроцедуры.Вставить("НаименованиеМерчанта",  НаименованиеМерчанта);
	ПараметрыПроцедуры.Вставить("ДанныеАутентификации",   ДанныеАутентификации);
	
	ПараметрыВыполнения = ДлительныеОперации.ПараметрыВыполненияВФоне(УникальныйИдентификатор);
	ПараметрыВыполнения.НаименованиеФоновогоЗадания = НСтр("ru = 'Подключение новой учетной записи платежной системы SWiP.'");
	
	РезультатВыполнения = ДлительныеОперации.ВыполнитьВФоне(
		"СервисИнтеграцииСSWiP.ПодключитьУчетнуюЗаписьSWiP",
		ПараметрыПроцедуры,
		ПараметрыВыполнения);
	
	Возврат РезультатВыполнения;
	
КонецФункции

&НаКлиенте
Процедура ПодключитьМерчантаЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	РезультатОперации = ПолучитьИзВременногоХранилища(Результат.АдресРезультата);
	Если Результат.Статус = "Выполнено"
		И РезультатОперации.КодОшибки <> ИнтеграцияСПлатежнымиСистемамиКлиент.КодОшибкиНеверныйЛогинИлиПароль() Тогда
		ТекстУспешногоЗавершения =СтроковыеФункцииКлиент.ФорматированнаяСтрока(
			НСтр("ru = 'Учетная запись успешно подключена. Для начала работы <a href = ""new:store"">создайте</a> торговую точку.'"));
	КонецЕсли;
	
	ОтобразитьРезультатОперации(
		Результат,
		РезультатОперации,
		ТекстУспешногоЗавершения,
		ДополнительныеПараметры);
	
КонецПроцедуры

&НаКлиенте
Процедура ПроверитьЗаполнениеДанныхПодключения(Отказ)
	
	Если Не ЗначениеЗаполнено(Логин) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Адрес электронной почты"".'"),
			,
			"Логин",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ОбщегоНазначенияКлиентСервер.АдресЭлектроннойПочтыСоответствуетТребованиям(Логин) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Некорректный адрес электронной почты.'"),
			,
			"Логин",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Пароль) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Пароль"".'"),
			,
			"Пароль",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ИдентификаторМерчанта) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""ID"".'"),
			,
			"ИдентификаторМерчанта",
			,
			Отказ);
	КонецЕсли;
	
	Если Не СтрокаСодержитТолькоДопустимыеСимволы(ИдентификаторМерчанта, АнглийскиеСимволыИЦифры) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'ID содержит запрещенные символы.'"),
			,
			"ИдентификаторМерчанта",
			,
			Отказ);
	КонецЕсли;
	
	Если СтрДлина(ИдентификаторМерчанта) < 2 Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'ID должен иметь длину не менее двух символов.'"),
			,
			"ИдентификаторМерчанта",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ТокенАутентификации) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Токен"".'"),
			,
			"ТокенАвторизации",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ПользовательскоеСоглашение Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Необходимо принять пользовательское соглашение.'"),
			,
			"ПользовательскоеСоглашение",
			,
			Отказ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область РегистрацияМерчанта

&НаКлиенте
Процедура ЗарегистрироватьНовогоМерчанта()
	
	Отказ = Ложь;
	ПроверитьЗаполнениеДанныхРегистрации(Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыОжидания = ДлительныеОперацииКлиент.ПараметрыОжидания(ЭтотОбъект);
	ПараметрыОжидания.ВыводитьОкноОжидания = Ложь;
	
	РезультатВыполнения = ЗарегистрироватьсяНаСервере(Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Элементы.ДекорацияНадписьДлительнаяОперация.Заголовок = НСтр("ru = 'Создание новой учетной записи в системе SWiP. Пожалуйста, подождите...'");
	Регистрация = Истина;
	Если РезультатВыполнения.Статус = "Выполнено" Тогда
		ЗарегистрироватьсяЗавершение(РезультатВыполнения, Неопределено);
		Возврат;
	ИначеЕсли РезультатВыполнения.Статус = "ПодключитьИПП" Тогда
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеИПП;
		УстановитьВидимостьДоступность(Элементы);
		Возврат;
	КонецЕсли;
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаДлительнаяОперация;
	УстановитьВидимостьДоступность(Элементы);
	
	ОповещениеОЗавершении = Новый ОписаниеОповещения(
		"ЗарегистрироватьсяЗавершение",
		ЭтотОбъект);
	
	ДлительныеОперацииКлиент.ОжидатьЗавершение(
		РезультатВыполнения,
		ОповещениеОЗавершении,
		ПараметрыОжидания);
	
КонецПроцедуры

&НаСервере
Функция ЗарегистрироватьсяНаСервере(Отказ)
	
	ПараметрыПроцедуры = Новый Структура;
	ПараметрыПроцедуры.Вставить("Логин",                Логин);
	ПараметрыПроцедуры.Вставить("Пароль",               Пароль);
	ПараметрыПроцедуры.Вставить("НомерТелефона",        НомерТелефона);
	ПараметрыПроцедуры.Вставить("НаименованиеМерчанта", НаименованиеМерчанта);
	
	Результат = СервисИнтеграцииСSWiP.ДанныеОрганизацииSWiP(
		Организация);
	Если Результат.СообщенияОбОшибках.Количество() <> 0 Тогда
		Для Каждого СообщениеОбОшибке Из Результат.СообщенияОбОшибках Цикл
			ОбщегоНазначения.СообщитьПользователю(
				СообщениеОбОшибке,
				,
				"Организация",
				,
				Отказ);
		КонецЦикла;
	КонецЕсли;
	
	ПараметрыПроцедуры.Вставить("ДанныеОрганизации", Результат.ДанныеОрганизации);
	
	Результат = СервисИнтеграцииСSWiP.ДанныеБанковскогоСчетаSWiP(
		БанковскийСчет);
	Если Результат.СообщенияОбОшибках.Количество() <> 0 Тогда
		Для Каждого СообщениеОбОшибке Из Результат.СообщенияОбОшибках Цикл
			ОбщегоНазначения.СообщитьПользователю(
				СообщениеОбОшибке,
				,
				"БанковскийСчет",
				,
				Отказ);
		КонецЦикла;
	КонецЕсли;
	
	ПараметрыПроцедуры.Вставить("ДанныеБанковскогоСчета", Результат.ДанныеБанковскогоСчета);
	
	Если Отказ Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Если Не ИнтернетПоддержкаПользователей.ЗаполненыДанныеАутентификацииПользователяИнтернетПоддержки() Тогда
		РезультатВыполнения = Новый Структура;
		РезультатВыполнения.Вставить("Статус", "ПодключитьИПП");
		РезультатВыполнения.Вставить("КраткоеПредставлениеОшибки", "");
		РезультатВыполнения.Вставить("ПодробноеПредставлениеОшибки", "");
		Возврат РезультатВыполнения;
	КонецЕсли;

	ПараметрыВыполнения = ДлительныеОперации.ПараметрыВыполненияВФоне(УникальныйИдентификатор);
	ПараметрыВыполнения.НаименованиеФоновогоЗадания = НСтр("ru = 'Регистрация новой учетной записи в платежной системе SWiP.'");
	
	РезультатВыполнения = ДлительныеОперации.ВыполнитьВФоне(
		"СервисИнтеграцииСSWiP.ЗарегистрироватьсяВSWiP",
		ПараметрыПроцедуры,
		ПараметрыВыполнения);
	
	Возврат РезультатВыполнения;
	
КонецФункции

&НаКлиенте
Процедура ЗарегистрироватьсяЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	ТекстУспешногоЗавершения = НСтр("ru = 'Учетная запись успешно зарегистрирована. Ожидайте активации в платежной системе.'");
	ОтобразитьРезультатОперации(
		Результат,
		ПолучитьИзВременногоХранилища(Результат.АдресРезультата),
		ТекстУспешногоЗавершения,
		ДополнительныеПараметры);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтобразитьРезультатОперации(
		Результат,
		РезультатОперации,
		ТекстУспешногоЗавершения,
		ДополнительныеПараметры)
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Результат.Статус = "Выполнено" Тогда
		Если ЗначениеЗаполнено(РезультатОперации.КодОшибки) Тогда
			УстановитьОтображениеОшибки(
				Элементы,
				РезультатОперации.СообщениеОбОшибке);
		Иначе
			Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаНастройкаЗавершена;
			Элементы.ДекорацияНадписьУспешноеЗавершение.Заголовок = ТекстУспешногоЗавершения;
			УчетнаяЗапись = РезультатОперации.УчетнаяЗапись
		КонецЕсли;
		
	ИначеЕсли Результат.Статус = "Ошибка" Тогда
		УстановитьОтображениеОшибки(
			Элементы,
			Результат.КраткоеПредставлениеОшибки);
	КонецЕсли;
	
	УстановитьВидимостьДоступность(Элементы);
	
КонецПроцедуры

&НаКлиенте
Процедура ПроверитьЗаполнениеДанныхРегистрации(Отказ)
	
	Если Не ЗначениеЗаполнено(НаименованиеМерчанта) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Наименование"".'"),
			,
			"НаименованиеМерчанта",
			,
			Отказ);
	КонецЕсли;
	
	Если Не СтрокаСодержитТолькоДопустимыеСимволы(НаименованиеМерчанта, АнглийскиеСимволыИЦифры) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Наименование содержит запрещенные символы.'"),
			,
			"НаименованиеМерчанта",
			,
			Отказ);
	КонецЕсли;
	
	Если СтрДлина(НаименованиеМерчанта) < 2 Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Наименование должно иметь длину не менее двух символов.'"),
			,
			"НаименованиеМерчанта",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Логин) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Логин"".'"),
			,
			"Логин",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ОбщегоНазначенияКлиентСервер.АдресЭлектроннойПочтыСоответствуетТребованиям(Логин) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Некорректный адрес электронной почты.'"),
			,
			"Логин",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Пароль) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Пароль"".'"),
			,
			"Пароль",
			,
			Отказ);
	КонецЕсли;
	
	Если Не СтрокаСодержитТолькоДопустимыеСимволы(Пароль, АнглийскиеСимволыИЦифры) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Поле ""Пароль"" содержит недопустимые символы. Пароль должен состоять только из английских букв и цифр.'"),
			,
			"Пароль",
			,
			Отказ);
	КонецЕсли;
	
	Если СтрДлина(Пароль) < 4 Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Пароль должен иметь длину не менее четырех символов.'"),
			,
			"Пароль",
			,
			Отказ);
	КонецЕсли;
	
	Если Пароль <> ПодтверждениеПароля Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Пароль и подтверждение пароля не совпадают.'"),
			,
			"Пароль",
			,
			Отказ);
	КонецЕсли;
	
	Если Не СтрНачинаетсяС(НомерТелефона, "7") Или СтрДлина(НомерТелефона) <> 11
			Или Не СтрокаСодержитТолькоДопустимыеСимволы(НомерТелефона, "0123456789") Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Некорректный формат номера телефона. Введите номер телефона в формате 7ХХХХХХХХХХ.'"),
			,
			"НомерТелефона",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Организация) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Организация"".'"),
			,
			"Организация",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(БанковскийСчет) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Не заполнено поле ""Банковский счет"".'"),
			,
			"БанковскийСчет",
			,
			Отказ);
	КонецЕсли;
	
	Если Не ПользовательскоеСоглашение Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(
			НСтр("ru = 'Необходимо принять пользовательское соглашение.'"),
			,
			"ПользовательскоеСоглашение",
			,
			Отказ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ПрочиеСлужебныеПроцедурыФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьВидимостьДоступность(Элементы)
	
	Если Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаРегистрация Тогда
		Элементы.Зарегистрироваться.КнопкаПоУмолчанию = Истина;
	ИначеЕсли Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеИПП Тогда
		Элементы.Далее.КнопкаПоУмолчанию = Истина;
	ИначеЕсли Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаПодключениеУчетнойЗаписи Тогда
		Элементы.Подключить.КнопкаПоУмолчанию = Истина;
	ИначеЕсли Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаНастройкаЗавершена Тогда
		Элементы.ОК.КнопкаПоУмолчанию = Истина;
	ИначеЕсли Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаОшибка Тогда
		Элементы.ПовторитьПопытку.КнопкаПоУмолчанию = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьОтображениеОшибки(Элементы, ОписаниеОшибки)
	
	ЧастиСтрок = Новый Массив;
	ЧастиСтрок.Добавить(ОписаниеОшибки);
	ЧастиСтрок.Добавить(Символы.ПС);
	ЧастиСтрок.Добавить(Символы.ПС);
	ЧастиСтрок.Добавить(НСтр("ru = 'Подробнее см.'"));
	ЧастиСтрок.Добавить(" ");
	ЧастиСтрок.Добавить(
		Новый ФорматированнаяСтрока(
			НСтр("ru = 'журнал регистрации.'")
			,
			,
			,
			,
			"OpenLog"));
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница    = Элементы.ГруппаОшибка;
	Элементы.ДекорацияОписаниеОшибки.Заголовок = Новый ФорматированнаяСтрока(ЧастиСтрок);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция СтрокаСодержитТолькоДопустимыеСимволы(Строка, ДопустимыеСимволы)
	МассивСимволов = Новый Массив;
	Для Позиция = 1 По СтрДлина(ДопустимыеСимволы) Цикл
		МассивСимволов.Добавить(Сред(ДопустимыеСимволы,Позиция,1));
	КонецЦикла;
	
	Для Позиция = 1 По СтрДлина(Строка) Цикл
		Если МассивСимволов.Найти(Сред(Строка, Позиция, 1)) = Неопределено Тогда
			Возврат Ложь;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Истина;
	
КонецФункции

&НаСервере
Функция ДанныеАутентификацииЗаписаны()
	
	Результат = ИнтернетПоддержкаПользователей.ПроверитьЛогинИПароль(ЛогинИПП, ПарольИПП);
	Если ЗначениеЗаполнено(Результат.КодОшибки) Тогда
		ОбщегоНазначения.СообщитьПользователю(
			Результат.СообщениеОбОшибке,
			,
			,
			"ЛогинИПП");
		Возврат Ложь;
	Иначе
		ДанныеАутентификации = Новый Структура;
		ДанныеАутентификации.Вставить("Логин",  ЛогинИПП);
		ДанныеАутентификации.Вставить("Пароль", ПарольИПП);
		ИнтернетПоддержкаПользователей.СлужебнаяСохранитьДанныеАутентификации(
			ДанныеАутентификации);
		Возврат Истина;
	КонецЕсли;
	
КонецФункции

#КонецОбласти

#КонецОбласти
