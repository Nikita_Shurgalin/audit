#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ВариантыОтчетовКлиент.ПоказатьПанельОтчетов("к2Закупки", ПараметрыВыполненияКоманды);
	
КонецПроцедуры

#КонецОбласти