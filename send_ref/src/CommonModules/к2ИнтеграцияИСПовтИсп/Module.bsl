#Область СлужебныйПрограммныйИнтерфейс

// Возвращает объект ЗащищенноеСоединениеOpenSSL
// 
// Возвращаемое значение:
// 	ЗащищенноеСоединениеOpenSSL - Описание
Функция ЗащищенноеСоединение() Экспорт
	
	ЗащищенноеСоединение = ОбщегоНазначенияКлиентСервер.НовоеЗащищенноеСоединение(Неопределено, Новый СертификатыУдостоверяющихЦентровОС());
	
	Возврат ЗащищенноеСоединение;
	
КонецФункции

// Возвращает структуру реквизитов справочника.
//
// Параметры:
//		ИмяСправочника - Строка - имя справочника как оно задано в метаданных
// Возвращаемое значение:
//		Структура - стандартные и обычные реквизиты справочника.
//
Функция РеквизитыСправочника(ИмяСправочника) Экспорт
	
	СтруктураРеквизитов   = Новый Структура;
	МетаданныеСправочника = Метаданные.Справочники[ИмяСправочника];
	
	Для Каждого Реквизит Из МетаданныеСправочника.СтандартныеРеквизиты Цикл
		СтруктураРеквизитов.Вставить(Реквизит.Имя, Реквизит.Синоним);
	КонецЦикла;
	
	Для Каждого Реквизит Из МетаданныеСправочника.Реквизиты Цикл
		СтруктураРеквизитов.Вставить(Реквизит.Имя, Реквизит.Синоним);
	КонецЦикла;
	
	Возврат СтруктураРеквизитов;
	
КонецФункции

// Возвращает признак принадлежности переданного в параметре вида продукции к виду продукции ИС МП.
// 
// Параметры:
// 	ВидПродукции              - ПеречислениеСсылка.ВидыПродукцииИС - Вид продукции для анализа.
// Возвращаемое значение:
// 	Булево - Принадлежность к виду продукции ИСМП
//
Функция ЭтоПродукцияИСМП(ВидПродукции) Экспорт
	
	ВидыПродукцииИСМП = к2ИнтеграцияИСКлиентСервер.ВидыПродукцииИСМП();
	
	Возврат ВидыПродукцииИСМП.Найти(ВидПродукции) <> Неопределено;
	
КонецФункции

Функция ДействияПриИзмененииСтатуса() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	к2ДействияПриИзмененииСтатусаИСМП.ТипДокумента КАК ТипДокумента,
		|	к2ДействияПриИзмененииСтатусаИСМП.Статус КАК Статус,
		|	к2ДействияПриИзмененииСтатусаИСМП.Действие КАК Действие
		|ИЗ
		|	РегистрСведений.к2ДействияПриИзмененииСтатусаИСМП КАК к2ДействияПриИзмененииСтатусаИСМП";
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Возврат РезультатЗапроса.Выгрузить();
	
КонецФункции

#КонецОбласти
