
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновитьЗначенияКонстант();
	
	Подсистема = Метаданные.Подсистемы[Параметры.Подсистема];
	
	ПредставлениеПодсистемы = "";
	ПредставлениеПодсистемы = СклонениеПредставленийОбъектов.ПросклонятьФИО(Подсистема.Синоним, 2, , 2);
	
	Заголовок = стрШаблон("Настройки подсистемы %1", нРег(ПредставлениеПодсистемы));
	
	СоздатьГруппыПоСоответствиюПоПодсистемам(Подсистема);
	
	СоздатьЭлементыФормы(Подсистема);	
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы                                                                                       

&НаКлиенте
Процедура ПрименитьИЗакрыть(Команда)
	
	ПрименитьИЗакрытьНаСервере();	
	
	Закрыть();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаСервере
Процедура ОбновитьЗначенияКонстант()
	
	ПутьДанных = РеквизитФормыВЗначение("КонстантыФормы");
	ПутьДанных.Прочитать();
	ЗначениеВРеквизитФормы(ПутьДанных, "КонстантыФормы"); 	
	
КонецПроцедуры

&НаСервере
Функция СоответствиеПоПодсистемам()
	
	Соответствие = Новый Соответствие;
	Соответствие.Вставить(Метаданные.Подсистемы.к2ИсполнениеЗаказовКлиентов, "Виды цен, Маркировка, Заказы клиентов, Прочее");
		
	Возврат Соответствие;
	
КонецФункции

&НаСервере
Функция СоответствиеПоГруппам()
	
	Соответствие = Новый Соответствие;
	
	Соответствие.Вставить("к2ВидЦенБазовый", "Виды цен");
	Соответствие.Вставить("к2ВидЦенСебестоимость", "Виды цен");
	Соответствие.Вставить("к2ВидЦенПеремещения", "Виды цен");
	Соответствие.Вставить("к2ПрефиксШтрихкодовУзлаРИБ", "Маркировка");
	Соответствие.Вставить("к2ШаблонЭтикеткиУпаковочногоЛиста", "Маркировка");
	Соответствие.Вставить("к2КонтролироватьОстаткиПоЗаданиямНаМаркировку", "Маркировка");
	Соответствие.Вставить("к2ОбеспечиватьПотребностьОстатками", "Маркировка");
	Соответствие.Вставить("к2ИспользоватьПроверкиДокументов", "Заказы клиентов");
	Соответствие.Вставить("к2ВестиУчетПоСменам", "Прочее");
	Соответствие.Вставить("к2ВестиУчетТоваровОрганизации", "Прочее");
	
	Возврат Соответствие;
	
КонецФункции

&НаСервере
Процедура СоздатьГруппыПоСоответствиюПоПодсистемам(Подсистема)
	
	СтрокаГруппПодсистемы = СоответствиеПоПодсистемам().Получить(Подсистема);
	
	Если ЗначениеЗаполнено(СтрокаГруппПодсистемы) Тогда
		
		Для Каждого ГруппаПодсистемы Из стрРазделить(СтрокаГруппПодсистемы, ",") Цикл
			
			ДобавитьГруппу(ГруппаПодсистемы, ЭтотОбъект);
			
		КонецЦикла;
		
	КонецЕсли;			
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьГруппу(Заголовок, ГруппаРодитель = Неопределено)
	
	Если Элементы.Найти(Заголовок) = Неопределено ИЛИ Заголовок = "" Тогда
		
		ИмяГруппы = СтрЗаменить(Заголовок, " ", "");
		
		Если ГруппаРодитель = Неопределено Тогда
			НоваяГруппа = Элементы.Добавить(ИмяГруппы, Тип("ГруппаФормы"), ЭтотОбъект);
		Иначе
			НоваяГруппа = Элементы.Добавить(ИмяГруппы, Тип("ГруппаФормы"), ГруппаРодитель);
		КонецЕсли;
		НоваяГруппа.Вид = ВидГруппыФормы.ОбычнаяГруппа;
		НоваяГруппа.Группировка = ГруппировкаПодчиненныхЭлементовФормы.Вертикальная;
		НоваяГруппа.Заголовок = Заголовок;
		
		НоваяГруппа.Поведение = ПоведениеОбычнойГруппы.Обычное;

		НоваяГруппа.Поведение = ПоведениеОбычнойГруппы.Свертываемая;
		НоваяГруппа.ОтображениеУправления = ОтображениеУправленияОбычнойГруппы.Картинка;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура СоздатьЭлементыФормы(Данные)
	
	Для каждого Элемент Из Данные.Состав Цикл
		
		Если ОбщегоНазначения.ЭтоКонстанта(Элемент) Тогда
			
			СоздатьГруппыПоСоответствиюПоКонстантам(Элемент.Имя);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура СоздатьГруппыПоСоответствиюПоКонстантам(ИмяКонстанты)

	ГруппаКонстанты = СоответствиеПоГруппам().Получить(ИмяКонстанты); 
	
	Если ЗначениеЗаполнено(ГруппаКонстанты) Тогда
		ИмяЭлемента = СтрЗаменить(ГруппаКонстанты, " ", "");
		Если Элементы.Найти(ИмяЭлемента) <> Неопределено Тогда 
			ДобавитьПолеКонстанты(ИмяКонстанты, Элементы[ИмяЭлемента]);
		КонецЕсли;
	Иначе
	    ДобавитьПолеКонстанты(ИмяКонстанты, ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьПолеКонстанты(ИмяОбъекта, ГруппаРодитель)
	                                   
	Если ТипЗнч(Константы[ИмяОбъекта].Получить()) = Тип("ХранилищеЗначения") Тогда 
		
		Возврат;
	
	ИначеЕсли ТипЗнч(Константы[ИмяОбъекта].Получить()) = Тип("Булево") Тогда
		                                                                               
		НовоеПоле = Элементы.Добавить(ИмяОбъекта, Тип("ПолеФормы"), ГруппаРодитель);
		
		НовоеПоле.ПутьКДанным = "КонстантыФормы." + ИмяОбъекта;
		
		НовоеПоле.Вид = ВидПоляФормы.ПолеФлажка;
		НовоеПоле.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Право;
		
		Попытка
			НовоеПоле.Подсказка = Константы[ИмяОбъекта].СоздатьМенеджерЗначения().ОписаниеКонстанты();
		Исключение
		КонецПопытки;
		
		НовоеПоле.ОтображениеПодсказки = ОтображениеПодсказки.ОтображатьСнизу;
		
	Иначе
		
		ГруппаЭлемента = Элементы.Добавить(ИмяОбъекта + "ГруппаПоля", Тип("ГруппаФормы"), ГруппаРодитель);
		ГруппаЭлемента.Вид = ВидГруппыФормы.ОбычнаяГруппа;
		ГруппаЭлемента.Группировка = ГруппировкаПодчиненныхЭлементовФормы.Вертикальная; 
		ГруппаЭлемента.ОтображатьЗаголовок = Ложь;
		
		НовоеПоле = Элементы.Добавить(ИмяОбъекта, Тип("ПолеФормы"), ГруппаЭлемента);
		
		НовоеПоле.ПутьКДанным = "КонстантыФормы." + ИмяОбъекта;
		
		НовоеПоле.Вид = ВидПоляФормы.ПолеВвода;
		
		Попытка
			ГруппаЭлемента.Подсказка = Константы[ИмяОбъекта].СоздатьМенеджерЗначения().ОписаниеКонстанты();
		Исключение
		КонецПопытки;
		ГруппаЭлемента.ОтображениеПодсказки = ОтображениеПодсказки.ОтображатьСнизу;
		
	КонецЕсли;	
		
КонецПроцедуры

&НаСервере
Процедура ПрименитьИЗакрытьНаСервере()
	
	НаборКонстант = РеквизитФормыВЗначение("КонстантыФормы");
	НаборКонстант.Записать();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
