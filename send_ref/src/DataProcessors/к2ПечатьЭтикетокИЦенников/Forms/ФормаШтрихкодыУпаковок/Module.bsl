
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Объект.КоличествоЭкземпляров = 1;
	Объект.НазначениеШаблона     = Перечисления.к2НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляШтрихкодовУпаковок;
	Объект.ШаблонЭтикетки        = Справочники.к2ШаблоныЭтикетокИЦенников.ШаблонПоУмолчанию(Объект.НазначениеШаблона);
	
	Если ЭтоАдресВременногоХранилища(Параметры.АдресВХранилище) Тогда
		
		ТаблицаШтрихкодов = ПолучитьИзВременногоХранилища(Параметры.АдресВХранилище);
		
		Объект.ШтрихкодыУпаковок.Загрузить(ТаблицаШтрихкодов);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Печать(Команда)
	
	ОчиститьСообщения();
	
	Если Не ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрКоманды = Новый Массив;
	ПараметрКоманды.Добавить(ПредопределенноеЗначение("Справочник.к2ШтрихкодыУпаковокТоваров.ПустаяСсылка"));
	
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
		"Обработка.к2ПечатьЭтикетокИЦенников",
		"ЭтикеткаШтрихкодыУпаковки",
		ПараметрКоманды,
		ЭтотОбъект,
		ПолучитьПараметрыДляШтрихкодовУпаковок());
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция ПолучитьПараметрыДляШтрихкодовУпаковок()
	
	ПараметрыПечати = Новый Структура;
	ПараметрыПечати.Вставить("АдресВХранилище"       , ПоместитьВоВременноеХранилище(Объект.ШтрихкодыУпаковок.Выгрузить(), УникальныйИдентификатор));
	ПараметрыПечати.Вставить("ШаблонЭтикетки"        , Объект.ШаблонЭтикетки);
	ПараметрыПечати.Вставить("КоличествоЭкземпляров" , Объект.КоличествоЭкземпляров);
	ПараметрыПечати.Вставить("СтруктураМакетаШаблона", Неопределено);
	
	Возврат ПараметрыПечати;
	
КонецФункции

#КонецОбласти