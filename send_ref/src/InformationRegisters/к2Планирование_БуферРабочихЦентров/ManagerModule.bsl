#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

Функция Таблица() Экспорт

	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	к2РабочиеЦентры.Ссылка КАК РабочийЦентр,
	               |	к2Планирование_БуферРабочихЦентров.РазмерБуфера_мин КАК РазмерБуфера_мин,
	               |	1 КАК Приоритет
	               |ПОМЕСТИТЬ втДанные
	               |ИЗ
	               |	РегистрСведений.к2Планирование_БуферРабочихЦентров КАК к2Планирование_БуферРабочихЦентров,
	               |	Справочник.к2РабочиеЦентры КАК к2РабочиеЦентры
	               |ГДЕ
	               |	к2Планирование_БуферРабочихЦентров.ВидРабочегоЦентра = ЗНАЧЕНИЕ(Справочник.к2ВидыРабочихЦентров.ПустаяСсылка)
	               |	И к2Планирование_БуферРабочихЦентров.РабочийЦентр = ЗНАЧЕНИЕ(Справочник.к2РабочиеЦентры.ПустаяСсылка)
	               |
	               |ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ
	               |	к2РабочиеЦентры.Ссылка,
	               |	к2Планирование_БуферРабочихЦентров.РазмерБуфера_мин,
	               |	2
	               |ИЗ
	               |	РегистрСведений.к2Планирование_БуферРабочихЦентров КАК к2Планирование_БуферРабочихЦентров
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.к2РабочиеЦентры КАК к2РабочиеЦентры
	               |		ПО к2Планирование_БуферРабочихЦентров.ВидРабочегоЦентра = к2РабочиеЦентры.ВидРабочегоЦентра
	               |ГДЕ
	               |	к2Планирование_БуферРабочихЦентров.РабочийЦентр = ЗНАЧЕНИЕ(Справочник.к2РабочиеЦентры.ПустаяСсылка)
	               |	И НЕ к2Планирование_БуферРабочихЦентров.ВидРабочегоЦентра = ЗНАЧЕНИЕ(Справочник.к2ВидыРабочихЦентров.ПустаяСсылка)
	               |
	               |ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ
	               |	к2Планирование_БуферРабочихЦентров.РабочийЦентр,
	               |	к2Планирование_БуферРабочихЦентров.РазмерБуфера_мин,
	               |	3
	               |ИЗ
	               |	РегистрСведений.к2Планирование_БуферРабочихЦентров КАК к2Планирование_БуферРабочихЦентров
	               |ГДЕ
	               |	к2Планирование_БуферРабочихЦентров.ВидРабочегоЦентра = ЗНАЧЕНИЕ(Справочник.к2ВидыРабочихЦентров.ПустаяСсылка)
	               |	И НЕ к2Планирование_БуферРабочихЦентров.РабочийЦентр = ЗНАЧЕНИЕ(Справочник.к2РабочиеЦентры.ПустаяСсылка)
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	втДанные.РабочийЦентр КАК РабочийЦентр,
	               |	МАКСИМУМ(втДанные.Приоритет) КАК Приоритет
	               |ПОМЕСТИТЬ втПриоритеты
	               |ИЗ
	               |	втДанные КАК втДанные
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	втДанные.РабочийЦентр
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	втПриоритеты.РабочийЦентр КАК РабочийЦентр,
	               |	втПриоритеты.Приоритет КАК Приоритет,
	               |	втДанные.РазмерБуфера_мин КАК РазмерБуфера_мин
	               |ИЗ
	               |	втПриоритеты КАК втПриоритеты
	               |		ЛЕВОЕ СОЕДИНЕНИЕ втДанные КАК втДанные
	               |		ПО втПриоритеты.РабочийЦентр = втДанные.РабочийЦентр
	               |			И втПриоритеты.Приоритет = втДанные.Приоритет";  
	
	Возврат Запрос.Выполнить().Выгрузить();

КонецФункции

#КонецОбласти

#КонецЕсли
