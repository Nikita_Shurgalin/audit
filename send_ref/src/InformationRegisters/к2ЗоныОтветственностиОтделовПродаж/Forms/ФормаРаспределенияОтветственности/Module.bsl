
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗаполнитьТаблицуГруппПланирования();
	ОбновитьСтруктуруВыводаКроссТаблицы(ЭтотОбъект, ТаблицаГруппПланирования.Выгрузить());
	ЗаполнитьАктуальныеТоварныеКатегории();
	ОбновитьНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Обновить(Команда)
	ОбновитьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура Сохранить(Команда)
	СохранитьНаСервере();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеРеквизитовФормы

&НаСервере
Процедура ОбновитьНаСервере()
	
	Для Каждого СтрокаТаблицы Из ТаблицаЗонОтветственности Цикл 
		ОбновитьДанныеПоСтрокеНаСервере(СтрокаТаблицы);
	КонецЦикла;	
	
	УстановитьУсловноеОформление();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеПоСтрокеНаСервере(СтрокаТаблицы)
	
	ОчиститьДанныеПоСтрокеНаСервере(СтрокаТаблицы);
	ЗаполнитьПоСохраненнымДанным(СтрокаТаблицы);
	

КонецПроцедуры

&НаСервере
Процедура ОчиститьДанныеПоСтрокеНаСервере(СтрокаТаблицы)
	
	Для Каждого Строка Из ТаблицаГруппПланирования Цикл 
		
		СтрокаТаблицы[ПрефиксПоляОтдел() + Строка.ИмяКолонки] = Справочники.к2ОтделыПродаж.ПустаяСсылка();
		
	КонецЦикла;	

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПоСохраненнымДанным(СтрокаТаблицы)
	
	Для Каждого Строка Из ТаблицаГруппПланирования Цикл 
		
		СтрокаТаблицы[ПрефиксПоляОтдел() + Строка.ИмяКолонки] = РегистрыСведений.к2ЗоныОтветственностиОтделовПродаж.ОтветственныйОтдел(СтрокаТаблицы.ТоварнаяКатегория, Строка.ГруппаПланирования);
		
	КонецЦикла;	

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьАктуальныеТоварныеКатегории()
	
	ТаблицаЗонОтветственности.Очистить();
	Для Каждого ТК Из МассивАктуальныхТоварныхКатегорий() Цикл 
		новаяСтрока = ТаблицаЗонОтветственности.Добавить();
		новаяСтрока.товарнаяКатегория = ТК;
	КонецЦикла;
	
КонецПроцедуры

Функция МассивАктуальныхТоварныхКатегорий()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	к2ТоварныеКатегории.Ссылка КАК Ссылка
	               |ИЗ
	               |	Справочник.к2ТоварныеКатегории КАК к2ТоварныеКатегории
	               |ГДЕ
	               |	НЕ к2ТоварныеКатегории.ПометкаУдаления
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	к2ТоварныеКатегории.Наименование";
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции	

#КонецОбласти

#Область Прочее

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	#Область ЦветаОтделов
	
		ИнициализоватьСоответствиеЦветовОтделам();

		СоответствиеЦветов = ПолучитьИзВременногоХранилища(АдресСоответствияЦветовОтделов);
		
		Для Каждого ЭлементСоответствия Из СоответствиеЦветов Цикл 
			Для Каждого Строка Из ТаблицаГруппПланирования.НайтиСтроки(Новый Структура("Активна", Истина)) Цикл 
				
				НовыйЭлементУсловногоОформления = ЭтотОбъект.УсловноеОформление.Элементы.Добавить();
				НовыйЭлементУсловногоОформления.Использование = Истина;

				ЭлементОтбора = НовыйЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
				ЭлементОтбора.Использование = Истина;
				ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТаблицаЗонОтветственности." + ПрефиксПоляОтдел() + Строка.ИмяКолонки);
				ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
				ЭлементОтбора.ПравоеЗначение = ЭлементСоответствия.Ключ;
				
				ЭлементОтбора = НовыйЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
				ЭлементОтбора.Использование = Истина;
				ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТаблицаЗонОтветственности." + ПрефиксПоляОтдел() + Строка.ИмяКолонки);
				ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Заполнено;

				НовыйЭлементУсловногоОформления.Оформление.УстановитьЗначениеПараметра("ЦветФона", ЭлементСоответствия.Значение);
				
				ОформляемоеПоле = НовыйЭлементУсловногоОформления.Поля.Элементы.Добавить();
				ОформляемоеПоле.Поле = Новый ПолеКомпоновкиДанных("ТаблицаЗонОтветственности" + ПрефиксПоляОтдел() + Строка.ИмяКолонки);
				
			КонецЦикла;
		КонецЦикла;
	
	#КонецОбласти
	
КонецПроцедуры

&НаСервере
Процедура ИнициализоватьСоответствиеЦветовОтделам()

	Если ЗначениеЗаполнено(АдресСоответствияЦветовОтделов) Тогда 
		СоответствиеЦветов = ПолучитьИзВременногоХранилища(АдресСоответствияЦветовОтделов);
	Иначе
		СоответствиеЦветов = Новый Соответствие;
	КонецЕсли;
	
	#Область ИнициализацияЦветовОтделов
	
		МассивТекущихОтделов = МассивТекущихОтделов(); 
		ГСЧ = Новый ГенераторСлучайныхЧисел;
		Для Каждого Отдел Из МассивТекущихОтделов Цикл 
			
			ЦветОтдела = СоответствиеЦветов.Получить(Отдел);
			
			Если ЦветОтдела = Неопределено Тогда 
			
				НовыйЦвет = СоответствиеУдачныхЦветовФона().Получить(МассивТекущихОтделов.Найти(Отдел));
				Если НовыйЦвет = Неопределено Тогда 
					НовыйЦвет = Новый Цвет(ГСЧ.СлучайноеЧисло(0, 255), ГСЧ.СлучайноеЧисло(0, 255), ГСЧ.СлучайноеЧисло(0, 255));
				КонецЕсли;
				
				СоответствиеЦветов.Вставить(отдел, НовыйЦвет);
				
			КонецЕсли;	
			
		КонецЦикла;	

	#КонецОбласти
	
	АдресСоответствияЦветовОтделов = ПоместитьВоВременноеХранилище(СоответствиеЦветов, Новый УникальныйИдентификатор);
	
КонецПроцедуры

&НаСервере
Функция СоответствиеУдачныхЦветовФона()
	
	соот = Новый Соответствие;
	
	соот.Вставить(0, Новый Цвет(204,255,153));
	соот.Вставить(1, Новый Цвет(204,255,255));
	соот.Вставить(2, Новый Цвет(153,204,255));
	соот.Вставить(3, Новый Цвет(204,204,255));
	соот.Вставить(4, Новый Цвет(255,153,204));
	соот.Вставить(5, Новый Цвет(255,204,153));
	соот.Вставить(6, Новый Цвет(255,255,153));
	
	Возврат соот;
	
КонецФункции	

&НаСервере
Функция МассивТекущихОтделов()
	
	МассивТекущихОтделов = Новый Массив;
	
	таблица = ТаблицаЗонОтветственности.Выгрузить();
	
	Для Каждого Строка Из ТаблицаГруппПланирования.НайтиСтроки(Новый Структура("Активна", Истина)) Цикл
		МассивДополнения = к2Коллекции.МассивУникальныхЗначений(таблица, ПрефиксПоляОтдел() + Строка.ИмяКолонки);
		ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивТекущихОтделов, МассивДополнения, Истина);
	КонецЦикла;
	
	Возврат МассивТекущихОтделов;
	
КонецФункции	

&НаСервере
Процедура СохранитьНаСервере()
	
	Для Каждого СтрокаТаблицы Из ТаблицаЗонОтветственности Цикл 
		
		Для Каждого Строка Из ТаблицаГруппПланирования.НайтиСтроки(Новый Структура("Активна", Истина)) Цикл 
			
				РегистрыСведений.к2ЗоныОтветственностиОтделовПродаж.Отразить(
					Новый Структура("ТоварнаяКатегория, ГруппаПланирования, ОтделПродаж",
					СтрокаТаблицы.ТоварнаяКатегория, Строка.ГруппаПланирования, СтрокаТаблицы[ПрефиксПоляОтдел() + Строка.ИмяКолонки]));
		КонецЦикла; 
		
	КонецЦикла;
	
КонецПроцедуры

#Область КроссТаблица

&НаСервере
Процедура ЗаполнитьТаблицуГруппПланирования()
	
	ТаблицаГруппПланирования.Очистить();
	
	Для Каждого ГруппаПланирования Из МассивГруппПланирования() Цикл 
		
		НоваяСтрока = ТаблицаГруппПланирования.Добавить();
		НоваяСтрока.ГруппаПланирования = ГруппаПланирования;
		НоваяСтрока.ИмяКолонки = СтрЗаменить(Строка(Новый УникальныйИдентификатор),"-","_");
		НоваяСтрока.Активна = Истина;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция МассивГруппПланирования()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	к2ГруппыПартнеровПланирования.Ссылка КАК Ссылка
	               |ИЗ
	               |	Справочник.к2ГруппыПартнеровПланирования КАК к2ГруппыПартнеровПланирования
	               |ГДЕ
	               |	НЕ к2ГруппыПартнеровПланирования.ПометкаУдаления
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	к2ГруппыПартнеровПланирования.Наименование";
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку(0);
	
КонецФункции

&НаСервереБезКонтекста
Процедура ОбновитьСтруктуруВыводаКроссТаблицы(Форма, ТаблицаГруппПланирования) Экспорт
	
	ИмяРеквизитаКроссТаблицы = "ТаблицаЗонОтветственности";
	ЭлементФормыКроссТаблицы = "ТаблицаЗонОтветственности";
	ТаблицаГруппПланирования = ТаблицаГруппПланирования;
	
	ДобавляемыеРеквизиты = Новый Массив();
	УдаляемыеРеквизиты = Новый Массив();
	
	ОписаниеТипов = Новый ОписаниеТипов("СправочникСсылка.к2ОтделыПродаж");
			
	// Создание реквизитов  таблицы
	Для каждого СтрокаГруппыПланирования Из ТаблицаГруппПланирования Цикл
		
		Реквизит = Новый РеквизитФормы(ПрефиксПоляОтдел() + СтрокаГруппыПланирования.ИмяКолонки, ОписаниеТипов, ИмяРеквизитаКроссТаблицы, 
			Строка(СтрокаГруппыПланирования.ГруппаПланирования));
		ДобавляемыеРеквизиты.Добавить(Реквизит);
		
	КонецЦикла;
	
	Форма.ИзменитьРеквизиты(ДобавляемыеРеквизиты, УдаляемыеРеквизиты);
	
	Для каждого СтрокаГруппыПланирования Из ТаблицаГруппПланирования Цикл
				
		Элемент = Форма.Элементы.Добавить(ЭлементФормыКроссТаблицы + ПрефиксПоляОтдел() + СтрокаГруппыПланирования.ИмяКолонки, 
			Тип("ПолеФормы"), Форма.Элементы[ЭлементФормыКроссТаблицы]);
			
		Элемент.Вид = ВидПоляФормы.ПолеФлажка;
		Элемент.ПутьКДанным = ИмяРеквизитаКроссТаблицы + "." + ПрефиксПоляОтдел() + СтрокаГруппыПланирования.ИмяКолонки;
		Элемент.Заголовок = Строка(СтрокаГруппыПланирования.ГруппаПланирования);
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПрефиксПоляОтдел()
	
	Возврат "Отдел_";
	
КонецФункции	

#КонецОбласти

#КонецОбласти

#КонецОбласти

