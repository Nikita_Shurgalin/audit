#Область ПрограммныйИнтерфейс

// Создает новый или обновляет существующий объект в ГИС Меркурий
//
// Параметры:
//  Форма	 - УправляемаяФорма - форма
//
Процедура СоздатьОбновитьЭлемент(Форма) Экспорт

	Отказ = Ложь;

	НачатьСинхронизацию(Форма, Отказ);

	Если Отказ Тогда

		Возврат;

	КонецЕсли;

	причина = "";
	Объект = Форма.Объект;

	Если ТипЗнч(Объект.Ссылка) = Тип("СправочникСсылка.к2Предприятия_Меркурий")
		Или ТипЗнч(Объект.Ссылка) = Тип("СправочникСсылка.к2ХозяйствующиеСубъекты_Меркурий") Тогда

		причина = Форма.Причина;

	КонецЕсли;

	Попытка

		к2ЗаявкиКлиентСервер_Меркурий.СоздатьИзменитьЭлементВМеркурии(Объект, Форма.Инициатор, Форма.ИмяФормы, причина);
	
		Форма.УстановитьВидимостьСинхронизации();
	
	Исключение
		
		текстОшибки = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
		
		к2МеркурийВызовСервера.ЗаписатьОшибкуВЖурналРегистрации(текстОшибки);
		
		ПрерватьСинхронизациюИзЗаОшибки(Форма, Объект, текстОшибки);
		
	КонецПопытки;
	
	Если Объект.ВыполняетсяИмпортДанных Тогда

		к2МеркурийКлиент.ОбновитьПрочитатьРезультатыОбработкиЗаявок(Форма, Объект.Ссылка);

	КонецЕсли;
	
	Если Объект.ВыполняетсяИмпортДанных Тогда

		ПодключитьДинамическийОбработчикОжидания(Форма, "ОбновитьПрочитатьРезультатыОбработкиЗаявок", Истина);

	КонецЕсли;

КонецПроцедуры

// Удаляет объект в ГИС Меркурий
//
// Параметры:
//  Форма	 - УправляемаяФорма - форма
//
Процедура УдалитьЭлемент(Форма) Экспорт

	Отказ = Ложь;

	НачатьСинхронизацию(Форма, Отказ);

	Если Отказ Тогда

		Возврат;

	КонецЕсли;

	к2ЗаявкиКлиентСервер_Меркурий.УдалитьЭлементВМеркурии(Форма.Объект, Форма.Инициатор, Форма.ИмяФормы);

	Форма.УстановитьВидимостьСинхронизации();

	Если Форма.Объект.ВыполняетсяИмпортДанных Тогда
		
		ПодключитьДинамическийОбработчикОжидания(Форма, "ОбновитьПрочитатьРезультатыОбработкиЗаявок");

	КонецЕсли;

КонецПроцедуры

// Обновляет информацию о заявке на форме
//
// Параметры:
//  Форма - УправляемаяФорма - форма
//
Процедура ПолучитьДанныеПоЗаявке(Форма) Экспорт

	Отказ = Ложь;

	Объект = Форма.Объект;
	
	Объект.ДатаЗапроса = к2МеркурийВызовСервера.ДатаСеанса();

	НачатьСинхронизацию(Форма, Отказ);

	Если Отказ Тогда

		Возврат;

	КонецЕсли;

	параметрыПодключения = к2МеркурийКлиентСерверПовтИсп.ПараметрыПодключенияПользователя(Форма.Инициатор);

	к2ЗаявкиКлиентСервер_Меркурий.ВыполнитьЗапросПоЗаявке(Объект, Форма.ИмяФормы, параметрыПодключения);

	Форма.УстановитьВидимостьСинхронизации();

	Если Объект.ВыполняетсяИмпортДанных Тогда

		ПодключитьДинамическийОбработчикОжидания(Форма, "ОбновитьПрочитатьРезультатыОбработкиЗаявок", Истина);

	КонецЕсли;

КонецПроцедуры

// Открывает форму редактирования многострочного комментария.
//
// Параметры:
//  МногострочныйТекст - Строка - произвольный текст, который необходимо отредактировать.
//  ФормаВладелец      - УправляемаяФорма - форма, в поле которой выполняется ввод комментария.
//  Заголовок          - Строка - Заголовок
Процедура ПоказатьФормуРедактированияКомментария(Знач МногострочныйТекст, Знач ФормаВладелец, Знач Заголовок = Неопределено) Экспорт

	ДополнительныеПараметры = Новый Структура("ФормаВладелец", ФормаВладелец);
	Оповещение = Новый ОписаниеОповещения("КомментарийЗавершениеВвода", к2ФормыКлиент_Меркурий, ДополнительныеПараметры);
	ЗаголовокФормы = ?(Заголовок <> Неопределено, Заголовок, НСтр("ru='Комментарий'"));
	ПоказатьВводСтроки(Оповещение, МногострочныйТекст, ЗаголовокФормы, , Истина);

КонецПроцедуры

// Обработчик оповещения для ПоказатьФормуРедактированияКомментария
//
// Параметры:
//  ВведенныйТекст			 - Строка - произвольный текст.
//  ДополнительныеПараметры	 - Структура - дополнительные параметры.
//
Процедура КомментарийЗавершениеВвода(Знач ВведенныйТекст, Знач ДополнительныеПараметры) Экспорт

	Если ВведенныйТекст = Неопределено Тогда

		Возврат;

	КонецЕсли;

	ДополнительныеПараметры.ФормаВладелец.Объект.Комментарий = ВведенныйТекст;
	ДополнительныеПараметры.ФормаВладелец.Модифицированность = Истина;

КонецПроцедуры

// Сворачивает/разворачивает панель отборов на форме
//
// Параметры:
//  Форма - УправляемаяФорма - форма
//  Видимость - Булево - флаг видимости панели
//
Процедура СвернутьРазвернутьПанельОтборов(Форма, Видимость) Экспорт

	Форма.Элементы.Панель.Видимость = Видимость;
	Форма.Элементы.ДекорацияРазвернутьПанельОтборов.Видимость = Не Видимость;
	Форма.Элементы.Группа_Панель.Ширина = ?(Видимость, 25, 0);

КонецПроцедуры

// Открывает диалог выбора периода
//
// Параметры:
//  Форма		 - УправляемаяФорма - форма-владелец
//  Период		 - СтандартныйПериод - период
//  Параметры	 - Структура - дополнительные параметры
//
Процедура ВыбратьПериодДляОтбора(Форма, Период, Параметры) Экспорт

	оповещение = Новый ОписаниеОповещения("ПредставлениеПериодаНажатиеЗавершение", к2ФормыКлиент_Меркурий, Параметры);

	Диалог = Новый ДиалогРедактированияСтандартногоПериода;
	Диалог.Период = Период;
	Диалог.Показать(Оповещение);

КонецПроцедуры

// Обработчик оповещения для ВыбратьПериодДляОтбора
//
// Параметры:
//  НовыйПериод	 - СтандартныйПериод - период
//  Параметры	 - Структура - дополнительные параметры
//
Процедура ПредставлениеПериодаНажатиеЗавершение(НовыйПериод, Параметры) Экспорт

	Если НовыйПериод = Неопределено Тогда

		Возврат;

	КонецЕсли;

	форма = Параметры.Форма;

	Если ТипЗнч(НовыйПериод) = Тип("СтандартныйПериод") Тогда

		форма[Параметры.ИмяРеквизита_Период] = НовыйПериод;

	ИначеЕсли ТипЗнч(НовыйПериод) = Тип("Дата") Тогда

		форма[Параметры.ИмяРеквизита_Период].ДатаОкончания = НовыйПериод;

	КонецЕсли;

	установленныйПериод = форма[Параметры.ИмяРеквизита_Период];

	форма[Параметры.ИмяПоля_Представление] = к2ФормыКлиентСервер_Меркурий.ПолучитьПредставлениеПериода(установленныйПериод,
																									   Параметры.Заголовок);
	к2ФормыКлиентСервер_Меркурий.УстановитьОтборПоПериоду(форма.Список.КомпоновщикНастроек.Настройки.Отбор,
														  установленныйПериод.ДатаНачала,
														  установленныйПериод.ДатаОкончания,
														  Параметры.ИмяПоляСписка);
	
	Если Параметры.Свойство("ОповещениеПослеВыбора") Тогда
		ВыполнитьОбработкуОповещения(Параметры.ОповещениеПослеВыбора, НовыйПериод);
	КонецЕсли;

КонецПроцедуры

// Обновляет форму после создания элементов по таблице импорта
//
// Параметры:
//  Форма				 - УправляемаяФорма - форма
//  созданныеЭлементы	 - Массив - созданные элементы.
//
Процедура СоздатьЭлементыПоВыбраннымСтрокамТаблицыИмпорта_Завершение(Форма, созданныеЭлементы) Экспорт

	Если созданныеЭлементы.Количество() > 0 Тогда

		Форма.Элементы.Страницы.ТекущаяСтраница = Форма.Элементы.Страница_Доступные;

	КонецЕсли;

	Форма.Элементы.Список.ВыделенныеСтроки.Очистить();

	Для Каждого цСсылка Из созданныеЭлементы Цикл

		Форма.Элементы.Список.ВыделенныеСтроки.Добавить(цСсылка);
		ОповеститьОбИзменении(цСсылка);

		Форма.Элементы.Список.ТекущаяСтрока = цСсылка;

	КонецЦикла;

КонецПроцедуры

// Обнуление данных обработчика перед запуском.
// 
// Параметры:
//  Форма - УправляемаяФорма - Форма
Процедура СброситьДинамическийОбработчикОжидания(Форма) Экспорт
	
	Форма.ПараметрыОпроса.Интервал = Форма.ПараметрыОпроса.НачальныйИнтервал;
	
КонецПроцедуры

// Подключить динамический обработчик ожидания.
// 
// Параметры:
//  Форма - УправляемаяФорма - Форма
//  ИмяПроцедуры - Строка - Имя процедуры, подключаемой в качестве обработчика ожидания
//  ЭтоПервыйВызов - Булево - Это первый вызов
Процедура ПодключитьДинамическийОбработчикОжидания(Форма, ИмяПроцедуры, ЭтоПервыйВызов = Ложь) Экспорт
	
	Если ЭтоПервыйВызов Тогда

		СброситьДинамическийОбработчикОжидания(Форма);

	КонецЕсли;
	
	ограничение = 60; // Больше минуты не делаем
	
	Форма.ПараметрыОпроса.Интервал = Мин(ограничение, Форма.ПараметрыОпроса.Интервал);

	Форма.ПодключитьОбработчикОжидания(ИмяПроцедуры, Окр(Форма.ПараметрыОпроса.Интервал), Истина);

	Форма.ПараметрыОпроса.Интервал = Форма.ПараметрыОпроса.Интервал * Форма.ПараметрыОпроса.Коэффициент;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Показывает значение активного элемента таблицы
//
// Параметры:
//  пТаблица  - ТаблицаФормы - Таблица
//  СтандартнаяОбработка - Булево - Переданный параметр для установки в Ложь при открытии.
//
Процедура ОткрытьЭлементПриВыбореВТаблице(пТаблица, СтандартнаяОбработка = Истина) Экспорт

	Если Не ТипЗнч(пТаблица) = Тип("ТаблицаФормы") Тогда

		Возврат;

	КонецЕсли;

	текДанные = пТаблица.ТекущиеДанные;
	текПоле = пТаблица.ТекущийЭлемент;

	ИмяТаблицы = пТаблица.Имя;

	Если Не СтрНачинаетсяС(текПоле.Имя, ИмяТаблицы) Тогда

		Возврат;

	КонецЕсли;

	ИмяПоля = Сред(текПоле.Имя, СтрДлина(ИмяТаблицы) + 1);

	Если Не ЗначениеЗаполнено(ИмяПоля) Тогда

		Возврат;

	КонецЕсли;

	Если Не к2ОбщегоНазначенияКлиентСервер_Меркурий.ЕстьРеквизитИлиСвойствоОбъекта(текДанные, ИмяПоля) Тогда

		Возврат;

	КонецЕсли;

	ТекущаяСсылка = текДанные[ИмяПоля];

	Если Не ЗначениеЗаполнено(ТекущаяСсылка) Тогда
		Возврат;
	КонецЕсли;
	
	навигационнаяСсылка = ПолучитьНавигационнуюСсылку(ТекущаяСсылка);

	ПерейтиПоНавигационнойСсылке(навигационнаяСсылка);

	СтандартнаяОбработка = Ложь;

КонецПроцедуры

#Область СобытияОбработкиУпаковок

Процедура ТоварыСУпаковкамиПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование, пФорма, Знач ПараметрыУпаковок) Экспорт

	Если Не НоваяСтрока Тогда

		Возврат;

	КонецЕсли;

	текДанные = Элемент.ТекущиеДанные;

	к2ФормыКлиентСервер_Меркурий.НастроитьНовуюСтрокуСУпаковками(пФорма, текДанные, Копирование, ПараметрыУпаковок);

КонецПроцедуры

Процедура ТоварыСУпаковкамиПриАктивизацииСтроки(Форма, Знач ПараметрыУпаковок) Экспорт
	
	текДанные = Форма.Элементы[ПараметрыУпаковок.ИмяПоляТовары].ТекущиеДанные;
		
	к2ФормыКлиентСервер_Меркурий.УстановитьОтборСтрокУпаковкам(Форма, текДанные, ПараметрыУпаковок);

КонецПроцедуры

Процедура ТоварыСУпаковкамиПередУдалением(Элемент, пФорма, Знач ПараметрыУпаковок) Экспорт
	
	УдалитьСтрокиУпаковки(пФорма, Элемент.ТекущиеДанные.КлючСвязи, ПараметрыУпаковок);

КонецПроцедуры

Процедура УпаковкиПриАктивизацииСтроки(пФорма, Знач ПараметрыУпаковок) Экспорт

	к2ФормыКлиентСервер_Меркурий.УстановитьОтборСтрокМаркировке(пФорма, ПараметрыУпаковок);

КонецПроцедуры

Процедура УпаковкиПередНачаломДобавления(пФорма, Знач ПараметрыУпаковок, Отказ) Экспорт

	Если пФорма.Элементы[ПараметрыУпаковок.ИмяПоляТовары].ТекущиеДанные = Неопределено Тогда

		Отказ = Истина;

	КонецЕсли;

КонецПроцедуры

Процедура УпаковкиПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование, пФорма, Знач ПараметрыУпаковок) Экспорт

	Если Не НоваяСтрока Тогда
		Возврат;
	КонецЕсли;

	текДанные = Элемент.ТекущиеДанные;

	Если Не Копирование Тогда

		текДанные[ПараметрыУпаковок.ИмяКлючаСвязи] = пФорма.Элементы[ПараметрыУпаковок.ИмяПоляТовары].ТекущиеДанные.КлючСвязи;

	КонецЕсли;

	НастроитьНовуюСтрокуУпаковки(пФорма, текДанные, Копирование, ПараметрыУпаковок);

КонецПроцедуры

Процедура УпаковкиПриОкончанииРедактирования(НоваяСтрока, пФорма, Знач ПараметрыУпаковок) Экспорт

	Если НоваяСтрока Тогда

		к2ФормыКлиентСервер_Меркурий.УстановитьОтборСтрокМаркировке(пФорма, ПараметрыУпаковок);

	КонецЕсли;

КонецПроцедуры

Процедура УпаковкиПередУдалением(Элемент, пФорма, Знач ПараметрыУпаковок) Экспорт

	УдалитьСтрокиМаркировки(пФорма, Элемент.ТекущиеДанные.КлючСвязиСМаркировкой, ПараметрыУпаковок);

КонецПроцедуры

Процедура УпаковкиПослеУдаления(Элемент, пФорма, Знач ПараметрыУпаковок) Экспорт

	ЗаполнитьКоличествоУпаковок(пФорма, ПараметрыУпаковок);

КонецПроцедуры

Процедура МаркировкаПередНачаломДобавления(пФорма, Знач ПараметрыУпаковок, Отказ) Экспорт

	Если пФорма.Элементы[ПараметрыУпаковок.ИмяПоляУпаковки].ТекущиеДанные = Неопределено Тогда

		Отказ = Истина;

	КонецЕсли;

КонецПроцедуры

Процедура МаркировкаПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование, пФорма, Знач ПараметрыУпаковок) Экспорт

	Если НоваяСтрока И Не Копирование Тогда

		Элемент.ТекущиеДанные.КлючСвязи = пФорма.Элементы[ПараметрыУпаковок.ИмяПоляУпаковки].ТекущиеДанные.КлючСвязиСМаркировкой;

	КонецЕсли;

КонецПроцедуры

Процедура МаркировкаПриОкончанииРедактирования(пФорма, Знач ПараметрыУпаковок) Экспорт

	к2ФормыКлиентСервер_Меркурий.ЗаполнитьКоличествоМаркировок(пФорма, ПараметрыУпаковок);

КонецПроцедуры

Процедура МаркировкаПослеУдаления(пФорма, Знач ПараметрыУпаковок) Экспорт

	к2ФормыКлиентСервер_Меркурий.ЗаполнитьКоличествоМаркировок(пФорма, ПараметрыУпаковок);

КонецПроцедуры

#КонецОбласти

#Область Даты

Процедура ДатаВыработкиВСтрокеНажатие(Элемент, СтандартнаяОбработка, пСтрокаПродукции) Экспорт

	СтандартнаяОбработка = Ложь;

	Если пСтрокаПродукции = Неопределено Тогда

		Возврат;

	КонецЕсли;

	параметрыОткрытия = Новый Структура;
	параметрыОткрытия.Вставить("Дата", пСтрокаПродукции.ДатаВыработки);
	параметрыОткрытия.Вставить("ДатаОкончания", пСтрокаПродукции.ДатаВыработкиОкончание);
	параметрыОткрытия.Вставить("ДатаСтрокой", пСтрокаПродукции.ДатаВыработкиСтрокой);
	параметрыОткрытия.Вставить("ЗаголовокДаты", НСтр("ru = 'Дата выработки'"));

	допПараметры = Новый Структура("СтрокаТаблицы", пСтрокаПродукции);

	описаниеОповещения = Новый ОписаниеОповещения("ДатаВыработкиВСтрокеНажатиеЗавершение", ЭтотОбъект, допПараметры);

	ОткрытьФорму("ОбщаяФорма.к2ВводДатыТовара_Меркурий",
				 параметрыОткрытия,
				 Элемент,
				 ,
				 ,
				 ,
				 описаниеОповещения,
				 РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);

КонецПроцедуры

Процедура СрокГодностиВСтрокеНажатие(Элемент, СтандартнаяОбработка, пСтрокаПродукции) Экспорт

	СтандартнаяОбработка = Ложь;

	Если пСтрокаПродукции = Неопределено Тогда

		Возврат;

	КонецЕсли;

	параметрыОткрытия = Новый Структура;
	параметрыОткрытия.Вставить("Дата", пСтрокаПродукции.ДатаОкончанияСрокаГодности);
	параметрыОткрытия.Вставить("ДатаОкончания", пСтрокаПродукции.ДатаОкончанияСрокаГодностиОкончание);
	параметрыОткрытия.Вставить("ДатаСтрокой", пСтрокаПродукции.ДатаОкончанияСрокаГодностиСтрокой);
	параметрыОткрытия.Вставить("ЗаголовокДаты", НСтр("ru = 'Срок годности'"));

	допПараметры = Новый Структура("СтрокаТаблицы", пСтрокаПродукции);

	описаниеОповещения = Новый ОписаниеОповещения("СрокГодностиПредставлениеНажатиеЗавершение", ЭтотОбъект, допПараметры);

	ОткрытьФорму("ОбщаяФорма.к2ВводДатыТовара_Меркурий",
				 параметрыОткрытия,
				 Элемент,
				 ,
				 ,
				 ,
				 описаниеОповещения,
				 РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);

КонецПроцедуры

Процедура ЗаполнитьПредставлениеДатыВыработкиВСтроке(пСтрокаПродукции) Экспорт

	Если ЗначениеЗаполнено(пСтрокаПродукции.ДатаВыработкиСтрокой)
		И ЗначениеЗаполнено(пСтрокаПродукции.ДатаВыработки) Тогда
		
		пСтрокаПродукции.ДатаВыработкиСтрокой = "";
		
	КонецЕсли;
	
	пСтрокаПродукции.ДатаВыработкиПредставление = к2МеркурийКлиентСервер.ПредставлениеДатыТоваров(пСтрокаПродукции.ДатаВыработки,
																								  пСтрокаПродукции.ДатаВыработкиОкончание,
																								  пСтрокаПродукции.ДатаВыработкиСтрокой);

КонецПроцедуры

Процедура ЗаполнитьПредставлениеСрокаГодностиВСтроке(пСтрокаПродукции) Экспорт

	Если ЗначениеЗаполнено(пСтрокаПродукции.ДатаОкончанияСрокаГодностиСтрокой)
		И ЗначениеЗаполнено(пСтрокаПродукции.ДатаОкончанияСрокаГодности) Тогда
		
		пСтрокаПродукции.ДатаОкончанияСрокаГодностиСтрокой = "";
		
	КонецЕсли;
	
	пСтрокаПродукции.СрокГодностиПредставление = к2МеркурийКлиентСервер.ПредставлениеДатыТоваров(пСтрокаПродукции.ДатаОкончанияСрокаГодности,
																								 пСтрокаПродукции.ДатаОкончанияСрокаГодностиОкончание,
																								 пСтрокаПродукции.ДатаОкончанияСрокаГодностиСтрокой);
КонецПроцедуры

#КонецОбласти

#Область Регионализация

Процедура Регионализация_ПриОткрытии(Форма) Экспорт

	Для Каждого цСтрока Из Форма.НеобходимыеУсловияПеремещения.ПолучитьЭлементы() Цикл
		
		Форма.Элементы.НеобходимыеУсловияПеремещения.Развернуть(цСтрока.ПолучитьИдентификатор(), Истина);
		
	КонецЦикла;

КонецПроцедуры

Процедура НеобходимыеУсловияПеремещенияВыполненоПриИзменении(Форма, Знач пСтрокаНеобходимыхУсловий, Знач пЕстьВидПродукции = Истина) Экспорт
	
	Если пСтрокаНеобходимыхУсловий.Выполнено Тогда

		ДобавитьСтрокуВСоблюдаемыеУсловия(Форма, пСтрокаНеобходимыхУсловий, пЕстьВидПродукции);

	Иначе

		УдалитьСтрокуСоблюдаемыхУсловий(Форма, пСтрокаНеобходимыхУсловий, пЕстьВидПродукции);

	КонецЕсли;

	к2ФормыКлиентСервер_Меркурий.ОбновитьОтметкуВыполненоГруппеНеобходимыхУсловийПеремещения(пСтрокаНеобходимыхУсловий.ПолучитьРодителя());
	
КонецПроцедуры

Процедура ВыбратьВсеСоблюдаемыеУсловия(Форма) Экспорт
	
	Форма.Объект.СоблюдаемыеУсловияПеремещения.Очистить();
	
	Для Каждого цСтрока1 Из Форма.НеобходимыеУсловияПеремещения.ПолучитьЭлементы() Цикл
		
		цСтрока1.Выполнено = Истина;
		
		Для Каждого цСтрока2 Из цСтрока1.ПолучитьЭлементы() Цикл
			
			цСтрока2.Выполнено = Истина;
			
			Для Каждого цСтрока3 Из цСтрока2.ПолучитьЭлементы() Цикл
				
				цСтрока3.Выполнено = Истина;
				
				Для Каждого цСтрока4 Из цСтрока3.ПолучитьЭлементы() Цикл
					
					цСтрока4.Выполнено = Истина;
					
					новСтрока = Форма.Объект.СоблюдаемыеУсловияПеремещения.Добавить();
					ЗаполнитьЗначенияСвойств(новСтрока, цСтрока4);
					новСтрока.Формулировка = цСтрока4.Представление;
					
				КонецЦикла;
				
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область РаботаСУпаковками

Процедура УдалитьСтрокиУпаковки(Форма, Знач пКлючСтроки, Знач ПараметрыУпаковок)

	тчУпаковки = Форма.Объект["ФизическаяУпаковка" + ПараметрыУпаковок.ПостфиксТЧ];
	отборУпаковки = Новый Структура(ПараметрыУпаковок.ИмяКлючаСвязи, пКлючСтроки);

	Для Каждого цСтрокаУпаковки Из тчУпаковки.НайтиСтроки(отборУпаковки) Цикл

		УдалитьСтрокиМаркировки(Форма, цСтрокаУпаковки.КлючСвязиСМаркировкой, ПараметрыУпаковок);
		тчУпаковки.Удалить(цСтрокаУпаковки);

	КонецЦикла;

КонецПроцедуры

Процедура УдалитьСтрокиМаркировки(Форма, Знач пКлючСвязиСМаркировкой, ПараметрыУпаковок)

	тчМаркировки = Форма.Объект["Маркировка" + ПараметрыУпаковок.ПостфиксТЧ];

	отбор = Новый Структура("КлючСвязи", пКлючСвязиСМаркировкой);

	Для Каждого цСтрокаМаркировки Из тчМаркировки.НайтиСтроки(отбор) Цикл

		тчМаркировки.Удалить(цСтрокаМаркировки);

	КонецЦикла;

КонецПроцедуры

Процедура ЗаполнитьКоличествоУпаковок(Форма, ПараметрыУпаковок)

	текДанные = Форма.Элементы[ПараметрыУпаковок.ИмяПоляТовары].ТекущиеДанные;

	к2ФормыКлиентСервер_Меркурий.ЗаполнитьКоличествоУпаковокВСтроке(текДанные, Форма, ПараметрыУпаковок);

КонецПроцедуры

Процедура НастроитьНовуюСтрокуУпаковки(Форма, СтрокаУпаковки, Знач Копирование, Знач ПараметрыУпаковок)
	
	ключСвязиИсточник = СтрокаУпаковки.КлючСвязиСМаркировкой;

	СтрокаУпаковки.КлючСвязиСМаркировкой = к2ФормыКлиентСервер_Меркурий.КлючСвязиСМаркировкой(Форма, ПараметрыУпаковок);

	Если Копирование Тогда

		СтрокаУпаковки.КоличествоМаркировок = к2ФормыКлиентСервер_Меркурий.СкопироватьСтрокиМаркировки(Форма,
																	 СтрокаУпаковки.КлючСвязиСМаркировкой,
																	 ключСвязиИсточник,
																	 ПараметрыУпаковок);
	КонецЕсли;

	к2ФормыКлиентСервер_Меркурий.ЗаполнитьКоличествоМаркировок(Форма, ПараметрыУпаковок);
	к2ФормыКлиентСервер_Меркурий.УстановитьОтборСтрокМаркировке(Форма, ПараметрыУпаковок);
	
КонецПроцедуры

#КонецОбласти

#Область Даты

Процедура ДатаВыработкиВСтрокеНажатиеЗавершение(Результат, ДополнительныеПараметры) Экспорт

	Если Не Результат = Неопределено Тогда

		ДополнительныеПараметры.СтрокаТаблицы.ДатаВыработки = Результат.Дата;
		ДополнительныеПараметры.СтрокаТаблицы.ДатаВыработкиОкончание = Результат.ДатаОкончания;
		ДополнительныеПараметры.СтрокаТаблицы.ДатаВыработкиСтрокой = Результат.ДатаСтрокой;
		ДополнительныеПараметры.СтрокаТаблицы.ДатаВыработкиПредставление = Результат.Представление;

	КонецЕсли;

КонецПроцедуры

Процедура СрокГодностиПредставлениеНажатиеЗавершение(Результат, ДополнительныеПараметры) Экспорт

	Если Не Результат = Неопределено Тогда

		ДополнительныеПараметры.СтрокаТаблицы.ДатаОкончанияСрокаГодности = Результат.Дата;
		ДополнительныеПараметры.СтрокаТаблицы.ДатаОкончанияСрокаГодностиОкончание = Результат.ДатаОкончания;
		ДополнительныеПараметры.СтрокаТаблицы.ДатаОкончанияСрокаГодностиСтрокой = Результат.ДатаСтрокой;
		ДополнительныеПараметры.СтрокаТаблицы.СрокГодностиПредставление = Результат.Представление;

	КонецЕсли;

КонецПроцедуры

#КонецОбласти

// Выполняет подготовительные действия с формой для синхронизации
//
// Параметры:
//  Форма	 - УправляемаяФорма  - форма элемента требующего синхронизацию
//  Объект	 - ДанныеФормыКоллекция - данные объекта хранимые в форме.
//  Отказ	 - Булево - признак отказа от синхронизации.
//
Процедура НачатьСинхронизацию(Форма, Отказ)

	Объект = Форма.Объект;
	
	ПроверитьИнициатора(Форма, Отказ);

	типОбъекта = ТипЗнч(Объект.Ссылка);
	
	проверятьПричину = типОбъекта = Тип("СправочникСсылка.к2Предприятия_Меркурий");

	Если проверятьПричину И Не ЗначениеЗаполнено(Форма.Причина) Тогда

		к2ОбщегоНазначенияКлиентСервер_Меркурий.СообщитьПользователю(НСтр("ru='Необходимо указать причину'"), , "Причина", , Отказ);

	КонецЕсли;

	Если Отказ Тогда

		Возврат;

	КонецЕсли;

	Объект.ВыполняетсяИмпортДанных = Истина;
	Форма.ДатаНачалаСинхронизации = к2МеркурийВызовСервера.ДатаСеанса();

	Если типОбъекта = Тип("ДокументСсылка.к2Инвентаризация_Меркурий")
		ИЛИ типОбъекта = Тип("ДокументСсылка.к2ИсходящаяПартия_Меркурий")
		ИЛИ типОбъекта = Тип("ДокументСсылка.к2ОбъединениеЗаписейЖурнала_Меркурий")
		ИЛИ типОбъекта = Тип("ДокументСсылка.к2ПроизводственнаяПартия_Меркурий") Тогда
		
		структЗаписи = Новый Структура;
		структЗаписи.Вставить("РежимЗаписи", РежимЗаписиДокумента.Проведение);
		
		записан = Форма.Записать(структЗаписи);
		
	Иначе
	
		записан = Форма.Записать();
	
	КонецЕсли;

	Если записан Тогда
		
		Форма.УстановитьВидимостьСинхронизации();
		Форма.РазблокироватьДанныеФормыДляРедактирования();

	Иначе

		Объект.ВыполняетсяИмпортДанных = Ложь;
		Форма.ДатаНачалаСинхронизации = Дата( 1, 1, 1);
		Отказ = Истина;

	КонецЕсли;

КонецПроцедуры

Процедура ПрерватьСинхронизациюИзЗаОшибки(Форма, Объект, Знач пТекстОшибки)

	Форма.Прочитать();

	Объект.ВыполняетсяИмпортДанных = Ложь;

	Форма.Записать();

	Форма.ДатаНачалаСинхронизации = Дата(1, 1, 1);
	Форма.УстановитьВидимостьСинхронизации();
	Форма.РазблокироватьДанныеФормыДляРедактирования();

	к2ФормыКлиентСервер_Меркурий.УстановитьВФормуОшибкуОбмена(Форма, пТекстОшибки);

КонецПроцедуры

Процедура ПроверитьИнициатора(Знач Форма, Отказ)

	Если Не ЗначениеЗаполнено(Форма.Инициатор) Тогда

		к2ОбщегоНазначенияКлиентСервер_Меркурий.СообщитьПользователю(НСтр("ru='Пользователь ГИС Меркурий не назначен'"),
																	 ,
																	 "Инициатор",
																	 ,
																	 Отказ);

	КонецЕсли;

КонецПроцедуры

#Область Регионализация

Процедура ДобавитьСтрокуВСоблюдаемыеУсловия(Форма, пСтрокаДерева, Знач пЕстьВидПродукции)
	
	найдено = Ложь;
	
	Для Каждого цСтрокаСоблюдаемогоУсловия Из Форма.Объект.СоблюдаемыеУсловияПеремещения Цикл
		
		Если цСтрокаСоблюдаемогоУсловия.Условие = пСтрокаДерева.Условие Тогда
			
			Если пЕстьВидПродукции
				И Не цСтрокаСоблюдаемогоУсловия.ВидПродукции = пСтрокаДерева.ВидПродукции Тогда
				
				Продолжить;
				
			КонецЕсли;
			
			найдено = Истина;
			Прервать;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Если Не найдено Тогда
		
		новСтрока = Форма.Объект.СоблюдаемыеУсловияПеремещения.Добавить();
		
		Если пЕстьВидПродукции Тогда
			
			новСтрока.ВидПродукции = пСтрокаДерева.ВидПродукции;
			
		КонецЕсли;
		
		новСтрока.Условие      = пСтрокаДерева.Условие;
		новСтрока.Формулировка = пСтрокаДерева.Представление;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура УдалитьСтрокуСоблюдаемыхУсловий(Форма, пСтрокаДерева, Знач пЕстьВидПродукции)

	массивДляУдаления = Новый Массив;

	Для Каждого цСтрокаСоблюдаемогоУсловия Из Форма.Объект.СоблюдаемыеУсловияПеремещения Цикл

		Если цСтрокаСоблюдаемогоУсловия.Условие = пСтрокаДерева.Условие Тогда
			
			Если пЕстьВидПродукции
				И Не цСтрокаСоблюдаемогоУсловия.ВидПродукции = пСтрокаДерева.ВидПродукции Тогда
				
				Продолжить;
				
			КонецЕсли;

			массивДляУдаления.Добавить(цСтрокаСоблюдаемогоУсловия);

		КонецЕсли;

	КонецЦикла;

	Для Каждого цСтрокаДляУдаления Из массивДляУдаления Цикл

		Форма.Объект.СоблюдаемыеУсловияПеремещения.Удалить(цСтрокаДляУдаления);

	КонецЦикла;

КонецПроцедуры

#КонецОбласти

#КонецОбласти