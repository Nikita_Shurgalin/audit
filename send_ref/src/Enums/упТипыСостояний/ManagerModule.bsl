#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Функция ПолучитьТипПоЭлементуСхемы( Знач пТипЭлементаСхемы ) Экспорт
	
	Если Не ТипЗнч( пТипЭлементаСхемы ) = Тип( "Тип" ) Тогда
		
		пТипЭлементаСхемы = ТипЗнч( пТипЭлементаСхемы );
		
	КонецЕсли;
	
	Тип = Перечисления.упТипыСостояний.ПустаяСсылка();
	
	// определяем тип состояния
	Если пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыСтарт") Тогда
		
		Тип = Перечисления.упТипыСостояний.Начало;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыЗавершение") Тогда
		
		Тип = Перечисления.упТипыСостояний.Завершение;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыДействие") Тогда
		
		Тип = Перечисления.упТипыСостояний.Действие;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыУсловие") Тогда
		
		Тип = Перечисления.упТипыСостояний.Условие;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыВыборВарианта") Тогда
		
		Тип = Перечисления.упТипыСостояний.ВыборВарианта;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыРазделение") Тогда
		
		Тип = Перечисления.упТипыСостояний.Разделение;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыСлияние") Тогда
		
		Тип = Перечисления.упТипыСостояний.Слияние;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыОбработка") Тогда
		
		Тип = Перечисления.упТипыСостояний.Обработка;
		
	ИначеЕсли пТипЭлементаСхемы = Тип("ЭлементГрафическойСхемыВложенныйБизнесПроцесс") Тогда
		
		Тип = Перечисления.упТипыСостояний.ВложенныйБизнесПроцесс;
		
	Иначе
		
		упГрафическаяСхема.СообщитьОбОшибке( НСтр( "ru='Не удалось определить тип состояния. Тип элемента: '" ) + пТипЭлементаСхемы );
		
	КонецЕсли;
	
	Возврат Тип;
	
КонецФункции


Функция АктивностиПользователя() Экспорт
	
	АктивностиПользователя = Новый Массив;
	
	АктивностиПользователя.Добавить( Перечисления.упТипыСостояний.Начало );
	АктивностиПользователя.Добавить( Перечисления.упТипыСостояний.Действие );
	АктивностиПользователя.Добавить( Перечисления.упТипыСостояний.Завершение );
	
	Возврат АктивностиПользователя;
	
КонецФункции

Функция УсловныеТипы() Экспорт
	
	УсловныеТипы = Новый Массив;
	
	УсловныеТипы.Добавить( Перечисления.упТипыСостояний.Условие );
	УсловныеТипы.Добавить( Перечисления.упТипыСостояний.ВыборВарианта );
	
	Возврат УсловныеТипы;
	
КонецФункции

Функция Слияния() Экспорт
	
	Слияния = Новый Массив;
	
	Слияния.Добавить( Перечисления.упТипыСостояний.Слияние );
	
	Возврат Слияния;
	
КонецФункции	//Слияния

Функция СтартовыеСостояния() Экспорт
	
	массивСтартовыхСостояний = Новый Массив;
	
	массивСтартовыхСостояний.Добавить( Перечисления.упТипыСостояний.Начало );
	
	Возврат массивСтартовыхСостояний;
	
КонецФункции	//СтартовыеСостояния

Функция ЭтоСтарт( пТип ) Экспорт
	
	Возврат Не СтартовыеСостояния().Найти( пТип ) = Неопределено;
	
КонецФункции	//ЭтоСтарт

Функция ЭтоСлияние( пТип ) Экспорт
	
	Возврат Не Слияния().Найти( пТип ) = Неопределено;
	
КонецФункции	//ЭтоСтарт


Функция ЭтоЗавершение( пТип ) Экспорт
	
	Возврат ( пТип = Перечисления.упТипыСостояний.Завершение );
	
КонецФункции	//ЭтоЗавершение

Функция ЭтоОбработка( пТип ) Экспорт
	
	Возврат ( пТип = Перечисления.упТипыСостояний.Обработка );
	
КонецФункции	//ЭтоСтарт

Функция ЭтоУсловнаяАктивность( пТип ) Экспорт
	
	Возврат Не УсловныеТипы().Найти( пТип ) = Неопределено;
	
КонецФункции	//ЭтоУсловнаяАктивность

Функция ЭтоУсловие( пТип ) Экспорт
	
	Возврат пТип = Перечисления.упТипыСостояний.Условие;
	
КонецФункции	//ЭтоУсловнаяАктивность

Функция ЭтоРазделение( пТип ) Экспорт
	
	Возврат пТип = Перечисления.упТипыСостояний.Разделение;
	
КонецФункции	//ЭтоРазделение

Функция ЭтоВыборВарианта( пТип ) Экспорт
	
	Возврат пТип = Перечисления.упТипыСостояний.ВыборВарианта;
	
КонецФункции	//ЭтоРазделение

Функция ЭтоПользовательскаяАктивность( пТип ) Экспорт
	
	Возврат Не АктивностиПользователя().Найти( пТип ) = Неопределено;
	
КонецФункции	//ЭтоУсловнаяАктивность


Функция ЭтоВложенныйБизнесПроцесс( пТип ) Экспорт
	
	Возврат пТип = Перечисления.упТипыСостояний.ВложенныйБизнесПроцесс;
	
КонецФункции

#КонецЕсли