
#Область ПрограммныйИнтерфейс

Функция ИмяФайлаДляВыгрузки(ЗаданиеНаМаркировку) Экспорт
	
	Возврат "OUT_MARK_" + ЗаданиеНаМаркировку.УникальныйИдентификатор() + ".xml";
		
КонецФункции

Функция ИмяФайлаДляЗагрузки(ЗаданиеНаМаркировку) Экспорт
	
	ГуидЗадания = ЗаданиеНаМаркировку.УникальныйИдентификатор();
	
	ИмяФайла = "IN_AGGREGATION_LVL0_" + ГуидЗадания;
	
	Возврат ИмяФайла;
	
КонецФункции

#КонецОбласти