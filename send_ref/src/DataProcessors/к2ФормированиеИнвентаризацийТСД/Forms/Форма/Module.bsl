
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Если Не к2ПроизводствоСервер.ЕстьСохраненныеНастройкиФормы(ИмяФормы) Тогда
		ПриЗагрузкеДанныхПриСозданииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	ПриЗагрузкеДанныхПриСозданииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(, ЭтотОбъект, "СканерШтрихкода");
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Включить(СкоростьПрокрутки);
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ВнешнееСобытие(Источник, Событие, Данные);
	
	Если Событие = "Штрихкод" И ВводДоступен() Тогда
		
		ОбработатьШтрихКод(СокрЛП(Данные));
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_Отключить();
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(, ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	СкладПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЯчейкаПриИзменении(Элемент)
	
	ОбновитьДанныеФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ИнвентаризацияПриИзменении(Элемент)
	
	ИнвентаризацияПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ГруппаСтраницыПриСменеСтраницы(Элемент, ТекущаяСтраница)
	
	Если ТекущаяСтраница = Элементы.ГруппаСтраницаТовары И Не ПроверитьЗаполнениеФормы() Тогда
		
		Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаСтраницаНастройки;
		
		ПоказатьОшибки();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ШтрихКодПриИзменении(Элемент)
	
	ОбработатьШтрихКод(ШтрихКод);
	
КонецПроцедуры

&НаКлиенте
Процедура СкоростьПрокруткиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ИмяРеквизита = Элементы.СкоростьПрокрутки.Имя;
	
	ОткрытьКалькуляторСенсорДляРеквизита(СтруктураПараметровКалькуляторСенсор_Число(ИмяРеквизита), ИмяРеквизита);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТовары

&НаКлиенте
Процедура ТоварыПриАктивизацииСтроки(Элемент)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_ПриАктивизацииСтроки();
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	
	к2ПроизводствоКлиент.КомпонентаУправленияСкроллингом_НачалоПеретаскивания(ПараметрыПеретаскивания, Выполнение);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = Элементы.ТоварыКоличествоФакт.Имя
		Или Поле.Имя = Элементы.ТоварыКоличество1Факт.Имя
		Или Поле.Имя = Элементы.ТоварыКоличество2Факт.Имя Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ОткрытьФормуКалькуляторСенсорДляТаблицы(ВыбраннаяСтрока, Поле.Имя);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаСкладВыбрать(Команда)
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("ИспользоватьАдресноеХранение", Ложь);
	СтруктураОтбора.Вставить("ЭтоГруппа", Ложь);
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("ТекущаяСтрока", Объект.Склад);
	СтруктураПараметров.Вставить("Отбор", СтруктураОтбора);
	
	ОткрытьФорму("Справочник.Склады.Форма.ФормаВыбораСенсор", СтруктураПараметров, Элементы.Склад);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаСкладОчистить(Команда)
	
	Объект.Склад = ПредопределенноеЗначение("Справочник.Склады.ПустаяСсылка");
	
	ОбновитьДанныеФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЯчейкаВыбрать(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ТекущаяСтрока", Объект.Ячейка);
	ПараметрыФормы.Вставить("Отбор", Новый Структура("Владелец", Объект.Склад));
	
	ОткрытьФорму("Справочник.к2СкладскиеЯчейки.Форма.ФормаВыбораСенсор", ПараметрыФормы, Элементы.Ячейка);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЯчейкаОчистить(Команда)
	
	Объект.Ячейка = ПредопределенноеЗначение("Справочник.к2СкладскиеЯчейки.ПустаяСсылка");
	
	ОбновитьДанныеФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаДокументИнвентаризацииВыбрать(Команда)
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("Проведен", Ложь);
	СтруктураОтбора.Вставить("Склад", Объект.Склад);
	СтруктураОтбора.Вставить("ПометкаУдаления", Ложь);
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ТекущаяСтрока", Объект.Инвентаризация);
	ПараметрыФормы.Вставить("Отбор", СтруктураОтбора);
	
	ОткрытьФорму("Документ.к2Инвентаризация.Форма.ФормаВыбораСенсор", ПараметрыФормы, Элементы.Инвентаризация);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаДокументИнвентаризацииОчистить(Команда)
	
	Объект.Инвентаризация = ПредопределенноеЗначение("Документ.к2Инвентаризация.ПустаяСсылка");
	
	ОбновитьДанныеФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЗавершитьРаботу(Команда)
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаКалькуляторШтрихКод(Команда)
	
	ИмяРеквизита = Элементы.ШтрихКод.Имя;
	
	ОткрытьКалькуляторСенсорДляРеквизита(СтруктураПараметровКалькуляторСенсор_Строка_60(), ИмяРеквизита);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаОК(Команда)
	
	ТекстОшибки = Неопределено;
	
	Элементы.ГруппаСтраницыОбщие.ТекущаяСтраница = Элементы.ГруппаСтраницаРабочая;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбработчикиСобытийЭлементовФормы

&НаСервере
Процедура СкладПриИзмененииНаСервере()
	
	ЗаполнитьИнвентаризациюПоСкладу();
	
	ОбновитьДанныеФормыНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьИнвентаризациюПоСкладу()
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	к2Инвентаризация.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.к2Инвентаризация КАК к2Инвентаризация
	|ГДЕ
	|	к2Инвентаризация.Склад = &Склад
	|	И НЕ к2Инвентаризация.ПометкаУдаления
	|	И НЕ к2Инвентаризация.Статус = ЗНАЧЕНИЕ(Перечисление.к2СтатусыИнвентаризации.Выполнено)
	|	И НЕ к2Инвентаризация.Проведен
	|
	|УПОРЯДОЧИТЬ ПО
	|	к2Инвентаризация.Дата УБЫВ";
	#КонецОбласти
	
	Запрос.УстановитьПараметр("Склад", Объект.Склад);
	
	Объект.Инвентаризация = к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).Ссылка;
	
КонецПроцедуры

&НаСервере
Процедура ИнвентаризацияПриИзмененииНаСервере()
	
	Объект.Склад = Объект.Инвентаризация.Склад;
	Объект.Ячейка = Неопределено;
	
	ОбновитьДанныеФормыНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбновлениеРеквизитовФормы

&НаКлиенте
Процедура ОбновитьДанныеФормы()
	
	ОбновитьДанныеФормыНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеФормыНаСервере()
	
	ОбновитьТаблицуТовары();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьТаблицуТовары()
	
	Объект.Товары.Очистить();
	
	Если Не ЗначениеЗаполнено(Объект.Инвентаризация) Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	к2ИнвентаризацияТары.НомерСтроки КАК НомерСтроки,
	|	к2ИнвентаризацияТары.Тара КАК Тара,
	|	к2ИнвентаризацияТары.Ячейка КАК Ячейка,
	|	к2ИнвентаризацияТары.Ответственный КАК Ответственный
	|ПОМЕСТИТЬ Тары
	|ИЗ
	|	Документ.к2Инвентаризация.Тары КАК к2ИнвентаризацияТары
	|ГДЕ
	|	к2ИнвентаризацияТары.Ссылка = &ДокументИнвентаризации
	|	И к2ИнвентаризацияТары.Ячейка = &Ячейка
	|	И к2ИнвентаризацияТары.Ответственный = &Ответственный
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	к2ИнвентаризацияТоварыНаТаре.Тара КАК Тара,
	|	к2ИнвентаризацияТоварыНаТаре.Номенклатура КАК Номенклатура,
	|	к2ИнвентаризацияТоварыНаТаре.Характеристика КАК Характеристика,
	|	к2ИнвентаризацияТоварыНаТаре.Серия КАК Серия,
	|	к2ИнвентаризацияТоварыНаТаре.КоличествоФакт КАК КоличествоФакт,
	|	к2ИнвентаризацияТоварыНаТаре.Количество1Факт КАК Количество1Факт,
	|	к2ИнвентаризацияТоварыНаТаре.Количество2Факт КАК Количество2Факт
	|ПОМЕСТИТЬ ТоварыНаТаре
	|ИЗ
	|	Документ.к2Инвентаризация.ТоварыНаТаре КАК к2ИнвентаризацияТоварыНаТаре
	|ГДЕ
	|	к2ИнвентаризацияТоварыНаТаре.Ссылка = &ДокументИнвентаризации
	|	И к2ИнвентаризацияТоварыНаТаре.Тара В
	|			(ВЫБРАТЬ
	|				Тары.Тара КАК Тара
	|			ИЗ
	|				Тары КАК Тары)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Тары.НомерСтроки КАК НомерСтроки,
	|	Тары.Тара КАК Тара,
	|	Тары.Ячейка КАК Ячейка,
	|	Тары.Ответственный КАК Ответственный,
	|	ТоварыНаТаре.Номенклатура КАК Номенклатура,
	|	ТоварыНаТаре.Характеристика КАК Характеристика,
	|	ТоварыНаТаре.Серия КАК Серия,
	|	ПРЕДСТАВЛЕНИЕ(ТоварыНаТаре.Номенклатура) КАК НоменклатураПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ТоварыНаТаре.Характеристика) КАК ХарактеристикаПредставление,
	|	ПРЕДСТАВЛЕНИЕ(ТоварыНаТаре.Серия) КАК СерияПредставление,
	|	ТоварыНаТаре.КоличествоФакт КАК КоличествоФакт,
	|	ТоварыНаТаре.Количество1Факт КАК Количество1Факт,
	|	ТоварыНаТаре.Количество2Факт КАК Количество2Факт
	|ИЗ
	|	Тары КАК Тары
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТоварыНаТаре КАК ТоварыНаТаре
	|		ПО Тары.Тара = ТоварыНаТаре.Тара";
		
	Запрос.УстановитьПараметр("ДокументИнвентаризации", Объект.Инвентаризация);
	Запрос.УстановитьПараметр("Ячейка", Объект.Ячейка);
	Запрос.УстановитьПараметр("Ответственный", Объект.Ответственный);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		НоваяСтрока = Объект.Товары.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Выборка);
		НоваяСтрока.НоменклатураПредставление = НоменклатураКлиентСервер.ПредставлениеНоменклатуры(Выборка.НоменклатураПредставление, Выборка.ХарактеристикаПредставление, Выборка.СерияПредставление);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область ПроверитьЗаполнениеФормы

&НаКлиенте
Функция ПроверитьЗаполнениеФормы()
	
	Отказ = Ложь;
	
	ПроверитьЗаполнениеФормыНаСервере(Отказ);
	
	Возврат Не Отказ;
	
КонецФункции

&НаСервере
Процедура ПроверитьЗаполнениеФормыНаСервере(Отказ)
	
	Если Не ПроверитьЗаполнение() Тогда
		
		Для Каждого Сообщение Из ПолучитьСообщенияПользователю(Истина) Цикл
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(Сообщение.Текст, ТекстОшибки, Отказ);
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область КалькуляторСенсор

&НаКлиенте
Функция СтруктураПараметровКалькуляторСенсор_Строка_60()
	
	СтруктураПараметров = к2ПроизводствоКлиентСервер.СтруктураПараметровКалькуляторСенсор();
	СтруктураПараметров.Тип = ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Строка");
	СтруктураПараметров.ДлинаСтроки = 60;
		
	Возврат СтруктураПараметров;
	
КонецФункции

&НаКлиенте
Функция СтруктураПараметровКалькуляторСенсор_Число(ИмяРеквизита)
	
	СтруктураПараметров = к2ПроизводствоКлиентСервер.СтруктураПараметровКалькуляторСенсор();
	СтруктураПараметров.Тип = ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Число");
	
	Если ИмяРеквизита = Элементы.СкоростьПрокрутки.Имя Тогда
	
		СтруктураПараметров.ДлинаЧисла = 15;
		СтруктураПараметров.ТочностьЧисла = 1;
		
	Иначе
		
		СтруктураПараметров.ДлинаЧисла = 15;
		СтруктураПараметров.ТочностьЧисла = 3;
		
	КонецЕсли;
	
	Возврат СтруктураПараметров;
	
КонецФункции

&НаКлиенте
Процедура ОткрытьКалькуляторСенсорДляРеквизита(СтруктураПараметров, ИмяРеквизита)
	
	Оповещение = Новый ОписаниеОповещения("ПослеЗакрытияФормыКалькулятораДляРеквизита", ЭтотОбъект, Новый Структура("ИмяРеквизита", ИмяРеквизита));
		
	ОткрытьФорму("ОбщаяФорма.к2КалькуляторСенсор", СтруктураПараметров, ЭтотОбъект, , , , Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗакрытияФормыКалькулятораДляРеквизита(РезультатЗакрытия, ДополнительныеПараметры) Экспорт
	
	Если РезультатЗакрытия = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ИмяРеквизита = ДополнительныеПараметры.ИмяРеквизита;
	
	Если ИмяРеквизита = Элементы.ШтрихКод.Имя Тогда
		
		ОбработатьШтрихКод(РезультатЗакрытия.Результат);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыТовары

&НаКлиенте
Процедура ОткрытьФормуКалькуляторСенсорДляТаблицы(ВыбраннаяСтрока, ИмяПоля)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Тип", ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Число"));
	ПараметрыФормы.Вставить("ДлинаЧисла", 15);
	ПараметрыФормы.Вставить("Точность", 3);
	
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ТекущаяСтрока", ВыбраннаяСтрока);
	ДополнительныеПараметры.Вставить("ИмяКоличества", ИмяКоличества(ИмяПоля));
	
	Оповещение = Новый ОписаниеОповещения("ПослеОткрытияФормыКалькуляторСенсорДляТаблицы", ЭтотОбъект, ДополнительныеПараметры);
	
	ОткрытьФорму("ОбщаяФорма.к2КалькуляторСенсор", ПараметрыФормы, , , , , Оповещение);
	
КонецПроцедуры

&НаКлиенте
Функция ИмяКоличества(ИмяПоля)
	
	Если ИмяПоля = Элементы.ТоварыКоличество2Факт.Имя Тогда
		ИмяКоличества = "Количество2Факт";
	ИначеЕсли ИмяПоля = Элементы.ТоварыКоличество1Факт.Имя Тогда
		ИмяКоличества = "Количество1Факт";
	Иначе
		ИмяКоличества = "КоличествоФакт";
	КонецЕсли;
	
	Возврат ИмяКоличества;
	
КонецФункции

&НаКлиенте
Процедура ПослеОткрытияФормыКалькуляторСенсорДляТаблицы(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Или Не ТипЗнч(Результат) = Тип("Структура") Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные = Объект.Товары.НайтиПоИдентификатору(ДополнительныеПараметры.ТекущаяСтрока);
	ТекущиеДанные[ДополнительныеПараметры.ИмяКоличества] = Результат.Результат;
	
	ЗаписатьДокументИнвентаризацииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработкаШтрихКода

&НаКлиенте
Процедура ОбработатьШтрихКод(Знач ОбрабатываемыйШтрихКод)
	
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(ОбрабатываемыйШтрихКод);
	
	ШтрихКод = ПараметрыШтрихкода.Штрихкод;
	
	ОбработатьШтрихКодТараНаСервере(ПараметрыШтрихкода);
	
	ПоказатьОшибки();
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихКодТараНаСервере(ПараметрыШтрихкода)
	
	Тара = к2РаботаСТаройСервер.ТараПоШтрихкоду(ПараметрыШтрихкода, ТекстОшибки);
	
	Если ЗначениеЗаполнено(Тара) Тогда
		
		ДобавитьВыбраннуюТаруВТовары(Тара);
		ЗаписатьДокументИнвентаризацииНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьВыбраннуюТаруВТовары(Тара)
	
	МоментВремени = ?(ЗначениеЗаполнено(Объект.Инвентаризация), Объект.Инвентаризация.Дата, ТекущаяДатаСеанса());
	
	СоставТары = РегистрыНакопления.к2ТоварыНаТаре.СоставТары(Тара, МоментВремени);
	
	Если Не СоставТары.Количество() = 0 Тогда
		
		Для Каждого Строка Из Объект.Товары.НайтиСтроки(Новый Структура("Тара", Тара)) Цикл
			Объект.Товары.Удалить(Строка);
		КонецЦикла;
		
		Для Каждого СтрокаСостава Из СоставТары Цикл
			
			НоваяСтрока = Объект.Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаСостава);
			НоваяСтрока.Ответственный = Объект.Ответственный;
			НоваяСтрока.Ячейка = Объект.Ячейка;
			
		КонецЦикла;
		
	Иначе
		
		ШаблонСообщения = НСтр("ru='Не удалось определить состав тары ""%1"".'");
		ТекстСообщения = СтрШаблон(ШаблонСообщения, Тара);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ЗаписьДокументаИнвентаризации

&НаСервере
Процедура ЗаписатьДокументИнвентаризацииНаСервере()
	
	Если ЗначениеЗаполнено(Объект.Инвентаризация) Тогда
		ДокументОбъект = Объект.Инвентаризация.ПолучитьОбъект();
	Иначе
		ДокументОбъект = Документы.к2Инвентаризация.СоздатьДокумент();
	КонецЕсли;
	
	ДанныеЗаполнения = Новый Структура;
	
	ДанныеЗаполнения.Вставить("Дата", ТекущаяДатаСеанса());
	ДанныеЗаполнения.Вставить("СпособСоздания", Перечисления.к2СпособыСозданияДокументов.ФормированиеИнвентаризаций);
	ДанныеЗаполнения.Вставить("Склад", Объект.Склад);
	ДанныеЗаполнения.Вставить("Статус", Перечисления.к2СтатусыИнвентаризации.ВРаботе);
	ДанныеЗаполнения.Вставить("Ответственный", Объект.Ответственный);
	
	ДокументОбъект.Заполнить(ДанныеЗаполнения);
	ЗаполнитьТоварыДляИнвентаризации(ДокументОбъект);
	ДокументОбъект.ТоварыНаТареЗаполнитьПоУчету();
	
	Если к2ПроведениеСервер.ЗаписатьДокумент(ДокументОбъект, ТекстОшибки) Тогда
		
		Объект.Инвентаризация = ДокументОбъект.Ссылка;
		ОбновитьДанныеФормыНаСервере();
		
	КонецЕсли;
		
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТоварыДляИнвентаризации(ДокументОбъект)
	
	Для Каждого Строка Из ДокументОбъект.Тары.НайтиСтроки(Новый Структура("Ответственный", Объект.Ответственный)) Цикл
		ДокументОбъект.Тары.Удалить(Строка);
	КонецЦикла;
	
	Таблица = к2Коллекции.ПолучитьСвернутуюТаблицу(Объект.Товары, "Тара, Ячейка, Ответственный");
	
	Для Каждого Строка Из Таблица Цикл
		
		НоваяСтрока = ДокументОбъект.Тары.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
		НоваяСтрока.НаМестеХранения = Истина;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЗагрузкеДанныхПриСозданииНаСервере()
	
	УстановитьЗначенияПоУмолчанию();
	
	СкладПриИзмененииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();
	
	Объект.Ответственный = Пользователи.ТекущийПользователь();
	
	СкоростьПрокрутки = 1;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьОшибки()
	
	Если ЗначениеЗаполнено(ТекстОшибки) Тогда
		Элементы.ГруппаСтраницыОбщие.ТекущаяСтраница = Элементы.ГруппаСтраницаОшибки;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.ГруппаНастройкиЯчейка.Видимость = глРеквизит(Объект.Склад, "ИспользоватьАдресноеХранение");
	
	Элементы.ГруппаШтрихКод.Видимость = Не к2РаботаСПодключаемымОборудованием.СканерШтрихКодовПодключен();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
