
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ФормаСписка_ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	к2ИзменениеФорм.НастроитьКолонкуСписка_Дата(Элементы.ДатаДоставки);
	к2ИзменениеФорм.НастроитьКолонкуСписка_Дата(Элементы.ДатаОтгрузки);
	
	ПараметрыПанели = СоздатьПанельОтборов();
	
	к2ПроверкиДокументов.ФормаСписка_ПриСозданииНаСервере(ЭтотОбъект, ПараметрыПроверок(ПараметрыПанели));
	
	УстановитьУсловноеОформление();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии( ЭтотОбъект, Отказ );
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_СостоянияЗаказовНаПеремещение" Тогда
		УстановитьУсловноеОформлениеПоЦветам();
	КонецЕсли;
	
	Если ИмяСобытия = "ЗаписаныЗаказыНаПеремещение" Тогда
		
		Элементы.Список.ВыделенныеСтроки.Очистить();
		
		Для каждого цЗаказ Из Параметр Цикл
			
			к2КоллекцииКлиентСервер.ВыделитьСтрокуВСписке(Элементы.Список, цЗаказ);
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	ПодключитьОбработчикОжидания("ПриАктивизацииСтрокиСписка", 1, Истина);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗагрузитьЗаказыИзФайла(Команда)
	
	к2ЗагрузкаДокументовКлиент.ЗагрузитьЗаказыНаПеремещениеИзФайла();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьЗаказыИзБланков(Команда)
	
	ПараметрыОткрытия = Новый Структура;
	ПараметрыОткрытия.Вставить("ВариантСозданияБланка", ПредопределенноеЗначение("Перечисление.к2ВариантыСозданияБланка.ЗаказНаПеремещение"));
	
	ОткрытьФорму("Обработка.к2СозданиеБланковЗаявок.Форма.ЗагрузкаБланковИзФайловExcel",
		ПараметрыОткрытия,
		ЭтотОбъект,
		ЭтотОбъект);	
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПодключаемыеКоманды

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Элементы.Список);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Элементы.Список);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УстановитьУсловноеОформлениеПоЦветам();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформлениеПоЦветам()
	
	СтруктураОтборов = Новый Структура;
	СтруктураОтборов.Вставить("ТипДокумента", Метаданные.Документы.к2ЗаказНаПеремещение.ПолноеИмя());
	
	к2УО.ПоСостоянию(
		Список.КомпоновщикНастроек.Настройки.УсловноеОформление,
		СтруктураОтборов,
		"ЦветОформления",
		"Состояние" );
	
КонецПроцедуры

&НаСервере
Функция ПараметрыПроверок(ПараметрыПанелиОтборов)
	
	СтруктураПараметровПроверок = к2ПроверкиДокументов.ПараметрыДобавленияПроверок();
	
	СтруктураПараметровПроверок.ЭлементРодитель = ПараметрыПанелиОтборов.Элементы.БоковаяПанельОтбора;
	СтруктураПараметровПроверок.МестоРасположения = ПараметрыПанелиОтборов.Элементы.ДекорацияРастяжения;
	СтруктураПараметровПроверок.ИмяМетаданных = Метаданные.Документы.к2ЗаказНаПеремещение.ПолноеИмя();
	СтруктураПараметровПроверок.РежимВыбора = Параметры.РежимВыбора;
	
	Возврат СтруктураПараметровПроверок;
	
КонецФункции

#Область ПроверкиДокументов

&НаКлиенте
Процедура Подключаемый_ПроверитьДокументы(Команда)
	
	СтруктураПроверок = к2ПроверкиДокументовКлиент.ПодготовитьДанныеПроверокСписка( Элементы.Список );
	
	Если СтруктураПроверок.Отказ Тогда
		
		ОбщегоНазначенияКлиент.СообщитьПользователю( СтруктураПроверок.ТекстСообщения );
		
	Иначе	
		
		ПроверитьДокументыНаСервере(СтруктураПроверок.МассивДокументов);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_УстановитьСостояние(Команда)
	
	к2ПроверкиДокументовКлиент.УстановитьСостояние( ЭтотОбъект );
		
КонецПроцедуры

&НаКлиенте
Процедура ПриАктивизацииСтрокиСписка()

	ТекущаяСтрока = Элементы.Список.ТекущаяСтрока;
	ТипСтроки = ТипЗнч(ТекущаяСтрока);
	
	Если Не ТипСтроки = Тип("ДокументСсылка.к2ЗаказНаПеремещение") Тогда
		Возврат;
	КонецЕсли;
	
	ПриАктивизацииСтрокиСпискаСервер( ТекущаяСтрока );
	
КонецПроцедуры	

&НаСервере
Процедура ПриАктивизацииСтрокиСпискаСервер( Документ )
	
	к2ПроверкиДокументов.Форма_ОбновитьОтображениеРезультатовПроверок( ЭтотОбъект, Документ );
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыборСостоянияЗавершение( Результат, ДополнительныеПараметры ) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьСостояниеНаСервере( Результат, ДополнительныеПараметры.Документы );
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСостояниеНаСервере( Состояние, Документы )
	
	к2ПроверкиДокументов.УстановитьСостояние( Состояние, Документы );
	
	Элементы.Список.Обновить();
	ТекущаяСтрока = Элементы.Список.ТекущаяСтрока;
	
	к2ПроверкиДокументов.Форма_ОбновитьОтображениеРезультатовПроверок( ЭтотОбъект, ТекущаяСтрока );
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьДокументыНаСервере( ДокументыДляПроверки )
	
	к2ПроверкиДокументов.ПроверитьДокументы( ДокументыДляПроверки );
	
	Элементы.Список.Обновить();
	ТекущаяСтрока = Элементы.Список.ТекущаяСтрока;
	
	к2ПроверкиДокументов.Форма_ОбновитьОтображениеРезультатовПроверок( ЭтотОбъект, ТекущаяСтрока );
	
КонецПроцедуры

#КонецОбласти

#Область ПанельОтборов

&НаСервере
Функция СоздатьПанельОтборов()
	
	параметрыПанели = к2Отборы.ПараметрыПанелиОтборов();
	параметрыПанели.Кнопка_НастроитьСписок           = Элементы.НастройкаСписка;
	параметрыПанели.Группа_ПользовательскиеНастройки = Элементы.СписокКомпоновщикНастроекПользовательскиеНастройки;
	параметрыПанели.Метаданные = Метаданные.Документы.к2ЗаказНаПеремещение;
	
	новыйОтбор = к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Дата");
	новыйОтбор.Заголовок = НСтр("ru='Период'");
	
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "ДатаОтгрузки");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "ДатаДоставки");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Организация");
	
	новыйОтбор = к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "СкладОтправитель");
	новыйОтбор.Заголовок = НСтр("ru='Отправитель'");
	
	новыйОтбор = к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "СкладПолучатель");
	новыйОтбор.Заголовок = НСтр("ru='Получатель'");
	
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Товары.Номенклатура");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Состояние");
	ОтборЭтап = к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Этап");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Подразделение");
	к2Отборы.НовыйРеквизитОтбора_ПоМетаданным(параметрыПанели, "Ответственный");
	
	к2Отборы.СоздатьПанельОтборов(ЭтотОбъект, параметрыПанели);
	
	к2ЭтапыДокументов.ЗадатьПараметрыВыбораДляЭтапа(ОтборЭтап.Элемент, Перечисления.к2ДокументСЭтапом.ЗаказНаПеремещение);
	
	Возврат параметрыПанели;
	
КонецФункции

&НаКлиенте
Процедура Подключаемый_ОбработкаПанелиОтборов(Элемент, СтандартнаяОбработка = Истина)
	
	к2ОтборыКлиент.ОбработкаПанелиОтборов(ЭтотОбъект, Элемент, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_УстановитьОтбор(Знач пИмяЭлемента, Знач пСохранять = Истина) Экспорт
	
	к2Отборы.УстановитьОтборСписку(ЭтотОбъект, пИмяЭлемента, пСохранять);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
