#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗаполнитьСвязи();
	
	к2Формы_Меркурий.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("СоответствиеДанных_Меркурий_Запись");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	
	ЗаполнитьСвязи();
	
КонецПроцедуры

&НаКлиенте
Процедура ПредприятиеПриИзменении(Элемент)
	
	ЗаполнитьСвязи();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьСвязи()
	
	ХС              = Неопределено;
	КонтрагентПоХС  = Неопределено;
	Контрагент      = Неопределено;
	ХСПоКонтрагенту = Неопределено;
	
	Если ЗначениеЗаполнено(Контрагент) Тогда
		
		ХСПоКонтрагенту = к2МеркурийСервер.ХозяйствующийСубъект(Контрагент);
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Запись.Предприятие) Тогда
		
		ХС = к2ОбщегоНазначения_Меркурий.ЗначениеРеквизитаОбъекта(Запись.Предприятие, "ХозяйствующийСубъект");
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ХС) Тогда
		
		КонтрагентПоХС = к2МеркурийСервер.Контрагент(ХС);
		
	КонецЕсли;
	
	Элементы.Группа_Контрагент.Видимость = ЗначениеЗаполнено(Контрагент);
	Элементы.Группа_ХС.Видимость         = ЗначениеЗаполнено(ХС);
	
	Элементы.Группа_СвязьКонтрагента.Видимость     = ЗначениеЗаполнено(ХСПоКонтрагенту);
	Элементы.ДекорацияКонтрагентНеСвязан.Видимость = Не ЗначениеЗаполнено(ХСПоКонтрагенту);
	
	Элементы.Группа_СвязьХС.Видимость      = ЗначениеЗаполнено(КонтрагентПоХС);
	Элементы.ДекорацияХСНеСвязан.Видимость = Не ЗначениеЗаполнено(КонтрагентПоХС);
	
	Если ЗначениеЗаполнено(Контрагент)
		И ЗначениеЗаполнено(КонтрагентПоХС)
		И Не Контрагент = КонтрагентПоХС Тогда
		
		Элементы.Контрагент.ЦветТекста     = ЦветаСтиля.ЦветОсобогоТекста;
		Элементы.КонтрагентПоХС.ЦветТекста = ЦветаСтиля.ЦветОсобогоТекста;
		
	ИначеЕсли ЗначениеЗаполнено(Контрагент)
		И ЗначениеЗаполнено(КонтрагентПоХС)
		И Контрагент = КонтрагентПоХС Тогда
		
		Элементы.Контрагент.ЦветТекста     = ЦветаСтиля.к2ЦветСтатусаОформлен_Меркурий;
		Элементы.КонтрагентПоХС.ЦветТекста = ЦветаСтиля.к2ЦветСтатусаОформлен_Меркурий;
		
	Иначе
		
		Элементы.Контрагент.ЦветТекста     = Новый Цвет;
		Элементы.КонтрагентПоХС.ЦветТекста = Новый Цвет;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ХС)
		И ЗначениеЗаполнено(ХСПоКонтрагенту)
		И Не ХС = ХСПоКонтрагенту Тогда
		
		Элементы.ХС.ЦветТекста              = ЦветаСтиля.ЦветОсобогоТекста;
		Элементы.ХСПоКонтрагенту.ЦветТекста = ЦветаСтиля.ЦветОсобогоТекста;
		
	ИначеЕсли ЗначениеЗаполнено(ХС)
		И ЗначениеЗаполнено(ХСПоКонтрагенту)
		И ХС = ХСПоКонтрагенту Тогда
		
		Элементы.ХС.ЦветТекста              = ЦветаСтиля.к2ЦветСтатусаОформлен_Меркурий;
		Элементы.ХСПоКонтрагенту.ЦветТекста = ЦветаСтиля.к2ЦветСтатусаОформлен_Меркурий;
		
	Иначе
		
		Элементы.ХС.ЦветТекста              = Новый Цвет;
		Элементы.ХСПоКонтрагенту.ЦветТекста = Новый Цвет;
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ХС)
		И ЗначениеЗаполнено(Контрагент) Тогда
		
		ИННХС          = к2ОбщегоНазначения_Меркурий.ЗначениеРеквизитаОбъекта(ХС, "ИНН");
		ИННКонтрагента = к2ОбщегоНазначения_Меркурий.ЗначениеРеквизитаОбъекта(Контрагент, "ИНН");
		
		Если ИННХС = ИННКонтрагента Тогда
			
			Элементы.ХС.ЦветТекста         = ЦветаСтиля.к2ЦветСтатусаОформлен_Меркурий;
			Элементы.Контрагент.ЦветТекста = ЦветаСтиля.к2ЦветСтатусаОформлен_Меркурий;
			
		Иначе
			
			Элементы.ХС.ЦветТекста         = ЦветаСтиля.ЦветОсобогоТекста;
			Элементы.Контрагент.ЦветТекста = ЦветаСтиля.ЦветОсобогоТекста;
			
		КонецЕсли;
		
		Элементы.ДекорацияИННРазличаются.Видимость = Не ИННХС = ИННКонтрагента;
		
	Иначе
		
		Элементы.ХС.ЦветТекста                     = Новый Цвет;
		Элементы.Контрагент.ЦветТекста             = Новый Цвет;
		Элементы.ДекорацияИННРазличаются.Видимость = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти