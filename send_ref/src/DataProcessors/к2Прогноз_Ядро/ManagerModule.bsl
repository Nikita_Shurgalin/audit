
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура УстановитьНастройкиКомпоновщика(КомпоновщикНастроек,
									 Знач НастройкиКомпоновкиДанных = Неопределено,
									 Знач УникальныйИдентификатор = Неопределено) Экспорт
	
	СхемаКомпоновкиДанных = СКД_ОтборДанных();
	
	Если УникальныйИдентификатор = Неопределено Тогда
		
		ИсточникНастроек = Новый ИсточникДоступныхНастроекКомпоновкиДанных(СхемаКомпоновкиДанных);
		
	Иначе
		
		АдресСхемыКомпоновкиДанных = ПоместитьВоВременноеХранилище(СхемаКомпоновкиДанных, УникальныйИдентификатор);
		
		ИсточникНастроек = Новый ИсточникДоступныхНастроекКомпоновкиДанных(АдресСхемыКомпоновкиДанных);
		
	КонецЕсли;
	
	КомпоновщикНастроек.Инициализировать(ИсточникНастроек);
	
	Если НастройкиКомпоновкиДанных <> Неопределено Тогда
		
		КомпоновщикНастроек.ЗагрузитьНастройки(НастройкиКомпоновкиДанных);
		КомпоновщикНастроек.Восстановить(СпособВосстановленияНастроекКомпоновкиДанных.Полное);
		
		ПустойКомпоновщик = Новый КомпоновщикНастроекКомпоновкиДанных;
		КомпоновщикНастроек.ЗагрузитьПользовательскиеНастройки(ПустойКомпоновщик.ПользовательскиеНастройки);
		
	Иначе
		
		КомпоновщикНастроек.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура УстановитьНастройкиИзСценария(пОбъект, Знач пСценарий, Знач пУникальныйИдентификаторФормы = Неопределено) Экспорт
	
	пОбъект.Сценарий                 = пСценарий;
	пОбъект.Периодичность            = пСценарий.Периодичность;
	пОбъект.ПериодовПолученияИстории = пСценарий.ПериодовПолученияИстории;
	пОбъект.ПериодовПланирования     = пСценарий.ПериодовПланирования;
	пОбъект.Календарь                = пСценарий.Календарь;
	
	УстановитьНастройкиКомпоновщика(пОбъект.КомпоновщикНастроек_ОтборДанных,
									пСценарий.ХранилищеНастроекКомпоновкиДанных.Получить(),
									пУникальныйИдентификаторФормы);
	
	ПриИзмененииПериодовПланирования(пОбъект);
	ПриИзмененииПериодовПолученияИстории(пОбъект);
	
КонецПроцедуры

Процедура ПриИзмененииПериодовПланирования(пОбъект) Экспорт
	
	ПроверитьЗаполнитьБазовыеРеквизиты(пОбъект);
	пОбъект.ГоризонтПланирования = к2Даты.Добавить(
		пОбъект.КонецПериодаИстории,
		пОбъект.Периодичность,
		пОбъект.ПериодовПланирования);
	
КонецПроцедуры

Процедура ПриИзмененииПериодовПолученияИстории(пОбъект) Экспорт
	
	ПроверитьЗаполнитьБазовыеРеквизиты(пОбъект);
	пОбъект.НачалоПериодаИстории = к2Даты.Добавить(
		пОбъект.КонецПериодаИстории,
		пОбъект.Периодичность,
		(-1)*пОбъект.ПериодовПолученияИстории);
	
КонецПроцедуры

Функция СКД_ОтборДанных() Экспорт
	
	Возврат Обработки.к2Прогноз_Ядро.ПолучитьМакет("СКД_ОтборДанных");
	
КонецФункции

Функция СКД_ИсторияПродаж() Экспорт
	
	Возврат Обработки.к2Прогноз_Ядро.ПолучитьМакет("СКД_ИсторияПродаж");
	
КонецФункции

Функция СКД_ФакторыПродаж() Экспорт
	
	Возврат Обработки.к2Прогноз_Ядро.ПолучитьМакет("СКД_ФакторыПродаж");
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьЗаполнитьБазовыеРеквизиты(пОбъект)
	
	Если Не ЗначениеЗаполнено(пОбъект.Периодичность) Тогда
		
		пОбъект.Периодичность = Перечисления.к2Периодичность.Месяц;
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(пОбъект.КонецПериодаИстории) Тогда
		
		пОбъект.КонецПериодаИстории = к2Даты.НачалоПериода(ТекущаяДатаСеанса(), пОбъект.Периодичность);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

