
#Область ОписаниеПеременных

&НаКлиенте
Перем ПодключаемоеОборудование;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	УстановитьЗначенияПоУмолчанию();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, ЭтотОбъект, "СканерШтрихкода");
	
	к2РаботаСПодключаемымОборудованиемКлиент.ПриОткрытии(ЭтотОбъект, Отказ, ПодключаемоеОборудование);
	
	УстановитьВидимостьПодключаемоеОборудование();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтотОбъект);
	
	к2РаботаСПодключаемымОборудованиемКлиент.ПриЗакрытии(ЭтотОбъект, ЗавершениеРаботы, ПодключаемоеОборудование);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если СоздатьНовуюТару Тогда
		
		МассивНепроверяемыхРеквизитов.Добавить("Тара");
		
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	Если Событие = "Штрихкод" И ВводДоступен() Тогда 
		
		ОбработатьШтрихКод(Данные);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ШтрихКодПриИзменении(Элемент)
	
	ОбработатьШтрихКод(ШтрихКод);
	
КонецПроцедуры

&НаКлиенте
Процедура ТараПриИзменении(Элемент)
	
	ТараПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаКалькуляторШтрихкод(Команда)
	
	ПараметрыКалькулятора = ПараметрыКалькулятора_Строка();
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьКалькуляторШтрихКодЗавершение", ЭтотОбъект);
	
	ОткрытьФорму("ОбщаяФорма.к2КалькуляторСенсор", ПараметрыКалькулятора, ЭтотОбъект, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаКалькуляторНомерТары(Команда)
	
	ПараметрыКалькулятора = ПараметрыКалькулятора_НомерТары();
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьКалькуляторНомерТарыЗавершение", ЭтотОбъект);
	
	ОткрытьФорму("ОбщаяФорма.к2КалькуляторСенсор", ПараметрыКалькулятора, ЭтотОбъект, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаКалькуляторНовыйВес(Команда)
	
	ПараметрыКалькулятора = ПараметрыКалькулятора_Число();
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьКалькуляторНовыйВесЗавершение", ЭтотОбъект);
	
	ОткрытьФорму("ОбщаяФорма.к2КалькуляторСенсор", ПараметрыКалькулятора, ЭтотОбъект, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаПолучитьВесТары(Команда)
	
	ПолучитьВес(Элементы.НовыйВесТары.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаПодтвердить(Команда)
	
	ТекстОшибки = "";
	
	ПодтвердитьНаСервере(ТекстОшибки);
	
	Если ЗначениеЗаполнено(ТекстОшибки) Тогда
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьЗначенияПоУмолчанию()
	
	к2УчетныеТочки.ЗаполнитьПараметрыОбработки(Параметры, ЭтотОбъект);
	
	к2ПроизводствоСервер.УстановитьЗаголовокОбработки(ЭтотОбъект);
	
	ИспользоватьПодключаемоеОборудование = к2РаботаСПодключаемымОборудованием.ИспользоватьПодключаемоеОборудование();
	
	СканерШтрихКодовПодключен = к2РаботаСПодключаемымОборудованием.СканерШтрихКодовПодключен();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	Элементы.ГруппаШтрихКод.Видимость = Не СканерШтрихКодовПодключен;
	
	Элементы.ГруппаТекущиеДанные.Видимость = ЗначениеЗаполнено(ДатаПоследнееВзвешивание) Или ЗначениеЗаполнено(ВесПоследнееВзвешивание);
	
	Элементы.ГруппаПанельДействий.Доступность = ЗначениеЗаполнено(Тара) Или СоздатьНовуюТару;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьВидимостьПодключаемоеОборудование()
	
	Весы = ПодключаемоеОборудование.Весы;
	
	Элементы.КомандаПолучитьВесТары.Видимость = Весы.Использовать;
	Элементы.КомандаКалькуляторНовыйВес.Видимость = Не Элементы.КомандаПолучитьВесТары.Видимость;
	
КонецПроцедуры

#Область РаботаСТарой

&НаСервере
Процедура ТараПриИзмененииНаСервере()
	
	ОчиститьРеквизиты();
	
	РазобратьТару = Не ТараПустая();
	
	ТипТары = к2РаботаСТаройСервер.ТипТары(Тара);
	
	ЗаполнитьАктуальныеДанныеПоТаре();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Функция ТараПустая()
	
	СтруктураСоставТары = к2РаботаСТаройСервер.СоставТары(Тара);
	
	Возврат СтруктураСоставТары.Товары.Количество() = 0 И СтруктураСоставТары.Вложения.Количество() = 0;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьАктуальныеДанныеПоТаре()
	
	СтруктураДанныхПоТаре = РегистрыСведений.к2ВзвешиванияТары.ПоследнееВзвешивание(Тара);
	
	ДатаПоследнееВзвешивание = СтруктураДанныхПоТаре.Дата;
	ВесПоследнееВзвешивание = СтруктураДанныхПоТаре.Вес;
	
КонецПроцедуры

#КонецОбласти

#Область Подтверждение

&НаСервере
Процедура ПодтвердитьНаСервере(ТекстОшибки)
	
	Если Не ПроверитьЗаполнение() Тогда
		
		к2ОбщегоНазначения.ДобавитьСообщенияПользователюВТекстОшибки(ТекстОшибки, Истина);
		Возврат;
		
	КонецЕсли;
	
	Если СохранитьНовыйВесТары(ТекстОшибки) Тогда
		ОбновитьФормуНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция СохранитьНовыйВесТары(ТекстОшибки)
	
	Отказ = Ложь;
	
	НачатьТранзакцию();
	
	Попытка
		
		СформироватьКомплектацию();
		
		Тара = НоваяТара();
		
		ЗаписатьВесТары();
		
		ЗафиксироватьТранзакцию();
		
	Исключение
		
		ОтменитьТранзакцию();
		
		ИнформацияОбОшибке = ИнформацияОбОшибке();
		
		КраткоеПредставлениеОшибки = КраткоеПредставлениеОшибки(ИнформацияОбОшибке);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(КраткоеПредставлениеОшибки, ТекстОшибки, Отказ);
		
		ПодробноеПредставлениеОшибки = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке);
		
		ИмяСобытия = НСтр("ru = 'к2ВзвешиваниеТары.Подтверждение'", ОбщегоНазначения.КодОсновногоЯзыка());
		
		ЗаписьЖурналаРегистрации(ИмяСобытия
			, УровеньЖурналаРегистрации.Ошибка
			, 
			, 
			, ПодробноеПредставлениеОшибки);
		
	КонецПопытки;
	
	Возврат Не Отказ;
	
КонецФункции

&НаСервере
Процедура СформироватьКомплектацию()
	
	Если Не РазобратьТару Тогда
		Возврат;
	КонецЕсли;
	
	МестонахождениеТары = РегистрыСведений.к2МестонахождениеТары.СтруктураМестонахожденияТары(Тара);
	
	СтруктураЗаполнения = Новый Структура;
	СтруктураЗаполнения.Вставить("Дата"          , ТекущаяДатаСеанса());
	СтруктураЗаполнения.Вставить("ТипОперации"   , Перечисления.к2ТипыОперацийКомплектацииТары.Разобрать);
	СтруктураЗаполнения.Вставить("СпособСоздания", Перечисления.к2СпособыСозданияДокументов.ВзвешиваниеТары);
	СтруктураЗаполнения.Вставить("Тара"          , Тара);
	СтруктураЗаполнения.Вставить("Склад"         , МестонахождениеТары.Склад);
	СтруктураЗаполнения.Вставить("Ячейка"        , МестонахождениеТары.Ячейка);
	СтруктураЗаполнения.Вставить("Ответственный" , Ответственный);
	СтруктураЗаполнения.Вставить("Основание"     , Документы.к2Переработка.ПустаяСсылка());
	
	ДокументОбъект = Документы.к2КомплектацияТары.СоздатьДокумент();
	ДокументОбъект.Заполнить(СтруктураЗаполнения);
	
	к2УправлениеДаннымиСервер.ПровестиДокумент_СообщенияИсключением(ДокументОбъект);
	
КонецПроцедуры

&НаСервере
Функция НоваяТара()
	
	Если Не СоздатьНовуюТару Тогда
		Возврат Тара;
	КонецЕсли;
	
	ТараОбъект = Справочники.к2Тара.СоздатьЭлемент();
	ТараОбъект.Код = Прав(Штрихкод, ДлинаКодаТары());
	ТараОбъект.Владелец = ТипТары;
	ТараОбъект.Вес = НовыйВесТары;
	ТараОбъект.ДатаПоследнегоВзвешивания = ТекущаяДатаСеанса();
	ТараОбъект.ШтрихКод = к2РаботаСТаройСервер.СформироватьШтрихКод(ТараОбъект);
	
	к2УправлениеДаннымиСервер.ЗаписатьСправочник_СообщенияИсключением(ТараОбъект);
	
	Возврат ТараОбъект.Ссылка;
	
КонецФункции

&НаСервере
Процедура ЗаписатьВесТары()
	
	ОтразитьВесВРегистре();
	
	ОтразитьВесВСправочнике();
	
КонецПроцедуры

&НаСервере
Процедура ОтразитьВесВРегистре()
	
	СтруктураЗаполнения = Новый Структура;
	СтруктураЗаполнения.Вставить("Период", ТекущаяДатаСеанса());
	СтруктураЗаполнения.Вставить("Тара", Тара);
	СтруктураЗаполнения.Вставить("Вес", НовыйВесТары);
	
	РегистрыСведений.к2ВзвешиванияТары.Отразить(СтруктураЗаполнения);
	
КонецПроцедуры

&НаСервере
Процедура ОтразитьВесВСправочнике()
	
	СтруктураДанныеТары = РегистрыСведений.к2ВзвешиванияТары.ПоследнееВзвешивание(Тара);
	
	ТараОбъект = Тара.ПолучитьОбъект();
	ТараОбъект.Вес = СтруктураДанныеТары.Вес;
	ТараОбъект.ДатаПоследнегоВзвешивания = СтруктураДанныеТары.Дата;
	
	к2УправлениеДаннымиСервер.ЗаписатьСправочник_СообщенияИсключением(ТараОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область КалькуляторСенсор

#Область ПараметрыКалькулятора

&НаКлиенте
Функция ПараметрыКалькулятора_Строка()
	
	ПараметрыКалькулятора = к2ПроизводствоКлиентСервер.СтруктураПараметровКалькуляторСенсор();
	
	ПараметрыКалькулятора.Тип = ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Строка");
	ПараметрыКалькулятора.ДлинаСтроки = 60;
	
	Возврат ПараметрыКалькулятора;
	
КонецФункции

&НаКлиенте
Функция ПараметрыКалькулятора_Число()
	
	СтруктураПараметров = к2ПроизводствоКлиентСервер.СтруктураПараметровКалькуляторСенсор();
	СтруктураПараметров.Тип = ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Число");
	СтруктураПараметров.ДлинаЧисла = 15;
	СтруктураПараметров.ТочностьЧисла = 3;
	
	Возврат СтруктураПараметров;
	
КонецФункции

&НаКлиенте
Функция ПараметрыКалькулятора_НомерТары()
	
	ПараметрыКалькулятора = к2ПроизводствоКлиентСервер.СтруктураПараметровКалькуляторСенсор();
	ПараметрыКалькулятора.Тип = ПредопределенноеЗначение("Перечисление.к2ТипыКалькулятораСенсор.Строка");
	ПараметрыКалькулятора.ДлинаСтроки = ДлинаКодаТары();
	ПараметрыКалькулятора.Заголовок = НСтр("ru = 'Введите код тары'");
	
	Возврат ПараметрыКалькулятора;
	
КонецФункции

#КонецОбласти

&НаКлиенте
Процедура ОткрытьКалькуляторШтрихКодЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ОбработатьШтрихкод(Результат.Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьКалькуляторНомерТарыЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	СписокТары = СписокТарыПоЗначащимСимволамНомера(Результат.Результат, н_ТипыТары);
	
	Если СписокТары.Количество() > 1 Тогда
		
		СтруктураПараметров = Новый Структура;
		СтруктураПараметров.Вставить("Тары", СписокТары);
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьФормуВыбораТарыЗавершение", ЭтотОбъект);
		
		ОткрытьФорму("Справочник.к2Тара.Форма.ФормаВыбораСенсор", СтруктураПараметров, ЭтотОбъект, , , , ОписаниеОповещения);
		
	ИначеЕсли СписокТары.Количество() = 1 Тогда
		
		ОткрытьФормуВыбораТарыЗавершение(СписокТары[0].Значение, Неопределено);
		
	Иначе
		
		ТекстОшибки = НСтр("ru = 'Тара с указанным номером не найдена'");
		к2ПроизводствоКлиент.ПоказатьПредупреждениеПользователю(ТекстОшибки);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуВыбораТарыЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Тара = Результат;
	ТараПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьКалькуляторНовыйВесЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	НовыйВесТары = Результат.Результат;
	
КонецПроцедуры

#КонецОбласти

#Область ШтрихКод

&НаКлиенте
Процедура ОбработатьШтрихКод(Знач ОбрабатываемыйШтрихКод)
	
	Отказ = Ложь;
	ТекстОшибки = "";
	
	ПараметрыШтрихкода = к2ШтрихкодыУпаковокКлиентСервер.ПараметрыШтрихкода(ОбрабатываемыйШтрихКод);
	
	ШтрихКод = ПараметрыШтрихкода.Штрихкод;
	
	Если к2ШтрихкодыУпаковокКлиентСервер.ЭтоПараметрыШтрихКодаТары(ПараметрыШтрихкода) Тогда
		
		ОбработатьШтрихКодТарыНаСервере(ПараметрыШтрихкода, ТекстОшибки, Отказ);
		
	Иначе
		
		ШаблонСообщения = НСтр("ru = 'Введенный штрихкод ""%1"" не является штрихкодом тары'");
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ОбрабатываемыйШтрихКод);
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Если Отказ Тогда
		к2ПроизводствоКлиент.ПоказатьОповещениеПользователю(ТекстОшибки, , 0);
	Иначе
		ПоказатьВопросСоздатьНовуюТару();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихКодТарыНаСервере(ПараметрыШтрихкода, ТекстОшибки, Отказ)
	
	Тара = к2РаботаСТаройСервер.ТараПоШтрихкоду(ПараметрыШтрихкода);
	
	ТараПриИзмененииНаСервере();
	
	Если Не ЗначениеЗаполнено(ТипТары) Тогда
		ТипТары = ТипТарыПоШтрихкоду(ПараметрыШтрихкода.Штрихкод);
	КонецЕсли;
	
	ПроверитьТипТары(ТекстОшибки, Отказ);
	
КонецПроцедуры

&НаСервере
Функция ТипТарыПоШтрихкоду(ШтрихкодТары)
	
	ТипТарыПоШтрихКоду = Неопределено;
	
	Если ЗначениеЗаполнено(ШтрихкодТары) Тогда
		
		ДлинаКода = Метаданные.Справочники.к2ТипыТары.ДлинаКода;
		
		СтрокаНомерТипаТары = Лев(ШтрихкодТары, ДлинаКода);
		
		ТипТарыПоШтрихКоду = Справочники.к2ТипыТары.НайтиТипТарыПоНомеру(СтрокаНомерТипаТары);
		
	КонецЕсли;
	
	Возврат ТипТарыПоШтрихКоду;
	
КонецФункции

&НаСервере
Процедура ПроверитьТипТары(ТекстОшибки, Отказ)
	
	Если ЗначениеЗаполнено(ТипТары) Тогда
		
		Если глРеквизит(ТипТары, "ФиксированныйВес") Тогда
			
			ШаблонСообщения = НСтр("ru = 'Тара ""%1"" имеет фиксированный вес.'");
			
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ШтрихКод);
			
			к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки, Отказ);
			
		КонецЕсли;
		
	Иначе
		
		ТекстСообщения = НСтр("ru = 'Не удалось определить тип тары по штрих-коду.'");
		
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Если н_ТипыТары.НайтиПоЗначению(ТипТары) = Неопределено Тогда
		
		ШаблонСообщения = НСтр("ru = 'Тип тары ""%1"" не соответствует настройкам.
									 |В настройках указаны типы ""%2"".'");
		
		МассивЗначений = н_ТипыТары.ВыгрузитьЗначения();
		СтрокаТипыТары = СтрСоединить(МассивЗначений, ", ");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ТипТары, СтрокаТипыТары);
		к2КоллекцииКлиентСервер.ДобавитьСтрокуВТекст(ТекстСообщения, ТекстОшибки, Отказ);
		
	КонецЕсли;
	
	Если Отказ Тогда
		
		ОбновитьФормуНаСервере();
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВопросСоздатьНовуюТару()
	
	Если Не (ЗначениеЗаполнено(ТипТары) И Не ЗначениеЗаполнено(Тара)) Тогда
		Возврат;
	КонецЕсли;
	
	ТекстВопроса = НСтр("ru = 'Тара с указанным штрихкодом не найдена в системе. Создать новую тару при подтверждении веса?'");
	
	Оповещение = Новый ОписаниеОповещения("ПоказатьВопросСоздатьНовуюТаруЗавершение", ЭтотОбъект);
	
	ПоказатьВопросПользователю(ТекстВопроса, ЭтотОбъект, Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВопросПользователю(ТекстСообщения, Владелец, ОписаниеОповещения = Неопределено)
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("РежимДиалогаСтрокой", Строка(РежимДиалогаВопрос.ДаНет));
	СтруктураПараметров.Вставить("ТекстДиалога", ТекстСообщения);
	СтруктураПараметров.Вставить("Картинка", БиблиотекаКартинок.Вопрос32);
	
	ОткрытьФорму("ОбщаяФорма.к2ФормаДиалогаСенсор", СтруктураПараметров, Владелец, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВопросСоздатьНовуюТаруЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	СоздатьНовуюТару = Результат = КодВозвратаДиалога.Да;
	
	ПоказатьВопросСоздатьНовуюТаруЗавершениеНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ПоказатьВопросСоздатьНовуюТаруЗавершениеНаСервере()
	
	Если СоздатьНовуюТару Тогда
		УстановитьВидимостьДоступность();
	Иначе
		ОбновитьФормуНаСервере();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Весы

&НаКлиенте
Процедура ПолучитьВес(ИмяРеквизита)
	
	ЭтотОбъект[ИмяРеквизита] = к2РаботаСПодключаемымОборудованиемКлиент.ПолучитьВес_ЭлектронныеВесы(ЭтотОбъект, ПодключаемоеОборудование.Весы);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ОбновитьФормуНаСервере()
	
	ОчиститьФорму();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаСервере
Процедура ОчиститьФорму()
	
	ШтрихКод = Неопределено;
	СоздатьНовуюТару = Ложь;
	Тара = Неопределено;
	
	ОчиститьРеквизиты();
	
КонецПроцедуры

&НаСервере
Процедура ОчиститьРеквизиты()
	
	ТипТары = Неопределено;
	ДатаПоследнееВзвешивание = Неопределено;
	ВесПоследнееВзвешивание = Неопределено;
	НовыйВесТары = Неопределено;
	РазобратьТару = Ложь;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ДлинаКодаТары()
	
	Возврат Метаданные.Справочники.к2Тара.ДлинаКода;
	
КонецФункции

&НаСервереБезКонтекста
Функция СписокТарыПоЗначащимСимволамНомера(СимволыНомера, ТипыТары)
	
	МассивТары = к2РаботаСТаройСервер.МассивТарыПоЗначащимСимволамНомера(СимволыНомера, ТипыТары);
	
	СписокТары = Новый СписокЗначений;
	СписокТары.ЗагрузитьЗначения(МассивТары);
	
	Возврат СписокТары;
	
КонецФункции

#КонецОбласти

#КонецОбласти
