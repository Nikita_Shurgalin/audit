#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если ФиксированныйВес Тогда
		ПроверяемыеРеквизиты.Добавить("Вес");
	КонецЕсли;
	
	ПроверитьИспользованиеДляУпаковочныхЛистов(Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	Если Не ИспользуетсяДляУпаковочныхЛистов Тогда 
		ЦифраРасширенияSSCC = Неопределено;
	Иначе
		Номенклатура = Неопределено;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьИспользованиеДляУпаковочныхЛистов(Отказ)
	
	ПроверитьДублированиеЦифрыРасширенияSSCC(Отказ);
	ПроверитьИспользованиеЦифрыРасширенияSSCC(Отказ);
	
КонецПроцедуры

Процедура ПроверитьДублированиеЦифрыРасширенияSSCC(Отказ)
	
	Если Не ИспользуетсяДляУпаковочныхЛистов Тогда 
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	к2ТипыТары.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.к2ТипыТары КАК к2ТипыТары
	|ГДЕ
	|	к2ТипыТары.ИспользуетсяДляУпаковочныхЛистов
	|	И к2ТипыТары.ЦифраРасширенияSSCC = &ЦифраРасширенияSSCC
	|	И НЕ к2ТипыТары.ПометкаУдаления
	|	И НЕ к2ТипыТары.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.УстановитьПараметр("ЦифраРасширенияSSCC", ЦифраРасширенияSSCC);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда 
		
		ШаблонСообщения = НСтр("ru='Выбранное расширение SSCC уже указано в %1'");
		ТекстСообщения = СтрШаблон(ШаблонСообщения, Выборка.Ссылка);
		
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, "ЦифраРасширенияSSCC", "Объект.ЦифраРасширенияSSCC", Отказ);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьИспользованиеЦифрыРасширенияSSCC(Отказ)
	
	Если Не ИспользуетсяДляУпаковочныхЛистов Тогда 
		Возврат;
	КонецЕсли;
	
	Если Не глРеквизит(Ссылка, "ЦифраРасширенияSSCC") = ЦифраРасширенияSSCC Тогда 
		
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	к2УпаковочныйЛист.Ссылка КАК Ссылка
		|ИЗ
		|	Документ.к2УпаковочныйЛист КАК к2УпаковочныйЛист
		|ГДЕ
		|	к2УпаковочныйЛист.ЦифраРасширенияSSCC = &ЦифраРасширенияSSCC
		|	И НЕ к2УпаковочныйЛист.ТипТары = &ТипТары";
		
		Запрос.УстановитьПараметр("ТипТары", Ссылка);
		Запрос.УстановитьПараметр("ЦифраРасширенияSSCC", ЦифраРасширенияSSCC);
		
		Если Не Запрос.Выполнить().Пустой() Тогда 
			
			ШаблонСообщения = НСтр("ru='Расширение SSCC ""%1"" уже используется в упаковочных листах.'");
			ТекстСообщения = СтрШаблон(ШаблонСообщения, ЦифраРасширенияSSCC);
			
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, "ЦифраРасширенияSSCC", "Объект.ЦифраРасширенияSSCC", Отказ);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
	
#КонецЕсли