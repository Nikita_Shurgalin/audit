#Область ОписаниеПеременных

&НаКлиенте
Перем ВыполняетсяЗапись;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_к2УпаковкиНоменклатуры", ПараметрыЗаписи, Объект.Ссылка);

КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если к2КоллекцииКлиентСервер.Разделить(Объект.Числитель, Объект.Знаменатель) = 1
		И (Модифицированность Или (Не ЗначениеЗаполнено(Объект.Ссылка))) Тогда
		
		Если Не ЕстьЕдиничнаяУпаковкаСЕдиницейИзмерения(Объект.Ссылка, Объект.Владелец, Объект.ЕдиницаИзмерения) И Не ВыполняетсяЗапись Тогда
			
			Если ТипЗнч(Объект.Владелец) = Тип("СправочникСсылка.Номенклатура") Тогда
				ТекстСообщения = НСтр("ru = 'Для номенклатуры ""%Владелец%"" уже создана упаковка с коэффициентом 1 и единицей измерения ""%ЕдиницаИзмерения%"".'");
			Иначе
				ТекстСообщения = НСтр("ru = 'Для набора ""%Владелец%"" уже создана упаковка с коэффициентом 1 и единицей измерения ""%ЕдиницаИзмерения%"".'");
			КонецЕсли;
			
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Владелец%", Строка(Объект.Владелец));
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ЕдиницаИзмерения%", Строка(Объект.ЕдиницаИзмерения));
			
			Отказ = Истина;
			
			Кнопки = Новый СписокЗначений();
			Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Записать'"));
			Кнопки.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Не записывать'"));
			
			ПоказатьВопрос(Новый ОписаниеОповещения("ПередЗаписьюЗавершение", ЭтотОбъект), ТекстСообщения, Кнопки, , , НСтр("ru = 'Дублирование единичной упаковки'"));
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ЕдиницаИзмеренияПриИзменении(Элемент)
		
	Объект.Наименование = ОбновитьНаименование(Объект.ТипУпаковки, Объект.ЕдиницаИзмерения, Объект.Числитель, Объект.Знаменатель, ЕдиницаИзмеренияВладельца);
	
	НастроитьФорму();

КонецПроцедуры

&НаКлиенте
Процедура ТипУпаковкиПриИзменении(Элемент)
	
	ОтработатьЛогикуСвязиРеквизитов();
	
КонецПроцедуры

&НаКлиенте
Процедура КоэффициентПриИзменении(Элемент)
	
	ОтработатьЛогикуСвязиРеквизитов();
	
КонецПроцедуры

&НаКлиенте
Процедура КоличествоУпаковокПриИзменении(Элемент)
	
	ОтработатьЛогикуСвязиРеквизитов();
	
КонецПроцедуры

&НаКлиенте
Процедура РодительПриИзменении(Элемент)
	
	ОтработатьЛогикуСвязиРеквизитов();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПередЗаписьюЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Нет Тогда
		Возврат;
	КонецЕсли;
	
	ВыполняетсяЗапись = Истина;
	Записать();
	ВыполняетсяЗапись = Ложь;

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ОбновитьКоэффициентРодителя();
	
	СтруктураРезультат = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Объект.Владелец, Новый Структура("ЕдиницаИзмеренияВладельца, ТипЕдиницыИзмеренияВладельца", "ЕдиницаИзмерения", "ЕдиницаИзмерения.ТипИзмеряемойВеличины"));
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, СтруктураРезультат);
	
	НастроитьФорму();
	
КонецПроцедуры

&НаСервере
Процедура НастроитьФорму()
	
	Если ТипЗнч(Объект.Владелец) = Тип("СправочникСсылка.Номенклатура") Тогда
		
		Элементы.Владелец.Заголовок = НСтр("ru = 'Номенклатура'");
		Элементы.ДекорацияПредупреждение.Видимость = Ложь;
		
	Иначе
		
		Элементы.Владелец.Заголовок = НСтр("ru = 'Набор упаковок'");
		Элементы.ДекорацияПредупреждение.Видимость = Истина;
		
	КонецЕсли;

	Если ЗначениеЗаполнено(Объект.ЕдиницаИзмерения) Тогда
		ПредставлениеЕдиницыИзмерения = Строка(Объект.ЕдиницаИзмерения);
	Иначе
		ПредставлениеЕдиницыИзмерения =  НСтр("ru = '<ед. изм. по классификатору>'");
	КонецЕсли;
	
	ТекстПредставления = НСтр("ru = '1 %ЕдиницаПоКлассификатору% состоит из'");
	ТекстПредставления = СтрЗаменить(ТекстПредставления, "%ЕдиницаПоКлассификатору%", ПредставлениеЕдиницыИзмерения);
	ПунктВыбора = Элементы.ТипУпаковки1.СписокВыбора.НайтиПоЗначению(Перечисления.к2ТипыУпаковокНоменклатуры.Конечная);
	ПунктВыбора.Представление = ТекстПредставления;
	
	ТекстПредставления = НСтр("ru = '1 %ЕдиницаПоКлассификатору% состоит из'");
	ТекстПредставления = СтрЗаменить(ТекстПредставления, "%ЕдиницаПоКлассификатору%", ПредставлениеЕдиницыИзмерения);
	ПунктВыбора = Элементы.ТипУпаковки1.СписокВыбора.НайтиПоЗначению(Перечисления.к2ТипыУпаковокНоменклатуры.Составная);
	ПунктВыбора.Представление = ТекстПредставления;
	
	ТекстПредставления = НСтр("ru = '1 %ЕдиницаИзмеренияВладельца% состоит из'");
	ТекстПредставления = СтрЗаменить(ТекстПредставления, "%ЕдиницаИзмеренияВладельца%", ЕдиницаИзмеренияВладельца);
	ПунктВыбора = Элементы.ТипУпаковки2.СписокВыбора.НайтиПоЗначению(Перечисления.к2ТипыУпаковокНоменклатуры.Разупаковка);
	ПунктВыбора.Представление = ТекстПредставления;
	                                                   
	Элементы.ТипУпаковки2.Видимость = Не Справочники.УпаковкиЕдиницыИзмерения.ЭтоМернаяЕдиница(ЕдиницаИзмеренияВладельца);
	Элементы.Знаменатель.Видимость = Элементы.ТипУпаковки2.Видимость;
	Элементы.НадписьЗнаменательЕдиницаИзмерения.Видимость = Элементы.ТипУпаковки2.Видимость;
	Элементы.ДекорацияРазупаковка.Видимость = Элементы.ТипУпаковки2.Видимость;
	
	Элементы.Числитель.Доступность = Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Конечная;
	Элементы.Числитель.Родитель.Доступность = Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Конечная;
	
	Элементы.КоличествоУпаковок.Доступность = Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Составная;
	Элементы.КоличествоУпаковок.Родитель.Доступность = Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Составная;
	
	Элементы.Знаменатель.Доступность = Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Разупаковка;
	Элементы.Знаменатель.Родитель.Доступность = Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Разупаковка;
	
	Если ТипЕдиницыИзмеренияВладельца = Перечисления.к2ТипыИзмеряемыхВеличин.КоличествоШтук Тогда
		Элементы.Числитель.МинимальноеЗначение = 1;
	Иначе
		Элементы.Числитель.МинимальноеЗначение = 0.001;
	КонецЕсли;
	 
КонецПроцедуры

#Область ПриИзмененииРеквизитов

&НаСервере
Процедура ОтработатьЛогикуСвязиРеквизитов()
	
	Справочники.УпаковкиЕдиницыИзмерения.ОтработатьЛогикуСвязиРеквизитов(Объект);
	
	Объект.Наименование = ОбновитьНаименование(Объект.ТипУпаковки, Объект.ЕдиницаИзмерения, Объект.Числитель, Объект.Знаменатель, ЕдиницаИзмеренияВладельца );
	
	НастроитьФорму();
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервереБезКонтекста
Функция ОбновитьНаименование(ТипУпаковки, ЕдиницаИзмерения, Числитель, Знаменатель, ЕдиницаИзмеренияВладельца)
	
	Возврат Справочники.УпаковкиЕдиницыИзмерения.СформироватьНаименование(ТипУпаковки, ЕдиницаИзмерения, Числитель, Знаменатель, ЕдиницаИзмеренияВладельца);
	
КонецФункции

&НаСервере
Процедура ОбновитьКоэффициентРодителя()
	
	Если Объект.ТипУпаковки = Перечисления.к2ТипыУпаковокНоменклатуры.Составная Тогда
		
		Результат = Справочники.УпаковкиЕдиницыИзмерения.КоэффициентПрочиеРеквизитыУпаковки(Объект.Родитель, Неопределено, "Числитель, Знаменатель");
		ЧислительРодителя = Результат.Числитель;
		ЗнаменательРодителя = Результат.Знаменатель;
		
	Иначе
		
		ЧислительРодителя = 1;
		ЗнаменательРодителя = 1;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ЕстьЕдиничнаяУпаковкаСЕдиницейИзмерения(Ссылка, Владелец, ЕдиницаИзмерения)
	
	ЕстьЕдиничнаяУпаковка = Ложь;
	
	Запрос = Новый Запрос;
	ТекстЗапроса =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	&ТекстЗапросаКоэффициентУпаковки КАК КоличествоЕдиничныхУпаковок,
	|	УпаковкиНоменклатуры.Владелец,
	|	УпаковкиНоменклатуры.Ссылка
	|ИЗ
	|	Справочник.УпаковкиЕдиницыИзмерения КАК УпаковкиНоменклатуры
	|ГДЕ
	|	&ТекстЗапросаКоэффициентУпаковки = 1
	|	И УпаковкиНоменклатуры.ПометкаУдаления = ЛОЖЬ
	|	И УпаковкиНоменклатуры.Владелец = &Владелец
	|	И УпаковкиНоменклатуры.ЕдиницаИзмерения = &ЕдиницаИзмерения";
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&ТекстЗапросаКоэффициентУпаковки", Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки("УпаковкиНоменклатуры", Неопределено));
	
	Запрос.Текст = ТекстЗапроса;
	
	Запрос.УстановитьПараметр("Владелец", Владелец);
	Запрос.УстановитьПараметр("ЕдиницаИзмерения", ЕдиницаИзмерения);
	РезультатЗапроса = Запрос.Выполнить();
		
	Если РезультатЗапроса.Пустой() Тогда
		
		ЕстьЕдиничнаяУпаковка =  Истина;
		
	Иначе
		
		Если ЗначениеЗаполнено(Ссылка) Тогда
			
			Выборка = РезультатЗапроса.Выбрать();
			Выборка.Следующий();
			
			ЕстьЕдиничнаяУпаковка = Выборка.Ссылка = Ссылка;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ЕстьЕдиничнаяУпаковка;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#Область Инициализация

ВыполняетсяЗапись = Ложь;

#КонецОбласти
