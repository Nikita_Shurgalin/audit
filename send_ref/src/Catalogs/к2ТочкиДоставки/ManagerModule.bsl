#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ПрограммныйИнтерфейс

// Получает склад отгрузки номенклатуры.
//
// Параметры:
//	Номенклатура - СправочникСсылка.к2Номенклатуры - Номенклатура.
//  ТочкаДоставки - СправочникСсылка.к2ТочкиДоставки - Точка доставки.
//
// Возвращаемое значение:
//  СправочникСсылка.Склады - склад.
//
Функция ПолучитьСкладОтгрузки(Номенклатура, ТочкаДоставки) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	к2ТочкиДоставкиУточненияПоОтгрузке.Склад КАК Склад,
	               |	0 КАК Приоритет
	               |ПОМЕСТИТЬ ВТ
	               |ИЗ
	               |	Справочник.к2ТочкиДоставки.УточненияПоОтгрузке КАК к2ТочкиДоставкиУточненияПоОтгрузке
	               |ГДЕ
	               |	к2ТочкиДоставкиУточненияПоОтгрузке.ТоварнаяКатегория = &ТоварнаяКатегория
	               |	И к2ТочкиДоставкиУточненияПоОтгрузке.Ссылка = &Ссылка
	               |
	               |ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ
	               |	к2ТочкиДоставки.СкладОтгрузки,
	               |	1
	               |ИЗ
	               |	Справочник.к2ТочкиДоставки КАК к2ТочкиДоставки
	               |ГДЕ
	               |	к2ТочкиДоставки.Ссылка = &Ссылка
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ВТ.Склад КАК Склад,
	               |	ВТ.Приоритет КАК Приоритет
	               |ИЗ
	               |	ВТ КАК ВТ
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	Приоритет";
	Запрос.УстановитьПараметр("ТоварнаяКатегория", глРеквизит(Номенклатура, "ТоварнаяКатегория"));
	Запрос.УстановитьПараметр("Ссылка", ТочкаДоставки);
	
	Возврат к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).Склад;
	
КонецФункции

// Значения реквизитов выбранной точки доставки.
//
// Параметры:
//  пТочкаДоставки - СправочникСсылка.к2ТочкиДоставки - Точка доставки.
//
// Возвращаемое значение:
//  Структура - реквизиты выбранной точки доставки.
//
Функция Сведения( Знач пТочкаДоставки ) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	к2ТочкиДоставки.Ссылка КАК Ссылка
		|ПОМЕСТИТЬ ТочкаДоставки
		|ИЗ
		|	Справочник.к2ТочкиДоставки КАК к2ТочкиДоставки
		|ГДЕ
		|	к2ТочкиДоставки.Ссылка = &ТочкаДоставки
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	ТочкаДоставки.Ссылка.НаименованиеПолное КАК НаименованиеПолное,
		|	Контрагенты.ИНН КАК ИНН,
		|	ТочкаДоставки.Ссылка.КПП КАК КПП,
		|	Контрагенты.КодПоОКПО КАК КодПоОКПО,
		|	Контрагенты.ЮрФизЛицо КАК ЮрФизЛицо,
		|	Контрагенты.СтранаРегистрации КАК СтранаРегистрации,
		|	Контрагенты.Ссылка КАК Контрагент
		|ИЗ
		|	ТочкаДоставки КАК ТочкаДоставки
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Контрагенты КАК Контрагенты
		|		ПО ТочкаДоставки.Ссылка.Владелец = Контрагенты.Ссылка";
	
	Запрос.УстановитьПараметр( "ТочкаДоставки", пТочкаДоставки );
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Представление              = Выборка.НаименованиеПолное;
		Наименование               = Выборка.НаименованиеПолное;
		ИНН                        = Выборка.ИНН;
		КПП                        = Выборка.КПП;
		КодПоОКПО                  = Выборка.КодПоОКПО;
		ЮрФизЛицо                  = Выборка.ЮрФизЛицо;
		СтранаРегистрации          = Выборка.СтранаРегистрации;
		
		ЮрАдрес = УправлениеКонтактнойИнформацией.КонтактнаяИнформацияОбъекта(
			Выборка.Контрагент,
			Справочники.ВидыКонтактнойИнформации.ЮрАдресКонтрагента,
			ТекущаяДатаСеанса() );
		ФактАдрес = УправлениеКонтактнойИнформацией.КонтактнаяИнформацияОбъекта(
			пТочкаДоставки,
			Справочники.ВидыКонтактнойИнформации.АдресТочкиДоставки,
			ТекущаяДатаСеанса() );
		Телефон = УправлениеКонтактнойИнформацией.КонтактнаяИнформацияОбъекта(
			пТочкаДоставки,
			Справочники.ВидыКонтактнойИнформации.ТелефонТочкиДоставки,
			ТекущаяДатаСеанса() );
		
	Иначе
		
		Представление      = "";
		Наименование       = "";
		ИНН                = "";
		КПП                = "";
		КодПоОКПО          = "";
		ЮрФизЛицо          = Перечисления.к2ЮрФизЛицо.ПустаяСсылка();
		
		ЮрАдрес   = "";
		ФактАдрес = "";
		Телефон   = "";
		
	КонецЕсли;
	
	СтруктураСведений = Новый Структура;
	СтруктураСведений.Вставить( "Представление", Представление );
	СтруктураСведений.Вставить( "Наименование", Наименование );
	СтруктураСведений.Вставить( "ИНН", ИНН );
	СтруктураСведений.Вставить( "КПП", КПП );
	СтруктураСведений.Вставить( "КодПоОКПО", КодПоОКПО );
	СтруктураСведений.Вставить( "ЮрФизЛицо", ЮрФизЛицо );
	СтруктураСведений.Вставить( "СтранаРегистрации", СтранаРегистрации );
	СтруктураСведений.Вставить( "ЮридическийАдрес", ЮрАдрес );
	СтруктураСведений.Вставить( "ФактическийАдрес", ФактАдрес );
	СтруктураСведений.Вставить( "Телефоны", Телефон );
	
	Возврат СтруктураСведений;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов( Настройки ) Экспорт
	
	Возврат;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти

#КонецОбласти

#КонецЕсли