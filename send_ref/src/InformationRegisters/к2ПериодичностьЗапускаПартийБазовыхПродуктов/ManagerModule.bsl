
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Функция ТекущаяПериодичностьЗапуска(БазовыйПродукт) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	к2ПериодичностьЗапускаПартийБазовыхПродуктовСрезПоследних.ПериодичностьЗапуска КАК ПериодичностьЗапуска
	               |ИЗ
	               |	РегистрСведений.к2ПериодичностьЗапускаПартийБазовыхПродуктов.СрезПоследних(, БазовыйПродукт = &БазовыйПродукт) КАК к2ПериодичностьЗапускаПартийБазовыхПродуктовСрезПоследних";
	
	Запрос.УстановитьПараметр("БазовыйПродукт", БазовыйПродукт);
	
	Возврат к2ОбщегоНазначения.СтруктураПервойВыборкиЗапроса(Запрос).ПериодичностьЗапуска;
	
КонецФункции	

Процедура Отразить(Структура) Экспорт 
	
	мз = СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(мз, Структура);
	мз.Записать();
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
