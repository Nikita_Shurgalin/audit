
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере( Отказ, СтандартнаяОбработка )
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	Параметры.Свойство( "Номенклатура", Номенклатура );
	Параметры.Свойство( "Характеристика", Характеристика );
	Параметры.Свойство( "ВидАнализа", ВидАнализа );
	Параметры.Свойство( "Показатель", Показатель );
	
	ЗаполнитьСписокВыбора_Номенклатура();
	ЗаполнитьСписокВыбора_ВидАнализа();
	ЗаполнитьСписокВыбора_Показатель();
	ЗаполнитьСписокВыбора_Характеристика();
	ЗаполнитьНормативИГраницы();
	ОбновитьДиаграмму();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	к2СобытияФормКлиент.ПриОткрытии(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура НоменклатураПриИзменении( Элемент )
	
	ЗаполнитьСписокВыбора_ВидАнализа();
	ЗаполнитьСписокВыбора_Показатель();
	ЗаполнитьНормативИГраницы();
	ОбновитьДиаграмму();
	
	УстановитьВидимостьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ВидАнализаПриИзменении( Элемент )
	
	ЗаполнитьСписокВыбора_Номенклатура();
	ЗаполнитьСписокВыбора_Показатель();
	ЗаполнитьНормативИГраницы();
	ОбновитьДиаграмму();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказательПриИзменении( Элемент )
	
	ЗаполнитьСписокВыбора_Номенклатура();
	ЗаполнитьСписокВыбора_ВидАнализа();
	ЗаполнитьНормативИГраницы();
	ОбновитьДиаграмму();
	
КонецПроцедуры

&НаКлиенте
Процедура ПериодПриИзменении(Элемент)
	
	ОбновитьДиаграмму();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОбновитьДиаграмму_Команда( Команда )
	
	ОбновитьДиаграмму();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьСписокВыбора_Номенклатура()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	к2РезультатыАнализовНоменклатурыСрезПоследних.Номенклатура КАК Номенклатура
		|ИЗ
		|	РегистрСведений.к2РезультатыАнализовНоменклатуры.СрезПоследних(, &Условие) КАК к2РезультатыАнализовНоменклатурыСрезПоследних
		|
		|УПОРЯДОЧИТЬ ПО
		|	к2РезультатыАнализовНоменклатурыСрезПоследних.Номенклатура.Наименование";
	
	массивУсловий = Новый Массив;
	
	Если ЗначениеЗаполнено( ВидАнализа ) Тогда
		
		массивУсловий.Добавить( "ВидАнализа = &ВидАнализа" );
		Запрос.УстановитьПараметр( "ВидАнализа", ВидАнализа );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Показатель ) Тогда
		
		массивУсловий.Добавить( "Показатель = &Показатель" );
		Запрос.УстановитьПараметр( "Показатель", Показатель );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Характеристика ) Тогда
		
		массивУсловий.Добавить( "Характеристика = &Характеристика" );
		Запрос.УстановитьПараметр( "Характеристика", Характеристика );
		
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить( Запрос.Текст, "&Условие", СтрСоединить( массивУсловий, " И " ) );
	
	Элементы.Номенклатура.СписокВыбора.ЗагрузитьЗначения( Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку( 0 ) );
	
	Если Элементы.Номенклатура.СписокВыбора.Количество() = 1 Тогда
		
		Номенклатура = Элементы.Номенклатура.СписокВыбора[0].Значение;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокВыбора_ВидАнализа()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	к2РезультатыАнализовНоменклатурыСрезПоследних.ВидАнализа КАК ВидАнализа
		|ИЗ
		|	РегистрСведений.к2РезультатыАнализовНоменклатуры.СрезПоследних(, &Условие) КАК к2РезультатыАнализовНоменклатурыСрезПоследних
		|
		|УПОРЯДОЧИТЬ ПО
		|	к2РезультатыАнализовНоменклатурыСрезПоследних.ВидАнализа.Наименование";
	
	массивУсловий = Новый Массив;
	
	Если ЗначениеЗаполнено( Номенклатура ) Тогда
		
		массивУсловий.Добавить( "Номенклатура = &Номенклатура" );
		Запрос.УстановитьПараметр( "Номенклатура", Номенклатура );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Показатель ) Тогда
		
		массивУсловий.Добавить( "Показатель = &Показатель" );
		Запрос.УстановитьПараметр( "Показатель", Показатель );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Характеристика ) Тогда
		
		массивУсловий.Добавить( "Характеристика = &Характеристика" );
		Запрос.УстановитьПараметр( "Характеристика", Характеристика );
		
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить( Запрос.Текст, "&Условие", стрСобрать( массивУсловий, " И " ) );
	
	Элементы.ВидАнализа.СписокВыбора.ЗагрузитьЗначения( Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку( 0 ) );
	
	Если Элементы.ВидАнализа.СписокВыбора.Количество() = 1 Тогда
		
		ВидАнализа = Элементы.ВидАнализа.СписокВыбора[0].Значение;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокВыбора_Показатель()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	к2РезультатыАнализовНоменклатурыСрезПоследних.Показатель КАК Показатель
		|ИЗ
		|	РегистрСведений.к2РезультатыАнализовНоменклатуры.СрезПоследних(, &Условие) КАК к2РезультатыАнализовНоменклатурыСрезПоследних
		|
		|УПОРЯДОЧИТЬ ПО
		|	к2РезультатыАнализовНоменклатурыСрезПоследних.Показатель.Наименование";
	
	массивУсловий = Новый Массив;
	
	Если ЗначениеЗаполнено( Номенклатура ) Тогда
		
		массивУсловий.Добавить( "Номенклатура = &Номенклатура" );
		Запрос.УстановитьПараметр( "Номенклатура", Номенклатура );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( ВидАнализа ) Тогда
		
		массивУсловий.Добавить( "ВидАнализа = &ВидАнализа" );
		Запрос.УстановитьПараметр( "ВидАнализа", ВидАнализа );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Характеристика ) Тогда
		
		массивУсловий.Добавить( "Характеристика = &Характеристика" );
		Запрос.УстановитьПараметр( "Характеристика", Характеристика );
		
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить( Запрос.Текст, "&Условие", стрСобрать( массивУсловий, " И " ) );
	
	Элементы.Показатель.СписокВыбора.ЗагрузитьЗначения( Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку( 0 ) );
	
	Если Элементы.Показатель.СписокВыбора.Количество() = 1 Тогда
		
		Показатель = Элементы.Показатель.СписокВыбора[0].Значение;
		ЗаполнитьНормативИГраницы();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокВыбора_Характеристика()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
	               |	к2РезультатыАнализовНоменклатурыСрезПоследних.Характеристика КАК Характеристика
	               |ИЗ
	               |	РегистрСведений.к2РезультатыАнализовНоменклатуры.СрезПоследних(, &Условие) КАК к2РезультатыАнализовНоменклатурыСрезПоследних
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	к2РезультатыАнализовНоменклатурыСрезПоследних.Характеристика.Наименование";
	
	массивУсловий = Новый Массив;
	
	Если ЗначениеЗаполнено( Номенклатура ) Тогда
		
		массивУсловий.Добавить( "Номенклатура = &Номенклатура" );
		Запрос.УстановитьПараметр( "Номенклатура", Номенклатура );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( ВидАнализа ) Тогда
		
		массивУсловий.Добавить( "ВидАнализа = &ВидАнализа" );
		Запрос.УстановитьПараметр( "ВидАнализа", ВидАнализа );
		
	КонецЕсли;
	
	Если ЗначениеЗаполнено( Характеристика ) Тогда
		
		массивУсловий.Добавить( "Показатель = &Показатель" );
		Запрос.УстановитьПараметр( "Показатель", Показатель );
		
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить( Запрос.Текст, "&Условие", стрСобрать( массивУсловий, " И " ) );
	
	Элементы.Характеристика.СписокВыбора.ЗагрузитьЗначения( Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку( 0 ) );
	
	Если Элементы.Характеристика.СписокВыбора.Количество() = 1 Тогда
		
		Характеристика = Элементы.Характеристика.СписокВыбора[0].Значение;
		ЗаполнитьНормативИГраницы();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьНормативИГраницы()
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	               |	к2АнализыНоменклатуры_НормативыСрезПоследних.ЗначениеС КАК ЗначениеС,
	               |	к2АнализыНоменклатуры_НормативыСрезПоследних.ЗначениеПо КАК ЗначениеПо
	               |ИЗ
	               |	РегистрСведений.к2АнализыНоменклатуры_Нормативы.СрезПоследних(
	               |			,
	               |			Номенклатура = &Номенклатура
	               |				И Характеристика = &Характеристика
	               |				И ВидАнализа = &ВидАнализа
	               |				И Показатель = &Показатель) КАК к2АнализыНоменклатуры_НормативыСрезПоследних";
	Запрос.УстановитьПараметр( "Номенклатура", Номенклатура );
	Запрос.УстановитьПараметр( "Характеристика", Характеристика );
	Запрос.УстановитьПараметр( "ВидАнализа", ВидАнализа );
	Запрос.УстановитьПараметр( "Показатель", Показатель );
	
	выборка = Запрос.Выполнить().Выбрать();
	
	Если выборка.Следующий() Тогда
		
		ширинаНорматива = нормативПо - НормативС;
		
		Если ДиапазонДиаграммыС = Окр( НормативС - 0.2*ширинаНорматива, 3 ) Тогда
			
			ДиапазонДиаграммыС = 0;
			
		КонецЕсли;
		
		Если ДиапазонДиаграммыПо = Окр( НормативПо + 0.2*ширинаНорматива, 3 ) Тогда
			
			ДиапазонДиаграммыПо = 0;
			
		КонецЕсли;
		
		нормативС  = выборка.ЗначениеС;
		нормативПо = выборка.ЗначениеПо;
		
	КонецЕсли;
	
	ширинаНорматива = нормативПо - НормативС;
	
	Если Не ЗначениеЗаполнено( ДиапазонДиаграммыС ) Тогда
		
		ДиапазонДиаграммыС = Окр( НормативС - 0.2*ширинаНорматива, 3 );
		
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено( ДиапазонДиаграммыПо ) Тогда
		
		ДиапазонДиаграммыПо = Окр( НормативПо + 0.2*ширинаНорматива, 3 );
			
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДиаграмму()
	
	Диаграмма.Очистить();
	
	Если Это_8_3_12_ИСтарше() Тогда
		
		Диаграмма.ТипДиаграммы = ТипДиаграммы.График;
		
	Иначе
		
		Диаграмма.ТипДиаграммы = ТипДиаграммы.ГистограммаСНакоплением;
		
	КонецЕсли;
	
	Диаграмма.ОбластьЛегенды.Расположение = РасположениеЛегендыДиаграммы.Нет;
	Диаграмма.ОбластьПостроения.Расположение = РасположениеОбластиПостроенияДиаграммы.СвободноеМесто;
	
	Диаграмма.ОтображатьЗаголовок = Ложь;
	
	Если Не ЗначениеЗаполнено( Номенклатура )
		И Не ЗначениеЗаполнено( ВидАнализа )
		И Не ЗначениеЗаполнено( Показатель ) Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Диаграмма.РежимПробелов = РежимПробеловДиаграммы.Нет;
	
	Диаграмма.АвтоМинимальноеЗначение = Истина;
	
	Если ЗначениеЗаполнено( ДиапазонДиаграммыС ) Тогда
		
		Диаграмма.АвтоМинимальноеЗначение = Ложь;
		Диаграмма.МинимальноеЗначение     = ДиапазонДиаграммыС;
		
	КонецЕсли;
	
	Диаграмма.АвтоМаксимальноеЗначение = Истина;
	
	Если ЗначениеЗаполнено( ДиапазонДиаграммыПо ) Тогда
		
		Диаграмма.АвтоМаксимальноеЗначение = Ложь;
		Диаграмма.МаксимальноеЗначение     = ДиапазонДиаграммыПо;
		
	КонецЕсли;
	
	Диаграмма.Окантовка                 = Ложь;
	Диаграмма.ПропускатьБазовоеЗначение = Истина;
	
	Диаграмма.РежимПолупрозрачности = РежимПолупрозрачностиДиаграммы.Использовать;
	Диаграмма.РежимСглаживания = РежимСглаживанияДиаграммы.ГладкаяКривая;
	
	Серия_ПоказательВНорме           = Диаграмма.Серии.Добавить( "Показатель" );
	Серия_ПоказательВНорме.Индикатор = Истина;
	Серия_ПоказательВНорме.Цвет      = WebЦвета.СинийСоСтальнымОттенком;
	Серия_ПоказательВНорме.Маркер    = ТипМаркераДиаграммы.Круг;
	Серия_ПоказательВНорме.Линия     = Новый Линия( ТипЛинииДиаграммы.Сплошная, 2 );
	
	Если Это_8_3_12_ИСтарше() Тогда

		Пока Диаграмма.ИнформационныеИнтервалыЗначений.Количество() > 0 Цикл
			
			Диаграмма.ИнформационныеИнтервалыЗначений.Удалить(0);
			
		КонецЦикла;
		
		новИнтервал = Диаграмма.ИнформационныеИнтервалыЗначений.Добавить();
		
		новИнтервал.Начало = НормативС;
		новИнтервал.Конец = НормативПо;
		
		новИнтервал.Цвет = Новый Цвет( 159, 222, 169 );
		
	Иначе
		
		СерияНиже                = Диаграмма.Серии.Добавить( "Ниже" );
		СерияНиже.Цвет           = Диаграмма.ЦветФона;
		СерияНиже.ПриоритетЦвета = Истина;
		
		СерияНорматив                = Диаграмма.Серии.Добавить( "Норматив" );
		СерияНорматив.Цвет           = Новый Цвет( 159, 222, 169 );
		СерияНорматив.ПриоритетЦвета = Истина;
		
	КонецЕсли;
	
	Запрос       = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	к2РезультатыАнализовНоменклатуры.ЗначениеПоказателя КАК ЗначениеПоказателя,
	               |	к2РезультатыАнализовНоменклатуры.Период КАК Период,
	               |	к2РезультатыАнализовНоменклатуры.Серия КАК Серия
	               |ИЗ
	               |	РегистрСведений.к2РезультатыАнализовНоменклатуры КАК к2РезультатыАнализовНоменклатуры
	               |ГДЕ
	               |	к2РезультатыАнализовНоменклатуры.Номенклатура = &Номенклатура
	               |	И к2РезультатыАнализовНоменклатуры.ВидАнализа = &ВидАнализа
	               |	И к2РезультатыАнализовНоменклатуры.Показатель = &Показатель
	               |	И к2РезультатыАнализовНоменклатуры.Период >= &НачалоПериода
	               |	И к2РезультатыАнализовНоменклатуры.Период <= &КонецПериода
	               |	И НЕ к2РезультатыАнализовНоменклатуры.ЗначениеПоказателя = 0
	               |	И НЕ к2РезультатыАнализовНоменклатуры.ЗначениеПоказателя = НЕОПРЕДЕЛЕНО
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	Период";
	Запрос.УстановитьПараметр( "Номенклатура", Номенклатура );
	Запрос.УстановитьПараметр( "ВидАнализа", ВидАнализа );
	Запрос.УстановитьПараметр( "Показатель", Показатель );
	Запрос.УстановитьПараметр( "НачалоПериода", Период.ДатаНачала );
	
	Если ЗначениеЗаполнено( Период.ДатаОкончания ) Тогда
		
		Запрос.УстановитьПараметр( "КонецПериода", Период.ДатаОкончания );
		
	Иначе
		
		Запрос.УстановитьПараметр( "КонецПериода", Дата( 3999, 12, 31 ) );
		
	КонецЕсли;
	
	выборка = Запрос.Выполнить().Выбрать();
	
	Пока выборка.Следующий() Цикл
		
		// Заполним данные диаграммы
		Точка = Диаграмма.УстановитьТочку( Формат( выборка.Период, "ДФ=dd.MM.yy" ));
		
		расшифровка = Формат( выборка.Период, "ДФ=dd.MM.yy" ) + " - " + выборка.Серия;
		
		Диаграмма.УстановитьЗначение( Точка, Серия_ПоказательВНорме, выборка.ЗначениеПоказателя, , расшифровка );
		
		Если Не Это_8_3_12_ИСтарше() Тогда
			
			Диаграмма.УстановитьЗначение( Точка, СерияНиже, нормативС );
			Диаграмма.УстановитьЗначение( Точка, СерияНорматив, нормативПо - НормативС );
			
		КонецЕсли;
		
		Если ТипЗнч( выборка.ЗначениеПоказателя ) = Тип("Число")
			И выборка.ЗначениеПоказателя < Диаграмма.МинимальноеЗначение Тогда
			
			Диаграмма.МинимальноеЗначение = выборка.ЗначениеПоказателя * 0.9;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьДоступность()
	
	ИспользованиеХарактеристик = глРеквизит(Номенклатура, "ИспользованиеХарактеристик");
	ИспользуютсяХарактеристикиНоменклатуры =
		(Не ИспользованиеХарактеристик = Перечисления.к2ВариантыИспользованияХарактеристикНоменклатуры.НеИспользовать);
	
	Элементы.Характеристика.Видимость = ИспользуютсяХарактеристикиНоменклатуры;
	
КонецПроцедуры

Функция Это_8_3_12_ИСтарше()

	СистемнаяИнформация = Новый СистемнаяИнформация;
	Текущая       = СистемнаяИнформация.ВерсияПриложения;
	
	Возврат ОбщегоНазначенияКлиентСервер.СравнитьВерсии(Текущая, "8.3.12.0") > 0;

КонецФункции

#КонецОбласти
