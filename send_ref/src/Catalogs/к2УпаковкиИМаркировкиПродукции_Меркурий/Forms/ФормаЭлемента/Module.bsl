#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	к2СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	к2СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	к2СобытияФорм.ПередЗаписьюНаСервере(ЭтотОбъект, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ХозяйствующийСубъектПолучательПриИзменении(Элемент)
	
	ПредставлениеНаименования();
	
КонецПроцедуры

&НаКлиенте
Процедура НаименованиеПродукцииПриИзменении(Элемент)
	
	ПредставлениеНаименования();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыФизическаяУпаковка

&НаКлиенте
Процедура ФизическаяУпаковкаПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока Тогда
		
		Элемент.ТекущиеДанные.КлючСвязиСМаркировкой = Объект.ФизическаяУпаковкаПродукции.Количество();
		
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыМаркировкаПродукции

&НаКлиенте
Процедура МаркировкаПродукцииПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Если Элементы.ФизическаяУпаковка.ТекущиеДанные = Неопределено Тогда
		
		Отказ = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура МаркировкаПродукцииПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока И Не Копирование Тогда
		
		Элемент.ТекущиеДанные.КлючСвязи = Элементы.ФизическаяУпаковка.ТекущиеДанные.КлючСвязиСМаркировкой;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаКлиенте
Процедура ПредставлениеНаименования()
	
	массивСтрок = Новый Массив;
	массивСтрок.Добавить(Объект.ХозяйствующийСубъектПолучатель);
	массивСтрок.Добавить(Объект.НаименованиеПродукции);
	
	Объект.Наименование = СтрСоединить(массивСтрок);

КонецПроцедуры

#КонецОбласти

#КонецОбласти