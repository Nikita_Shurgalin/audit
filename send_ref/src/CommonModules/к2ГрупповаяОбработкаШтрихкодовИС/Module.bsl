#Область ПрограммныйИнтерфейс

Функция ВидУпаковкиИПредставлениеШтрихкода(Знач Штрихкод, ВидПродукции, КешДанныхРазбора = Неопределено) Экспорт
	
	РезультатРазбора            = Неопределено;
	ПримечаниеКРазборуШтрихкода = Неопределено;
	
	Если НайтиНедопустимыеСимволыXML(Штрихкод) > 0 Тогда
		РезультатРазбора = МенеджерОборудованияМаркировкаКлиентСервер.РазобратьСтрокуШтрихкодаGS1(Штрихкод);
		ДанныеРазбора = к2ШтрихкодированиеИССлужебный.РазобратьКодМаркировки(
			РезультатРазбора, ВидПродукции, ПримечаниеКРазборуШтрихкода);
	Иначе
		ДанныеРазбора = к2ШтрихкодированиеИССлужебный.РазобратьКодМаркировки(
			Штрихкод, ВидПродукции, ПримечаниеКРазборуШтрихкода);
	КонецЕсли;
	
	ВидУпаковки = Неопределено;
	Если ДанныеРазбора = Неопределено Тогда
		Если РезультатРазбора <> Неопределено И РезультатРазбора.Разобран Тогда
			НормализованныйШтрихкод = РезультатРазбора.ПредставлениеШтрихкода;
		Иначе
			НормализованныйШтрихкод = Штрихкод;
		КонецЕсли;
	Иначе
		ВидУпаковки             = ДанныеРазбора.ВидУпаковки;
		НормализованныйШтрихкод = ДанныеРазбора.НормализованныйКодМаркировки;
	КонецЕсли;
	
	Если КешДанныхРазбора <> Неопределено Тогда
		ДанныеРазбораИРезультат = Новый Структура;
		ДанныеРазбораИРезультат.Вставить("ДанныеРазбора",               ДанныеРазбора);
		ДанныеРазбораИРезультат.Вставить("ПримечаниеКРазборуШтрихкода", ПримечаниеКРазборуШтрихкода);
		КешДанныхРазбора.Вставить(Штрихкод, ДанныеРазбораИРезультат);
	КонецЕсли;
	
	Возврат Новый Структура("ВидУпаковки, НормализованныйШтрихкод", ВидУпаковки, НормализованныйШтрихкод);
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ЗагрузкаИзФормДокументов

// При необходимости изменяет данные поступившие из ТСД по правилам: вложение упаковки всегда должно 
//   быть в массиве после строки самой упаковки; если строки с штрихкодом упаковки нет - она добавляется.
//
// Параметры:
//   ДанныеТСД - Массив - данные поступившие с ТСД (изменяемый параметр)
//
// Возвращаемое значение:
//   Булево - в данных ТСД есть иерархия
//
Функция ДополнитьУпорядочитьДанныеТСД(ДанныеТСД) Экспорт
	
	// 1. Дополнить данные поступившие из ТСД / внешних источников (в формате ТСД)
	ВсеШтрихкоды = Новый Массив;
	Для Каждого ЭлементМассива Из ДанныеТСД Цикл
		ВсеШтрихкоды.Добавить(ЭлементМассива.Штрихкод);
	КонецЦикла;
	Для Каждого ЭлементМассива Из ДанныеТСД Цикл
		ШтрихкодУпаковки = "";
		Если Не ЭлементМассива.Свойство("ШтрихкодУпаковки", ШтрихкодУпаковки) Тогда
			Продолжить;
		ИначеЕсли Не ЗначениеЗаполнено(ШтрихкодУпаковки) Тогда
			Продолжить;
		ИначеЕсли ВсеШтрихкоды.Найти(ШтрихкодУпаковки) = Неопределено Тогда
			ВсеШтрихкоды.Добавить(ШтрихкодУпаковки);
			НовыйЭлемент = ОбщегоНазначения.СкопироватьРекурсивно(ЭлементМассива, Ложь);
			Для Каждого КлючИЗначение Из НовыйЭлемент Цикл
				ТипКолонки = ТипЗнч(ЭлементМассива[КлючИЗначение.Ключ]);
				Если ТипКолонки = Тип("Строка") Тогда
					НовыйЭлемент[КлючИЗначение.Ключ] = "";
				ИначеЕсли ТипКолонки = Тип("Число") Тогда
					НовыйЭлемент[КлючИЗначение.Ключ] = 0;
				Иначе
					НовыйЭлемент[КлючИЗначение.Ключ] = Неопределено;
				КонецЕсли;
			КонецЦикла;
			НовыйЭлемент.Штрихкод         = ШтрихкодУпаковки;
			НовыйЭлемент.ШтрихкодУпаковки = "";
			ДанныеТСД.Добавить(НовыйЭлемент);
		КонецЕсли;
	КонецЦикла;
	
	// 2. Сортировать данные
	ТаблицаСоответствия = Новый ТаблицаЗначений;
	ТаблицаСоответствия.Колонки.Добавить("ШтрихкодУпаковки");
	ТаблицаСоответствия.Колонки.Добавить("Штрихкод");
	ТаблицаСоответствия.Колонки.Добавить("Уровень");
	ТаблицаСоответствия.Колонки.Добавить("Элемент");
	
	Для Каждого ЭлементМассива Из ДанныеТСД Цикл
		
		ШтрихкодУпаковки = "";
		ЭлементМассива.Свойство("ШтрихкодУпаковки", ШтрихкодУпаковки);
		
		НоваяСтрока = ТаблицаСоответствия.Добавить();
		НоваяСтрока.ШтрихкодУпаковки = ШтрихкодУпаковки;
		НоваяСтрока.Штрихкод         = ЭлементМассива.Штрихкод;
		НоваяСтрока.Уровень          = 0;
		НоваяСтрока.Элемент          = ЭлементМассива;
		
	КонецЦикла;
	
	ТаблицаСоответствия.Индексы.Добавить("Уровень");
	ТаблицаСоответствия.Индексы.Добавить("Уровень, Штрихкод");
	
	Уровень = 0;
	СчитатьИерархию = Истина;
	СтрокТаблицы    = ТаблицаСоответствия.Количество();
	Пока СчитатьИерархию И Уровень <= СтрокТаблицы Цикл
		СчитатьИерархию = Ложь;
		Для Каждого СтрокаТЧ Из ТаблицаСоответствия.НайтиСтроки(Новый Структура("Уровень", Уровень)) Цикл
			Если СтрокаТЧ.ШтрихкодУпаковки = "" Тогда
				Продолжить;
			КонецЕсли;
			Упаковки = ТаблицаСоответствия.НайтиСтроки(Новый Структура("Уровень, Штрихкод", Уровень, СтрокаТЧ.ШтрихкодУпаковки));
			Если Упаковки.Количество() Тогда
				СчитатьИерархию = Истина;
				СтрокаТЧ.Уровень = Уровень + 1;
			КонецЕсли;
		КонецЦикла;
		Уровень = Уровень + 1;
	КонецЦикла;
	
	Если (Уровень > СтрокТаблицы) Тогда
		ДанныеТСД = Новый Массив;
		ВызватьИсключение НСтр("ru = 'Получены некорректные данные из ТСД или внешнего файла: обнаружено зацикливание упаковок'");
	КонецЕсли;
	
	ТаблицаСоответствия.Сортировать("Уровень");
	
	СортированныйМассив = Новый Массив;
	Для Каждого СтрокаТЧ Из ТаблицаСоответствия Цикл
		ЭлементДанных = СтрокаТЧ.Элемент;
		ЭлементДанных.Вставить("Уровень", СтрокаТЧ.Уровень);
		СортированныйМассив.Добавить(ЭлементДанных);
	КонецЦикла;
	ДанныеТСД = СортированныйМассив;
	
	Возврат Уровень > 1;
	
КонецФункции

Процедура УпорядочитьДанныеТСДПоРезультатамОбработкиШтрихкодов(ДанныеТСД, РезультатыОбработки) Экспорт
	
	ТаблицаСоответствия = Новый ТаблицаЗначений;
	ТаблицаСоответствия.Колонки.Добавить("Уровень",            Новый ОписаниеТипов("Число"));
	ТаблицаСоответствия.Колонки.Добавить("ТребуетсяУточнение", Новый ОписаниеТипов("Булево"));
	ТаблицаСоответствия.Колонки.Добавить("Элемент");
	
	ТаблицаСоответствия.Колонки.Добавить("Номенклатура",   Метаданные.ОпределяемыеТипы.к2НоменклатураИСМП.Тип);
	ТаблицаСоответствия.Колонки.Добавить("Характеристика", Метаданные.ОпределяемыеТипы.к2ХарактеристикаНоменклатурыИСМП.Тип);
	ТаблицаСоответствия.Колонки.Добавить("Серия",          Метаданные.ОпределяемыеТипы.к2СерияНоменклатурыИСМП.Тип);
	ТаблицаСоответствия.Колонки.Добавить("GTIN",           Метаданные.ОпределяемыеТипы.GTIN.Тип);
	ТаблицаСоответствия.Колонки.Добавить("МРЦ",            Новый ОписаниеТипов("Число"));
	ТаблицаСоответствия.Колонки.Добавить("ГоденДо",        Новый ОписаниеТипов("Дата"));
	
	ЗаполнятьМРЦ     = Неопределено;
	ЗаполнятьГоденДо = Неопределено;
	
	Для Каждого ЭлементМассива Из ДанныеТСД Цикл
		
		РезультатОбработки = РезультатыОбработки.Получить(ЭлементМассива.НормализованныйШтрихкод);
		ДанныеШтрихкода = РезультатОбработки.ДанныеШтрихкода;
		
		НоваяСтрока = ТаблицаСоответствия.Добавить();
		НоваяСтрока.Уровень = ЭлементМассива.Уровень;
		НоваяСтрока.Элемент = ЭлементМассива;
		
		Если ДанныеШтрихкода <> Неопределено Тогда
			
			НоваяСтрока.Номенклатура   = ДанныеШтрихкода.Номенклатура;
			НоваяСтрока.Характеристика = ДанныеШтрихкода.Характеристика;
			НоваяСтрока.Серия          = ДанныеШтрихкода.Серия;
			НоваяСтрока.GTIN           = ДанныеШтрихкода.GTIN;
			
			Если ЗаполнятьМРЦ = Неопределено Тогда
				ЗаполнятьМРЦ = ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(РезультатОбработки.ДанныеШтрихкода, "МРЦ");
			КонецЕсли;
			
			Если ЗаполнятьГоденДо = Неопределено Тогда
				ЗаполнятьГоденДо = ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(РезультатОбработки.ДанныеШтрихкода, "ГоденДо");
			КонецЕсли;
			
			Если ЗаполнятьМРЦ Тогда
				НоваяСтрока.МРЦ = ДанныеШтрихкода.МРЦ;
			КонецЕсли;
			
			Если ЗаполнятьГоденДо Тогда
				НоваяСтрока.ГоденДо = ДанныеШтрихкода.ГоденДо;
			КонецЕсли;
			
		КонецЕсли;
		
		ЭлементМассива.Вставить("РезультатОбработки", РезультатОбработки);
		
	КонецЦикла;
	
	ПоляИндекса = Новый Массив;
	ПоляИндекса.Добавить("Уровень");
	ПоляИндекса.Добавить("Номенклатура");
	ПоляИндекса.Добавить("Характеристика");
	ПоляИндекса.Добавить("Серия");
	ПоляИндекса.Добавить("GTIN");
	Если ЗаполнятьМРЦ <> Неопределено И ЗаполнятьМРЦ Тогда
		ПоляИндекса.Добавить("МРЦ");
	КонецЕсли;
	Если ЗаполнятьГоденДо <> Неопределено И ЗаполнятьГоденДо Тогда
		ПоляИндекса.Добавить("ГоденДо");
	КонецЕсли;
	ТаблицаСоответствия.Индексы.Добавить(СтрСоединить(ПоляИндекса, ","));
	
	ПоляСортировки = Новый Массив;
	ПоляСортировки.Добавить("Уровень ВОЗР");
	ПоляСортировки.Добавить("Номенклатура УБЫВ");
	ПоляСортировки.Добавить("Характеристика УБЫВ");
	ПоляСортировки.Добавить("Серия УБЫВ");
	ПоляСортировки.Добавить("GTIN ВОЗР");
	Если ЗаполнятьМРЦ <> Неопределено И ЗаполнятьМРЦ Тогда
		ПоляСортировки.Добавить("МРЦ УБЫВ");
	КонецЕсли;
	Если ЗаполнятьГоденДо <> Неопределено И ЗаполнятьГоденДо Тогда
		ПоляСортировки.Добавить("ГоденДо УБЫВ");
	КонецЕсли;
	ТаблицаСоответствия.Сортировать(СтрСоединить(ПоляСортировки, ","));
	
	СортированныйМассив = Новый Массив;
	Для Каждого СтрокаТЧ Из ТаблицаСоответствия Цикл
		СортированныйМассив.Добавить(СтрокаТЧ.Элемент);
	КонецЦикла;
	
	ДанныеТСД = СортированныйМассив;
	
КонецПроцедуры

// Переносит ошибки при обработке отдельных штрихкодов в общую ошибку
//
// Параметры:
//   РезультатОбработки - См. РезультатЗагрузкиШтрихкодовИзТСД
Процедура ПроверитьНаОшибкиРезультатОбработкиДанныхТСД(РезультатОбработки, Форма, ПараметрыСканирования) Экспорт
	
	ТребуетсяСопоставлениеНоменклатуры = Ложь;
	
	Для Каждого Элемент Из РезультатОбработки.ШтрихкодыТСД Цикл
		
		Если Элемент.РезультатОбработки = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		
		Если Элемент.РезультатОбработки.ТребуетсяСопоставлениеНоменклатуры Тогда
			ТребуетсяСопоставлениеНоменклатуры = Истина;
		КонецЕсли;
		
		Если Элемент.РезультатОбработки.ОшибкаДопустимостиВидовПродукции
			Или Элемент.РезультатОбработки.ОбщаяОшибка Тогда
			РезультатОбработки.ОбщаяОшибка      = Истина;
			РезультатОбработки.ТекстОбщейОшибки = Элемент.РезультатОбработки.ТекстОшибки;
		КонецЕсли;
		
		Если Элемент.РезультатОбработки.ЕстьОшибкиВДеревеУпаковок Тогда
			РезультатОбработки.ЕстьОшибкиВДереве   = Истина;
			РезультатОбработки.АдресДереваУпаковок = Элемент.РезультатОбработки.АдресДереваУпаковок;
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	
	Если РезультатОбработки.ОбщаяОшибка Тогда
		Возврат;
	КонецЕсли;
	
	Если ТребуетсяСопоставлениеНоменклатуры Тогда
		
		ДанныеДляУточненияСведений = к2ШтрихкодированиеИСВызовСервера.ДанныеДляУточненияСведенийПользователя(Форма.КэшМаркируемойПродукции);
		Если ДанныеДляУточненияСведений.Операция = "СопоставлениеНоменклатуры" Тогда
			РезультатОбработки.ШтрихкодыДляСопоставления = ДанныеДляУточненияСведений.Данные.ШтрихкодыКСопоставлению;
		КонецЕсли;
		
		Возврат;
		
	КонецЕсли;
	
	ДанныеШтрихкодовСОшибками = Новый Массив;
	Для Каждого Элемент Из РезультатОбработки.ШтрихкодыТСД Цикл
		
		Если Элемент.РезультатОбработки = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		
		Если ЗначениеЗаполнено(Элемент.РезультатОбработки.ТекстОшибки)
			И ТипЗнч(Элемент.РезультатОбработки.ДанныеШтрихкода) = Тип("Структура") Тогда
				ДанныеШтрихкодовСОшибками.Добавить(
					Элемент.РезультатОбработки.ДанныеШтрихкода);
		КонецЕсли;
		
	КонецЦикла;
	
	Если ДанныеШтрихкодовСОшибками.Количество() = 0 И Не РезультатОбработки.ЕстьОшибкиВДереве Тогда
		Возврат;
	КонецЕсли;
	
	Если РезультатОбработки.ЕстьОшибкиВДереве И ДанныеШтрихкодовСОшибками.Количество() > 0 Тогда
		
		ДеревоУпаковок = ПолучитьИзВременногоХранилища(РезультатОбработки.АдресДереваУпаковок);
		
		Для Каждого ДанныеШтрихкода Из ДанныеШтрихкодовСОшибками Цикл
			НоваяСтрокаДерева = ДеревоУпаковок.Строки.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаДерева, ДанныеШтрихкода);
			НоваяСтрокаДерева.ЕстьОшибки = Истина;
		КонецЦикла;
		
		ПоместитьВоВременноеХранилище(ДеревоУпаковок, РезультатОбработки.АдресДереваУпаковок);
		
	ИначеЕсли ДанныеШтрихкодовСОшибками.Количество() > 0 Тогда
		
		РезультатОбработки.ЕстьОшибкиВДереве = Истина;
		ДеревоУпаковок = ИнициализироватьДеревоУпаковок(ПараметрыСканирования);
		
		Для Каждого ДанныеШтрихкода Из ДанныеШтрихкодовСОшибками Цикл
			НоваяСтрокаДерева = ДеревоУпаковок.Строки.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаДерева, ДанныеШтрихкода);
			НоваяСтрокаДерева.ЕстьОшибки = Истина;
		КонецЦикла;
		
		РезультатОбработки.АдресДереваУпаковок = ПоместитьВоВременноеХранилище(ДеревоУпаковок, Новый УникальныйИдентификатор);
		
	КонецЕсли;
	
КонецПроцедуры

Функция ИнициализироватьДеревоУпаковок(ПараметрыСканирования) Экспорт
	
	Модуль = ОбщегоНазначения.ОбщийМодуль("к2ШтрихкодированиеИС");
	ДеревоУпаковок = Модуль.ИнициализироватьДеревоУпаковок();
	
	Для Каждого ВидПродукции Из к2ИнтеграцияИСКлиентСервер.ВидыПродукцииИСМП() Цикл
		Если к2ШтрихкодированиеИСКлиентСервер.ДопустимВидПродукции(ПараметрыСканирования, ВидПродукции) Тогда
			Модуль = ОбщегоНазначения.ОбщийМодуль("к2ШтрихкодированиеИСМПСлужебный");
			Модуль.НормализоватьСвойстваКоллекцииВложенныхШтрихкодов(ДеревоУпаковок);
			Прервать;
		КонецЕсли;
	КонецЦикла;
		
	Возврат ДеревоУпаковок;
	
КонецФункции

#КонецОбласти

#Область СохраненныйВыборДляГрупповойОбработки

Процедура ОбработатьУточнениеДанныхДляФормыПроверкиИПодбора(СохраненныйВыбор, ОбновляемыеШтрихкодыУпаковок) Экспорт
	
	ПараметрыСканирования = Неопределено;
	КэшированныеЗначения = Неопределено;
	РезультатВыбора = Новый Структура("ДанныеВыбора", СохраненныйВыбор);
	Для Каждого ОбновляемыйЭлемент Из ОбновляемыеШтрихкодыУпаковок Цикл 
		РезультатОбработки = ОбновляемыйЭлемент.РезультатОбработки;
		РезультатОбработки.ДанныеШтрихкода.Номенклатура = Неопределено; //Сбросим для перезаписи
		к2ШтрихкодированиеИС.ОбработатьУточнениеДанныхДляФормыПроверкиИПодбора(РезультатВыбора, РезультатОбработки, ПараметрыСканирования, КэшированныеЗначения);
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти