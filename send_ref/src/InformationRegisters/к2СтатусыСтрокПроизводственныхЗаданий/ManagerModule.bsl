#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Отражает движения из структуры на регистр
//
// Параметры:
// 	 - Структура - Структура - структура для отражения в регистр
//
Процедура ОтразитьСтатус(Структура) Экспорт
	
	мз = СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(мз, Структура);
	мз.Записать(Истина);
	
КонецПроцедуры

// Образец структуры для отражения в регистр
//
// Возвращаемое значение:
//   - Структура - Структура - структура для отражения в регистр
//
Функция СтруктураОтражения() Экспорт 
	
	Возврат Новый Структура("КодСтрокиЗадания, ПроизводственноеЗадание, СтатусСтроки");
	
КонецФункции

// Удаляет запись из регистра по отбору
//
// Параметры:
//  СтруктураОтбора	 - Структура - должна содержать поля:
//  	- ПроизводственноеЗадание - ДокументСсылка.к2ПроизводственноеЗадание - ссылка на документ производственного задания
//  	- КодСтрокиЗадания - Число - код строки производственного задания
//
Процедура УдалитьЗапись(СтруктураОтбора) Экспорт
	
	НаборЗаписей = РегистрыСведений.к2СтатусыСтрокПроизводственныхЗаданий.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.ПроизводственноеЗадание.Установить(СтруктураОтбора.ПроизводственноеЗадание);
	НаборЗаписей.Отбор.КодСтрокиЗадания.Установить(СтруктураОтбора.КодСтрокиЗадания);
	НаборЗаписей.Записать();
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
