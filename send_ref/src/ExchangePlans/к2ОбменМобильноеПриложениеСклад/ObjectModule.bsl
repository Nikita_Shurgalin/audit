#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Наименование) Тогда
		Если ЭтотУзел Тогда
			Наименование = НСтр("ru = 'Центральный'");
		Иначе
			ТекстНаименование = НСтр("ru = '%Пользователь%'");
			ТекстНаименование = СтрЗаменить(ТекстНаименование, "%Пользователь%", СокрЛП(Пользователь));
			Наименование = ТекстНаименование;
		КонецЕсли;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Код) Тогда
		Если ЭтотУзел Тогда
			Код = "001";
		Иначе
			НовыйКод = СокрЛП(Пользователь.УникальныйИдентификатор());
			ПроверитьНовыйКод(НовыйКод, ЭтотОбъект.Ссылка, Отказ);
			Если Отказ Тогда
				Сообщение = НСтр("ru = 'Для пользователя ""%Пользователь%"" уже существует настройка,
				|которая еще не была задействована для синхронизации данных с мобильным приложением.
				|Используйте ее для настройки обмена данными.'");
				Сообщение = СтрЗаменить(Сообщение, "%Пользователь%", Пользователь);
				Поле = "Пользователь";
				ОбщегоНазначения.СообщитьПользователю(Сообщение, ЭтотОбъект, Поле,, Отказ);
				Возврат;
			Иначе
				Код = НовыйКод;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ ЭтотУзел И ЗначениеЗаполнено(Ссылка) И НЕ ПометкаУдаления
		И Код = СокрЛП(Пользователь.УникальныйИдентификатор()) Тогда
		ПланыОбмена.ЗарегистрироватьИзменения(Ссылка, Метаданные.Справочники.Номенклатура);
		ПланыОбмена.ЗарегистрироватьИзменения(Ссылка, Метаданные.Справочники.к2СерииНоменклатуры);
		ПланыОбмена.ЗарегистрироватьИзменения(Ссылка, Метаданные.Документы.к2УпаковочныйЛист);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередУдалением(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ПланыОбмена.УдалитьРегистрациюИзменений(Ссылка);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьНовыйКод(НовыйКод, СсылкаНаОбъект, Отказ)
	
	Запрос = Новый Запрос("ВЫБРАТЬ
	|	МобильноеПриложениеЗаказыКлиентов.Ссылка
	|ИЗ
	|	ПланОбмена.МобильноеПриложениеЗаказыКлиентов КАК МобильноеПриложениеЗаказыКлиентов
	|ГДЕ
	|	НЕ МобильноеПриложениеЗаказыКлиентов.Ссылка = &СсылкаНаОбъект
	|	И МобильноеПриложениеЗаказыКлиентов.Код = &Код");
	
	Запрос.УстановитьПараметр("СсылкаНаОбъект", СсылкаНаОбъект);
	Запрос.УстановитьПараметр("Код", НовыйКод);
	
	Результат = Запрос.Выполнить();
	
	Если НЕ Результат.Пустой() Тогда
		Отказ = Истина;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#КонецЕсли
