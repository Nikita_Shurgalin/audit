
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СоздатьТаблицуДней(ЭтотОбъект, ИмяТаблицыДней());
	ЗаполнитьТаблицуДней(ЭтотОбъект[ИмяТаблицыДней()]);
	
	ОбновитьСтруктуруВыводаТаблицыДней();
	
КонецПроцедуры

&НаКлиенте
Процедура ГруппаПланированияПриИзменении(Элемент)
	
	ТаблицаВесаДней.Очистить();
	Элементы.ТаблицаВесаДней.Обновить();
	
	ЗаполнитьДанныеВесов();
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаВесаДнейПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	ТекущиеДанные = Элемент.ТекущиеДанные;   
	                                                          
	ВесВСтроке = 0;
	
	Для Каждого СтрокаТоварнаяКатегория Из ЭтотОбъект.ТаблицаДней Цикл 
		
		ВесВСтроке = ВесВСтроке + ТекущиеДанные[ПрефиксВес() + СтрокаТоварнаяКатегория.ИмяКолонки];
		
	КонецЦикла;
	
	Если ВесВСтроке > 1 Тогда
		
		ТекущиеДанные[СтрЗаменить(Элемент.ТекущийЭлемент.Имя, Элемент.Имя, "")] = 0;
		
		ТекстСообщения = НСтр("ru='У товарной категории %1 значение в строке > 1.'");
		ТекстСообщения = стрЗаполнить(ТекстСообщения, Строка(ТекущиеДанные.ТоварнаяКатегория));
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Записать(Команда)
	
	ЗаписатьНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаСервере
Процедура ЗаписатьНаСервере()
	
	Для Каждого СтрокаВесов Из ТаблицаВесаДней Цикл
		
		Для Каждого СтрокаТоварнаяКатегория Из ЭтотОбъект.ТаблицаДней Цикл 
			
			СтруктураЗаписи = Новый Структура;
			СтруктураЗаписи.Вставить("ГруппаПланирования", ГруппаПланирования);
			СтруктураЗаписи.Вставить("ТоварнаяКатегория", СтрокаВесов.ТоварнаяКатегория);
			СтруктураЗаписи.Вставить("НомерДняНедели", СтрокаТоварнаяКатегория.НомерДня);
			СтруктураЗаписи.Вставить("Вес", СтрокаВесов[ПрефиксВес() + СтрокаТоварнаяКатегория.ИмяКолонки]);
			
			РегистрыСведений.к2ВесаДнейБалансировки.Отразить(СтруктураЗаписи);
			
		КонецЦикла;
		
	КонецЦикла;
	                                         
КонецПроцедуры

#Область ОбщиеМетодыРаботыСКроссТаблицами

&НаСервереБезКонтекста
Процедура СоздатьТаблицуДней(Форма, Знач ИмяТаблицыДней) Экспорт 
	
	РеквизитыВерхнегоУровня = Форма.ПолучитьРеквизиты();
	
	ОписаниеТиповТЗ = Новый ОписаниеТипов("ТаблицаЗначений");
	ОписаниеТиповЧ = к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(5, 2); 
	ОписаниеТиповБулево = Новый ОписаниеТипов("Булево");
	ОписаниеТиповСтрока = Новый ОписаниеТипов("Строка");
	ОписаниеНомерДня = Новый ОписаниеТипов("Число");
	
	// Создание реквизитов  таблицы
	ДобавляемыеРеквизиты = Новый Массив();
	РеквизитСуществует = Ложь;
	Для Каждого Реквизит Из РеквизитыВерхнегоУровня Цикл
		Если Реквизит.Имя = ИмяТаблицыДней Тогда
			РеквизитСуществует = Истина;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	Если НЕ РеквизитСуществует Тогда
		Реквизит = Новый РеквизитФормы(ИмяТаблицыДней, ОписаниеТиповТЗ);
		ДобавляемыеРеквизиты.Добавить(Реквизит);
		
		Реквизит = Новый РеквизитФормы("ИмяКолонки", ОписаниеТиповСтрока, ИмяТаблицыДней);
		ДобавляемыеРеквизиты.Добавить(Реквизит);
		
		Реквизит = Новый РеквизитФормы("НомерДня", ОписаниеНомерДня, ИмяТаблицыДней);
		ДобавляемыеРеквизиты.Добавить(Реквизит);
		
		Реквизит = Новый РеквизитФормы("Заголовок", ОписаниеТиповСтрока, ИмяТаблицыДней);
		ДобавляемыеРеквизиты.Добавить(Реквизит);
		
	КонецЕсли;
	
	Если ДобавляемыеРеквизиты.Количество() > 0 Тогда
		Форма.ИзменитьРеквизиты(ДобавляемыеРеквизиты);
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ЗаполнитьТаблицуДней(ТаблицаДней) Экспорт 

	ТаблицаДней.Очистить();
	
	Для Каждого НомерДня Из МассивДни() Цикл
	
		НоваяСтрока = ТаблицаДней.Добавить();
		НоваяСтрока.ИмяКолонки = СтрЗаменить(Строка(Новый УникальныйИдентификатор),"-","_");
		НоваяСтрока.НомерДня = НомерДня;
		НоваяСтрока.Заголовок = Строка(НомерДня);
		
	КонецЦикла;	
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ОбновитьСтруктуруВыводаКроссТаблицы(Форма, Параметры) Экспорт
	
	ИмяРеквизитаКроссТаблицы = Параметры.ИмяРеквизитаКроссТаблицы;
	ЭлементФормыКроссТаблицы = Параметры.ЭлементФормыКроссТаблицы;
	ТаблицаДней = Параметры.ТаблицаДней;
	Поля = Параметры.Поля;
	
	Если Параметры.Свойство("ВыводитьПредыдущийПериод") Тогда
		ВыводитьПредыдущийПериод = Параметры.ВыводитьПредыдущийПериод;
	Иначе
		ВыводитьПредыдущийПериод = Ложь;
	КонецЕсли;
	
	Если Параметры.Свойство("СоздаватьОбщуюГруппу") Тогда
		СоздаватьОбщуюГруппу = Параметры.СоздаватьОбщуюГруппу;
	Иначе
		СоздаватьОбщуюГруппу = Ложь;
	КонецЕсли;
	Если Параметры.Свойство("СоздаватьГруппуПериода") Тогда
		СоздаватьГруппуПериода = Параметры.СоздаватьГруппуПериода;
	Иначе
		СоздаватьГруппуПериода = Ложь;
	КонецЕсли;
	Если Параметры.Свойство("ЗаголовокПоляГруппировки") Тогда
		ЗаголовокПоляГруппировки = Параметры.ЗаголовокПоляГруппировки;
	Иначе
		ЗаголовокПоляГруппировки = "";
	КонецЕсли;
	
	ДобавляемыеРеквизиты = Новый Массив();
	УдаляемыеРеквизиты = Новый Массив();
	РеквизитыТаблицыПлана = Форма.ПолучитьРеквизиты(ИмяРеквизитаКроссТаблицы);
	РеквизитыВерхнегоУровня = Форма.ПолучитьРеквизиты();
	
	Для каждого СтруктураПоля Из Поля Цикл
		
		Если СтруктураПоля.Свойство("СоздаватьРеквизит") Тогда
			СоздаватьРеквизит = СтруктураПоля.СоздаватьРеквизит;
		Иначе
			СоздаватьРеквизит = Истина;
		КонецЕсли;
		Если НЕ СоздаватьРеквизит Тогда
			Продолжить;
		КонецЕсли;
		
		ПрефиксРеквизитаКолонки = СтруктураПоля.ПрефиксРеквизитаКолонки;
		Если СтруктураПоля.Свойство("УдалятьРеквизитыТаблицы") Тогда
			УдалятьРеквизитыТаблицы = СтруктураПоля.УдалятьРеквизитыТаблицы;
		Иначе
			УдалятьРеквизитыТаблицы = Ложь;
		КонецЕсли;
		Если СтруктураПоля.Свойство("ТипЭлемента") Тогда
			ТипЭлемента = СтруктураПоля.ТипЭлемента;
		Иначе
			ТипЭлемента = "Число";
		КонецЕсли;
		Если СтруктураПоля.Свойство("СоздаватьИтоговыеРеквизиты") Тогда
			СоздаватьИтоговыеРеквизиты = СтруктураПоля.СоздаватьИтоговыеРеквизиты;
		Иначе
			СоздаватьИтоговыеРеквизиты = Ложь;
		КонецЕсли;
		Если СтруктураПоля.Свойство("РасширенныйФорматЧислаДаты") Тогда
			РасширенныйФорматЧислаДаты = СтруктураПоля.РасширенныйФорматЧислаДаты;
		Иначе
			РасширенныйФорматЧислаДаты = Ложь;
		КонецЕсли;
				
		Если СоздаватьИтоговыеРеквизиты И ТипЭлемента <> "Число" И ТипЗнч(ТипЭлемента) <> Тип("ОписаниеТипов") Тогда
			СоздаватьИтоговыеРеквизиты = Ложь;
		КонецЕсли;
		
		Если ТипЗнч(ТипЭлемента) = Тип("ОписаниеТипов") Тогда
			ОписаниеТипов = ТипЭлемента;
		ИначеЕсли ТипЭлемента = "Булево" Тогда
			ОписаниеТипов = Новый ОписаниеТипов("Булево");
		ИначеЕсли ТипЭлемента = "Дата" Тогда
			
			Если РасширенныйФорматЧислаДаты Тогда
				ОписаниеТипов = к2ОбщегоНазначения.ПолучитьОписаниеТиповДаты(ЧастиДаты.ДатаВремя);
			Иначе
				ОписаниеТипов = к2ОбщегоНазначения.ПолучитьОписаниеТиповДаты(ЧастиДаты.Дата);
			КонецЕсли;
		Иначе
			
			Если РасширенныйФорматЧислаДаты Тогда
				ОписаниеТипов = к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(5, 2);
			Иначе
				ОписаниеТипов = к2ОбщегоНазначения.ПолучитьОписаниеТиповЧисла(5, 2); 
			КонецЕсли; 
			
		КонецЕсли;
		
		// Удаление реквизитов
		Если УдалятьРеквизитыТаблицы Тогда
			Для Каждого Реквизит Из РеквизитыТаблицыПлана Цикл
				Если СтрНайти(Реквизит.Имя, ПрефиксРеквизитаКолонки) Тогда
					УдаляемыеРеквизиты.Добавить(ИмяРеквизитаКроссТаблицы+"." + Реквизит.Имя);
				КонецЕсли;
			КонецЦикла;
		КонецЕсли;
		
		// Создание реквизитов  таблицы
		Для каждого СтрокаДень Из ТаблицаДней Цикл
			
			РеквизитСуществует = Ложь;
			Для Каждого Реквизит Из РеквизитыТаблицыПлана Цикл
				Если Реквизит.Имя = ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки Тогда
					РеквизитСуществует = Истина;
					Прервать;
				КонецЕсли;
			КонецЦикла;
			Если РеквизитСуществует Тогда
				Продолжить;
			КонецЕсли; 
			
			Реквизит = Новый РеквизитФормы(ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки, ОписаниеТипов, ИмяРеквизитаКроссТаблицы, 
				СтрокаДень.Заголовок);
			ДобавляемыеРеквизиты.Добавить(Реквизит);
		КонецЦикла;
		
		// Создаем итоговые реквизиты 
		Если СоздаватьИтоговыеРеквизиты Тогда
			ОписаниеТиповСтрока = Новый ОписаниеТипов("Строка");
			Для каждого СтрокаДень Из ТаблицаДней Цикл
				
				РеквизитСуществует = Ложь;
				Для Каждого Реквизит Из РеквизитыВерхнегоУровня Цикл
					Если Реквизит.Имя = СтрЗаменить(ИмяРеквизитаКроссТаблицы + ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки,".","_") Тогда
						РеквизитСуществует = Истина;
						Прервать;
					КонецЕсли;
				КонецЦикла;
				Если РеквизитСуществует Тогда
					Продолжить;
				КонецЕсли; 
				
				ПрефиксРеквизита = НСтр("ru='Итого'");
				
				Реквизит = Новый РеквизитФормы(СтрЗаменить(ИмяРеквизитаКроссТаблицы + ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки,".","_"), ОписаниеТиповСтрока, 
					, ПрефиксРеквизита + " " + СтрокаДень.Заголовок);
				ДобавляемыеРеквизиты.Добавить(Реквизит);
			КонецЦикла;
		КонецЕсли;
		
	КонецЦикла;
	
	Форма.ИзменитьРеквизиты(ДобавляемыеРеквизиты, УдаляемыеРеквизиты);
	
	// Удаление элементов формы
	УдаляемыеЭлементы = Новый Массив();
	Для Каждого ТекЭлемент Из Форма.Элементы[ЭлементФормыКроссТаблицы].ПодчиненныеЭлементы Цикл
		Если СтрНайти(ТекЭлемент.Имя, ЭлементФормыКроссТаблицы + "ГруппаРеквизитов") Тогда
			УдаляемыеЭлементы.Добавить(ТекЭлемент);
			Продолжить;
		КонецЕсли;
		ЭлементНайден = Ложь;
		Для каждого СтрокаДень Из ТаблицаДней Цикл
			Если  СтрНайти(ТекЭлемент.Имя, ЭлементФормыКроссТаблицы + "Группа"+СтрокаДень.ИмяКолонки) Тогда
				УдаляемыеЭлементы.Добавить(ТекЭлемент);
				ЭлементНайден = Истина;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		Если ЭлементНайден Тогда
			Продолжить;
		КонецЕсли; 
		Для каждого СтруктураПоля Из Поля Цикл
			ПрефиксРеквизитаКолонки = СтруктураПоля.ПрефиксРеквизитаКолонки;
			Если СтрНайти(ТекЭлемент.Имя, ПрефиксРеквизитаКолонки) > 0  Тогда
				УдаляемыеЭлементы.Добавить(ТекЭлемент);
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	Для Каждого УдаляемыйЭлемент Из УдаляемыеЭлементы Цикл
		Форма.Элементы.Удалить(УдаляемыйЭлемент);
	КонецЦикла;
	
	Если СоздаватьОбщуюГруппу Тогда
			ЭлементФормы = Форма.Элементы.Добавить(ЭлементФормыКроссТаблицы + "ГруппаРеквизитов", Тип("ГруппаФормы"), 
				Форма.Элементы[ЭлементФормыКроссТаблицы]);
			ЭлементФормы.Группировка      = ГруппировкаКолонок.Горизонтальная;
			ЭлементФормы.ОтображатьВШапке = Истина;
			ЭлементФормы.Заголовок        = ЗаголовокПоляГруппировки;
			
			ВладелецПоля = ЭлементФормы;
	Иначе
		ВладелецПоля = Форма.Элементы[ЭлементФормыКроссТаблицы];
	КонецЕсли; 
	
	Для каждого СтрокаДень Из ТаблицаДней Цикл
		
		ЭлементДляПодвала = Неопределено;
		
		Если СоздаватьГруппуПериода Тогда
			ЭлементФормы = Форма.Элементы.Добавить(ЭлементФормыКроссТаблицы + "Группа"+СтрокаДень.ИмяКолонки, Тип("ГруппаФормы"), 
				Форма.Элементы[ЭлементФормыКроссТаблицы]);
			ЭлементФормы.Группировка      = ГруппировкаКолонок.Горизонтальная;
			ЭлементФормы.ОтображатьВШапке = Истина;
			ЭлементФормы.Заголовок        = СтрокаДень.Заголовок;
			
			ВладелецПоля = ЭлементФормы;
		КонецЕсли;
		
		Для каждого СтруктураПоля Из Поля Цикл
			ПрефиксРеквизитаКолонки = СтруктураПоля.ПрефиксРеквизитаКолонки;
			Если СтруктураПоля.Свойство("СоздаватьЭлемент") Тогда
				СоздаватьЭлемент = СтруктураПоля.СоздаватьЭлемент;
			Иначе
				СоздаватьЭлемент = Истина;
			КонецЕсли;
			Если СтруктураПоля.Свойство("СоздаватьИтоговыеРеквизиты") Тогда
				СоздаватьИтоговыеРеквизиты = СтруктураПоля.СоздаватьИтоговыеРеквизиты;
			Иначе
				СоздаватьИтоговыеРеквизиты = Ложь;
			КонецЕсли;
			Если СтруктураПоля.Свойство("ПоказыватьИтоговыеРеквизитыВПредыдущемЭлементе") Тогда
				ПоказыватьИтоговыеРеквизитыВПредыдущемЭлементе = СтруктураПоля.ПоказыватьИтоговыеРеквизитыВПредыдущемЭлементе;
			Иначе
				ПоказыватьИтоговыеРеквизитыВПредыдущемЭлементе = Ложь;
			КонецЕсли;
			Если СтруктураПоля.Свойство("ПоказыватьИтоговыеРеквизитыВГруппе") Тогда
				ПоказыватьИтоговыеРеквизитыВГруппе = СтруктураПоля.ПоказыватьИтоговыеРеквизитыВГруппе;
			Иначе
				ПоказыватьИтоговыеРеквизитыВГруппе = Ложь;
			КонецЕсли;
			Если СтруктураПоля.Свойство("ШиринаЭлемента") Тогда
				ШиринаЭлемента = СтруктураПоля.ШиринаЭлемента;
			Иначе
				ШиринаЭлемента = 6;
			КонецЕсли;
			Если СтруктураПоля.Свойство("СтруктураДействий") Тогда
				СтруктураДействий = СтруктураПоля.СтруктураДействий;
			Иначе
				СтруктураДействий = Новый Структура();
			КонецЕсли;
			Если СтруктураПоля.Свойство("ТипЭлементаФормы") Тогда
				ТипЭлементаФормы = СтруктураПоля.ТипЭлементаФормы;
			Иначе
				ТипЭлементаФормы = Тип("ПолеФормы");
			КонецЕсли;
			Если СтруктураПоля.Свойство("ВидПоля") Тогда
				ВидПоля = СтруктураПоля.ВидПоля;
			Иначе
				ВидПоля = ВидПоляФормы.ПолеВвода;
			КонецЕсли;
			Если СтруктураПоля.Свойство("ПрефиксГруппы") Тогда
				РодительЭлемента = Форма.Элементы[ЭлементФормыКроссТаблицы + СтруктураПоля.ПрефиксГруппы + СтрокаДень.ИмяКолонки];
			Иначе
				РодительЭлемента = ВладелецПоля;
			КонецЕсли;
			
			Если СоздаватьЭлемент Тогда
				// Перепривязываем данные
				// Создание элементов формы
				Элемент = Форма.Элементы.Добавить(ЭлементФормыКроссТаблицы + ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки, 
					ТипЭлементаФормы, РодительЭлемента);
				Элемент.Вид = ВидПоля;
				Если ТипЭлементаФормы = Тип("ПолеФормы") ИЛИ ТипЭлементаФормы = Тип("ТаблицаФормы") Тогда
					Элемент.ПутьКДанным = ИмяРеквизитаКроссТаблицы+"."+ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки;
				КонецЕсли;
				Если Элемент.Вид <> ВидПоляФормы.ПолеФлажка Тогда
					Элемент.Ширина = ШиринаЭлемента;
				КонецЕсли; 
				Если СтруктураПоля.Свойство("Заголовок") Тогда
					Элемент.Заголовок = СтруктураПоля.Заголовок;
				КонецЕсли; 
				Если СтруктураПоля.Свойство("СвойстваЭлемента") И ТипЗнч(СтруктураПоля.СвойстваЭлемента) = Тип("Структура") Тогда
					ЗаполнитьЗначенияСвойств(Элемент, СтруктураПоля.СвойстваЭлемента);
				КонецЕсли; 
				Для каждого Действие Из СтруктураДействий Цикл
					Элемент.УстановитьДействие(Действие.Ключ, Действие.Значение);
				КонецЦикла;
				ЭлементДляПодвала = Элемент;
			КонецЕсли;
			
			Если СоздаватьИтоговыеРеквизиты 
				И (СоздаватьЭлемент 
					ИЛИ ПоказыватьИтоговыеРеквизитыВПредыдущемЭлементе 
					ИЛИ ПоказыватьИтоговыеРеквизитыВГруппе) Тогда
				Если (СоздаватьЭлемент ИЛИ ПоказыватьИтоговыеРеквизитыВПредыдущемЭлементе) И ЭлементДляПодвала <> Неопределено Тогда
					ЭлементДляПодвала.ПутьКДаннымПодвала = СтрЗаменить(ИмяРеквизитаКроссТаблицы + ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки, ".","_");
				КонецЕсли;
				
				Если ПоказыватьИтоговыеРеквизитыВГруппе 
					И СоздаватьГруппуПериода 
					И ВладелецПоля <> Неопределено 
					И ТипЗнч(ВладелецПоля) = Тип("ГруппаФормы") 
					И ВладелецПоля.ОтображатьВШапке Тогда
					Если НЕ ПустаяСтрока(ВладелецПоля.Заголовок) Тогда
						ВладелецПоля.Заголовок = ВладелецПоля.Заголовок + ": ";
					КонецЕсли; 
					ВладелецПоля.ПутьКДаннымШапки = СтрЗаменить(ИмяРеквизитаКроссТаблицы + ПрефиксРеквизитаКолонки + СтрокаДень.ИмяКолонки, ".","_");
					ВладелецПоля.ФорматШапки = "ЧДЦ=2; ЧН=0,00";
				КонецЕсли;
			КонецЕсли;
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ДобавитьЧисловоеПолеКроссТаблицы(ПараметрыВывода, СоздаватьЭлемент = Истина, 
	СтруктураДействий = Неопределено, ИмяПоля, Заголовок, ТолькоПросмотр) Экспорт 

	Если НЕ ПараметрыВывода.Свойство("Поля") Тогда
		ПараметрыВывода.Вставить("Поля", Новый Массив());
	КонецЕсли;
	
	СтруктураПоля = Новый Структура;
	СтруктураПоля.Вставить("ПрефиксРеквизитаКолонки", ИмяПоля);
	СтруктураПоля.Вставить("УдалятьРеквизитыТаблицы", Истина);
	СтруктураПоля.Вставить("СоздаватьЭлемент", СоздаватьЭлемент);
	СтруктураПоля.Вставить("ТипЭлемента", "Число");
	Если ЗначениеЗаполнено(Заголовок) Тогда
		СтруктураПоля.Вставить("Заголовок", Заголовок);
	КонецЕсли;
	СтруктураПоля.Вставить("СоздаватьИтоговыеРеквизиты", Истина);
	СтруктураПоля.Вставить("ШиринаЭлемента", 6);
	СтруктураПоля.Вставить("СвойстваЭлемента", Новый Структура());
	СтруктураПоля.СвойстваЭлемента.Вставить("ВыделятьОтрицательные", Истина);
	СтруктураПоля.СвойстваЭлемента.Вставить("ТолькоПросмотр", ТолькоПросмотр);
	СтруктураПоля.СвойстваЭлемента.Вставить("Формат", "ЧН=0");
	
	Если СтруктураДействий <> Неопределено Тогда
		СтруктураПоля.Вставить("СтруктураДействий", СтруктураДействий);
	КонецЕсли; 
	
	ПараметрыВывода.Поля.Добавить(СтруктураПоля);

КонецПроцедуры

#КонецОбласти

&НаСервере
Процедура ОбновитьСтруктуруВыводаТаблицыДней()
	
	ПараметрыВывода = Новый Структура;
	ПараметрыВывода.Вставить("ИмяРеквизитаКроссТаблицы", "ТаблицаВесаДней");
	ПараметрыВывода.Вставить("ЭлементФормыКроссТаблицы", "ТаблицаВесаДней");
	ПараметрыВывода.Вставить("ТаблицаДней", ЭтотОбъект.ТаблицаДней.Выгрузить());
	ПараметрыВывода.Вставить("ГруппироватьПоля", Истина);
	ПараметрыВывода.Вставить("СоздаватьГруппуПериода", Ложь);
	ПараметрыВывода.Вставить("Поля", Новый Массив());
	
	ДобавитьЧисловоеПолеКроссТаблицы(ПараметрыВывода, Истина, Неопределено, ПрефиксВес(), , Ложь);
	ОбновитьСтруктуруВыводаКроссТаблицы(ЭтотОбъект, ПараметрыВывода);

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеВесов()
	
	ТаблицаВесаДней.Очистить();
	
	Для Каждого СтрокаВесов Из ТаблицаВесаДней() Цикл
		
		СтрокиТоварныеКатегории = ЭтотОбъект.ТаблицаВесаДней.НайтиСтроки(Новый Структура("ТоварнаяКатегория", СтрокаВесов.ТоварнаяКатегория));
		
		Если СтрокиТоварныеКатегории.Количество() = 0 Тогда
			СтрокаТоварнаяКатегория = ТаблицаВесаДней.Добавить();
			СтрокаТоварнаяКатегория.ТоварнаяКатегория = СтрокаВесов.ТоварнаяКатегория;
		Иначе 
			СтрокаТоварнаяКатегория = СтрокиТоварныеКатегории[0];
		КонецЕсли;
		
		СтрокиДней = ЭтотОбъект.ТаблицаДней.НайтиСтроки(Новый Структура("НомерДня", СтрокаВесов.НомерДня));
		Если СтрокиДней.Количество() > 0 Тогда
			ИдентификаторДень = СтрокиДней[0].ИмяКолонки;
			СтрокаТоварнаяКатегория[ПрефиксВес() + ИдентификаторДень] = СтрокаВесов.Вес;
		КонецЕсли;
		
	КонецЦикла;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ИмяТаблицыДней()

	Возврат "ТаблицаДней";
	
КонецФункции	

&НаКлиентеНаСервереБезКонтекста
Функция ПрефиксВес()
	
	Возврат "Вес_";
	
КонецФункции

&НаСервереБезКонтекста
Функция МассивДни()
	
	Массив = Новый Массив;
	
	Для НомерДня = 1 По 7 Цикл 
		Массив.Добавить(НомерДня);
	КонецЦикла;
	
	Возврат Массив;
	
КонецФункции

&НаСервере
Функция ТаблицаВесаДней()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ксВесаДнейБалансировки.ГруппаПланирования КАК ГруппаПланирования,
		|	ксВесаДнейБалансировки.НомерДняНедели КАК НомерДня,
		|	ЕСТЬNULL(ксВесаДнейБалансировки.Вес, 0) КАК Вес,
		|	ксВесаДнейБалансировки.ТоварнаяКатегория КАК ТоварнаяКатегория
		|ПОМЕСТИТЬ ВТ
		|ИЗ
		|	РегистрСведений.к2ВесаДнейБалансировки КАК ксВесаДнейБалансировки
		|ГДЕ
		|	ксВесаДнейБалансировки.ГруппаПланирования = &ГруппаПланирования
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ТоварныеКатегории.Ссылка КАК ТоварнаяКатегория,
		|	ВТ.НомерДня КАК НомерДня,
		|	ВТ.Вес КАК Вес
		|ИЗ
		|	Справочник.к2ТоварныеКатегории КАК ТоварныеКатегории
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ КАК ВТ
		|		ПО ТоварныеКатегории.Ссылка = ВТ.ТоварнаяКатегория";
	
	Запрос.УстановитьПараметр("ГруппаПланирования", ГруппаПланирования);

	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

#КонецОбласти

#КонецОбласти


