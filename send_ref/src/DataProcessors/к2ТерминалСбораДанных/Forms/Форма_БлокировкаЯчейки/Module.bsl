
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
		
	к2ТерминалСбораДанныхСервер.ПриСозданииНаСервере(ЭтотОбъект, СтандартнаяОбработка, Отказ);
	
	Если Не УчетПоЯчейкамНаСкладе Тогда
		Отказ = Истина;	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
		
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(, ЭтотОбъект, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" Тогда
		Если ИмяСобытия = "ScanData" Тогда
			
			ОбработатьШтрихкодНаСервере(Параметр[0]);
	
			Если ЗначениеЗаполнено(Объект.ТекстОшибки) Тогда 		
				к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(Объект.ТекстОшибки);	    
			КонецЕсли;
		
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование

	Если ИмяСобытия = "ЗакрытьФормуТерминалСбораДанных" Тогда
		Объект.ПодтвержденоЗакрытиеФормы = Истина;
		Закрыть();
	КонецЕсли

КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	к2ТерминалСбораДанныхКлиент.ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка, Объект)
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(, ЭтотОбъект);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура АдресЯчейкиПриИзменении(Элемент)
	
	ОбработатьШтрихкодНаСервере(АдресЯчейки);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

#Область СтраницаПометкаЯчейки	

&НаКлиенте
Процедура ПометкаЯчейкиОк(Команда)
	
	ЗавершитьПометкуЯчейки();	
	
КонецПроцедуры

&НаКлиенте
Процедура ПометкаЯчейкиОтмена(Команда)
	
	Объект.ПодтвержденоЗакрытиеФормы = Истина;
	Закрыть();
		
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Штрихкод

&НаСервере
Процедура ОбработатьШтрихкодНаСервере(Штрихкод)
	
	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");	
	
	ОбработкаОбъект.ОбработатьШтрихкод_СканированиеЯчейки(Штрихкод);
	
	ЗначениеВРеквизитФормы(ОбработкаОбъект, "Объект");
	
	ЯчейкаБлокировки = Объект.ТекущаяЯчейка;
	
	Если ЗначениеЗаполнено(ЯчейкаБлокировки) Тогда 
		АдресЯчейки = Штрихкод;		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиенте
Процедура ЗавершитьПометкуЯчейки()
		
	Если Не ЗначениеЗаполнено(ЯчейкаБлокировки) Тогда 
		
		ИмяСтраницыВозвратаПослеОшибки = Элементы.ГруппаСтраницы.ТекущаяСтраница.Имя;
		к2ТерминалСбораДанныхКлиент.ПоказатьОшибкуПользователю(НСтр("ru = 'Сканируйте ячейку.'"));
	    
	Иначе
		
		ЗаблокироватьЯчейкуНаСервере(ЯчейкаБлокировки);
		ЯчейкаБлокировки = Неопределено;
		АдресЯчейки = Неопределено;
		к2ТерминалСбораДанныхКлиент.ПоказатьОповещениеПользователю(стрЗаполнить(НСтр("ru = 'Ячейка %1 заблокирована'"), ЯчейкаБлокировки));
		
	КонецЕсли;
			
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ЗаблокироватьЯчейкуНаСервере(Ячейка)
	
	СтруктураЗаписи = Новый Структура("Ячейка, ПричинаБлокировки");
	СтруктураЗаписи.Ячейка = Ячейка;
	СтруктураЗаписи.ПричинаБлокировки = Перечисления.к2ПричиныБлокировкиЯчеек.Отклонение;
	
	РегистрыСведений.к2ЗаблокированныеЯчейки.Отразить(СтруктураЗаписи);

КонецПроцедуры

#КонецОбласти

#КонецОбласти
